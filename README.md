# Managing HBM Bandwidth of FPGAs with Overlay NoCs

In this repository, we provide the RTL implementation of BFT and Hoplite NoC overlays used to interface Processing Elements with HBM in Xilinx Alveo U280. We provide Functional Demo(s) for characterizing the impact of our NoCs on HBM's throughput using synthetic benchmarks and a dense matrix multiplication application. We also provide FPGA mapping scripts that map our NoCs onto Alveo U280 using Vivado's implementation flow.

## Motivation and Idea
The Xilinx Alveo U280 FPGA offers 8GB of HBM divided
into two stacks, subdivided into 16 pseudo channels and
controlled by eight memory controllers. Each pseudo channel
has a capacity of 256 MB with a data width of 64 bits, and
they are exposed to the FPGA fabric as 32 AXI3 ports. Consequently, the total bandwidth of the HBM is split across the 32 ports. To access the entire memory space from any HBM port, a hardened internal crossbar is used. It is constructed using 8 AXI switches with lateral links, where four AXI3 ports (masters) and their corresponding four pseudo channels (slaves) are interfaced together using one AXI switch - we call this grouping a “bank”. A section of this crossbar is shown below. 

<img src="pics/HBMInternalCrossbar.png">

As seen in the figure, an AXI switch is then connected to adjoining AXI switches
using four lateral links (two on either side). Where each lateral link is shared by two masters, limiting the available throughput to 50% of the maximum throughput. This setup also requires a dead cycle to switch between the masters during writes, which further degrades throughput.

Thus, the throughput bottlenecks of HBM stem from the dependence on the internal crossbar to support full addressing. Therefore we propose using popular NoC overlays such as BFT and Hoplite to perform global addressing by routing the memory packets to their corresponding AXI ports directly; we call this process “homing” the memory
packets as they get routed to their correct port without needing the internal crossbar.


## Understanding the Synthetic Benchmarks

We conduct synthetic benchmarking experiments using 32
PEs. Each PE issues write transactions of 32B totaling 256MB,
followed by read requests to the same locations. PEs choose
the target port id for read and write transactions based on the
addressing policy. The addressing policies used in our experiments are tabulated in the following figure. 


<img src="pics/SyntheticBenchmarkPolicies.png">


The PE position, addressing policy, and radius determine the
pseudo channel(s). Consequently, the AXI port is used by
the PE in writing and reading memory transactions. The addressing
policy is the logic that computes the target pseudo channel
P. The radius defines the number of potential pseudo channels
that the PE can target near channel P. For instance, if the radius
is 1, the PE can target only pseudo channel P. If the radius
is increased to four, however, the PEs will target the nearest
four channels centered around P with uniform pseudo-random
probability. We use the syntax AP-R to identify the addressing
policy used, where AP is the addressing policy, and R is
the radius. The offset field, however, determines the memory
location within a pseudo channel. Hence the throughput is
not affected by the internal crossbar and is not influenced by
the NoC. The methodology used to generate memory offsets
is trivial, and PEs generate offsets linearly.

## Prerequisites for Demos

* [Vivado 2019.2](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2019-2.html)
* [Vitis 2019.2](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis/2019-2.html)
* [Alveo U280 2019.2 XDMA files](https://www.xilinx.com/products/boards-and-kits/alveo/package-files-archive/u280-2019-2.html)
* Alveo U280 Vivado board files
* Host with Alveo U280 installed (Not required for FPGA Mapping Demo)
* Python 3.2

  
## Functional Demo - Synthetic Benchmarks

Our functional demo uses Vitis and Vivado to implement and execute our design in Xilinx's Alveo U280. The Processing Elements used in this demo run several synthetic workloads on the board to characterize the HBM throughput performance. To run the synthetic benchmark functional demo, execute the following instruction from the top folder of the repository. The automated script packages the design as a kernel and calls `v++` to synthesize, place and route the design targetting Xilinx Alveo U280. We use Vitis compiler options to instantiate profilers, which we use to extract the port level throughput achieved by the kernel. The script also compiles the host code. It programs the Alveo U280 board with the generated bitstream and initiates the benchmarking with the desired workload (supplied to it as a run time argument). The `collect_results.sh` script, located in the `Vitis/Hardware` folder of the corresponding chosen topology, performs a sweep of available workloads, collects the throughput performance and places it in `average_vitis.csv`.

```bash
$ make run_func_demo TOPO=$TOPO
    Arguments:
        TOPO: The topology of the NoC. Valid options are:
            1. noNoC_SF - The Single flit(SF) PEs are interfaced directly to HBM
            2. BFT_SF - The  SF PEs are interfaced to HBM using SF BFT
            3. HOP_SF - The  SF PEs are interfaced to HBM using SF Hoplite
            4. noNoC_MF - The Multi flit (MF) PEs are interfaced directly to HBM
            5. BFT_MF - The  MF PEs are interfaced to HBM using MF BFT
            6. SWEEP - Runs all the above configurations

Example:
$ make run_func_demo TOPO=noNoC_SF
            
```

## Understanding the output of the demo

Depending on the input, this can take anywhere from 2 to 5 hrs to complete.  After the execution, the port level bandwidth (in MBps) measured for the various experiments is printed. For example, if `TOPO` was set `noNoC_MF`, the following will be printed after the execution.

```bash
$ make run_func_demo TOPO=noNoC_SF
.
.
.
policy,burst_len,bandwidth
P2P-32,1,210.072
P2P-8,1,395.131
P2P-4,1,488.311
P2P-2,1,732.571
P2P-1,1,3919.91
CB-1,1,3310.6
CB-4,1,502.125
CS-1,1,1393.08
CS-16,1,218.997
BR,1,2147.8
TOR,1,1427.93
NN,1,3862.45
CC,1,1398.31

```

Instead, suppose `TOPO` was set to SWEEP instead. In that case, the script will implement all the configurations mentioned above and collect, group the results based on the policy, and place the results in CSV format in the `results` folder. It will also generate plots and place them in `results/plots` folder.


Alternatively, the generated binary can be executed manually. For instance, to execute `noNoC_SF` design, navigate to `sources/single_flit/clients_only/Vitis/Hardware/`, and run the following command in the terminal.

```bash
$ ./clients_only binary_container_1.xclbin cmd
    Arguments:
        cmd: Determines the synthetic workload used by the processing elements. Valid options are:
        1. 0xff020  - P2P-32
        2. 0xff008  - P2P-8
        3. 0xff004  - P2P-4
        4. 0xff002  - P2P-2 
        5. 0xff001  - P2P-1
        6. 0xff041  - CB-1
        7. 0xff044  - CB-4
        8. 0xff081  - CS-1
        9. 0xff090  - CS-16
        10. 0xff0c1 - BR
        11. 0xff101 - TOR
        12. 0xff141 - NN
        13. 0xff181 - CC

Example
$ ./clients_only binary_container_1.xclbin 0xff001
```
After executing the synthetic workload on the board, the host code will print the number of cycles taken by the processing elements to complete the workload. Following is an example output from one such run. Additionally, Vitis will save the kernel profile data in `profile_summary.csv`, which contains, among other things, the port level bandwidth and execution latency.
```bash
.
.
.
Cycle count m18 axi : 30458359
Cycle count m19 axi : 30492863
Cycle count m20 axi : 30458177
Cycle count m21 axi : 30493568
Cycle count m22 axi : 30457889
Cycle count m23 axi : 30493433
Cycle count m24 axi : 30457919
Cycle count m25 axi : 30492903
Cycle count m26 axi : 30458078
Cycle count m27 axi : 30493572
Cycle count m28 axi : 30458074
Cycle count m29 axi : 30492849
Cycle count m30 axi : 30457996
Cycle count m31 axi : 30493278
```


## Functional Demo - Dense Matrix Multiplication

  To run the Dense Matrix Multiplication functional demo, execute the following instruction from the top folder of the repository. The script first synthesizes and exports the HLS implementation of a dense matrix-matrix multiplication processing element. Like the previous functional demo, the script based on the input packages the source files into a Xilinx object file (.xo), and uses Vitis to create a binary (.xclbin). It also compiles the host code and uses it to execute the binary in the hardware.

```bash
$ make run_mm_demo TOPO=$TOPO
    Arguments:
        TOPO: The topology of the NoC. Valid options are:
            1. noNoC_MF - The Multi flit (MF) PEs are interfaced directly to HBM
            2. BFT_MF - The  MF PEs are interfaced to HBM using MF BFT
            3. SWEEP - Runs all the above configurations

Example:
$ make run_func_demo TOPO=noNoC_SF
            
```

## Understanding the output of the demo

 Depending on the topology used, this can take anywhere from 8 to 13 hrs to complete. The host code loads the HBM with randomly generated input matrices and starts the kernel. The kernel computes the blocks of output matrices assigned to them and writes back the result to HBM. After the execution is completed, the input matrices, the expected output and the actual output from the hardware are printed. If the expected output matches the output from the board, `MATRIX CORRECT` will be printed. Otherwise, `MATRIX INCORRECT` will be printed


 
## FPGA Mapping Demo

Just like the functional demos, the FPGA Mapping Demo uses Vitis and Vivado to implement our design (without the clients) targetting Xilinx'x Alveo U280. It reports the resource breakdown and the maximum frequency achieved by design. To run a functional demo, execute the following instruction from the top folder of the repository.

```bash
$ make run_fp_demo TOPO=$TOPO
    Arguments:
        TOPO: The topology of the NoC. Valid options are:
            1. BFT_SF - Single flit BFT
            2. HOP_SF - Single flit Hoplite
            3. BFT_MF - Multi flit BFT
            4. SWEEP - Runs all the above configurations

Example:
$ make run_fp_demo TOPO=BFT_SF
            
```

## Understanding the output of the demo

After the implementation, the resource breakdown in LUTs and FF consumed by the NoC and Vitis instantiated modules is printed. For example, if `TOPO` was set `BFT_SF`, the following will be printed after the execution.

```bash
$ make run_fp_demo TOPO=BFT_SF
.
.
.
LUTs Consumed by the NoC: 73120.68
FFs Consumed by the NoC: 114957.68
LUTs Consumed by the Vitis: 154460
FFs Consumed by the Vitis: 381436.32
Maximum Frequency (MHz): 321

```
You can also find the associated reports, log files, and the dcp files, by navigating to the corresponding folder. For instance after running `make run_fp_demo TOPO=BFT_SF`, navigate to `/sources/single_flit/bft/Vitis/Hardware/` to find the post routed dcp (`pfm_top_wrapper_routed.dcp`). The logs from Vitis run can be found in `binary_container_1.build/logs/`, and the Vivado implementation log can be found in `binary_container_1.build/link/vivado/vpl/prj/prj.runs/impl_1/`. 

## Debugging the Kernel

1. If the kernel hangs, there are two possible causes. 1) AXI Firewall violations or 2) The kernel "done" conditions haven't been met.
2. To debug the latter, terminate the host code by issuing a SIGINT signal (`CTRL+C`). Then, run `xbutil status --aim. This will print the port level read and write statics, which can be used to track down potential bugs. In rare cases, an ILA might be required to debug the kernel. Follow this [guide](https://www.xilinx.com/html_docs/xilinx2019_2/vitis_doc/Chunk1252764274.html?hl=chipscope#qrf1524519730361) from Xilinx to do so. 
3. To debug AXI Firewall violations follow [these instructions](https://www.xilinx.com/html_docs/xilinx2019_2/vitis_doc/Chunk1252764274.html?hl=chipscope#kak1538739908835) from Xilinx. 