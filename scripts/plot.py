import matplotlib 
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib import rc,rcParams
import numpy as np
import pandas as pd

import csv

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 24}

matplotlib.rc('font', **font)
plt.rcParams["figure.figsize"] = [10, 10]

max_p = [6900,6900,6900,6900,6900]

file_name=["P2P-32","P2P-8","P2P-4","P2P-2","P2P-1","CB-1","CB-4","CS-1","CS-16","BR","TOR","NN","CC"]
bft = []
hoplite = []
no_noc = []
label=['1','2','4','8','16']
cmap1= list(['#CA0407'])
# cmap2= list(['#9AC6EF','#FFE590','#B4E88B'])
cmap2= list(['#A7ACD9','#ED4D6E','#46B1C9'])
for file in file_name:
  with open("../results/"+file+".csv",'r') as csvfile:
      plots = csv.reader(csvfile, delimiter=',')
      next(plots)  
      for row in plots:
#           print(row)
          no_noc.append(float(row[2]))
          bft.append(float(row[3]))
          hoplite.append(float(row[4]))



  bft_df = pd.DataFrame({'Experiment':label,'BFT':bft})
  no_noc_df = pd.DataFrame({'Experiment':label,'No NoC':no_noc})
  hoplite_df = pd.DataFrame({'Experiment':label,'Hoplite':hoplite})

  max_p_df = pd.DataFrame({'Experiment':label,'Theo. Max':max_p})

  df = pd.merge(no_noc_df,bft_df,how='inner',on='Experiment')
  df2 = pd.merge(df,hoplite_df,how='inner',on='Experiment')
  df3 = pd.merge(df2,max_p_df,how='inner',on='Experiment')

  ax = df3[['Experiment','Theo. Max']].plot(x='Experiment',y='Theo. Max', linestyle='-',color=cmap1,legend=False,linewidth=2)

  ax = df3[['Experiment','No NoC','BFT','Hoplite']].plot(x='Experiment',kind='bar',ax=ax,color=cmap2,edgecolor='k',legend=False,linewidth=1.5)
  p,l = ax.get_legend_handles_labels()
  ax.legend(p,l,ncol=4,bbox_to_anchor=(0,1.02,1,0.2), loc="lower center",frameon=False)
  ax.set_ylabel('Bandwidth MBps/port', fontweight='bold',fontsize=26)
  ax.set_xlabel('Axi Burst length', fontweight='bold',fontsize=26)
  plt.setp(ax.spines.values(), linewidth=2)

  # plt.show()
  fig = ax.get_figure()
  plt.subplot.left  : 0.5
  fig.savefig("../results/plots/"+file+".pdf",bbox_inches='tight')
  bft.clear()
  hoplite.clear()
  no_noc.clear()