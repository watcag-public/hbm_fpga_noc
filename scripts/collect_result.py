import pandas as pd



noNoC_sf = pd.read_csv("../sources/single_flit/clients_only/average_vitis.csv",sep=",")
hoplite_sf = pd.read_csv("../sources/single_flit/hoplite/average_vitis.csv",sep=",")
bft_sf =  pd.read_csv("../sources/single_flit/bft/average_vitis.csv",sep=",")
noNoC_mf =  pd.read_csv("../sources/multi_flit/clients_only/average_vitis.csv",sep=",")
bft_mf = pd.read_csv("../sources/multi_flit/bft/average_vitis.csv",sep=",")



policies = ['P2P-32','P2P-8','P2P-4','P2P-2','P2P-1','CB-1','CB-4','CS-1','CS-16','BR','TOR','NN','CC']
for p in policies:
    bft = bft_mf[(bft_mf['policy']==p)]
    bft = pd.concat([bft_sf[(bft_sf['policy']==p)],bft],axis=0)
    noNoC = noNoC_mf[(noNoC_mf['policy']==p)]
    noNoC = pd.concat([noNoC_sf[(noNoC_sf['policy']==p)],noNoC],axis=0)
    hoplite=hoplite_sf[(hoplite_sf['policy']==p)]
    df_par = pd.merge(left=noNoC,right=bft,on=['policy','burst_len'])
    df_full = df_par.merge(hoplite,how='left',on=['policy','burst_len'])
    df_full.columns=['policy','burst_len','noNoC','BFT','Hoplite']
    df_full=df_full.fillna(0)
    df_full.to_csv('../results/'+p+'.csv',index=False)
