#!/bin/bash
export VIVADO=/tools/Xilinx/Vivado/2019.2
export XRT=/opt/xilinx/xrt
export VITIS=/tools/Xilinx/Vitis/2019.2
home='pwd'
FP=1
DEFAULT="SWEEP"
TOPO="${1: -$DEFAULT}"
echo $TOPO

if [[ "$TOPO" == "HOP_SF" || "$TOPO" == "SWEEP" ]]; then
    cd sources/single_flit/hoplite/
    sed -i '$ d' src/phalanx.h
    echo "\`define FP $FP">> src/phalanx.h
    vivado -mode tcl -source create_project.tcl -tclargs hoplite_hbm
    cp ./hoplite_hbm.xo ./Vitis/src/
    cd ./Vitis/src/
    cp hoplite_hbm ../Hardware/
    cd ../Hardware/
    ./hw_$FP.sh
    cp binary_container_1.build/link/vivado/vpl/prj/prj.runs/impl_1/pfm_top_wrapper_routed.dcp ./
    vivado -mode tcl pfm_top_wrapper_routed.dcp -source resource.tcl
    lut=`cat hier.log | grep -w "bft_32_xbar_v2_1" | cut -d'|' -f6 | xargs`
    ff=`cat hier.log | grep -w "bft_32_xbar_v2_1" | cut -d'|' -f10 | xargs`

    total_lut=`cat hier.log | grep -w "pfm_top_wrapper " | cut -d'|' -f6 | xargs`
    total_ff=`cat hier.log | grep -w "pfm_top_wrapper " | cut -d'|' -f10 | xargs`
    
    lut_corrected=$(echo "$lut * 0.46"| bc)
    ff_corrected=$(echo "$ff * 0.46"| bc)

    lut_vitis=$(echo "$total_lut-$lut"| bc)
    ff_vitis=$(echo "$total_ff-$ff * 0.46"| bc)


    echo "LUTs Consumed by the NoC: $lut_corrected"
    echo "FFs Consumed by the NoC: $ff_corrected"

    echo "LUTs Consumed by the Vitis: $lut_vitis"
    echo "FFs Consumed by the Vitis: $ff_vitis"

    fmax=`cat binary_container_1.build/logs/v++_binary_container_1.log | grep "Kernel (DATA) clock: clkwiz_kernel_clk_out1 =" | cut -d"," -f2| cut -d"=" -f2 | xargs`
    echo "Maximum Frequency: $fmax"
    
fi
if [[ "$TOPO" == "BFT_SF" || "$TOPO" == "SWEEP" ]]; then
    cd sources/single_flit/bft/
    sed -i '$ d' src/commands.h
    echo "\`define FP $FP">> src/commands.h
    vivado -mode tcl -source create_project.tcl -tclargs bft_32_xbar_v2
    cp ./bft_32_xbar_v2.xo ./Vitis/src/
    cd ./Vitis/src/
    cp bft_32_xbar_v2 ../Hardware/
    cd ../Hardware/
    ./hw_$FP.sh
    cp /binary_container_1.build/link/vivado/vpl/prj/prj.runs/impl_1/pfm_top_wrapper_routed.dcp ./
    vivado -mode tcl pfm_top_wrapper_routed.dcp -source ../resource.tcl
    lut=`cat hier.log | grep -w "bft_32_xbar_v2_1" | cut -d'|' -f6 | xargs`
    ff=`cat hier.log | grep -w "bft_32_xbar_v2_1" | cut -d'|' -f10 | xargs`

    total_lut=`cat hier.log | grep -w "pfm_top_wrapper " | cut -d'|' -f6 | xargs`
    total_ff=`cat hier.log | grep -w "pfm_top_wrapper " | cut -d'|' -f10 | xargs`
    
    lut_corrected=$(echo "$lut * 0.46"| bc)
    ff_corrected=$(echo "$ff * 0.46"| bc)

    lut_vitis=$(echo "$total_lut-$lut"| bc)
    ff_vitis=$(echo "$total_ff-$ff * 0.46"| bc)


    echo "LUTs Consumed by the NoC: $lut_corrected"
    echo "FFs Consumed by the NoC: $ff_corrected"

    echo "LUTs Consumed by the Vitis: $lut_vitis"
    echo "FFs Consumed by the Vitis: $ff_vitis"

    fmax=`cat binary_container_1.build/logs/v++_binary_container_1.log | grep "Kernel (DATA) clock: clkwiz_kernel_clk_out1 =" | cut -d"," -f2| cut -d"=" -f2 | xargs`
    echo "Maximum Frequency: $fmax"

fi
if [[ "$TOPO" == "BFT_MF" || "$TOPO" == "SWEEP" ]]; then
    cd sources/multi_flit/bft/
    sed -i '$ d' src/commands.h
    echo "\`define FP $FP">> src/commands.h
    vivado -mode tcl -source create_project.tcl -tclargs bft_32_xbar_v2
    cp ./bft_32_xbar_v2.xo ./Vitis/src/
    cd ./Vitis/src/
    cd ../Hardware/
    ./hw_$FP.sh

    cp /binary_container_1.build/link/vivado/vpl/prj/prj.runs/impl_1/pfm_top_wrapper_routed.dcp ./
    vivado -mode tcl pfm_top_wrapper_routed.dcp -source resource.tcl
    
    lut=`cat hier.log | grep -w "bft_32_xbar_v2_1" | cut -d'|' -f6 | xargs`
    ff=`cat hier.log | grep -w "bft_32_xbar_v2_1" | cut -d'|' -f10 | xargs`

    total_lut=`cat hier.log | grep -w "pfm_top_wrapper " | cut -d'|' -f6 | xargs`
    total_ff=`cat hier.log | grep -w "pfm_top_wrapper " | cut -d'|' -f10 | xargs`
    
    lut_corrected=$(echo "$lut * 0.46"| bc)
    ff_corrected=$(echo "$ff * 0.46"| bc)

    lut_vitis=$(echo "$total_lut-$lut"| bc)
    ff_vitis=$(echo "$total_ff-$ff * 0.46"| bc)


    echo "LUTs Consumed by the NoC: $lut_corrected"
    echo "FFs Consumed by the NoC: $ff_corrected"

    echo "LUTs Consumed by the Vitis: $lut_vitis"
    echo "FFs Consumed by the Vitis: $ff_vitis"

    fmax=`cat binary_container_1.build/logs/v++_binary_container_1.log | grep "Kernel (DATA) clock: clkwiz_kernel_clk_out1 =" | cut -d"," -f2| cut -d"=" -f2 | xargs`
    echo "Maximum Frequency: $fmax"
fi

