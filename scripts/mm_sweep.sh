#!/bin/bash
export VIVADO=/tools/Xilinx/Vivado/2019.2
export XRT=/opt/xilinx/xrt
export VITIS=/tools/Xilinx/Vitis/2019.2

home='pwd'
FP=0
DEFAULT="SWEEP"
TOPO="${1: -$DEFAULT}"
echo $TOPO
cd sources/dense_matrix_mul/matrix_mul_hls_ip/
vivado_hls -f script.tcl

if [[ "$TOPO" == "noNoC_MF" || "$TOPO" == "SWEEP" ]]; then
    cd ../clients_only/
    vivado -mode tcl -source create_project.tcl -tclargs clients_only
    sh package_xo.sh
    cp ./clients_only.xo ./Vitis/src/
    cd ./Vitis/src/
    g++ -std=c++0x -DVITIS_PLATFORM=xilinx_u280_xdma_201920_1 -D__USE_XOPEN2K8 -I$XRT/include/ -I$VIVADO/include/ -O2 -Wall -c -fmessage-length=0 -o "host_example_wr.o" "host_example_wr.cpp"
    g++ -o "clients_only" host_example_wr.o -lxilinxopencl -lpthread -lrt -lstdc++ -lmpfr -lgmp -lhlsmc++-GCC46 -lIp_floating_point_v7_0_bitacc_cmodel -lIp_xfft_v9_1_bitacc_cmodel -lIp_fir_compiler_v7_2_bitacc_cmodel -lIp_dds_compiler_v6_0_bitacc_cmodel -L$XRT/lib/ -L$VIVADO/lnx64/tools/fpo_v7_0 -L$VIVADO/lnx64/lib/csim -L$VIVADO/lnx64/tools/dds_v6_0 -L$VIVADO/lnx64/tools/fir_v7_0 -L$VIVADO/lnx64/tools/fft_v9_1 -Wl,-rpath-link,$XRT/lib -Wl,-rpath,$VIVADO/lnx64/lib/csim -Wl,-rpath,$VIVADO/lnx64/tools/fpo_v7_0 -Wl,-rpath,$VIVADO/lnx64/tools/fft_v9_1 -Wl,-rpath,$VIVADO/lnx64/tools/fir_v7_0 -Wl,-rpath,$VIVADO/lnx64/tools/dds_v6_0
    cp clients_only ../Hardware/
    cd ../Hardware/
    ./hw.sh
    ./clients_only binary_container_1.xclbin 0xff001
fi

if [[ "$TOPO" == "BFT_MF" || "$TOPO" == "SWEEP" ]]; then
    cd ../bft/
    vivado -mode tcl -source create_project.tcl -tclargs bft_32_xbar_v2
    sh package_xo.sh
    cp ./bft_32_xbar_v2.xo ./Vitis/src/
    cd ./Vitis/src/
    g++ -std=c++0x -DVITIS_PLATFORM=xilinx_u280_xdma_201920_1 -D__USE_XOPEN2K8 -I$XRT/include/ -I$VIVADO/include/ -O2 -Wall -c -fmessage-length=0 -o "host_example_wr.o" "host_example_wr.cpp"
    g++ -o "bft_32_xbar_v2" host_example_wr.o -lxilinxopencl -lpthread -lrt -lstdc++ -lmpfr -lgmp -lhlsmc++-GCC46 -lIp_floating_point_v7_0_bitacc_cmodel -lIp_xfft_v9_1_bitacc_cmodel -lIp_fir_compiler_v7_2_bitacc_cmodel -lIp_dds_compiler_v6_0_bitacc_cmodel -L$XRT/lib/ -L$VIVADO/lnx64/tools/fpo_v7_0 -L$VIVADO/lnx64/lib/csim -L$VIVADO/lnx64/tools/dds_v6_0 -L$VIVADO/lnx64/tools/fir_v7_0 -L$VIVADO/lnx64/tools/fft_v9_1 -Wl,-rpath-link,$XRT/lib -Wl,-rpath,$VIVADO/lnx64/lib/csim -Wl,-rpath,$VIVADO/lnx64/tools/fpo_v7_0 -Wl,-rpath,$VIVADO/lnx64/tools/fft_v9_1 -Wl,-rpath,$VIVADO/lnx64/tools/fir_v7_0 -Wl,-rpath,$VIVADO/lnx64/tools/dds_v6_0
    cp bft_32_xbar_v2 ../Hardware/
    cd ../Hardware/
    ./hw.sh
    ./bft_32_xbar_v2 binary_container_1.xclbin 0xff001
fi

