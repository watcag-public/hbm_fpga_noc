
#!/bin/bash
set -x



rm average_vitis.csv
echo "policy,burst_len,bandwidth">>average_vitis.csv
declare -a policy=("P2P-32" "P2P-8" "P2P-4" "P2P-2" "P2P-1" "CB-1" "CB-4" "CS-1" "CS-16" "BR" "TOR" "NN" "CC")
cmd=("0x1ff01f"  "0x1ff008"  "0x1ff004"  "0x1ff002"  "0x1ff001"  "0x1ff041"  "0x1ff044"  "0x1ff081"  "0x1ff090" "0x1ff0c1" "0x1ff101" "0x1ff141" "0x1ff181")  
i=0
for cmd in ${cmd[@]}
do
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    bw=`awk '{ total += $1 } END { print total/NR }' results_vitis.txt`
    
    echo "${policy[i]},2,$bw" >> average_vitis.csv
    rm results_vitis.txt
    ((i=i+1))
done

i=0
cmd=("0x3ff01f"  "0x3ff008"  "0x3ff004"  "0x3ff002"  "0x3ff001"  "0x3ff041"  "0x3ff044"  "0x3ff081"  "0x3ff090" "0x3ff0c1" "0x3ff101" "0x3ff141" "0x3ff181")  
for cmd in ${cmd[@]}
do
    
     ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    bw=`awk '{ total += $1 } END { print total/NR }' results_vitis.txt`
    
    echo "${policy[i]},4,$bw" >> average_vitis.csv
    rm results_vitis.txt
    ((i=i+1))
done

i=0
cmd=("0x7ff01f"  "0x7ff008"  "0x7ff004"  "0x7ff002"  "0x7ff001"  "0x7ff041"  "0x7ff044"  "0x7ff081"  "0x7ff090" "0x7ff0c1" "0x7ff101" "0x7ff141" "0x7ff181")  
for cmd in ${cmd[@]}
do
    
     ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    bw=`awk '{ total += $1 } END { print total/NR }' results_vitis.txt`
    
    echo "${policy[i]},8,$bw" >> average_vitis.csv
    rm results_vitis.txt
    ((i=i+1))
done

i=0
cmd=("0xfff01f"  "0xfff008"  "0xfff004"  "0xfff002"  "0xfff001"  "0xfff041"  "0xfff044"  "0xfff081"  "0xfff090" "0xfff0c1" "0xfff101" "0xfff141" "0xfff181")  
for cmd in ${cmd[@]}
do
    
    
     ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./bft_32_xbar_v2 binary_container_1.xclbin $cmd
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    bw=`awk '{ total += $1 } END { print total/NR }' results_vitis.txt`
    
    echo "${policy[i]},16,$bw" >> average_vitis.csv
    rm results_vitis.txt
    ((i=i+1))
done
