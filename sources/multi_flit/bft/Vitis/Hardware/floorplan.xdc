
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *inst_control_s_axi*}]



add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].readFifo*}] 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].readFifo2*}] 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].readRespFifo*}] 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].readRespFifo2*}] 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].writeDataFifo*}] 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].writeDataFifo2*}] 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].writeFifo*}] 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *genblk8[*].writeFifo2*}] 



#level4
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[4].ms[*].ns[*]*}]

#level3

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[*].ns[*]*}]

#SLR0 
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[0].ns[*]*}]
create_pblock pi01_level1
add_cells_to_pblock [get_pblocks pi01_level1] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[0].*}]
resize_pblock pi01_level1 -add CLOCKREGION_X0Y0:CLOCKREGION_X3Y3

create_pblock pi23_level1
add_cells_to_pblock [get_pblocks pi23_level1] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[1].*}]
resize_pblock pi23_level1 -add CLOCKREGION_X4Y0:CLOCKREGION_X6Y3


create_pblock pi0_level
add_cells_to_pblock [get_pblocks pi0_level] [get_cells -hier -filter {NAME =~ */xs[0]*}]
resize_pblock pi0_level -add CLOCKREGION_X0Y0:CLOCKREGION_X3Y1

create_pblock pi1_level
add_cells_to_pblock [get_pblocks pi1_level] [get_cells -hier -filter {NAME =~ */xs[1]*}]
resize_pblock pi1_level -add CLOCKREGION_X0Y2:CLOCKREGION_X3Y3

create_pblock pi2_level
add_cells_to_pblock [get_pblocks pi2_level] [get_cells -hier -filter {NAME =~ */xs[2]*}]
resize_pblock pi2_level -add CLOCKREGION_X4Y0:CLOCKREGION_X6Y1

create_pblock pi3_level
add_cells_to_pblock [get_pblocks pi3_level] [get_cells -hier -filter {NAME =~ */xs[3]*}]
resize_pblock pi3_level -add CLOCKREGION_X4Y2:CLOCKREGION_X6Y3




#SLR1
create_pblock pi45_level1
add_cells_to_pblock [get_pblocks pi45_level1] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[2].*}]
add_cells_to_pblock [get_pblocks pi45_level1] [get_cells -hier -filter {NAME =~ */n2.ls[2].ms[1].*}]
resize_pblock pi45_level1 -add CLOCKREGION_X4Y4:CLOCKREGION_X6Y7
add_cells_to_pblock [get_pblocks pi45_level1] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[3].*}]

create_pblock pi4_level0
add_cells_to_pblock [get_pblocks pi4_level0] [get_cells -hier -filter {NAME =~ */xs[4]*}]
resize_pblock pi4_level0 -add CLOCKREGION_X4Y4:CLOCKREGION_X6Y5

create_pblock pi5_level0
add_cells_to_pblock [get_pblocks pi5_level0] [get_cells -hier -filter {NAME =~ */xs[5]*}]
resize_pblock pi5_level0 -add CLOCKREGION_X4Y6:CLOCKREGION_X6Y7

add_cells_to_pblock [get_pblocks pi45_level1] [get_cells -hier -filter {NAME =~ */xs[6]*}]
add_cells_to_pblock [get_pblocks pi45_level1] [get_cells -hier -filter {NAME =~ */xs[7]*}]

create_pblock pi891011_level2
add_cells_to_pblock [get_pblocks pi891011_level2] [get_cells -hier -filter {NAME =~ */n2.ls[2].ms[2].*}]
resize_pblock pi891011_level2 -add CLOCKREGION_X0Y4:CLOCKREGION_X3Y7

create_pblock pi89_level1
add_cells_to_pblock [get_pblocks pi89_level1] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[4].*}]
resize_pblock pi89_level1 -add CLOCKREGION_X0Y4:CLOCKREGION_X1Y7

create_pblock pi1011_level1
add_cells_to_pblock [get_pblocks pi1011_level1] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[5].*}]
resize_pblock pi1011_level1 -add CLOCKREGION_X2Y4:CLOCKREGION_X3Y7

create_pblock pi8_level0
add_cells_to_pblock [get_pblocks pi8_level0] [get_cells -hier -filter {NAME =~ */xs[8]*}]
resize_pblock pi8_level0 -add CLOCKREGION_X0Y4:CLOCKREGION_X1Y5

create_pblock pi9_level0
add_cells_to_pblock [get_pblocks pi9_level0] [get_cells -hier -filter {NAME =~ */xs[9]*}]
resize_pblock pi9_level0 -add CLOCKREGION_X0Y6:CLOCKREGION_X1Y7

create_pblock pi10_level0
add_cells_to_pblock [get_pblocks pi10_level0] [get_cells -hier -filter {NAME =~ */xs[10]*}]
resize_pblock pi10_level0 -add CLOCKREGION_X2Y4:CLOCKREGION_X3Y5

create_pblock pi11_level0
add_cells_to_pblock [get_pblocks pi11_level0] [get_cells -hier -filter {NAME =~ */xs[11]*}]
resize_pblock pi11_level0 -add CLOCKREGION_X2Y6:CLOCKREGION_X3Y7


#SLR2
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[6].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ */n2.ls[1].ms[7].*}]
create_pblock pi12_level0
add_cells_to_pblock [get_pblocks pi12_level0] [get_cells -hier -filter {NAME =~ */xs[12]*}]
resize_pblock pi12_level0 -add CLOCKREGION_X0Y8:CLOCKREGION_X3Y11

create_pblock pi13_level0
add_cells_to_pblock [get_pblocks pi13_level0] [get_cells -hier -filter {NAME =~ */xs[13]*}]
resize_pblock pi13_level0 -add CLOCKREGION_X4Y8:CLOCKREGION_X6Y11

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ */xs[14]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ */xs[15]*}]

#level 2 to level 1
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U0/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U0/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U0/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U1/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U1/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U1/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U0/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/s_axis_u0_wready_INST_0*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/genblk1.bp_U1/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[6].ns[*].pi_level.sb/s_axis_u1_wready_INST_0*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U0/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U0/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U0/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U1/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U1/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U1/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U0/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/s_axis_u0_wready_INST_0*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/genblk1.bp_U1/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[1].ms[7].ns[*].pi_level.sb/s_axis_u1_wready_INST_0*}]


#level 2 to level 3

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_R/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_R/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_R/hr_d_reg[0]*}]



add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_R/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/s_axis_r_wready_INST_0*}]


add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_L/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_L/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_L/hr_d_reg[0]*}]



add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_L/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/s_axis_l_wready_INST_0*}]


#level 3 to level 2

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U0/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U0/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U0/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U1/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U1/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U1/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U0/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/s_axis_u0_wready_INST*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/genblk1.bp_U1/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[3].ns[*].pi_level.sb/s_axis_u1_wready_INST*}]



add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U0/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U0/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U0/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U1/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U1/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U1/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U0/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/s_axis_u0_wready_INST*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/genblk1.bp_U1/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[2].ns[*].pi_level.sb/s_axis_u1_wready_INST*}]




add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U0/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U0/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U0/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U1/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U1/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U1/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U0/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/s_axis_u0_wready_INST*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/genblk1.bp_U1/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *n2.ls[2].ms[1].ns[*].pi_level.sb/s_axis_u1_wready_INST*}]

#level 2 to level 3


add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[*].ns[*].pi_level.sb/genblk1.bp_R/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[*].ns[*].pi_level.sb/genblk1.bp_R/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[*].ns[*].pi_level.sb/genblk1.bp_R/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[*].ns[*].pi_level.sb/genblk1.bp_R/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[*].ns[*].pi_level.sb/s_axis_r_wready_INST_0*}]


add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[1].ns[*].pi_level.sb/genblk1.bp_L/hr_b_reg[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[1].ns[*].pi_level.sb/genblk1.bp_L/hr_v_reg[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[1].ns[*].pi_level.sb/genblk1.bp_L/hr_d_reg[0]*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[1].ns[*].pi_level.sb/genblk1.bp_L/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *n2.ls[3].ms[1].ns[*].pi_level.sb/s_axis_l_wready_INST_0*}]
