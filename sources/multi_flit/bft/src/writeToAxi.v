`timescale 1ps / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 1
// Create Date: 10/31/2019 09:35:29 PM
// Design Name: 
// Module Name: writeToHBM_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "commands.h"


 
module writeToAxi
#(parameter C_M00_AXI_ADDR_WIDTH = 64,
	parameter C_M00_AXI_DATA_WIDTH = 256,
		parameter C_M08_AXI_ADDR_WIDTH = 64,
	parameter C_M08_AXI_DATA_WIDTH = 256,
	parameter C_M09_AXI_ADDR_WIDTH = 64,
	parameter C_M09_AXI_DATA_WIDTH = 256,
	parameter C_M10_AXI_ADDR_WIDTH = 64,
	parameter C_M10_AXI_DATA_WIDTH = 256,
	parameter C_M11_AXI_ADDR_WIDTH = 64,
	parameter C_M11_AXI_DATA_WIDTH = 256,
	parameter C_M12_AXI_ADDR_WIDTH = 64,
	parameter C_M12_AXI_DATA_WIDTH = 256,
	parameter C_M13_AXI_ADDR_WIDTH = 64,
	parameter C_M13_AXI_DATA_WIDTH = 256,
	parameter C_M14_AXI_ADDR_WIDTH = 64,
	parameter C_M14_AXI_DATA_WIDTH = 256,
	parameter C_M15_AXI_ADDR_WIDTH = 64,
	parameter C_M15_AXI_DATA_WIDTH = 256,
	parameter  C_M16_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M16_AXI_DATA_WIDTH       = 256,      
    parameter C_M17_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M17_AXI_DATA_WIDTH       = 256,      
    parameter C_M18_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M18_AXI_DATA_WIDTH       = 256,      
    parameter C_M19_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M19_AXI_DATA_WIDTH       = 256,      
    parameter C_M20_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M20_AXI_DATA_WIDTH       = 256,      
    parameter C_M21_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M21_AXI_DATA_WIDTH       = 256,      
    parameter C_M22_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M22_AXI_DATA_WIDTH       = 256,      
    parameter C_M23_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M23_AXI_DATA_WIDTH       = 256,      
    parameter C_M24_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M24_AXI_DATA_WIDTH       = 256,      
    parameter C_M25_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M25_AXI_DATA_WIDTH       = 256,      
    parameter C_M26_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M26_AXI_DATA_WIDTH       = 256,      
    parameter C_M27_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M27_AXI_DATA_WIDTH       = 256,      
    parameter C_M28_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M28_AXI_DATA_WIDTH       = 256,      
    parameter C_M29_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M29_AXI_DATA_WIDTH       = 256,      
    parameter C_M30_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M30_AXI_DATA_WIDTH       = 256,      
    parameter C_M31_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M31_AXI_DATA_WIDTH       = 256       
	)
	 (

//                    BVALID        ,
                    HBM_REF_CLK_0 ,
                    AXI_ACLK      ,
                    AXI_ARESET_N  ,
                    APB_PCLK      ,
                    APB_PRESET_N  ,
                    A,
                
                   
                    AXI_00_AWVALID,
                    AXI_00_AWADDR,
                    AXI_00_AWREADY,
                    AXI_00_AWLEN,
                    AXI_00_WDATA,
                    AXI_00_WVALID ,
                    AXI_00_WREADY,
                    AXI_00_WLAST,
                    AXI_00_ARREADY,
                    AXI_00_ARADDR,
                    AXI_00_ARVALID,
                    AXI_00_ARLEN,
                    AXI_00_RDATA,
                    AXI_00_RLAST,
                    AXI_00_RREADY,
                    AXI_00_RVALID
                    
                    `ifdef XBARorM1orM0
                   ,AXI_01_AWVALID,
                    AXI_01_AWADDR,
                    AXI_01_AWREADY,
                    AXI_01_AWLEN,
                    AXI_01_WDATA,
                    AXI_01_WVALID ,
                    AXI_01_WREADY,
                    AXI_01_WLAST,
                    AXI_01_ARREADY,
                    AXI_01_ARADDR,
                    AXI_01_ARVALID,
                    AXI_01_ARLEN,
                    AXI_01_RDATA,
                    AXI_01_RLAST,
                    AXI_01_RREADY,
                    AXI_01_RVALID,
                    
                    AXI_02_AWVALID,
                    AXI_02_AWADDR,
                    AXI_02_AWREADY,
                    AXI_02_AWLEN,
                    AXI_02_WDATA,
                    AXI_02_WVALID ,
                    AXI_02_WREADY,
                    AXI_02_WLAST,
                    AXI_02_ARREADY,
                    AXI_02_ARADDR,
                    AXI_02_ARVALID,
                    AXI_02_ARLEN,
                    AXI_02_RDATA,
                    AXI_02_RLAST,
                    AXI_02_RREADY,
                    AXI_02_RVALID,
                    
                    AXI_03_AWVALID,
                    AXI_03_AWADDR,
                    AXI_03_AWREADY,
                    AXI_03_AWLEN,
                    AXI_03_WDATA,
                    AXI_03_WVALID ,
                    AXI_03_WREADY,
                    AXI_03_WLAST,
                    AXI_03_ARREADY,
                    AXI_03_ARADDR,
                    AXI_03_ARVALID,
                    AXI_03_ARLEN,
                    AXI_03_RDATA,
                    AXI_03_RLAST,
                    AXI_03_RREADY,
                    AXI_03_RVALID
                    
                    `endif
                    `ifdef XBARorM1
                   ,AXI_04_AWVALID,
                    AXI_04_AWADDR,
                    AXI_04_AWREADY,
                    AXI_04_AWLEN,
                    AXI_04_WDATA,
                    AXI_04_WVALID ,
                    AXI_04_WREADY,
                    AXI_04_WLAST,
                    AXI_04_ARREADY,
                    AXI_04_ARADDR,
                    AXI_04_ARVALID,
                    AXI_04_ARLEN,
                    AXI_04_RDATA,
                    AXI_04_RLAST,
                    AXI_04_RREADY,
                    AXI_04_RVALID,
                    
                    AXI_05_AWVALID,
                    AXI_05_AWADDR,
                    AXI_05_AWREADY,
                    AXI_05_AWLEN,
                    AXI_05_WDATA,
                    AXI_05_WVALID ,
                    AXI_05_WREADY,
                    AXI_05_WLAST,
                    AXI_05_ARREADY,
                    AXI_05_ARADDR,
                    AXI_05_ARVALID,
                    AXI_05_ARLEN,
                    AXI_05_RDATA,
                    AXI_05_RLAST,
                    AXI_05_RREADY,
                    AXI_05_RVALID,
                    
                    AXI_06_AWVALID,
                    AXI_06_AWADDR,
                    AXI_06_AWREADY,
                    AXI_06_AWLEN,
                    AXI_06_WDATA,
                    AXI_06_WVALID ,
                    AXI_06_WREADY,
                    AXI_06_WLAST,
                    AXI_06_ARREADY,
                    AXI_06_ARADDR,
                    AXI_06_ARVALID,
                    AXI_06_ARLEN,
                    AXI_06_RDATA,
                    AXI_06_RLAST,
                    AXI_06_RREADY,
                    AXI_06_RVALID,
                    
                    AXI_07_AWVALID,
                    AXI_07_AWADDR,
                    AXI_07_AWREADY,
                    AXI_07_AWLEN,
                    AXI_07_WDATA,
                    AXI_07_WVALID ,
                    AXI_07_WREADY,
                    AXI_07_WLAST,
                    AXI_07_ARREADY,
                    AXI_07_ARADDR,
                    AXI_07_ARVALID,
                    AXI_07_ARLEN,
                    AXI_07_RDATA,
                    AXI_07_RLAST,
                    AXI_07_RREADY,
                    AXI_07_RVALID
                    `endif
                    `ifdef XBARorM0
                   ,AXI_08_AWVALID,
                    AXI_08_AWADDR,
                    AXI_08_AWREADY,
                    AXI_08_AWLEN,
                    AXI_08_WDATA,
                    AXI_08_WVALID ,
                    AXI_08_WREADY,
                    AXI_08_WLAST,
                    AXI_08_ARREADY,
                    AXI_08_ARADDR,
                    AXI_08_ARVALID,
                    AXI_08_ARLEN,
                    AXI_08_RDATA,
                    AXI_08_RLAST,
                    AXI_08_RREADY,
                    AXI_08_RVALID,
                    
                    AXI_09_AWVALID,
                    AXI_09_AWADDR,
                    AXI_09_AWREADY,
                    AXI_09_AWLEN,
                    AXI_09_WDATA,
                    AXI_09_WVALID ,
                    AXI_09_WREADY,
                    AXI_09_WLAST,
                    AXI_09_ARREADY,
                    AXI_09_ARADDR,
                    AXI_09_ARVALID,
                    AXI_09_ARLEN,
                    AXI_09_RDATA,
                    AXI_09_RLAST,
                    AXI_09_RREADY,
                    AXI_09_RVALID,
                    
                    AXI_10_AWVALID,
                    AXI_10_AWADDR,
                    AXI_10_AWREADY,
                    AXI_10_AWLEN,
                    AXI_10_WDATA,
                    AXI_10_WVALID ,
                    AXI_10_WREADY,
                    AXI_10_WLAST,
                    AXI_10_ARREADY,
                    AXI_10_ARADDR,
                    AXI_10_ARVALID,
                    AXI_10_ARLEN,
                    AXI_10_RDATA,
                    AXI_10_RLAST,
                    AXI_10_RREADY,
                    AXI_10_RVALID,
                    
                    AXI_11_AWVALID,
                    AXI_11_AWADDR,
                    AXI_11_AWREADY,
                    AXI_11_AWLEN,
                    AXI_11_WDATA,
                    AXI_11_WVALID ,
                    AXI_11_WREADY,
                    AXI_11_WLAST,
                    AXI_11_ARREADY,
                    AXI_11_ARADDR,
                    AXI_11_ARVALID,
                    AXI_11_ARLEN,
                    AXI_11_RDATA,
                    AXI_11_RLAST,
                    AXI_11_RREADY,
                    AXI_11_RVALID
                    `endif
                    `ifdef XBAR
                   ,AXI_12_AWVALID,
                    AXI_12_AWADDR,
                    AXI_12_AWREADY,
                    AXI_12_AWLEN,
                    AXI_12_WDATA,
                    AXI_12_WVALID ,
                    AXI_12_WREADY,
                    AXI_12_WLAST,
                    AXI_12_ARREADY,
                    AXI_12_ARADDR,
                    AXI_12_ARVALID,
                    AXI_12_ARLEN,
                    AXI_12_RDATA,
                    AXI_12_RLAST,
                    AXI_12_RREADY,
                    AXI_12_RVALID,
                    
                    AXI_13_AWVALID,
                    AXI_13_AWADDR,
                    AXI_13_AWREADY,
                    AXI_13_AWLEN,
                    AXI_13_WDATA,
                    AXI_13_WVALID ,
                    AXI_13_WREADY,
                    AXI_13_WLAST,
                    AXI_13_ARREADY,
                    AXI_13_ARADDR,
                    AXI_13_ARVALID,
                    AXI_13_ARLEN,
                    AXI_13_RDATA,
                    AXI_13_RLAST,
                    AXI_13_RREADY,
                    AXI_13_RVALID,
                    
                    AXI_14_AWVALID,
                    AXI_14_AWADDR,
                    AXI_14_AWREADY,
                    AXI_14_AWLEN,
                    AXI_14_WDATA,
                    AXI_14_WVALID ,
                    AXI_14_WREADY,
                    AXI_14_WLAST,
                    AXI_14_ARREADY,
                    AXI_14_ARADDR,
                    AXI_14_ARVALID,
                    AXI_14_ARLEN,
                    AXI_14_RDATA,
                    AXI_14_RLAST,
                    AXI_14_RREADY,
                    AXI_14_RVALID,
                    
                    AXI_15_AWVALID,
                    AXI_15_AWADDR,
                    AXI_15_AWREADY,
                    AXI_15_AWLEN,
                    AXI_15_WDATA,
                    AXI_15_WVALID ,
                    AXI_15_WREADY,
                    AXI_15_WLAST,
                    AXI_15_ARREADY,
                    AXI_15_ARADDR,
                    AXI_15_ARVALID,
                    AXI_15_ARLEN,
                    AXI_15_RDATA,
                    AXI_15_RLAST,
                    AXI_15_RREADY,
                    AXI_15_RVALID,
                    
                    AXI_16_AWVALID,
                    AXI_16_AWADDR,
                    AXI_16_AWREADY,
                    AXI_16_AWLEN,
                    AXI_16_WDATA,
                    AXI_16_WVALID ,
                    AXI_16_WREADY,
                    AXI_16_WLAST,
                    AXI_16_ARREADY,
                    AXI_16_ARADDR,
                    AXI_16_ARVALID,
                    AXI_16_ARLEN,
                    AXI_16_RDATA,
                    AXI_16_RLAST,
                    AXI_16_RREADY,
                    AXI_16_RVALID,
                    
                    AXI_17_AWVALID,
                    AXI_17_AWADDR,
                    AXI_17_AWREADY,
                    AXI_17_AWLEN,
                    AXI_17_WDATA,
                    AXI_17_WVALID ,
                    AXI_17_WREADY,
                    AXI_17_WLAST,
                    AXI_17_ARREADY,
                    AXI_17_ARADDR,
                    AXI_17_ARVALID,
                    AXI_17_ARLEN,
                    AXI_17_RDATA,
                    AXI_17_RLAST,
                    AXI_17_RREADY,
                    AXI_17_RVALID,
                    
                    AXI_18_AWVALID,
                    AXI_18_AWADDR,
                    AXI_18_AWREADY,
                    AXI_18_AWLEN,
                    AXI_18_WDATA,
                    AXI_18_WVALID ,
                    AXI_18_WREADY,
                    AXI_18_WLAST,
                    AXI_18_ARREADY,
                    AXI_18_ARADDR,
                    AXI_18_ARVALID,
                    AXI_18_ARLEN,
                    AXI_18_RDATA,
                    AXI_18_RLAST,
                    AXI_18_RREADY,
                    AXI_18_RVALID,
                    
                    AXI_19_AWVALID,
                    AXI_19_AWADDR,
                    AXI_19_AWREADY,
                    AXI_19_AWLEN,
                    AXI_19_WDATA,
                    AXI_19_WVALID ,
                    AXI_19_WREADY,
                    AXI_19_WLAST,
                    AXI_19_ARREADY,
                    AXI_19_ARADDR,
                    AXI_19_ARVALID,
                    AXI_19_ARLEN,
                    AXI_19_RDATA,
                    AXI_19_RLAST,
                    AXI_19_RREADY,
                    AXI_19_RVALID
                   
                   ,AXI_20_AWVALID,
                    AXI_20_AWADDR,
                    AXI_20_AWREADY,
                    AXI_20_AWLEN,
                    AXI_20_WDATA,
                    AXI_20_WVALID ,
                    AXI_20_WREADY,
                    AXI_20_WLAST,
                    AXI_20_ARREADY,
                    AXI_20_ARADDR,
                    AXI_20_ARVALID,
                    AXI_20_ARLEN,
                    AXI_20_RDATA,
                    AXI_20_RLAST,
                    AXI_20_RREADY,
                    AXI_20_RVALID,
                    
                    AXI_21_AWVALID,
                    AXI_21_AWADDR,
                    AXI_21_AWREADY,
                    AXI_21_AWLEN,
                    AXI_21_WDATA,
                    AXI_21_WVALID ,
                    AXI_21_WREADY,
                    AXI_21_WLAST,
                    AXI_21_ARREADY,
                    AXI_21_ARADDR,
                    AXI_21_ARVALID,
                    AXI_21_ARLEN,
                    AXI_21_RDATA,
                    AXI_21_RLAST,
                    AXI_21_RREADY,
                    AXI_21_RVALID,
                    
                    AXI_22_AWVALID,
                    AXI_22_AWADDR,
                    AXI_22_AWREADY,
                    AXI_22_AWLEN,
                    AXI_22_WDATA,
                    AXI_22_WVALID ,
                    AXI_22_WREADY,
                    AXI_22_WLAST,
                    AXI_22_ARREADY,
                    AXI_22_ARADDR,
                    AXI_22_ARVALID,
                    AXI_22_ARLEN,
                    AXI_22_RDATA,
                    AXI_22_RLAST,
                    AXI_22_RREADY,
                    AXI_22_RVALID,
                    
                    AXI_23_AWVALID,
                    AXI_23_AWADDR,
                    AXI_23_AWREADY,
                    AXI_23_AWLEN,
                    AXI_23_WDATA,
                    AXI_23_WVALID ,
                    AXI_23_WREADY,
                    AXI_23_WLAST,
                    AXI_23_ARREADY,
                    AXI_23_ARADDR,
                    AXI_23_ARVALID,
                    AXI_23_ARLEN,
                    AXI_23_RDATA,
                    AXI_23_RLAST,
                    AXI_23_RREADY,
                    AXI_23_RVALID,
                    
                    AXI_24_AWVALID,
                    AXI_24_AWADDR,
                    AXI_24_AWREADY,
                    AXI_24_AWLEN,
                    AXI_24_WDATA,
                    AXI_24_WVALID ,
                    AXI_24_WREADY,
                    AXI_24_WLAST,
                    AXI_24_ARREADY,
                    AXI_24_ARADDR,
                    AXI_24_ARVALID,
                    AXI_24_ARLEN,
                    AXI_24_RDATA,
                    AXI_24_RLAST,
                    AXI_24_RREADY,
                    AXI_24_RVALID,
                    
                    AXI_25_AWVALID,
                    AXI_25_AWADDR,
                    AXI_25_AWREADY,
                    AXI_25_AWLEN,
                    AXI_25_WDATA,
                    AXI_25_WVALID ,
                    AXI_25_WREADY,
                    AXI_25_WLAST,
                    AXI_25_ARREADY,
                    AXI_25_ARADDR,
                    AXI_25_ARVALID,
                    AXI_25_ARLEN,
                    AXI_25_RDATA,
                    AXI_25_RLAST,
                    AXI_25_RREADY,
                    AXI_25_RVALID,
                    
                    AXI_26_AWVALID,
                    AXI_26_AWADDR,
                    AXI_26_AWREADY,
                    AXI_26_AWLEN,
                    AXI_26_WDATA,
                    AXI_26_WVALID ,
                    AXI_26_WREADY,
                    AXI_26_WLAST,
                    AXI_26_ARREADY,
                    AXI_26_ARADDR,
                    AXI_26_ARVALID,
                    AXI_26_ARLEN,
                    AXI_26_RDATA,
                    AXI_26_RLAST,
                    AXI_26_RREADY,
                    AXI_26_RVALID,
                    
                    AXI_27_AWVALID,
                    AXI_27_AWADDR,
                    AXI_27_AWREADY,
                    AXI_27_AWLEN,
                    AXI_27_WDATA,
                    AXI_27_WVALID ,
                    AXI_27_WREADY,
                    AXI_27_WLAST,
                    AXI_27_ARREADY,
                    AXI_27_ARADDR,
                    AXI_27_ARVALID,
                    AXI_27_ARLEN,
                    AXI_27_RDATA,
                    AXI_27_RLAST,
                    AXI_27_RREADY,
                    AXI_27_RVALID,
                    
                    AXI_28_AWVALID,
                    AXI_28_AWADDR,
                    AXI_28_AWREADY,
                    AXI_28_AWLEN,
                    AXI_28_WDATA,
                    AXI_28_WVALID ,
                    AXI_28_WREADY,
                    AXI_28_WLAST,
                    AXI_28_ARREADY,
                    AXI_28_ARADDR,
                    AXI_28_ARVALID,
                    AXI_28_ARLEN,
                    AXI_28_RDATA,
                    AXI_28_RLAST,
                    AXI_28_RREADY,
                    AXI_28_RVALID,
                   
                    AXI_29_AWVALID,
                    AXI_29_AWADDR,
                    AXI_29_AWREADY,
                    AXI_29_AWLEN,
                    AXI_29_WDATA,
                    AXI_29_WVALID ,
                    AXI_29_WREADY,
                    AXI_29_WLAST,
                    AXI_29_ARREADY,
                    AXI_29_ARADDR,
                    AXI_29_ARVALID,
                    AXI_29_ARLEN,
                    AXI_29_RDATA,
                    AXI_29_RLAST,
                    AXI_29_RREADY,
                    AXI_29_RVALID,
                    
                    AXI_30_AWVALID,
                    AXI_30_AWADDR,
                    AXI_30_AWREADY,
                    AXI_30_AWLEN,
                    AXI_30_WDATA,
                    AXI_30_WVALID ,
                    AXI_30_WREADY,
                    AXI_30_WLAST,
                    AXI_30_ARREADY,
                    AXI_30_ARADDR,
                    AXI_30_ARVALID,
                    AXI_30_ARLEN,
                    AXI_30_RDATA,
                    AXI_30_RLAST,
                    AXI_30_RREADY,
                    AXI_30_RVALID,
                    
                    AXI_31_AWVALID,
                    AXI_31_AWADDR,
                    AXI_31_AWREADY,
                    AXI_31_AWLEN,
                    AXI_31_WDATA,
                    AXI_31_WVALID ,
                    AXI_31_WREADY,
                    AXI_31_WLAST,
                    AXI_31_ARREADY,
                    AXI_31_ARADDR,
                    AXI_31_ARVALID,
                    AXI_31_ARLEN,
                    AXI_31_RDATA,
                    AXI_31_RLAST,
                    AXI_31_RREADY,
                    AXI_31_RVALID
                    `endif
                    
                    ,m00_axi_awvalid    
                    ,m00_axi_awready    
                    ,m00_axi_awaddr     
                    ,m00_axi_awlen      
                    ,m00_axi_wvalid     
                    ,m00_axi_wready     
                    ,m00_axi_wdata      
                    ,m00_axi_wstrb      
                    ,m00_axi_wlast      
                    ,m00_axi_bvalid     
                    ,m00_axi_bready     
                    ,m00_axi_arvalid    
                    ,m00_axi_arready    
                    ,m00_axi_araddr     
                    ,m00_axi_arlen      
                    ,m00_axi_rvalid     
                    ,m00_axi_rready     
                    ,m00_axi_rdata      
                    ,m00_axi_rlast    
                    `ifdef XBARorM1orM0   
                    ,m01_axi_awvalid    
                    ,m01_axi_awready    
                    ,m01_axi_awaddr     
                    ,m01_axi_awlen      
                    ,m01_axi_wvalid     
                    ,m01_axi_wready     
                    ,m01_axi_wdata      
                    ,m01_axi_wstrb      
                    ,m01_axi_wlast      
                    ,m01_axi_bvalid     
                    ,m01_axi_bready     
                    ,m01_axi_arvalid    
                    ,m01_axi_arready    
                    ,m01_axi_araddr     
                    ,m01_axi_arlen      
                    ,m01_axi_rvalid     
                    ,m01_axi_rready     
                    ,m01_axi_rdata      
                    ,m01_axi_rlast  
                             
                    ,m02_axi_awvalid    
                    ,m02_axi_awready    
                    ,m02_axi_awaddr     
                    ,m02_axi_awlen      
                    ,m02_axi_wvalid     
                    ,m02_axi_wready     
                    ,m02_axi_wdata      
                    ,m02_axi_wstrb      
                    ,m02_axi_wlast      
                    ,m02_axi_bvalid     
                    ,m02_axi_bready     
                    ,m02_axi_arvalid    
                    ,m02_axi_arready    
                    ,m02_axi_araddr     
                    ,m02_axi_arlen      
                    ,m02_axi_rvalid     
                    ,m02_axi_rready     
                    ,m02_axi_rdata      
                    ,m02_axi_rlast  
                             
                    ,m03_axi_awvalid    
                    ,m03_axi_awready    
                    ,m03_axi_awaddr     
                    ,m03_axi_awlen      
                    ,m03_axi_wvalid     
                    ,m03_axi_wready     
                    ,m03_axi_wdata      
                    ,m03_axi_wstrb      
                    ,m03_axi_wlast      
                    ,m03_axi_bvalid     
                    ,m03_axi_bready     
                    ,m03_axi_arvalid    
                    ,m03_axi_arready    
                    ,m03_axi_araddr     
                    ,m03_axi_arlen      
                    ,m03_axi_rvalid     
                    ,m03_axi_rready     
                    ,m03_axi_rdata      
                    ,m03_axi_rlast  
                    `endif
                    `ifdef XBARorM1
                    ,m04_axi_awvalid   
                    ,m04_axi_awready   
                    ,m04_axi_awaddr    
                    ,m04_axi_awlen     
                    ,m04_axi_wvalid    
                    ,m04_axi_wready    
                    ,m04_axi_wdata     
                    ,m04_axi_wstrb     
                    ,m04_axi_wlast     
                    ,m04_axi_bvalid    
                    ,m04_axi_bready    
                    ,m04_axi_arvalid   
                    ,m04_axi_arready   
                    ,m04_axi_araddr    
                    ,m04_axi_arlen     
                    ,m04_axi_rvalid    
                    ,m04_axi_rready    
                    ,m04_axi_rdata     
                    ,m04_axi_rlast   
                    
                    ,m05_axi_awvalid 
                    ,m05_axi_awready 
                    ,m05_axi_awaddr  
                    ,m05_axi_awlen   
                    ,m05_axi_wvalid  
                    ,m05_axi_wready  
                    ,m05_axi_wdata   
                    ,m05_axi_wstrb   
                    ,m05_axi_wlast   
                    ,m05_axi_bvalid  
                    ,m05_axi_bready  
                    ,m05_axi_arvalid 
                    ,m05_axi_arready 
                    ,m05_axi_araddr  
                    ,m05_axi_arlen   
                    ,m05_axi_rvalid  
                    ,m05_axi_rready  
                    ,m05_axi_rdata   
                    ,m05_axi_rlast   
                        
                    ,m06_axi_awvalid 
                    ,m06_axi_awready 
                    ,m06_axi_awaddr  
                    ,m06_axi_awlen   
                    ,m06_axi_wvalid  
                    ,m06_axi_wready  
                    ,m06_axi_wdata   
                    ,m06_axi_wstrb   
                    ,m06_axi_wlast   
                    ,m06_axi_bvalid  
                    ,m06_axi_bready  
                    ,m06_axi_arvalid 
                    ,m06_axi_arready 
                    ,m06_axi_araddr  
                    ,m06_axi_arlen   
                    ,m06_axi_rvalid  
                    ,m06_axi_rready  
                    ,m06_axi_rdata   
                    ,m06_axi_rlast   
                        
                    ,m07_axi_awvalid 
                    ,m07_axi_awready 
                    ,m07_axi_awaddr  
                    ,m07_axi_awlen   
                    ,m07_axi_wvalid  
                    ,m07_axi_wready  
                    ,m07_axi_wdata   
                    ,m07_axi_wstrb   
                    ,m07_axi_wlast   
                    ,m07_axi_bvalid  
                    ,m07_axi_bready  
                    ,m07_axi_arvalid 
                    ,m07_axi_arready 
                    ,m07_axi_araddr  
                    ,m07_axi_arlen   
                    ,m07_axi_rvalid  
                    ,m07_axi_rready  
                    ,m07_axi_rdata   
                    ,m07_axi_rlast   
    
                    `endif
                    `ifdef XBARorM0 
                    ,m08_axi_awvalid
                    ,m08_axi_awready
                    ,m08_axi_awaddr 
                    ,m08_axi_awlen  
                    ,m08_axi_wvalid 
                    ,m08_axi_wready 
                    ,m08_axi_wdata  
                    ,m08_axi_wstrb  
                    ,m08_axi_wlast  
                    ,m08_axi_bvalid 
                    ,m08_axi_bready 
                    ,m08_axi_arvalid
                    ,m08_axi_arready
                    ,m08_axi_araddr 
                    ,m08_axi_arlen  
                    ,m08_axi_rvalid 
                    ,m08_axi_rready 
                    ,m08_axi_rdata  
                    ,m08_axi_rlast  
                     
                 
               
                    ,m09_axi_awvalid
                    ,m09_axi_awready
                    ,m09_axi_awaddr 
                    ,m09_axi_awlen  
                    ,m09_axi_wvalid 
                    ,m09_axi_wready 
                    ,m09_axi_wdata  
                    ,m09_axi_wstrb  
                    ,m09_axi_wlast  
                    ,m09_axi_bvalid 
                    ,m09_axi_bready 
                    ,m09_axi_arvalid
                    ,m09_axi_arready
                    ,m09_axi_araddr 
                    ,m09_axi_arlen  
                    ,m09_axi_rvalid 
                    ,m09_axi_rready 
                    ,m09_axi_rdata  
                    ,m09_axi_rlast  
                              
                    ,m10_axi_awvalid
                    ,m10_axi_awready
                    ,m10_axi_awaddr 
                    ,m10_axi_awlen  
                    ,m10_axi_wvalid 
                    ,m10_axi_wready 
                    ,m10_axi_wdata  
                    ,m10_axi_wstrb  
                    ,m10_axi_wlast  
                    ,m10_axi_bvalid 
                    ,m10_axi_bready 
                    ,m10_axi_arvalid
                    ,m10_axi_arready
                    ,m10_axi_araddr 
                    ,m10_axi_arlen  
                    ,m10_axi_rvalid 
                    ,m10_axi_rready 
                    ,m10_axi_rdata  
                    ,m10_axi_rlast  
                                 
                    ,m11_axi_awvalid
                    ,m11_axi_awready
                    ,m11_axi_awaddr 
                    ,m11_axi_awlen  
                    ,m11_axi_wvalid 
                    ,m11_axi_wready 
                    ,m11_axi_wdata  
                    ,m11_axi_wstrb  
                    ,m11_axi_wlast  
                    ,m11_axi_bvalid 
                    ,m11_axi_bready 
                    ,m11_axi_arvalid
                    ,m11_axi_arready
                    ,m11_axi_araddr 
                    ,m11_axi_arlen  
                    ,m11_axi_rvalid 
                    ,m11_axi_rready 
                    ,m11_axi_rdata  
                    ,m11_axi_rlast  
                    `endif
                    `ifdef XBAR 
            
                    ,m12_axi_awvalid
                    ,m12_axi_awready
                    ,m12_axi_awaddr 
                    ,m12_axi_awlen  
                    ,m12_axi_wvalid 
                    ,m12_axi_wready 
                    ,m12_axi_wdata  
                    ,m12_axi_wstrb  
                    ,m12_axi_wlast  
                    ,m12_axi_bvalid 
                    ,m12_axi_bready 
                    ,m12_axi_arvalid
                    ,m12_axi_arready
                    ,m12_axi_araddr 
                    ,m12_axi_arlen  
                    ,m12_axi_rvalid 
                    ,m12_axi_rready 
                    ,m12_axi_rdata  
                    ,m12_axi_rlast  
                                 
                    ,m13_axi_awvalid
                    ,m13_axi_awready
                    ,m13_axi_awaddr 
                    ,m13_axi_awlen  
                    ,m13_axi_wvalid 
                    ,m13_axi_wready 
                    ,m13_axi_wdata  
                    ,m13_axi_wstrb  
                    ,m13_axi_wlast  
                    ,m13_axi_bvalid 
                    ,m13_axi_bready 
                    ,m13_axi_arvalid
                    ,m13_axi_arready
                    ,m13_axi_araddr 
                    ,m13_axi_arlen  
                    ,m13_axi_rvalid 
                    ,m13_axi_rready 
                    ,m13_axi_rdata  
                    ,m13_axi_rlast  
                                  
                    ,m14_axi_awvalid
                    ,m14_axi_awready
                    ,m14_axi_awaddr 
                    ,m14_axi_awlen  
                    ,m14_axi_wvalid 
                    ,m14_axi_wready 
                    ,m14_axi_wdata  
                    ,m14_axi_wstrb  
                    ,m14_axi_wlast  
                    ,m14_axi_bvalid 
                    ,m14_axi_bready 
                    ,m14_axi_arvalid
                    ,m14_axi_arready
                    ,m14_axi_araddr 
                    ,m14_axi_arlen  
                    ,m14_axi_rvalid 
                    ,m14_axi_rready 
                    ,m14_axi_rdata  
                    ,m14_axi_rlast  
                               
                    ,m15_axi_awvalid
                    ,m15_axi_awready
                    ,m15_axi_awaddr 
                    ,m15_axi_awlen  
                    ,m15_axi_wvalid 
                    ,m15_axi_wready 
                    ,m15_axi_wdata  
                    ,m15_axi_wstrb  
                    ,m15_axi_wlast  
                    ,m15_axi_bvalid 
                    ,m15_axi_bready 
                    ,m15_axi_arvalid
                    ,m15_axi_arready
                    ,m15_axi_araddr 
                    ,m15_axi_arlen  
                    ,m15_axi_rvalid 
                    ,m15_axi_rready 
                    ,m15_axi_rdata  
                    ,m15_axi_rlast  
    
    ,m16_axi_awvalid
    ,m16_axi_awready  
    ,m16_axi_awaddr   
    ,m16_axi_awlen    
    ,m16_axi_wvalid   
    ,m16_axi_wready   
    ,m16_axi_wdata    
    ,m16_axi_wstrb    
    ,m16_axi_wlast    
    ,m16_axi_bvalid   
    ,m16_axi_bready   
    ,m16_axi_arvalid  
    ,m16_axi_arready  
    ,m16_axi_araddr   
    ,m16_axi_arlen    
    ,m16_axi_rvalid   
    ,m16_axi_rready   
    ,m16_axi_rdata    
    ,m16_axi_rlast    
                     
    ,m17_axi_awvalid  
    ,m17_axi_awready  
    ,m17_axi_awaddr   
    ,m17_axi_awlen    
    ,m17_axi_wvalid   
    ,m17_axi_wready   
    ,m17_axi_wdata    
    ,m17_axi_wstrb    
    ,m17_axi_wlast    
    ,m17_axi_bvalid   
    ,m17_axi_bready   
    ,m17_axi_arvalid  
    ,m17_axi_arready  
    ,m17_axi_araddr   
    ,m17_axi_arlen    
    ,m17_axi_rvalid   
    ,m17_axi_rready   
    ,m17_axi_rdata    
    ,m17_axi_rlast    
                     
    ,m18_axi_awvalid  
    ,m18_axi_awready  
    ,m18_axi_awaddr   
    ,m18_axi_awlen    
    ,m18_axi_wvalid   
    ,m18_axi_wready   
    ,m18_axi_wdata    
    ,m18_axi_wstrb    
    ,m18_axi_wlast    
    ,m18_axi_bvalid   
    ,m18_axi_bready   
    ,m18_axi_arvalid  
    ,m18_axi_arready  
    ,m18_axi_araddr   
    ,m18_axi_arlen    
    ,m18_axi_rvalid   
    ,m18_axi_rready   
    ,m18_axi_rdata    
    ,m18_axi_rlast    
                     
    ,m19_axi_awvalid  
    ,m19_axi_awready  
    ,m19_axi_awaddr   
    ,m19_axi_awlen    
    ,m19_axi_wvalid   
    ,m19_axi_wready   
    ,m19_axi_wdata    
    ,m19_axi_wstrb    
    ,m19_axi_wlast    
    ,m19_axi_bvalid   
    ,m19_axi_bready   
    ,m19_axi_arvalid  
    ,m19_axi_arready  
    ,m19_axi_araddr   
    ,m19_axi_arlen    
    ,m19_axi_rvalid   
    ,m19_axi_rready   
    ,m19_axi_rdata    
    ,m19_axi_rlast    
                    
    ,m20_axi_awvalid  
    ,m20_axi_awready  
    ,m20_axi_awaddr   
    ,m20_axi_awlen    
    ,m20_axi_wvalid   
    ,m20_axi_wready   
    ,m20_axi_wdata    
    ,m20_axi_wstrb    
    ,m20_axi_wlast    
    ,m20_axi_bvalid   
    ,m20_axi_bready   
    ,m20_axi_arvalid  
    ,m20_axi_arready  
    ,m20_axi_araddr   
    ,m20_axi_arlen    
    ,m20_axi_rvalid   
    ,m20_axi_rready   
    ,m20_axi_rdata    
    ,m20_axi_rlast    
                     
    ,m21_axi_awvalid  
    ,m21_axi_awready  
    ,m21_axi_awaddr   
    ,m21_axi_awlen    
    ,m21_axi_wvalid   
    ,m21_axi_wready   
    ,m21_axi_wdata    
    ,m21_axi_wstrb    
    ,m21_axi_wlast    
    ,m21_axi_bvalid   
    ,m21_axi_bready   
    ,m21_axi_arvalid  
    ,m21_axi_arready  
    ,m21_axi_araddr   
    ,m21_axi_arlen    
    ,m21_axi_rvalid   
    ,m21_axi_rready   
    ,m21_axi_rdata    
    ,m21_axi_rlast    
                     
    ,m22_axi_awvalid  
    ,m22_axi_awready  
    ,m22_axi_awaddr   
    ,m22_axi_awlen    
    ,m22_axi_wvalid   
    ,m22_axi_wready   
    ,m22_axi_wdata    
    ,m22_axi_wstrb    
    ,m22_axi_wlast    
    ,m22_axi_bvalid   
    ,m22_axi_bready   
    ,m22_axi_arvalid  
    ,m22_axi_arready  
    ,m22_axi_araddr   
    ,m22_axi_arlen    
    ,m22_axi_rvalid   
    ,m22_axi_rready   
    ,m22_axi_rdata    
    ,m22_axi_rlast    
                    
    ,m23_axi_awvalid  
    ,m23_axi_awready  
    ,m23_axi_awaddr   
    ,m23_axi_awlen    
    ,m23_axi_wvalid   
    ,m23_axi_wready   
    ,m23_axi_wdata    
    ,m23_axi_wstrb    
    ,m23_axi_wlast    
    ,m23_axi_bvalid   
    ,m23_axi_bready   
    ,m23_axi_arvalid  
    ,m23_axi_arready  
    ,m23_axi_araddr   
    ,m23_axi_arlen    
    ,m23_axi_rvalid   
    ,m23_axi_rready   
    ,m23_axi_rdata    
    ,m23_axi_rlast    
                     
    ,m24_axi_awvalid  
    ,m24_axi_awready  
    ,m24_axi_awaddr   
    ,m24_axi_awlen    
    ,m24_axi_wvalid   
    ,m24_axi_wready   
    ,m24_axi_wdata    
    ,m24_axi_wstrb    
    ,m24_axi_wlast    
    ,m24_axi_bvalid   
    ,m24_axi_bready   
    ,m24_axi_arvalid  
    ,m24_axi_arready  
    ,m24_axi_araddr   
    ,m24_axi_arlen    
    ,m24_axi_rvalid   
    ,m24_axi_rready   
    ,m24_axi_rdata    
    ,m24_axi_rlast    
                    
    ,m25_axi_awvalid  
    ,m25_axi_awready  
    ,m25_axi_awaddr   
    ,m25_axi_awlen    
    ,m25_axi_wvalid   
    ,m25_axi_wready   
    ,m25_axi_wdata    
    ,m25_axi_wstrb    
    ,m25_axi_wlast    
    ,m25_axi_bvalid   
    ,m25_axi_bready   
    ,m25_axi_arvalid  
    ,m25_axi_arready  
    ,m25_axi_araddr   
    ,m25_axi_arlen    
    ,m25_axi_rvalid   
    ,m25_axi_rready   
    ,m25_axi_rdata    
    ,m25_axi_rlast    
                     
    ,m26_axi_awvalid  
    ,m26_axi_awready  
    ,m26_axi_awaddr   
    ,m26_axi_awlen    
    ,m26_axi_wvalid   
    ,m26_axi_wready   
    ,m26_axi_wdata    
    ,m26_axi_wstrb    
    ,m26_axi_wlast    
    ,m26_axi_bvalid   
    ,m26_axi_bready   
    ,m26_axi_arvalid  
    ,m26_axi_arready  
    ,m26_axi_araddr   
    ,m26_axi_arlen    
    ,m26_axi_rvalid   
    ,m26_axi_rready   
    ,m26_axi_rdata    
    ,m26_axi_rlast    
                 
    ,m27_axi_awvalid  
    ,m27_axi_awready  
    ,m27_axi_awaddr   
    ,m27_axi_awlen    
    ,m27_axi_wvalid   
    ,m27_axi_wready   
    ,m27_axi_wdata    
    ,m27_axi_wstrb    
    ,m27_axi_wlast    
    ,m27_axi_bvalid   
    ,m27_axi_bready   
    ,m27_axi_arvalid  
    ,m27_axi_arready  
    ,m27_axi_araddr   
    ,m27_axi_arlen    
    ,m27_axi_rvalid   
    ,m27_axi_rready   
    ,m27_axi_rdata    
    ,m27_axi_rlast    
                     
    ,m28_axi_awvalid  
    ,m28_axi_awready  
    ,m28_axi_awaddr   
    ,m28_axi_awlen    
    ,m28_axi_wvalid   
    ,m28_axi_wready   
    ,m28_axi_wdata    
    ,m28_axi_wstrb    
    ,m28_axi_wlast    
    ,m28_axi_bvalid   
    ,m28_axi_bready   
    ,m28_axi_arvalid  
    ,m28_axi_arready  
    ,m28_axi_araddr   
    ,m28_axi_arlen    
    ,m28_axi_rvalid   
    ,m28_axi_rready   
    ,m28_axi_rdata    
    ,m28_axi_rlast    
                     
    ,m29_axi_awvalid  
    ,m29_axi_awready  
    ,m29_axi_awaddr   
    ,m29_axi_awlen    
    ,m29_axi_wvalid   
    ,m29_axi_wready   
    ,m29_axi_wdata    
    ,m29_axi_wstrb    
    ,m29_axi_wlast    
    ,m29_axi_bvalid   
    ,m29_axi_bready   
    ,m29_axi_arvalid  
    ,m29_axi_arready  
    ,m29_axi_araddr   
    ,m29_axi_arlen    
    ,m29_axi_rvalid   
    ,m29_axi_rready   
    ,m29_axi_rdata    
    ,m29_axi_rlast    
                     
    ,m30_axi_awvalid  
    ,m30_axi_awready  
    ,m30_axi_awaddr   
    ,m30_axi_awlen    
    ,m30_axi_wvalid   
    ,m30_axi_wready   
    ,m30_axi_wdata    
    ,m30_axi_wstrb    
    ,m30_axi_wlast    
    ,m30_axi_bvalid   
    ,m30_axi_bready   
    ,m30_axi_arvalid  
    ,m30_axi_arready  
    ,m30_axi_araddr   
    ,m30_axi_arlen    
    ,m30_axi_rvalid   
    ,m30_axi_rready   
    ,m30_axi_rdata    
    ,m30_axi_rlast    
                     
    ,m31_axi_awvalid  
    ,m31_axi_awready  
    ,m31_axi_awaddr   
    ,m31_axi_awlen    
    ,m31_axi_wvalid   
    ,m31_axi_wready   
    ,m31_axi_wdata    
    ,m31_axi_wstrb    
    ,m31_axi_wlast    
    ,m31_axi_bvalid   
    ,m31_axi_bready   
    ,m31_axi_arvalid  
    ,m31_axi_arready  
    ,m31_axi_araddr   
    ,m31_axi_arlen    
    ,m31_axi_rvalid   
    ,m31_axi_rready   
    ,m31_axi_rdata    
    ,m31_axi_rlast    
    
                    `endif  
                    ,done 
                    ,cmd
                    ,ce
                 );
    
    
    input `Cmd cmd;
    
     input wire ce;
//   output wire BVALID         ;
     input wire  HBM_REF_CLK_0  ;
     input wire  AXI_ACLK       ;
     input wire  AXI_ARESET_N   ;
     input wire  APB_PCLK       ;
     input wire  APB_PRESET_N   ;
      input wire [63:0]  A   ;
    //Output from HBM
    
  
    // AXI Write address channel
      input wire [32:0]   AXI_00_AWADDR  ;//= 32'b0;
      input wire          AXI_00_AWVALID ;//= 1'b0;
      reg [1:0]    AXI_00_AWBURST ;//= 2'b01; // Fixed address size not supported.
      input wire [3:0]    AXI_00_AWLEN   ;//= 4'b0;   
      reg [2:0]    AXI_00_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
      reg [5:0]    AXI_00_AWID    ;//= 6'b0;
      output wire         AXI_00_AWREADY ;
          
          // AXI Write channel
      input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_00_WDATA        ;//= 256'd96;
      input wire          AXI_00_WLAST        ;//= 1'b1;
      reg [63:0 ]  AXI_00_WSTRB        ;//= 32'hFFFF_FFFF;
      reg [31:0 ]  AXI_00_WDATA_PARITY ;//= 32'b0;
      input wire          AXI_00_WVALID       ;//;//= 1'b0;
      output wire         AXI_00_WREADY       ;
          
          // AXI Write Response channel
      reg                 AXI_00_BREADY   = 0    ;//=1'b1; //input to HBM
      wire [5:0]          AXI_00_BID;
      wire [1:0]          AXI_00_BRESP;
       wire                AXI_00_BVALID;

          // AXI Read Address channel
           input wire [32:0]   AXI_00_ARADDR ;// = 32'b0;
           input wire          AXI_00_ARVALID;// = 1'b0;
           reg [1:0]    AXI_00_ARBURST;// = 2'b01; // Fixed address size not supported.
           input wire [3:0]    AXI_00_ARLEN  ;// = 4'b0;   
           reg [2:0]    AXI_00_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
           reg [5:0]    AXI_00_ARID   ;// = 6'b0;
          output wire         AXI_00_ARREADY;

          // AXI Read Response 

           output wire         AXI_00_RVALID;
                  wire [5:0]   AXI_00_RID;
           output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_00_RDATA;
                  wire [31:0]  AXI_00_RDATA_PARITY;
                  wire [1:0]   AXI_00_RRESP;
           output wire         AXI_00_RLAST;
           input  wire         AXI_00_RREADY;
    
    reg[31:0] writeCount[31:0];
    reg[31:0] readCount[31:0];
    reg[31:0] writeCount_add_0[15:0];
    reg[31:0] writeCount_add_1[7:0];
    reg[31:0] writeCount_add_2[3:0];
    reg[31:0] writeCount_add_3[1:0];
    reg[31:0] writeCount_add_4;
    // AXI 01
    reg [31:0] writeRequest=0;
    `ifdef XBARorM1orM0                                                                                              
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_01_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_01_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_01_AWBURST ;//= 2'b01; // Fixed address size not supported.          
         input wire [3:0]     AXI_01_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_01_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_01_AWID    ;//= 6'b0;                                                
        output wire           AXI_01_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_01_WDATA        ;//= 256'd96;                                       
          input wire          AXI_01_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_01_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_01_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_01_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_01_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_01_BREADY    =1'b0   ;//=1'b1; //input to HBM                            
          wire [5:0]          AXI_01_BID;                                                             
          wire [1:0]          AXI_01_BRESP;                                                           
          wire                AXI_01_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                       1                                         
          input wire [32:0]   AXI_01_ARADDR ;// = 32'b0;                                              
          input wire          AXI_01_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_01_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_01_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_01_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_01_ARID   ;// = 6'b0;                                               
          output wire         AXI_01_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_01_RVALID;                                                          
                 wire [5:0]   AXI_01_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_01_RDATA;                                                           
                 wire [31:0]  AXI_01_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_01_RRESP;                                                           
          output wire         AXI_01_RLAST;                                                           
          input  wire         AXI_01_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_02_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_02_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_02_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_02_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_02_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_02_AWID    ;//= 6'b0;                                               
        output wire           AXI_02_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_02_WDATA        ;//= 256'd96;                                       
          input wire          AXI_02_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_02_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_02_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_02_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_02_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_02_BREADY     =1'b0    ;//=1'b1; //input to HBM                            
          wire [5:0]          AXI_02_BID;                                                             
          wire [1:0]          AXI_02_BRESP;                                                           
          wire                AXI_02_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_02_ARADDR ;// = 32'b0;                                              
          input wire          AXI_02_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_02_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_02_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_02_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_02_ARID   ;// = 6'b0;                                               
          output wire         AXI_02_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_02_RVALID;                                                          
                 wire [5:0]   AXI_02_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_02_RDATA;                                                           
                 wire [31:0]  AXI_02_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_02_RRESP;                                                           
          output wire         AXI_02_RLAST;                                                           
          input  wire         AXI_02_RREADY;                                                          


         input wire [32:0]    AXI_03_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_03_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_03_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_03_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_03_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_03_AWID    ;//= 6'b0;                                               
        output wire           AXI_03_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_03_WDATA        ;//= 256'd96;                                       
          input wire          AXI_03_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_03_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_03_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_03_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_03_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_03_BREADY     =1'b0    ;//=1'b1; //input to HBM                            
          wire [5:0]          AXI_03_BID;                                                             
          wire [1:0]          AXI_03_BRESP;                                                           
          wire                AXI_03_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_03_ARADDR ;// = 32'b0;                                              
          input wire          AXI_03_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_03_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_03_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_03_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_03_ARID   ;// = 6'b0;                                               
          output wire         AXI_03_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_03_RVALID;                                                          
                 wire [5:0]   AXI_03_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_03_RDATA;                                                           
                 wire [31:0]  AXI_03_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_03_RRESP;                                                           
          output wire         AXI_03_RLAST;                                                           
          input  wire         AXI_03_RREADY;                                                          

            `endif
            `ifdef XBARorM1

    // AXI Write address channel
          input wire [32:0]   AXI_04_AWADDR  ;//= 32'b0;
          input wire          AXI_04_AWVALID ;//= 1'b0;
                 reg [1:0]    AXI_04_AWBURST ;//= 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_04_AWLEN   ;//= 4'b0;   
                 reg [2:0]    AXI_04_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_04_AWID    ;//= 6'b0;
          output wire         AXI_04_AWREADY ;
          
          // AXI Write channel
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_04_WDATA        ;//= 256'd96;
          input wire          AXI_04_WLAST        ;//= 1'b1;
                 reg [63:0 ]  AXI_04_WSTRB        ;//= 32'hFFFF_FFFF;
                 reg [31:0 ]  AXI_04_WDATA_PARITY ;//= 32'b0;
          input wire          AXI_04_WVALID       ;//;//= 1'b0;
          output wire         AXI_04_WREADY       ;
          
          // AXI Write Response channel
          reg                 AXI_04_BREADY      =1'b1; //input to HBM
          wire [5:0]          AXI_04_BID;
          wire [1:0]          AXI_04_BRESP;
          wire                AXI_04_BVALID;

          // AXI Read Address channel
          input wire [32:0]   AXI_04_ARADDR ;// = 32'b0;
          input wire          AXI_04_ARVALID;// = 1'b0;
                 reg [1:0]    AXI_04_ARBURST;// = 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_04_ARLEN  ;// = 4'b0;   
                 reg [2:0]    AXI_04_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_04_ARID   ;// = 6'b0;
          output wire         AXI_04_ARREADY;

          // AXI Read Response 

          output wire         AXI_04_RVALID;
                 wire [5:0]   AXI_04_RID;
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_04_RDATA;
                 wire [31:0]  AXI_04_RDATA_PARITY;
                 wire [1:0]   AXI_04_RRESP;
          output wire         AXI_04_RLAST;
          input  wire         AXI_04_RREADY;
    
    
    // AXI 01
                                                                                                  
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_05_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_05_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_05_AWBURST ;//= 2'b01; // Fixed address size not supported.          
        input wire [3:0]     AXI_05_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_05_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_05_AWID    ;//= 6'b0;                                                
        output wire           AXI_05_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_05_WDATA        ;//= 256'd96;                                       
          input wire          AXI_05_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_05_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_05_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_05_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_05_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_05_BREADY      =1'b1; //input to HBM                            
          wire [5:0]          AXI_05_BID;                                                             
          wire [1:0]          AXI_05_BRESP;                                                           
          wire                AXI_05_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                                                                
          input wire [32:0]   AXI_05_ARADDR ;// = 32'b0;                                              
          input wire          AXI_05_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_05_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_05_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_05_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_05_ARID   ;// = 6'b0;                                               
          output wire         AXI_05_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_05_RVALID;                                                          
                 wire [5:0]   AXI_05_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_05_RDATA;                                                           
                 wire [31:0]  AXI_05_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_05_RRESP;                                                           
          output wire         AXI_05_RLAST;                                                           
          input  wire         AXI_05_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_06_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_06_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_06_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_06_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_06_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_06_AWID    ;//= 6'b0;                                               
        output wire           AXI_06_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_06_WDATA        ;//= 256'd96;                                       
          input wire          AXI_06_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_06_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_06_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_06_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_06_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_06_BREADY      =1'b1; //input to HBM                            
          wire [5:0]          AXI_06_BID;                                                             
          wire [1:0]          AXI_06_BRESP;                                                           
          wire                AXI_06_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_06_ARADDR ;// = 32'b0;                                              
          input wire          AXI_06_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_06_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_06_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_06_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_06_ARID   ;// = 6'b0;                                               
          output wire         AXI_06_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_06_RVALID;                                                          
                 wire [5:0]   AXI_06_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_06_RDATA;                                                           
                 wire [31:0]  AXI_06_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_06_RRESP;                                                           
          output wire         AXI_06_RLAST;                                                           
          input  wire         AXI_06_RREADY;                                                          


         input wire [32:0]    AXI_07_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_07_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_07_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_07_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_07_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_07_AWID    ;//= 6'b0;                                               
        output wire           AXI_07_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_07_WDATA        ;//= 256'd96;                                       
          input wire          AXI_07_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_07_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_07_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_07_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_07_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_07_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_07_BID;                                                             
          wire [1:0]          AXI_07_BRESP;                                                           
          wire                AXI_07_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_07_ARADDR ;// = 32'b0;                                              
          input wire          AXI_07_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_07_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_07_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_07_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_07_ARID   ;// = 6'b0;                                               
          output wire         AXI_07_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_07_RVALID;                                                          
                 wire [5:0]   AXI_07_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_07_RDATA;                                                           
                 wire [31:0]  AXI_07_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_07_RRESP;                                                           
          output wire         AXI_07_RLAST;                                                           
          input  wire         AXI_07_RREADY;                                                          


              `endif
            `ifdef XBARorM0 
    // AXI Write address channel
          input wire [32:0]   AXI_08_AWADDR  ;//= 32'b0;
          input wire          AXI_08_AWVALID ;//= 1'b0;
                 reg [1:0]    AXI_08_AWBURST ;//= 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_08_AWLEN   ;//= 4'b0;   
                 reg [2:0]    AXI_08_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_08_AWID    ;//= 6'b0;
          output wire         AXI_08_AWREADY ;
          
          // AXI Write channel
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_08_WDATA        ;//= 256'd96;
          input wire          AXI_08_WLAST        ;//= 1'b1;
                 reg [63:0 ]  AXI_08_WSTRB        ;//= 32'hFFFF_FFFF;
                 reg [31:0 ]  AXI_08_WDATA_PARITY ;//= 32'b0;
          input wire          AXI_08_WVALID       ;//;//= 1'b0;
          output wire         AXI_08_WREADY       ;
          
          // AXI Write Response channel
          reg                 AXI_08_BREADY       =1'b1; //input to HBM
          wire [5:0]          AXI_08_BID;
          wire [1:0]          AXI_08_BRESP;
          wire                AXI_08_BVALID;

          // AXI Read Address channel
          input wire [32:0]   AXI_08_ARADDR ;// = 32'b0;
          input wire          AXI_08_ARVALID;// = 1'b0;
                 reg [1:0]    AXI_08_ARBURST;// = 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_08_ARLEN  ;// = 4'b0;   
                 reg [2:0]    AXI_08_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_08_ARID   ;// = 6'b0;
          output wire         AXI_08_ARREADY;

          // AXI Read Response 

          output wire         AXI_08_RVALID;
                 wire [5:0]   AXI_08_RID;
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_08_RDATA;
                 wire [31:0]  AXI_08_RDATA_PARITY;
                 wire [1:0]   AXI_08_RRESP;
          output wire         AXI_08_RLAST;
          input  wire         AXI_08_RREADY;
    
    
    // AXI 01
                                                                                                  
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_09_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_09_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_09_AWBURST ;//= 2'b01; // Fixed address size not supported.          
         input wire [3:0]     AXI_09_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_09_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_09_AWID    ;//= 6'b0;                                                
        output wire           AXI_09_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_09_WDATA        ;//= 256'd96;                                       
          input wire         AXI_09_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_09_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_09_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_09_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_09_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_09_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_09_BID;                                                             
          wire [1:0]          AXI_09_BRESP;                                                           
          wire                AXI_09_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                                                                
          input wire [32:0]   AXI_09_ARADDR ;// = 32'b0;                                              
          input wire          AXI_09_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_09_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_09_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_09_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_09_ARID   ;// = 6'b0;                                               
          output wire         AXI_09_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_09_RVALID;                                                          
                 wire [5:0]   AXI_09_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_09_RDATA;                                                           
                 wire [31:0]  AXI_09_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_09_RRESP;                                                           
          output wire         AXI_09_RLAST;                                                           
          input  wire         AXI_09_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_10_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_10_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_10_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_10_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_10_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_10_AWID    ;//= 6'b0;                                               
        output wire           AXI_10_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_10_WDATA        ;//= 256'd96;                                       
          input wire          AXI_10_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_10_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_10_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_10_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_10_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_10_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_10_BID;                                                             
          wire [1:0]          AXI_10_BRESP;                                                           
          wire                AXI_10_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_10_ARADDR ;// = 32'b0;                                              
          input wire          AXI_10_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_10_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_10_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_10_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_10_ARID   ;// = 6'b0;                                               
          output wire         AXI_10_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_10_RVALID;                                                          
                 wire [5:0]   AXI_10_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_10_RDATA;                                                           
                 wire [31:0]  AXI_10_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_10_RRESP;                                                           
          output wire         AXI_10_RLAST;                                                           
          input  wire         AXI_10_RREADY;                                                          


         input wire [32:0]    AXI_11_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_11_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_11_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_11_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_11_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_11_AWID    ;//= 6'b0;                                               
        output wire           AXI_11_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_11_WDATA        ;//= 256'd96;                                       
          input wire          AXI_11_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_11_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_11_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_11_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_11_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_11_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_11_BID;                                                             
          wire [1:0]          AXI_11_BRESP;                                                           
          wire                AXI_11_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_11_ARADDR ;// = 32'b0;                                              
          input wire          AXI_11_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_11_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_11_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_11_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_11_ARID   ;// = 6'b0;                                               
          output wire         AXI_11_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_11_RVALID;                                                          
                 wire [5:0]   AXI_11_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_11_RDATA;                                                           
                 wire [31:0]  AXI_11_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_11_RRESP;                                                           
          output wire         AXI_11_RLAST;                                                           
          input  wire         AXI_11_RREADY;                                                          

        `endif
        `ifdef XBAR 
    // AXI Write address channel
          input wire [32:0]   AXI_12_AWADDR  ;//= 32'b0;
          input wire          AXI_12_AWVALID ;//= 1'b0;
                 reg [1:0]    AXI_12_AWBURST ;//= 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_12_AWLEN   ;//= 4'b0;   
                 reg [2:0]    AXI_12_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_12_AWID    ;//= 6'b0;
          output wire         AXI_12_AWREADY ;
          
          // AXI Write channel
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_12_WDATA        ;//= 256'd96;
          input wire          AXI_12_WLAST        ;//= 1'b1;
                 reg [63:0 ]  AXI_12_WSTRB        ;//= 32'hFFFF_FFFF;
                 reg [31:0 ]  AXI_12_WDATA_PARITY ;//= 32'b0;
          input wire          AXI_12_WVALID       ;//;//= 1'b0;
          output wire         AXI_12_WREADY       ;
          
          // AXI Write Response channel
          reg                 AXI_12_BREADY       =1'b1; //input to HBM
          wire [5:0]          AXI_12_BID;
          wire [1:0]          AXI_12_BRESP;
          wire                AXI_12_BVALID;

          // AXI Read Address channel
          input wire [32:0]   AXI_12_ARADDR ;// = 32'b0;
          input wire          AXI_12_ARVALID;// = 1'b0;
                 reg [1:0]    AXI_12_ARBURST;// = 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_12_ARLEN  ;// = 4'b0;   
                 reg [2:0]    AXI_12_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_12_ARID   ;// = 6'b0;
          output wire         AXI_12_ARREADY;

          // AXI Read Response 

          output wire         AXI_12_RVALID;
                 wire [5:0]   AXI_12_RID;
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_12_RDATA;
                 wire [31:0]  AXI_12_RDATA_PARITY;
                 wire [1:0]   AXI_12_RRESP;
          output wire         AXI_12_RLAST;
          input  wire         AXI_12_RREADY;
    
    
    // AXI 01
                                                                                                  
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_13_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_13_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_13_AWBURST ;//= 2'b01; // Fixed address size not supported.          
         input wire [3:0]     AXI_13_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_13_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_13_AWID    ;//= 6'b0;                                                
        output wire           AXI_13_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_13_WDATA        ;//= 256'd96;                                       
          input wire          AXI_13_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_13_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_13_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_13_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_13_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_13_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_13_BID;                                                             
          wire [1:0]          AXI_13_BRESP;                                                           
          wire                AXI_13_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                                                                
          input wire [32:0]   AXI_13_ARADDR ;// = 32'b0;                                              
          input wire          AXI_13_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_13_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_13_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_13_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_13_ARID   ;// = 6'b0;                                               
          output wire         AXI_13_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_13_RVALID;                                                          
                 wire [5:0]   AXI_13_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_13_RDATA;                                                           
                 wire [31:0]  AXI_13_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_13_RRESP;                                                           
          output wire         AXI_13_RLAST;                                                           
          input  wire         AXI_13_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_14_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_14_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_14_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_14_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_14_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_14_AWID    ;//= 6'b0;                                               
        output wire           AXI_14_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_14_WDATA        ;//= 256'd96;                                       
         input wire          AXI_14_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_14_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_14_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_14_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_14_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_14_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_14_BID;                                                             
          wire [1:0]          AXI_14_BRESP;                                                           
          wire                AXI_14_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_14_ARADDR ;// = 32'b0;                                              
          input wire          AXI_14_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_14_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_14_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_14_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_14_ARID   ;// = 6'b0;                                               
          output wire         AXI_14_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_14_RVALID;                                                          
                 wire [5:0]   AXI_14_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_14_RDATA;                                                           
                 wire [31:0]  AXI_14_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_14_RRESP;                                                           
          output wire         AXI_14_RLAST;                                                           
          input  wire         AXI_14_RREADY;                                                          


         input wire [32:0]    AXI_15_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_15_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_15_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_15_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_15_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_15_AWID    ;//= 6'b0;                                               
        output wire           AXI_15_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_15_WDATA        ;//= 256'd96;                                       
          input wire          AXI_15_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_15_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_15_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_15_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_15_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_15_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_15_BID;                                                             
          wire [1:0]          AXI_15_BRESP;                                                           
          wire                AXI_15_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_15_ARADDR ;// = 32'b0;                                              
          input wire          AXI_15_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_15_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_15_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_15_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_15_ARID   ;// = 6'b0;                                               
          output wire         AXI_15_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_15_RVALID;                                                          
                 wire [5:0]   AXI_15_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_15_RDATA;                                                           
                 wire [31:0]  AXI_15_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_15_RRESP;                                                           
          output wire         AXI_15_RLAST;                                                           
          input  wire         AXI_15_RREADY;                                                          



  
    // AXI Write address channel
      input wire [32:0]   AXI_16_AWADDR  ;//= 32'b0;
      input wire          AXI_16_AWVALID ;//= 1'b0;
      reg [1:0]    AXI_16_AWBURST ;//= 2'b01; // Fixed address size not supported.
      input wire [3:0]    AXI_16_AWLEN   ;//= 4'b0;   
      reg [2:0]    AXI_16_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
      reg [5:0]    AXI_16_AWID    ;//= 6'b0;
      output wire         AXI_16_AWREADY ;
          
          // AXI Write channel
      input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_16_WDATA        ;//= 256'd96;
      input wire          AXI_16_WLAST        ;//= 1'b1;
      reg [63:0 ]  AXI_16_WSTRB        ;//= 32'hFFFF_FFFF;
      reg [31:0 ]  AXI_16_WDATA_PARITY ;//= 32'b0;
      input wire          AXI_16_WVALID       ;//;//= 1'b0;
      output wire         AXI_16_WREADY       ;
          
          // AXI Write Response channel
      reg                 AXI_16_BREADY   = 0    ;//=1'b1; //input to HBM
      wire [5:0]          AXI_16_BID;
      wire [1:0]          AXI_16_BRESP;
       wire                AXI_16_BVALID;

          // AXI Read Address channel
           input wire [32:0]   AXI_16_ARADDR ;// = 32'b0;
           input wire          AXI_16_ARVALID;// = 1'b0;
           reg [1:0]    AXI_16_ARBURST;// = 2'b01; // Fixed address size not supported.
           input wire [3:0]    AXI_16_ARLEN  ;// = 4'b0;   
           reg [2:0]    AXI_16_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
           reg [5:0]    AXI_16_ARID   ;// = 6'b0;
          output wire         AXI_16_ARREADY;

          // AXI Read Response 

           output wire         AXI_16_RVALID;
                  wire [5:0]   AXI_16_RID;
           output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_16_RDATA;
                  wire [31:0]  AXI_16_RDATA_PARITY;
                  wire [1:0]   AXI_16_RRESP;
           output wire         AXI_16_RLAST;
           input  wire         AXI_16_RREADY;
    
                                                                                                  
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_17_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_17_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_17_AWBURST ;//= 2'b01; // Fixed address size not supported.          
         input wire [3:0]     AXI_17_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_17_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_17_AWID    ;//= 6'b0;                                                
        output wire           AXI_17_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_17_WDATA        ;//= 256'd96;                                       
          input wire          AXI_17_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_17_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_17_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_17_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_17_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_17_BREADY    =1'b0   ;//=1'b1; //input to HBM                            
          wire [5:0]          AXI_17_BID;                                                             
          wire [1:0]          AXI_17_BRESP;                                                           
          wire                AXI_17_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                       1                                         
          input wire [32:0]   AXI_17_ARADDR ;// = 32'b0;                                              
          input wire          AXI_17_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_17_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_17_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_17_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_17_ARID   ;// = 6'b0;                                               
          output wire         AXI_17_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_17_RVALID;                                                          
                 wire [5:0]   AXI_17_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_17_RDATA;                                                           
                 wire [31:0]  AXI_17_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_17_RRESP;                                                           
          output wire         AXI_17_RLAST;                                                           
          input  wire         AXI_17_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_18_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_18_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_18_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_18_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_18_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_18_AWID    ;//= 6'b0;                                               
        output wire           AXI_18_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_18_WDATA        ;//= 256'd96;                                       
          input wire          AXI_18_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_18_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_18_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_18_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_18_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_18_BREADY     =1'b0    ;//=1'b1; //input to HBM                            
          wire [5:0]          AXI_18_BID;                                                             
          wire [1:0]          AXI_18_BRESP;                                                           
          wire                AXI_18_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_18_ARADDR ;// = 32'b0;                                              
          input wire          AXI_18_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_18_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_18_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_18_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_18_ARID   ;// = 6'b0;                                               
          output wire         AXI_18_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_18_RVALID;                                                          
                 wire [5:0]   AXI_18_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_18_RDATA;                                                           
                 wire [31:0]  AXI_18_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_18_RRESP;                                                           
          output wire         AXI_18_RLAST;                                                           
          input  wire         AXI_18_RREADY;                                                          


         input wire [32:0]    AXI_19_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_19_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_19_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_19_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_19_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_19_AWID    ;//= 6'b0;                                               
        output wire           AXI_19_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_19_WDATA        ;//= 256'd96;                                       
          input wire          AXI_19_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_19_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_19_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_19_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_19_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_19_BREADY     =1'b0    ;//=1'b1; //input to HBM                            
          wire [5:0]          AXI_19_BID;                                                             
          wire [1:0]          AXI_19_BRESP;                                                           
          wire                AXI_19_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_19_ARADDR ;// = 32'b0;                                              
          input wire          AXI_19_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_19_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_19_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_19_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_19_ARID   ;// = 6'b0;                                               
          output wire         AXI_19_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_19_RVALID;                                                          
                 wire [5:0]   AXI_19_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_19_RDATA;                                                           
                 wire [31:0]  AXI_19_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_19_RRESP;                                                           
          output wire         AXI_19_RLAST;                                                           
          input  wire         AXI_19_RREADY;                                                          


    // AXI Write address channel
          input wire [32:0]   AXI_20_AWADDR  ;//= 32'b0;
          input wire          AXI_20_AWVALID ;//= 1'b0;
                 reg [1:0]    AXI_20_AWBURST ;//= 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_20_AWLEN   ;//= 4'b0;   
                 reg [2:0]    AXI_20_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_20_AWID    ;//= 6'b0;
          output wire         AXI_20_AWREADY ;
          
          // AXI Write channel
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_20_WDATA        ;//= 256'd96;
          input wire          AXI_20_WLAST        ;//= 1'b1;
                 reg [63:0 ]  AXI_20_WSTRB        ;//= 32'hFFFF_FFFF;
                 reg [31:0 ]  AXI_20_WDATA_PARITY ;//= 32'b0;
          input wire          AXI_20_WVALID       ;//;//= 1'b0;
          output wire         AXI_20_WREADY       ;
          
          // AXI Write Response channel
          reg                 AXI_20_BREADY      =1'b1; //input to HBM
          wire [5:0]          AXI_20_BID;
          wire [1:0]          AXI_20_BRESP;
          wire                AXI_20_BVALID;

          // AXI Read Address channel
          input wire [32:0]   AXI_20_ARADDR ;// = 32'b0;
          input wire          AXI_20_ARVALID;// = 1'b0;
                 reg [1:0]    AXI_20_ARBURST;// = 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_20_ARLEN  ;// = 4'b0;   
                 reg [2:0]    AXI_20_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_20_ARID   ;// = 6'b0;
          output wire         AXI_20_ARREADY;

          // AXI Read Response 

          output wire         AXI_20_RVALID;
                 wire [5:0]   AXI_20_RID;
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_20_RDATA;
                 wire [31:0]  AXI_20_RDATA_PARITY;
                 wire [1:0]   AXI_20_RRESP;
          output wire         AXI_20_RLAST;
          input  wire         AXI_20_RREADY;
    
    
    // AXI 01
                                                                                                  
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_21_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_21_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_21_AWBURST ;//= 2'b01; // Fixed address size not supported.          
         input wire [3:0]     AXI_21_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_21_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_21_AWID    ;//= 6'b0;                                                
        output wire           AXI_21_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_21_WDATA        ;//= 256'd96;                                       
          input wire          AXI_21_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_21_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_21_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_21_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_21_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_21_BREADY      =1'b1; //input to HBM                            
          wire [5:0]          AXI_21_BID;                                                             
          wire [1:0]          AXI_21_BRESP;                                                           
          wire                AXI_21_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                                                                
          input wire [32:0]   AXI_21_ARADDR ;// = 32'b0;                                              
          input wire          AXI_21_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_21_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_21_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_21_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_21_ARID   ;// = 6'b0;                                               
          output wire         AXI_21_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_21_RVALID;                                                          
                 wire [5:0]   AXI_21_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_21_RDATA;                                                           
                 wire [31:0]  AXI_21_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_21_RRESP;                                                           
          output wire         AXI_21_RLAST;                                                           
          input  wire         AXI_21_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_22_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_22_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_22_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_22_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_22_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_22_AWID    ;//= 6'b0;                                               
        output wire           AXI_22_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_22_WDATA        ;//= 256'd96;                                       
          input wire          AXI_22_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_22_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_22_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_22_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_22_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_22_BREADY      =1'b1; //input to HBM                            
          wire [5:0]          AXI_22_BID;                                                             
          wire [1:0]          AXI_22_BRESP;                                                           
          wire                AXI_22_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_22_ARADDR ;// = 32'b0;                                              
          input wire          AXI_22_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_22_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_22_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_22_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_22_ARID   ;// = 6'b0;                                               
          output wire         AXI_22_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_22_RVALID;                                                          
                 wire [5:0]   AXI_22_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_22_RDATA;                                                           
                 wire [31:0]  AXI_22_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_22_RRESP;                                                           
          output wire         AXI_22_RLAST;                                                           
          input  wire         AXI_22_RREADY;                                                          


         input wire [32:0]    AXI_23_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_23_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_23_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_23_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_23_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_23_AWID    ;//= 6'b0;                                               
        output wire           AXI_23_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_23_WDATA        ;//= 256'd96;                                       
          input wire          AXI_23_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_23_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_23_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_23_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_23_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_23_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_23_BID;                                                             
          wire [1:0]          AXI_23_BRESP;                                                           
          wire                AXI_23_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_23_ARADDR ;// = 32'b0;                                              
          input wire          AXI_23_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_23_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_23_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_23_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_23_ARID   ;// = 6'b0;                                               
          output wire         AXI_23_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_23_RVALID;                                                          
                 wire [5:0]   AXI_23_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_23_RDATA;                                                           
                 wire [31:0]  AXI_23_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_23_RRESP;                                                           
          output wire         AXI_23_RLAST;                                                           
          input  wire         AXI_23_RREADY;                                                          


  
    // AXI Write address channel
          input wire [32:0]   AXI_24_AWADDR  ;//= 32'b0;
          input wire          AXI_24_AWVALID ;//= 1'b0;
                 reg [1:0]    AXI_24_AWBURST ;//= 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_24_AWLEN   ;//= 4'b0;   
                 reg [2:0]    AXI_24_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_24_AWID    ;//= 6'b0;
          output wire         AXI_24_AWREADY ;
          
          // AXI Write channel
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_24_WDATA        ;//= 256'd96;
          input wire          AXI_24_WLAST        ;//= 1'b1;
                 reg [63:0 ]  AXI_24_WSTRB        ;//= 32'hFFFF_FFFF;
                 reg [31:0 ]  AXI_24_WDATA_PARITY ;//= 32'b0;
          input wire          AXI_24_WVALID       ;//;//= 1'b0;
          output wire         AXI_24_WREADY       ;
          
          // AXI Write Response channel
          reg                 AXI_24_BREADY       =1'b1; //input to HBM
          wire [5:0]          AXI_24_BID;
          wire [1:0]          AXI_24_BRESP;
          wire                AXI_24_BVALID;

          // AXI Read Address channel
          input wire [32:0]   AXI_24_ARADDR ;// = 32'b0;
          input wire          AXI_24_ARVALID;// = 1'b0;
                 reg [1:0]    AXI_24_ARBURST;// = 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_24_ARLEN  ;// = 4'b0;   
                 reg [2:0]    AXI_24_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_24_ARID   ;// = 6'b0;
          output wire         AXI_24_ARREADY;

          // AXI Read Response 

          output wire         AXI_24_RVALID;
                 wire [5:0]   AXI_24_RID;
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_24_RDATA;
                 wire [31:0]  AXI_24_RDATA_PARITY;
                 wire [1:0]   AXI_24_RRESP;
          output wire         AXI_24_RLAST;
          input  wire         AXI_24_RREADY;
    
    
    // AXI 01
                                                                                                  
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_25_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_25_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_25_AWBURST ;//= 2'b01; // Fixed address size not supported.          
         input wire [3:0]     AXI_25_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_25_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_25_AWID    ;//= 6'b0;                                                
        output wire           AXI_25_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_25_WDATA        ;//= 256'd96;                                       
          input wire         AXI_25_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_25_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_25_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_25_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_25_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_25_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_25_BID;                                                             
          wire [1:0]          AXI_25_BRESP;                                                           
          wire                AXI_25_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                                                                
          input wire [32:0]   AXI_25_ARADDR ;// = 32'b0;                                              
          input wire          AXI_25_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_25_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_25_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_25_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_25_ARID   ;// = 6'b0;                                               
          output wire         AXI_25_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_25_RVALID;                                                          
                 wire [5:0]   AXI_25_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_25_RDATA;                                                           
                 wire [31:0]  AXI_25_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_25_RRESP;                                                           
          output wire         AXI_25_RLAST;                                                           
          input  wire         AXI_25_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_26_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_26_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_26_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_26_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_26_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_26_AWID    ;//= 6'b0;                                               
        output wire           AXI_26_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_26_WDATA        ;//= 256'd96;                                       
          input wire          AXI_26_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_26_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_26_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_26_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_26_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_26_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_26_BID;                                                             
          wire [1:0]          AXI_26_BRESP;                                                           
          wire                AXI_26_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_26_ARADDR ;// = 32'b0;                                              
          input wire          AXI_26_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_26_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_26_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_26_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_26_ARID   ;// = 6'b0;                                               
          output wire         AXI_26_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_26_RVALID;                                                          
                 wire [5:0]   AXI_26_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_26_RDATA;                                                           
                 wire [31:0]  AXI_26_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_26_RRESP;                                                           
          output wire         AXI_26_RLAST;                                                           
          input  wire         AXI_26_RREADY;                                                          


         input wire [32:0]    AXI_27_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_27_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_27_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_27_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_27_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_27_AWID    ;//= 6'b0;                                               
        output wire           AXI_27_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_27_WDATA        ;//= 256'd96;                                       
          input wire          AXI_27_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_27_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_27_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_27_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_27_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_27_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_27_BID;                                                             
          wire [1:0]          AXI_27_BRESP;                                                           
          wire                AXI_27_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_27_ARADDR ;// = 32'b0;                                              
          input wire          AXI_27_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_27_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_27_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_27_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_27_ARID   ;// = 6'b0;                                               
          output wire         AXI_27_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_27_RVALID;                                                          
                 wire [5:0]   AXI_27_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_27_RDATA;                                                           
                 wire [31:0]  AXI_27_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_27_RRESP;                                                           
          output wire         AXI_27_RLAST;                                                           
          input  wire         AXI_27_RREADY;                                                          


    // AXI Write address channel
          input wire [32:0]   AXI_28_AWADDR  ;//= 32'b0;
          input wire          AXI_28_AWVALID ;//= 1'b0;
                 reg [1:0]    AXI_28_AWBURST ;//= 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_28_AWLEN   ;//= 4'b0;   
                 reg [2:0]    AXI_28_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_28_AWID    ;//= 6'b0;
          output wire         AXI_28_AWREADY ;
          
          // AXI Write channel
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_28_WDATA        ;//= 256'd96;
          input wire          AXI_28_WLAST        ;//= 1'b1;
                 reg [63:0 ]  AXI_28_WSTRB        ;//= 32'hFFFF_FFFF;
                 reg [31:0 ]  AXI_28_WDATA_PARITY ;//= 32'b0;
          input wire          AXI_28_WVALID       ;//;//= 1'b0;
          output wire         AXI_28_WREADY       ;
          
          // AXI Write Response channel
          reg                 AXI_28_BREADY       =1'b1; //input to HBM
          wire [5:0]          AXI_28_BID;
          wire [1:0]          AXI_28_BRESP;
          wire                AXI_28_BVALID;

          // AXI Read Address channel
          input wire [32:0]   AXI_28_ARADDR ;// = 32'b0;
          input wire          AXI_28_ARVALID;// = 1'b0;
                 reg [1:0]    AXI_28_ARBURST;// = 2'b01; // Fixed address size not supported.
          input wire [3:0]    AXI_28_ARLEN  ;// = 4'b0;   
                 reg [2:0]    AXI_28_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
                 reg [5:0]    AXI_28_ARID   ;// = 6'b0;
          output wire         AXI_28_ARREADY;

          // AXI Read Response 

          output wire         AXI_28_RVALID;
                 wire [5:0]   AXI_28_RID;
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_28_RDATA;
                 wire [31:0]  AXI_28_RDATA_PARITY;
                 wire [1:0]   AXI_28_RRESP;
          output wire         AXI_28_RLAST;
          input  wire         AXI_28_RREADY;
    
    
    // AXI 01
                                                                                                  
  // AXI Write address channel                                                                
         input wire [32:0]    AXI_29_AWADDR  ;//= 32'b0;                                               
         input wire           AXI_29_AWVALID ;//= 1'b0;                                                
                reg [1:0]     AXI_29_AWBURST ;//= 2'b01; // Fixed address size not supported.          
         input wire [3:0]     AXI_29_AWLEN   ;//= 4'b0;                                                
                reg [2:0]     AXI_29_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only    
                reg [5:0]     AXI_29_AWID    ;//= 6'b0;                                                
        output wire           AXI_29_AWREADY ;                                                         
                                                                                                     
          // AXI Write channel                                                                       
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_29_WDATA        ;//= 256'd96;                                       
          input wire          AXI_29_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_29_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_29_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_29_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_29_WREADY       ;                                                   
                                                                                                     
          // AXI Write Response chanel                                                               
          reg                 AXI_29_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_29_BID;                                                             
          wire [1:0]          AXI_29_BRESP;                                                           
          wire                AXI_29_BVALID;                                                          
                                                                                                   
          // AXI Read Address chann                                                                
          input wire [32:0]   AXI_29_ARADDR ;// = 32'b0;                                              
          input wire          AXI_29_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_29_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_29_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_29_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_29_ARID   ;// = 6'b0;                                               
          output wire         AXI_29_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_29_RVALID;                                                          
                 wire [5:0]   AXI_29_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_29_RDATA;                                                           
                 wire [31:0]  AXI_29_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_29_RRESP;                                                           
          output wire         AXI_29_RLAST;                                                           
          input  wire         AXI_29_RREADY;                                                          
                                                                                              

  // AXI 03                                                                                         
                                                                                                    
// AXI Write address channel                                                                        
         input wire [32:0]    AXI_30_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_30_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_30_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_30_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_30_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_30_AWID    ;//= 6'b0;                                               
        output wire           AXI_30_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_30_WDATA        ;//= 256'd96;                                       
          input wire          AXI_30_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_30_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_30_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_30_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_30_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_30_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_30_BID;                                                             
          wire [1:0]          AXI_30_BRESP;                                                           
          wire                AXI_30_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_30_ARADDR ;// = 32'b0;                                              
          input wire          AXI_30_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_30_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_30_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_30_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_30_ARID   ;// = 6'b0;                                               
          output wire         AXI_30_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_30_RVALID;                                                          
                 wire [5:0]   AXI_30_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_30_RDATA;                                                           
                 wire [31:0]  AXI_30_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_30_RRESP;                                                           
          output wire         AXI_30_RLAST;                                                           
          input  wire         AXI_30_RREADY;                                                          


         input wire [32:0]    AXI_31_AWADDR  ;//= 32'b0;                                              
         input wire           AXI_31_AWVALID ;//= 1'b0;                                               
                reg [1:0]     AXI_31_AWBURST ;//= 2'b01; // Fixed address size not supported.         
         input wire [3:0]     AXI_31_AWLEN   ;//= 4'b0;                                               
                reg [2:0]     AXI_31_AWSIZE  ;//= 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                reg [5:0]     AXI_31_AWID    ;//= 6'b0;                                               
        output wire           AXI_31_AWREADY ;                                                        
                                                                                                    
          // AXI Write channel                                                                      
          input wire [C_M00_AXI_DATA_WIDTH - 1:0]  AXI_31_WDATA        ;//= 256'd96;                                       
          input wire          AXI_31_WLAST        ;//= 1'b1;                                          
                 reg [63:0 ]  AXI_31_WSTRB        ;//= 32'hFFFF_FFFF;                                      
                 reg [31:0 ]  AXI_31_WDATA_PARITY ;//= 32'b0;                                         
          input wire          AXI_31_WVALID       ;//;//= 1'b0;                                       
          output wire         AXI_31_WREADY       ;                                                   
                                                                                                   
          // AXI Write Response cha                                                                
          reg                 AXI_31_BREADY       =1'b1; //input to HBM                            
          wire [5:0]          AXI_31_BID;                                                             
          wire [1:0]          AXI_31_BRESP;                                                           
          wire                AXI_31_BVALID;                                                          
                                                                                                    
          // AXI Read Address chann                                                                 
          input wire [32:0]   AXI_31_ARADDR ;// = 32'b0;                                              
          input wire          AXI_31_ARVALID;// = 1'b0;                                               
                 reg [1:0]    AXI_31_ARBURST;// = 2'b01; // Fixed address size not supported.         
          input wire [3:0]    AXI_31_ARLEN  ;// = 4'b0;                                               
                 reg [2:0]    AXI_31_ARSIZE ;// = 3'b101; // Only 3'b101 supported. 2**5 bytes only   
                 reg [5:0]    AXI_31_ARID   ;// = 6'b0;                                               
          output wire         AXI_31_ARREADY;                                                         
                                                                                                   
          // AXI Read Response                                                                     
                                                                                                   
          output wire         AXI_31_RVALID;                                                          
                 wire [5:0]   AXI_31_RID;                                                             
          output wire [C_M00_AXI_DATA_WIDTH - 1:0] AXI_31_RDATA;                                                           
                 wire [31:0]  AXI_31_RDATA_PARITY;                                                    
                 wire [1:0]   AXI_31_RRESP;                                                           
          output wire         AXI_31_RLAST;                                                           
          input  wire         AXI_31_RREADY;                                                          



`endif



  output wire                                    m00_axi_awvalid     ;
  input  wire                                    m00_axi_awready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m00_axi_awaddr      ;
  output wire [8-1:0]                            m00_axi_awlen       ;
  output wire                                    m00_axi_wvalid      ;
  input  wire                                    m00_axi_wready      ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m00_axi_wdata       ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m00_axi_wstrb       ;
  output wire                                    m00_axi_wlast       ;
  input  wire                                    m00_axi_bvalid      ;
  output wire                                    m00_axi_bready      ;
  output wire                                    m00_axi_arvalid     ;
  input  wire                                    m00_axi_arready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m00_axi_araddr      ;
  output wire [8-1:0]                            m00_axi_arlen       ;
  input  wire                                    m00_axi_rvalid      ;
  output wire                                    m00_axi_rready      ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m00_axi_rdata       ;
  input  wire                                    m00_axi_rlast       ;
  `ifdef XBARorM1orM0
  output wire                                    m01_axi_awvalid     ;
  input  wire                                    m01_axi_awready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m01_axi_awaddr      ;
  output wire [8-1:0]                            m01_axi_awlen       ;
  output wire                                    m01_axi_wvalid      ;
  input  wire                                    m01_axi_wready      ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m01_axi_wdata       ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m01_axi_wstrb       ;
  output wire                                    m01_axi_wlast       ;
  input  wire                                    m01_axi_bvalid      ;
  output wire                                    m01_axi_bready      ;
  output wire                                    m01_axi_arvalid     ;
  input  wire                                    m01_axi_arready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m01_axi_araddr      ;
  output wire [8-1:0]                            m01_axi_arlen       ;
  input  wire                                    m01_axi_rvalid      ;
  output wire                                    m01_axi_rready      ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m01_axi_rdata       ;
  input  wire                                    m01_axi_rlast       ;
  
  output wire                                    m02_axi_awvalid     ;
  input  wire                                    m02_axi_awready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m02_axi_awaddr      ;
  output wire [8-1:0]                            m02_axi_awlen       ;
  output wire                                    m02_axi_wvalid      ;
  input  wire                                    m02_axi_wready      ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m02_axi_wdata       ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m02_axi_wstrb       ;
  output wire                                    m02_axi_wlast       ;
  input  wire                                    m02_axi_bvalid      ;
  output wire                                    m02_axi_bready      ;
  output wire                                    m02_axi_arvalid     ;
  input  wire                                    m02_axi_arready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m02_axi_araddr      ;
  output wire [8-1:0]                            m02_axi_arlen       ;
  input  wire                                    m02_axi_rvalid      ;
  output wire                                    m02_axi_rready      ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m02_axi_rdata       ;
  input  wire                                    m02_axi_rlast       ;
  
  output wire                                    m03_axi_awvalid     ;
  input  wire                                    m03_axi_awready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m03_axi_awaddr      ;
  output wire [8-1:0]                            m03_axi_awlen       ;
  output wire                                    m03_axi_wvalid      ;
  input  wire                                    m03_axi_wready      ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m03_axi_wdata       ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m03_axi_wstrb       ;
  output wire                                    m03_axi_wlast       ;
  input  wire                                    m03_axi_bvalid      ;
  output wire                                    m03_axi_bready      ;
  output wire                                    m03_axi_arvalid     ;
  input  wire                                    m03_axi_arready     ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m03_axi_araddr      ;
  output wire [8-1:0]                            m03_axi_arlen       ;
  input  wire                                    m03_axi_rvalid      ;
  output wire                                    m03_axi_rready      ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m03_axi_rdata       ;
  input  wire                                    m03_axi_rlast       ;
  `endif
`ifdef XBARorM1
  
  output wire                                    m04_axi_awvalid   ;
  input  wire                                    m04_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m04_axi_awaddr    ;
  output wire [8-1:0]                            m04_axi_awlen     ;
  output wire                                    m04_axi_wvalid    ;
  input  wire                                    m04_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m04_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m04_axi_wstrb     ;
  output wire                                    m04_axi_wlast     ;
  input  wire                                    m04_axi_bvalid    ;
  output wire                                    m04_axi_bready    ;
  output wire                                    m04_axi_arvalid   ;
  input  wire                                    m04_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m04_axi_araddr    ;
  output wire [8-1:0]                            m04_axi_arlen     ;
  input  wire                                    m04_axi_rvalid    ;
  output wire                                    m04_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m04_axi_rdata     ;
  input  wire                                    m04_axi_rlast     ;
  
  output wire                                    m05_axi_awvalid   ;
  input  wire                                    m05_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m05_axi_awaddr    ;
  output wire [8-1:0]                            m05_axi_awlen     ;
  output wire                                    m05_axi_wvalid    ;
  input  wire                                    m05_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m05_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m05_axi_wstrb     ;
  output wire                                    m05_axi_wlast     ;
  input  wire                                    m05_axi_bvalid    ;
  output wire                                    m05_axi_bready    ;
  output wire                                    m05_axi_arvalid   ;
  input  wire                                    m05_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m05_axi_araddr    ;
  output wire [8-1:0]                            m05_axi_arlen     ;
  input  wire                                    m05_axi_rvalid    ;
  output wire                                    m05_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m05_axi_rdata     ;
  input  wire                                    m05_axi_rlast     ;
  
  output wire                                    m06_axi_awvalid   ;
  input  wire                                    m06_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m06_axi_awaddr    ;
  output wire [8-1:0]                            m06_axi_awlen     ;
  output wire                                    m06_axi_wvalid    ;
  input  wire                                    m06_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m06_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m06_axi_wstrb     ;
  output wire                                    m06_axi_wlast     ;
  input  wire                                    m06_axi_bvalid    ;
  output wire                                    m06_axi_bready    ;
  output wire                                    m06_axi_arvalid   ;
  input  wire                                    m06_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m06_axi_araddr    ;
  output wire [8-1:0]                            m06_axi_arlen     ;
  input  wire                                    m06_axi_rvalid    ;
  output wire                                    m06_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m06_axi_rdata     ;
  input  wire                                    m06_axi_rlast     ;
  
  output wire                                    m07_axi_awvalid   ;
  input  wire                                    m07_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m07_axi_awaddr    ;
  output wire [8-1:0]                            m07_axi_awlen     ;
  output wire                                    m07_axi_wvalid    ;
  input  wire                                    m07_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m07_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m07_axi_wstrb     ;
  output wire                                    m07_axi_wlast     ;
  input  wire                                    m07_axi_bvalid    ;
  output wire                                    m07_axi_bready    ;
  output wire                                    m07_axi_arvalid   ;
  input  wire                                    m07_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m07_axi_araddr    ;
  output wire [8-1:0]                            m07_axi_arlen     ;
  input  wire                                    m07_axi_rvalid    ;
  output wire                                    m07_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m07_axi_rdata     ;
  input  wire                                    m07_axi_rlast     ;
 `endif
`ifdef XBARorM0 
   
  output wire                                    m08_axi_awvalid;
  input  wire                                    m08_axi_awready;
  output wire [C_M08_AXI_ADDR_WIDTH-1:0]         m08_axi_awaddr ;
  output wire [8-1:0]                            m08_axi_awlen  ;
  output wire                                    m08_axi_wvalid ;
  input  wire                                    m08_axi_wready ;
  output wire [C_M08_AXI_DATA_WIDTH-1:0]         m08_axi_wdata  ;
  output wire [C_M08_AXI_DATA_WIDTH/8-1:0]       m08_axi_wstrb  ;
  output wire                                    m08_axi_wlast  ;
  input  wire                                    m08_axi_bvalid ;
  output wire                                    m08_axi_bready ;
  output wire                                    m08_axi_arvalid;
  input  wire                                    m08_axi_arready;
  output wire [C_M08_AXI_ADDR_WIDTH-1:0]         m08_axi_araddr ;
  output wire [8-1:0]                            m08_axi_arlen  ;
  input  wire                                    m08_axi_rvalid ;
  output wire                                    m08_axi_rready ;
  input  wire [C_M08_AXI_DATA_WIDTH-1:0]         m08_axi_rdata  ;
  input  wire                                    m08_axi_rlast  ;
  // AXI4 master interface m09_axi                              ;
  output wire                                    m09_axi_awvalid;
  input  wire                                    m09_axi_awready;
  output wire [C_M09_AXI_ADDR_WIDTH-1:0]         m09_axi_awaddr ;
  output wire [8-1:0]                            m09_axi_awlen  ;
  output wire                                    m09_axi_wvalid ;
  input  wire                                    m09_axi_wready ;
  output wire [C_M09_AXI_DATA_WIDTH-1:0]         m09_axi_wdata  ;
  output wire [C_M09_AXI_DATA_WIDTH/8-1:0]       m09_axi_wstrb  ;
  output wire                                    m09_axi_wlast  ;
  input  wire                                    m09_axi_bvalid ;
  output wire                                    m09_axi_bready ;
  output wire                                    m09_axi_arvalid;
  input  wire                                    m09_axi_arready;
  output wire [C_M09_AXI_ADDR_WIDTH-1:0]         m09_axi_araddr ;
  output wire [8-1:0]                            m09_axi_arlen  ;
  input  wire                                    m09_axi_rvalid ;
  output wire                                    m09_axi_rready ;
  input  wire [C_M09_AXI_DATA_WIDTH-1:0]         m09_axi_rdata  ;
  input  wire                                    m09_axi_rlast  ;
  // AXI4 master interface m10_axi                              ;
  output wire                                    m10_axi_awvalid;
  input  wire                                    m10_axi_awready;
  output wire [C_M10_AXI_ADDR_WIDTH-1:0]         m10_axi_awaddr ;
  output wire [8-1:0]                            m10_axi_awlen  ;
  output wire                                    m10_axi_wvalid ;
  input  wire                                    m10_axi_wready ;
  output wire [C_M10_AXI_DATA_WIDTH-1:0]         m10_axi_wdata  ;
  output wire [C_M10_AXI_DATA_WIDTH/8-1:0]       m10_axi_wstrb  ;
  output wire                                    m10_axi_wlast  ;
  input  wire                                    m10_axi_bvalid ;
  output wire                                    m10_axi_bready ;
  output wire                                    m10_axi_arvalid;
  input  wire                                    m10_axi_arready;
  output wire [C_M10_AXI_ADDR_WIDTH-1:0]         m10_axi_araddr ;
  output wire [8-1:0]                            m10_axi_arlen  ;
  input  wire                                    m10_axi_rvalid ;
  output wire                                    m10_axi_rready ;
  input  wire [C_M10_AXI_DATA_WIDTH-1:0]         m10_axi_rdata  ;
  input  wire                                    m10_axi_rlast  ;
  // AXI4 master interface m11_axi                              ;
  output wire                                    m11_axi_awvalid;
  input  wire                                    m11_axi_awready;
  output wire [C_M11_AXI_ADDR_WIDTH-1:0]         m11_axi_awaddr ;
  output wire [8-1:0]                            m11_axi_awlen  ;
  output wire                                    m11_axi_wvalid ;
  input  wire                                    m11_axi_wready ;
  output wire [C_M11_AXI_DATA_WIDTH-1:0]         m11_axi_wdata  ;
  output wire [C_M11_AXI_DATA_WIDTH/8-1:0]       m11_axi_wstrb  ;
  output wire                                    m11_axi_wlast  ;
  input  wire                                    m11_axi_bvalid ;
  output wire                                    m11_axi_bready ;
  output wire                                    m11_axi_arvalid;
  input  wire                                    m11_axi_arready;
  output wire [C_M11_AXI_ADDR_WIDTH-1:0]         m11_axi_araddr ;
  output wire [8-1:0]                            m11_axi_arlen  ;
  input  wire                                    m11_axi_rvalid ;
  output wire                                    m11_axi_rready ;
  input  wire [C_M11_AXI_DATA_WIDTH-1:0]         m11_axi_rdata  ;
  input  wire                                    m11_axi_rlast  ;
  // AXI4 master interface m12_axi 
  `endif
`ifdef XBAR 
       
  output wire                                    m12_axi_awvalid;
  input  wire                                    m12_axi_awready;
  output wire [C_M12_AXI_ADDR_WIDTH-1:0]         m12_axi_awaddr ;
  output wire [8-1:0]                            m12_axi_awlen  ;
  output wire                                    m12_axi_wvalid ;
  input  wire                                    m12_axi_wready ;
  output wire [C_M12_AXI_DATA_WIDTH-1:0]         m12_axi_wdata  ;
  output wire [C_M12_AXI_DATA_WIDTH/8-1:0]       m12_axi_wstrb  ;
  output wire                                    m12_axi_wlast  ;
  input  wire                                    m12_axi_bvalid ;
  output wire                                    m12_axi_bready ;
  output wire                                    m12_axi_arvalid;
  input  wire                                    m12_axi_arready;
  output wire [C_M12_AXI_ADDR_WIDTH-1:0]         m12_axi_araddr ;
  output wire [8-1:0]                            m12_axi_arlen  ;
  input  wire                                    m12_axi_rvalid ;
  output wire                                    m12_axi_rready ;
  input  wire [C_M12_AXI_DATA_WIDTH-1:0]         m12_axi_rdata  ;
  input  wire                                    m12_axi_rlast  ;
  // AXI4 master interface m13_axi                              ;
  output wire                                    m13_axi_awvalid;
  input  wire                                    m13_axi_awready;
  output wire [C_M13_AXI_ADDR_WIDTH-1:0]         m13_axi_awaddr ;
  output wire [8-1:0]                            m13_axi_awlen  ;
  output wire                                    m13_axi_wvalid ;
  input  wire                                    m13_axi_wready ;
  output wire [C_M13_AXI_DATA_WIDTH-1:0]         m13_axi_wdata  ;
  output wire [C_M13_AXI_DATA_WIDTH/8-1:0]       m13_axi_wstrb  ;
  output wire                                    m13_axi_wlast  ;
  input  wire                                    m13_axi_bvalid ;
  output wire                                    m13_axi_bready ;
  output wire                                    m13_axi_arvalid;
  input  wire                                    m13_axi_arready;
  output wire [C_M13_AXI_ADDR_WIDTH-1:0]         m13_axi_araddr ;
  output wire [8-1:0]                            m13_axi_arlen  ;
  input  wire                                    m13_axi_rvalid ;
  output wire                                    m13_axi_rready ;
  input  wire [C_M13_AXI_DATA_WIDTH-1:0]         m13_axi_rdata  ;
  input  wire                                    m13_axi_rlast  ;
  // AXI4 master interface m14_axi                              ;
  output wire                                    m14_axi_awvalid;
  input  wire                                    m14_axi_awready;
  output wire [C_M14_AXI_ADDR_WIDTH-1:0]         m14_axi_awaddr ;
  output wire [8-1:0]                            m14_axi_awlen  ;
  output wire                                    m14_axi_wvalid ;
  input  wire                                    m14_axi_wready ;
  output wire [C_M14_AXI_DATA_WIDTH-1:0]         m14_axi_wdata  ;
  output wire [C_M14_AXI_DATA_WIDTH/8-1:0]       m14_axi_wstrb  ;
  output wire                                    m14_axi_wlast  ;
  input  wire                                    m14_axi_bvalid ;
  output wire                                    m14_axi_bready ;
  output wire                                    m14_axi_arvalid;
  input  wire                                    m14_axi_arready;
  output wire [C_M14_AXI_ADDR_WIDTH-1:0]         m14_axi_araddr ;
  output wire [8-1:0]                            m14_axi_arlen  ;
  input  wire                                    m14_axi_rvalid ;
  output wire                                    m14_axi_rready ;
  input  wire [C_M14_AXI_DATA_WIDTH-1:0]         m14_axi_rdata  ;
  input  wire                                    m14_axi_rlast  ;
  // AXI4 master interface m15_axi                              ;
  output wire                                    m15_axi_awvalid;
  input  wire                                    m15_axi_awready;
  output wire [C_M15_AXI_ADDR_WIDTH-1:0]         m15_axi_awaddr ;
  output wire [8-1:0]                            m15_axi_awlen  ;
  output wire                                    m15_axi_wvalid ;
  input  wire                                    m15_axi_wready ;
  output wire [C_M15_AXI_DATA_WIDTH-1:0]         m15_axi_wdata  ;
  output wire [C_M15_AXI_DATA_WIDTH/8-1:0]       m15_axi_wstrb  ;
  output wire                                    m15_axi_wlast  ;
  input  wire                                    m15_axi_bvalid ;
  output wire                                    m15_axi_bready ;
  output wire                                    m15_axi_arvalid;
  input  wire                                    m15_axi_arready;
  output wire [C_M15_AXI_ADDR_WIDTH-1:0]         m15_axi_araddr ;
  output wire [8-1:0]                            m15_axi_arlen  ;
  input  wire                                    m15_axi_rvalid ;
  output wire                                    m15_axi_rready ;
  input  wire [C_M15_AXI_DATA_WIDTH-1:0]         m15_axi_rdata  ;
  input  wire                                    m15_axi_rlast  ;
  
   
  output wire                                    m16_axi_awvalid      ;
  input  wire                                    m16_axi_awready      ;
  output wire [C_M16_AXI_ADDR_WIDTH-1:0]         m16_axi_awaddr       ;
  output wire [8-1:0]                            m16_axi_awlen        ;
  output wire                                    m16_axi_wvalid       ;
  input  wire                                    m16_axi_wready       ;
  output wire [C_M16_AXI_DATA_WIDTH-1:0]         m16_axi_wdata        ;
  output wire [C_M16_AXI_DATA_WIDTH/8-1:0]       m16_axi_wstrb        ;
  output wire                                    m16_axi_wlast        ;
  input  wire                                    m16_axi_bvalid       ;
  output wire                                    m16_axi_bready       ;
  output wire                                    m16_axi_arvalid      ;
  input  wire                                    m16_axi_arready      ;
  output wire [C_M16_AXI_ADDR_WIDTH-1:0]         m16_axi_araddr       ;
  output wire [8-1:0]                            m16_axi_arlen        ;
  input  wire                                    m16_axi_rvalid       ;
  output wire                                    m16_axi_rready       ;
  input  wire [C_M16_AXI_DATA_WIDTH-1:0]         m16_axi_rdata        ;
  input  wire                                    m16_axi_rlast        ;
  // AXI4 master interface m17_axi                                    ;
  output wire                                    m17_axi_awvalid      ;
  input  wire                                    m17_axi_awready      ;
  output wire [C_M17_AXI_ADDR_WIDTH-1:0]         m17_axi_awaddr       ;
  output wire [8-1:0]                            m17_axi_awlen        ;
  output wire                                    m17_axi_wvalid       ;
  input  wire                                    m17_axi_wready       ;
  output wire [C_M17_AXI_DATA_WIDTH-1:0]         m17_axi_wdata        ;
  output wire [C_M17_AXI_DATA_WIDTH/8-1:0]       m17_axi_wstrb        ;
  output wire                                    m17_axi_wlast        ;
  input  wire                                    m17_axi_bvalid       ;
  output wire                                    m17_axi_bready       ;
  output wire                                    m17_axi_arvalid      ;
  input  wire                                    m17_axi_arready      ;
  output wire [C_M17_AXI_ADDR_WIDTH-1:0]         m17_axi_araddr       ;
  output wire [8-1:0]                            m17_axi_arlen        ;
  input  wire                                    m17_axi_rvalid       ;
  output wire                                    m17_axi_rready       ;
  input  wire [C_M17_AXI_DATA_WIDTH-1:0]         m17_axi_rdata        ;
  input  wire                                    m17_axi_rlast        ;
  // AXI4 master interface m18_axi                                    ;
  output wire                                    m18_axi_awvalid      ;
  input  wire                                    m18_axi_awready      ;
  output wire [C_M18_AXI_ADDR_WIDTH-1:0]         m18_axi_awaddr       ;
  output wire [8-1:0]                            m18_axi_awlen        ;
  output wire                                    m18_axi_wvalid       ;
  input  wire                                    m18_axi_wready       ;
  output wire [C_M18_AXI_DATA_WIDTH-1:0]         m18_axi_wdata        ;
  output wire [C_M18_AXI_DATA_WIDTH/8-1:0]       m18_axi_wstrb        ;
  output wire                                    m18_axi_wlast        ;
  input  wire                                    m18_axi_bvalid       ;
  output wire                                    m18_axi_bready       ;
  output wire                                    m18_axi_arvalid      ;
  input  wire                                    m18_axi_arready      ;
  output wire [C_M18_AXI_ADDR_WIDTH-1:0]         m18_axi_araddr       ;
  output wire [8-1:0]                            m18_axi_arlen        ;
  input  wire                                    m18_axi_rvalid       ;
  output wire                                    m18_axi_rready       ;
  input  wire [C_M18_AXI_DATA_WIDTH-1:0]         m18_axi_rdata        ;
  input  wire                                    m18_axi_rlast        ;
  // AXI4 master interface m19_axi                                    ;
  output wire                                    m19_axi_awvalid      ;
  input  wire                                    m19_axi_awready      ;
  output wire [C_M19_AXI_ADDR_WIDTH-1:0]         m19_axi_awaddr       ;
  output wire [8-1:0]                            m19_axi_awlen        ;
  output wire                                    m19_axi_wvalid       ;
  input  wire                                    m19_axi_wready       ;
  output wire [C_M19_AXI_DATA_WIDTH-1:0]         m19_axi_wdata        ;
  output wire [C_M19_AXI_DATA_WIDTH/8-1:0]       m19_axi_wstrb        ;
  output wire                                    m19_axi_wlast        ;
  input  wire                                    m19_axi_bvalid       ;
  output wire                                    m19_axi_bready       ;
  output wire                                    m19_axi_arvalid      ;
  input  wire                                    m19_axi_arready      ;
  output wire [C_M19_AXI_ADDR_WIDTH-1:0]         m19_axi_araddr       ;
  output wire [8-1:0]                            m19_axi_arlen        ;
  input  wire                                    m19_axi_rvalid       ;
  output wire                                    m19_axi_rready       ;
  input  wire [C_M19_AXI_DATA_WIDTH-1:0]         m19_axi_rdata        ;
  input  wire                                    m19_axi_rlast        ;
  // AXI4 master interface m20_axi                                    ;
  output wire                                    m20_axi_awvalid      ;
  input  wire                                    m20_axi_awready      ;
  output wire [C_M20_AXI_ADDR_WIDTH-1:0]         m20_axi_awaddr       ;
  output wire [8-1:0]                            m20_axi_awlen        ;
  output wire                                    m20_axi_wvalid       ;
  input  wire                                    m20_axi_wready       ;
  output wire [C_M20_AXI_DATA_WIDTH-1:0]         m20_axi_wdata        ;
  output wire [C_M20_AXI_DATA_WIDTH/8-1:0]       m20_axi_wstrb        ;
  output wire                                    m20_axi_wlast        ;
  input  wire                                    m20_axi_bvalid       ;
  output wire                                    m20_axi_bready       ;
  output wire                                    m20_axi_arvalid      ;
  input  wire                                    m20_axi_arready      ;
  output wire [C_M20_AXI_ADDR_WIDTH-1:0]         m20_axi_araddr       ;
  output wire [8-1:0]                            m20_axi_arlen        ;
  input  wire                                    m20_axi_rvalid       ;
  output wire                                    m20_axi_rready       ;
  input  wire [C_M20_AXI_DATA_WIDTH-1:0]         m20_axi_rdata        ;
  input  wire                                    m20_axi_rlast        ;
  // AXI4 master interface m21_axi                                    ;
  output wire                                    m21_axi_awvalid      ;
  input  wire                                    m21_axi_awready      ;
  output wire [C_M21_AXI_ADDR_WIDTH-1:0]         m21_axi_awaddr       ;
  output wire [8-1:0]                            m21_axi_awlen        ;
  output wire                                    m21_axi_wvalid       ;
  input  wire                                    m21_axi_wready       ;
  output wire [C_M21_AXI_DATA_WIDTH-1:0]         m21_axi_wdata        ;
  output wire [C_M21_AXI_DATA_WIDTH/8-1:0]       m21_axi_wstrb        ;
  output wire                                    m21_axi_wlast        ;
  input  wire                                    m21_axi_bvalid       ;
  output wire                                    m21_axi_bready       ;
  output wire                                    m21_axi_arvalid      ;
  input  wire                                    m21_axi_arready      ;
  output wire [C_M21_AXI_ADDR_WIDTH-1:0]         m21_axi_araddr       ;
  output wire [8-1:0]                            m21_axi_arlen        ;
  input  wire                                    m21_axi_rvalid       ;
  output wire                                    m21_axi_rready       ;
  input  wire [C_M21_AXI_DATA_WIDTH-1:0]         m21_axi_rdata        ;
  input  wire                                    m21_axi_rlast        ;
  // AXI4 master interface m22_axi                                    ;
  output wire                                    m22_axi_awvalid      ;
  input  wire                                    m22_axi_awready      ;
  output wire [C_M22_AXI_ADDR_WIDTH-1:0]         m22_axi_awaddr       ;
  output wire [8-1:0]                            m22_axi_awlen        ;
  output wire                                    m22_axi_wvalid       ;
  input  wire                                    m22_axi_wready       ;
  output wire [C_M22_AXI_DATA_WIDTH-1:0]         m22_axi_wdata        ;
  output wire [C_M22_AXI_DATA_WIDTH/8-1:0]       m22_axi_wstrb        ;
  output wire                                    m22_axi_wlast        ;
  input  wire                                    m22_axi_bvalid       ;
  output wire                                    m22_axi_bready       ;
  output wire                                    m22_axi_arvalid      ;
  input  wire                                    m22_axi_arready      ;
  output wire [C_M22_AXI_ADDR_WIDTH-1:0]         m22_axi_araddr       ;
  output wire [8-1:0]                            m22_axi_arlen        ;
  input  wire                                    m22_axi_rvalid       ;
  output wire                                    m22_axi_rready       ;
  input  wire [C_M22_AXI_DATA_WIDTH-1:0]         m22_axi_rdata        ;
  input  wire                                    m22_axi_rlast        ;
  // AXI4 master interface m23_axi                                    ;
  output wire                                    m23_axi_awvalid      ;
  input  wire                                    m23_axi_awready      ;
  output wire [C_M23_AXI_ADDR_WIDTH-1:0]         m23_axi_awaddr       ;
  output wire [8-1:0]                            m23_axi_awlen        ;
  output wire                                    m23_axi_wvalid       ;
  input  wire                                    m23_axi_wready       ;
  output wire [C_M23_AXI_DATA_WIDTH-1:0]         m23_axi_wdata        ;
  output wire [C_M23_AXI_DATA_WIDTH/8-1:0]       m23_axi_wstrb        ;
  output wire                                    m23_axi_wlast        ;
  input  wire                                    m23_axi_bvalid       ;
  output wire                                    m23_axi_bready       ;
  output wire                                    m23_axi_arvalid      ;
  input  wire                                    m23_axi_arready      ;
  output wire [C_M23_AXI_ADDR_WIDTH-1:0]         m23_axi_araddr       ;
  output wire [8-1:0]                            m23_axi_arlen        ;
  input  wire                                    m23_axi_rvalid       ;
  output wire                                    m23_axi_rready       ;
  input  wire [C_M23_AXI_DATA_WIDTH-1:0]         m23_axi_rdata        ;
  input  wire                                    m23_axi_rlast        ;
  // AXI4 master interface m24_axi                                    ;
  output wire                                    m24_axi_awvalid      ;
  input  wire                                    m24_axi_awready      ;
  output wire [C_M24_AXI_ADDR_WIDTH-1:0]         m24_axi_awaddr       ;
  output wire [8-1:0]                            m24_axi_awlen        ;
  output wire                                    m24_axi_wvalid       ;
  input  wire                                    m24_axi_wready       ;
  output wire [C_M24_AXI_DATA_WIDTH-1:0]         m24_axi_wdata        ;
  output wire [C_M24_AXI_DATA_WIDTH/8-1:0]       m24_axi_wstrb        ;
  output wire                                    m24_axi_wlast        ;
  input  wire                                    m24_axi_bvalid       ;
  output wire                                    m24_axi_bready       ;
  output wire                                    m24_axi_arvalid      ;
  input  wire                                    m24_axi_arready      ;
  output wire [C_M24_AXI_ADDR_WIDTH-1:0]         m24_axi_araddr       ;
  output wire [8-1:0]                            m24_axi_arlen        ;
  input  wire                                    m24_axi_rvalid       ;
  output wire                                    m24_axi_rready       ;
  input  wire [C_M24_AXI_DATA_WIDTH-1:0]         m24_axi_rdata        ;
  input  wire                                    m24_axi_rlast        ;
  // AXI4 master interface m25_axi                                    ;
  output wire                                    m25_axi_awvalid      ;
  input  wire                                    m25_axi_awready      ;
  output wire [C_M25_AXI_ADDR_WIDTH-1:0]         m25_axi_awaddr       ;
  output wire [8-1:0]                            m25_axi_awlen        ;
  output wire                                    m25_axi_wvalid       ;
  input  wire                                    m25_axi_wready       ;
  output wire [C_M25_AXI_DATA_WIDTH-1:0]         m25_axi_wdata        ;
  output wire [C_M25_AXI_DATA_WIDTH/8-1:0]       m25_axi_wstrb        ;
  output wire                                    m25_axi_wlast        ;
  input  wire                                    m25_axi_bvalid       ;
  output wire                                    m25_axi_bready       ;
  output wire                                    m25_axi_arvalid      ;
  input  wire                                    m25_axi_arready      ;
  output wire [C_M25_AXI_ADDR_WIDTH-1:0]         m25_axi_araddr       ;
  output wire [8-1:0]                            m25_axi_arlen        ;
  input  wire                                    m25_axi_rvalid       ;
  output wire                                    m25_axi_rready       ;
  input  wire [C_M25_AXI_DATA_WIDTH-1:0]         m25_axi_rdata        ;
  input  wire                                    m25_axi_rlast        ;
  // AXI4 master interface m26_axi                                    ;
  output wire                                    m26_axi_awvalid      ;
  input  wire                                    m26_axi_awready      ;
  output wire [C_M26_AXI_ADDR_WIDTH-1:0]         m26_axi_awaddr       ;
  output wire [8-1:0]                            m26_axi_awlen        ;
  output wire                                    m26_axi_wvalid       ;
  input  wire                                    m26_axi_wready       ;
  output wire [C_M26_AXI_DATA_WIDTH-1:0]         m26_axi_wdata        ;
  output wire [C_M26_AXI_DATA_WIDTH/8-1:0]       m26_axi_wstrb        ;
  output wire                                    m26_axi_wlast        ;
  input  wire                                    m26_axi_bvalid       ;
  output wire                                    m26_axi_bready       ;
  output wire                                    m26_axi_arvalid      ;
  input  wire                                    m26_axi_arready      ;
  output wire [C_M26_AXI_ADDR_WIDTH-1:0]         m26_axi_araddr       ;
  output wire [8-1:0]                            m26_axi_arlen        ;
  input  wire                                    m26_axi_rvalid       ;
  output wire                                    m26_axi_rready       ;
  input  wire [C_M26_AXI_DATA_WIDTH-1:0]         m26_axi_rdata        ;
  input  wire                                    m26_axi_rlast        ;
  // AXI4 master interface m27_axi                                    ;
  output wire                                    m27_axi_awvalid      ;
  input  wire                                    m27_axi_awready      ;
  output wire [C_M27_AXI_ADDR_WIDTH-1:0]         m27_axi_awaddr       ;
  output wire [8-1:0]                            m27_axi_awlen        ;
  output wire                                    m27_axi_wvalid       ;
  input  wire                                    m27_axi_wready       ;
  output wire [C_M27_AXI_DATA_WIDTH-1:0]         m27_axi_wdata        ;
  output wire [C_M27_AXI_DATA_WIDTH/8-1:0]       m27_axi_wstrb        ;
  output wire                                    m27_axi_wlast        ;
  input  wire                                    m27_axi_bvalid       ;
  output wire                                    m27_axi_bready       ;
  output wire                                    m27_axi_arvalid      ;
  input  wire                                    m27_axi_arready      ;
  output wire [C_M27_AXI_ADDR_WIDTH-1:0]         m27_axi_araddr       ;
  output wire [8-1:0]                            m27_axi_arlen        ;
  input  wire                                    m27_axi_rvalid       ;
  output wire                                    m27_axi_rready       ;
  input  wire [C_M27_AXI_DATA_WIDTH-1:0]         m27_axi_rdata        ;
  input  wire                                    m27_axi_rlast        ;
  // AXI4 master interface m28_axi                                    ;
  output wire                                    m28_axi_awvalid      ;
  input  wire                                    m28_axi_awready      ;
  output wire [C_M28_AXI_ADDR_WIDTH-1:0]         m28_axi_awaddr       ;
  output wire [8-1:0]                            m28_axi_awlen        ;
  output wire                                    m28_axi_wvalid       ;
  input  wire                                    m28_axi_wready       ;
  output wire [C_M28_AXI_DATA_WIDTH-1:0]         m28_axi_wdata        ;
  output wire [C_M28_AXI_DATA_WIDTH/8-1:0]       m28_axi_wstrb        ;
  output wire                                    m28_axi_wlast        ;
  input  wire                                    m28_axi_bvalid       ;
  output wire                                    m28_axi_bready       ;
  output wire                                    m28_axi_arvalid      ;
  input  wire                                    m28_axi_arready      ;
  output wire [C_M28_AXI_ADDR_WIDTH-1:0]         m28_axi_araddr       ;
  output wire [8-1:0]                            m28_axi_arlen        ;
  input  wire                                    m28_axi_rvalid       ;
  output wire                                    m28_axi_rready       ;
  input  wire [C_M28_AXI_DATA_WIDTH-1:0]         m28_axi_rdata        ;
  input  wire                                    m28_axi_rlast        ;
  // AXI4 master interface m29_axi                                    ;
  output wire                                    m29_axi_awvalid      ;
  input  wire                                    m29_axi_awready      ;
  output wire [C_M29_AXI_ADDR_WIDTH-1:0]         m29_axi_awaddr       ;
  output wire [8-1:0]                            m29_axi_awlen        ;
  output wire                                    m29_axi_wvalid       ;
  input  wire                                    m29_axi_wready       ;
  output wire [C_M29_AXI_DATA_WIDTH-1:0]         m29_axi_wdata        ;
  output wire [C_M29_AXI_DATA_WIDTH/8-1:0]       m29_axi_wstrb        ;
  output wire                                    m29_axi_wlast        ;
  input  wire                                    m29_axi_bvalid       ;
  output wire                                    m29_axi_bready       ;
  output wire                                    m29_axi_arvalid      ;
  input  wire                                    m29_axi_arready      ;
  output wire [C_M29_AXI_ADDR_WIDTH-1:0]         m29_axi_araddr       ;
  output wire [8-1:0]                            m29_axi_arlen        ;
  input  wire                                    m29_axi_rvalid       ;
  output wire                                    m29_axi_rready       ;
  input  wire [C_M29_AXI_DATA_WIDTH-1:0]         m29_axi_rdata        ;
  input  wire                                    m29_axi_rlast        ;
  // AXI4 master interface m30_axi                                    ;
  output wire                                    m30_axi_awvalid      ;
  input  wire                                    m30_axi_awready      ;
  output wire [C_M30_AXI_ADDR_WIDTH-1:0]         m30_axi_awaddr       ;
  output wire [8-1:0]                            m30_axi_awlen        ;
  output wire                                    m30_axi_wvalid       ;
  input  wire                                    m30_axi_wready       ;
  output wire [C_M30_AXI_DATA_WIDTH-1:0]         m30_axi_wdata        ;
  output wire [C_M30_AXI_DATA_WIDTH/8-1:0]       m30_axi_wstrb        ;
  output wire                                    m30_axi_wlast        ;
  input  wire                                    m30_axi_bvalid       ;
  output wire                                    m30_axi_bready       ;
  output wire                                    m30_axi_arvalid      ;
  input  wire                                    m30_axi_arready      ;
  output wire [C_M30_AXI_ADDR_WIDTH-1:0]         m30_axi_araddr       ;
  output wire [8-1:0]                            m30_axi_arlen        ;
  input  wire                                    m30_axi_rvalid       ;
  output wire                                    m30_axi_rready       ;
  input  wire [C_M30_AXI_DATA_WIDTH-1:0]         m30_axi_rdata        ;
  input  wire                                    m30_axi_rlast        ;
  // AXI4 master interface m31_axi                                    ;
  output wire                                    m31_axi_awvalid      ;
  input  wire                                    m31_axi_awready      ;
  output wire [C_M31_AXI_ADDR_WIDTH-1:0]         m31_axi_awaddr       ;
  output wire [8-1:0]                            m31_axi_awlen        ;
  output wire                                    m31_axi_wvalid       ;
  input  wire                                    m31_axi_wready       ;
  output wire [C_M31_AXI_DATA_WIDTH-1:0]         m31_axi_wdata        ;
  output wire [C_M31_AXI_DATA_WIDTH/8-1:0]       m31_axi_wstrb        ;
  output wire                                    m31_axi_wlast        ;
  input  wire                                    m31_axi_bvalid       ;
  output wire                                    m31_axi_bready       ;
  output wire                                    m31_axi_arvalid      ;
  input  wire                                    m31_axi_arready      ;
  output wire [C_M31_AXI_ADDR_WIDTH-1:0]         m31_axi_araddr       ;
  output wire [8-1:0]                            m31_axi_arlen        ;
  input  wire                                    m31_axi_rvalid       ;
  output wire                                    m31_axi_rready       ;
  input  wire [C_M31_AXI_DATA_WIDTH-1:0]         m31_axi_rdata        ;
  input  wire                                    m31_axi_rlast        ;
  
  `endif
  
  output wire                                    done                ;

 


     reg done_r = 0;
     reg [C_M00_AXI_DATA_WIDTH - 1:0] WDATA [31:0] ;//=  0;
     reg [32:0 ] WADDR [31:0] ;// = 0; 
     reg [32:0]  RADDR [31:0] ;// = 0;
     reg      [31:0]  rValid  =0 ;// = 0;
     reg     [31:0]   wValid  =0;// = 0;
     reg       [31:0] aValid  = 0;// = 0;
function [3:0] log2(input [4:0] i);
		case (i)
		  1: log2=0;
		  2: log2=1;
		  4: log2=2;
		  8: log2=3;
		  16:log2=4;
		  default:log2=1;
		endcase
endfunction
//localparam integer packet_count =2**5;
reg [63:0] packet_count = 0;//1 << (`packet_count-log2(cmd`burst_len+1'b1));
assign done = done_r;

assign m00_axi_awvalid = AXI_00_AWVALID;//aValid[0];
assign AXI_00_AWREADY  = m00_axi_awready;
assign m00_axi_awaddr  = AXI_00_AWADDR;//WADDR  [0];
assign m00_axi_awlen   = AXI_00_AWLEN;
assign m00_axi_wvalid  = AXI_00_WVALID;//wValid [0];
assign AXI_00_WREADY   = m00_axi_wready;
assign m00_axi_wdata   = AXI_00_WDATA;//WDATA  [0];
assign m00_axi_wstrb   = AXI_00_WSTRB;
assign m00_axi_wlast   = AXI_00_WLAST;
assign AXI_00_BVALID   = m00_axi_bvalid;
assign m00_axi_bready  = AXI_00_BREADY;
assign m00_axi_arvalid = AXI_00_ARVALID;//rValid [0];
assign AXI_00_ARREADY  = m00_axi_arready;
assign m00_axi_araddr  = AXI_00_ARADDR;//RADDR  [0];
assign m00_axi_arlen   = AXI_00_ARLEN;
assign AXI_00_RVALID   = m00_axi_rvalid;
assign m00_axi_rready  = AXI_00_RREADY;
assign AXI_00_RDATA    = m00_axi_rdata;
assign AXI_00_RLAST    = m00_axi_rlast;


`ifdef XBARorM1orM0

assign m01_axi_awvalid = AXI_01_AWVALID;//aValid[0];
assign AXI_01_AWREADY  = m01_axi_awready;
assign m01_axi_awaddr  = AXI_01_AWADDR;//WADDR  [0];
assign m01_axi_awlen   = AXI_01_AWLEN;
assign m01_axi_wvalid  = AXI_01_WVALID;//wValid [0];
assign AXI_01_WREADY   = m01_axi_wready;
assign m01_axi_wdata   = AXI_01_WDATA;//WDATA  [0];
assign m01_axi_wstrb   = AXI_01_WSTRB;
assign m01_axi_wlast   = AXI_01_WLAST;
assign AXI_01_BVALID   = m01_axi_bvalid;
assign m01_axi_bready  = AXI_01_BREADY;
assign m01_axi_arvalid = AXI_01_ARVALID;//rValid [0];
assign AXI_01_ARREADY  = m01_axi_arready;
assign m01_axi_araddr  = AXI_01_ARADDR;//RADDR  [0];
assign m01_axi_arlen   = AXI_01_ARLEN;
assign AXI_01_RVALID   = m01_axi_rvalid;
assign m01_axi_rready  = AXI_01_RREADY;
assign AXI_01_RDATA    = m01_axi_rdata;
assign AXI_01_RLAST    = m01_axi_rlast;

assign m02_axi_awvalid = AXI_02_AWVALID;//aValid[0];
assign AXI_02_AWREADY  = m02_axi_awready;
assign m02_axi_awaddr  = AXI_02_AWADDR;//WADDR  [0];
assign m02_axi_awlen   = AXI_02_AWLEN;
assign m02_axi_wvalid  = AXI_02_WVALID;//wValid [0];
assign AXI_02_WREADY   = m02_axi_wready;
assign m02_axi_wdata   = AXI_02_WDATA;//WDATA  [0];
assign m02_axi_wstrb   = AXI_02_WSTRB;
assign m02_axi_wlast   = AXI_02_WLAST;
assign AXI_02_BVALID   = m02_axi_bvalid;
assign m02_axi_bready  = AXI_02_BREADY;
assign m02_axi_arvalid = AXI_02_ARVALID;//rValid [0];
assign AXI_02_ARREADY  = m02_axi_arready;
assign m02_axi_araddr  = AXI_02_ARADDR;//RADDR  [0];
assign m02_axi_arlen   = AXI_02_ARLEN;
assign AXI_02_RVALID   = m02_axi_rvalid;
assign m02_axi_rready  = AXI_02_RREADY;
assign AXI_02_RDATA    = m02_axi_rdata;
assign AXI_02_RLAST    = m02_axi_rlast;


assign m03_axi_awvalid = AXI_03_AWVALID;//aValid[0];
assign AXI_03_AWREADY  = m03_axi_awready;
assign m03_axi_awaddr  = AXI_03_AWADDR;//WADDR  [0];
assign m03_axi_awlen   = AXI_03_AWLEN;
assign m03_axi_wvalid  = AXI_03_WVALID;//wValid [0];
assign AXI_03_WREADY   = m03_axi_wready;
assign m03_axi_wdata   = AXI_03_WDATA;//WDATA  [0];
assign m03_axi_wstrb   = AXI_03_WSTRB;
assign m03_axi_wlast   = AXI_03_WLAST;
assign AXI_03_BVALID   = m03_axi_bvalid;
assign m03_axi_bready  = AXI_03_BREADY;
assign m03_axi_arvalid = AXI_03_ARVALID;//rValid [0];
assign AXI_03_ARREADY  = m03_axi_arready;
assign m03_axi_araddr  = AXI_03_ARADDR;//RADDR  [0];
assign m03_axi_arlen   = AXI_03_ARLEN;
assign AXI_03_RVALID   = m03_axi_rvalid;
assign m03_axi_rready  = AXI_03_RREADY;
assign AXI_03_RDATA    = m03_axi_rdata;
assign AXI_03_RLAST    = m03_axi_rlast;

`endif
`ifdef XBARorM1


assign m04_axi_awvalid = AXI_04_AWVALID;//aValid[0];
assign AXI_04_AWREADY  = m04_axi_awready;
assign m04_axi_awaddr  = AXI_04_AWADDR;//WADDR  [0];
assign m04_axi_awlen   = AXI_04_AWLEN;
assign m04_axi_wvalid  = AXI_04_WVALID;//wValid [0];
assign AXI_04_WREADY   = m04_axi_wready;
assign m04_axi_wdata   = AXI_04_WDATA;//WDATA  [0];
assign m04_axi_wstrb   = AXI_04_WSTRB;
assign m04_axi_wlast   = AXI_04_WLAST;
assign AXI_04_BVALID   = m04_axi_bvalid;
assign m04_axi_bready  = AXI_04_BREADY;
assign m04_axi_arvalid = AXI_04_ARVALID;//rValid [0];
assign AXI_04_ARREADY  = m04_axi_arready;
assign m04_axi_araddr  = AXI_04_ARADDR;//RADDR  [0];
assign m04_axi_arlen   = AXI_04_ARLEN;
assign AXI_04_RVALID   = m04_axi_rvalid;
assign m04_axi_rready  = AXI_04_RREADY;
assign AXI_04_RDATA    = m04_axi_rdata;
assign AXI_04_RLAST    = m04_axi_rlast;

assign m05_axi_awvalid = AXI_05_AWVALID;//aValid[0];
assign AXI_05_AWREADY  = m05_axi_awready;
assign m05_axi_awaddr  = AXI_05_AWADDR;//WADDR  [0];
assign m05_axi_awlen   = AXI_05_AWLEN;
assign m05_axi_wvalid  = AXI_05_WVALID;//wValid [0];
assign AXI_05_WREADY   = m05_axi_wready;
assign m05_axi_wdata   = AXI_05_WDATA;//WDATA  [0];
assign m05_axi_wstrb   = AXI_05_WSTRB;
assign m05_axi_wlast   = AXI_05_WLAST;
assign AXI_05_BVALID   = m05_axi_bvalid;
assign m05_axi_bready  = AXI_05_BREADY;
assign m05_axi_arvalid = AXI_05_ARVALID;//rValid [0];
assign AXI_05_ARREADY  = m05_axi_arready;
assign m05_axi_araddr  = AXI_05_ARADDR;//RADDR  [0];
assign m05_axi_arlen   = AXI_05_ARLEN;
assign AXI_05_RVALID   = m05_axi_rvalid;
assign m05_axi_rready  = AXI_05_RREADY;
assign AXI_05_RDATA    = m05_axi_rdata;
assign AXI_05_RLAST    = m05_axi_rlast;

assign m06_axi_awvalid = AXI_06_AWVALID;//aValid[0];
assign AXI_06_AWREADY  = m06_axi_awready;
assign m06_axi_awaddr  = AXI_06_AWADDR;//WADDR  [0];
assign m06_axi_awlen   = AXI_06_AWLEN;
assign m06_axi_wvalid  = AXI_06_WVALID;//wValid [0];
assign AXI_06_WREADY   = m06_axi_wready;
assign m06_axi_wdata   = AXI_06_WDATA;//WDATA  [0];
assign m06_axi_wstrb   = AXI_06_WSTRB;
assign m06_axi_wlast   = AXI_06_WLAST;
assign AXI_06_BVALID   = m06_axi_bvalid;
assign m06_axi_bready  = AXI_06_BREADY;
assign m06_axi_arvalid = AXI_06_ARVALID;//rValid [0];
assign AXI_06_ARREADY  = m06_axi_arready;
assign m06_axi_araddr  = AXI_06_ARADDR;//RADDR  [0];
assign m06_axi_arlen   = AXI_06_ARLEN;
assign AXI_06_RVALID   = m06_axi_rvalid;
assign m06_axi_rready  = AXI_06_RREADY;
assign AXI_06_RDATA    = m06_axi_rdata;
assign AXI_06_RLAST    = m06_axi_rlast;


assign m07_axi_awvalid = AXI_07_AWVALID;//aValid[0];
assign AXI_07_AWREADY  = m07_axi_awready;
assign m07_axi_awaddr  = AXI_07_AWADDR;//WADDR  [0];
assign m07_axi_awlen   = AXI_07_AWLEN;
assign m07_axi_wvalid  = AXI_07_WVALID;//wValid [0];
assign AXI_07_WREADY   = m07_axi_wready;
assign m07_axi_wdata   = AXI_07_WDATA;//WDATA  [0];
assign m07_axi_wstrb   = AXI_07_WSTRB;
assign m07_axi_wlast   = AXI_07_WLAST;
assign AXI_07_BVALID   = m07_axi_bvalid;
assign m07_axi_bready  = AXI_07_BREADY;
assign m07_axi_arvalid = AXI_07_ARVALID;//rValid [0];
assign AXI_07_ARREADY  = m07_axi_arready;
assign m07_axi_araddr  = AXI_07_ARADDR;//RADDR  [0];
assign m07_axi_arlen   = AXI_07_ARLEN;
assign AXI_07_RVALID   = m07_axi_rvalid;
assign m07_axi_rready  = AXI_07_RREADY;
assign AXI_07_RDATA    = m07_axi_rdata;
assign AXI_07_RLAST    = m07_axi_rlast;

`endif
`ifdef XBARorM0 



assign m08_axi_awvalid = AXI_08_AWVALID;//aValid[0];
assign AXI_08_AWREADY  = m08_axi_awready;
assign m08_axi_awaddr  = AXI_08_AWADDR;//WADDR  [0];
assign m08_axi_awlen   = AXI_08_AWLEN;
assign m08_axi_wvalid  = AXI_08_WVALID;//wValid [0];
assign AXI_08_WREADY   = m08_axi_wready;
assign m08_axi_wdata   = AXI_08_WDATA;//WDATA  [0];
assign m08_axi_wstrb   = AXI_08_WSTRB;
assign m08_axi_wlast   = AXI_08_WLAST;
assign AXI_08_BVALID   = m08_axi_bvalid;
assign m08_axi_bready  = AXI_08_BREADY;
assign m08_axi_arvalid = AXI_08_ARVALID;//rValid [0];
assign AXI_08_ARREADY  = m08_axi_arready;
assign m08_axi_araddr  = AXI_08_ARADDR;//RADDR  [0];
assign m08_axi_arlen   = AXI_08_ARLEN;
assign AXI_08_RVALID   = m08_axi_rvalid;
assign m08_axi_rready  = AXI_08_RREADY;
assign AXI_08_RDATA    = m08_axi_rdata;
assign AXI_08_RLAST    = m08_axi_rlast;

assign m09_axi_awvalid = AXI_09_AWVALID;//aValid[0];
assign AXI_09_AWREADY  = m09_axi_awready;
assign m09_axi_awaddr  = AXI_09_AWADDR;//WADDR  [0];
assign m09_axi_awlen   = AXI_09_AWLEN;
assign m09_axi_wvalid  = AXI_09_WVALID;//wValid [0];
assign AXI_09_WREADY   = m09_axi_wready;
assign m09_axi_wdata   = AXI_09_WDATA;//WDATA  [0];
assign m09_axi_wstrb   = AXI_09_WSTRB;
assign m09_axi_wlast   = AXI_09_WLAST;
assign AXI_09_BVALID   = m09_axi_bvalid;
assign m09_axi_bready  = AXI_09_BREADY;
assign m09_axi_arvalid = AXI_09_ARVALID;//rValid [0];
assign AXI_09_ARREADY  = m09_axi_arready;
assign m09_axi_araddr  = AXI_09_ARADDR;//RADDR  [0];
assign m09_axi_arlen   = AXI_09_ARLEN;
assign AXI_09_RVALID   = m09_axi_rvalid;
assign m09_axi_rready  = AXI_09_RREADY;
assign AXI_09_RDATA    = m09_axi_rdata;
assign AXI_09_RLAST    = m09_axi_rlast;

assign m10_axi_awvalid = AXI_10_AWVALID;//aValid[0];
assign AXI_10_AWREADY  = m10_axi_awready;
assign m10_axi_awaddr  = AXI_10_AWADDR;//WADDR  [0];
assign m10_axi_awlen   = AXI_10_AWLEN;
assign m10_axi_wvalid  = AXI_10_WVALID;//wValid [0];
assign AXI_10_WREADY   = m10_axi_wready;
assign m10_axi_wdata   = AXI_10_WDATA;//WDATA  [0];
assign m10_axi_wstrb   = AXI_10_WSTRB;
assign m10_axi_wlast   = AXI_10_WLAST;
assign AXI_10_BVALID   = m10_axi_bvalid;
assign m10_axi_bready  = AXI_10_BREADY;
assign m10_axi_arvalid = AXI_10_ARVALID;//rValid [0];
assign AXI_10_ARREADY  = m10_axi_arready;
assign m10_axi_araddr  = AXI_10_ARADDR;//RADDR  [0];
assign m10_axi_arlen   = AXI_10_ARLEN;
assign AXI_10_RVALID   = m10_axi_rvalid;
assign m10_axi_rready  = AXI_10_RREADY;
assign AXI_10_RDATA    = m10_axi_rdata;
assign AXI_10_RLAST    = m10_axi_rlast;


assign m11_axi_awvalid = AXI_11_AWVALID;//aValid[0];
assign AXI_11_AWREADY  = m11_axi_awready;
assign m11_axi_awaddr  = AXI_11_AWADDR;//WADDR  [0];
assign m11_axi_awlen   = AXI_11_AWLEN;
assign m11_axi_wvalid  = AXI_11_WVALID;//wValid [0];
assign AXI_11_WREADY   = m11_axi_wready;
assign m11_axi_wdata   = AXI_11_WDATA;//WDATA  [0];
assign m11_axi_wstrb   = AXI_11_WSTRB;
assign m11_axi_wlast   = AXI_11_WLAST;
assign AXI_11_BVALID   = m11_axi_bvalid;
assign m11_axi_bready  = AXI_11_BREADY;
assign m11_axi_arvalid = AXI_11_ARVALID;//rValid [0];
assign AXI_11_ARREADY  = m11_axi_arready;
assign m11_axi_araddr  = AXI_11_ARADDR;//RADDR  [0];
assign m11_axi_arlen   = AXI_11_ARLEN;
assign AXI_11_RVALID   = m11_axi_rvalid;
assign m11_axi_rready  = AXI_11_RREADY;
assign AXI_11_RDATA    = m11_axi_rdata;
assign AXI_11_RLAST    = m11_axi_rlast;

`endif
`ifdef XBAR 


assign m12_axi_awvalid = AXI_12_AWVALID;//aValid[0];
assign AXI_12_AWREADY  = m12_axi_awready;
assign m12_axi_awaddr  = AXI_12_AWADDR;//WADDR  [0];
assign m12_axi_awlen   = AXI_12_AWLEN;
assign m12_axi_wvalid  = AXI_12_WVALID;//wValid [0];
assign AXI_12_WREADY   = m12_axi_wready;
assign m12_axi_wdata   = AXI_12_WDATA;//WDATA  [0];
assign m12_axi_wstrb   = AXI_12_WSTRB;
assign m12_axi_wlast   = AXI_12_WLAST;
assign AXI_12_BVALID   = m12_axi_bvalid;
assign m12_axi_bready  = AXI_12_BREADY;
assign m12_axi_arvalid = AXI_12_ARVALID;//rValid [0];
assign AXI_12_ARREADY  = m12_axi_arready;
assign m12_axi_araddr  = AXI_12_ARADDR;//RADDR  [0];
assign m12_axi_arlen   = AXI_12_ARLEN;
assign AXI_12_RVALID   = m12_axi_rvalid;
assign m12_axi_rready  = AXI_12_RREADY;
assign AXI_12_RDATA    = m12_axi_rdata;
assign AXI_12_RLAST    = m12_axi_rlast;

assign m13_axi_awvalid = AXI_13_AWVALID;//aValid[0];
assign AXI_13_AWREADY  = m13_axi_awready;
assign m13_axi_awaddr  = AXI_13_AWADDR;//WADDR  [0];
assign m13_axi_awlen   = AXI_13_AWLEN;
assign m13_axi_wvalid  = AXI_13_WVALID;//wValid [0];
assign AXI_13_WREADY   = m13_axi_wready;
assign m13_axi_wdata   = AXI_13_WDATA;//WDATA  [0];
assign m13_axi_wstrb   = AXI_13_WSTRB;
assign m13_axi_wlast   = AXI_13_WLAST;
assign AXI_13_BVALID   = m13_axi_bvalid;
assign m13_axi_bready  = AXI_13_BREADY;
assign m13_axi_arvalid = AXI_13_ARVALID;//rValid [0];
assign AXI_13_ARREADY  = m13_axi_arready;
assign m13_axi_araddr  = AXI_13_ARADDR;//RADDR  [0];
assign m13_axi_arlen   = AXI_13_ARLEN;
assign AXI_13_RVALID   = m13_axi_rvalid;
assign m13_axi_rready  = AXI_13_RREADY;
assign AXI_13_RDATA    = m13_axi_rdata;
assign AXI_13_RLAST    = m13_axi_rlast;

assign m14_axi_awvalid = AXI_14_AWVALID;//aValid[0];
assign AXI_14_AWREADY  = m14_axi_awready;
assign m14_axi_awaddr  = AXI_14_AWADDR;//WADDR  [0];
assign m14_axi_awlen   = AXI_14_AWLEN;
assign m14_axi_wvalid  = AXI_14_WVALID;//wValid [0];
assign AXI_14_WREADY   = m14_axi_wready;
assign m14_axi_wdata   = AXI_14_WDATA;//WDATA  [0];
assign m14_axi_wstrb   = AXI_14_WSTRB;
assign m14_axi_wlast   = AXI_14_WLAST;
assign AXI_14_BVALID   = m14_axi_bvalid;
assign m14_axi_bready  = AXI_14_BREADY;
assign m14_axi_arvalid = AXI_14_ARVALID;//rValid [0];
assign AXI_14_ARREADY  = m14_axi_arready;
assign m14_axi_araddr  = AXI_14_ARADDR;//RADDR  [0];
assign m14_axi_arlen   = AXI_14_ARLEN;
assign AXI_14_RVALID   = m14_axi_rvalid;
assign m14_axi_rready  = AXI_14_RREADY;
assign AXI_14_RDATA    = m14_axi_rdata;
assign AXI_14_RLAST    = m14_axi_rlast;


assign m15_axi_awvalid = AXI_15_AWVALID;//aValid[0];
assign AXI_15_AWREADY  = m15_axi_awready;
assign m15_axi_awaddr  = AXI_15_AWADDR;//WADDR  [0];
assign m15_axi_awlen   = AXI_15_AWLEN;
assign m15_axi_wvalid  = AXI_15_WVALID;//wValid [0];
assign AXI_15_WREADY   = m15_axi_wready;
assign m15_axi_wdata   = AXI_15_WDATA;//WDATA  [0];
assign m15_axi_wstrb   = AXI_15_WSTRB;
assign m15_axi_wlast   = AXI_15_WLAST;
assign AXI_15_BVALID   = m15_axi_bvalid;
assign m15_axi_bready  = AXI_15_BREADY;
assign m15_axi_arvalid = AXI_15_ARVALID;//rValid [0];
assign AXI_15_ARREADY  = m15_axi_arready;
assign m15_axi_araddr  = AXI_15_ARADDR;//RADDR  [0];
assign m15_axi_arlen   = AXI_15_ARLEN;
assign AXI_15_RVALID   = m15_axi_rvalid;
assign m15_axi_rready  = AXI_15_RREADY;
assign AXI_15_RDATA    = m15_axi_rdata;
assign AXI_15_RLAST    = m15_axi_rlast;

assign m16_axi_awvalid = AXI_16_AWVALID;//aValid[0];
assign AXI_16_AWREADY  = m16_axi_awready;
assign m16_axi_awaddr  = AXI_16_AWADDR;//WADDR  [0];
assign m16_axi_awlen   = AXI_16_AWLEN;
assign m16_axi_wvalid  = AXI_16_WVALID;//wValid [0];
assign AXI_16_WREADY   = m16_axi_wready;
assign m16_axi_wdata   = AXI_16_WDATA;//WDATA  [0];
assign m16_axi_wstrb   = AXI_16_WSTRB;
assign m16_axi_wlast   = AXI_16_WLAST;
assign AXI_16_BVALID   = m16_axi_bvalid;
assign m16_axi_bready  = AXI_16_BREADY;
assign m16_axi_arvalid = AXI_16_ARVALID;//rValid [0];
assign AXI_16_ARREADY  = m16_axi_arready;
assign m16_axi_araddr  = AXI_16_ARADDR;//RADDR  [0];
assign m16_axi_arlen   = AXI_16_ARLEN;
assign AXI_16_RVALID   = m16_axi_rvalid;
assign m16_axi_rready  = AXI_16_RREADY;
assign AXI_16_RDATA    = m16_axi_rdata;
assign AXI_16_RLAST    = m16_axi_rlast;

assign m17_axi_awvalid = AXI_17_AWVALID;//aValid[0];
assign AXI_17_AWREADY  = m17_axi_awready;
assign m17_axi_awaddr  = AXI_17_AWADDR;//WADDR  [0];
assign m17_axi_awlen   = AXI_17_AWLEN;
assign m17_axi_wvalid  = AXI_17_WVALID;//wValid [0];
assign AXI_17_WREADY   = m17_axi_wready;
assign m17_axi_wdata   = AXI_17_WDATA;//WDATA  [0];
assign m17_axi_wstrb   = AXI_17_WSTRB;
assign m17_axi_wlast   = AXI_17_WLAST;
assign AXI_17_BVALID   = m17_axi_bvalid;
assign m17_axi_bready  = AXI_17_BREADY;
assign m17_axi_arvalid = AXI_17_ARVALID;//rValid [0];
assign AXI_17_ARREADY  = m17_axi_arready;
assign m17_axi_araddr  = AXI_17_ARADDR;//RADDR  [0];
assign m17_axi_arlen   = AXI_17_ARLEN;
assign AXI_17_RVALID   = m17_axi_rvalid;
assign m17_axi_rready  = AXI_17_RREADY;
assign AXI_17_RDATA    = m17_axi_rdata;
assign AXI_17_RLAST    = m17_axi_rlast;

assign m18_axi_awvalid = AXI_18_AWVALID;//aValid[0];
assign AXI_18_AWREADY  = m18_axi_awready;
assign m18_axi_awaddr  = AXI_18_AWADDR;//WADDR  [0];
assign m18_axi_awlen   = AXI_18_AWLEN;
assign m18_axi_wvalid  = AXI_18_WVALID;//wValid [0];
assign AXI_18_WREADY   = m18_axi_wready;
assign m18_axi_wdata   = AXI_18_WDATA;//WDATA  [0];
assign m18_axi_wstrb   = AXI_18_WSTRB;
assign m18_axi_wlast   = AXI_18_WLAST;
assign AXI_18_BVALID   = m18_axi_bvalid;
assign m18_axi_bready  = AXI_18_BREADY;
assign m18_axi_arvalid = AXI_18_ARVALID;//rValid [0];
assign AXI_18_ARREADY  = m18_axi_arready;
assign m18_axi_araddr  = AXI_18_ARADDR;//RADDR  [0];
assign m18_axi_arlen   = AXI_18_ARLEN;
assign AXI_18_RVALID   = m18_axi_rvalid;
assign m18_axi_rready  = AXI_18_RREADY;
assign AXI_18_RDATA    = m18_axi_rdata;
assign AXI_18_RLAST    = m18_axi_rlast;


assign m19_axi_awvalid = AXI_19_AWVALID;//aValid[0];
assign AXI_19_AWREADY  = m19_axi_awready;
assign m19_axi_awaddr  = AXI_19_AWADDR;//WADDR  [0];
assign m19_axi_awlen   = AXI_19_AWLEN;
assign m19_axi_wvalid  = AXI_19_WVALID;//wValid [0];
assign AXI_19_WREADY   = m19_axi_wready;
assign m19_axi_wdata   = AXI_19_WDATA;//WDATA  [0];
assign m19_axi_wstrb   = AXI_19_WSTRB;
assign m19_axi_wlast   = AXI_19_WLAST;
assign AXI_19_BVALID   = m19_axi_bvalid;
assign m19_axi_bready  = AXI_19_BREADY;
assign m19_axi_arvalid = AXI_19_ARVALID;//rValid [0];
assign AXI_19_ARREADY  = m19_axi_arready;
assign m19_axi_araddr  = AXI_19_ARADDR;//RADDR  [0];
assign m19_axi_arlen   = AXI_19_ARLEN;
assign AXI_19_RVALID   = m19_axi_rvalid;
assign m19_axi_rready  = AXI_19_RREADY;
assign AXI_19_RDATA    = m19_axi_rdata;
assign AXI_19_RLAST    = m19_axi_rlast;



assign m20_axi_awvalid = AXI_20_AWVALID;//aValid[0];
assign AXI_20_AWREADY  = m20_axi_awready;
assign m20_axi_awaddr  = AXI_20_AWADDR;//WADDR  [0];
assign m20_axi_awlen   = AXI_20_AWLEN;
assign m20_axi_wvalid  = AXI_20_WVALID;//wValid [0];
assign AXI_20_WREADY   = m20_axi_wready;
assign m20_axi_wdata   = AXI_20_WDATA;//WDATA  [0];
assign m20_axi_wstrb   = AXI_20_WSTRB;
assign m20_axi_wlast   = AXI_20_WLAST;
assign AXI_20_BVALID   = m20_axi_bvalid;
assign m20_axi_bready  = AXI_20_BREADY;
assign m20_axi_arvalid = AXI_20_ARVALID;//rValid [0];
assign AXI_20_ARREADY  = m20_axi_arready;
assign m20_axi_araddr  = AXI_20_ARADDR;//RADDR  [0];
assign m20_axi_arlen   = AXI_20_ARLEN;
assign AXI_20_RVALID   = m20_axi_rvalid;
assign m20_axi_rready  = AXI_20_RREADY;
assign AXI_20_RDATA    = m20_axi_rdata;
assign AXI_20_RLAST    = m20_axi_rlast;

assign m21_axi_awvalid = AXI_21_AWVALID;//aValid[0];
assign AXI_21_AWREADY  = m21_axi_awready;
assign m21_axi_awaddr  = AXI_21_AWADDR;//WADDR  [0];
assign m21_axi_awlen   = AXI_21_AWLEN;
assign m21_axi_wvalid  = AXI_21_WVALID;//wValid [0];
assign AXI_21_WREADY   = m21_axi_wready;
assign m21_axi_wdata   = AXI_21_WDATA;//WDATA  [0];
assign m21_axi_wstrb   = AXI_21_WSTRB;
assign m21_axi_wlast   = AXI_21_WLAST;
assign AXI_21_BVALID   = m21_axi_bvalid;
assign m21_axi_bready  = AXI_21_BREADY;
assign m21_axi_arvalid = AXI_21_ARVALID;//rValid [0];
assign AXI_21_ARREADY  = m21_axi_arready;
assign m21_axi_araddr  = AXI_21_ARADDR;//RADDR  [0];
assign m21_axi_arlen   = AXI_21_ARLEN;
assign AXI_21_RVALID   = m21_axi_rvalid;
assign m21_axi_rready  = AXI_21_RREADY;
assign AXI_21_RDATA    = m21_axi_rdata;
assign AXI_21_RLAST    = m21_axi_rlast;

assign m22_axi_awvalid = AXI_22_AWVALID;//aValid[0];
assign AXI_22_AWREADY  = m22_axi_awready;
assign m22_axi_awaddr  = AXI_22_AWADDR;//WADDR  [0];
assign m22_axi_awlen   = AXI_22_AWLEN;
assign m22_axi_wvalid  = AXI_22_WVALID;//wValid [0];
assign AXI_22_WREADY   = m22_axi_wready;
assign m22_axi_wdata   = AXI_22_WDATA;//WDATA  [0];
assign m22_axi_wstrb   = AXI_22_WSTRB;
assign m22_axi_wlast   = AXI_22_WLAST;
assign AXI_22_BVALID   = m22_axi_bvalid;
assign m22_axi_bready  = AXI_22_BREADY;
assign m22_axi_arvalid = AXI_22_ARVALID;//rValid [0];
assign AXI_22_ARREADY  = m22_axi_arready;
assign m22_axi_araddr  = AXI_22_ARADDR;//RADDR  [0];
assign m22_axi_arlen   = AXI_22_ARLEN;
assign AXI_22_RVALID   = m22_axi_rvalid;
assign m22_axi_rready  = AXI_22_RREADY;
assign AXI_22_RDATA    = m22_axi_rdata;
assign AXI_22_RLAST    = m22_axi_rlast;


assign m23_axi_awvalid = AXI_23_AWVALID;//aValid[0];
assign AXI_23_AWREADY  = m23_axi_awready;
assign m23_axi_awaddr  = AXI_23_AWADDR;//WADDR  [0];
assign m23_axi_awlen   = AXI_23_AWLEN;
assign m23_axi_wvalid  = AXI_23_WVALID;//wValid [0];
assign AXI_23_WREADY   = m23_axi_wready;
assign m23_axi_wdata   = AXI_23_WDATA;//WDATA  [0];
assign m23_axi_wstrb   = AXI_23_WSTRB;
assign m23_axi_wlast   = AXI_23_WLAST;
assign AXI_23_BVALID   = m23_axi_bvalid;
assign m23_axi_bready  = AXI_23_BREADY;
assign m23_axi_arvalid = AXI_23_ARVALID;//rValid [0];
assign AXI_23_ARREADY  = m23_axi_arready;
assign m23_axi_araddr  = AXI_23_ARADDR;//RADDR  [0];
assign m23_axi_arlen   = AXI_23_ARLEN;
assign AXI_23_RVALID   = m23_axi_rvalid;
assign m23_axi_rready  = AXI_23_RREADY;
assign AXI_23_RDATA    = m23_axi_rdata;
assign AXI_23_RLAST    = m23_axi_rlast;

assign m24_axi_awvalid = AXI_24_AWVALID;//aValid[0];
assign AXI_24_AWREADY  = m24_axi_awready;
assign m24_axi_awaddr  = AXI_24_AWADDR;//WADDR  [0];
assign m24_axi_awlen   = AXI_24_AWLEN;
assign m24_axi_wvalid  = AXI_24_WVALID;//wValid [0];
assign AXI_24_WREADY   = m24_axi_wready;
assign m24_axi_wdata   = AXI_24_WDATA;//WDATA  [0];
assign m24_axi_wstrb   = AXI_24_WSTRB;
assign m24_axi_wlast   = AXI_24_WLAST;
assign AXI_24_BVALID   = m24_axi_bvalid;
assign m24_axi_bready  = AXI_24_BREADY;
assign m24_axi_arvalid = AXI_24_ARVALID;//rValid [0];
assign AXI_24_ARREADY  = m24_axi_arready;
assign m24_axi_araddr  = AXI_24_ARADDR;//RADDR  [0];
assign m24_axi_arlen   = AXI_24_ARLEN;
assign AXI_24_RVALID   = m24_axi_rvalid;
assign m24_axi_rready  = AXI_24_RREADY;
assign AXI_24_RDATA    = m24_axi_rdata;
assign AXI_24_RLAST    = m24_axi_rlast;

assign m25_axi_awvalid = AXI_25_AWVALID;//aValid[0];
assign AXI_25_AWREADY  = m25_axi_awready;
assign m25_axi_awaddr  = AXI_25_AWADDR;//WADDR  [0];
assign m25_axi_awlen   = AXI_25_AWLEN;
assign m25_axi_wvalid  = AXI_25_WVALID;//wValid [0];
assign AXI_25_WREADY   = m25_axi_wready;
assign m25_axi_wdata   = AXI_25_WDATA;//WDATA  [0];
assign m25_axi_wstrb   = AXI_25_WSTRB;
assign m25_axi_wlast   = AXI_25_WLAST;
assign AXI_25_BVALID   = m25_axi_bvalid;
assign m25_axi_bready  = AXI_25_BREADY;
assign m25_axi_arvalid = AXI_25_ARVALID;//rValid [0];
assign AXI_25_ARREADY  = m25_axi_arready;
assign m25_axi_araddr  = AXI_25_ARADDR;//RADDR  [0];
assign m25_axi_arlen   = AXI_25_ARLEN;
assign AXI_25_RVALID   = m25_axi_rvalid;
assign m25_axi_rready  = AXI_25_RREADY;
assign AXI_25_RDATA    = m25_axi_rdata;
assign AXI_25_RLAST    = m25_axi_rlast;

assign m26_axi_awvalid = AXI_26_AWVALID;//aValid[0];
assign AXI_26_AWREADY  = m26_axi_awready;
assign m26_axi_awaddr  = AXI_26_AWADDR;//WADDR  [0];
assign m26_axi_awlen   = AXI_26_AWLEN;
assign m26_axi_wvalid  = AXI_26_WVALID;//wValid [0];
assign AXI_26_WREADY   = m26_axi_wready;
assign m26_axi_wdata   = AXI_26_WDATA;//WDATA  [0];
assign m26_axi_wstrb   = AXI_26_WSTRB;
assign m26_axi_wlast   = AXI_26_WLAST;
assign AXI_26_BVALID   = m26_axi_bvalid;
assign m26_axi_bready  = AXI_26_BREADY;
assign m26_axi_arvalid = AXI_26_ARVALID;//rValid [0];
assign AXI_26_ARREADY  = m26_axi_arready;
assign m26_axi_araddr  = AXI_26_ARADDR;//RADDR  [0];
assign m26_axi_arlen   = AXI_26_ARLEN;
assign AXI_26_RVALID   = m26_axi_rvalid;
assign m26_axi_rready  = AXI_26_RREADY;
assign AXI_26_RDATA    = m26_axi_rdata;
assign AXI_26_RLAST    = m26_axi_rlast;


assign m27_axi_awvalid = AXI_27_AWVALID;//aValid[0];
assign AXI_27_AWREADY  = m27_axi_awready;
assign m27_axi_awaddr  = AXI_27_AWADDR;//WADDR  [0];
assign m27_axi_awlen   = AXI_27_AWLEN;
assign m27_axi_wvalid  = AXI_27_WVALID;//wValid [0];
assign AXI_27_WREADY   = m27_axi_wready;
assign m27_axi_wdata   = AXI_27_WDATA;//WDATA  [0];
assign m27_axi_wstrb   = AXI_27_WSTRB;
assign m27_axi_wlast   = AXI_27_WLAST;
assign AXI_27_BVALID   = m27_axi_bvalid;
assign m27_axi_bready  = AXI_27_BREADY;
assign m27_axi_arvalid = AXI_27_ARVALID;//rValid [0];
assign AXI_27_ARREADY  = m27_axi_arready;
assign m27_axi_araddr  = AXI_27_ARADDR;//RADDR  [0];
assign m27_axi_arlen   = AXI_27_ARLEN;
assign AXI_27_RVALID   = m27_axi_rvalid;
assign m27_axi_rready  = AXI_27_RREADY;
assign AXI_27_RDATA    = m27_axi_rdata;
assign AXI_27_RLAST    = m27_axi_rlast;



assign m28_axi_awvalid = AXI_28_AWVALID;//aValid[0];
assign AXI_28_AWREADY  = m28_axi_awready;
assign m28_axi_awaddr  = AXI_28_AWADDR;//WADDR  [0];
assign m28_axi_awlen   = AXI_28_AWLEN;
assign m28_axi_wvalid  = AXI_28_WVALID;//wValid [0];
assign AXI_28_WREADY   = m28_axi_wready;
assign m28_axi_wdata   = AXI_28_WDATA;//WDATA  [0];
assign m28_axi_wstrb   = AXI_28_WSTRB;
assign m28_axi_wlast   = AXI_28_WLAST;
assign AXI_28_BVALID   = m28_axi_bvalid;
assign m28_axi_bready  = AXI_28_BREADY;
assign m28_axi_arvalid = AXI_28_ARVALID;//rValid [0];
assign AXI_28_ARREADY  = m28_axi_arready;
assign m28_axi_araddr  = AXI_28_ARADDR;//RADDR  [0];
assign m28_axi_arlen   = AXI_28_ARLEN;
assign AXI_28_RVALID   = m28_axi_rvalid;
assign m28_axi_rready  = AXI_28_RREADY;
assign AXI_28_RDATA    = m28_axi_rdata;
assign AXI_28_RLAST    = m28_axi_rlast;

assign m29_axi_awvalid = AXI_29_AWVALID;//aValid[0];
assign AXI_29_AWREADY  = m29_axi_awready;
assign m29_axi_awaddr  = AXI_29_AWADDR;//WADDR  [0];
assign m29_axi_awlen   = AXI_29_AWLEN;
assign m29_axi_wvalid  = AXI_29_WVALID;//wValid [0];
assign AXI_29_WREADY   = m29_axi_wready;
assign m29_axi_wdata   = AXI_29_WDATA;//WDATA  [0];
assign m29_axi_wstrb   = AXI_29_WSTRB;
assign m29_axi_wlast   = AXI_29_WLAST;
assign AXI_29_BVALID   = m29_axi_bvalid;
assign m29_axi_bready  = AXI_29_BREADY;
assign m29_axi_arvalid = AXI_29_ARVALID;//rValid [0];
assign AXI_29_ARREADY  = m29_axi_arready;
assign m29_axi_araddr  = AXI_29_ARADDR;//RADDR  [0];
assign m29_axi_arlen   = AXI_29_ARLEN;
assign AXI_29_RVALID   = m29_axi_rvalid;
assign m29_axi_rready  = AXI_29_RREADY;
assign AXI_29_RDATA    = m29_axi_rdata;
assign AXI_29_RLAST    = m29_axi_rlast;

assign m30_axi_awvalid = AXI_30_AWVALID;//aValid[0];
assign AXI_30_AWREADY  = m30_axi_awready;
assign m30_axi_awaddr  = AXI_30_AWADDR;//WADDR  [0];
assign m30_axi_awlen   = AXI_30_AWLEN;
assign m30_axi_wvalid  = AXI_30_WVALID;//wValid [0];
assign AXI_30_WREADY   = m30_axi_wready;
assign m30_axi_wdata   = AXI_30_WDATA;//WDATA  [0];
assign m30_axi_wstrb   = AXI_30_WSTRB;
assign m30_axi_wlast   = AXI_30_WLAST;
assign AXI_30_BVALID   = m30_axi_bvalid;
assign m30_axi_bready  = AXI_30_BREADY;
assign m30_axi_arvalid = AXI_30_ARVALID;//rValid [0];
assign AXI_30_ARREADY  = m30_axi_arready;
assign m30_axi_araddr  = AXI_30_ARADDR;//RADDR  [0];
assign m30_axi_arlen   = AXI_30_ARLEN;
assign AXI_30_RVALID   = m30_axi_rvalid;
assign m30_axi_rready  = AXI_30_RREADY;
assign AXI_30_RDATA    = m30_axi_rdata;
assign AXI_30_RLAST    = m30_axi_rlast;


assign m31_axi_awvalid = AXI_31_AWVALID;//aValid[0];
assign AXI_31_AWREADY  = m31_axi_awready;
assign m31_axi_awaddr  = AXI_31_AWADDR;//WADDR  [0];
assign m31_axi_awlen   = AXI_31_AWLEN;
assign m31_axi_wvalid  = AXI_31_WVALID;//wValid [0];
assign AXI_31_WREADY   = m31_axi_wready;
assign m31_axi_wdata   = AXI_31_WDATA;//WDATA  [0];
assign m31_axi_wstrb   = AXI_31_WSTRB;
assign m31_axi_wlast   = AXI_31_WLAST;
assign AXI_31_BVALID   = m31_axi_bvalid;
assign m31_axi_bready  = AXI_31_BREADY;
assign m31_axi_arvalid = AXI_31_ARVALID;//rValid [0];
assign AXI_31_ARREADY  = m31_axi_arready;
assign m31_axi_araddr  = AXI_31_ARADDR;//RADDR  [0];
assign m31_axi_arlen   = AXI_31_ARLEN;
assign AXI_31_RVALID   = m31_axi_rvalid;
assign m31_axi_rready  = AXI_31_RREADY;
assign AXI_31_RDATA    = m31_axi_rdata;
assign AXI_31_RLAST    = m31_axi_rlast;

`endif
reg ce_r = 0;
   integer i;
   always @(posedge AXI_ACLK) begin
    if(AXI_ARESET_N) begin
    
    writeCount_add_4 <= 0;
    writeCount_add_3[0] <= 0;
    writeCount_add_2[0] <= 0;
    writeCount_add_1[0] <= 0;
    writeCount_add_0[0]<=0;

    writeCount_add_3[1] <= 0;
    writeCount_add_2[1] <= 0;
    writeCount_add_1[1] <= 0;
    writeCount_add_0[1]<=0;
  for(i=0;i<32;i=i+1) begin
            writeCount[i] <= 0;
                    
        end
 for(i=0;i<32;i=i+1) begin
        readCount[i] <= 0;
         
        end
         packet_count <= 0;
         WDATA  [0]         <= 0;
         WADDR  [0]         <= 0;
         RADDR  [0]         <= 0;
         aValid [0]         <= 0;
         wValid [0]         <= 0;
         rValid [0]         <= 0;
          
         AXI_00_AWBURST      <= 2'b01; 
//         AXI_00_AWLEN        <= 4'd15;  
         AXI_00_AWSIZE       <= 3'b101;
         AXI_00_AWID         <= 6'b0;  
         
  
//         AXI_00_WLAST        <= 1'b1;         
         AXI_00_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_00_WDATA_PARITY <= 32'b0; 
         
         AXI_00_BREADY       <=1'b1;     

         AXI_00_ARBURST     <= 2'b01; 
//         AXI_00_ARLEN       <= 4'b0;  
         AXI_00_ARSIZE      <= 3'b101;
         AXI_00_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [1]         <= 0;
         WADDR  [1]         <= 0;
         RADDR  [1]         <= 0;
         aValid [1]         <= 0;
         wValid [1]         <= 0;
         rValid [1]         <= 0;
         
        `ifdef XBARorM1orM0

         AXI_01_AWBURST      <= 2'b01;   
//         AXI_01_AWLEN        <= 4'd15;    
         AXI_01_AWSIZE       <= 3'b101;  
         AXI_01_AWID         <= 6'b0;    
                                        
                                        
//         AXI_01_WLAST        <= 1'b1;    
         AXI_01_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF; 
         AXI_01_WDATA_PARITY <= 32'b0;   
                                        
         AXI_01_BREADY       <=1'b1;     
                                       
         AXI_01_ARBURST     <= 2'b01;   
//         AXI_01_ARLEN       <= 4'b0;    
         AXI_01_ARSIZE      <= 3'b101;  
         AXI_01_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [2]         <= 0;                
         WADDR  [2]         <= 0;                
         RADDR  [2]         <= 0;                
         aValid [2]         <= 0;                
         wValid [2]         <= 0;                
         rValid [2]         <= 0;                
                                                 
         AXI_02_AWBURST      <= 2'b01;           
//         AXI_02_AWLEN        <= 4'd15;            
         AXI_02_AWSIZE       <= 3'b101;          
         AXI_02_AWID         <= 6'b0;            
                                               
                                               
//         AXI_02_WLAST        <= 1'b1;            
         AXI_02_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_02_WDATA_PARITY <= 32'b0;           
                                             
         AXI_02_BREADY       <=1'b1;             
                                                
         AXI_02_ARBURST     <= 2'b01;            
//         AXI_02_ARLEN       <= 4'b0;             
         AXI_02_ARSIZE      <= 3'b101;           
         AXI_02_ARID        <= 6'b0;              
         
         WDATA  [3]         <= 0;                   
         WADDR  [3]         <= 0;                   
         RADDR  [3]         <= 0;                   
         aValid [3]         <= 0;                   
         wValid [3]         <= 0;                   
         rValid [3]         <= 0;                   
                                                    
         AXI_03_AWBURST      <= 2'b01;              
//         AXI_03_AWLEN        <= 4'd15;               
         AXI_03_AWSIZE       <= 3'b101;             
         AXI_03_AWID         <= 6'b0;               
                                                    
                                                    
//         AXI_03_WLAST        <= 1'b1;               
         AXI_03_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;         
         AXI_03_WDATA_PARITY <= 32'b0;              
                                                    
         AXI_03_BREADY       <=1'b1;                
                                                    
         AXI_03_ARBURST     <= 2'b01;               
//         AXI_03_ARLEN       <= 4'b0;                
         AXI_03_ARSIZE      <= 3'b101;              
         AXI_03_ARID        <= 6'b0;                   
                
         `endif
`ifdef XBARorM1
       
         WDATA  [4]         <= 0;
         WADDR  [4]         <= 0;
         RADDR  [4]         <= 0;
         aValid [4]         <= 0;
         wValid [4]         <= 0;
         rValid [4]         <= 0;
          
         AXI_04_AWBURST      <= 2'b01; 
//         AXI_04_AWLEN        <= 4'd15;  
         AXI_04_AWSIZE       <= 3'b101;
         AXI_04_AWID         <= 6'b0;  
         
  
//         AXI_04_WLAST        <= 1'b1;         
         AXI_04_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;        
         AXI_04_WDATA_PARITY <= 32'b0; 
         
         AXI_04_BREADY       <=1'b1;     

         AXI_04_ARBURST     <= 2'b01; 
//         AXI_04_ARLEN       <= 4'b0;  
         AXI_04_ARSIZE      <= 3'b101;
         AXI_04_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [5]         <= 0;
         WADDR  [5]         <= 0;
         RADDR  [5]         <= 0;
         aValid [5]         <= 0;
         wValid [5]         <= 0;
         wValid [5]         <= 0;
         rValid [5]         <= 0;
        
         AXI_05_AWBURST      <= 2'b01;   
//         AXI_05_AWLEN        <= 4'd15;    
         AXI_05_AWSIZE       <= 3'b101;  
         AXI_05_AWID         <= 6'b0;    
                                        
                                        
//         AXI_05_WLAST        <= 1'b1;    
         AXI_05_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;   
         AXI_05_WDATA_PARITY <= 32'b0;   
                                        
         AXI_05_BREADY       <=1'b1;     
                                       
         AXI_05_ARBURST     <= 2'b01;   
//         AXI_05_ARLEN       <= 4'b0;    
         AXI_05_ARSIZE      <= 3'b101;  
         AXI_05_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [6]         <= 0;                
         WADDR  [6]         <= 0;                
         RADDR  [6]         <= 0;                
         aValid [6]         <= 0;                
         wValid [6]         <= 0;                
         rValid [6]         <= 0;                
                                                 
         AXI_06_AWBURST      <= 2'b01;           
//         AXI_06_AWLEN        <= 4'd15;            
         AXI_06_AWSIZE       <= 3'b101;          
         AXI_06_AWID         <= 6'b0;            
                                               
                                               
//         AXI_06_WLAST        <= 1'b1;            
         AXI_06_WSTRB        <=64'hFFFF_FFFF_FFFF_FFFF;          
         AXI_06_WDATA_PARITY <= 32'b0;           
                                             
         AXI_06_BREADY       <=1'b1;             
                                                
         AXI_06_ARBURST     <= 2'b01;            
//         AXI_06_ARLEN       <= 4'b0;             
         AXI_06_ARSIZE      <= 3'b101;           
         AXI_06_ARID        <= 6'b0;              
         
         WDATA  [7]         <= 0;                   
         WADDR  [7]         <= 0;                   
         RADDR  [7]         <= 0;                   
         aValid [7]         <= 0;                   
         wValid [7]         <= 0;                   
         rValid [7]         <= 0;                   
                                                    
         AXI_07_AWBURST      <= 2'b01;              
//         AXI_07_AWLEN        <= 4'd15;               
         AXI_07_AWSIZE       <= 3'b101;             
         AXI_07_AWID         <= 6'b0;               
                                                    
                                                    
//         AXI_07_WLAST        <= 1'b1;               
         AXI_07_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;           
         AXI_07_WDATA_PARITY <= 32'b0;              
                                                    
         AXI_07_BREADY       <=1'b1;                
                                                    
         AXI_07_ARBURST     <= 2'b01;               
//         AXI_07_ARLEN       <= 4'b0;                
         AXI_07_ARSIZE      <= 3'b101;              
         AXI_07_ARID        <= 6'b0;    
         
         
`endif
`ifdef XBARorM0

         WDATA  [8]         <= 0;
         WADDR  [8]         <= 0;
         RADDR  [8]         <= 0;
         aValid [8]         <= 0;
         wValid [8]         <= 0;
         rValid [8]         <= 0;
          
         AXI_08_AWBURST      <= 2'b01; 
//         AXI_08_AWLEN        <= 4'd15;  
         AXI_08_AWSIZE       <= 3'b101;
         AXI_08_AWID         <= 6'b0;  
         
  
//         AXI_08_WLAST        <= 1'b1;         
         AXI_08_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_08_WDATA_PARITY <= 32'b0; 
         
         AXI_08_BREADY       <=1'b1;     

         AXI_08_ARBURST     <= 2'b01; 
//         AXI_08_ARLEN       <= 4'b0;  
         AXI_08_ARSIZE      <= 3'b101;
         AXI_08_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [9]         <= 0;
         WADDR  [9]         <= 0;
         RADDR  [9]         <= 0;
         aValid [9]         <= 0;
         wValid [9]         <= 0;
         rValid [9]         <= 0;
        
         AXI_09_AWBURST      <= 2'b01;   
//         AXI_09_AWLEN        <= 4'd15;    
         AXI_09_AWSIZE       <= 3'b101;  
         AXI_09_AWID         <= 6'b0;    
                                        
                                        
//         AXI_09_WLAST        <= 1'b1;    
         AXI_09_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;
         AXI_09_WDATA_PARITY <= 32'b0;   
                                        
         AXI_09_BREADY       <=1'b1;     
                                       
         AXI_09_ARBURST     <= 2'b01;   
//         AXI_09_ARLEN       <= 4'b0;    
         AXI_09_ARSIZE      <= 3'b101;  
         AXI_09_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [10]         <= 0;                
         WADDR  [10]         <= 0;                
         RADDR  [10]         <= 0;                
         aValid [10]         <= 0;                
         wValid [10]         <= 0;                
         rValid [10]         <= 0;                
                                                 
         AXI_10_AWBURST      <= 2'b01;           
//         AXI_10_AWLEN        <= 4'd15;            
         AXI_10_AWSIZE       <= 3'b101;          
         AXI_10_AWID         <= 6'b0;            
                                               
                                               
//         AXI_10_WLAST        <= 1'b1;            
         AXI_10_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;        
         AXI_10_WDATA_PARITY <= 32'b0;           
                                             
         AXI_10_BREADY       <=1'b1;             
                                                
         AXI_10_ARBURST     <= 2'b01;            
//         AXI_10_ARLEN       <= 4'b0;             
         AXI_10_ARSIZE      <= 3'b101;           
         AXI_10_ARID        <= 6'b0;              
         
         WDATA  [11]         <= 0;                   
         WADDR  [11]         <= 0;                   
         RADDR  [11]         <= 0;                   
         aValid [11]         <= 0;                   
         wValid [11]         <= 0;                   
         rValid [11]         <= 0;                   
                                                    
         AXI_11_AWBURST      <= 2'b01;              
//         AXI_11_AWLEN        <= 4'd15;               
         AXI_11_AWSIZE       <= 3'b101;             
         AXI_11_AWID         <= 6'b0;               
                                                    
                                                    
//         AXI_11_WLAST        <= 1'b1;               
         AXI_11_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;           
         AXI_11_WDATA_PARITY <= 32'b0;              
                                                    
         AXI_11_BREADY       <=1'b1;                
                                                    
         AXI_11_ARBURST     <= 2'b01;               
//         AXI_11_ARLEN       <= 4'b0;                
         AXI_11_ARSIZE      <= 3'b101;              
         AXI_11_ARID        <= 6'b0;                   
     `endif
`ifdef XBAR            
                
         WDATA  [12]         <= 0;
         WADDR  [12]         <= 0;
         RADDR  [12]         <= 0;
         aValid [12]         <= 0;
         wValid [12]         <= 0;
         rValid [12]         <= 0;
          
         AXI_12_AWBURST      <= 2'b01; 
//         AXI_12_AWLEN        <= 4'd15;  
         AXI_12_AWSIZE       <= 3'b101;
         AXI_12_AWID         <= 6'b0;  
         
  
//         AXI_12_WLAST        <= 1'b1;         
         AXI_12_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_12_WDATA_PARITY <= 32'b0; 
         
         AXI_12_BREADY       <=1'b1;     

         AXI_12_ARBURST     <= 2'b01; 
//         AXI_12_ARLEN       <= 4'b0;  
         AXI_12_ARSIZE      <= 3'b101;
         AXI_12_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [13]         <= 0;
         WADDR  [13]         <= 0;
         RADDR  [13]         <= 0;
         aValid [13]         <= 0;
         wValid [13]         <= 0;
         rValid [13]         <= 0;
        
         AXI_13_AWBURST      <= 2'b01;   
//         AXI_13_AWLEN        <= 4'd15;    
         AXI_13_AWSIZE       <= 3'b101;  
         AXI_13_AWID         <= 6'b0;    
                                        
                                        
//         AXI_13_WLAST        <= 1'b1;    
         AXI_13_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;
         AXI_13_WDATA_PARITY <= 32'b0;   
                                        
         AXI_13_BREADY       <=1'b1;     
                                       
         AXI_13_ARBURST     <= 2'b01;   
//         AXI_13_ARLEN       <= 4'b0;    
         AXI_13_ARSIZE      <= 3'b101;  
         AXI_13_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [14]         <= 0;                
         WADDR  [14]         <= 0;                
         RADDR  [14]         <= 0;                
         aValid [14]         <= 0;                
         wValid [14]         <= 0;                
         rValid [14]         <= 0;                
                                                 
         AXI_14_AWBURST      <= 2'b01;           
//         AXI_14_AWLEN        <= 4'd15;            
         AXI_14_AWSIZE       <= 3'b101;          
         AXI_14_AWID         <= 6'b0;            
                                               
                                               
//         AXI_14_WLAST        <= 1'b1;            
         AXI_14_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;        
         AXI_14_WDATA_PARITY <= 32'b0;           
                                             
         AXI_14_BREADY       <=1'b1;             
                                                
         AXI_14_ARBURST     <= 2'b01;            
//         AXI_14_ARLEN       <= 4'b0;             
         AXI_14_ARSIZE      <= 3'b101;           
         AXI_14_ARID        <= 6'b0;              
         
         WDATA  [15]         <= 0;                   
         WADDR  [15]         <= 0;                   
         RADDR  [15]         <= 0;                   
         aValid [15]         <= 0;                   
         wValid [15]         <= 0;                   
         rValid [15]         <= 0;                   
                                                    
         AXI_15_AWBURST      <= 2'b01;              
//         AXI_15_AWLEN        <= 4'd15;               
         AXI_15_AWSIZE       <= 3'b101;             
         AXI_15_AWID         <= 6'b0;               
                                                    
                                                    
//         AXI_15_WLAST        <= 1'b1;               
         AXI_15_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;           
         AXI_15_WDATA_PARITY <= 32'b0;              
                                                    
         AXI_15_BREADY       <=1'b1;                
                                                    
         AXI_15_ARBURST     <= 2'b01;               
//         AXI_15_ARLEN       <= 4'b0;                
         AXI_15_ARSIZE      <= 3'b101;              
         AXI_15_ARID        <= 6'b0;     
         
         WDATA  [16]         <= 0;
         WADDR  [16]         <= 0;
         RADDR  [16]         <= 0;
         aValid [16]         <= 0;
         wValid [16]         <= 0;
         rValid [16]         <= 0;
          
         AXI_16_AWBURST      <= 2'b01; 
//         AXI_16_AWLEN        <= 4'd15;  
         AXI_16_AWSIZE       <= 3'b101;
         AXI_16_AWID         <= 6'b0;  
         
  
//         AXI_16_WLAST        <= 1'b1;         
         AXI_16_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_16_WDATA_PARITY <= 32'b0; 
          
         AXI_16_BREADY       <=1'b1;     
             
         AXI_16_ARBURST     <= 2'b01; 
//         AXI_16_ARLEN       <= 4'b0;  
         AXI_16_ARSIZE      <= 3'b101;
         AXI_16_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [17]         <= 0;
         WADDR  [17]         <= 0;
         RADDR  [17]         <= 0;
         aValid [17]         <= 0;
         wValid [17]         <= 0;
         rValid [17]         <= 0;
        
         AXI_17_AWBURST      <= 2'b01;   
//         AXI_17_AWLEN        <= 4'd15;    
         AXI_17_AWSIZE       <= 3'b101;  
         AXI_17_AWID         <= 6'b0;    
                                   
                                   
//         AXI_17_WLAST        <= 1'b1;    
         AXI_17_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF; 
         AXI_17_WDATA_PARITY <= 32'b0;   
                                     
         AXI_17_BREADY       <=1'b1;     
                                    
         AXI_17_ARBURST     <= 2'b01;   
//         AXI_17_ARLEN       <= 4'b0;    
         AXI_17_ARSIZE      <= 3'b101;  
         AXI_17_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [18]         <= 0;                
         WADDR  [18]         <= 0;                
         RADDR  [18]         <= 0;                
         aValid [18]         <= 0;                
         wValid [18]         <= 0;                
         rValid [18]         <= 0;                
                                                 
         AXI_18_AWBURST      <= 2'b01;           
//         AXI_18_AWLEN        <= 4'd15;            
         AXI_18_AWSIZE       <= 3'b101;          
         AXI_18_AWID         <= 6'b0;            
                                           
                                           
//         AXI_18_WLAST        <= 1'b1;            
         AXI_18_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_18_WDATA_PARITY <= 32'b0;           
                                         
         AXI_18_BREADY       <=1'b1;             
                                             
         AXI_18_ARBURST     <= 2'b01;            
//         AXI_18_ARLEN       <= 4'b0;             
         AXI_18_ARSIZE      <= 3'b101;           
         AXI_18_ARID        <= 6'b0;              
         
         WDATA  [19]         <= 0;                   
         WADDR  [19]         <= 0;                   
         RADDR  [19]         <= 0;                   
         aValid [19]         <= 0;                   
         wValid [19]         <= 0;                   
         rValid [19]         <= 0;                   
                                                    
         AXI_19_AWBURST      <= 2'b01;              
//         AXI_19_AWLEN        <= 4'd15;               
         AXI_19_AWSIZE       <= 3'b101;             
         AXI_19_AWID         <= 6'b0;               
                                                
                                                
//         AXI_19_WLAST        <= 1'b1;               
         AXI_19_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;         
         AXI_19_WDATA_PARITY <= 32'b0;              
                                                 
         AXI_19_BREADY       <=1'b1;                
                                                
         AXI_19_ARBURST     <= 2'b01;               
//         AXI_19_ARLEN       <= 4'b0;                
         AXI_19_ARSIZE      <= 3'b101;              
         AXI_19_ARID        <= 6'b0;                   
                
                
         WDATA  [20]         <= 0;
         WADDR  [20]         <= 0;
         RADDR  [20]         <= 0;
         aValid [20]         <= 0;
         wValid [20]         <= 0;
         rValid [20]         <= 0;
          
         AXI_20_AWBURST      <= 2'b01; 
//         AXI_20_AWLEN        <= 4'd15;  
         AXI_20_AWSIZE       <= 3'b101;
         AXI_20_AWID         <= 6'b0;  
           
           
//         AXI_20_WLAST        <= 1'b1;         
         AXI_20_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;        
         AXI_20_WDATA_PARITY <= 32'b0; 
            
         AXI_20_BREADY       <=1'b1;     
             
         AXI_20_ARBURST     <= 2'b01; 
//         AXI_20_ARLEN       <= 4'b0;  
         AXI_20_ARSIZE      <= 3'b101;
         AXI_20_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [21]         <= 0;
         WADDR  [21]         <= 0;
         RADDR  [21]         <= 0;
         aValid [21]         <= 0;
         wValid [21]         <= 0;
         wValid [21]         <= 0;
         rValid [21]         <= 0;
        
         AXI_21_AWBURST      <= 2'b01;   
//         AXI_21_AWLEN        <= 4'd15;    
         AXI_21_AWSIZE       <= 3'b101;  
         AXI_21_AWID         <= 6'b0;    
                              
                              
//         AXI_21_WLAST        <= 1'b1;    
         AXI_21_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;   
         AXI_21_WDATA_PARITY <= 32'b0;   
                                   
         AXI_21_BREADY       <=1'b1;     
                                   
         AXI_21_ARBURST     <= 2'b01;   
//         AXI_21_ARLEN       <= 4'b0;    
         AXI_21_ARSIZE      <= 3'b101;  
         AXI_21_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [22]         <= 0;                
         WADDR  [22]         <= 0;                
         RADDR  [22]         <= 0;                
         aValid [22]         <= 0;                
         wValid [22]         <= 0;                
         rValid [22]         <= 0;                
                                                 
         AXI_22_AWBURST      <= 2'b01;           
//         AXI_22_AWLEN        <= 4'd15;            
         AXI_22_AWSIZE       <= 3'b101;          
         AXI_22_AWID         <= 6'b0;            
                                        
                                        
//         AXI_22_WLAST        <= 1'b1;            
         AXI_22_WSTRB        <=64'hFFFF_FFFF_FFFF_FFFF;          
         AXI_22_WDATA_PARITY <= 32'b0;           
                                          
         AXI_22_BREADY       <=1'b1;             
                                           
         AXI_22_ARBURST     <= 2'b01;            
//         AXI_22_ARLEN       <= 4'b0;             
         AXI_22_ARSIZE      <= 3'b101;           
         AXI_22_ARID        <= 6'b0;              
         
         WDATA  [23]         <= 0;                   
         WADDR  [23]         <= 0;                   
         RADDR  [23]         <= 0;                   
         aValid [23]         <= 0;                   
         wValid [23]         <= 0;                   
         rValid [23]         <= 0;                   
                                                    
         AXI_23_AWBURST      <= 2'b01;              
//         AXI_23_AWLEN        <= 4'd15;               
         AXI_23_AWSIZE       <= 3'b101;             
         AXI_23_AWID         <= 6'b0;               
                                               
                                               
//         AXI_23_WLAST        <= 1'b1;               
         AXI_23_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;           
         AXI_23_WDATA_PARITY <= 32'b0;              
                                     
         AXI_23_BREADY       <=1'b1;                
                                                
         AXI_23_ARBURST     <= 2'b01;               
//         AXI_23_ARLEN       <= 4'b0;                
         AXI_23_ARSIZE      <= 3'b101;              
         AXI_23_ARID        <= 6'b0;    
         
         

         WDATA  [24]         <= 0;
         WADDR  [24]         <= 0;
         RADDR  [24]         <= 0;
         aValid [24]         <= 0;
         wValid [24]         <= 0;
         rValid [24]         <= 0;
          
         AXI_24_AWBURST      <= 2'b01; 
//         AXI_24_AWLEN        <= 4'd15;  
         AXI_24_AWSIZE       <= 3'b101;
         AXI_24_AWID         <= 6'b0;  
            
            
//         AXI_24_WLAST        <= 1'b1;         
         AXI_24_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_24_WDATA_PARITY <= 32'b0; 
             
         AXI_24_BREADY       <=1'b1;     
            
         AXI_24_ARBURST     <= 2'b01; 
//         AXI_24_ARLEN       <= 4'b0;  
         AXI_24_ARSIZE      <= 3'b101;
         AXI_24_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [25]         <= 0;
         WADDR  [25]         <= 0;
         RADDR  [25]         <= 0;
         aValid [25]         <= 0;
         wValid [25]         <= 0;
         rValid [25]         <= 0;
        
         AXI_25_AWBURST      <= 2'b01;   
//         AXI_25_AWLEN        <= 4'd15;    
         AXI_25_AWSIZE       <= 3'b101;  
         AXI_25_AWID         <= 6'b0;    
                                    
                                    
//         AXI_25_WLAST        <= 1'b1;    
         AXI_25_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;
         AXI_25_WDATA_PARITY <= 32'b0;   
                                    
         AXI_25_BREADY       <=1'b1;     
                                
         AXI_25_ARBURST     <= 2'b01;   
//         AXI_25_ARLEN       <= 4'b0;    
         AXI_25_ARSIZE      <= 3'b101;  
         AXI_25_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [26]         <= 0;                
         WADDR  [26]         <= 0;                
         RADDR  [26]         <= 0;                
         aValid [26]         <= 0;                
         wValid [26]         <= 0;                
         rValid [26]         <= 0;                
                                                 
         AXI_26_AWBURST      <= 2'b01;           
//         AXI_26_AWLEN        <= 4'd15;            
         AXI_26_AWSIZE       <= 3'b101;          
         AXI_26_AWID         <= 6'b0;            
                                           
                                           
//         AXI_26_WLAST        <= 1'b1;            
         AXI_26_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;        
         AXI_26_WDATA_PARITY <= 32'b0;           
                                       
         AXI_26_BREADY       <=1'b1;             
                                           
         AXI_26_ARBURST     <= 2'b01;            
//         AXI_26_ARLEN       <= 4'b0;             
         AXI_26_ARSIZE      <= 3'b101;           
         AXI_26_ARID        <= 6'b0;              
         
         WDATA  [27]         <= 0;                   
         WADDR  [27]         <= 0;                   
         RADDR  [27]         <= 0;                   
         aValid [27]         <= 0;                   
         wValid [27]         <= 0;                   
         rValid [27]         <= 0;                   
                                                    
         AXI_27_AWBURST      <= 2'b01;              
//         AXI_27_AWLEN        <= 4'd15;               
         AXI_27_AWSIZE       <= 3'b101;             
         AXI_27_AWID         <= 6'b0;               
                                                    
                                                    
//         AXI_27_WLAST        <= 1'b1;               
         AXI_27_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;           
         AXI_27_WDATA_PARITY <= 32'b0;              
                                                    
         AXI_27_BREADY       <=1'b1;                
                                                    
         AXI_27_ARBURST     <= 2'b01;               
//         AXI_27_ARLEN       <= 4'b0;                
         AXI_27_ARSIZE      <= 3'b101;              
         AXI_27_ARID        <= 6'b0;                   
                
                
         WDATA  [28]         <= 0;
         WADDR  [28]         <= 0;
         RADDR  [28]         <= 0;
         aValid [28]         <= 0;
         wValid [28]         <= 0;
         rValid [28]         <= 0;
          
         AXI_28_AWBURST      <= 2'b01; 
//         AXI_28_AWLEN        <= 4'd15;  
         AXI_28_AWSIZE       <= 3'b101;
         AXI_28_AWID         <= 6'b0;  
         
  
//         AXI_28_WLAST        <= 1'b1;         
         AXI_28_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;     
         AXI_28_WDATA_PARITY <= 32'b0; 
         
         AXI_28_BREADY       <=1'b1;     

         AXI_28_ARBURST     <= 2'b01; 
//         AXI_28_ARLEN       <= 4'b0;  
         AXI_28_ARSIZE      <= 3'b101;
         AXI_28_ARID        <= 6'b0; 
        
        // AXI 01
        
         WDATA  [29]         <= 0;
         WADDR  [29]         <= 0;
         RADDR  [29]         <= 0;
         aValid [29]         <= 0;
         wValid [29]         <= 0;
         rValid [29]         <= 0;
        
         AXI_29_AWBURST      <= 2'b01;   
//         AXI_29_AWLEN        <= 4'd15;    
         AXI_29_AWSIZE       <= 3'b101;  
         AXI_29_AWID         <= 6'b0;    
                                        
                                        
//         AXI_29_WLAST        <= 1'b1;    
         AXI_29_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;
         AXI_29_WDATA_PARITY <= 32'b0;   
                                        
         AXI_29_BREADY       <=1'b1;     
                                       
         AXI_29_ARBURST     <= 2'b01;   
//         AXI_29_ARLEN       <= 4'b0;    
         AXI_29_ARSIZE      <= 3'b101;  
         AXI_29_ARID        <= 6'b0;    
             
          // AXI 02                                
                                          
         WDATA  [30]         <= 0;                
         WADDR  [30]         <= 0;                
         RADDR  [30]         <= 0;                
         aValid [30]         <= 0;                
         wValid [30]         <= 0;                
         rValid [30]         <= 0;                
                                                 
         AXI_30_AWBURST      <= 2'b01;           
//         AXI_30_AWLEN        <= 4'd15;            
         AXI_30_AWSIZE       <= 3'b101;          
         AXI_30_AWID         <= 6'b0;            
                                               
                                               
//         AXI_30_WLAST        <= 1'b1;            
         AXI_30_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;        
         AXI_30_WDATA_PARITY <= 32'b0;           
                                             
         AXI_30_BREADY       <=1'b1;             
                                                
         AXI_30_ARBURST     <= 2'b01;            
//         AXI_30_ARLEN       <= 4'b0;             
         AXI_30_ARSIZE      <= 3'b101;           
         AXI_30_ARID        <= 6'b0;              
         
         WDATA  [31]         <= 0;                   
         WADDR  [31]         <= 0;                   
         RADDR  [31]         <= 0;                   
         aValid [31]         <= 0;                   
         wValid [31]         <= 0;                   
         rValid [31]         <= 0;                   
                                                    
         AXI_31_AWBURST      <= 2'b01;              
//         AXI_31_AWLEN        <= 4'd15;               
         AXI_31_AWSIZE       <= 3'b101;             
         AXI_31_AWID         <= 6'b0;               
                                                    
                                                    
//         AXI_31_WLAST        <= 1'b1;               
         AXI_31_WSTRB        <= 64'hFFFF_FFFF_FFFF_FFFF;           
         AXI_31_WDATA_PARITY <= 32'b0;              
                                                    
         AXI_31_BREADY       <=1'b1;                
                                                    
         AXI_31_ARBURST     <= 2'b01;               
//         AXI_31_ARLEN       <= 4'b0;                
         AXI_31_ARSIZE      <= 3'b101;              
         AXI_31_ARID        <= 6'b0;     
         `endif
    end
    else begin
        ce_r <= ce;
        if(ce_r == 1) 
            packet_count <= 1 << (`packet_count-log2(cmd`burst_len+1'b1));
        if(AXI_00_AWVALID && AXI_00_AWREADY)
            writeRequest <= writeRequest + 1;
            
                   if(AXI_00_BVALID == 1 && AXI_00_BREADY)
            writeCount[0] <= writeCount[0] + 1;
            
            `ifdef XBARorM1orM0

                   if(AXI_01_BVALID == 1 && AXI_01_BREADY)
            writeCount[1] <= writeCount[1] + 1;
                   if(AXI_02_BVALID == 1 && AXI_02_BREADY)
            writeCount[2] <= writeCount[2] + 1;
                   if(AXI_03_BVALID == 1 && AXI_03_BREADY)
            writeCount[3] <= writeCount[3] + 1;
            
            `endif
`ifdef XBARorM1
                   if(AXI_04_BVALID == 1 && AXI_04_BREADY == 1)
            writeCount[4] <= writeCount[4] + 1;
                   if(AXI_05_BVALID == 1 && AXI_05_BREADY == 1)
            writeCount[5] <= writeCount[5] + 1;
                   if(AXI_06_BVALID == 1 && AXI_06_BREADY == 1)
            writeCount[6] <= writeCount[6] + 1;
                   if(AXI_07_BVALID == 1 && AXI_07_BREADY == 1)
            writeCount[7] <= writeCount[7] + 1;
            
            `endif
`ifdef XBARorM0 

               if(AXI_08_BVALID == 1 && AXI_08_BREADY == 1)
            writeCount[8] <= writeCount[8] + 1;
                   if(AXI_09_BVALID == 1 && AXI_09_BREADY == 1)
            writeCount[9] <= writeCount[9] + 1;
                   if(AXI_10_BVALID == 1 && AXI_10_BREADY == 1)
            writeCount[10] <= writeCount[10] + 1;
                   if(AXI_11_BVALID == 1 && AXI_11_BREADY == 1)
            writeCount[11] <= writeCount[11] + 1;
            
 
`endif
`ifdef XBAR

                   if(AXI_12_BVALID == 1 && AXI_12_BREADY == 1)
            writeCount[12] <= writeCount[12] + 1;
                   if(AXI_13_BVALID == 1 && AXI_13_BREADY == 1)
            writeCount[13] <= writeCount[13] + 1;
                   if(AXI_14_BVALID == 1 && AXI_14_BREADY == 1)
            writeCount[14] <= writeCount[14] + 1;
                   if(AXI_15_BVALID == 1 && AXI_15_BREADY == 1)
            writeCount[15] <= writeCount[15] + 1;
            
            
                if(AXI_16_BVALID == 1 && AXI_16_BREADY)
            writeCount[16] <= writeCount[16] + 1;
                   if(AXI_17_BVALID == 1 && AXI_17_BREADY)
            writeCount[17] <= writeCount[17] + 1;
                   if(AXI_18_BVALID == 1 && AXI_18_BREADY)
            writeCount[18] <= writeCount[18] + 1;
                   if(AXI_19_BVALID == 1 && AXI_19_BREADY)
            writeCount[19] <= writeCount[19] + 1;
                   if(AXI_20_BVALID == 1 && AXI_20_BREADY == 1)
            writeCount[20] <= writeCount[20] + 1;
                   if(AXI_21_BVALID == 1 && AXI_21_BREADY == 1)
            writeCount[21] <= writeCount[21] + 1;
                   if(AXI_22_BVALID == 1 && AXI_22_BREADY == 1)
            writeCount[22] <= writeCount[22] + 1;
                   if(AXI_23_BVALID == 1 && AXI_23_BREADY == 1)
            writeCount[23] <= writeCount[23] + 1;
            
               if(AXI_24_BVALID == 1 && AXI_24_BREADY == 1)
            writeCount[24] <= writeCount[24] + 1;
                   if(AXI_25_BVALID == 1 && AXI_25_BREADY == 1)
            writeCount[25] <= writeCount[25] + 1;
                   if(AXI_26_BVALID == 1 && AXI_26_BREADY == 1)
            writeCount[26] <= writeCount[26] + 1;
                   if(AXI_27_BVALID == 1 && AXI_27_BREADY == 1)
            writeCount[27] <= writeCount[27] + 1;
                   if(AXI_28_BVALID == 1 && AXI_28_BREADY == 1)
            writeCount[28] <= writeCount[28] + 1;
                   if(AXI_29_BVALID == 1 && AXI_29_BREADY == 1)
            writeCount[29] <= writeCount[29] + 1;
                   if(AXI_30_BVALID == 1 && AXI_30_BREADY == 1)
            writeCount[30] <= writeCount[30] + 1;
                   if(AXI_31_BVALID == 1 && AXI_31_BREADY == 1)
            writeCount[31] <= writeCount[31] + 1;
            
`endif


                    if(AXI_00_RVALID == 1 && AXI_00_RREADY )
            readCount[0] <= readCount[0] + 1;
            
            `ifdef XBARorM1orM0

                   if(AXI_01_RVALID == 1 && AXI_01_RREADY)
            readCount[1] <= readCount[1] + 1;
                   if(AXI_02_RVALID == 1 && AXI_02_RREADY )
            readCount[2] <= readCount[2] + 1;
                   if(AXI_03_RVALID == 1 && AXI_03_RREADY)
            readCount[3] <= readCount[3] + 1;
            
            `endif
`ifdef XBARorM1

                   if(AXI_04_RVALID == 1 && AXI_04_RREADY == 1)
            readCount[4] <= readCount[4] + 1;
                   if(AXI_05_RVALID == 1 && AXI_05_RREADY == 1)
            readCount[5] <= readCount[5] + 1;
                   if(AXI_06_RVALID == 1 && AXI_06_RREADY == 1)
            readCount[6] <= readCount[6] + 1;
                   if(AXI_07_RVALID == 1 && AXI_07_RREADY ==1)
            readCount[7] <= readCount[7] + 1;
            
 `endif
`ifdef XBARorM0 

              if(AXI_08_RVALID == 1 && AXI_08_RREADY == 1)
            readCount[8] <= readCount[8] + 1;
                   if(AXI_09_RVALID == 1 && AXI_09_RREADY == 1)
            readCount[9] <= readCount[9] + 1;
                   if(AXI_10_RVALID == 1 && AXI_10_RREADY == 1)
            readCount[10] <= readCount[10] + 1;
                   if(AXI_11_RVALID == 1 && AXI_11_RREADY == 1)
            readCount[11] <= readCount[11] + 1;
            
`endif
`ifdef XBAR             
                   if(AXI_12_RVALID == 1 && AXI_12_RREADY == 1)
            readCount[12] <= readCount[12] + 1;
                   if(AXI_13_RVALID == 1 && AXI_13_RREADY == 1)
            readCount[13] <= readCount[13] + 1;
                   if(AXI_14_RVALID == 1 && AXI_14_RREADY == 1)
            readCount[14] <= readCount[14] + 1;
                   if(AXI_15_RVALID == 1 && AXI_15_RREADY == 1)
            readCount[15] <= readCount[15] + 1;
            
           
                    if(AXI_16_RVALID == 1 && AXI_16_RREADY )
            readCount[16] <= readCount[16] + 1;
                   if(AXI_17_RVALID == 1 && AXI_17_RREADY)
            readCount[17] <= readCount[17] + 1;
                   if(AXI_18_RVALID == 1 && AXI_18_RREADY )
            readCount[18] <= readCount[18] + 1;
                   if(AXI_19_RVALID == 1 && AXI_19_RREADY)
            readCount[19] <= readCount[19] + 1;
                   if(AXI_20_RVALID == 1 && AXI_20_RREADY == 1)
            readCount[20] <= readCount[20] + 1;
                   if(AXI_21_RVALID == 1 && AXI_21_RREADY == 1)
            readCount[21] <= readCount[21] + 1;
                   if(AXI_22_RVALID == 1 && AXI_22_RREADY == 1)
            readCount[22] <= readCount[22] + 1;
                   if(AXI_23_RVALID == 1 && AXI_23_RREADY ==1)
            readCount[23] <= readCount[23] + 1;
            
               if(AXI_24_RVALID == 1 && AXI_24_RREADY == 1)
            readCount[24] <= readCount[24] + 1;
                   if(AXI_25_RVALID == 1 && AXI_25_RREADY == 1)
            readCount[25] <= readCount[25] + 1;
                   if(AXI_26_RVALID == 1 && AXI_26_RREADY == 1)
            readCount[26] <= readCount[26] + 1;
                   if(AXI_27_RVALID == 1 && AXI_27_RREADY == 1)
            readCount[27] <= readCount[27] + 1;
                   if(AXI_28_RVALID == 1 && AXI_28_RREADY == 1)
            readCount[28] <= readCount[28] + 1;
                   if(AXI_29_RVALID == 1 && AXI_29_RREADY == 1)
            readCount[29] <= readCount[29] + 1;
                   if(AXI_30_RVALID == 1 && AXI_30_RREADY == 1)
            readCount[30] <= readCount[30] + 1;
                   if(AXI_31_RVALID == 1 && AXI_31_RREADY == 1)
            readCount[31] <= readCount[31] + 1;
 `endif           
            writeCount_add_0[0] <= writeCount[0]+writeCount[1];
            writeCount_add_0[1] <= writeCount[2]+writeCount[3];
            writeCount_add_0[2] <= writeCount[4]+writeCount[5];
            writeCount_add_0[3] <= writeCount[6]+writeCount[7];
                       
            writeCount_add_0[4] <= writeCount[8]+writeCount[9];
            writeCount_add_0[5] <= writeCount[10]+writeCount[11];
            writeCount_add_0[6] <= writeCount[12]+writeCount[13];
            writeCount_add_0[7] <= writeCount[14]+writeCount[15];
            
            writeCount_add_0[8] <= writeCount[16]+writeCount[17];
            writeCount_add_0[9] <= writeCount[18]+writeCount[19];
            writeCount_add_0[10] <= writeCount[20]+writeCount[21];
            writeCount_add_0[11] <= writeCount[22]+writeCount[23];
               
            
            writeCount_add_1[0] <= writeCount_add_0[0]+writeCount_add_0[1];
            writeCount_add_1[1] <= writeCount_add_0[2]+writeCount_add_0[3];
            writeCount_add_1[2] <= writeCount_add_0[4]+writeCount_add_0[5];
            writeCount_add_1[3] <= writeCount_add_0[6]+writeCount_add_0[7];
            writeCount_add_1[4] <= writeCount_add_0[8]+writeCount_add_0[9];
            writeCount_add_1[5] <= writeCount_add_0[10]+writeCount_add_0[11];

            writeCount_add_2[0] <= writeCount_add_1[0]+writeCount_add_1[1];
            writeCount_add_2[1] <= writeCount_add_1[2]+writeCount_add_1[3];
            writeCount_add_2[2] <= writeCount_add_1[4]+writeCount_add_1[5];

                      
            writeCount_add_3[0] <= writeCount_add_2[0]+writeCount_add_2[1];
            writeCount_add_3[1] <= writeCount_add_2[2];
            
            writeCount_add_4 <= writeCount_add_3[0]+writeCount_add_3[1];
         
          if(writeCount_add_4 == (24*(packet_count+1)) && ce_r==1) begin
//           if(writeCount_add_4 == 32) begin
              done_r <= 1;

            writeCount[0] <= writeCount[0] + 1;
            writeCount[1] <= writeCount[1] + 1;
            writeCount[2] <= writeCount[2] + 1;
            writeCount[3] <= writeCount[3] + 1;
            
            readCount[0] <= readCount[0] + 1;
            readCount[1] <= readCount[1] + 1;
            readCount[2] <= readCount[2] + 1;
            readCount[3] <= readCount[3] + 1;
                        
            writeCount_add_4 <= 0;
            writeCount_add_3[0] <= 0;
            writeCount_add_2[0] <= 0;
            writeCount_add_1[0] <= 0;
            writeCount_add_0[0]<=0;
            writeCount[0] <= 0;
         end   
         else
             done_r <= 0;
             
             
//         WDATA  [0] <=        AXI_00_WDATA;
//         WADDR  [0] <=  {1'b0,AXI_00_AWADDR};
//         RADDR  [0] <=  {1'b0,AXI_00_ARADDR};
//         aValid [0] <=        AXI_00_AWVALID;
//         wValid [0] <=        AXI_00_WVALID;
//         rValid [0] <=        AXI_00_ARVALID;
         `ifdef XBARorM1orM0
         WDATA  [1] <=        AXI_01_WDATA;       
         WADDR  [1] <=  {1'b0,AXI_01_AWADDR};     
         RADDR  [1] <=  {1'b0,AXI_01_ARADDR};     
         aValid [1] <=        AXI_01_AWVALID;     
         wValid [1] <=        AXI_01_WVALID;      
         rValid [1] <=        AXI_01_ARVALID;     
         
         WDATA  [2] <=        AXI_02_WDATA;       
         WADDR  [2] <=  {1'b0,AXI_02_AWADDR};     
         RADDR  [2] <=  {1'b0,AXI_02_ARADDR};     
         aValid [2] <=        AXI_02_AWVALID;     
         wValid [2] <=        AXI_02_WVALID;      
         rValid [2] <=        AXI_02_ARVALID;    
         
         WDATA  [3] <=        AXI_03_WDATA;      
         WADDR  [3] <=  {1'b0,AXI_03_AWADDR};    
         RADDR  [3] <=  {1'b0,AXI_03_ARADDR};    
         aValid [3] <=        AXI_03_AWVALID;    
         wValid [3] <=        AXI_03_WVALID;     
         rValid [3] <=        AXI_03_ARVALID;    
         `endif
`ifdef XBARorM1
         WDATA  [4] <=        AXI_04_WDATA;
         WADDR  [4] <=  {1'b0,AXI_04_AWADDR};
         RADDR  [4] <=  {1'b0,AXI_04_ARADDR};
         aValid [4] <=        AXI_04_AWVALID;
         wValid [4] <=        AXI_04_WVALID;
         rValid [4] <=        AXI_04_ARVALID;
         
         WDATA  [5] <=        AXI_05_WDATA;       
         WADDR  [5] <=  {1'b0,AXI_05_AWADDR};     
         RADDR  [5] <=  {1'b0,AXI_05_ARADDR};     
         aValid [5] <=        AXI_05_AWVALID;     
         wValid [5] <=        AXI_05_WVALID;      
         rValid [5] <=        AXI_05_ARVALID;     
         
         WDATA  [6] <=        AXI_06_WDATA;       
         WADDR  [6] <=  {1'b0,AXI_06_AWADDR};     
         RADDR  [6] <=  {1'b0,AXI_06_ARADDR};     
         aValid [6] <=        AXI_06_AWVALID;     
         wValid [6] <=        AXI_06_WVALID;      
         rValid [6] <=        AXI_06_ARVALID;    
         
         WDATA  [7] <=        AXI_07_WDATA;      
         WADDR  [7] <=  {1'b0,AXI_07_AWADDR};    
         RADDR  [7] <=  {1'b0,AXI_07_ARADDR};    
         aValid [7] <=        AXI_07_AWVALID;    
         wValid [7] <=        AXI_07_WVALID;     
         rValid [7] <=        AXI_07_ARVALID;    
         `endif
`ifdef XBARorM0 


         WDATA  [8] <=        AXI_08_WDATA;
         WADDR  [8] <=  {1'b0,AXI_08_AWADDR};
         RADDR  [8] <=  {1'b0,AXI_08_ARADDR};
         aValid [8] <=        AXI_08_AWVALID;
         wValid [8] <=        AXI_08_WVALID;
         rValid [8] <=        AXI_08_ARVALID;
         
         WDATA  [9] <=        AXI_09_WDATA;       
         WADDR  [9] <=  {1'b0,AXI_09_AWADDR};     
         RADDR  [9] <=  {1'b0,AXI_09_ARADDR};     
         aValid [9] <=        AXI_09_AWVALID;     
         wValid [9] <=        AXI_09_WVALID;      
         rValid [9] <=        AXI_09_ARVALID;     
         
         WDATA  [10] <=        AXI_10_WDATA;       
         WADDR  [10] <=  {1'b0,AXI_10_AWADDR};     
         RADDR  [10] <=  {1'b0,AXI_10_ARADDR};     
         aValid [10] <=        AXI_10_AWVALID;     
         wValid [10] <=        AXI_10_WVALID;      
         rValid [10] <=        AXI_10_ARVALID;    
         
         WDATA  [11] <=        AXI_11_WDATA;      
         WADDR  [11] <=  {1'b0,AXI_11_AWADDR};    
         RADDR  [11] <=  {1'b0,AXI_11_ARADDR};    
         aValid [11] <=        AXI_11_AWVALID;    
         wValid [11] <=        AXI_11_WVALID;     
         rValid [11] <=        AXI_11_ARVALID;    
         
`endif
`ifdef XBAR 
         WDATA  [12] <=        AXI_12_WDATA;
         WADDR  [12] <=  {1'b0,AXI_12_AWADDR};
         RADDR  [12] <=  {1'b0,AXI_12_ARADDR};
         aValid [12] <=        AXI_12_AWVALID;
         wValid [12] <=        AXI_12_WVALID;
         rValid [12] <=        AXI_12_ARVALID;
         
         WDATA  [13] <=        AXI_13_WDATA;       
         WADDR  [13] <=  {1'b0,AXI_13_AWADDR};     
         RADDR  [13] <=  {1'b0,AXI_13_ARADDR};     
         aValid [13] <=        AXI_13_AWVALID;     
         wValid [13] <=        AXI_13_WVALID;      
         rValid [13] <=        AXI_13_ARVALID;     
         
         WDATA  [14] <=        AXI_14_WDATA;       
         WADDR  [14] <=  {1'b0,AXI_14_AWADDR};     
         RADDR  [14] <=  {1'b0,AXI_14_ARADDR};     
         aValid [14] <=        AXI_14_AWVALID;     
         wValid [14] <=        AXI_14_WVALID;      
         rValid [14] <=        AXI_14_ARVALID;    
         
         WDATA  [15] <=        AXI_15_WDATA;      
         WADDR  [15] <=  {1'b0,AXI_15_AWADDR};    
         RADDR  [15] <=  {1'b0,AXI_15_ARADDR};    
         aValid [15] <=        AXI_15_AWVALID;    
         wValid [15] <=        AXI_15_WVALID;     
         rValid [15] <=        AXI_15_ARVALID;    
         
         WDATA  [16] <=        AXI_16_WDATA;
         WADDR  [16] <=  {1'b0,AXI_16_AWADDR};
         RADDR  [16] <=  {1'b0,AXI_16_ARADDR};
         aValid [16] <=        AXI_16_AWVALID;
         wValid [16] <=        AXI_16_WVALID;
         rValid [16] <=        AXI_16_ARVALID;
         
         WDATA  [17] <=        AXI_17_WDATA;       
         WADDR  [17] <=  {1'b0,AXI_17_AWADDR};     
         RADDR  [17] <=  {1'b0,AXI_17_ARADDR};     
         aValid [17] <=        AXI_17_AWVALID;     
         wValid [17] <=        AXI_17_WVALID;      
         rValid [17] <=        AXI_17_ARVALID;     
         
         WDATA  [18] <=        AXI_18_WDATA;       
         WADDR  [18] <=  {1'b0,AXI_18_AWADDR};     
         RADDR  [18] <=  {1'b0,AXI_18_ARADDR};     
         aValid [18] <=        AXI_18_AWVALID;     
         wValid [18] <=        AXI_18_WVALID;      
         rValid [18] <=        AXI_18_ARVALID;    
         
         WDATA  [19] <=        AXI_19_WDATA;      
         WADDR  [19] <=  {1'b0,AXI_19_AWADDR};    
         RADDR  [19] <=  {1'b0,AXI_19_ARADDR};    
         aValid [19] <=        AXI_19_AWVALID;    
         wValid [19] <=        AXI_19_WVALID;     
         rValid [19] <=        AXI_19_ARVALID;    
         
         WDATA  [20] <=        AXI_20_WDATA;
         WADDR  [20] <=  {1'b0,AXI_20_AWADDR};
         RADDR  [20] <=  {1'b0,AXI_20_ARADDR};
         aValid [20] <=        AXI_20_AWVALID;
         wValid [20] <=        AXI_20_WVALID;
         rValid [20] <=        AXI_20_ARVALID;
         
         WDATA  [21] <=        AXI_21_WDATA;       
         WADDR  [21] <=  {1'b0,AXI_21_AWADDR};     
         RADDR  [21] <=  {1'b0,AXI_21_ARADDR};     
         aValid [21] <=        AXI_21_AWVALID;     
         wValid [21] <=        AXI_21_WVALID;      
         rValid [21] <=        AXI_21_ARVALID;     
         
         WDATA  [22] <=        AXI_22_WDATA;       
         WADDR  [22] <=  {1'b0,AXI_22_AWADDR};     
         RADDR  [22] <=  {1'b0,AXI_22_ARADDR};     
         aValid [22] <=        AXI_22_AWVALID;     
         wValid [22] <=        AXI_22_WVALID;      
         rValid [22] <=        AXI_22_ARVALID;    
         
         WDATA  [23] <=        AXI_23_WDATA;      
         WADDR  [23] <=  {1'b0,AXI_23_AWADDR};    
         RADDR  [23] <=  {1'b0,AXI_23_ARADDR};    
         aValid [23] <=        AXI_23_AWVALID;    
         wValid [23] <=        AXI_23_WVALID;     
         rValid [23] <=        AXI_23_ARVALID;    
         
         WDATA  [24] <=        AXI_24_WDATA;
         WADDR  [24] <=  {1'b0,AXI_24_AWADDR};
         RADDR  [24] <=  {1'b0,AXI_24_ARADDR};
         aValid [24] <=        AXI_24_AWVALID;
         wValid [24] <=        AXI_24_WVALID;
         rValid [24] <=        AXI_24_ARVALID;
         
         WDATA  [25] <=        AXI_25_WDATA;       
         WADDR  [25] <=  {1'b0,AXI_25_AWADDR};     
         RADDR  [25] <=  {1'b0,AXI_25_ARADDR};     
         aValid [25] <=        AXI_25_AWVALID;     
         wValid [25] <=        AXI_25_WVALID;      
         rValid [25] <=        AXI_25_ARVALID;     
         
         WDATA  [26] <=        AXI_26_WDATA;       
         WADDR  [26] <=  {1'b0,AXI_26_AWADDR};     
         RADDR  [26] <=  {1'b0,AXI_26_ARADDR};     
         aValid [26] <=        AXI_26_AWVALID;     
         wValid [26] <=        AXI_26_WVALID;      
         rValid [26] <=        AXI_26_ARVALID;    
         
         WDATA  [27] <=        AXI_27_WDATA;      
         WADDR  [27] <=  {1'b0,AXI_27_AWADDR};    
         RADDR  [27] <=  {1'b0,AXI_27_ARADDR};    
         aValid [27] <=        AXI_27_AWVALID;    
         wValid [27] <=        AXI_27_WVALID;     
         rValid [27] <=        AXI_27_ARVALID;    
         
         WDATA  [28] <=        AXI_28_WDATA;
         WADDR  [28] <=  {1'b0,AXI_28_AWADDR};
         RADDR  [28] <=  {1'b0,AXI_28_ARADDR};
         aValid [28] <=        AXI_28_AWVALID;
         wValid [28] <=        AXI_28_WVALID;
         rValid [28] <=        AXI_28_ARVALID;
         
         WDATA  [29] <=        AXI_29_WDATA;       
         WADDR  [29] <=  {1'b0,AXI_29_AWADDR};     
         RADDR  [29] <=  {1'b0,AXI_29_ARADDR};     
         aValid [29] <=        AXI_29_AWVALID;     
         wValid [29] <=        AXI_29_WVALID;      
         rValid [29] <=        AXI_29_ARVALID;     
         
         WDATA  [30] <=        AXI_30_WDATA;       
         WADDR  [30] <=  {1'b0,AXI_30_AWADDR};     
         RADDR  [30] <=  {1'b0,AXI_30_ARADDR};     
         aValid [30] <=        AXI_30_AWVALID;     
         wValid [30] <=        AXI_30_WVALID;      
         rValid [30] <=        AXI_30_ARVALID;    
         
         WDATA  [31] <=        AXI_31_WDATA;      
         WADDR  [31] <=  {1'b0,AXI_31_AWADDR};    
         RADDR  [31] <=  {1'b0,AXI_31_ARADDR};    
         aValid [31] <=        AXI_31_AWVALID;    
         wValid [31] <=        AXI_31_WVALID;     
         rValid [31] <=        AXI_31_ARVALID;    
       `endif  
         
    end
    
   end
   

endmodule
