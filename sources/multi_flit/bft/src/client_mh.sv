`timescale 1ns / 1ps
`include "commands.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/21/2020 05:56:08 PM
// Design Name: 
// Module Name: client_mh
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module client_mh#(

	parameter N 	= 2,		// total number of clients
	parameter M_A_W = 16,
	parameter A_W	= $clog2(N)+1,	// address width
	parameter P_W	= 32,		// data width
	parameter D_W   = 1+M_A_W+A_W+P_W,
	parameter WRAP  = 1,            // wrapping means throttling of reinjection
	parameter PAT   = 0,      // default RANDOM pattern
	parameter RATE  = 100,           // rate of injection (in percent) 
	parameter LIMIT = 16,           // when to stop injectin packets
	parameter SIGMA = 4,            // radius for LOCAL traffic
	parameter posx 	= 2,		// position
	parameter filename = "posx",
	parameter HBM_base_port = posx,
	parameter hop_addr = posx,
    parameter X_W		= 2,
	parameter Y_W		= 2,
	parameter HBM_BASE_OFFSET = posx
	
)


(
	input	wire			clk,
	input	wire			rst,
	input	wire			ce,
    input	wire	`Cmd    cmd,
	(*dont_touch ="true"*)input	wire	[D_W:0]	c_i,
	(*dont_touch ="true"*)input	wire			c_i_v,
	(*dont_touch ="true"*)output	wire			c_i_bp,
	(*dont_touch ="true"*)output	reg	[D_W:0]	    c_o=0,
	(*dont_touch ="true"*)output	reg			    c_o_v=0,
	(*dont_touch ="true"*)input	wire			c_o_bp,
	output	wire			done
);
/* -------------------------------------------------------------------------- */
localparam base = (posx<N/2)?(2*(posx)):(2*(posx)-N+1);//(posx);

(*dont_touch ="true"*)reg rst_r = 0;
always@(posedge clk) begin
    rst_r <= rst;
end

generate 
 if(`FP==1) begin
always @(posedge clk)begin

        c_o <= c_i;
        c_o_v <= c_i_v;

    if(rst_r)
        c_o_v <= 1'b0;
end
end 
else if(base<24)
begin

//localparam base = ((posx<N/8)?(8*(posx)+HBM_BASE_OFFSET):(8*(posx)-28+HBM_BASE_OFFSET));//HBM_base_port;//(posx<N/2)?(2*(posx)):(2*(posx)-N+1);//(posx);
//localparam base = 
localparam LEVELS	= $clog2(N);
localparam excess   = 0;// LEVELS - 5;

function [4:0] bitrev(input [4:0] i);
		bitrev = {i[0],i[1],i[2],i[3],i[4]};
endfunction

function [3:0] log2(input [4:0] i);
		case (i)
          1: log2=0;	
		  2: log2=1;
		  4: log2=2;
		  8: log2=3;
		  16:log2=4;
		  default:log2=1;
		endcase
endfunction
/* -------------------------------------------------------------------------- */
reg start = 1'b0;
reg start_p1 = 1'b0;
reg [4:0] cnt = 5'b0;
reg [4:0] len = 4'b0;
reg [63:0] write_burst_cnt= 0;
reg [63:0] read_cnt = 0;
reg [4:0] exp_base = base;
reg [4:0] port_id  = base;
reg [4:0] write_base = base;
reg [27:0] offset = 28'h0000000; //posx==1?33'h100000000:33'h1f0000000;
reg [63:0] read_response = 0;
enum {IDLE,HEAD,DATA,DATA_L,READ,STAT} state;
reg [255:0] now = 0;
reg error = 0;
reg `Cmd cmd_r = 0;
//wire txfer_done = ~c_o_bp & c_o_v;
wire txfer_done = ~c_o_bp;
reg  [15:0] r_dest_lfsr = posx+2;
wire [7:0] r_dest = r_dest_lfsr[7:0];
reg[32:0] start_addr = 0;
wire [7:0] r;
reg [1:0] stall = 2'd3;
wire [7:0] axi_len = {4'b0,cmd_r`burst_len_cmd};
wire [4:0] trans_len = axi_len+1'b1;
wire [63:0] burst_cnt = 1 << (`packet_count-log2(cmd_r`burst_len_cmd+1'b1));
reg [15:0] r_len_cnt=0;
/* -------------------------------------------------------------------------- */

//lfsr #(.x(posx+3)) rate_rand(.CLK(clk),.RESET(rst),.RAND(r),.enable(0));
//wire rate = (r <= cmd`rate)?1:0;
/* -------------------------------------------------------------------------- */
initial begin 
    state = IDLE;
end
assign c_i_bp = 1'b0;

always @(posedge clk) begin
    if(rst) 
        now <= 0;
    else
        now <= now+1; 
end
/* -------------------------------------------------------------------------- */
always@(posedge clk) begin


    start_p1 <= 1'b1;
    start <= start_p1;
    cmd_r <= cmd;
//    len <= cmd_r`burst_len_cmd+1'b1;
    if(rst) begin
        start <= 0;
        start_p1 <= 0; 
    end
end

reg test = 0;
always @(posedge clk) begin
    if(!rst) begin
        if(c_o[246:242] >=24& c_o_v) begin
            test <= 1;
        end
    end
end
always@(posedge clk) begin:fsm
if(rst) begin
    state <= IDLE;
    c_o_v <= 1'b0;
    write_base <= 0;
    offset <= 0;
    read_response <= 0;
    write_burst_cnt <= 0;
    cnt <= 0;
    read_cnt <= 0;
    error <= 0;
    stall <= 2'd3;
end else begin
    case(state)
    /*Idle state*/
    IDLE:begin
        state <= IDLE;
        if(start && write_burst_cnt < burst_cnt) begin
            state <= HEAD;
            c_o[D_W] <= 1'b0; // last bit;
            c_o_v <= 1'b1; // valid;
            c_o[`dest_addr] <= 6'd32; // dest address
            c_o[`src_addr]  <= hop_addr; // src address 
            c_o[`mem_addr_bft]  <= {port_id,offset}; // target memory address
            c_o[`len] <= axi_len; // length of transactin // not to be confused with length of stream.
            c_o[D_W-1] <= 1'b1; // head bit
            c_o[D_W-2] <= 1'b1; // RW
            c_o[D_W-3] <= 1'b0;//vc
            len <= trans_len;
            write_base <= port_id;
        end
        else if(write_burst_cnt == burst_cnt && read_cnt < burst_cnt) begin
            state <= READ;
            c_o[D_W] <= 1'b1; // last bit;
            c_o_v <= 1'b1 ; // valid;
            c_o[`dest_addr] <= 6'd32; // dest address
            c_o[`src_addr]  <= hop_addr; // src address 
            c_o[`mem_addr_bft]  <= {port_id,offset}; // target memory address
            c_o[`len] <= axi_len; // length of transactin // not to be confused with length of stream.
            c_o[D_W-1] <= 1'b1; // head bit
            c_o[D_W-2] <= 1'b0; // RW
            c_o[D_W-3] <= 1'b0;//vc
            len <= trans_len;
            offset <= offset + 32*(cmd_r`burst_len_cmd+1);
            stall <= stall >>1;
            if(stall!=0) begin
                c_o_v <= 1'b0;
                state <= IDLE;
                offset <= offset;
            end
        end
        if(read_response >= burst_cnt) begin
            state <= HEAD;
            c_o[D_W] <= 1'b0; // last bit;
            c_o_v <= 1'b1; // valid;
            c_o[`dest_addr] <= 6'd32; // dest address
            c_o[`src_addr]  <= hop_addr; // src address 
            c_o[`mem_addr_bft]  <= {base[LEVELS-1:excess],28'b0}; // target memory address
            c_o[`len] <= 8'd0; // length of transactin // not to be confused with length of stream.
            c_o[D_W-1] <= 1'b1; // head bit
            c_o[D_W-2] <= 1'b1; // RW
            c_o[D_W-3] <= 1'b0;//vc
        end
        
    end
    /*head state*/
    HEAD:begin
        state <= HEAD; 
        c_o <= c_o;
        c_o_v <= c_o_v;
        if(txfer_done) begin
            
            c_o[D_W] <= 1'b0;// last_bit
            c_o[D_W-1] <= 1'b0; // head bit
            c_o[D_W-2] <= 1'b0; // RW
            c_o[D_W-3] <= 1'b0;//vc
            c_o[D_W:0] <= {223'b0,{write_base,offset}};// data
            c_o_v <= 1'b1;// valid bit
            state <= DATA;
            offset <= offset + 32;
            if(c_o[`len] == 0 && read_response >= burst_cnt) begin
                c_o[D_W:0] <= 0;//{191'b0,error,now};
                c_o[255:0] <= (now | error<<65);
                c_o[D_W] <= 1'b1;// last_bit
                c_o_v <= 1'b1;// valid bit
                state <= DATA_L;
                read_response <= 0;
            end
            else if(c_o[`len] == 0) begin
                c_o[D_W:0] <= {223'b0,{write_base,offset}};// data
                c_o_v <= 1'b1;// valid bit
                c_o[D_W] <= 1'b1;// last_bit
                c_o[D_W-1] <= 1'b0; // head bit
                c_o[D_W-2] <= 1'b0; // RW
                c_o[D_W-3] <= 1'b0;//vc
                state <= DATA_L;
                offset <= offset + 32;
            end
            
        end
    end
    /*data state*/
    DATA:begin
        state <= DATA; 
        c_o[D_W:0] <= c_o[D_W:0];
        c_o_v <= c_o_v;
        if(txfer_done && (cnt < len-2)) begin
            c_o[D_W:0] <= {223'b0,{write_base,offset}};
            c_o_v <= 1'b1;
            c_o[D_W] <= 1'b0;
            cnt <= cnt + 1;
            offset <= offset + 32;
            state <= DATA;
        end
        else if(txfer_done && (cnt == len-2)) begin
            c_o[D_W:0] <= {223'b0,{write_base,offset}};
            c_o_v <= 1'b1;
            c_o[D_W] <= 1'b1; //last bit set
            offset <= offset + 32;
            state <= DATA_L;
        end
    end
    
        /*last data state*/
    DATA_L:begin
        state <= DATA_L; 
        c_o[D_W:0] <= c_o[D_W:0];
        c_o_v <= c_o_v;
        if(txfer_done) begin
            c_o[D_W:0] <= 0;
            c_o_v <= 1'b0;
            state <= IDLE;
            cnt <= 0;
            write_burst_cnt <= write_burst_cnt + 1'b1;
            if(write_burst_cnt == burst_cnt-1)
                offset <= 0;
            else begin
                state <= HEAD;
                c_o[D_W] <= 1'b0; // last bit;
                c_o_v <= 1'b1; // valid;
                c_o[`dest_addr] <= 6'd32; // dest address
                c_o[`src_addr]  <= hop_addr; // src address 
                c_o[`mem_addr_bft]  <= {port_id,offset}; // target memory address
                c_o[`len] <= axi_len; // length of transactin // not to be confused with length of stream.
                c_o[D_W-1] <= 1'b1; // head bit
                c_o[D_W-2] <= 1'b1; // RW
                c_o[D_W-3] <= 1'b0;//vc
                len <= trans_len;
                write_base <= port_id;
              
            end    
            if(write_burst_cnt == burst_cnt)begin
                state <= IDLE;
                c_o_v <= 1'b0;
            end
        end
    end
    
    READ:begin
        state <= READ; 
        c_o[D_W:0] <= c_o[D_W:0];
        c_o_v <= c_o_v;
        if(txfer_done) begin
            c_o[D_W:0] <= 0;
            c_o_v <= 1'b0;
            state <= IDLE;
            offset <= offset+32*(cmd_r`burst_len_cmd+1);
            read_cnt <= read_cnt + 1'b1;
            if(read_cnt == burst_cnt-1)
                offset <= 0;
            else begin
                state <= READ;
                c_o[D_W] <= 1'b1; // last bit;
                c_o_v <= 1'b1; // valid;
                c_o[`dest_addr] <= 6'd32; // dest address
                c_o[`src_addr]  <= hop_addr; // src address 
                c_o[`mem_addr_bft]  <= {port_id,offset}; // target memory address
                c_o[`len] <= axi_len; // length of transactin // not to be confused with length of stream.
                c_o[D_W-1] <= 1'b1; // head bit
                c_o[D_W-2] <= 1'b0; // RW
                c_o[D_W-3] <= 1'b0;//vc
                len <= trans_len;
             end 
             
 
            
             if(read_cnt == burst_cnt)begin
                state <= IDLE;
                c_o_v <= 1'b0;  
             end
        end
    end
    
    endcase
    if(c_i_v) begin
        
       if(c_i[D_W-1]) begin
            start_addr <= c_i[M_A_W-1:0];
            if(base==21)
                $display("Read response %0d Received at %0d: %x",read_response,HBM_base_port,c_i[M_A_W-1:0]);  
       end
        else begin
         if(base==21)
                $display("Read response %0d Received at %0d: %x",read_response,HBM_base_port,c_i[M_A_W-1:0]);  
            if((start_addr != c_i[M_A_W-1:0])&&start_addr[27:0]!=0&&c_i[255:0]==0)
                error <= 1'b1;
            start_addr <= start_addr + 32;
            r_len_cnt <= r_len_cnt + 1'b1;
        end
       if(c_i[D_W]) begin
        r_len_cnt <= 1'b0;
        read_response <= read_response+1'b1;
        if(r_len_cnt != axi_len)
            error <= 1'b1;
       end
    end
    if(txfer_done & c_o_v) begin
       if(c_o[D_W-1] && base==21) 
            $display("Sent RW:%b at %d : %x",c_o[D_W-2],HBM_base_port,c_o[`mem_addr_bft]);
//        if(c_o[`mem_addr_bft]=='h0d0006280 && base==12) 
//            $display("Sent at %d : %x",posx,c_o);
//       else 
//            
    end
  end
end
/* -------------------------------------------------------------------------- */
integer iter;
always@(posedge clk) begin:port
if(rst) begin
    port_id <= base;
    exp_base <= base;
    
           case(cmd_r`channel)
     `cb: begin
//          if((((base[LEVELS-1:excess]) >> 2)&1'b1) == 0) begin
//            port_id[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 4);
//            exp_base[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 4);
            
//          end
//          else begin
//            port_id[LEVELS-1:excess] <= (base[LEVELS-1:excess] - 4);
//            exp_base[LEVELS-1:excess] <= (base[LEVELS-1:excess] - 4);
//          end
            if(((base>>2)==5)) begin
                port_id <= base - 4;
                exp_base <= base - 4;
                
            end
            else begin
                port_id <= base + 4;
                exp_base <= base + 4;
                if((base>>2)==3) begin
                    port_id <= (base + 4)%16;
                    exp_base <=(base + 4)%16;
                end
            end
      end
      `cs: begin
//            port_id[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16)  ;
//            exp_base[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16) ;
            
//            if((base[LEVELS-1:excess]+16)>23) begin
//                port_id[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16) - 24 ;
//                exp_base[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16) - 24;
//            end

            if(base>15) begin
                port_id <= base - 16;
                exp_base <= base - 16;
            end
            else if(base <8) begin
                port_id <= base + 16;
                exp_base <= base + 16;
            end
            else begin
                port_id <= base + 8;
                exp_base <= base + 8;
            end
      end
      `br:  begin
            port_id[LEVELS-1:excess] <=  bitrev(base[LEVELS-1:excess]);
            exp_base[LEVELS-1:excess] <=  bitrev(base[LEVELS-1:excess]);
      end
      `nn: begin 
            port_id[LEVELS-1:excess] <= (base[LEVELS-1:excess]+1'b1);
            exp_base[LEVELS-1:excess] <= (base[LEVELS-1:excess]+1'b1);
            if((base[LEVELS-1:excess]+1)>23) begin
                port_id[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16) - 24 ;
                exp_base[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16) - 24;
            end
      end
      `to: begin
            port_id[LEVELS-1:excess] <= ((base[LEVELS-1:excess]) + (24/2) - 1);
            exp_base[LEVELS-1:excess] <= ((base[LEVELS-1:excess]) + (24/2) - 1);
            if(((base[LEVELS-1:excess]) + (24/2)-1)>23) begin
                port_id[LEVELS-1:excess] <= ((base[LEVELS-1:excess]) + (24/2) - 1) - 24 ;
                exp_base[LEVELS-1:excess] <= ((base[LEVELS-1:excess]) + (24/2) - 1) - 24;
            end
      end
      `cc: begin
            port_id[LEVELS-1:excess] <= 24 - 1 - (base[LEVELS-1:excess]);
            exp_base[LEVELS-1:excess] <= 24 - 1 - (base[LEVELS-1:excess]);
      end
      default : begin	    
            port_id[LEVELS-1:excess] <= (base[LEVELS-1:excess]);
            exp_base[LEVELS-1:excess] <= (base[LEVELS-1:excess]);
      end
   endcase
 end else begin
 
   
    case(cmd_r`channel)
        `cs:begin
           port_id[LEVELS-1:excess] <= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1);
           if(exp_base[LEVELS-1:excess] < 16) begin
             if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))) < (cmd_r`sigma>>1))
                 port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) + 16;
                 else if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))-(cmd_r`sigma>>1))>(16)-1)
                     port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) - 16;
           end
           else begin
                  if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))) -(cmd_r`sigma>>1) < 16 )
                     port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) + 8;
                 else if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))-(cmd_r`sigma>>1))>(24)-1)
                     port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) - 8;
             end
        end
        `cb:begin
              
            port_id[LEVELS-1:excess] <= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1);
            
            if(exp_base[LEVELS-1:excess] < 4) begin
               if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))) < (cmd_r`sigma>>1))
                     port_id[LEVELS-1:excess] <= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) + 4;
                 else if((exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1))-(cmd_r`sigma>>1))>(4)-1)
                     port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) - 4;
             end
             
             else begin
                  if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))) -(cmd_r`sigma>>1) < (exp_base[LEVELS-1:excess] - (exp_base[LEVELS-1:excess] % 4)))
                     port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) + 4;
                 else if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))-(cmd_r`sigma>>1))>(exp_base[LEVELS-1:excess]+ (3-(exp_base[LEVELS-1:excess] % 4))))
                     port_id[LEVELS-1:excess] <= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) - 4;
            
            end
        end
        
        default: begin
             port_id[LEVELS-1:excess] <= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1);
             if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))) < (cmd_r`sigma>>1))
                 port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) + 24;
             else if((exp_base[LEVELS-1:excess]+ (r_dest&(cmd_r`sigma-1))-(cmd_r`sigma>>1))>24-1)
                 port_id[LEVELS-1:excess]<= exp_base[LEVELS-1:excess] + (r_dest&(cmd_r`sigma-1)) - (cmd_r`sigma>>1) - 24;
        end
   endcase  
   if(state!= IDLE && (~txfer_done ))
    port_id <= port_id;       
   end
end

always @(posedge clk) begin:lfsr
if(rst) begin
   r_dest_lfsr <= posx+2;
end else begin
   if((state == DATA_L || state==READ) && txfer_done ) begin
        r_dest_lfsr[0] <= r_dest_lfsr[15] ^ r_dest_lfsr[13] ^ r_dest_lfsr[12] ^ r_dest_lfsr[10];
        for(iter=0;iter<15;iter=iter+1)
            r_dest_lfsr[iter+1]<=r_dest_lfsr[iter];
       if( write_burst_cnt == burst_cnt-1 ) 
            r_dest_lfsr <= posx+2;
   end
   
   if((state == IDLE) && stall ==0) begin
        r_dest_lfsr[0] <= r_dest_lfsr[15] ^ r_dest_lfsr[13] ^ r_dest_lfsr[12] ^ r_dest_lfsr[10];
        for(iter=0;iter<15;iter=iter+1)
            r_dest_lfsr[iter+1]<=r_dest_lfsr[iter];
   end
   if(  state == DATA && write_burst_cnt == burst_cnt-1 && cnt==(len-2) ) 
            r_dest_lfsr <= posx+2;

end
end
end
else begin
    assign c_i_bp = 1'b0;
    always @(posedge clk) begin
        c_o<=0;
    end
end
endgenerate
endmodule
