// Mux direction controls
`define NONE 	3'b000
`define LEFT 	3'b100
`define RIGHT 	3'b101
`define U0 	3'b110
`define U1 	3'b111
`define Msg_W 261 

`define v		    [`Msg_W-1] // 260
`define l           [`Msg_W-2] // 259
`define h           [`Msg_W-3] // 258
`define rw          [`Msg_W-4] // 257
`define vc          [`Msg_W-5] // 256 

`define offset_len 202
`define dest_addr A_W+`offset_len-1:`offset_len
`define src_addr  2*A_W+`offset_len-1:A_W+`offset_len
`define addr_info 1+1+1+1+8+M_A_W+2*A_W+`offset_len:`offset_len
`define addr_info_len 1+1+1+1+8+M_A_W+2*A_W
`define len 8+M_A_W+2*A_W+`offset_len-1:M_A_W+2*A_W+`offset_len
`define mem_addr M_A_W+2*A_W+`offset_len-1:2*A_W+`offset_len
`define mem_addr_bft M_A_W+2*A_W+`offset_len-1:2*A_W+`offset_len
`define port_bft M_A_W+2*A_W+`offset_len-1:M_A_W+2*A_W+`offset_len-5

`define size 256
`define pe_cnt 32
`define block_size `size/`pe_cnt
`define req_iter 2