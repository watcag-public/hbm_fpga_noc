
delete_pblock [get_pblocks  pblock_ddr4_mem00]
delete_pblock [get_pblocks  pblock_ddr4_mem01]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4]*}]
#add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *inst_control_s_axi*}]



add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -filter {NAME =~ *ys[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -filter {NAME =~ *ys[3]*}]


add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -filter {NAME =~ *ys[2]*}]

create_pblock cli0_cli1_cli2_cli3_cli4_cli5
resize_pblock cli0_cli1_cli2_cli3_cli4_cli5 -add CLOCKREGION_X0Y2:CLOCKREGION_X6Y3
add_cells_to_pblock [get_pblocks cli0_cli1_cli2_cli3_cli4_cli5] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/*}]
add_cells_to_pblock [get_pblocks cli0_cli1_cli2_cli3_cli4_cli5] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/n2.ls[2]*}]
create_pblock cli0_cli1_cli2_cli3
resize_pblock cli0_cli1_cli2_cli3 -add CLOCKREGION_X0Y2:CLOCKREGION_X3Y3
add_cells_to_pblock [get_pblocks cli0_cli1_cli2_cli3] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/n2.ls[1].ms[0]*}]
create_pblock cli0_cli1
resize_pblock cli0_cli1 -add CLOCKREGION_X0Y2:CLOCKREGION_X1Y3
add_cells_to_pblock [get_pblocks cli0_cli1] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/xs[0]*}]
create_pblock cli2_cli3
resize_pblock cli2_cli3 -add CLOCKREGION_X2Y2:CLOCKREGION_X3Y3
add_cells_to_pblock [get_pblocks cli2_cli3] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/xs[1]*}]
create_pblock cli4_cli5
resize_pblock cli4_cli5 -add CLOCKREGION_X4Y2:CLOCKREGION_X6Y3
add_cells_to_pblock [get_pblocks cli4_cli5] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/xs[2]*}]
add_cells_to_pblock [get_pblocks cli4_cli5] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/xs[3]*}]
add_cells_to_pblock [get_pblocks cli4_cli5] [get_cells -hier -filter {NAME =~ *ys[0].row_bft/n2.ls[1].ms[1]*}]

create_pblock cli6_cli7_cli8_cli9_cli10_cli11
resize_pblock cli6_cli7_cli8_cli9_cli10_cli11 -add CLOCKREGION_X0Y4:CLOCKREGION_X6Y5
add_cells_to_pblock [get_pblocks cli6_cli7_cli8_cli9_cli10_cli11] [get_cells -hier -filter {NAME =~ *ys[3].row_bft/n2.ls[2]*}]
create_pblock cli6_cli7_cli8_cli9
resize_pblock cli6_cli7_cli8_cli9 -add CLOCKREGION_X0Y4:CLOCKREGION_X3Y5
add_cells_to_pblock [get_pblocks cli6_cli7_cli8_cli9] [get_cells -hier -filter {NAME =~ *ys[3].row_bft/n2.ls[1].ms[0]*}]

create_pblock cli6_cli7
resize_pblock cli6_cli7 -add CLOCKREGION_X0Y4:CLOCKREGION_X1Y5
add_cells_to_pblock [get_pblocks cli6_cli7] [get_cells -hier -filter {NAME =~ *ys[3].row_bft/xs[0]*}]
create_pblock cli8_cli9
resize_pblock cli8_cli9 -add CLOCKREGION_X2Y4:CLOCKREGION_X3Y5
add_cells_to_pblock [get_pblocks cli8_cli9] [get_cells -hier -filter {NAME =~ *ys[3].row_bft/xs[1]*}]
create_pblock cli10_cli11
resize_pblock cli10_cli11 -add CLOCKREGION_X4Y4:CLOCKREGION_X6Y5
add_cells_to_pblock [get_pblocks cli10_cli11] [get_cells -hier -filter {NAME =~ *ys[3].row_bft/xs[2]*}]
add_cells_to_pblock [get_pblocks cli10_cli11] [get_cells -hier -filter {NAME =~ *ys[3].row_bft/xs[3]*}]
add_cells_to_pblock [get_pblocks cli10_cli11] [get_cells -hier -filter {NAME =~ *ys[3].row_bft/n2.ls[1].ms[1]*}]


create_pblock cli12_cli13_cli14_cli15_cli16_cli17
resize_pblock cli12_cli13_cli14_cli15_cli16_cli17 -add CLOCKREGION_X0Y6:CLOCKREGION_X6Y7
add_cells_to_pblock [get_pblocks cli12_cli13_cli14_cli15_cli16_cli17] [get_cells -hier -filter {NAME =~ *ys[1].row_bft/n2.ls[2]*}]
create_pblock cli12_cli13_cli14_cli15
resize_pblock cli12_cli13_cli14_cli15 -add CLOCKREGION_X0Y6:CLOCKREGION_X3Y7
add_cells_to_pblock [get_pblocks cli12_cli13_cli14_cli15] [get_cells -hier -filter {NAME =~ *ys[1].row_bft/n2.ls[1].ms[0]*}]

create_pblock cli12_cli13
resize_pblock cli12_cli13 -add CLOCKREGION_X0Y6:CLOCKREGION_X1Y7
add_cells_to_pblock [get_pblocks cli12_cli13] [get_cells -hier -filter {NAME =~ *ys[1].row_bft/xs[0]*}]
create_pblock cli14_cli15
resize_pblock cli14_cli15 -add CLOCKREGION_X2Y6:CLOCKREGION_X3Y7
add_cells_to_pblock [get_pblocks cli14_cli15] [get_cells -hier -filter {NAME =~ *ys[1].row_bft/xs[1]*}]
create_pblock cli16_cli17
resize_pblock cli16_cli17 -add CLOCKREGION_X4Y6:CLOCKREGION_X6Y7
add_cells_to_pblock [get_pblocks cli16_cli17] [get_cells -hier -filter {NAME =~ *ys[1].row_bft/xs[2]*}]
add_cells_to_pblock [get_pblocks cli16_cli17] [get_cells -hier -filter {NAME =~ *ys[1].row_bft/xs[3]*}]
add_cells_to_pblock [get_pblocks cli16_cli17] [get_cells -hier -filter {NAME =~ *ys[1].row_bft/n2.ls[1].ms[1]*}]

create_pblock cli18_cli19_cli20_cli21_cli22_cli23
resize_pblock cli18_cli19_cli20_cli21_cli22_cli23 -add CLOCKREGION_X0Y8:CLOCKREGION_X6Y9
add_cells_to_pblock [get_pblocks cli18_cli19_cli20_cli21_cli22_cli23] [get_cells -hier -filter {NAME =~ *ys[2].row_bft/n2.ls[2]*}]
create_pblock cli18_cli19_cli20_cli21
resize_pblock cli18_cli19_cli20_cli21 -add CLOCKREGION_X0Y8:CLOCKREGION_X3Y9
add_cells_to_pblock [get_pblocks cli18_cli19_cli20_cli21] [get_cells -hier -filter {NAME =~ *ys[2].row_bft/n2.ls[1].ms[0]*}]

create_pblock cli18_cli19
resize_pblock cli18_cli19 -add CLOCKREGION_X0Y8:CLOCKREGION_X1Y9
add_cells_to_pblock [get_pblocks cli18_cli19] [get_cells -hier -filter {NAME =~ *ys[2].row_bft/xs[0]*}]
create_pblock cli20_cli21
resize_pblock cli20_cli21 -add CLOCKREGION_X2Y8:CLOCKREGION_X3Y9
add_cells_to_pblock [get_pblocks cli20_cli21] [get_cells -hier -filter {NAME =~ *ys[2].row_bft/xs[1]*}]
create_pblock cli22_cli23
resize_pblock cli22_cli23 -add CLOCKREGION_X4Y8:CLOCKREGION_X6Y9
add_cells_to_pblock [get_pblocks cli22_cli23] [get_cells -hier -filter {NAME =~ *ys[2].row_bft/xs[2]*}]
add_cells_to_pblock [get_pblocks cli22_cli23] [get_cells -hier -filter {NAME =~ *ys[2].row_bft/xs[3]*}]
add_cells_to_pblock [get_pblocks cli22_cli23] [get_cells -hier -filter {NAME =~ *ys[2].row_bft/n2.ls[1].ms[1]*}]


############
create_pblock x4y0
resize_pblock x4y0 -add CLOCKREGION_X0Y0:CLOCKREGION_X0Y1
add_cells_to_pblock [get_pblocks x4y0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[0].cs[*].hop*}]

create_pblock x4y1
resize_pblock x4y1 -add CLOCKREGION_X1Y0:CLOCKREGION_X1Y1
add_cells_to_pblock [get_pblocks x4y1]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[1].cs[*].hop*}]

create_pblock x4y2
resize_pblock x4y2 -add CLOCKREGION_X2Y0:CLOCKREGION_X2Y1
add_cells_to_pblock [get_pblocks x4y2]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[2].cs[*].hop*}]

create_pblock x4y3
resize_pblock x4y3 -add CLOCKREGION_X3Y0:CLOCKREGION_X3Y1
add_cells_to_pblock [get_pblocks x4y3]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[3].cs[*].hop*}]

create_pblock x4y4
resize_pblock x4y4 -add CLOCKREGION_X4Y0:CLOCKREGION_X4Y1
add_cells_to_pblock [get_pblocks x4y4]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[4].cs[*].hop*}]

create_pblock x4y5
resize_pblock x4y5 -add CLOCKREGION_X5Y0:CLOCKREGION_X5Y1
add_cells_to_pblock [get_pblocks x4y5]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[5].cs[*].hop*}]

#########
create_pblock x0y0
resize_pblock x0y0 -add CLOCKREGION_X0Y2:CLOCKREGION_X0Y3
add_cells_to_pblock [get_pblocks x0y0]  [get_cells -hier -filter {NAME =~ *ys[0].xs[0].cs[*].hop*}]

create_pblock x0y1
resize_pblock x0y1 -add CLOCKREGION_X1Y2:CLOCKREGION_X1Y3
add_cells_to_pblock [get_pblocks x0y1]  [get_cells -hier -filter {NAME =~ *ys[0].xs[1].cs[*].hop*}]

create_pblock x0y2
resize_pblock x0y2 -add CLOCKREGION_X2Y2:CLOCKREGION_X2Y3
add_cells_to_pblock [get_pblocks x0y2]  [get_cells -hier -filter {NAME =~ *ys[0].xs[2].cs[*].hop*}]

create_pblock x0y3
resize_pblock x0y3 -add CLOCKREGION_X3Y2:CLOCKREGION_X3Y3
add_cells_to_pblock [get_pblocks x0y3]  [get_cells -hier -filter {NAME =~ *ys[0].xs[3].cs[*].hop*}]

create_pblock x0y4
resize_pblock x0y4 -add CLOCKREGION_X4Y2:CLOCKREGION_X4Y3
add_cells_to_pblock [get_pblocks x0y4]  [get_cells -hier -filter {NAME =~ *ys[0].xs[4].cs[*].hop*}]

create_pblock x0y5
resize_pblock x0y5 -add CLOCKREGION_X5Y2:CLOCKREGION_X5Y3
add_cells_to_pblock [get_pblocks x0y5]  [get_cells -hier -filter {NAME =~ *ys[0].xs[5].cs[*].hop*}]

#########
create_pblock x3y0
resize_pblock x3y0 -add CLOCKREGION_X0Y4:CLOCKREGION_X0Y5
add_cells_to_pblock [get_pblocks x3y0]  [get_cells -hier -filter {NAME =~ *ys[3].xs[0].cs[*].hop*}]

create_pblock x3y1
resize_pblock x3y1 -add CLOCKREGION_X1Y4:CLOCKREGION_X1Y5
add_cells_to_pblock [get_pblocks x3y1]  [get_cells -hier -filter {NAME =~ *ys[3].xs[1].cs[*].hop*}]

create_pblock x3y2
resize_pblock x3y2 -add CLOCKREGION_X2Y4:CLOCKREGION_X2Y5
add_cells_to_pblock [get_pblocks x3y2]  [get_cells -hier -filter {NAME =~ *ys[3].xs[2].cs[*].hop*}]

create_pblock x3y3
resize_pblock x3y3 -add CLOCKREGION_X3Y4:CLOCKREGION_X3Y5
add_cells_to_pblock [get_pblocks x3y3]  [get_cells -hier -filter {NAME =~ *ys[3].xs[3].cs[*].hop*}]

create_pblock x3y4
resize_pblock x3y4 -add CLOCKREGION_X4Y4:CLOCKREGION_X4Y5
add_cells_to_pblock [get_pblocks x3y4]  [get_cells -hier -filter {NAME =~ *ys[3].xs[4].cs[*].hop*}]

create_pblock x3y5
resize_pblock x3y5 -add CLOCKREGION_X5Y4:CLOCKREGION_X5Y5
add_cells_to_pblock [get_pblocks x3y5]  [get_cells -hier -filter {NAME =~ *ys[3].xs[5].cs[*].hop*}]

#########
create_pblock x1y0
resize_pblock x1y0 -add CLOCKREGION_X0Y6:CLOCKREGION_X0Y7
add_cells_to_pblock [get_pblocks x1y0]  [get_cells -hier -filter {NAME =~ *ys[1].xs[0].cs[*].hop*}]

create_pblock x1y1
resize_pblock x1y1 -add CLOCKREGION_X1Y6:CLOCKREGION_X1Y7
add_cells_to_pblock [get_pblocks x1y1]  [get_cells -hier -filter {NAME =~ *ys[1].xs[1].cs[*].hop*}]

create_pblock x1y2
resize_pblock x1y2 -add CLOCKREGION_X2Y6:CLOCKREGION_X2Y7
add_cells_to_pblock [get_pblocks x1y2]  [get_cells -hier -filter {NAME =~ *ys[1].xs[2].cs[*].hop*}]

create_pblock x1y3
resize_pblock x1y3 -add CLOCKREGION_X3Y6:CLOCKREGION_X3Y7
add_cells_to_pblock [get_pblocks x1y3]  [get_cells -hier -filter {NAME =~ *ys[1].xs[3].cs[*].hop*}]

create_pblock x1y4
resize_pblock x1y4 -add CLOCKREGION_X4Y6:CLOCKREGION_X4Y7
add_cells_to_pblock [get_pblocks x1y4]  [get_cells -hier -filter {NAME =~ *ys[1].xs[4].cs[*].hop*}]

create_pblock x1y5
resize_pblock x1y5 -add CLOCKREGION_X5Y6:CLOCKREGION_X5Y7
add_cells_to_pblock [get_pblocks x1y5]  [get_cells -hier -filter {NAME =~ *ys[1].xs[5].cs[*].hop*}]


#########
create_pblock x2y0
resize_pblock x2y0 -add CLOCKREGION_X0Y8:CLOCKREGION_X0Y9
add_cells_to_pblock [get_pblocks x2y0]  [get_cells -hier -filter {NAME =~ *ys[2].xs[0].cs[*].hop*}]

create_pblock x2y1
resize_pblock x2y1 -add CLOCKREGION_X1Y8:CLOCKREGION_X1Y9
add_cells_to_pblock [get_pblocks x2y1]  [get_cells -hier -filter {NAME =~ *ys[2].xs[1].cs[*].hop*}]

create_pblock x2y2
resize_pblock x2y2 -add CLOCKREGION_X2Y8:CLOCKREGION_X2Y9
add_cells_to_pblock [get_pblocks x2y2]  [get_cells -hier -filter {NAME =~ *ys[2].xs[2].cs[*].hop*}]

create_pblock x2y3
resize_pblock x2y3 -add CLOCKREGION_X3Y8:CLOCKREGION_X3Y9
add_cells_to_pblock [get_pblocks x2y3]  [get_cells -hier -filter {NAME =~ *ys[2].xs[3].cs[*].hop*}]

create_pblock x2y4
resize_pblock x2y4 -add CLOCKREGION_X4Y8:CLOCKREGION_X4Y9
add_cells_to_pblock [get_pblocks x2y4]  [get_cells -hier -filter {NAME =~ *ys[2].xs[4].cs[*].hop*}]

create_pblock x2y5
resize_pblock x2y5 -add CLOCKREGION_X5Y8:CLOCKREGION_X5Y9
add_cells_to_pblock [get_pblocks x2y5]  [get_cells -hier -filter {NAME =~ *ys[2].xs[5].cs[*].hop*}]




#first_row




#Second_row
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[2-3]\].*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[1\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[0-1]\].*}]





#Third_row
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[2-3]\].*}]


add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[2\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[0-1]\].*}]


#Forth_row
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[2-3]\].*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys\[3\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[0-1]\].*}]



#Fifth_row
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[2-3]\].*}]


add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_d_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_v_reg\[[2-3]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_reg\[[0-1]\].*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -regexp {.*ys_hbm\[4\].xs\[[0-9]\].cs\[[0-9]\].hop\/north\/hr_b_vc_reg\[[0-1]\].*}]




#pblock_dynamic_SLR2




add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *ys[1].xs[*].cs[*].hop/north/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *ys[2].xs[*].cs[*].hop/north/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2] [get_cells -hier -filter {NAME =~ *ys[3].xs[*].cs[*].hop/north/hr_v[0]_i_1*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1] [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[*].cs[*].hop/north/hr_v[0]_i_1*}]
