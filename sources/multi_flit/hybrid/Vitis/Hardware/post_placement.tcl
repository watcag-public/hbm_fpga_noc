
#set_property MAX_FANOUT 100 [get_nets -hier -regexp {.*s_d_r\[[0-9]+]_i.*}]
#phys_opt_design -force_replication_on_nets [get_nets -hier -regexp {.*s_d_r\[[0-9]+]_i.*}] 

phys_opt_design -directive AggressiveExplore
phys_opt_design -slr_crossing_opt -tns_cleanup

phys_opt_design -retime
set_param route.enableGlobalHoldIter 1 
