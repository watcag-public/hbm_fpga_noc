// This is a generated file. Use and modify at your own risk.
////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
Vendor: Xilinx
Associated Filename: main.c
#Purpose: This example shows a basic vector add +1 (constant) by manipulating
#         memory inplace.
*******************************************************************************/

#include <fcntl.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <CL/opencl.h>
#include <CL/cl_ext.h>
#include "xclhal2.h"
#include <chrono>
////////////////////////////////////////////////////////////////////////////////

#define NUM_WORKGROUPS (1)
#define WORKGROUP_SIZE (256)
#define MAX_LENGTH 8192
#define MEM_ALIGNMENT 4096

// #define size_m 192*2
// #define pe_cnt 24
#define values_per_beat 8
#define burst_length 16
// #define block_size size_m / pe_cnt
#define patches size_m / (block_size * pe_cnt)

#if defined(VITIS_PLATFORM) && !defined(TARGET_DEVICE)
#define STR_VALUE(arg) #arg
#define GET_STRING(name) STR_VALUE(name)
#define TARGET_DEVICE GET_STRING(VITIS_PLATFORM)
#endif

typedef unsigned int uint;
cl_uint ** create_matrix(uint rows, uint cols){
  cl_uint** matrix = new cl_uint*[rows];
  for (int i = 0; i < rows; ++i)
      matrix[i] = new cl_uint[cols];
  return matrix;
}

cl_uint * create_array(uint size){
  cl_uint* matrix = new cl_uint[size];
  return matrix;
}

void delete_matrix(cl_uint ** matrix, uint rows){
  for (int i = 0; i < rows; ++i)
      delete [] matrix[i];
  delete [] matrix;
  return;
}

void  delete_array(cl_uint * array){
  delete [] array;
  return;
}

////////////////////////////////////////////////////////////////////////////////

cl_uint load_file_to_memory(const char *filename, char **result)
{
  cl_uint size = 0;
  FILE *f = fopen(filename, "rb");
  if (f == NULL)
  {
    *result = NULL;
    return -1; // -1 means file opening fail
  }
  fseek(f, 0, SEEK_END);
  size = ftell(f);
  fseek(f, 0, SEEK_SET);
  *result = (char *)malloc(size + 1);
  if (size != fread(*result, sizeof(char), size, f))
  {
    free(*result);
    return -2; // -2 means file reading fail
  }
  fclose(f);
  (*result)[size] = 0;
  return size;
}
using namespace std;
int main(int argc, char **argv)
{

  cl_int err; // error code returned from api calls
  cl_uint check_status = 0;

  cl_platform_id platform_id; // platform id
  cl_device_id device_id;     // compute device id
  cl_context context;         // compute context
  cl_command_queue commands;  // compute command queue
  cl_program program;         // compute programs
  cl_kernel kernel;           // compute kernel

  char cl_platform_vendor[1001];
  char target_device_name[1001] = TARGET_DEVICE;

  // static cl_uint A[size_m][size_m];
  uint pe_cnt = 24;
  // uint size_m  = atoi(argv[2]);
  // uint block_size = size_m /pe_cnt;
   
  // static cl_uint** A;
  // A = create_matrix(size_m,size_m);
  // static cl_uint* B,*C,*final,*final2;
  // B = create_array(size_m);
  // C = create_array(size_m);
  // final = create_array(size_m);
  // final2 = create_array(size_m);

  // int count = 0;

  // static cl_uint ** row_ptr, **val, **col_ptr;

  // row_ptr = create_matrix(pe_cnt,block_size + 1);
  // val = create_matrix(pe_cnt,block_size * size_m);
  // col_ptr = create_matrix(pe_cnt,block_size * size_m);

  // uint nzv = 0;
  // int track = 0;
  // for (int i = 0; i < size_m; i++)
  // {
  //   for (int j = 0; j < size_m; j++)
  //   {
  //     uint p = rand()%10000;
  //     A[i][j] = 0;
  //     if (p<15)
  //     {
  //       A[i][j] = rand()%2;
  //     }
  //     else{
  //       track++;
  //     }
  //   }
  //   B[i] = rand() % 3;
  // }
  // int row = 0;
  // cl_uint * nzvA;
  // nzvA = create_array(pe_cnt);
  // for (int e = 0; e < pe_cnt; e++)
  // {
  //   nzvA[e] = 0;
  //   row = (e)*block_size;
  //   for (int i = 0; i < block_size; i++)
  //   {
  //     row_ptr[e][i] = nzvA[e];
  //     for (int j = 0; j < size_m; j++)
  //     {
  //       if (A[i + row][j] != 0)
  //       {
  //         val[e][nzvA[e]] = A[i + row][j];
  //         col_ptr[e][nzvA[e]++] = j;
  //       }
  //     }
  //     cout << hex << row_ptr[e][i] << " ";
  //   }
  //   row_ptr[e][block_size] = nzvA[e];
  //   cout << row_ptr[e][block_size] << " " << endl;
  //   cout << "element " << e << "  " << hex << nzvA[e] << endl;
  //   nzv += nzvA[e];
  //   if(nzvA[e]>3072){
  //     cout<<"Increase sparsity"<<endl;
  //     return -1;
  //   }
  // }

  // for (int i = 0; i < size_m; i++)
  // {
  //   C[i] = 0;
  //   for (int j = 0; j < size_m; j++)
  //   {
  //     C[i] = C[i] + (A[i][j] * B[j]);
  //   }
  // }
  
  // for (int i = 0; i < size_m; i++)
  // {
  //   final2[i] = 0;
  //   for (int j = 0; j < size_m; j++)
  //   {
  //     final2[i] = final2[i] + (A[i][j] * C[j]);
  //   }
  // }

  //   for (int i = 0; i < size_m; i++)
  // {
  //   final[i] = 0;
  //   for (int j = 0; j < size_m; j++)
  //   {
  //     final[i] = final[i] + (A[i][j] * final2[j]);
  //   }
  // }
  // count = 0;
  // cl_uint sclar00_arg = 0;
  // sclar00_arg |=  (size_m & 0x7FFF) | ((block_size & 0x3FF)<<15) |(((pe_cnt-1) & 0x1F)<<25);
  // cout<<"Scalar "<<std::hex<<sclar00_arg<<endl;
  // cout<<"Total nzv"<<std::dec<<nzv<<endl;
  // cout << "Matrix ready" << endl;

  cl_uint **h_A_output = (cl_uint **)aligned_alloc(MEM_ALIGNMENT, pe_cnt * sizeof(cl_uint *));
  cout << "Dp ready" << endl;
  for (int i = 0; i < pe_cnt; i++)
  {
    h_A_output[i] = (cl_uint *)aligned_alloc(MEM_ALIGNMENT, (MAX_LENGTH) * sizeof(cl_uint *)); // host memory for output vector
  }

  cl_mem d_X[32];

  if (argc != 3)
  {
    printf("Usage: %s xclbin cmd\n", argv[0]);
    return EXIT_FAILURE;
  }

  // Fill our data sets with pattern

  // Get all platforms and then select Xilinx platform
  cl_platform_id platforms[16]; // platform id
  cl_uint platform_count;
  cl_uint platform_found = 0;
  err = clGetPlatformIDs(16, platforms, &platform_count);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to find an OpenCL platform!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  printf("INFO: Found %d platforms\n", platform_count);

  // Find Xilinx Plaftorm
  for (cl_uint iplat = 0; iplat < platform_count; iplat++)
  {
    err = clGetPlatformInfo(platforms[iplat], CL_PLATFORM_VENDOR, 1000, (void *)cl_platform_vendor, NULL);
    if (err != CL_SUCCESS)
    {
      printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
      printf("Test failed\n");
      return EXIT_FAILURE;
    }
    if (strcmp(cl_platform_vendor, "Xilinx") == 0)
    {
      printf("INFO: Selected platform %d from %s\n", iplat, cl_platform_vendor);
      platform_id = platforms[iplat];
      platform_found = 1;
    }
  }
  if (!platform_found)
  {
    printf("ERROR: Platform Xilinx not found. Exit.\n");
    return EXIT_FAILURE;
  }

  // Get Accelerator compute device
  cl_uint num_devices;
  cl_uint device_found = 0;
  cl_device_id devices[16]; // compute device id
  char cl_device_name[1001];
  err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &num_devices);
  printf("INFO: Found %d devices\n", num_devices);
  if (err != CL_SUCCESS)
  {
    printf("ERROR: Failed to create a device group!\n");
    printf("ERROR: Test failed\n");
    return -1;
  }

  //iterate all devices to select the target device.
  for (cl_uint i = 0; i < num_devices; i++)
  {
    err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name, 0);
    if (err != CL_SUCCESS)
    {
      printf("Error: Failed to get device name for device %d!\n", i);
      printf("Test failed\n");
      return EXIT_FAILURE;
    }
    printf("CL_DEVICE_NAME %s\n", cl_device_name);
    if (strcmp(cl_device_name, target_device_name) == 0)
    {
      device_id = devices[i];
      device_found = 1;
      printf("Selected %s as the target device\n", cl_device_name);
    }
  }

  if (!device_found)
  {
    printf("Target device %s not found. Exit.\n", target_device_name);
    return EXIT_FAILURE;
  }

  // Create a compute context
  //
  context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
  if (!context)
  {
    printf("Error: Failed to create a compute context!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  // Create a command commands
  commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
  if (!commands)
  {
    printf("Error: Failed to create a command commands!\n");
    printf("Error: code %i\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  cl_int status;

  // Create Program Objects
  // Load binary from disk
  unsigned char *kernelbinary;
  char *xclbin = argv[1];

  //------------------------------------------------------------------------------
  // xclbin
  //------------------------------------------------------------------------------
  printf("INFO: loading xclbin %s\n", xclbin);
  cl_uint n_i0 = load_file_to_memory(xclbin, (char **)&kernelbinary);
  if (n_i0 < 0)
  {
    printf("failed to load kernel from xclbin: %s\n", xclbin);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  size_t n0 = n_i0;

  // Create the compute program from offline
  program = clCreateProgramWithBinary(context, 1, &device_id, &n0,
                                      (const unsigned char **)&kernelbinary, &status, &err);
  free(kernelbinary);

  if ((!program) || (err != CL_SUCCESS))
  {
    printf("Error: Failed to create compute program from binary %d!\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  // Build the program executable
  //
  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
  if (err != CL_SUCCESS)
  {
    size_t len;
    char buffer[2048];

    printf("Error: Failed to build program executable!\n");
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
    printf("%s\n", buffer);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  // Create the compute kernel in the program we wish to run
  //
  kernel = clCreateKernel(program, "bft_32_xbar_v2", &err);
  if (!kernel || err != CL_SUCCESS)
  {
    printf("Error: Failed to create compute kernel!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  // Create structs to define memory bank mapping
  cl_mem_ext_ptr_t mem_ext;
  mem_ext.obj = NULL;
  mem_ext.param = 0;

  for (int i = 0; i < pe_cnt; i++)
  {

    mem_ext.flags = i | XCL_MEM_TOPOLOGY;
    d_X[i] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX, sizeof(cl_uint) * 81920, &mem_ext, &err);
    if (err != CL_SUCCESS)
    {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }

    if (!(d_X[i]))
    {

      printf("Error: Failed to allocate device memory!\n");
      printf("Test failed\n");
      return EXIT_FAILURE;
    }
  }

  cout << "Memory bank mapping done" << endl;
  // for (int i = 0; i < pe_cnt; i++)
  // {
  //   uint currentBaseA = 0;
  //   err = clEnqueueWriteBuffer(commands, d_X[i], CL_TRUE, currentBaseA, sizeof(cl_uint) * (block_size + 1), row_ptr[i], 0, NULL, NULL);
  //   if (err != CL_SUCCESS)
  //   {
  //     printf("Error: Failed to write to source array h_data: d_A: %d!\n", err);
  //     printf("Test failed\n");
  //     return EXIT_FAILURE;
  //   }
  // }


  cout << "Buffer written" << endl;

  // Set the arguments to our compute kernel
  // cl_uint vector_length = MAX_LENGTH;
  err = 0;
  cl_uint d_scalar00 = std::stoul(argv[2], nullptr, 0);
  // cl_uint d_scalar01 = std::stoul(argv[2], nullptr, 0);
  err |= clSetKernelArg(kernel, 0, sizeof(cl_uint), &d_scalar00); // Not used in example RTL logic.
  // err |= clSetKernelArg(kernel, 1, sizeof(cl_uint), &d_scalar01); // Not used in example RTL logic.
  // err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_X[]);
  cout << "Scalar set" << endl;
 for (int i = 1; i < pe_cnt + 1; i++)
  {
    err |= clSetKernelArg(kernel, i, sizeof(cl_mem), &d_X[i - 1]);
  }

  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to set kernel arguments! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  size_t global[1];
  size_t local[1];
  // Execute the kernel over the entire range of our 1d input data set
  // using the maximum number of work group items for this device

  using std::chrono::high_resolution_clock;
  using std::chrono::duration_cast;
  using std::chrono::duration;
  using std::chrono::milliseconds;



  printf("Starting the program \n", err);
  global[0] = 1;
  local[0] = 1;
  err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, (size_t *)&global, (size_t *)&local, 0, NULL, NULL);
  if (err)
  {
    printf("Error: Failed to execute kernel! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
auto t1 = high_resolution_clock::now();
 
  clFinish(commands);
auto t2 = high_resolution_clock::now();
 duration<double, std::milli> ms_double = t2 - t1;
std::cout <<"Time:"<< ms_double.count() << "ms";

  printf("Kernel done \n", err);
  cl_event readevent;

  err = 0;

  for (int i = 0; i < pe_cnt; i++)
  {
    err |= clEnqueueReadBuffer(commands, d_X[i], CL_TRUE, 0, sizeof(cl_uint) * MAX_LENGTH, h_A_output[i], 0, NULL, &readevent);
  }

  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to read output array! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  clWaitForEvents(1, &readevent);
  // Check Results
  printf("Done reading \n", err);

  // int fail = 0;

  // static cl_uint *output_hw;
  // output_hw = create_array(size_m);
  // for (int elements = 0; elements < pe_cnt; elements++)
  // {
  //   for (int n = 0; n < block_size; n++)
  //   {
  //     output_hw[count] = h_A_output[elements][n];
  //     count++;
  //   }
  // }
  // for (int i = 0; i < size_m; i++)
  // {

      
  //     if (output_hw[i] != final[i]){
  //       fail = 1;
  //     std::cout<<dec<< output_hw[i] << " "<<final[i]<<endl;
  //     }
        
  //   // std::cout << std::endl;
  // }
  // // uint fail = 0;
  // if (fail)
  // {
  //   printf("Matrix incorrect \n");
  // }
  // else
  //   printf("Matrix correct \n");

  printf("Comp. done \n", err);

  //--------------------------------------------------------------------------
  // Shutdown and cleanup
  //--------------------------------------------------------------------------

  for (int i = 0; i < pe_cnt; i++)
  {
    free(h_A_output[i]);
    // clReleaseMemObject(d_X[i]);
  }
  free(h_A_output);

  for (int i = 0; i < pe_cnt; i++)
  {

    clReleaseMemObject(d_X[i]);
  }
  cout << "Cleanup done" << endl;

  // free(h_data);
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(commands);
  clReleaseContext(context);

  if (check_status)
  {
    printf("INFO: Test failed\n");
    return EXIT_FAILURE;
  }
  else
  {
    printf("INFO: Test completed successfully.\n");
    return EXIT_SUCCESS;
  }

  // delete_matrix(A,size_m);
  // delete_array(B);
  // delete_array(C);
  // delete_array(final);
  // delete_array(final2);

  // delete_matrix(row_ptr,pe_cnt);
  // delete_matrix(val,pe_cnt);
  // delete_matrix(col_ptr,pe_cnt);
  // delete_array(nzvA);
  // delete_array(output_hw);
} // end of main
