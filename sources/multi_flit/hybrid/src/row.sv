`timescale 1ns / 1ps
`include "bft.vh" 
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/22/2021 06:44:01 PM
// Design Name: 
// Module Name: row
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module row#(	
    parameter WRAP		= 1,
	parameter PAT		= 0,
	parameter N		= 8,
	parameter	P_W	= 256,
	parameter A_W	= $clog2(N)+1,
	parameter M_A_W = 33,
	parameter D_W	= 258,//1+1+M_A_W+A_W+P_W,
	parameter LEVELS	= $clog2(N),
	parameter LIMIT		= 1024,
	parameter RATE		= 0,
	parameter SIGMA		= 0,
	parameter FD		= 32,
	parameter HR    = 1,
	parameter ROWS = 4,
	parameter X_MAX = 1<<3,
	parameter Y_MAX = 1<<3-3,
	parameter HBM_BASE_OFFSET = 0,
	parameter hop_addr = 0,
	parameter row_no = 0)(
	
	input  wire clk,
	input  wire rst,
    input  wire [D_W-1:0] grid_dn_0,
    input  wire grid_dn_v_0,
    input  wire grid_dn_l_0,
    output wire grid_dn_r_0,
    
    output wire [D_W-1:0] grid_up_0,
    output wire grid_up_v_0,
    output wire grid_up_l_0,
    input  wire grid_up_r_0,
 
    input  wire [D_W-1:0] grid_dn_1,
    input  wire grid_dn_v_1,
    input  wire grid_dn_l_1,
    output wire grid_dn_r_1,
    
    output wire [D_W-1:0] grid_up_1,
    output wire grid_up_v_1,
    output wire grid_up_l_1,
    input  wire grid_up_r_1,   
    
    input  wire [D_W-1:0] grid_dn_2,
    input  wire grid_dn_v_2,
    input  wire grid_dn_l_2,
    output wire grid_dn_r_2,
    
    output wire [D_W-1:0] grid_up_2,
    output wire grid_up_v_2,
    output wire grid_up_l_2,
    input  wire grid_up_r_2,
 
    input  wire [D_W-1:0] grid_dn_3,
    input  wire grid_dn_v_3,
    input  wire grid_dn_l_3,
    output wire grid_dn_r_3,
    
    output wire [D_W-1:0] grid_up_3,
    output wire grid_up_v_3,
    output wire grid_up_l_3,
    input  wire grid_up_r_3,     
     
    input  wire [D_W-1:0] grid_dn_4,
    input  wire grid_dn_v_4,
    input  wire grid_dn_l_4,
    output wire grid_dn_r_4,
    
    output wire [D_W-1:0] grid_up_4,
    output wire grid_up_v_4,
    output wire grid_up_l_4,
    input  wire grid_up_r_4,
 
    input  wire [D_W-1:0] grid_dn_5,
    input  wire grid_dn_v_5,
    input  wire grid_dn_l_5,
    output wire grid_dn_r_5,
    
    output wire [D_W-1:0] grid_up_5,
    output wire grid_up_v_5,
    output wire grid_up_l_5,
    input  wire grid_up_r_5,   
    
    input  wire [D_W-1:0] grid_dn_6,
    input  wire grid_dn_v_6,
    input  wire grid_dn_l_6,
    output wire grid_dn_r_6,
    
    output wire [D_W-1:0] grid_up_6,
    output wire grid_up_v_6,
    output wire grid_up_l_6,
    input  wire grid_up_r_6,
 
    input  wire [D_W-1:0] grid_dn_7,
    input  wire grid_dn_v_7,
    input  wire grid_dn_l_7,
    output wire grid_dn_r_7,
    
    output wire [D_W-1:0] grid_up_7,
    output wire grid_up_v_7,
    output wire grid_up_l_7,
    input  wire grid_up_r_7,
    
    input wire `Cmd	cmd_r0,
    input wire ce0,
    
    input wire `Cmd	cmd_r1,
    input wire ce1,
    
    input wire `Cmd	cmd_r2,
    input wire ce2,
    
    input wire `Cmd	cmd_r3,
    input wire ce3,
    
    input wire `Cmd	cmd_r4,
    input wire ce4,
    
    input wire `Cmd	cmd_r5,
    input wire ce5,
    
    input wire `Cmd	cmd_r6,
    input wire ce6,
    
    input wire `Cmd	cmd_r7,
    input wire ce7,
//    input wire [63:0] received[N-1:0]

    output wire [31:0] r0,
    output wire [31:0] r1,
    output wire [31:0] r2,
    output wire [31:0] r3,
    output wire [31:0] r4,
    output wire [31:0] r5,
    output wire [31:0] r6,
    output wire [31:0] r7,

	output wire i0,
    output wire i1,
    output wire i2,
    output wire i3,
    output wire i4,
    output wire i5,
    output wire i6,
    output wire i7,

	input wire s0,
    input wire s1,
    input wire s2,
    input wire s3,
    input wire s4,
    input wire s5,
    input wire s6,
    input wire s7

    );
   
    wire `Cmd	cmd_r[N-1:0];
    wire ce[N-1:0];
    
    wire [31:0] received[N-1:0];
	wire sync[N-1:0];
	wire iter_done[N-1:0];
    
    
    wire	[D_W-1:0]	grid_up		[LEVELS-1:0][2*N-1:0];	
	wire			grid_up_v	[LEVELS-1:0][2*N-1:0];	
	wire 			grid_up_r	[LEVELS-1:0][2*N-1:0];	
	wire 			grid_up_l	[LEVELS-1:0][2*N-1:0];	

	wire 	[D_W-1:0] 	grid_dn 	[LEVELS-1:0][2*N-1:0];	
	wire 		 	grid_dn_v 	[LEVELS-1:0][2*N-1:0];	
	wire 	 		grid_dn_r 	[LEVELS-1:0][2*N-1:0];	
	wire 	 		grid_dn_l 	[LEVELS-1:0][2*N-1:0];	

	wire 	[D_W-1:0] 	peo 		[N-1:0];	
	wire 		 	peo_v 		[N-1:0];	
	wire 	 		peo_r 		[N-1:0];	
	wire 	 		peo_l 		[N-1:0];	

	wire 	[D_W-1:0] 	pei 		[N-1:0];	
	wire 		 	pei_v 		[N-1:0];	
	wire 		 	pei_r 		[N-1:0];	
	wire 		 	pei_l 		[N-1:0];	
	
   genvar i;

        assign grid_dn[LEVELS-1][0] = grid_dn_0;
        assign grid_dn_v[LEVELS-1][0] = grid_dn_v_0;
        assign grid_dn_l[LEVELS-1][0] = grid_dn_l_0;
        assign grid_dn_r_0 = grid_dn_r[LEVELS-1][0];
        
        assign grid_up_0 = grid_up[LEVELS-1][0];
        assign grid_up_v_0 = grid_up_v[LEVELS-1][0];
        assign grid_up_l_0 = grid_up_l[LEVELS-1][0];
        assign grid_up_r[LEVELS-1][0] = grid_up_r_0;
        

        assign grid_dn[LEVELS-1][1] = grid_dn_2;
        assign grid_dn_v[LEVELS-1][1] = grid_dn_v_2;
        assign grid_dn_l[LEVELS-1][1] = grid_dn_l_2;
        assign grid_dn_r_2 = grid_dn_r[LEVELS-1][1];
        
        assign grid_up_2 = grid_up[LEVELS-1][1];
        assign grid_up_v_2 = grid_up_v[LEVELS-1][1];
        assign grid_up_l_2 = grid_up_l[LEVELS-1][1];
        assign grid_up_r[LEVELS-1][1] = grid_up_r_2;
        
        assign grid_dn[LEVELS-1][2] = grid_dn_4;
        assign grid_dn_v[LEVELS-1][2] = grid_dn_v_4;
        assign grid_dn_l[LEVELS-1][2] = grid_dn_l_4;
        assign grid_dn_r_4 = grid_dn_r[LEVELS-1][2];
        
        assign grid_up_4 = grid_up[LEVELS-1][2];
        assign grid_up_v_4 = grid_up_v[LEVELS-1][2];
        assign grid_up_l_4 = grid_up_l[LEVELS-1][2];
        assign grid_up_r[LEVELS-1][2] = grid_up_r_4;
        

        assign grid_dn[LEVELS-1][3] = grid_dn_6;
        assign grid_dn_v[LEVELS-1][3] = grid_dn_v_6;
        assign grid_dn_l[LEVELS-1][3] = grid_dn_l_6;
        assign grid_dn_r_6 = grid_dn_r[LEVELS-1][3];
        
        assign grid_up_6 = grid_up[LEVELS-1][3];
        assign grid_up_v_6 = grid_up_v[LEVELS-1][3];
        assign grid_up_l_6 = grid_up_l[LEVELS-1][3];
        assign grid_up_r[LEVELS-1][3] = grid_up_r_6;
        

        assign grid_dn[LEVELS-1][4] = grid_dn_1;
        assign grid_dn_v[LEVELS-1][4] = grid_dn_v_1;
        assign grid_dn_l[LEVELS-1][4] = grid_dn_l_1;
        assign grid_dn_r_1 = grid_dn_r[LEVELS-1][4];
        
        assign grid_up_1 = grid_up[LEVELS-1][4];
        assign grid_up_v_1 = grid_up_v[LEVELS-1][4];
        assign grid_up_l_1 = grid_up_l[LEVELS-1][4];
        assign grid_up_r[LEVELS-1][4] = grid_up_r_1;
        

        assign grid_dn[LEVELS-1][5] = grid_dn_3;
        assign grid_dn_v[LEVELS-1][5] = grid_dn_v_3;
        assign grid_dn_l[LEVELS-1][5] = grid_dn_l_3;
        assign grid_dn_r_3 = grid_dn_r[LEVELS-1][5];
        
        assign grid_up_3 = grid_up[LEVELS-1][5];
        assign grid_up_v_3 = grid_up_v[LEVELS-1][5];
        assign grid_up_l_3 = grid_up_l[LEVELS-1][5];
        assign grid_up_r[LEVELS-1][5] = grid_up_r_3;
        
        assign grid_dn[LEVELS-1][6] = grid_dn_5;
        assign grid_dn_v[LEVELS-1][6] = grid_dn_v_5;
        assign grid_dn_l[LEVELS-1][6] = grid_dn_l_5;
        assign grid_dn_r_5 = grid_dn_r[LEVELS-1][6];
        
        assign grid_up_5 = grid_up[LEVELS-1][6];
        assign grid_up_v_5 = grid_up_v[LEVELS-1][6];
        assign grid_up_l_5 = grid_up_l[LEVELS-1][6];
        assign grid_up_r[LEVELS-1][6] = grid_up_r_5;
        

        assign grid_dn[LEVELS-1][7] = grid_dn_7;
        assign grid_dn_v[LEVELS-1][7] = grid_dn_v_7;
        assign grid_dn_l[LEVELS-1][7] = grid_dn_l_7;
        assign grid_dn_r_7 = grid_dn_r[LEVELS-1][7];
        
        assign grid_up_7 = grid_up[LEVELS-1][7];
        assign grid_up_v_7 = grid_up_v[LEVELS-1][7];
        assign grid_up_l_7 = grid_up_l[LEVELS-1][7];
        assign grid_up_r[LEVELS-1][7] = grid_up_r_7;
                
        assign cmd_r[0] = cmd_r0;
        assign ce[0] = ce0;
        assign cmd_r[1] = cmd_r1;
        assign ce[1] = ce1;
        assign cmd_r[2] = cmd_r2;
        assign ce[2] = ce2;
        assign cmd_r[3] = cmd_r3;
        assign ce[3] = ce3;
        assign cmd_r[4] = cmd_r4;
        assign ce[4] = ce4;
        assign cmd_r[5] = cmd_r5;
        assign ce[5] = ce5;
        assign cmd_r[6] = cmd_r6;
        assign ce[6] = ce6;
        assign cmd_r[7] = cmd_r7;
        assign ce[7] = ce7;        
        
        assign r0 = received[0];
        assign r1 = received[1];
        assign r2 = received[2];
        assign r3 = received[3];
        assign r4 = received[4];
        assign r5 = received[5];
        assign r6 = received[6];
        assign r7 = received[7];
		
		assign  i0 = iter_done[0];
		assign 	i1 = iter_done[1];
		assign 	i2 = iter_done[2];
		assign 	i3 = iter_done[3];
		assign 	i4 = iter_done[4];
		assign 	i5 = iter_done[5];
		assign 	i6 = iter_done[6];
		assign 	i7 = iter_done[7];   

		assign  sync[0] = s0;
		assign 	sync[1] = s1;
		assign 	sync[2] = s2;
		assign 	sync[3] = s3;
		assign 	sync[4] = s4;
		assign 	sync[5] = s5;
		assign 	sync[6] = s6;
		assign 	sync[7] = s7;		
            
localparam integer TYPE_LEVELS=11;

		

//    	wire [63:0] received [31:0];
		// tree 
`ifdef TREE
	localparam TYPE = {32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0};
`endif
	// xbar 
`ifdef XBAR
	localparam TYPE = {32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1};
`endif
	// mesh0 0.5 
`ifdef MESH0
	localparam TYPE = {32'd1,32'd0,32'd1,32'd0,32'd1,32'd0,32'd1,32'd0,32'd1,32'd0,32'd1};
`endif
	// mesh0 0.67 
`ifdef MESH1
//	localparam TYPE = {32'd1,32'd1,32'd0,32'd0,32'd1,32'd1,32'd0,32'd0,32'd1,32'd1,32'd0};
       localparam TYPE = {32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1};
`endif

    genvar m, n, l, m1, q,hr;
	generate if(N>2) begin: n2
	for (l = 1; l < LEVELS; l = l + 1) begin : ls
		for (m = 0; m < N/(1<<(l+1)); m = m + 1) begin : ms
			for (n = 0; n < (1<<(l)); n = n + 1) begin : ns
				if(((TYPE >> (32*(TYPE_LEVELS-1-l))) & {32{1'b1}})==1) begin: pi_level
							(* keep_hierarchy = "yes" *) pi_switch_top #(.WRAP(WRAP),.P_W(P_W),.M_A_W(M_A_W), .D_W(D_W),.A_W(A_W), .N(32), 
						.posl(l), .posx(m*(1<<l)+n+(hop_addr>>1)),.FD(FD-2*(HR*(1<<(l>>1)))),.HR(HR*(1<<(l>>1))))
					sb(.clk(clk), .rst(1'b0),
						.s_axis_l_wdata(grid_up[l-1][m*(1<<(l+1))+n]),
						.s_axis_l_wvalid(grid_up_v[l-1][m*(1<<(l+1))+n]),
						.s_axis_l_wready(grid_up_r[l-1][m*(1<<(l+1))+n]),
						.s_axis_l_wlast(grid_up_l[l-1][m*(1<<(l+1))+n]),
						.s_axis_r_wdata(grid_up[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_r_wvalid(grid_up_v[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_r_wready(grid_up_r[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_r_wlast(grid_up_l[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_u0_wdata(grid_dn[l][m*(1<<(l+1))+n]),
                        .s_axis_u0_wvalid(grid_dn_v[l][m*(1<<(l+1))+n]),
						.s_axis_u0_wready(grid_dn_r[l][m*(1<<(l+1))+n]),
						.s_axis_u0_wlast(grid_dn_l[l][m*(1<<(l+1))+n]),
						.s_axis_u1_wdata(grid_dn[l][m*(1<<(l+1))+n+(1<<(l))]),
                        .s_axis_u1_wvalid (grid_dn_v[l][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_u1_wready(grid_dn_r[l][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_u1_wlast(grid_dn_l[l][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_l_wdata(grid_dn[l-1][m*(1<<(l+1))+n]),
						.m_axis_l_wvalid(grid_dn_v[l-1][m*(1<<(l+1))+n]),
						.m_axis_l_wready(grid_dn_r[l-1][m*(1<<(l+1))+n]),
						.m_axis_l_wlast(grid_dn_l[l-1][m*(1<<(l+1))+n]),
						.m_axis_r_wdata(grid_dn[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_r_wvalid(grid_dn_v[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_r_wready(grid_dn_r[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_r_wlast(grid_dn_l[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_u0_wdata(grid_up[l][m*(1<<(l+1))+n]),
						.m_axis_u0_wvalid(grid_up_v[l][m*(1<<(l+1))+n]),
						.m_axis_u0_wready(grid_up_r[l][m*(1<<(l+1))+n]),
						.m_axis_u0_wlast(grid_up_l[l][m*(1<<(l+1))+n]),
						.m_axis_u1_wdata(grid_up[l][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_u1_wvalid(grid_up_v[l][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_u1_wready(grid_up_r[l][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_u1_wlast(grid_up_l[l][m*(1<<(l+1))+n+(1<<(l))])
//						.done(done_sw[l][m*(1<<l)+n])
                        );
		    		end
				if(((TYPE >> (32*(TYPE_LEVELS-1-l))) & {32{1'b1}})==0) begin: t_level
					t_switch_top #(.WRAP(WRAP),.M_A_W(M_A_W),.P_W(P_W), .D_W(D_W),.A_W(A_W), .N(N), 
						.posl(l), .posx(m*(1<<l)+n),.FD(FD-2*(HR*(1<<(l>>1)))),.HR(HR*(1<<(l>>1))))
					sb(.clk(clk), .rst(1'b0),
						.s_axis_l_wdata(grid_up[l-1][m*(1<<(l+1))+n]),
						.s_axis_l_wvalid(grid_up_v[l-1][m*(1<<(l+1))+n]),
						.s_axis_l_wready(grid_up_r[l-1][m*(1<<(l+1))+n]),
						.s_axis_l_wlast(grid_up_l[l-1][m*(1<<(l+1))+n]),
						.s_axis_r_wdata(grid_up[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_r_wvalid(grid_up_v[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_r_wready(grid_up_r[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_r_wlast(grid_up_l[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.s_axis_u0_wdata(grid_dn[l][m*(1<<(l+1))+n]),
                        .s_axis_u0_wvalid(grid_dn_v[l][m*(1<<(l+1))+n]),//.s_axis_u0_wvalid(grid_dn_v[l][m*(1<<(l+1))+n]),
						.s_axis_u0_wready(grid_dn_r[l][m*(1<<(l+1))+n]),
						.s_axis_u0_wlast(grid_dn_l[l][m*(1<<(l+1))+n]),
						.m_axis_l_wdata(grid_dn[l-1][m*(1<<(l+1))+n]),
						.m_axis_l_wvalid(grid_dn_v[l-1][m*(1<<(l+1))+n]),
						.m_axis_l_wready(grid_dn_r[l-1][m*(1<<(l+1))+n]),
						.m_axis_l_wlast(grid_dn_l[l-1][m*(1<<(l+1))+n]),
						.m_axis_r_wdata(grid_dn[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_r_wvalid(grid_dn_v[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_r_wready(grid_dn_r[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_r_wlast(grid_dn_l[l-1][m*(1<<(l+1))+n+(1<<(l))]),
						.m_axis_u0_wdata(grid_up[l][m*(1<<(l+1))+n]),
						.m_axis_u0_wvalid(grid_up_v[l][m*(1<<(l+1))+n]),
						.m_axis_u0_wready(grid_up_r[l][m*(1<<(l+1))+n]),
						.m_axis_u0_wlast(grid_up_l[l][m*(1<<(l+1))+n])
//						.done(done_sw[l][m*(1<<l)+n])
                        );
		    		end
			end
		end
	end
	end endgenerate
	
	generate for (m = 0; m < N/2; m = m + 1) begin : xs
        if(`SPMV==0) begin
				(* keep_hierarchy = "yes" *) client_top #(.M_A_W(M_A_W),.P_W(P_W),.D_W(D_W), .A_W(A_W),.N(32), .posx(2*m), .LIMIT(LIMIT), .RATE(RATE), .SIGMA(SIGMA),.WRAP(WRAP), .PAT(PAT)
				 ,.HBM_base_port(HBM_BASE_OFFSET+((2*m)*(Y_MAX-1))) ,.hop_addr(hop_addr+2*m) ,.HBM_BASE_OFFSET(HBM_BASE_OFFSET) ,.base((2*m)<N/2?(2*m)*8+row_no:8*(2*m)-28+row_no))
			cli0(.clk(clk), .rst(ce[2*m]),.cmd(cmd_r[2*m]),//.rst(ce[2*m]),.cmd(cmd_r[2*m]
			.s_axis_c_wdata(pei[2*m]),
			.s_axis_c_wvalid(pei_v[2*m]),
			.s_axis_c_wready(pei_r[2*m]),
			.s_axis_c_wlast(pei_l[2*m]),
			.m_axis_c_wdata(peo[2*m]),
			.m_axis_c_wvalid(peo_v[2*m]),
			.m_axis_c_wready(peo_r[2*m]),
			.m_axis_c_wlast(peo_l[2*m]),
			.received(received[2*m]));
						(* keep_hierarchy = "yes" *) client_top #(.M_A_W(M_A_W),.P_W(P_W),.D_W(D_W), .A_W(A_W),.N(32), .posx(2*m+1), .LIMIT(LIMIT), .SIGMA(SIGMA), .RATE(RATE), .WRAP(WRAP), .PAT(PAT)
						,.HBM_base_port(HBM_BASE_OFFSET+((2*m+1)*(Y_MAX-1))),.hop_addr(hop_addr+2*m+1),.HBM_BASE_OFFSET(HBM_BASE_OFFSET),.base((2*m+1)<N/2?(2*m+1)*8+row_no:8*(2*m+1)-28+row_no))
             cli1(.clk(clk),.rst(ce[2*m+1]),.cmd(cmd_r[2*m+1]),
			.s_axis_c_wdata(pei[2*m+1]),
			.s_axis_c_wvalid(pei_v[2*m+1]),
			.s_axis_c_wready(pei_r[2*m+1]),
			.s_axis_c_wlast(pei_l[2*m+1]),
			.m_axis_c_wdata(peo[2*m+1]),
			.m_axis_c_wvalid(peo_v[2*m+1]),
			.m_axis_c_wready(peo_r[2*m+1]),
			.m_axis_c_wlast(peo_l[2*m+1]),
			.received(received[2*m+1]));
		end
		else begin
						(* keep_hierarchy = "yes" *) client_top_spmv #(.M_A_W(M_A_W),.P_W(P_W),.D_W(D_W), .A_W(A_W),.N(32), .posx(2*m), .LIMIT(LIMIT), .RATE(RATE), .SIGMA(SIGMA),.WRAP(WRAP), .PAT(PAT)
				 ,.HBM_base_port(HBM_BASE_OFFSET+((2*m)*(Y_MAX-1))) ,.hop_addr(hop_addr+2*m) ,.HBM_BASE_OFFSET(HBM_BASE_OFFSET) ,.base((2*m)<N/2?(2*m)*8+row_no:8*(2*m)-28+row_no))
			cli0(.clk(clk), .rst(ce[2*m]),.cmd(cmd_r[2*m]),//.rst(ce[2*m]),.cmd(cmd_r[2*m]
			.s_axis_c_wdata(pei[2*m]),
			.s_axis_c_wvalid(pei_v[2*m]),
			.s_axis_c_wready(pei_r[2*m]),
			.s_axis_c_wlast(pei_l[2*m]),
			.m_axis_c_wdata(peo[2*m]),
			.m_axis_c_wvalid(peo_v[2*m]),
			.m_axis_c_wready(peo_r[2*m]),
			.m_axis_c_wlast(peo_l[2*m]),
			.received(received[2*m])
			,.iter_done(iter_done[2*m])
			,.sync(sync[2*m]));
						(* keep_hierarchy = "yes" *) client_top_spmv #(.M_A_W(M_A_W),.P_W(P_W),.D_W(D_W), .A_W(A_W),.N(32), .posx(2*m+1), .LIMIT(LIMIT), .SIGMA(SIGMA), .RATE(RATE), .WRAP(WRAP), .PAT(PAT)
						,.HBM_base_port(HBM_BASE_OFFSET+((2*m+1)*(Y_MAX-1))),.hop_addr(hop_addr+2*m+1),.HBM_BASE_OFFSET(HBM_BASE_OFFSET),.base((2*m+1)<N/2?(2*m+1)*8+row_no:8*(2*m+1)-28+row_no))
             cli1(.clk(clk),.rst(ce[2*m+1]),.cmd(cmd_r[2*m+1]),
			.s_axis_c_wdata(pei[2*m+1]),
			.s_axis_c_wvalid(pei_v[2*m+1]),
			.s_axis_c_wready(pei_r[2*m+1]),
			.s_axis_c_wlast(pei_l[2*m+1]),
			.m_axis_c_wdata(peo[2*m+1]),
			.m_axis_c_wvalid(peo_v[2*m+1]),
			.m_axis_c_wready(peo_r[2*m+1]),
			.m_axis_c_wlast(peo_l[2*m+1]),
			.received(received[2*m+1])
			,.iter_done(iter_done[2*m+1])
			,.sync(sync[2*m+1]));
		end
		
		if(((TYPE >> (32*(TYPE_LEVELS-1))) & {32{1'b1}})==1) begin: pi_level0
					(* keep_hierarchy = "yes" *)pi_switch_top #(.WRAP(WRAP) ,.P_W(P_W),.M_A_W(M_A_W),.D_W(D_W),.A_W(A_W), .N(32), .posl(0), .posx(m+(hop_addr>>1)),.FD(FD-2*HR),.HR(HR))
				sb(.clk(clk), .rst(1'b0), 
					.s_axis_l_wdata(peo[2*m]),
					.s_axis_l_wvalid(peo_v[2*m]),
					.s_axis_l_wready(peo_r[2*m]),
					.s_axis_l_wlast(peo_l[2*m]),
					.s_axis_r_wdata(peo[2*m+1]),
					.s_axis_r_wvalid(peo_v[2*m+1]),
					.s_axis_r_wready(peo_r[2*m+1]),
					.s_axis_r_wlast(peo_l[2*m+1]),
					.s_axis_u0_wdata(grid_dn[0][2*m]),
					.s_axis_u0_wvalid(grid_dn_v[0][2*m]),
					.s_axis_u0_wready(grid_dn_r[0][2*m]),
					.s_axis_u0_wlast(grid_dn_l[0][2*m]),
					.s_axis_u1_wdata(grid_dn[0][2*m+1]),
					.s_axis_u1_wvalid(grid_dn_v[0][2*m+1]),
					.s_axis_u1_wready(grid_dn_r[0][2*m+1]),
					.s_axis_u1_wlast(grid_dn_l[0][2*m+1]),
					.m_axis_l_wdata(pei[2*m]),
					.m_axis_l_wvalid(pei_v[2*m]),
					.m_axis_l_wready(pei_r[2*m]),
					.m_axis_l_wlast(pei_l[2*m]),
					.m_axis_r_wdata(pei[2*m+1]),
					.m_axis_r_wvalid(pei_v[2*m+1]),
					.m_axis_r_wready(pei_r[2*m+1]),
					.m_axis_r_wlast(pei_l[2*m+1]),
					.m_axis_u0_wdata(grid_up[0][2*m]),
					.m_axis_u0_wvalid(grid_up_v[0][2*m]),
					.m_axis_u0_wready(grid_up_r[0][2*m]),
					.m_axis_u0_wlast(grid_up_l[0][2*m]),
					.m_axis_u1_wdata(grid_up[0][2*m+1]),
					.m_axis_u1_wvalid(grid_up_v[0][2*m+1]),
					.m_axis_u1_wready(grid_up_r[0][2*m+1]),
					.m_axis_u1_wlast(grid_up_l[0][2*m+1])
//					.done(done_sw[0][m])
);
		end
		if(((TYPE >> (32*(TYPE_LEVELS-1))) & {32{1'b1}})==0) begin: t_level0
				t_switch_top #(.M_A_W(M_A_W),.P_W(P_W),.WRAP(WRAP), .D_W(D_W), .N(N),.A_W(A_W), .posl(0), .posx(m),.FD(FD-2*HR),.HR(HR))
				sb(.clk(clk), .rst(1'b0), 
					.s_axis_l_wdata(peo[2*m]),
					.s_axis_l_wvalid(peo_v[2*m]),
					.s_axis_l_wready(peo_r[2*m]),
					.s_axis_l_wlast(peo_l[2*m]),
					.s_axis_r_wdata(peo[2*m+1]),
					.s_axis_r_wvalid(peo_v[2*m+1]),
					.s_axis_r_wready(peo_r[2*m+1]),
					.s_axis_r_wlast(peo_l[2*m+1]),
					.s_axis_u0_wdata(grid_dn[0][2*m]),
					.s_axis_u0_wvalid(grid_dn_v[0][2*m]),
					.s_axis_u0_wready(grid_dn_r[0][2*m]),
					.s_axis_u0_wlast(grid_dn_l[0][2*m]),
					.m_axis_l_wdata(pei[2*m]),
					.m_axis_l_wvalid(pei_v[2*m]),
					.m_axis_l_wready(pei_r[2*m]),
					.m_axis_l_wlast(pei_l[2*m]),
					.m_axis_r_wdata(pei[2*m+1]),
					.m_axis_r_wvalid(pei_v[2*m+1]),
					.m_axis_r_wready(pei_r[2*m+1]),
					.m_axis_r_wlast(pei_l[2*m+1]),
					.m_axis_u0_wdata(grid_up[0][2*m]),
					.m_axis_u0_wvalid(grid_up_v[0][2*m]),
					.m_axis_u0_wready(grid_up_r[0][2*m]),
					.m_axis_u0_wlast(grid_up_l[0][2*m])
//					.done(done_sw[0][m])
                );
		end
	end endgenerate
	
endmodule
