`include "phalanx.h"
`include "phalanx_tb.h"
`include "parameters.vh"

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/28/2020 03:58:30 PM
// Design Name: 
// Module Name: fanin_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fanin_top #(
	parameter NUM_CHANNELS = 1, // choose how many Hoplites to try
	parameter X_W	= 4,	// X address width
	parameter Y_W	= 4,	// Y address width
	parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width
	parameter X_MAX	= 1<<X_W,
	parameter Y_MAX	= 1<<Y_W,
	parameter X	= 3,	// X address of this node
	parameter Y	= 2		// Y address of this node
) (
	input wire clk,
	input wire rst,
	output wire `Msg o,	// output message to client
	output wire o_v,	// valid to client
	input wire o_busy,
	input wire `MsgChan oo,	// input message to client
	input wire [NUM_CHANNELS-1:0] oo_v,	// output valid from hoplite
	output wire [NUM_CHANNELS-1:0] ii_busy, // client refusing delivery
	input wire done_all
);

wire `MsgChan oo_s;
wire [NUM_CHANNELS-1:0] oo_v_s;
wire [NUM_CHANNELS-1:0] ii_busy_s;

wire [NUM_CHANNELS-1:0] dummy;
assign ii_busy = dummy;
genvar i;
generate
    for(i=0;i<NUM_CHANNELS;i=i+1) begin : chan
         (*keep_hierarchy = "true"*)shadow_reg_combi #(
                                 .D_W( D_W   )
                                ,.A_W( 0 )
                           )
                           bp(
                                 .clk( clk                           ) 
                                ,.rst( rst                           )
                                ,.i_v( oo_v[i]                       ) 
                                ,.i_d( {1'b0,oo[`Msg_W*(i+1)-1:`Msg_W*(i)]}   ) 
                                ,.i_b( dummy[i]                    )
                                ,.o_v( oo_v_s[i]                     )
                                ,.o_d( oo_s[`Msg_W*(i+1)-1:`Msg_W*(i)] ) 
                                ,.o_b( ii_busy_s[i]                   )
                             );
    end
endgenerate

 (*keep_hierarchy = "true"*)Fanin #(
         .NUM_CHANNELS ( NUM_CHANNELS )
        ,.P_W          ( P_W          )
        ,.M_A_W        ( M_A_W        )
        ,.D_W          ( D_W          )
        ,.X_W          ( X_W          )
        ,.Y_W          ( Y_W          )
        ,.X_MAX        ( X_MAX        )
        ,.Y_MAX        ( Y_MAX        )
        ,.X            ( X            )
        ,.Y            ( Y            )

       )
			fanin_inst (
			             .clk     (clk       )
			            ,.rst     (rst       )
					    ,.o       (o         )
					    ,.o_v     (o_v       )
					    ,.o_busy(o_busy)
					    ,.oo      (oo_s      )
					    ,.ii_busy (ii_busy_s     )
					    ,.oo_v    (oo_v_s   )
					    ,.done_all(done_all  )
					   );


endmodule
