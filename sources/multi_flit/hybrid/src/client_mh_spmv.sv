`timescale 1ns / 1ps
`include "client.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/21/2020 05:56:08 PM
// Design Name: 
// Module Name: client_mh
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module client_mh_spmv#(

	parameter N 	= 2,		// total number of clients
	parameter M_A_W = 16,
	parameter A_W	= $clog2(N)+1,	// address width
	parameter P_W	= 32,		// data width
	parameter D_W   = 1+M_A_W+A_W+P_W,
	parameter WRAP  = 1,            // wrapping means throttling of reinjection
	parameter PAT   = `RANDOM,      // default RANDOM pattern
	parameter RATE  = 100,           // rate of injection (in percent) 
	parameter LIMIT = 16,           // when to stop injectin packets
	parameter SIGMA = 4,            // radius for LOCAL traffic
	parameter posx 	= 2,		// position
	parameter filename = "posx"
	
)


(
	input	wire			clk,
	input	wire			rst,
	input	wire			ce,
	input	wire	`Cmd    cmd,
	(*dont_touch ="true"*) input	wire	[D_W:0]	c_i,
	(*dont_touch ="true"*) input	wire			c_i_v,
	(*dont_touch ="true"*) output	wire			c_i_bp,
	(*dont_touch ="true"*) output	reg	[D_W:0]	    c_o=0,
	(*dont_touch ="true"*) output	reg			    c_o_v=0,
	(*dont_touch ="true"*) input	wire			c_o_bp,
	output	wire			done
);
/* -------------------------------------------------------------------------- */
assign c_i_bp=0;
//reg rst_r = 0;
//always@(posedge clk) begin
//    rst_r <= rst;
//end
//    assign c_i_bp  = 0;
//always @(posedge clk)begin

//    if(c_i_v & ~c_o_bp) begin
//        c_o <= c_i;
//        c_o_v <= c_i_v;
//    end
//end

endmodule
