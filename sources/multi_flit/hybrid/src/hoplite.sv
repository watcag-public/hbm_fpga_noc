`default_nettype none

///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// hoplite.v -- Hoplite austere unidirectional 2-D torus deflection router
//
// hard tabs=4

`include "phalanx.h"
`include "phalanx_tb.h"
`include "parameters.vh"

///////////////////////////////////////////////////////////////////////////////
// Hoplite NS-FIFO with deflection -- austere unidirectional 2-D torus deflection router
//

///////////////////////////////////////////////////////////

module Hoplite #(
    parameter N     = 4,
    parameter X_W   = 4,
    parameter Y_W   = 4,
    parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width
    parameter X_MAX = N,
    parameter Y_MAX = N,
    parameter X     = 0,    // X address of this node
    parameter Y     = 0,    // Y address of this node
    parameter FIFO_D_WS = 16, // West -> South FIFO depth
    parameter CHAN = 1
) (
    input wire clk,        // clock
    input wire rst,        // reset
    input wire `Msg n,
    output wire n_b, 
    output wire n_b_vc,
    input wire `Msg i, 
    output wire i_ack,      // input accepted this cycle
    output wire o_v,        // s_out is valid output message for this node
    output reg `Msg s_out=0, // southbound output message
    input wire s_b,
    input wire s_b_vc,
    output wire `Msg out,
    input wire i_busy
);
    localparam pl= Y==0?1:4;
    reg	[1:0]	rr=0; //0->N, 1->I
    wire [2:0] src_x = n`src_x;
    wire [2:0] src_y = n`src_y;
    
    wire n_v_c;
    wire [`Msg_W-2:0] n_d_c;
    reg n_b_c = 0;
    
    wire n_v_vc;
    wire [`Msg_W-2:0] n_d_vc;
    reg n_b_vc_c = 0;
    
    wire i_v_c;
    wire [`Msg_W-2:0] i_d_c;
    reg i_b_c = 0;
    wire i_b;

    wire route_i_to_s;
    wire route_n_to_o, route_n_to_s;
    wire route_n_to_s_or_o;
    
    wire route_vc_to_o, route_vc_to_s;
    wire route_vc_to_s_or_o;
    

    reg `Msg s_out_r = 0;
    reg `Msg n_out_r = 0;
    reg `Msg s_out_c=0,s_out_vc=0,out_c=0;
    reg `Msg s_out_test=0;
    
    wire n_last,n_head,n_route,route_n_to_s_o;
    reg  n_o_lock=0, n_s_lock=0;
    reg  n_gets_o=0, n_gets_s=0;
    
    wire vc_last,vc_head,vc_route,route_vc_to_s_o;
    reg  vc_o_lock=0, vc_s_lock=0;
    reg  vc_gets_o=0, vc_gets_s=0;
    
    wire i_last,i_head,i_route;
    reg  i_e_lock=0, i_s_lock=0;
    reg  i_gets_e=0, i_gets_s=0;
    

    wire n_wins = (rr==0);
    wire i_wins = (rr==1);
    
    reg [3:0] rst_r = 4'b1111;
    always@(posedge clk) begin
    

//    else begin
     rst_r <= rst_r >> 1;
     n_o_lock <= n_gets_o|n_o_lock;
     n_s_lock <= n_gets_s|n_s_lock;
     
     vc_o_lock <= vc_gets_o|vc_o_lock;
     vc_s_lock <= vc_gets_s|vc_s_lock;
     
     
     i_e_lock <= i_gets_e|i_e_lock;
     i_s_lock <= i_gets_s|i_s_lock;
     
     if(n_last & (!n_b_c) & n_v_c) begin
         n_o_lock <= 1'b0;
         n_s_lock <= 1'b0;        
     end
     
      if(vc_last & (!n_b_vc_c) & n_v_vc) begin
         vc_o_lock <= 1'b0;
         vc_s_lock <= 1'b0;        
     end
     
     if(i_last & (!i_b_c) & i_v_c) begin
         i_e_lock <= 1'b0;
         i_s_lock <= 1'b0;        
     end   
//    end     

    if(rst_r[0]) begin
               
     n_o_lock <=0;
     n_s_lock <=0;
     
     vc_o_lock <=0;
     vc_s_lock <=0;
                
                
     i_e_lock <=0;
     i_s_lock <=0;
    end
end
    
    reg `Msg i_reg = 0;
    
    always@(posedge clk) begin
        if(i_b == 0)begin
            i_reg <= i;
        end
    end
    wire north_valid = n`v&(~n`vc);
    wire vc_valid = n`v&(n`vc);
         (*keep_hierarchy = "true"*)shadow_reg_combi #(.D_W(`Msg_W-1),.A_W(0)) pe (.clk(clk), .rst(1'b0), 
        .i_v(i_reg`v), .i_d(i_reg`xv), .i_b(i_b),
        .o_v(i_v_c), .o_d(i_d_c), .o_b(i_b_c));
        
          (*keep_hierarchy = "true"*) pipe_bp_vc #(.D_W(`Msg_W-1),.FD(16),.HR(pl)) north (.clk(clk), .rst(rst_r[0]), 
        .i_v(n`v), .i_d(n`xv), .i_b(n_b), .i_b_vc(n_b_vc),
        .o_v(n_v_c), .o_d(n_d_c), .o_b(n_b_c),
        .o_v_vc(n_v_vc), .o_d_vc(n_d_vc), .o_b_vc(n_b_vc_c)        
        );
        
        
//        (*keep_hierarchy = "true"*) pipe_bp #(.D_W(`Msg_W-1),.FD(16),.HR(2)) north (.clk(clk), .rst(rst_r[0]), 
//        .i_v(n`v&(~n`vc)), .i_d(n`xv), .i_b(n_b),
//        .o_v(n_v_c), .o_d(n_d_c), .o_b(n_b_c));
        
//        (*keep_hierarchy = "true"*) pipe_bp #(.D_W(`Msg_W-1),.FD(16),.HR(2)) north_vc (.clk(clk), .rst(rst_r[0]), 
//        .i_v(n`v&(n`vc)), .i_d(n`xv), .i_b(n_b_vc),
//        .o_v(n_v_vc), .o_d(n_d_vc), .o_b(n_b_vc_c));


    
    assign n_last  = n_d_c`l;
    assign n_head  = n_d_c`h;
    assign n_route = n_d_c`h & n_v_c;
    
    assign vc_last  = n_d_vc`l;
    assign vc_head  = n_d_vc`h;
    assign vc_route = n_d_vc`h & n_v_vc;
    
    assign i_last  = i_d_c`l;
    assign i_head  = i_d_c`h;
    assign i_route = i_d_c`h & i_v_c;
    
    assign route_i_to_s      = ((i_route)|i_s_lock) ; // PE input is valid and PE's X is equal to Hop's X (In same col)

    assign route_n_to_o        = (((n_route) &&  (n_d_c`y == Y))|n_o_lock) ;    // North Input is valid and N's Y is equal to Hop's Y (dest) and input is not busy
    assign route_n_to_s        = (((n_route) && (n_d_c`y != Y))|n_s_lock) ;    // North Input is valid and N's Y is not equal to Hop's Y 
    assign route_n_to_s_o      = route_n_to_o| route_n_to_s;

    assign route_vc_to_o        = (((vc_route) && (n_d_vc`y == Y))|vc_o_lock) ;    // VC Input is valid and N's Y is equal to Hop's Y (dest) and input is not busy
    assign route_vc_to_s        = (((vc_route) && (n_d_vc`y != Y))|vc_s_lock) ;    // VC Input is valid and N's Y is not equal to Hop's Y 
    assign route_vc_to_s_o      = route_vc_to_o|route_vc_to_s;




    //generating backpressure logic for client. 


    always@(*) begin        
        n_gets_s  = ~s_b    & ((route_n_to_s & ~route_vc_to_s & ~i_s_lock & ~vc_s_lock & ((n_wins)  | (~route_i_to_s))) | n_s_lock);
        n_gets_o  = ~i_busy & ((route_n_to_o & ~route_vc_to_o & ~vc_o_lock             ) | n_o_lock);    
        
        vc_gets_s  = ~s_b_vc & ((route_vc_to_s & ((n_wins))) | vc_s_lock);
        vc_gets_o  = ~i_busy & ((route_vc_to_o & ~n_o_lock             ) | vc_o_lock);    
        
        i_gets_s  = ((~i_d_c`vc&~s_b)|(i_d_c`vc&~s_b_vc)) & ((route_i_to_s  & ~n_s_lock & ~vc_s_lock & ((i_wins)  | ((~route_n_to_s) & (~route_vc_to_s)))) | i_s_lock);
 
    end
    
    always@(*) begin
            n_b_c = (route_n_to_s & (~n_gets_s |vc_gets_s ))| (route_n_to_o & ~n_gets_o);
            n_b_vc_c = (route_vc_to_s & (~vc_gets_s ))| (route_vc_to_o & ~vc_gets_o);
            i_b_c = (route_i_to_s  & (~i_gets_s | vc_gets_s));

    end


    //DOR logic
reg [`Msg_W-1:0] s_mux=0, o_mux=0;

    always@(*) begin
        case({vc_gets_s,n_gets_s, i_gets_s })
            3'b100 : begin
                s_mux`xv = n_d_vc`xv;
                s_mux`v  = n_v_vc;
            end
            3'b101 : begin
                s_mux`xv = n_d_vc`xv;
                s_mux`v  = n_v_vc;
            end
                3'b110 : begin
                s_mux`xv = n_d_vc`xv;
                s_mux`v  = n_v_vc;
            end
            3'b010 : begin
                s_mux`xv = n_d_c`xv;
                s_mux`v  = n_v_c;
            end
            3'b001 : begin
                s_mux`xv = i_d_c`xv;
                s_mux`v  = i_v_c;

            end
            default: begin
                s_mux`xv = 0;
                s_mux`v  = 0;

            end
        endcase
    end

    always@(*) begin
        case({vc_gets_o,n_gets_o})
            2'b10 : begin
                o_mux`xv = n_d_vc`xv;
                o_mux`v  = n_v_vc;
            end
            2'b01 : begin
                o_mux`xv = n_d_c`xv;
                o_mux`v  = n_v_c;

            end
            default: begin
                o_mux`xv = 0;
                o_mux`v  = 0;

            end
        endcase
    end
    
reg [63:0] i_bp_time =0,n_bp_time =0,vc_bp_time=0;
reg [63:0] now = 0; 

reg [63:0] total_i=0, total_n=0, total_vc = 0;
always @(posedge clk) begin

    if(i_b_c) begin
        i_bp_time = i_bp_time + 1;
    end
    
    if(n_b_c) begin
        n_bp_time = n_bp_time + 1;
    end
    if(n_b_vc_c) begin
        vc_bp_time = vc_bp_time + 1;
    end
    
    if(i_d_c`h & i_v_c & ~i_b_c) begin
        total_i += 1;
    end
    
    if(n_d_c`h & n_v_c & ~n_b_c) begin
        total_n += 1;
    end
    
    if(n_d_vc`h & n_v_vc & ~n_b_vc_c) begin
        total_vc += 1;
    end
end
reg i_b_c_r=0, n_b_c_r=0, n_b_vc_c_r=0;

always @(posedge clk) begin
    i_b_c_r<= i_b_c;
    n_b_c_r <= n_b_c;
    n_b_vc_c_r <= n_b_vc_c;
    now <= now+1;
    if(rst) begin
        now <= 0;
        if(now >0) begin
             $display("%d,%d,%d,%d,%d,%d,%d,%d,%d",X,Y,CHAN,i_bp_time,n_bp_time,vc_bp_time,total_i,total_n,total_vc);
        end
    end
end
//reg which_s=0;
//always @(*) begin
//    if(s_out`v ==0) begin
//        if(s_out`vc)
//            which_s=s_b_vc;
//        else
//            which_s=s_b;
//    end
//    else begin
//        which_s=s_b_vc;
//    end
//end

reg c_bp=1, vc_bp=1;
    always @(posedge clk) begin : dumb_name_to_keep_vivado_happy
           
              s_out_c <= s_mux;
              s_out_c`v<= ~s_mux`vc & s_mux`v;
              if(s_out_c`v & c_bp)
                s_out_c <= s_out_c;
                
              s_out_vc <= s_mux;
              s_out_vc`v<= s_mux`vc & s_mux`v;
              if(s_out_vc`v & vc_bp)
                s_out_vc <= s_out_vc;
//            if(!s_b) begin
//                s_out_c <= s_mux;
//                s_out_c`v <= ~s_mux`vc & s_mux`v;
//            end
//            if(!s_b_vc) begin
//                s_out_vc <= s_mux;
//                s_out_vc`v <= s_mux`vc & s_mux`v;
//            end
               if(!i_busy) begin
                    out_c <= o_mux;
               end

            s_out_test <= s_mux;
            if(s_out_test`v & ((s_out_test`vc & s_b_vc) | (~s_out_test`vc & s_b))) begin
                s_out_test<=s_out_test;
            end
    end

    assign i_ack = ~i_b;

    assign out = out_c;
    assign o_v = out_c`v; 

always@(*) begin
    s_out = s_mux;
    if(Y==Y_MAX-1)
        s_out`vc = 1;          
 end


always@(posedge clk)
begin

		case(rr)
			0:
			begin
				if(i_b_c)
				begin
					rr	<= 1;
				end				  
			end
			1:
			begin
				if(n_b_c||n_b_vc_c)
				begin
					rr	<= 0;
				end
			end
            default:
            begin
                rr  <= 0;
            end
		endcase
//	end
end

endmodule
