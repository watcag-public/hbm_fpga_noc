`timescale 1ns / 1ps
`include "client.vh" 
//credits: http://billauer.co.il/reg_fifo.html

module fwft_fifo
#(parameter DW = 10,
  parameter WATERMARK = 16,
  parameter OUT_REG=1
)
(
    input wire clk,
    input wire srst,
    input wire [DW:0] din,
    input wire wr_en,
    output wire full,
    output wire  [DW:0] dout,
    input wire rd_en,
    output wire empty

    );
   reg                   dout_valid=0;
   reg                   fifo_valid=0,middle_valid=0;
   reg [DW:0] dout_reg = 0;

   reg [DW:0] middle_dout = 0;
   
   wire [DW:0] fifo_dout;
   wire                  fifo_rd_en, fifo_empty;
   wire will_update_middle, will_update_dout;

   reg [5:0] prog_full_thresh = WATERMARK;
    fifo_generator_0 fifo
(
	.clk		(clk				),
	.srst		(srst				),
	.din		(din),//
	.wr_en		(wr_en),//
	.prog_full	    (full			),//
	.dout		(fifo_dout			),//
	.rd_en		(fifo_rd_en),//
	.empty		(fifo_empty			),//
	.prog_full_thresh(prog_full_thresh-1)
);

generate if(`FP==0) begin
   assign fifo_rd_en = !fifo_empty && (!dout_valid || rd_en);
   assign empty = !dout_valid;
   assign dout = fifo_dout;
   always @(posedge clk)
         begin
            if (fifo_rd_en)
               dout_valid <= 1;
            else if (rd_en)
               dout_valid <= 0;
         end 

end else begin
   assign dout = dout_reg;
   assign will_update_middle = fifo_valid && (middle_valid == will_update_dout);
   assign will_update_dout = (middle_valid || fifo_valid) && (rd_en || !dout_valid);
   assign fifo_rd_en = (!fifo_empty) && !(middle_valid && dout_valid && fifo_valid);
   assign empty = !dout_valid;

   always @(posedge clk)
          begin
            if (will_update_middle)
               middle_dout <= fifo_dout;
            
            if (will_update_dout)
               dout_reg <= middle_valid ? middle_dout : fifo_dout;
            
            if (fifo_rd_en)
               fifo_valid <= 1;
            else if (will_update_middle || will_update_dout)
               fifo_valid <= 0;
            
            if (will_update_middle)
               middle_valid <= 1;
            else if (will_update_dout)
               middle_valid <= 0;
            
            if (will_update_dout)
               dout_valid <= 1;
            else if (rd_en)
               dout_valid <= 0;
         end 
end
endgenerate
endmodule

