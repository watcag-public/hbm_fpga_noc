///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// phalanx.h -- Phalanx/Hoplite types
//
// hard tabs=4

`timescale 1ns/1ps

`define Multicast 1

/*

///////////////////////////////////////////////////////////////////////////////
// System Verilog type schema

typedef logic [D_W-1:0] D;	// data
typedef logic [X_W-1:0] Xaddr;	// Xaddr address
typedef logic [Y_W-1:0] Yaddr;	// Yaddr address

typedef logic bit;

typedef packed struct {
	bit v;			// valid
	bit mx;			// multicast on x
	bit my;			// multicast on y
	Xaddr x;			// mx ? src x : dest x
	Yaddr y;			// my ? src y : dest y
	D d;			// data payload
} Msg;				// message

typedef enum logic [1:0] {
	I, W, N
} Sel;				// output port mux select

typedef struct packed {
	Sel s_sel;		// south output port mux select
	Sel e_sel;		// east  output port mux select
	bit s_v;		// south output port valid
	bit e_v;		// east  output port valid
	bit o_v;		// output valid -- s_out is valid output packet
	bit i_ack;		// input ack -- accepted this cycle
} Rt;				// routing function

*/

// Boring old Verilog type constructors and deconstructors
// (Thanks to T.Thorn for the struct`field macro notation)
//
`define Msg_(v,x,y,d) {(v),(x),(y),(d)}
`define Msg_W 261 // valid,last,head,RW,VC,unused,Burst_len,mem_addr,src_x,src_y,dest_x,dest_y,unused
				  //  1,    1,    1, 1,  1, 1,     8,         33,     3,   3,     3,    3,    202
`define offset 202
`define M_A_W 33

`define Xaddr		[X_W-1:0]
`define Yaddr		[Y_W-1:0]
`define Msg		    [`Msg_W-1:0]

`define v		    [`Msg_W-1] // 260
`define l           [`Msg_W-2] // 259
`define h           [`Msg_W-3] // 258
`define rw          [`Msg_W-4] // 257
`define vc          [`Msg_W-5] // 256 
                               //255 unused
`define xv			[`Msg_W-2:0] 

`define w_vl        [`Msg_W-3:0]

`define dest_x		[X_W+Y_W+`offset-1:Y_W+`offset] // 207 205  
`define dest_y		[Y_W+`offset-1:`offset]//204 202

`define src_y       [Y_W+X_W+Y_W+`offset-1:X_W+Y_W+`offset] // 210 208
`define src_x 		[X_W+Y_W+X_W+Y_W+`offset-1:Y_W+X_W+Y_W+`offset]// 213 211

`define x			`dest_x
`define y			`dest_x

`define mem_addr    [M_A_W+2*(X_W+Y_W)+`offset-1:2*(X_W+Y_W)+`offset] // 246 214
`define port_id     [M_A_W+2*(X_W+Y_W)+`offset-1:M_A_W+2*(X_W+Y_W)+`offset-5]
`define burst_len   [8+M_A_W+2*(X_W+Y_W)+`offset-1:M_A_W+2*(X_W+Y_W)+`offset] // 254 247

`define d			[        P_W-1 :       0]

`define MsgChan		[NUM_CHANNELS*`Msg_W-1:0]
`define Chan [NUM_CHANNELS-1:0]
`define _mx(m)	(MCAST & m`mx)
`define _my(m)	(MCAST & m`my)

`define Rt_(o_sel,s_sel,e_sel,s_v,e_v,i_ack,o_v) \
	{o_sel,s_sel,e_sel,s_v,e_v,i_ack,o_v}
`define Rt	[9:0]
`define o_sel	[9:8]
`define s_sel	[7:6]
`define s_sel_n	[7]
`define e_sel	[5:4]
`define e_sel_w	[4]
`define s_v	[3]
`define e_v	[2]
`define o_v	[1]
`define i_ack	[0]

`define I		2'd0
`define W		2'd1
`define N		2'd2

// 0 is NORTH for o_sel
`define NO		2'd0 
`define WO		2'd1
`define IO		2'd2

`define X		[X_W-1:0]
`define Y		[Y_W-1:0]
`define NEXTX 	(((X)+1) % X_MAX)
`define NEXTY 	(((Y)+1) % Y_MAX)

`define com reg	/* a wire that is assigned in an always @* block */
`define KEEP (* keep *)

// `define RW [D_W-1]
 `define MA [M_A_W+P_W-1:P_W]
// `define P  [P_W-1:0]
// `define X_A [X_W+Y_W+M_A_W + P_W-1:Y_W+M_A_W + P_W]
// `define Y_A [Y_W+M_A_W + P_W-1:M_A_W + P_W] 
// `define XY_A [X_W+Y_W+M_A_W + P_W-1:M_A_W + P_W]