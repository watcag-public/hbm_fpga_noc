`default_nettype none
`include "phalanx.h"
`include "phalanx_tb.h"
`include "parameters.vh"
// Module for pipelining long links w/ backpressure signals. 
// An absorption FIFO at the end takes care of ensuring packets aren't dropped

module pipe_bp_vc
#
(
  parameter D_W = 32, // Data width
  parameter FD = 32,  // FIFO depth
  parameter HR = 4    // Pipeline length
)
(
  input  wire clk,
  input  wire rst,

  input  wire [D_W-1:0] i_d,
  input  wire i_v,
  output reg  i_b,
  output reg  i_b_vc,

  output reg  [D_W-1:0] o_d,
  output reg  o_v,
  input  wire o_b,
  
  output reg  [D_W-1:0] o_d_vc,
  output reg  o_v_vc,
  input  wire o_b_vc
);

// FIFO itf sigs
reg rd, rd_vc, wr, wr_vc;

reg [D_W-1:0] wrdata;

wire fifo_full, fifo_empty;
wire [D_W-1:0] rddata;


wire fifo_full_vc, fifo_empty_vc;
wire [D_W-1:0] rddata_vc;


generate
if (HR > 0) begin
    integer ii;

    // Pipeline registers, don't infer SRL as it beats the purpose of pipelining
    // a long link
    (* shreg_extract = "no" *) reg hr_b [HR-1:0];
    (* shreg_extract = "no" *) reg hr_b_vc [HR-1:0];
    (* shreg_extract = "no" *) reg hr_v [HR-1:0];
    (* shreg_extract = "no" *) reg [D_W-1:0] hr_d [HR-1:0];

initial begin
  for (ii = 0; ii < HR; ii = ii + 1)
          begin
              hr_d[ii] = {D_W{1'b0}};
              hr_v[ii] = 1'b0;
              hr_b[ii] = 1'b0;
              hr_b_vc[ii] = 1'b0;
              
          end
end
    // Manage pipeline
    always @(posedge(clk))
    begin : pipe
          hr_d[0] <= i_d;
          hr_v[0] <= i_v && ((~i_d`vc & ~i_b)|(i_d`vc & ~i_b_vc));
          hr_b[0] <= fifo_full;
          
          hr_b_vc[0] <= fifo_full_vc;
          
          for (ii = 1; ii < HR; ii = ii + 1)
          begin
              hr_d[ii] <= hr_d[ii-1];
              hr_v[ii] <= hr_v[ii-1];
              hr_b[ii] <= hr_b[ii-1];
              
              hr_b_vc[ii] <= hr_b_vc[ii-1];
          end          
      end
    

    // Manage FIFO and bp interface signals
    always @(*)
    begin: itf_sigs

        // FIFO itf sigs
        rd     = (!o_b) && (!fifo_empty);
        wr     = hr_v[HR-1]& ~hr_d[HR-1]`vc;
        wrdata = hr_d[HR-1];
        
        // BP itf sigs
        o_d    = rddata;
        o_v    = !fifo_empty;
        i_b    = hr_b[HR-1];
        
        rd_vc  = (!o_b_vc) && (!fifo_empty_vc);
        wr_vc  = hr_v[HR-1]& hr_d[HR-1]`vc;
        
        o_d_vc = rddata_vc;
        o_v_vc = !fifo_empty_vc;
        i_b_vc = hr_b_vc[HR-1];
        
    end
end
endgenerate
(*keep_hierarchy = "true"*)fwft_fifo
#(
    .DW(D_W-1),
    .WATERMARK(FD - 2 * HR)
) 
fifo_inst
(
    .clk(clk),
    .srst(rst), 

    .wr_en(wr),
    .din(wrdata),

    .rd_en(rd),
    .dout(rddata),

    .full(fifo_full),
    .empty(fifo_empty)

);

(*keep_hierarchy = "true"*)fwft_fifo
#(
    .DW(D_W-1),
    .WATERMARK(FD - 2 * HR)
    
) 
fifo_inst_vc
(
    .clk(clk),
    .srst(rst), 

    .wr_en(wr_vc),
    .din(wrdata),

    .rd_en(rd_vc),
    .dout(rddata_vc),

    .full(fifo_full_vc),
    .empty(fifo_empty_vc)

);


endmodule
