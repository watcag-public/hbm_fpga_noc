
`include	"client.vh"
`include	"bft.vh"
`timescale 1ps / 1ps
module client_top_spmv 
#(
    parameter M_A_W = 16,
	parameter N 	= 2,		// total number of clients
	parameter P_W	= 32,       // data width 
	parameter A_W	= $clog2(N)+1,	// address width
	parameter D_W	= 1+M_A_W+A_W+P_W,		
	parameter WRAP  = 1,            // wrapping means throttling of reinjection
	parameter PAT   = `RANDOM,      // default RANDOM pattern
	parameter RATE  = 10,           // rate of injection (in percent) 
	parameter LIMIT = 16,           // when to stop injectin packets
	parameter SIGMA = 4,            // radius for LOCAL traffic
	parameter posx 	= 2,		// position
    parameter HBM_base_port = posx,
	parameter hop_addr = 0,
	parameter HBM_BASE_OFFSET = posx,
	parameter base = posx

)
(
	input	wire     	clk,
	input	wire		rst,
	input	wire	`Cmd		cmd,
//	[1+M_A_W+A_W+A_W+D_W-1:0]
	input	wire	[D_W-1:0]	s_axis_c_wdata,
	input	wire			s_axis_c_wvalid,
	output	wire			s_axis_c_wready,
	input	wire			s_axis_c_wlast,

	output	wire	[D_W-1:0]	m_axis_c_wdata,
	output	wire			m_axis_c_wvalid,
	input	wire			m_axis_c_wready,
	output	wire			m_axis_c_wlast,
    output  wire [31:0]     received, 
	output	wire			done,
	output  wire            iter_done,
	input   wire             sync
);

assign received = 0;
//localparam base = (posx[4:0]==5'd31)?33'b0:{posx[4:0],28'b0};
// localparam loc  = (posx<N/2)?(2*(posx)):(2*(posx)-N+1);
// localparam base = {loc[4:0],28'b0};
wire ce = ~rst;
generate if(base>=24) begin 
wire	[D_W:0]	c_i_d_c;
wire			c_i_v_c;
wire			c_i_b_c;

wire	[D_W:0]	c_o_d_c;
wire			c_o_v_c;
wire			c_o_b_c;


reg	[D_W:0]	c_o_d_c_r = 0;
reg			c_o_v_c_r = 0;

always@(posedge clk) begin
    if(rst) begin
        c_o_d_c_r <= 0;
        c_o_v_c_r <= 0;
    end
    else begin
        if(c_o_b_c == 0) begin
            c_o_d_c_r <= c_o_d_c;
            c_o_v_c_r <= c_o_v_c;
        end
    end
end
assign	m_axis_c_wdata	= c_o_d_c_r[D_W-1:0];
assign	m_axis_c_wvalid	= c_o_v_c_r;
assign	c_o_b_c		= ~m_axis_c_wready;
assign	m_axis_c_wlast	= c_o_d_c_r[D_W];


reg iter_done_reg = 0;
assign iter_done = iter_done_reg;
always@ (posedge clk) begin
    iter_done_reg <= 1'b1;
    if(sync)
        iter_done_reg <= 1'b0;
end
(* keep_hierarchy = "yes" *) client_mh_spmv
#(
	.N		(N		), 	
	.D_W		(D_W		),
	.A_W		(A_W		),
	.WRAP		(WRAP		),	
	.PAT		(PAT		),
	.RATE  		(RATE		),
	.LIMIT		(LIMIT		),
	.SIGMA		(SIGMA		),
	.posx		(posx		)
	,.P_W(P_W),.M_A_W(M_A_W)
)
client_inst
(
	.clk		(clk		),
	.rst		(rst		),
	.ce		(ce		),
	.cmd		(cmd		),
	
	.c_i		(c_i_d_c	),
	.c_i_v		(c_i_v_c	),
	.c_i_bp		(c_i_b_c	),

	.c_o		(c_o_d_c	),
	.c_o_v		(c_o_v_c	),
	.c_o_bp		(c_o_b_c	),

	.done		(done		)
);


wire			bp_i_v_c;
wire			bp_i_b_c;
wire	[D_W:0]	bp_i_d_c;


assign	bp_i_v_c	= s_axis_c_wvalid;
assign	bp_i_d_c	= {s_axis_c_wlast, s_axis_c_wdata};
assign	s_axis_c_wready	= ~bp_i_b_c;



wire			bp_o_v_c;
wire			bp_o_b_c;
wire	[D_W:0]	bp_o_d_c;
assign	c_i_d_c		= bp_o_d_c;
assign	c_i_v_c		= bp_o_v_c;
assign	bp_o_b_c	= c_i_b_c;

(* keep_hierarchy = "yes" *) shadow_reg_combi
#(
	.A_W		(A_W	)
	,.D_W(D_W)
)
bp_C
(
	.clk		(clk		), 
	.rst		(rst		), 
	.i_v		(bp_i_v_c	),
	.i_d		(bp_i_d_c	), 
	.i_b		(bp_i_b_c	),
	.o_v		(bp_o_v_c	),
	.o_d		(bp_o_d_c	), 
	.o_b		(bp_o_b_c	) 
);

end
else begin
localparam baseA = base;
localparam baseB = base+'h10000;
localparam baseC = base+'h20000;
//generate if(loc <4) begin
wire	[D_W:0]	c_o_d_c;
wire			c_o_v_c;
wire			c_o_b_c;

reg	[D_W:0]	c_o_d_c_r=0;
reg			c_o_v_c_r=0;


wire read_ready;

wire [D_W-1:0] read_data;
wire read_last;
wire read_valid;

(* keep_hierarchy = "yes" *) axis_data_fifo_1 read(
       .s_axis_aresetn(~rst)
      ,.s_axis_aclk(clk)
      ,.s_axis_tvalid(s_axis_c_wvalid) // grid_up_v[LEVELS-1][ii] & grid_up[LEVELS-1][ii][D_W-1] &grid_up_r[LEVELS-1][ii ] 
      ,.s_axis_tready(s_axis_c_wready)
      ,.s_axis_tdata(s_axis_c_wdata)
      ,.s_axis_tlast(s_axis_c_wlast)
      
      ,.m_axis_tvalid(read_valid)
      ,.m_axis_tready(read_ready) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata(read_data)
      ,.m_axis_tlast(read_last)
    
    );
    
    
 (* keep_hierarchy = "yes" *)   axis_data_fifo_1 write(
       .s_axis_aresetn(~rst)
      ,.s_axis_aclk(clk)
      ,.s_axis_tvalid(c_o_v_c_r) // grid_up_v[LEVELS-1][ii] & grid_up[LEVELS-1][ii][D_W-1] &grid_up_r[LEVELS-1][ii ] 
      ,.s_axis_tready(c_o_b_c)
      ,.s_axis_tdata(c_o_d_c_r[258:0])
      ,.s_axis_tlast(c_o_d_c_r[259])
      
      ,.m_axis_tvalid(m_axis_c_wvalid)
      ,.m_axis_tready(m_axis_c_wready) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata(m_axis_c_wdata)
      ,.m_axis_tlast(m_axis_c_wlast)
    
    );
    
    
//assign	m_axis_c_wdata	= c_o_d_c_r[D_W-1:0];
//assign	m_axis_c_wvalid	= c_o_v_c_r;
//assign	c_o_b_c		= m_axis_c_wready;
//assign	m_axis_c_wlast	= c_o_d_c_r[D_W];

always@(posedge clk) begin
    if(rst) begin
        c_o_d_c_r <= 0;
        c_o_v_c_r <= 0;
    end
    else begin
        if( c_o_b_c ==1) begin
            c_o_d_c_r <= {c_o_d_c[258:256],1'b0,c_o_d_c[255:0]};
            c_o_v_c_r <= c_o_v_c;
        end
    end
end

reg ce_r = 0;

//wire iter_done;
//reg [15:0] sync_bit = 0;


wire ce_pulse = ce & ~ce_r;
always@(posedge clk) begin
    ce_r <= ce;
   
end

//{1'b0,req_iter,b_size_log,p_count_log,size}
  (* keep_hierarchy = "yes" *)   wrapper_1 cli_mm(
        .ap_clk(clk),
        .ap_rst_n(~rst),
        .ap_start(ce_pulse),
        .ap_done(done),
        .ap_idle(),
        .ap_ready(),
        .baseA_V('h000000),
        .baseB_V('h20000),
        .baseC_V('h40000),
        .port_id_V(base),
        .posx_V(hop_addr),
        .write_port_V_V_TDATA(c_o_d_c),
        .write_port_V_V_TVALID(c_o_v_c	),
        .write_port_V_V_TREADY(c_o_b_c	),
        .read_port_V_V_TDATA({read_last,read_data[258:257],read_data[255:0]}),
        .read_port_V_V_TVALID(read_valid),
        .read_port_V_V_TREADY(read_ready),
        .matrix_size_V(cmd[14:0]),
        .p_count_V(cmd[29:25]),
        .b_size_log_V(cmd[24:15]),
        .required_iter_V(cmd[39:30]),
        .sync_bit(sync),
        .iter_done(iter_done)
        
);
end
endgenerate

endmodule
