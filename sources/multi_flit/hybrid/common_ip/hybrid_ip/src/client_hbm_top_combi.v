///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// client.v -- Phalanx testbench client
//
// hard tabs=4

`include "phalanx.h"
`include "phalanx_tb.h"

///////////////////////////////////////////////////////////////////////////////
// Testbench client -- inject various test traffic patterns into the NOC
//
module client_hbm_top_combi #(
	parameter PAT		  = `RANDOM,
	parameter RATE		  = 5,
	parameter INTERVAL	  = 16,
	parameter PACKLIMIT   = 16,
	parameter SIGMA		  = 3,
	parameter P_W         = 256,
	parameter M_A_W       = 33,
	parameter X_W		  = 2,
	parameter Y_W		  = 2,
    parameter D_W		  = 1+P_W+M_A_W+X_W+Y_W,	// data width
	parameter CORES		  = 4,
	parameter X_MAX		  = 1<<X_W,
	parameter Y_MAX		  = 1<<Y_W,
	parameter X			  = 0,	// X address of this node
	parameter Y			  = 0,
	parameter CHAN        = 0,		// Y address of this node
	parameter ADDR_WIDTH = M_A_W,
	parameter DATA_WIDTH = 256,
	parameter NUM_CHANNELS = 4
) (
	input  wire clk,		// clock
	input  wire rst,		// reset
	input  wire ce,			// clock enable
	input  wire `Cmd cmd,	// client test command
	
    output wire `MsgChan i,		// router input message, registered, validated by i`v
	input  wire `Chan i_ack,		// router input accepted this cycle
	
	input  wire `Chan o_v,		// router output message valid
	input  wire `MsgChan o,		// router output message from router
    output wire `Chan i_busy,
	
	output wire                                    awvalid0      ,   
    input  wire                                    awready0      ,
    output wire [ADDR_WIDTH-1:0]                   awaddr0       ,
    output wire [8-1:0]                            awlen0        ,
    
    output wire                                    wvalid0       ,
    input  wire                                    wready0       ,
    output wire [DATA_WIDTH-1:0]                   wdata0        ,
    output wire                                    wlast0        ,
    
    input  wire                                    bvalid0       ,
    output wire                                    bready0       ,
    
    output wire                                    arvalid0      ,
    input  wire                                    arready0      ,
    output wire [ADDR_WIDTH-1:0]                   araddr0       ,
    output wire [8-1:0]                            arlen0        ,
        
    input  wire                                    rvalid0       ,
    output wire                                    rready0       ,
    input  wire [DATA_WIDTH-1:0]                   rdata0        ,
    input  wire                                    rlast0        , 
    
	output wire                                    awvalid1      ,   
    input  wire                                    awready1      ,
    output wire [ADDR_WIDTH-1: 0]                   awaddr1       ,
    output wire [8-1:0]                            awlen1        ,
    
    output wire                                    wvalid1       ,
    input  wire                                    wready1       ,
    output wire [DATA_WIDTH-1:0]                   wdata1        ,
    output wire                                    wlast1        ,
    
    input  wire                                    bvalid1       ,
    output wire                                    bready1       ,
    
    output wire                                    arvalid1      ,
    input  wire                                    arready1      ,
    output wire [ADDR_WIDTH-1:0]                   araddr1       ,
    output wire [8-1:0]                            arlen1        ,
        
    input  wire                                    rvalid1       ,
    output wire                                    rready1       ,
    input  wire [DATA_WIDTH-1:0]                   rdata1        ,
    input  wire                                    rlast1        , 
    
	output wire                                    awvalid2      ,   
    input  wire                                    awready2      ,
    output wire [ADDR_WIDTH-1:0]                   awaddr2       ,
    output wire [8-1:0]                            awlen2        ,
    
    output wire                                    wvalid2       ,
    input  wire                                    wready2       ,
    output wire [DATA_WIDTH-1:0]                   wdata2        ,
    output wire                                    wlast2        ,
    
    input  wire                                    bvalid2       ,
    output wire                                    bready2       ,
    
    output wire                                    arvalid2      ,
    input  wire                                    arready2      ,
    output wire [ADDR_WIDTH-1:0]                   araddr2       ,
    output wire [8-1:0]                            arlen2        ,
        
    input  wire                                    rvalid2       ,
    output wire                                    rready2       ,
    input  wire [DATA_WIDTH-1:0]                   rdata2        ,
    input  wire                                    rlast2        , 

	output wire                                    awvalid3      ,   
    input  wire                                    awready3      ,
    output wire [ADDR_WIDTH-1:0]                   awaddr3       ,
    output wire [8-1:0]                            awlen3        ,
    
    output wire                                    wvalid3       ,
    input  wire                                    wready3       ,
    output wire [DATA_WIDTH-1:0]                   wdata3        ,
    output wire                                    wlast3        ,
    
    input  wire                                    bvalid3       ,
    output wire                                    bready3       ,
    
    output wire                                    arvalid3      ,
    input  wire                                    arready3      ,
    output wire [ADDR_WIDTH-1:0]                   araddr3       ,
    output wire [8-1:0]                            arlen3        ,
        
    input  wire                                    rvalid3       ,
    output wire                                    rready3       ,
    input  wire [DATA_WIDTH-1:0]                   rdata3        ,
    input  wire                                    rlast3         
    
);                                                                

   wire                  awvalid `Chan  ;   
   wire                  awready `Chan  ;   
   wire [33-1:0]         awaddr  `Chan  ; 
   wire [8-1:0]          awlen   `Chan  ;    
    
   wire                  wvalid  `Chan  ;   
   wire                  wready  `Chan  ;   
   wire [DATA_WIDTH-1:0] wdata   `Chan  ; 
   wire                  wlast   `Chan  ; 
      
   wire                  bvalid  `Chan  ;   
   wire                  bready  `Chan  ;
      
   wire                  arvalid `Chan  ;   
   wire                  arready `Chan  ;   
   wire [33-1:0]         araddr  `Chan  ; 
   wire [8-1:0]          arlen   `Chan  ;   
   
   wire                  rvalid  `Chan  ;   
   wire                  rready  `Chan  ;   
   wire [DATA_WIDTH-1:0] rdata   `Chan  ;   
   wire                  rlast   `Chan  ;  
   
   wire `MsgChan i_d;		// router input message, registered, validated by i`v
   wire `Chan i_ack_d;		// router input accepted this cycle
	
//   assign i_ack_d = i_ack>>2;
//   assign i     = i_d << `Msg_W*2; 
   assign i_ack_d = i_ack;
   assign i     = i_d;
assign awvalid0   = awvalid[0]  ;
assign awready[0] = awready0    ;
assign awaddr0    = awaddr [0]  ;
assign awlen0     = awlen [0]   ;

assign wvalid0    = wvalid [0]  ;
assign wready [0] = wready0     ;
assign wdata0     = wdata [0]   ;
assign wlast0     = wlast [0]   ;

assign bvalid [0] = bvalid0     ;
assign bready0    = bready [0]  ;

assign arvalid0   = arvalid[0]  ;
assign arready[0] = arready0    ;
assign araddr0    = araddr [0]  ;
assign arlen0     = arlen [0]   ;

assign rvalid [0] = rvalid0     ;
assign rready0    = rready [0]  ;
assign rdata  [0] = rdata0      ;
assign rlast  [0] = rlast0      ;
//
assign awvalid1   = awvalid[1]  ;
assign awready[1] = awready1    ;
assign awaddr1    = awaddr [1]  ;
assign awlen1     = awlen [1]   ;

assign wvalid1    = wvalid [1]  ;
assign wready [1] = wready1     ;
assign wdata1     = wdata [1]   ;
assign wlast1     = wlast [1]   ;

assign bvalid [1] = bvalid1     ;
assign bready1    = bready [1]  ;

assign arvalid1   = arvalid[1]  ;
assign arready[1] = arready1    ;
assign araddr1    = araddr [1]  ;
assign arlen1     = arlen [1]   ;

assign rvalid [1] = rvalid1     ;
assign rready1    = rready [1]  ;
assign rdata  [1] = rdata1      ;
assign rlast  [1] = rlast1      ;

assign awvalid2   = awvalid[2]  ;
assign awready[2] = awready2    ;
assign awaddr2    = awaddr [2]  ;
assign awlen2     = awlen [2]   ;

assign wvalid2    = wvalid [2]  ;
assign wready [2] = wready2     ;
assign wdata2     = wdata [2]   ;
assign wlast2     = wlast [2]   ;

assign bvalid [2] = bvalid2     ;
assign bready2    = bready [2]  ;

assign arvalid2   = arvalid[2]  ;
assign arready[2] = arready2    ;
assign araddr2    = araddr [2]  ;
assign arlen2     = arlen [2]   ;

assign rvalid [2] = rvalid2     ;
assign rready2    = rready [2]  ;
assign rdata  [2] = rdata2      ;
assign rlast  [2] = rlast2      ;
//
assign awvalid3   = awvalid[3]  ;
assign awready[3] = awready3    ;
assign awaddr3    = awaddr [3]  ;
assign awlen3     = awlen [3]   ;

assign wvalid3    = wvalid [3]  ;
assign wready [3] = wready3     ;
assign wdata3     = wdata [3]   ;
assign wlast3     = wlast [3]   ;

assign bvalid [3] = bvalid3     ;
assign bready3    = bready [3]  ;

assign arvalid3   = arvalid[3]  ;
assign arready[3] = arready3    ;
assign araddr3    = araddr [3]  ;
assign arlen3     = arlen [3]   ;

assign rvalid [3] = rvalid3     ;
assign rready3    = rready [3]  ;
assign rdata  [3] = rdata3      ;
assign rlast  [3] = rlast3      ;
//fanin_top #(.NUM_CHANNELS(NUM_CHANNELS),
//        .P_W(P_W) ,.M_A_W(M_A_W)  ,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX))
//    fanin_inst (.clk(clk), .rst(rst), 
//            .o(o[`xy]), 
//            .o_v(o_v[`xy]),
//            .oo(oo[`xy]),
//            .ii_busy(ii_busy[`xy]),
//            .oo_v(oo_v[`xy]),
//            .done_all(done_all_r1));
   
genvar c; 

        for (c = 0; c < NUM_CHANNELS; c = c + 1) begin : cs
                           (*keep_hierarchy = "true"*)client_hbm_top #(.PAT(PAT), .RATE(RATE), .PACKLIMIT(PACKLIMIT ),.P_W(P_W) ,.M_A_W(M_A_W) 
                            ,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX), .X(X), .Y(Y),.CHAN(c) , .ADDR_WIDTH(33) , .DATA_WIDTH(DATA_WIDTH))
                             cli(.clk(clk), .rst(rst), .ce(ce), .cmd(cmd)
                            ,.i_ack    (     i_ack_d[c]       )
                            ,.o_v      (     o_v[c]         )
                            ,.o        (     o`Slice        ) // message from the router
                            ,.i        (     i_d`Slice        )       // message to the router
                            ,.i_busy   (     i_busy    [c]  )
                            
                            ,.awvalid  (     awvalid   [c]  )
                            ,.awready  (     awready   [c]  )
                            ,.awaddr   (     awaddr    [c]  )
                            ,.awlen    (     awlen     [c]  )
                            
                            ,.wvalid   (     wvalid    [c]  )
                            ,.wready   (     wready    [c]  )
                            ,.wdata    (     wdata     [c]  )
                            ,.wlast    (     wlast     [c]  )
                            
                            ,.bvalid   (     bvalid    [c]  )
                            ,.bready   (     bready    [c]  )
                            
                            ,.arvalid  (     arvalid   [c]  )
                            ,.arready  (     arready   [c]  )
                            ,.araddr   (     araddr    [c]  )
                            ,.arlen    (     arlen     [c]  )
                            
                            ,.rvalid   (     rvalid    [c]  )
                            ,.rready   (     rready    [c]  )
                            ,.rdata    (     rdata     [c]  )
                            ,.rlast    (     rlast     [c]  )
                            
                            );
    
        end

endmodule
