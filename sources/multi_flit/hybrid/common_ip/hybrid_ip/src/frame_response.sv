`timescale 1ns / 1ps
`include "phalanx.h"
`include "phalanx_tb.h"
`include "parameters.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/02/2020 10:46:55 PM
// Design Name: 
// Module Name: frame_response
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module frame_response #(
	parameter M_A_W = 16,
	parameter A_W	= 6,	// address width
	parameter P_W	= 32,		// data width
	parameter X_W		= 2,
	parameter Y_W		= 2,
	parameter D_W		= 1+X_W+Y_W+M_A_W + P_W,	// data width
	parameter CORES		= 4,
	parameter X_MAX		= 1<<X_W,
	parameter Y_MAX		= 1<<Y_W,
	parameter X			= 0,	// X address of this node
	parameter Y			= 0		// Y address of this node
    )(
        input wire clk,
        input wire rst,
        input wire [D_W-1:0] header_data,
        input wire header_valid,
        output reg header_ready=0,
        
        input wire [D_W-1:0] read_data,
        input wire read_valid,
        output wire read_ready,
        
        output reg [D_W-1:0] output_data=0,
        output reg output_valid=0,
        input wire output_ready
   
   );
   
wire [D_W-1:0] read_data_f;
wire read_valid_f;
reg read_ready_f=0;
wire read_ready_b;
assign read_ready = ~read_ready_b;
pipe_bp #(.D_W(D_W),.HR(2)) north (.clk(clk), .rst(1'b0), 
.i_v(read_valid), .i_d(read_data), .i_b(read_ready_b),
.o_v(read_valid_f), .o_d(read_data_f), .o_b(read_ready_f));

enum {IDLE,DATA} state = IDLE;

always@(posedge clk) begin :fsm_control
    case(state)
        IDLE: begin
            state <= IDLE;
            if(read_valid_f && header_valid ) begin
                if( output_valid && output_ready)begin
                     state <= DATA;
                end
            end
        
       end 
       DATA:begin
            state <= DATA;
            if(output_data`l && output_valid && output_ready) begin
                state<=IDLE;
            end
       end
    endcase
end
always@(*) begin :fsm_data
    case(state)
        IDLE: begin
            header_ready=0;
            output_valid=0;
            output_data=0;
            read_ready_f = 1;
            if(read_valid_f && header_valid ) begin
                output_data`dest_x = header_data`src_x;
                output_data`dest_y = header_data`src_y;
                output_data`src_x  = X;
                output_data`src_y  = Y;
                output_data`h      = 1'b1;
                output_data`vc     = 1'b1;
                output_data`l      = 1'b0;
                output_data[M_A_W-1:0] =  header_data`mem_addr;
                output_valid = header_valid;
                header_ready = output_ready;
                read_ready_f = 1;
            end
        end
        DATA:begin
            output_data`h = 1'b0;
            output_data`vc= 1'b1;
            output_data`l = read_data_f`l;
            output_data[255:0] = read_data_f[255:0];
            output_valid = read_valid_f;
            header_ready = 0;
            read_ready_f = ~output_ready;
        end
    endcase  
end
endmodule
