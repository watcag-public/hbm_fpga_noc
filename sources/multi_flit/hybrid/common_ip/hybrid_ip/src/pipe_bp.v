`default_nettype none

// Module for pipelining long links w/ backpressure signals. 
// An absorption FIFO at the end takes care of ensuring packets aren't dropped

module pipe_bp
#
(
  parameter D_W = 32, // Data width
  parameter FD = 32,  // FIFO depth
  parameter HR = 4,    // Pipeline length
  parameter OUT_REG=1
)
(
  input  wire clk,
  input  wire rst,

  input  wire [D_W-1:0] i_d,
  input  wire i_v,
  output reg  i_b,

  output reg  [D_W-1:0] o_d,
  output reg  o_v,
  input  wire o_b
);

// FIFO itf sigs
reg rd, wr;
reg [D_W-1:0] wrdata;

wire fifo_full, fifo_empty;
wire [D_W-1:0] rddata;

generate
if (HR > 0) begin
    integer ii;

    // Pipeline registers, don't infer SRL as it beats the purpose of pipelining
    // a long link
    (* shreg_extract = "no" *) reg hr_b [HR-1:0];
    (* shreg_extract = "no" *) reg hr_v [HR-1:0];
    (* shreg_extract = "no" *) reg [D_W-1:0] hr_d [HR-1:0];

initial begin
  for (ii = 0; ii < HR; ii = ii + 1)
          begin
              hr_d[ii] <= {D_W{1'b0}};
              hr_v[ii] <= 1'b0;
              hr_b[ii] <= 1'b0;
          end
end
    // Manage pipeline
    always @(posedge(clk))
    begin : pipe
          hr_d[0] <= i_d;
          hr_v[0] <= i_v && !i_b;
          hr_b[0] <= fifo_full;
          for (ii = 1; ii < HR; ii = ii + 1)
          begin
              hr_d[ii] <= hr_d[ii-1];
              hr_v[ii] <= hr_v[ii-1];
              hr_b[ii] <= hr_b[ii-1];
          end          
      end
    

    // Manage FIFO and bp interface signals
    always @(*)
    begin: itf_sigs

        // FIFO itf sigs
        rd     <= (!o_b) && (!fifo_empty);
        wr     <= hr_v[HR-1];
        wrdata <= hr_d[HR-1];

        // BP itf sigs
        o_d    <= rddata;
        o_v    <= !fifo_empty;
        i_b    <= hr_b[HR-1];
    end
end

else begin // HR <= 0
    // Manage FIFO and bp interface signals
    always @(*)
    begin: itf_sigs

        // FIFO itf sigs
        rd     = (!o_b) && (!fifo_empty);
        wr     = (i_v) && (!fifo_full);
        wrdata = i_d;

        // BP itf sigs
        o_d    = rddata;
        o_v    = !fifo_empty;
        i_b    = fifo_full;
    end
end
endgenerate

// Absorption FIFO instantiation
//fifo_lut
//#(
//    .WIDTH(D_W),
//    .DEPTH(FD),
//    .WATERMARK(FD - 2 * HR)
//) 
//fifo_inst
//(
//    .clk(clk),
//    .rst(rst), 

//    .wr(wr),
//    .wrdata(wrdata),

//    .rd(rd),
//    .rddata(rddata),

//    .full(fifo_full),
//    .empty(fifo_empty)

//);

fwft_fifo
#(
    .DW(D_W-1),
    .WATERMARK(FD - 2 * HR),
    .OUT_REG(OUT_REG)
) 
fifo_inst
(
    .clk(clk),
    .srst(rst), 

    .wr_en(wr),
    .din(wrdata),

    .rd_en(rd),
    .dout(rddata),

    .full(fifo_full),
    .empty(fifo_empty)

);

endmodule
