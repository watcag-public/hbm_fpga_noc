///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// client.v -- Phalanx testbench client
//
// hard tabs=4

`include "phalanx.h"
`include "phalanx_tb.h"
`include "client.vh"
///////////////////////////////////////////////////////////////////////////////
// Testbench client -- inject various test traffic patterns into the NOC
//
module Client_hbm #(
	parameter PAT		= `RANDOM,
	parameter RATE		= 5,
	parameter INTERVAL	= 16,
	parameter PACKLIMIT     = 16,
	parameter SIGMA		= 3,
	parameter P_W       = 256,
	parameter M_A_W     = 33,
    parameter X_W		= 2,
	parameter Y_W		= 2,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width

	parameter CORES		= 4,
	parameter X_MAX		= 1<<X_W,
	parameter Y_MAX		= 1<<Y_W,
	parameter X			= 0,	// X address of this node
	parameter Y			= 0,
	parameter CHAN      = 0,		// Y address of this node
    parameter ADDR_WIDTH = M_A_W,
	parameter DATA_WIDTH = 256
) (
	input  wire clk,		// clock
	input  wire rst,		// reset
	input  wire ce,			// clock enable
	input  wire `Cmd cmd,	// client test command
	input  wire i_ack,		// router input accepted this cycle
	input  wire o_v,		// router output message valid
	input  wire `Msg o,		// router output message from router
	output wire  `Msg i,		// router input message, registered, validated by i`v
	output wire done,		// done
	output [63:0] received,
	output wire i_busy,
	output wire                                    awvalid      ,   
    input  wire                                    awready      ,
    output wire [ADDR_WIDTH-1:0]                   awaddr       ,
    output wire [8-1:0]                            awlen        ,
    
    output wire                                    wvalid       ,
    input  wire                                    wready       ,
    output wire [DATA_WIDTH-1:0]                   wdata        ,
    output wire                                    wlast        ,
    
    input  wire                                    bvalid       ,
    output wire                                    bready       ,
    
    output wire                                    arvalid      ,
    input  wire                                    arready      ,
    output wire [ADDR_WIDTH-1:0]                   araddr       ,
    output wire [8-1:0]                            arlen        ,
        
    input  wire                                    rvalid       ,
    output wire                                    rready       ,
    input  wire [DATA_WIDTH-1:0]                   rdata        ,
    input  wire                                    rlast         
);

	integer r;

	reg done_sig;
	reg [63:0] now=0;

	reg [63:0] attempts=0;
	reg [63:0] received_r = 0 ;
	reg [63:0] sent = 0;
	assign received = received_r;
	integer waiting=0;

	integer wait_r=0;
	
	
	wire busy;
	

	wire [D_W-1:0] writeAddress_o;
    wire           writeAddress_ready;
    wire           writeAddress_l;
    
    wire [D_W-1:0] writeData_o;
    wire           writeData_ready;
    wire           writeData_l;
    
    wire [D_W-1:0] readAddress_o;
    wire           readAddress_ready;
    wire           readAddress_l;
    
    wire [D_W-1:0] readResp_o;
    wire           readResp_ready;
    wire           readResp_l;
    
    wire           nextReadResp;
    wire [47:0] readResp_temp;
    
    localparam posx = ((CHAN) + (X)*(Y_MAX-1));
    
    wire writeValid;
    reg  writeValid_lock = 0;

    
	assign awaddr = writeAddress_o[M_A_W+8-1:8];
	assign awlen  = writeAddress_o[7:0];
	
	assign wdata  = writeData_o[255:0];
	assign wlast  = writeData_l;
	
    assign araddr = readAddress_o[M_A_W+8-1:8];
    assign arlen  = readAddress_o[7:0];
    
    
    assign bready = 1'b1;
//    assign rready = i_ack;
    
    
    assign busy   = writeAddress_ready & writeData_ready & readAddress_ready & readResp_ready; //& readAddress_ready & readResp_ready
    assign i_busy = ~busy;
    
    genvar ii;
     

    assign writeValid = (o_v & o`rw & busy)| writeValid_lock;

    reg [3:0] rst_r = 4'b1111;
    always @(posedge clk) begin
        rst_r <= rst_r >> 1;
        writeValid_lock <= writeValid;
        if(o`l && busy && o_v)
            writeValid_lock <= 1'b0;
    end
    
    
    
    wire dummy;
    // Three fifos need 33 bits. So 5 bytes of data. One fifo needs 33+8, so 6 bytes of data
     (*keep_hierarchy = "true"*)axis_data_fifo_0 writeAddress (
                               .s_axis_aresetn    (~rst_r[0])
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & o`rw & busy)
                              ,.s_axis_tready   (writeAddress_ready)
                              ,.s_axis_tdata    ({o`mem_addr,o`burst_len})
                              ,.s_axis_tlast    (o`l)
                              
                              ,.m_axis_tvalid   (awvalid)
                              ,.m_axis_tready   (awready)
                              ,.m_axis_tdata    (writeAddress_o)
                              ,.m_axis_tlast    (writeAddress_l)
                            );
                            
    (*keep_hierarchy = "true"*)axis_data_fifo_2 writeData (
                               .s_axis_aresetn  (~rst_r[0])
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & busy & writeValid_lock)
                              ,.s_axis_tready   (writeData_ready)
                              ,.s_axis_tdata    (o[P_W-1:0] )
                              ,.s_axis_tlast    (o`l)
                              
                              ,.m_axis_tvalid   (wvalid)
                              ,.m_axis_tready   (wready)
                              ,.m_axis_tdata    (writeData_o)
                              ,.m_axis_tlast    (writeData_l)
                        );

    
     (*keep_hierarchy = "true"*)axis_data_fifo_0 readAddress (
                               .s_axis_aresetn  (~rst_r[0])
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & ~o`rw & busy & ~writeValid_lock )
                              ,.s_axis_tready   (readAddress_ready)
                              ,.s_axis_tdata    ({o`mem_addr,o`burst_len})
                              ,.s_axis_tlast    (o`l)
                              
                              
                              ,.m_axis_tvalid   (arvalid)
                              ,.m_axis_tready   (arready)
                              ,.m_axis_tdata    (readAddress_o)
                              ,.m_axis_tlast    (readAddress_l)
                            );
                                                  
    (*keep_hierarchy = "true"*)axis_data_fifo_0 readResponse (
                               .s_axis_aresetn  (~rst_r[0])
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & ~o`rw & busy & ~writeValid_lock)
                              ,.s_axis_tready   (readResp_ready)
                              ,.s_axis_tdata    ({o`src_x,o`src_y,o`mem_addr})
                              ,.s_axis_tlast    (o`l)
                              
                              ,.m_axis_tvalid   (dummy)
                              ,.m_axis_tready   (nextReadResp)
                              ,.m_axis_tdata    (readResp_temp)
                              ,.m_axis_tlast    (readResp_l)
                        );                        


assign readResp_o = {dummy,readResp_l,2'b0,8'b0,readResp_temp[32:0],readResp_temp[38:36],readResp_temp[35:33],208'b0};

 (*keep_hierarchy = "true"*)frame_response#(.P_W(P_W) ,.M_A_W(M_A_W) 
                    ,.D_W(D_W-1), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX), .X(X), .Y(Y)  ) response(
    .clk(clk)
    ,.rst(rst_r[0])
    ,.header_data(readResp_o)
    ,.header_valid(dummy)
    ,.header_ready(nextReadResp)
    
    ,.read_data({rlast,3'b0,rdata[255:0]})
    ,.read_valid(rvalid)
    ,.read_ready(rready)
    
    ,.output_data(i`xv)
    ,.output_valid(i`v)
    ,.output_ready(i_ack)
    
);

//frame_response_0 response(
//    .ap_clk(clk)
//    ,.ap_rst_n(~rst)
//    ,.ap_start(1'b1)
//    ,.header_V_V_TDATA(readResp_o)
//    ,.header_V_V_TREADY(nextReadResp)
//    ,.header_V_V_TVALID(dummy)
    
//    ,.input_V_V_TDATA({rlast,2'b0,rdata[255:0]})
//    ,.input_V_V_TREADY(rready)
//    ,.input_V_V_TVALID(rvalid)
    
//    ,.output_V_V_TDATA(i`xv)    
//    ,.output_V_V_TREADY(i_ack)                                   
//    ,.output_V_V_TVALID(i`v)       
//);

//frame_response_1 response(
//    .ap_clk(clk)
//    ,.ap_rst(rst)
//    ,.ap_start(1'b1)
//    ,.header_V_V_dout(readResp_o)
//    ,.header_V_V_read(nextReadResp)
//    ,.header_V_V_empty_n(dummy)
    
//    ,.input_V_V_dout({rlast,2'b0,rdata[255:0]})
//    ,.input_V_V_read(rready)
//    ,.input_V_V_empty_n(rvalid)
    
//    ,.output_V_V_din(i`xv)    
//    ,.output_V_V_full_n(i_ack)                                   
//    ,.output_V_V_write(i`v)       
//);



	always @(posedge clk) begin
		if (rst) begin
//			i <= 0;
			now <= 0;
			done_sig<=0;
		end else begin
			now <= now + 1;
			if (o_v  & busy) begin
			    received_r <= received_r + 1'b1;
//			    if(posx==7)
//                    $display("Time%0d: Received packet at PE(%0d) with address=%0x packetid=%0x,RW=%d ",now-1,posx,((o`src_y) + (o`src_x)*(Y_MAX-1)),o[32:0],o`rw);
			end
            if(i`v & i_ack) begin
                sent <= sent + 1'b1;
//                if(posx==7)
//                $display("Time%0d: Sent packet from PE(%0d) to PE(%0d) chan)(%0d) with packetid=%0d , data=%X, source_addr =%0d, mem_addr =%X, RW=%d",
//                            now,                   posx,     i`x,  i`port_id & ('d3),            ((posx)*PACKLIMIT+sent),i,((i`y) + (i`x)*(Y_MAX-1)),i`mem_addr,i`rw);
            end

            wait_r <= wait_r + 1'b1;
	   end
	end
endmodule
