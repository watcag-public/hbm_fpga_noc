`include "phalanx.h"
`include "phalanx_tb.h"
`include "parameters.vh"

module Fanout #(
	parameter NUM_CHANNELS = 1, // choose how many Hoplites to try
	parameter X_W	= 1,	// X address width
	parameter Y_W	= 1,	// Y address width
	parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width
	parameter X_MAX	= 1<<X_W,
	parameter Y_MAX	= 1<<Y_W,
	parameter X	= 3,	// X address of this node
	parameter Y	= 2		// Y address of this node
) (
	input wire clk,
	input wire rst,
	input  wire `Msg i,	// input message from client
	output wire i_ack,	// acknowledgement to client
	output wire `MsgChan ii,	// input message to chosen hoplite
	input wire `Chan ii_ack, // input accepted by chosen hoplite
	input wire done_all
);

    wire [4:0] port_id = i`port_id;
    wire [32:0] port_id_confirm = i`mem_addr;
	reg [$clog2(NUM_CHANNELS):0] choice = 0;
    reg [$clog2(NUM_CHANNELS):0] choice_reg = 0;
    reg lock = 0;
    
//	reg [5:0] dest;
	always @(*) begin
        if(lock == 0) begin
            choice = 0;
            if(i`v & i`rw )
                choice = i`port_id & ('d3);
           else if(i`v & ~i`rw)
                choice = i`port_id & ('d3);
		end
		else 
		  choice = choice_reg;
	end

    always @(posedge clk) begin
        if(i`v & i`h) begin
            choice_reg <= choice;
            lock <= 1'b1;
        end
        if(i`v & i`l & i_ack) begin
            choice_reg <= 0;
            lock <= 1'b0;
        end
    end
    
	genvar j;
	generate for (j=0; j< NUM_CHANNELS; j=j+1) begin: js
		assign ii[`Msg_W*(j+1)-1:`Msg_W*j] = (choice==j)? i: 0;
	end endgenerate

	assign i_ack = ii_ack[choice]; 

        `ifndef SYNTHESIS
        integer packets_recv, packets_sent;
        reg displayed;
        always @(posedge clk) begin
                if(rst) begin
                        packets_recv <= 0;
                        packets_sent <= 0;
                        displayed <=0;
                end else begin
                        if(i`v) begin
                                packets_recv <= packets_recv + 1;
                        end
                        if(ii_ack!=0) begin
                                packets_sent <= packets_sent + 1;
                        end
                        if(done_all & !displayed) begin
                                displayed <= 1;
                                //`ifdef DEBUG
                                $display("Sanity, Fanout(%0d,%0d), recv=%0d, sent=%0d",X,Y,packets_recv, packets_sent);
                                //`endif
                        end
                end
        end
        `endif


endmodule

