# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "C_M00_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M00_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M01_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M01_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M02_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M02_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M03_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M03_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M04_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M04_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M05_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M05_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M06_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M06_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M07_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M07_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M08_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M08_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M09_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M09_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M10_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M10_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M11_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M11_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M12_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M12_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M13_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M13_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M14_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M14_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M15_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M15_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M16_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M16_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M17_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M17_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M18_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M18_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M19_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M19_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M20_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M20_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M21_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M21_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M22_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M22_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M23_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M23_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M24_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M24_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M25_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M25_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M26_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M26_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M27_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M27_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M28_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M28_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M29_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M29_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M30_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M30_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M31_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_M31_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_CONTROL_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_CONTROL_DATA_WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.C_M00_AXI_ADDR_WIDTH { PARAM_VALUE.C_M00_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M00_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M00_AXI_ADDR_WIDTH { PARAM_VALUE.C_M00_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M00_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M00_AXI_DATA_WIDTH { PARAM_VALUE.C_M00_AXI_DATA_WIDTH } {
	# Procedure called to update C_M00_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M00_AXI_DATA_WIDTH { PARAM_VALUE.C_M00_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M00_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M01_AXI_ADDR_WIDTH { PARAM_VALUE.C_M01_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M01_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M01_AXI_ADDR_WIDTH { PARAM_VALUE.C_M01_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M01_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M01_AXI_DATA_WIDTH { PARAM_VALUE.C_M01_AXI_DATA_WIDTH } {
	# Procedure called to update C_M01_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M01_AXI_DATA_WIDTH { PARAM_VALUE.C_M01_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M01_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M02_AXI_ADDR_WIDTH { PARAM_VALUE.C_M02_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M02_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M02_AXI_ADDR_WIDTH { PARAM_VALUE.C_M02_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M02_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M02_AXI_DATA_WIDTH { PARAM_VALUE.C_M02_AXI_DATA_WIDTH } {
	# Procedure called to update C_M02_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M02_AXI_DATA_WIDTH { PARAM_VALUE.C_M02_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M02_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M03_AXI_ADDR_WIDTH { PARAM_VALUE.C_M03_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M03_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M03_AXI_ADDR_WIDTH { PARAM_VALUE.C_M03_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M03_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M03_AXI_DATA_WIDTH { PARAM_VALUE.C_M03_AXI_DATA_WIDTH } {
	# Procedure called to update C_M03_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M03_AXI_DATA_WIDTH { PARAM_VALUE.C_M03_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M03_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M04_AXI_ADDR_WIDTH { PARAM_VALUE.C_M04_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M04_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M04_AXI_ADDR_WIDTH { PARAM_VALUE.C_M04_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M04_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M04_AXI_DATA_WIDTH { PARAM_VALUE.C_M04_AXI_DATA_WIDTH } {
	# Procedure called to update C_M04_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M04_AXI_DATA_WIDTH { PARAM_VALUE.C_M04_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M04_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M05_AXI_ADDR_WIDTH { PARAM_VALUE.C_M05_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M05_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M05_AXI_ADDR_WIDTH { PARAM_VALUE.C_M05_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M05_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M05_AXI_DATA_WIDTH { PARAM_VALUE.C_M05_AXI_DATA_WIDTH } {
	# Procedure called to update C_M05_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M05_AXI_DATA_WIDTH { PARAM_VALUE.C_M05_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M05_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M06_AXI_ADDR_WIDTH { PARAM_VALUE.C_M06_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M06_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M06_AXI_ADDR_WIDTH { PARAM_VALUE.C_M06_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M06_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M06_AXI_DATA_WIDTH { PARAM_VALUE.C_M06_AXI_DATA_WIDTH } {
	# Procedure called to update C_M06_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M06_AXI_DATA_WIDTH { PARAM_VALUE.C_M06_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M06_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M07_AXI_ADDR_WIDTH { PARAM_VALUE.C_M07_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M07_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M07_AXI_ADDR_WIDTH { PARAM_VALUE.C_M07_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M07_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M07_AXI_DATA_WIDTH { PARAM_VALUE.C_M07_AXI_DATA_WIDTH } {
	# Procedure called to update C_M07_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M07_AXI_DATA_WIDTH { PARAM_VALUE.C_M07_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M07_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M08_AXI_ADDR_WIDTH { PARAM_VALUE.C_M08_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M08_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M08_AXI_ADDR_WIDTH { PARAM_VALUE.C_M08_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M08_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M08_AXI_DATA_WIDTH { PARAM_VALUE.C_M08_AXI_DATA_WIDTH } {
	# Procedure called to update C_M08_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M08_AXI_DATA_WIDTH { PARAM_VALUE.C_M08_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M08_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M09_AXI_ADDR_WIDTH { PARAM_VALUE.C_M09_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M09_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M09_AXI_ADDR_WIDTH { PARAM_VALUE.C_M09_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M09_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M09_AXI_DATA_WIDTH { PARAM_VALUE.C_M09_AXI_DATA_WIDTH } {
	# Procedure called to update C_M09_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M09_AXI_DATA_WIDTH { PARAM_VALUE.C_M09_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M09_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M10_AXI_ADDR_WIDTH { PARAM_VALUE.C_M10_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M10_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M10_AXI_ADDR_WIDTH { PARAM_VALUE.C_M10_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M10_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M10_AXI_DATA_WIDTH { PARAM_VALUE.C_M10_AXI_DATA_WIDTH } {
	# Procedure called to update C_M10_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M10_AXI_DATA_WIDTH { PARAM_VALUE.C_M10_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M10_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M11_AXI_ADDR_WIDTH { PARAM_VALUE.C_M11_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M11_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M11_AXI_ADDR_WIDTH { PARAM_VALUE.C_M11_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M11_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M11_AXI_DATA_WIDTH { PARAM_VALUE.C_M11_AXI_DATA_WIDTH } {
	# Procedure called to update C_M11_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M11_AXI_DATA_WIDTH { PARAM_VALUE.C_M11_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M11_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M12_AXI_ADDR_WIDTH { PARAM_VALUE.C_M12_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M12_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M12_AXI_ADDR_WIDTH { PARAM_VALUE.C_M12_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M12_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M12_AXI_DATA_WIDTH { PARAM_VALUE.C_M12_AXI_DATA_WIDTH } {
	# Procedure called to update C_M12_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M12_AXI_DATA_WIDTH { PARAM_VALUE.C_M12_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M12_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M13_AXI_ADDR_WIDTH { PARAM_VALUE.C_M13_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M13_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M13_AXI_ADDR_WIDTH { PARAM_VALUE.C_M13_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M13_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M13_AXI_DATA_WIDTH { PARAM_VALUE.C_M13_AXI_DATA_WIDTH } {
	# Procedure called to update C_M13_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M13_AXI_DATA_WIDTH { PARAM_VALUE.C_M13_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M13_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M14_AXI_ADDR_WIDTH { PARAM_VALUE.C_M14_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M14_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M14_AXI_ADDR_WIDTH { PARAM_VALUE.C_M14_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M14_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M14_AXI_DATA_WIDTH { PARAM_VALUE.C_M14_AXI_DATA_WIDTH } {
	# Procedure called to update C_M14_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M14_AXI_DATA_WIDTH { PARAM_VALUE.C_M14_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M14_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M15_AXI_ADDR_WIDTH { PARAM_VALUE.C_M15_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M15_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M15_AXI_ADDR_WIDTH { PARAM_VALUE.C_M15_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M15_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M15_AXI_DATA_WIDTH { PARAM_VALUE.C_M15_AXI_DATA_WIDTH } {
	# Procedure called to update C_M15_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M15_AXI_DATA_WIDTH { PARAM_VALUE.C_M15_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M15_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M16_AXI_ADDR_WIDTH { PARAM_VALUE.C_M16_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M16_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M16_AXI_ADDR_WIDTH { PARAM_VALUE.C_M16_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M16_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M16_AXI_DATA_WIDTH { PARAM_VALUE.C_M16_AXI_DATA_WIDTH } {
	# Procedure called to update C_M16_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M16_AXI_DATA_WIDTH { PARAM_VALUE.C_M16_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M16_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M17_AXI_ADDR_WIDTH { PARAM_VALUE.C_M17_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M17_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M17_AXI_ADDR_WIDTH { PARAM_VALUE.C_M17_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M17_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M17_AXI_DATA_WIDTH { PARAM_VALUE.C_M17_AXI_DATA_WIDTH } {
	# Procedure called to update C_M17_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M17_AXI_DATA_WIDTH { PARAM_VALUE.C_M17_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M17_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M18_AXI_ADDR_WIDTH { PARAM_VALUE.C_M18_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M18_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M18_AXI_ADDR_WIDTH { PARAM_VALUE.C_M18_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M18_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M18_AXI_DATA_WIDTH { PARAM_VALUE.C_M18_AXI_DATA_WIDTH } {
	# Procedure called to update C_M18_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M18_AXI_DATA_WIDTH { PARAM_VALUE.C_M18_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M18_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M19_AXI_ADDR_WIDTH { PARAM_VALUE.C_M19_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M19_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M19_AXI_ADDR_WIDTH { PARAM_VALUE.C_M19_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M19_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M19_AXI_DATA_WIDTH { PARAM_VALUE.C_M19_AXI_DATA_WIDTH } {
	# Procedure called to update C_M19_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M19_AXI_DATA_WIDTH { PARAM_VALUE.C_M19_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M19_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M20_AXI_ADDR_WIDTH { PARAM_VALUE.C_M20_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M20_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M20_AXI_ADDR_WIDTH { PARAM_VALUE.C_M20_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M20_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M20_AXI_DATA_WIDTH { PARAM_VALUE.C_M20_AXI_DATA_WIDTH } {
	# Procedure called to update C_M20_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M20_AXI_DATA_WIDTH { PARAM_VALUE.C_M20_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M20_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M21_AXI_ADDR_WIDTH { PARAM_VALUE.C_M21_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M21_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M21_AXI_ADDR_WIDTH { PARAM_VALUE.C_M21_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M21_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M21_AXI_DATA_WIDTH { PARAM_VALUE.C_M21_AXI_DATA_WIDTH } {
	# Procedure called to update C_M21_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M21_AXI_DATA_WIDTH { PARAM_VALUE.C_M21_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M21_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M22_AXI_ADDR_WIDTH { PARAM_VALUE.C_M22_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M22_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M22_AXI_ADDR_WIDTH { PARAM_VALUE.C_M22_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M22_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M22_AXI_DATA_WIDTH { PARAM_VALUE.C_M22_AXI_DATA_WIDTH } {
	# Procedure called to update C_M22_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M22_AXI_DATA_WIDTH { PARAM_VALUE.C_M22_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M22_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M23_AXI_ADDR_WIDTH { PARAM_VALUE.C_M23_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M23_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M23_AXI_ADDR_WIDTH { PARAM_VALUE.C_M23_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M23_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M23_AXI_DATA_WIDTH { PARAM_VALUE.C_M23_AXI_DATA_WIDTH } {
	# Procedure called to update C_M23_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M23_AXI_DATA_WIDTH { PARAM_VALUE.C_M23_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M23_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M24_AXI_ADDR_WIDTH { PARAM_VALUE.C_M24_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M24_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M24_AXI_ADDR_WIDTH { PARAM_VALUE.C_M24_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M24_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M24_AXI_DATA_WIDTH { PARAM_VALUE.C_M24_AXI_DATA_WIDTH } {
	# Procedure called to update C_M24_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M24_AXI_DATA_WIDTH { PARAM_VALUE.C_M24_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M24_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M25_AXI_ADDR_WIDTH { PARAM_VALUE.C_M25_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M25_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M25_AXI_ADDR_WIDTH { PARAM_VALUE.C_M25_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M25_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M25_AXI_DATA_WIDTH { PARAM_VALUE.C_M25_AXI_DATA_WIDTH } {
	# Procedure called to update C_M25_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M25_AXI_DATA_WIDTH { PARAM_VALUE.C_M25_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M25_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M26_AXI_ADDR_WIDTH { PARAM_VALUE.C_M26_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M26_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M26_AXI_ADDR_WIDTH { PARAM_VALUE.C_M26_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M26_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M26_AXI_DATA_WIDTH { PARAM_VALUE.C_M26_AXI_DATA_WIDTH } {
	# Procedure called to update C_M26_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M26_AXI_DATA_WIDTH { PARAM_VALUE.C_M26_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M26_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M27_AXI_ADDR_WIDTH { PARAM_VALUE.C_M27_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M27_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M27_AXI_ADDR_WIDTH { PARAM_VALUE.C_M27_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M27_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M27_AXI_DATA_WIDTH { PARAM_VALUE.C_M27_AXI_DATA_WIDTH } {
	# Procedure called to update C_M27_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M27_AXI_DATA_WIDTH { PARAM_VALUE.C_M27_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M27_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M28_AXI_ADDR_WIDTH { PARAM_VALUE.C_M28_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M28_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M28_AXI_ADDR_WIDTH { PARAM_VALUE.C_M28_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M28_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M28_AXI_DATA_WIDTH { PARAM_VALUE.C_M28_AXI_DATA_WIDTH } {
	# Procedure called to update C_M28_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M28_AXI_DATA_WIDTH { PARAM_VALUE.C_M28_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M28_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M29_AXI_ADDR_WIDTH { PARAM_VALUE.C_M29_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M29_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M29_AXI_ADDR_WIDTH { PARAM_VALUE.C_M29_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M29_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M29_AXI_DATA_WIDTH { PARAM_VALUE.C_M29_AXI_DATA_WIDTH } {
	# Procedure called to update C_M29_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M29_AXI_DATA_WIDTH { PARAM_VALUE.C_M29_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M29_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M30_AXI_ADDR_WIDTH { PARAM_VALUE.C_M30_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M30_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M30_AXI_ADDR_WIDTH { PARAM_VALUE.C_M30_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M30_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M30_AXI_DATA_WIDTH { PARAM_VALUE.C_M30_AXI_DATA_WIDTH } {
	# Procedure called to update C_M30_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M30_AXI_DATA_WIDTH { PARAM_VALUE.C_M30_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M30_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M31_AXI_ADDR_WIDTH { PARAM_VALUE.C_M31_AXI_ADDR_WIDTH } {
	# Procedure called to update C_M31_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M31_AXI_ADDR_WIDTH { PARAM_VALUE.C_M31_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_M31_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_M31_AXI_DATA_WIDTH { PARAM_VALUE.C_M31_AXI_DATA_WIDTH } {
	# Procedure called to update C_M31_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M31_AXI_DATA_WIDTH { PARAM_VALUE.C_M31_AXI_DATA_WIDTH } {
	# Procedure called to validate C_M31_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH } {
	# Procedure called to update C_S_AXI_CONTROL_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH } {
	# Procedure called to validate C_S_AXI_CONTROL_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH { PARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH } {
	# Procedure called to update C_S_AXI_CONTROL_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH { PARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH } {
	# Procedure called to validate C_S_AXI_CONTROL_DATA_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH { MODELPARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH PARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_CONTROL_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH { MODELPARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH PARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_CONTROL_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M00_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M00_AXI_ADDR_WIDTH PARAM_VALUE.C_M00_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M00_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M00_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M00_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M00_AXI_DATA_WIDTH PARAM_VALUE.C_M00_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M00_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M00_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M01_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M01_AXI_ADDR_WIDTH PARAM_VALUE.C_M01_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M01_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M01_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M01_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M01_AXI_DATA_WIDTH PARAM_VALUE.C_M01_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M01_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M01_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M02_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M02_AXI_ADDR_WIDTH PARAM_VALUE.C_M02_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M02_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M02_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M02_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M02_AXI_DATA_WIDTH PARAM_VALUE.C_M02_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M02_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M02_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M03_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M03_AXI_ADDR_WIDTH PARAM_VALUE.C_M03_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M03_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M03_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M03_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M03_AXI_DATA_WIDTH PARAM_VALUE.C_M03_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M03_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M03_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M04_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M04_AXI_ADDR_WIDTH PARAM_VALUE.C_M04_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M04_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M04_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M04_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M04_AXI_DATA_WIDTH PARAM_VALUE.C_M04_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M04_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M04_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M05_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M05_AXI_ADDR_WIDTH PARAM_VALUE.C_M05_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M05_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M05_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M05_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M05_AXI_DATA_WIDTH PARAM_VALUE.C_M05_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M05_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M05_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M06_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M06_AXI_ADDR_WIDTH PARAM_VALUE.C_M06_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M06_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M06_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M06_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M06_AXI_DATA_WIDTH PARAM_VALUE.C_M06_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M06_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M06_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M07_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M07_AXI_ADDR_WIDTH PARAM_VALUE.C_M07_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M07_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M07_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M07_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M07_AXI_DATA_WIDTH PARAM_VALUE.C_M07_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M07_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M07_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M08_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M08_AXI_ADDR_WIDTH PARAM_VALUE.C_M08_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M08_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M08_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M08_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M08_AXI_DATA_WIDTH PARAM_VALUE.C_M08_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M08_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M08_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M09_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M09_AXI_ADDR_WIDTH PARAM_VALUE.C_M09_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M09_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M09_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M09_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M09_AXI_DATA_WIDTH PARAM_VALUE.C_M09_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M09_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M09_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M10_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M10_AXI_ADDR_WIDTH PARAM_VALUE.C_M10_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M10_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M10_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M10_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M10_AXI_DATA_WIDTH PARAM_VALUE.C_M10_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M10_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M10_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M11_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M11_AXI_ADDR_WIDTH PARAM_VALUE.C_M11_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M11_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M11_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M11_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M11_AXI_DATA_WIDTH PARAM_VALUE.C_M11_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M11_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M11_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M12_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M12_AXI_ADDR_WIDTH PARAM_VALUE.C_M12_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M12_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M12_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M12_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M12_AXI_DATA_WIDTH PARAM_VALUE.C_M12_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M12_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M12_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M13_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M13_AXI_ADDR_WIDTH PARAM_VALUE.C_M13_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M13_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M13_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M13_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M13_AXI_DATA_WIDTH PARAM_VALUE.C_M13_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M13_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M13_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M14_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M14_AXI_ADDR_WIDTH PARAM_VALUE.C_M14_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M14_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M14_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M14_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M14_AXI_DATA_WIDTH PARAM_VALUE.C_M14_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M14_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M14_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M15_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M15_AXI_ADDR_WIDTH PARAM_VALUE.C_M15_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M15_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M15_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M15_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M15_AXI_DATA_WIDTH PARAM_VALUE.C_M15_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M15_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M15_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M16_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M16_AXI_ADDR_WIDTH PARAM_VALUE.C_M16_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M16_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M16_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M16_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M16_AXI_DATA_WIDTH PARAM_VALUE.C_M16_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M16_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M16_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M17_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M17_AXI_ADDR_WIDTH PARAM_VALUE.C_M17_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M17_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M17_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M17_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M17_AXI_DATA_WIDTH PARAM_VALUE.C_M17_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M17_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M17_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M18_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M18_AXI_ADDR_WIDTH PARAM_VALUE.C_M18_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M18_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M18_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M18_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M18_AXI_DATA_WIDTH PARAM_VALUE.C_M18_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M18_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M18_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M19_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M19_AXI_ADDR_WIDTH PARAM_VALUE.C_M19_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M19_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M19_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M19_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M19_AXI_DATA_WIDTH PARAM_VALUE.C_M19_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M19_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M19_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M20_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M20_AXI_ADDR_WIDTH PARAM_VALUE.C_M20_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M20_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M20_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M20_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M20_AXI_DATA_WIDTH PARAM_VALUE.C_M20_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M20_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M20_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M21_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M21_AXI_ADDR_WIDTH PARAM_VALUE.C_M21_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M21_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M21_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M21_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M21_AXI_DATA_WIDTH PARAM_VALUE.C_M21_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M21_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M21_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M22_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M22_AXI_ADDR_WIDTH PARAM_VALUE.C_M22_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M22_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M22_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M22_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M22_AXI_DATA_WIDTH PARAM_VALUE.C_M22_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M22_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M22_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M23_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M23_AXI_ADDR_WIDTH PARAM_VALUE.C_M23_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M23_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M23_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M23_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M23_AXI_DATA_WIDTH PARAM_VALUE.C_M23_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M23_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M23_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M24_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M24_AXI_ADDR_WIDTH PARAM_VALUE.C_M24_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M24_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M24_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M24_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M24_AXI_DATA_WIDTH PARAM_VALUE.C_M24_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M24_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M24_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M25_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M25_AXI_ADDR_WIDTH PARAM_VALUE.C_M25_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M25_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M25_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M25_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M25_AXI_DATA_WIDTH PARAM_VALUE.C_M25_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M25_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M25_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M26_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M26_AXI_ADDR_WIDTH PARAM_VALUE.C_M26_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M26_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M26_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M26_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M26_AXI_DATA_WIDTH PARAM_VALUE.C_M26_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M26_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M26_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M27_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M27_AXI_ADDR_WIDTH PARAM_VALUE.C_M27_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M27_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M27_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M27_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M27_AXI_DATA_WIDTH PARAM_VALUE.C_M27_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M27_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M27_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M28_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M28_AXI_ADDR_WIDTH PARAM_VALUE.C_M28_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M28_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M28_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M28_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M28_AXI_DATA_WIDTH PARAM_VALUE.C_M28_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M28_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M28_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M29_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M29_AXI_ADDR_WIDTH PARAM_VALUE.C_M29_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M29_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M29_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M29_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M29_AXI_DATA_WIDTH PARAM_VALUE.C_M29_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M29_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M29_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M30_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M30_AXI_ADDR_WIDTH PARAM_VALUE.C_M30_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M30_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M30_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M30_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M30_AXI_DATA_WIDTH PARAM_VALUE.C_M30_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M30_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M30_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M31_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_M31_AXI_ADDR_WIDTH PARAM_VALUE.C_M31_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M31_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_M31_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_M31_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_M31_AXI_DATA_WIDTH PARAM_VALUE.C_M31_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M31_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_M31_AXI_DATA_WIDTH}
}

