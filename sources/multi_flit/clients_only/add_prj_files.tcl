add_files -norecurse -scan_for_includes {./src/}
import_files -norecurse {./src/}

move_files -fileset sim_1 [get_files ./prj/prj.srcs/sources_1/imports/src/bft_tb.sv]

create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name axis_data_fifo_0
set_property -dict [list CONFIG.TDATA_NUM_BYTES {32} CONFIG.FIFO_DEPTH {16} CONFIG.HAS_TLAST {1}] [get_ips axis_data_fifo_0]
generate_target {instantiation_template} [get_files ./prj/prj.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]
generate_target all [get_files ./prj/prj.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]

catch { config_ip_cache -export [get_ips -all axis_data_fifo_0] }
export_ip_user_files -of_objects [get_files ./prj/prj.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ./prj/prj.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]
launch_runs -jobs 2 axis_data_fifo_0_synth_1
export_simulation -of_objects [get_files ./prj/prj.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci] -directory ./prj/prj.ip_user_files/sim_scripts -ip_user_files_dir ./prj/prj.ip_user_files -ipstatic_source_dir ./prj/prj.ip_user_files/ipstatic -lib_map_path [list {modelsim= ./prj/prj.cache/compile_simlib/modelsim} {questa= ./prj/prj.cache/compile_simlib/questa} {ies= ./prj/prj.cache/compile_simlib/ies} {xcelium= ./prj/prj.cache/compile_simlib/xcelium} {vcs= ./prj/prj.cache/compile_simlib/vcs} {riviera= ./prj/prj.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name axis_data_fifo_1
set_property -dict [list CONFIG.TDATA_NUM_BYTES {6} CONFIG.FIFO_DEPTH {16} CONFIG.HAS_TLAST {1}] [get_ips axis_data_fifo_1]
generate_target {instantiation_template} [get_files ./prj/prj.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci]
update_compile_order -fileset sources_1
generate_target all [get_files  ./prj/prj.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci]
catch { config_ip_cache -export [get_ips -all axis_data_fifo_1] }
export_ip_user_files -of_objects [get_files ./prj/prj.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ./prj/prj.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci]
launch_runs -jobs 2 axis_data_fifo_1_synth_1
export_simulation -of_objects [get_files ./prj/prj.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci] -directory ./prj/prj.ip_user_files/sim_scripts -ip_user_files_dir ./prj/prj.ip_user_files -ipstatic_source_dir ./prj/prj.ip_user_files/ipstatic -lib_map_path [list {modelsim=./prj/prj.cache/compile_simlib/modelsim} {questa=./prj/prj.cache/compile_simlib/questa} {ies=./prj/prj.cache/compile_simlib/ies} {xcelium=./prj/prj.cache/compile_simlib/xcelium} {vcs=./prj/prj.cache/compile_simlib/vcs} {riviera=./prj/prj.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fifo_generator_1
set_property -dict [list CONFIG.Component_Name {fifo_generator_1} CONFIG.Fifo_Implementation {Common_Clock_Shift_Register} CONFIG.Input_Data_Width {260} CONFIG.Input_Depth {16} CONFIG.Output_Data_Width {260} CONFIG.Output_Depth {16} CONFIG.Use_Embedded_Registers {false} CONFIG.Data_Count_Width {4} CONFIG.Write_Data_Count_Width {4} CONFIG.Read_Data_Count_Width {4} CONFIG.Programmable_Full_Type {Single_Programmable_Full_Threshold_Input_Port} CONFIG.Full_Threshold_Assert_Value {14} CONFIG.Full_Threshold_Negate_Value {13}] [get_ips fifo_generator_1]
generate_target {instantiation_template} [get_files ./prj/prj.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1.xci]
generate_target all [get_files  ./prj/prj.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1.xci]
catch { config_ip_cache -export [get_ips -all fifo_generator_1] }
export_ip_user_files -of_objects [get_files ./prj/prj.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ./prj/prj.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1.xci]
launch_runs -jobs 2 fifo_generator_1_synth_1
export_simulation -of_objects [get_files ./prj/prj.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1.xci] -directory ./prj/prj.ip_user_files/sim_scripts -ip_user_files_dir ./prj/prj.ip_user_files -ipstatic_source_dir ./prj/prj.ip_user_files/ipstatic -lib_map_path [list {modelsim=./prj/prj.cache/compile_simlib/modelsim} {questa=./prj/prj.cache/compile_simlib/questa} {ies=./prj/prj.cache/compile_simlib/ies} {xcelium=./prj/prj.cache/compile_simlib/xcelium} {vcs=./prj/prj.cache/compile_simlib/vcs} {riviera=./prj/prj.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet


