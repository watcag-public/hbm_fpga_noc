ipx::package_project -root_dir ./prj/ip -force -vendor user.org -library user -taxonomy /UserIP -import_files -set_current false
ipx::unload_core ./prj/ip/component.xml
ipx::edit_ip_in_project -upgrade true -name tmp_edit_project -directory ./prj/ip ./prj/ip/component.xml
source clk.tcl
set_property core_revision 2 [ipx::current_core]
ipx::update_source_project_archive -component [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
ipx::move_temp_component_back -component [ipx::current_core]
close_project -delete