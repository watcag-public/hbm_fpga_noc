// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
`timescale 1ns/1ps
`include "commands.h"

module bft_32_xbar_v2_control_s_axi
#(parameter
    C_S_AXI_ADDR_WIDTH = 8,
    C_S_AXI_DATA_WIDTH = 32
)(
    input  wire                          ACLK,
    input  wire                          ARESET,
    input  wire                          ACLK_EN,
    input  wire [C_S_AXI_ADDR_WIDTH-1:0] AWADDR,
    input  wire                          AWVALID,
    output wire                          AWREADY,
    input  wire [C_S_AXI_DATA_WIDTH-1:0] WDATA,
    input  wire [C_S_AXI_DATA_WIDTH/8-1:0] WSTRB,
    input  wire                          WVALID,
    output wire                          WREADY,
    output wire [1:0]                    BRESP,
    output wire                          BVALID,
    input  wire                          BREADY,
    input  wire [C_S_AXI_ADDR_WIDTH-1:0] ARADDR,
    input  wire                          ARVALID,
    output wire                          ARREADY,
    output wire [C_S_AXI_DATA_WIDTH-1:0] RDATA,
    output wire [1:0]                    RRESP,
    output wire                          RVALID,
    input  wire                          RREADY,
    output wire                          interrupt,
    output wire                          ap_start,
    input  wire                          ap_done,
    input  wire                          ap_ready,
    input  wire                          ap_idle,
    output wire [31:0]                   scalar00,
    output wire [63:0]                   A
    `ifdef XBARorM1orM0

    ,output wire [63:0]                   B,
    output wire [63:0]                   C,
    output wire [63:0]                   D
    `endif
`ifdef XBARorM1

    ,output wire [63:0]                   E,
    output wire [63:0]                   F,
    output wire [63:0]                   G,
    output wire [63:0]                   H
 `endif
`ifdef XBARorM0 

    ,output wire [63:0]                   I,
    output wire [63:0]                   J,
    output wire [63:0]                   K,
    output wire [63:0]                   L
    `endif
`ifdef XBAR 
    ,output wire [63:0]                   M,
    output wire [63:0]                   N,
    output wire [63:0]                   O,
    output wire [63:0]                   P,
    output wire [64-1:0]                 Q ,
    output wire [64-1:0]                 R ,
    output wire [64-1:0]                 S ,
    output wire [64-1:0]                 T ,
    output wire [64-1:0]                 U ,
    output wire [64-1:0]                 V ,
    output wire [64-1:0]                 W ,
    output wire [64-1:0]                 X ,
    output wire [64-1:0]                 Y ,
    output wire [64-1:0]                 Z ,
    output wire [64-1:0]                 AA,
    output wire [64-1:0]                 AB,
    output wire [64-1:0]                 AC,
    output wire [64-1:0]                 AD,
    output wire [64-1:0]                 AE,
    output wire [64-1:0]                 AF
`endif
);
//------------------------Address Info-------------------
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read/COR)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x10 : Data signal of scalar00
//        bit 31~0 - scalar00[31:0] (Read/Write)
// 0x14 : reserved
// 0x18 : Data signal of A
//        bit 31~0 - A[31:0] (Read/Write)
// 0x1c : Data signal of A
//        bit 31~0 - A[63:32] (Read/Write)
// 0x20 : reserved
// 0x24 : Data signal of B
//        bit 31~0 - B[31:0] (Read/Write)
// 0x28 : Data signal of B
//        bit 31~0 - B[63:32] (Read/Write)
// 0x2c : reserved
// 0x30 : Data signal of C
//        bit 31~0 - C[31:0] (Read/Write)
// 0x34 : Data signal of C
//        bit 31~0 - C[63:32] (Read/Write)
// 0x38 : reserved
// 0x3c : Data signal of D
//        bit 31~0 - D[31:0] (Read/Write)
// 0x40 : Data signal of D
//        bit 31~0 - D[63:32] (Read/Write)
// 0x44 : reserved
// 0x48 : Data signal of E
//        bit 31~0 - E[31:0] (Read/Write)
// 0x4c : Data signal of E
//        bit 31~0 - E[63:32] (Read/Write)
// 0x50 : reserved
// 0x54 : Data signal of F
//        bit 31~0 - F[31:0] (Read/Write)
// 0x58 : Data signal of F
//        bit 31~0 - F[63:32] (Read/Write)
// 0x5c : reserved
// 0x60 : Data signal of G
//        bit 31~0 - G[31:0] (Read/Write)
// 0x64 : Data signal of G
//        bit 31~0 - G[63:32] (Read/Write)
// 0x68 : reserved
// 0x6c : Data signal of H
//        bit 31~0 - H[31:0] (Read/Write)
// 0x70 : Data signal of H
//        bit 31~0 - H[63:32] (Read/Write)
// 0x74 : reserved
// 0x78 : Data signal of I
//        bit 31~0 - I[31:0] (Read/Write)
// 0x7c : Data signal of I
//        bit 31~0 - I[63:32] (Read/Write)
// 0x80 : reserved
// 0x84 : Data signal of J
//        bit 31~0 - J[31:0] (Read/Write)
// 0x88 : Data signal of J
//        bit 31~0 - J[63:32] (Read/Write)
// 0x8c : reserved
// 0x90 : Data signal of K
//        bit 31~0 - K[31:0] (Read/Write)
// 0x94 : Data signal of K
//        bit 31~0 - K[63:32] (Read/Write)
// 0x98 : reserved
// 0x9c : Data signal of L
//        bit 31~0 - L[31:0] (Read/Write)
// 0xa0 : Data signal of L
//        bit 31~0 - L[63:32] (Read/Write)
// 0xa4 : reserved
// 0xa8 : Data signal of M
//        bit 31~0 - M[31:0] (Read/Write)
// 0xac : Data signal of M
//        bit 31~0 - M[63:32] (Read/Write)
// 0xb0 : reserved
// 0xb4 : Data signal of N
//        bit 31~0 - N[31:0] (Read/Write)
// 0xb8 : Data signal of N
//        bit 31~0 - N[63:32] (Read/Write)
// 0xbc : reserved
// 0xc0 : Data signal of O
//        bit 31~0 - O[31:0] (Read/Write)
// 0xc4 : Data signal of O
//        bit 31~0 - O[63:32] (Read/Write)
// 0xc8 : reserved
// 0xcc : Data signal of P
//        bit 31~0 - P[31:0] (Read/Write)
// 0xd0 : Data signal of P
//        bit 31~0 - P[63:32] (Read/Write)
// 0xd4 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

//------------------------Parameter----------------------
localparam
    ADDR_AP_CTRL         = 8'h00,
    ADDR_GIE             = 8'h04,
    ADDR_IER             = 8'h08,
    ADDR_ISR             = 8'h0c,
    ADDR_SCALAR00_DATA_0 = 8'h10,
    ADDR_SCALAR00_CTRL   = 8'h14,
    ADDR_A_DATA_0        = 8'h18,
    ADDR_A_DATA_1        = 8'h1c,
    ADDR_A_CTRL          = 8'h20,
    `ifdef XBARorM1orM0

    ADDR_B_DATA_0        = 8'h24,
    ADDR_B_DATA_1        = 8'h28,
    ADDR_B_CTRL          = 8'h2c,
    ADDR_C_DATA_0        = 8'h30,
    ADDR_C_DATA_1        = 8'h34,
    ADDR_C_CTRL          = 8'h38,
    ADDR_D_DATA_0        = 8'h3c,
    ADDR_D_DATA_1        = 8'h40,
    ADDR_D_CTRL          = 8'h44,
    
`endif
`ifdef XBARorM1 
   ADDR_E_DATA_0        = 8'h48,
    ADDR_E_DATA_1        = 8'h4c,
    ADDR_E_CTRL          = 8'h50,
    ADDR_F_DATA_0        = 8'h54,
    ADDR_F_DATA_1        = 8'h58,
    ADDR_F_CTRL          = 8'h5c,
    ADDR_G_DATA_0        = 8'h60,
    ADDR_G_DATA_1        = 8'h64,
    ADDR_G_CTRL          = 8'h68,
    ADDR_H_DATA_0        = 8'h6c,
    ADDR_H_DATA_1        = 8'h70,
    ADDR_H_CTRL          = 8'h74,
  
`endif
`ifdef XBARorM0 
  
    ADDR_I_DATA_0        = 8'h78,
    ADDR_I_DATA_1        = 8'h7c,
    ADDR_I_CTRL          = 8'h80,
    ADDR_J_DATA_0        = 8'h84,
    ADDR_J_DATA_1        = 8'h88,
    ADDR_J_CTRL          = 8'h8c,
    ADDR_K_DATA_0        = 8'h90,
    ADDR_K_DATA_1        = 8'h94,
    ADDR_K_CTRL          = 8'h98,
    ADDR_L_DATA_0        = 8'h9c,
    ADDR_L_DATA_1        = 8'ha0,
    ADDR_L_CTRL          = 8'ha4,
  
  `endif
`ifdef XBAR 
    ADDR_M_DATA_0        = 8'ha8,
    ADDR_M_DATA_1        = 8'hac,
    ADDR_M_CTRL          = 8'hb0,
    ADDR_N_DATA_0        = 8'hb4,
    ADDR_N_DATA_1        = 8'hb8,
    ADDR_N_CTRL          = 8'hbc,
    ADDR_O_DATA_0        = 8'hc0,
    ADDR_O_DATA_1        = 8'hc4,
    ADDR_O_CTRL          = 8'hc8,
    ADDR_P_DATA_0        = 8'hcc,
    ADDR_P_DATA_1        = 8'hd0,
    ADDR_P_CTRL          = 8'hd4,
    
    ADDR_Q_DATA_0        = 8'hd8,
    ADDR_Q_DATA_1        = 8'hdc,
    ADDR_Q_CTRL          = 8'he0,
    ADDR_R_DATA_0        = 8'he4,
    ADDR_R_DATA_1        = 8'he8,
    ADDR_R_CTRL          = 8'hec,
    ADDR_S_DATA_0        = 8'hf0,
    ADDR_S_DATA_1        = 8'hf4,
    ADDR_S_CTRL          = 8'hf8,
    ADDR_T_DATA_0        = 8'hfc,
    ADDR_T_DATA_1        = 12'h100,
    ADDR_T_CTRL          = 12'h104,
    ADDR_U_DATA_0        = 12'h108,
    ADDR_U_DATA_1        = 12'h10c,
    ADDR_U_CTRL          = 12'h110,
    ADDR_V_DATA_0        = 12'h114,
    ADDR_V_DATA_1        = 12'h118,
    ADDR_V_CTRL          = 12'h11c,
    ADDR_W_DATA_0        = 12'h120,
    ADDR_W_DATA_1        = 12'h124,
    ADDR_W_CTRL          = 12'h128,
    ADDR_X_DATA_0        = 12'h12c,
    ADDR_X_DATA_1        = 12'h130,
    ADDR_X_CTRL          = 12'h134,
    ADDR_Y_DATA_0        = 12'h138,
    ADDR_Y_DATA_1        = 12'h13c,
    ADDR_Y_CTRL          = 12'h140,
    ADDR_Z_DATA_0        = 12'h144,
    ADDR_Z_DATA_1        = 12'h148,
    ADDR_Z_CTRL          = 12'h14c,
    ADDR_AA_DATA_0        = 12'h150,
    ADDR_AA_DATA_1        = 12'h154,
    ADDR_AA_CTRL          = 12'h158,
    ADDR_AB_DATA_0        = 12'h15c,
    ADDR_AB_DATA_1        = 12'h160,
    ADDR_AB_CTRL          = 12'h164,
    ADDR_AC_DATA_0        = 12'h168,
    ADDR_AC_DATA_1        = 12'h16c,
    ADDR_AC_CTRL          = 12'h170,
    ADDR_AD_DATA_0        = 12'h174,
    ADDR_AD_DATA_1        = 12'h178,
    ADDR_AD_CTRL          = 12'h17c,
    ADDR_AE_DATA_0        = 12'h180,
    ADDR_AE_DATA_1        = 12'h184,
    ADDR_AE_CTRL          = 12'h188,
    ADDR_AF_DATA_0        = 12'h18c,
    ADDR_AF_DATA_1        = 12'h190,
    ADDR_AF_CTRL          = 12'h194,
    
    `endif
    WRIDLE               = 2'd0,
    WRDATA               = 2'd1,
    WRRESP               = 2'd2,
    WRRESET              = 2'd3,
    RDIDLE               = 2'd0,
    RDDATA               = 2'd1,
    RDRESET              = 2'd2,
    ADDR_BITS         = 12;

//------------------------Local signal-------------------
    reg  [1:0]                    wstate = WRRESET;
    reg  [1:0]                    wnext;
    reg  [ADDR_BITS-1:0]          waddr;
    wire [31:0]                   wmask;
    wire                          aw_hs;
    wire                          w_hs;
    reg  [1:0]                    rstate = RDRESET;
    reg  [1:0]                    rnext;
    reg  [31:0]                   rdata;
    wire                          ar_hs;
    wire [ADDR_BITS-1:0]          raddr;
    // internal registers
    reg                           int_ap_idle;
    reg                           int_ap_ready;
    reg                           int_ap_done = 1'b0;
    reg                           int_ap_start = 1'b0;
    reg                           int_auto_restart = 1'b0;
    reg                           int_gie = 1'b0;
    reg  [1:0]                    int_ier = 2'b0;
    reg  [1:0]                    int_isr = 2'b0;
    reg  [31:0]                   int_scalar00 = 'b0;
    
    reg  [63:0]                   int_A = 'b0;
    `ifdef XBARorM1orM0

    reg  [63:0]                   int_B = 'b0;
    reg  [63:0]                   int_C = 'b0;
    reg  [63:0]                   int_D = 'b0;
    `endif
`ifdef XBARorM1

    reg  [63:0]                   int_E = 'b0;
    reg  [63:0]                   int_F = 'b0;
    reg  [63:0]                   int_G = 'b0;
    reg  [63:0]                   int_H = 'b0;
    
`endif
`ifdef XBARorM0 

    reg  [63:0]                   int_I = 'b0;
    reg  [63:0]                   int_J = 'b0;
    reg  [63:0]                   int_K = 'b0;
    reg  [63:0]                   int_L = 'b0;
    
`endif
`ifdef XBAR 
    reg  [63:0]                   int_M = 'b0;
    reg  [63:0]                   int_N = 'b0;
    reg  [63:0]                   int_O = 'b0;
    reg  [63:0]                   int_P = 'b0;
    reg  [63:0]                   int_Q   = 'b0;
    reg  [63:0]                   int_R   = 'b0;
    reg  [63:0]                   int_S   = 'b0;
    reg  [63:0]                   int_T   = 'b0;
    reg  [63:0]                   int_U   = 'b0;
    reg  [63:0]                   int_V   = 'b0;
    reg  [63:0]                   int_W   = 'b0;
    reg  [63:0]                   int_X   = 'b0;
    reg  [63:0]                   int_Y   = 'b0;
    reg  [63:0]                   int_Z   = 'b0;
    reg  [63:0]                   int_AA  = 'b0;
    reg  [63:0]                   int_AB  = 'b0;
    reg  [63:0]                   int_AC  = 'b0;
    reg  [63:0]                   int_AD  = 'b0;
    reg  [63:0]                   int_AE  = 'b0;
    reg  [63:0]                   int_AF  = 'b0;
    `endif
//------------------------Instantiation------------------

//------------------------AXI write fsm------------------
assign AWREADY = (wstate == WRIDLE);
assign WREADY  = (wstate == WRDATA);
assign BRESP   = 2'b00;  // OKAY
assign BVALID  = (wstate == WRRESP);
assign wmask   = { {8{WSTRB[3]}}, {8{WSTRB[2]}}, {8{WSTRB[1]}}, {8{WSTRB[0]}} };
assign aw_hs   = AWVALID & AWREADY;
assign w_hs    = WVALID & WREADY;

// wstate
always @(posedge ACLK) begin
    if (ARESET)
        wstate <= WRRESET;
    else if (ACLK_EN)
        wstate <= wnext;
end

// wnext
always @(*) begin
    case (wstate)
        WRIDLE:
            if (AWVALID)
                wnext = WRDATA;
            else
                wnext = WRIDLE;
        WRDATA:
            if (WVALID)
                wnext = WRRESP;
            else
                wnext = WRDATA;
        WRRESP:
            if (BREADY)
                wnext = WRIDLE;
            else
                wnext = WRRESP;
        default:
            wnext = WRIDLE;
    endcase
end

// waddr
always @(posedge ACLK) begin
    if (ACLK_EN) begin
        if (aw_hs)
            waddr <= AWADDR[ADDR_BITS-1:0];
    end
end

//------------------------AXI read fsm-------------------
assign ARREADY = (rstate == RDIDLE);
assign RDATA   = rdata;
assign RRESP   = 2'b00;  // OKAY
assign RVALID  = (rstate == RDDATA);
assign ar_hs   = ARVALID & ARREADY;
assign raddr   = ARADDR[ADDR_BITS-1:0];

// rstate
always @(posedge ACLK) begin
    if (ARESET)
        rstate <= RDRESET;
    else if (ACLK_EN)
        rstate <= rnext;
end

// rnext
always @(*) begin
    case (rstate)
        RDIDLE:
            if (ARVALID)
                rnext = RDDATA;
            else
                rnext = RDIDLE;
        RDDATA:
            if (RREADY & RVALID)
                rnext = RDIDLE;
            else
                rnext = RDDATA;
        default:
            rnext = RDIDLE;
    endcase
end

// rdata
always @(posedge ACLK) begin
    if (ACLK_EN) begin
        if (ar_hs) begin
            rdata <= 1'b0;
            case (raddr)
                ADDR_AP_CTRL: begin
                    rdata[0] <= int_ap_start;
                    rdata[1] <= int_ap_done;
                    rdata[2] <= int_ap_idle;
                    rdata[3] <= int_ap_ready;
                    rdata[7] <= int_auto_restart;
                end
                ADDR_GIE: begin
                    rdata <= int_gie;
                end
                ADDR_IER: begin
                    rdata <= int_ier;
                end
                ADDR_ISR: begin
                    rdata <= int_isr;
                end
                ADDR_SCALAR00_DATA_0: begin
                    rdata <= int_scalar00[31:0];
                end
                ADDR_A_DATA_0: begin
                    rdata <= int_A[31:0];
                end
                ADDR_A_DATA_1: begin
                    rdata <= int_A[63:32];
                end
                `ifdef XBARorM1orM0

                ADDR_B_DATA_0: begin
                    rdata <= int_B[31:0];
                end
                ADDR_B_DATA_1: begin
                    rdata <= int_B[63:32];
                end
                ADDR_C_DATA_0: begin
                    rdata <= int_C[31:0];
                end
                ADDR_C_DATA_1: begin
                    rdata <= int_C[63:32];
                end
                ADDR_D_DATA_0: begin
                    rdata <= int_D[31:0];
                end
                ADDR_D_DATA_1: begin
                    rdata <= int_D[63:32];
                end
                
`endif
`ifdef XBARorM1
                ADDR_E_DATA_0: begin
                    rdata <= int_E[31:0];
                end
                ADDR_E_DATA_1: begin
                    rdata <= int_E[63:32];
                end
                ADDR_F_DATA_0: begin
                    rdata <= int_F[31:0];
                end
                ADDR_F_DATA_1: begin
                    rdata <= int_F[63:32];
                end
                ADDR_G_DATA_0: begin
                    rdata <= int_G[31:0];
                end
                ADDR_G_DATA_1: begin
                    rdata <= int_G[63:32];
                end
                ADDR_H_DATA_0: begin
                    rdata <= int_H[31:0];
                end
                ADDR_H_DATA_1: begin
                    rdata <= int_H[63:32];
                end
             
`endif
`ifdef XBARorM0 
   
                ADDR_I_DATA_0: begin
                    rdata <= int_I[31:0];
                end
                ADDR_I_DATA_1: begin
                    rdata <= int_I[63:32];
                end
                ADDR_J_DATA_0: begin
                    rdata <= int_J[31:0];
                end
                ADDR_J_DATA_1: begin
                    rdata <= int_J[63:32];
                end
                ADDR_K_DATA_0: begin
                    rdata <= int_K[31:0];
                end
                ADDR_K_DATA_1: begin
                    rdata <= int_K[63:32];
                end
                ADDR_L_DATA_0: begin
                    rdata <= int_L[31:0];
                end
                ADDR_L_DATA_1: begin
                    rdata <= int_L[63:32];
                end
       
`endif
`ifdef XBAR          
                ADDR_M_DATA_0: begin
                    rdata <= int_M[31:0];
                end
                ADDR_M_DATA_1: begin
                    rdata <= int_M[63:32];
                end
                ADDR_N_DATA_0: begin
                    rdata <= int_N[31:0];
                end
                ADDR_N_DATA_1: begin
                    rdata <= int_N[63:32];
                end
                ADDR_O_DATA_0: begin
                    rdata <= int_O[31:0];
                end
                ADDR_O_DATA_1: begin
                    rdata <= int_O[63:32];
                end
                ADDR_P_DATA_0: begin
                    rdata <= int_P[31:0];
                end
                ADDR_P_DATA_1: begin
                    rdata <= int_P[63:32];
                end
                
                ADDR_Q_DATA_0: begin
                    rdata <= int_Q[31:0];
                end
                ADDR_Q_DATA_1: begin
                    rdata <= int_Q[63:32];
                end
                ADDR_R_DATA_0: begin
                    rdata <= int_R[31:0];
                end
                ADDR_R_DATA_1: begin
                    rdata <= int_R[63:32];
                end
                ADDR_S_DATA_0: begin
                    rdata <= int_S[31:0];
                end
                ADDR_S_DATA_1: begin
                    rdata <= int_S[63:32];
                end
                ADDR_T_DATA_0: begin
                    rdata <= int_T[31:0];
                end
                ADDR_T_DATA_1: begin
                    rdata <= int_T[63:32];
                end
                ADDR_U_DATA_0: begin
                    rdata <= int_U[31:0];
                end
                ADDR_U_DATA_1: begin
                    rdata <= int_U[63:32];
                end
                ADDR_V_DATA_0: begin
                    rdata <= int_V[31:0];
                end
                ADDR_V_DATA_1: begin
                    rdata <= int_V[63:32];
                end
                ADDR_W_DATA_0: begin
                    rdata <= int_W[31:0];
                end
                ADDR_W_DATA_1: begin
                    rdata <= int_W[63:32];
                end
                ADDR_X_DATA_0: begin
                    rdata <= int_X[31:0];
                end
                ADDR_X_DATA_1: begin
                    rdata <= int_X[63:32];
                end
                ADDR_Y_DATA_0: begin
                    rdata <= int_Y[31:0];
                end
                ADDR_Y_DATA_1: begin
                    rdata <= int_Y[63:32];
                end
                ADDR_Z_DATA_0: begin
                    rdata <= int_Z[31:0];
                end
                ADDR_Z_DATA_1: begin
                    rdata <= int_Z[63:32];
                end
                ADDR_AA_DATA_0: begin
                    rdata <= int_AA[31:0];
                end
                ADDR_AA_DATA_1: begin
                    rdata <= int_AA[63:32];
                end
                ADDR_AB_DATA_0: begin
                    rdata <= int_AB[31:0];
                end
                ADDR_AB_DATA_1: begin
                    rdata <= int_AB[63:32];
                end
                ADDR_AC_DATA_0: begin
                    rdata <= int_AC[31:0];
                end
                ADDR_AC_DATA_1: begin
                    rdata <= int_AC[63:32];
                end
                ADDR_AD_DATA_0: begin
                    rdata <= int_AD[31:0];
                end
                ADDR_AD_DATA_1: begin
                    rdata <= int_AD[63:32];
                end
                ADDR_AE_DATA_0: begin
                    rdata <= int_AE[31:0];
                end
                ADDR_AE_DATA_1: begin
                    rdata <= int_AE[63:32];
                end
                ADDR_AF_DATA_0: begin
                    rdata <= int_AF[31:0];
                end
                ADDR_AF_DATA_1: begin
                    rdata <= int_AF[63:32];
                end
                
                `endif
            endcase
        end
    end
end


//------------------------Register logic-----------------
assign interrupt = int_gie & (|int_isr);
assign ap_start  = int_ap_start;
assign scalar00  = int_scalar00;
assign A         = int_A;
`ifdef XBARorM1orM0

assign B         = int_B;
assign C         = int_C;
assign D         = int_D;

`endif
`ifdef XBARorM1

assign E         = int_E;
assign F         = int_F;
assign G         = int_G;
assign H         = int_H;
`endif
`ifdef XBARorM0 
assign I         = int_I;
assign J         = int_J;
assign K         = int_K;
assign L         = int_L;

`endif
`ifdef XBAR 
assign M         = int_M;
assign N         = int_N;
assign O         = int_O;
assign P         = int_P;
assign Q         = int_Q ;
assign R         = int_R ;
assign S         = int_S ;
assign T         = int_T ;
assign U         = int_U ;
assign V         = int_V ;
assign W         = int_W ;
assign X         = int_X ;
assign Y         = int_Y ;
assign Z         = int_Z ;
assign AA        = int_AA;
assign AB        = int_AB;
assign AC        = int_AC;
assign AD        = int_AD;
assign AE        = int_AE;
assign AF        = int_AF;
`endif
// int_ap_start
always @(posedge ACLK) begin
    if (ARESET)
        int_ap_start <= 1'b0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AP_CTRL && WSTRB[0] && WDATA[0])
            int_ap_start <= 1'b1;
        else if (ap_ready)
            int_ap_start <= int_auto_restart; // clear on handshake/auto restart
    end
end

// int_ap_done
always @(posedge ACLK) begin
    if (ARESET)
        int_ap_done <= 1'b0;
    else if (ACLK_EN) begin
        if (ap_done)
            int_ap_done <= 1'b1;
        else if (ar_hs && raddr == ADDR_AP_CTRL)
            int_ap_done <= 1'b0; // clear on read
    end
end

// int_ap_idle
always @(posedge ACLK) begin
    if (ARESET)
        int_ap_idle <= 1'b0;
    else if (ACLK_EN) begin
            int_ap_idle <= ap_idle;
    end
end

// int_ap_ready
always @(posedge ACLK) begin
    if (ARESET)
        int_ap_ready <= 1'b0;
    else if (ACLK_EN) begin
            int_ap_ready <= ap_ready;
    end
end

// int_auto_restart
always @(posedge ACLK) begin
    if (ARESET)
        int_auto_restart <= 1'b0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AP_CTRL && WSTRB[0])
            int_auto_restart <=  WDATA[7];
    end
end

// int_gie
always @(posedge ACLK) begin
    if (ARESET)
        int_gie <= 1'b0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_GIE && WSTRB[0])
            int_gie <= WDATA[0];
    end
end

// int_ier
always @(posedge ACLK) begin
    if (ARESET)
        int_ier <= 1'b0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_IER && WSTRB[0])
            int_ier <= WDATA[1:0];
    end
end

// int_isr[0]
always @(posedge ACLK) begin
    if (ARESET)
        int_isr[0] <= 1'b0;
    else if (ACLK_EN) begin
        if (int_ier[0] & ap_done)
            int_isr[0] <= 1'b1;
        else if (w_hs && waddr == ADDR_ISR && WSTRB[0])
            int_isr[0] <= int_isr[0] ^ WDATA[0]; // toggle on write
    end
end

// int_isr[1]
always @(posedge ACLK) begin
    if (ARESET)
        int_isr[1] <= 1'b0;
    else if (ACLK_EN) begin
        if (int_ier[1] & ap_ready)
            int_isr[1] <= 1'b1;
        else if (w_hs && waddr == ADDR_ISR && WSTRB[0])
            int_isr[1] <= int_isr[1] ^ WDATA[1]; // toggle on write
    end
end

// int_scalar00[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_scalar00[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_SCALAR00_DATA_0)
            int_scalar00[31:0] <= (WDATA[31:0] & wmask) | (int_scalar00[31:0] & ~wmask);
    end
end

// int_A[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_A[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_A_DATA_0)
            int_A[31:0] <= (WDATA[31:0] & wmask) | (int_A[31:0] & ~wmask);
    end
end

// int_A[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_A[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_A_DATA_1)
            int_A[63:32] <= (WDATA[31:0] & wmask) | (int_A[63:32] & ~wmask);
    end
end
`ifdef XBARorM1orM0

// int_B[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_B[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_B_DATA_0)
            int_B[31:0] <= (WDATA[31:0] & wmask) | (int_B[31:0] & ~wmask);
    end
end

// int_B[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_B[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_B_DATA_1)
            int_B[63:32] <= (WDATA[31:0] & wmask) | (int_B[63:32] & ~wmask);
    end
end

// int_C[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_C[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_C_DATA_0)
            int_C[31:0] <= (WDATA[31:0] & wmask) | (int_C[31:0] & ~wmask);
    end
end

// int_C[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_C[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_C_DATA_1)
            int_C[63:32] <= (WDATA[31:0] & wmask) | (int_C[63:32] & ~wmask);
    end
end

// int_D[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_D[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_D_DATA_0)
            int_D[31:0] <= (WDATA[31:0] & wmask) | (int_D[31:0] & ~wmask);
    end
end

// int_D[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_D[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_D_DATA_1)
            int_D[63:32] <= (WDATA[31:0] & wmask) | (int_D[63:32] & ~wmask);
    end
end

`endif
`ifdef XBARorM1
// int_E[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_E[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_E_DATA_0)
            int_E[31:0] <= (WDATA[31:0] & wmask) | (int_E[31:0] & ~wmask);
    end
end

// int_E[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_E[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_E_DATA_1)
            int_E[63:32] <= (WDATA[31:0] & wmask) | (int_E[63:32] & ~wmask);
    end
end

// int_F[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_F[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_F_DATA_0)
            int_F[31:0] <= (WDATA[31:0] & wmask) | (int_F[31:0] & ~wmask);
    end
end

// int_F[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_F[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_F_DATA_1)
            int_F[63:32] <= (WDATA[31:0] & wmask) | (int_F[63:32] & ~wmask);
    end
end

// int_G[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_G[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_G_DATA_0)
            int_G[31:0] <= (WDATA[31:0] & wmask) | (int_G[31:0] & ~wmask);
    end
end

// int_G[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_G[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_G_DATA_1)
            int_G[63:32] <= (WDATA[31:0] & wmask) | (int_G[63:32] & ~wmask);
    end
end

// int_H[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_H[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_H_DATA_0)
            int_H[31:0] <= (WDATA[31:0] & wmask) | (int_H[31:0] & ~wmask);
    end
end

// int_H[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_H[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_H_DATA_1)
            int_H[63:32] <= (WDATA[31:0] & wmask) | (int_H[63:32] & ~wmask);
    end
end

`endif
`ifdef XBARorM0 

// int_I[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_I[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_I_DATA_0)
            int_I[31:0] <= (WDATA[31:0] & wmask) | (int_I[31:0] & ~wmask);
    end
end

// int_I[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_I[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_I_DATA_1)
            int_I[63:32] <= (WDATA[31:0] & wmask) | (int_I[63:32] & ~wmask);
    end
end

// int_J[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_J[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_J_DATA_0)
            int_J[31:0] <= (WDATA[31:0] & wmask) | (int_J[31:0] & ~wmask);
    end
end

// int_J[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_J[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_J_DATA_1)
            int_J[63:32] <= (WDATA[31:0] & wmask) | (int_J[63:32] & ~wmask);
    end
end

// int_K[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_K[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_K_DATA_0)
            int_K[31:0] <= (WDATA[31:0] & wmask) | (int_K[31:0] & ~wmask);
    end
end

// int_K[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_K[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_K_DATA_1)
            int_K[63:32] <= (WDATA[31:0] & wmask) | (int_K[63:32] & ~wmask);
    end
end

// int_L[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_L[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_L_DATA_0)
            int_L[31:0] <= (WDATA[31:0] & wmask) | (int_L[31:0] & ~wmask);
    end
end

// int_L[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_L[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_L_DATA_1)
            int_L[63:32] <= (WDATA[31:0] & wmask) | (int_L[63:32] & ~wmask);
    end
end

`endif
`ifdef XBAR 

// int_M[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_M[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_M_DATA_0)
            int_M[31:0] <= (WDATA[31:0] & wmask) | (int_M[31:0] & ~wmask);
    end
end

// int_M[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_M[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_M_DATA_1)
            int_M[63:32] <= (WDATA[31:0] & wmask) | (int_M[63:32] & ~wmask);
    end
end

// int_N[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_N[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_N_DATA_0)
            int_N[31:0] <= (WDATA[31:0] & wmask) | (int_N[31:0] & ~wmask);
    end
end

// int_N[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_N[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_N_DATA_1)
            int_N[63:32] <= (WDATA[31:0] & wmask) | (int_N[63:32] & ~wmask);
    end
end

// int_O[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_O[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_O_DATA_0)
            int_O[31:0] <= (WDATA[31:0] & wmask) | (int_O[31:0] & ~wmask);
    end
end

// int_O[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_O[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_O_DATA_1)
            int_O[63:32] <= (WDATA[31:0] & wmask) | (int_O[63:32] & ~wmask);
    end
end

// int_P[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_P[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_P_DATA_0)
            int_P[31:0] <= (WDATA[31:0] & wmask) | (int_P[31:0] & ~wmask);
    end
end

// int_P[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_P[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_P_DATA_1)
            int_P[63:32] <= (WDATA[31:0] & wmask) | (int_P[63:32] & ~wmask);
    end
end



// int_Q[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_Q[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_Q_DATA_0)
            int_Q[31:0] <= (WDATA[31:0] & wmask) | (int_Q[31:0] & ~wmask);
    end
end

// int_Q[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_Q[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_Q_DATA_1)
            int_Q[63:32] <= (WDATA[31:0] & wmask) | (int_Q[63:32] & ~wmask);
    end
end

// int_R[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_R[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_R_DATA_0)
            int_R[31:0] <= (WDATA[31:0] & wmask) | (int_R[31:0] & ~wmask);
    end
end

// int_R[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_R[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_R_DATA_1)
            int_R[63:32] <= (WDATA[31:0] & wmask) | (int_R[63:32] & ~wmask);
    end
end

// int_S[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_S[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_S_DATA_0)
            int_S[31:0] <= (WDATA[31:0] & wmask) | (int_S[31:0] & ~wmask);
    end
end

// int_S[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_S[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_S_DATA_1)
            int_S[63:32] <= (WDATA[31:0] & wmask) | (int_S[63:32] & ~wmask);
    end
end

// int_T[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_T[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_T_DATA_0)
            int_T[31:0] <= (WDATA[31:0] & wmask) | (int_T[31:0] & ~wmask);
    end
end

// int_T[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_T[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_T_DATA_1)
            int_T[63:32] <= (WDATA[31:0] & wmask) | (int_T[63:32] & ~wmask);
    end
end

// int_U[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_U[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_U_DATA_0)
            int_U[31:0] <= (WDATA[31:0] & wmask) | (int_U[31:0] & ~wmask);
    end
end

// int_U[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_U[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_U_DATA_1)
            int_U[63:32] <= (WDATA[31:0] & wmask) | (int_U[63:32] & ~wmask);
    end
end

// int_V[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_V[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_V_DATA_0)
            int_V[31:0] <= (WDATA[31:0] & wmask) | (int_V[31:0] & ~wmask);
    end
end

// int_V[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_V[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_V_DATA_1)
            int_V[63:32] <= (WDATA[31:0] & wmask) | (int_V[63:32] & ~wmask);
    end
end

// int_W[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_W[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_W_DATA_0)
            int_W[31:0] <= (WDATA[31:0] & wmask) | (int_W[31:0] & ~wmask);
    end
end

// int_W[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_W[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_W_DATA_1)
            int_W[63:32] <= (WDATA[31:0] & wmask) | (int_W[63:32] & ~wmask);
    end
end

// int_h[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_X[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_X_DATA_0)
            int_X[31:0] <= (WDATA[31:0] & wmask) | (int_X[31:0] & ~wmask);
    end
end

// int_h[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_X[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_X_DATA_1)
            int_X[63:32] <= (WDATA[31:0] & wmask) | (int_X[63:32] & ~wmask);
    end
end

// int_Y[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_Y[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_Y_DATA_0)
            int_Y[31:0] <= (WDATA[31:0] & wmask) | (int_Y[31:0] & ~wmask);
    end
end

// int_Y[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_Y[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_Y_DATA_1)
            int_Y[63:32] <= (WDATA[31:0] & wmask) | (int_Y[63:32] & ~wmask);
    end
end

// int_Z[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_Z[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_Z_DATA_0)
            int_Z[31:0] <= (WDATA[31:0] & wmask) | (int_Z[31:0] & ~wmask);
    end
end

// int_Z[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_Z[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_Z_DATA_1)
            int_Z[63:32] <= (WDATA[31:0] & wmask) | (int_Z[63:32] & ~wmask);
    end
end

// int_AA[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_AA[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AA_DATA_0)
            int_AA[31:0] <= (WDATA[31:0] & wmask) | (int_AA[31:0] & ~wmask);
    end
end

// int_AA[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_AA[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AA_DATA_1)
            int_AA[63:32] <= (WDATA[31:0] & wmask) | (int_AA[63:32] & ~wmask);
    end
end

// int_AB[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_AB[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AB_DATA_0)
            int_AB[31:0] <= (WDATA[31:0] & wmask) | (int_AB[31:0] & ~wmask);
    end
end

// int_AB[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_AB[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AB_DATA_1)
            int_AB[63:32] <= (WDATA[31:0] & wmask) | (int_AB[63:32] & ~wmask);
    end
end

// int_AC[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_AC[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AC_DATA_0)
            int_AC[31:0] <= (WDATA[31:0] & wmask) | (int_AC[31:0] & ~wmask);
    end
end

// int_AC[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_AC[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AC_DATA_1)
            int_AC[63:32] <= (WDATA[31:0] & wmask) | (int_AC[63:32] & ~wmask);
    end
end

// int_AD[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_AD[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AD_DATA_0)
            int_AD[31:0] <= (WDATA[31:0] & wmask) | (int_AD[31:0] & ~wmask);
    end
end

// int_AD[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_AD[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AD_DATA_1)
            int_AD[63:32] <= (WDATA[31:0] & wmask) | (int_AD[63:32] & ~wmask);
    end
end

// int_AE[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_AE[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AE_DATA_0)
            int_AE[31:0] <= (WDATA[31:0] & wmask) | (int_AE[31:0] & ~wmask);
    end
end

// int_AE[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_AE[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AE_DATA_1)
            int_AE[63:32] <= (WDATA[31:0] & wmask) | (int_AE[63:32] & ~wmask);
    end
end

// int_AF[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_AF[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AF_DATA_0)
            int_AF[31:0] <= (WDATA[31:0] & wmask) | (int_AF[31:0] & ~wmask);
    end
end

// int_AF[63:32]
always @(posedge ACLK) begin
    if (ARESET)
        int_AF[63:32] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_AF_DATA_1)
            int_AF[63:32] <= (WDATA[31:0] & wmask) | (int_AF[63:32] & ~wmask);
    end
end

`endif

//------------------------Memory logic-------------------

endmodule
