`define NONE 	3'b000
`define LEFT 	3'b100
`define RIGHT 	3'b101
`define U0 	3'b110
`define U1 	3'b111

`define Msg_W 261 // valid,last,head,RW,VC,unused,Burst_len,mem_addr,src_x,src_y,dest_x,dest_y,unused
	          //  1,    1,    1,  1, 1, 1,     8,         33,     3,   3,     3,    3,    202
`define v		    [`Msg_W-1] // 260
`define l           [`Msg_W-2] // 259
`define h           [`Msg_W-3] // 258
`define rw          [`Msg_W-4] // 257
`define vc          [`Msg_W-5] // 256 
	          
`define offset_len 202
`define of [201:0]
`define dest_addr A_W+`offset_len-1:`offset_len //207 202
`define src_addr  2*A_W+`offset_len-1:A_W+`offset_len//213 208
`define addr_info 1+1+1+1+8+M_A_W+2*A_W+`offset_len:`offset_len //258 202//H,VC,RW,,UNUSED
//259 l
// 260 V
`define addr_info_len 1+1+1+1+8+M_A_W+2*A_W
`define len 8+M_A_W+2*A_W+`offset_len-1:M_A_W+2*A_W+`offset_len// burst len 254 247
`define mem_addr M_A_W+2*A_W+`offset_len-1:2*A_W+`offset_len //  246 214
`define mem_addr_bft M_A_W+2*A_W+`offset_len-1:2*A_W+`offset_len //  246 214
`define port_bft M_A_W+2*A_W+`offset_len-1:M_A_W+2*A_W+`offset_len-5 //  246 242
`define addr_info2 1+8+M_A_W+2*A_W+`offset_len:`offset_len