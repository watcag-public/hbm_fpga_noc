`include "commands.h" 


/*
    TODO 
    fix Wlast and Len support
    Extend the support to 2*ii+1
    Read and Read Response
*/
`timescale 1ps / 1ps

	 module bft #(
	parameter WRAP		= 1,
	parameter PAT		= 0,
	parameter N		= 32,
	parameter	P_W	= 256,
	parameter A_W	= $clog2(N)+1,
	parameter M_A_W = 33,
	parameter D_W	= 259,//1+1+M_A_W+A_W+P_W,
	parameter LEVELS	= $clog2(N),
	parameter LIMIT		= 1024,
	parameter RATE		= 0,
	parameter SIGMA		= `SIGMA,
	parameter FD		= 32,
	parameter HR            = 1,
	parameter C_M00_AXI_ADDR_WIDTH = 64,
	parameter C_M00_AXI_DATA_WIDTH = 256,
	parameter C_M08_AXI_ADDR_WIDTH = 64,
	parameter C_M08_AXI_DATA_WIDTH = 256,
	parameter C_M09_AXI_ADDR_WIDTH = 64,
	parameter C_M09_AXI_DATA_WIDTH = 256,
	parameter C_M10_AXI_ADDR_WIDTH = 64,
	parameter C_M10_AXI_DATA_WIDTH = 256,
	parameter C_M11_AXI_ADDR_WIDTH = 64,
	parameter C_M11_AXI_DATA_WIDTH = 256,
	parameter C_M12_AXI_ADDR_WIDTH = 64,
	parameter C_M12_AXI_DATA_WIDTH = 256,
	parameter C_M13_AXI_ADDR_WIDTH = 64,
	parameter C_M13_AXI_DATA_WIDTH = 256,
	parameter C_M14_AXI_ADDR_WIDTH = 64,
	parameter C_M14_AXI_DATA_WIDTH = 256,
	parameter C_M15_AXI_ADDR_WIDTH = 64,
	parameter C_M15_AXI_DATA_WIDTH = 256,
	
	parameter  C_M16_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M16_AXI_DATA_WIDTH       = 256,      
    parameter C_M17_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M17_AXI_DATA_WIDTH       = 256,      
    parameter C_M18_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M18_AXI_DATA_WIDTH       = 256,      
    parameter C_M19_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M19_AXI_DATA_WIDTH       = 256,      
    parameter C_M20_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M20_AXI_DATA_WIDTH       = 256,      
    parameter C_M21_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M21_AXI_DATA_WIDTH       = 256,      
    parameter C_M22_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M22_AXI_DATA_WIDTH       = 256,      
    parameter C_M23_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M23_AXI_DATA_WIDTH       = 256,      
    parameter C_M24_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M24_AXI_DATA_WIDTH       = 256,      
    parameter C_M25_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M25_AXI_DATA_WIDTH       = 256,      
    parameter C_M26_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M26_AXI_DATA_WIDTH       = 256,      
    parameter C_M27_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M27_AXI_DATA_WIDTH       = 256,      
    parameter C_M28_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M28_AXI_DATA_WIDTH       = 256,      
    parameter C_M29_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M29_AXI_DATA_WIDTH       = 256,      
    parameter C_M30_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M30_AXI_DATA_WIDTH       = 256,      
    parameter C_M31_AXI_ADDR_WIDTH       = 64 ,      
    parameter C_M31_AXI_DATA_WIDTH       = 256       
	
) (
	

	beginClient
    ,AXI_ACLK
    ,AXI_ARESET_N
    ,cmd
    ,m00_axi_awvalid   
    ,m00_axi_awready   
    ,m00_axi_awaddr    
    ,m00_axi_awlen     
    ,m00_axi_wvalid    
    ,m00_axi_wready    
    ,m00_axi_wdata     
    ,m00_axi_wstrb     
    ,m00_axi_wlast     
    ,m00_axi_bvalid    
    ,m00_axi_bready    
    ,m00_axi_arvalid   
    ,m00_axi_arready   
    ,m00_axi_araddr    
    ,m00_axi_arlen     
    ,m00_axi_rvalid    
    ,m00_axi_rready    
    ,m00_axi_rdata     
    ,m00_axi_rlast   
    
 `ifdef XBARorM1orM0

    ,m01_axi_awvalid 
    ,m01_axi_awready 
    ,m01_axi_awaddr  
    ,m01_axi_awlen   
    ,m01_axi_wvalid  
    ,m01_axi_wready  
    ,m01_axi_wdata   
    ,m01_axi_wstrb   
    ,m01_axi_wlast   
    ,m01_axi_bvalid  
    ,m01_axi_bready  
    ,m01_axi_arvalid 
    ,m01_axi_arready 
    ,m01_axi_araddr  
    ,m01_axi_arlen   
    ,m01_axi_rvalid  
    ,m01_axi_rready  
    ,m01_axi_rdata   
    ,m01_axi_rlast   
        
    ,m02_axi_awvalid 
    ,m02_axi_awready 
    ,m02_axi_awaddr  
    ,m02_axi_awlen   
    ,m02_axi_wvalid  
    ,m02_axi_wready  
    ,m02_axi_wdata   
    ,m02_axi_wstrb   
    ,m02_axi_wlast   
    ,m02_axi_bvalid  
    ,m02_axi_bready  
    ,m02_axi_arvalid 
    ,m02_axi_arready 
    ,m02_axi_araddr  
    ,m02_axi_arlen   
    ,m02_axi_rvalid  
    ,m02_axi_rready  
    ,m02_axi_rdata   
    ,m02_axi_rlast   
        
    ,m03_axi_awvalid 
    ,m03_axi_awready 
    ,m03_axi_awaddr  
    ,m03_axi_awlen   
    ,m03_axi_wvalid  
    ,m03_axi_wready  
    ,m03_axi_wdata   
    ,m03_axi_wstrb   
    ,m03_axi_wlast   
    ,m03_axi_bvalid  
    ,m03_axi_bready  
    ,m03_axi_arvalid 
    ,m03_axi_arready 
    ,m03_axi_araddr  
    ,m03_axi_arlen   
    ,m03_axi_rvalid  
    ,m03_axi_rready  
    ,m03_axi_rdata   
    ,m03_axi_rlast   
`endif
`ifdef XBARorM1
    ,m04_axi_awvalid   
    ,m04_axi_awready   
    ,m04_axi_awaddr    
    ,m04_axi_awlen     
    ,m04_axi_wvalid    
    ,m04_axi_wready    
    ,m04_axi_wdata     
    ,m04_axi_wstrb     
    ,m04_axi_wlast     
    ,m04_axi_bvalid    
    ,m04_axi_bready    
    ,m04_axi_arvalid   
    ,m04_axi_arready   
    ,m04_axi_araddr    
    ,m04_axi_arlen     
    ,m04_axi_rvalid    
    ,m04_axi_rready    
    ,m04_axi_rdata     
    ,m04_axi_rlast   
    
    ,m05_axi_awvalid 
    ,m05_axi_awready 
    ,m05_axi_awaddr  
    ,m05_axi_awlen   
    ,m05_axi_wvalid  
    ,m05_axi_wready  
    ,m05_axi_wdata   
    ,m05_axi_wstrb   
    ,m05_axi_wlast   
    ,m05_axi_bvalid  
    ,m05_axi_bready  
    ,m05_axi_arvalid 
    ,m05_axi_arready 
    ,m05_axi_araddr  
    ,m05_axi_arlen   
    ,m05_axi_rvalid  
    ,m05_axi_rready  
    ,m05_axi_rdata   
    ,m05_axi_rlast   
        
    ,m06_axi_awvalid 
    ,m06_axi_awready 
    ,m06_axi_awaddr  
    ,m06_axi_awlen   
    ,m06_axi_wvalid  
    ,m06_axi_wready  
    ,m06_axi_wdata   
    ,m06_axi_wstrb   
    ,m06_axi_wlast   
    ,m06_axi_bvalid  
    ,m06_axi_bready  
    ,m06_axi_arvalid 
    ,m06_axi_arready 
    ,m06_axi_araddr  
    ,m06_axi_arlen   
    ,m06_axi_rvalid  
    ,m06_axi_rready  
    ,m06_axi_rdata   
    ,m06_axi_rlast   
        
    ,m07_axi_awvalid 
    ,m07_axi_awready 
    ,m07_axi_awaddr  
    ,m07_axi_awlen   
    ,m07_axi_wvalid  
    ,m07_axi_wready  
    ,m07_axi_wdata   
    ,m07_axi_wstrb   
    ,m07_axi_wlast   
    ,m07_axi_bvalid  
    ,m07_axi_bready  
    ,m07_axi_arvalid 
    ,m07_axi_arready 
    ,m07_axi_araddr  
    ,m07_axi_arlen   
    ,m07_axi_rvalid  
    ,m07_axi_rready  
    ,m07_axi_rdata   
    ,m07_axi_rlast   
    
   `endif
   `ifdef XBARorM0
    ,m08_axi_awvalid
    ,m08_axi_awready
    ,m08_axi_awaddr 
    ,m08_axi_awlen  
    ,m08_axi_wvalid 
    ,m08_axi_wready 
    ,m08_axi_wdata  
    ,m08_axi_wstrb  
    ,m08_axi_wlast  
    ,m08_axi_bvalid 
    ,m08_axi_bready 
    ,m08_axi_arvalid
    ,m08_axi_arready
    ,m08_axi_araddr 
    ,m08_axi_arlen  
    ,m08_axi_rvalid 
    ,m08_axi_rready 
    ,m08_axi_rdata  
    ,m08_axi_rlast  
                    
    ,m09_axi_awvalid
    ,m09_axi_awready
    ,m09_axi_awaddr 
    ,m09_axi_awlen  
    ,m09_axi_wvalid 
    ,m09_axi_wready 
    ,m09_axi_wdata  
    ,m09_axi_wstrb  
    ,m09_axi_wlast  
    ,m09_axi_bvalid 
    ,m09_axi_bready 
    ,m09_axi_arvalid
    ,m09_axi_arready
    ,m09_axi_araddr 
    ,m09_axi_arlen  
    ,m09_axi_rvalid 
    ,m09_axi_rready 
    ,m09_axi_rdata  
    ,m09_axi_rlast  
             
           
    ,m10_axi_awvalid
    ,m10_axi_awready
    ,m10_axi_awaddr 
    ,m10_axi_awlen  
    ,m10_axi_wvalid 
    ,m10_axi_wready 
    ,m10_axi_wdata  
    ,m10_axi_wstrb  
    ,m10_axi_wlast  
    ,m10_axi_bvalid 
    ,m10_axi_bready 
    ,m10_axi_arvalid
    ,m10_axi_arready
    ,m10_axi_araddr 
    ,m10_axi_arlen  
    ,m10_axi_rvalid 
    ,m10_axi_rready 
    ,m10_axi_rdata  
    ,m10_axi_rlast  
                 
    ,m11_axi_awvalid
    ,m11_axi_awready
    ,m11_axi_awaddr 
    ,m11_axi_awlen  
    ,m11_axi_wvalid 
    ,m11_axi_wready 
    ,m11_axi_wdata  
    ,m11_axi_wstrb  
    ,m11_axi_wlast  
    ,m11_axi_bvalid 
    ,m11_axi_bready 
    ,m11_axi_arvalid
    ,m11_axi_arready
    ,m11_axi_araddr 
    ,m11_axi_arlen  
    ,m11_axi_rvalid 
    ,m11_axi_rready 
    ,m11_axi_rdata  
    ,m11_axi_rlast  
   `endif
 `ifdef XBAR                
    ,m12_axi_awvalid
    ,m12_axi_awready
    ,m12_axi_awaddr 
    ,m12_axi_awlen  
    ,m12_axi_wvalid 
    ,m12_axi_wready 
    ,m12_axi_wdata  
    ,m12_axi_wstrb  
    ,m12_axi_wlast  
    ,m12_axi_bvalid 
    ,m12_axi_bready 
    ,m12_axi_arvalid
    ,m12_axi_arready
    ,m12_axi_araddr 
    ,m12_axi_arlen  
    ,m12_axi_rvalid 
    ,m12_axi_rready 
    ,m12_axi_rdata  
    ,m12_axi_rlast  
         
    ,m13_axi_awvalid
    ,m13_axi_awready
    ,m13_axi_awaddr 
    ,m13_axi_awlen  
    ,m13_axi_wvalid 
    ,m13_axi_wready 
    ,m13_axi_wdata  
    ,m13_axi_wstrb  
    ,m13_axi_wlast  
    ,m13_axi_bvalid 
    ,m13_axi_bready 
    ,m13_axi_arvalid
    ,m13_axi_arready
    ,m13_axi_araddr 
    ,m13_axi_arlen  
    ,m13_axi_rvalid 
    ,m13_axi_rready 
    ,m13_axi_rdata  
    ,m13_axi_rlast  
                  
    ,m14_axi_awvalid
    ,m14_axi_awready
    ,m14_axi_awaddr 
    ,m14_axi_awlen  
    ,m14_axi_wvalid 
    ,m14_axi_wready 
    ,m14_axi_wdata  
    ,m14_axi_wstrb  
    ,m14_axi_wlast  
    ,m14_axi_bvalid 
    ,m14_axi_bready 
    ,m14_axi_arvalid
    ,m14_axi_arready
    ,m14_axi_araddr 
    ,m14_axi_arlen  
    ,m14_axi_rvalid 
    ,m14_axi_rready 
    ,m14_axi_rdata  
    ,m14_axi_rlast  
               
    ,m15_axi_awvalid
    ,m15_axi_awready
    ,m15_axi_awaddr 
    ,m15_axi_awlen  
    ,m15_axi_wvalid 
    ,m15_axi_wready 
    ,m15_axi_wdata  
    ,m15_axi_wstrb  
    ,m15_axi_wlast  
    ,m15_axi_bvalid 
    ,m15_axi_bready 
    ,m15_axi_arvalid
    ,m15_axi_arready
    ,m15_axi_araddr 
    ,m15_axi_arlen  
    ,m15_axi_rvalid 
    ,m15_axi_rready 
    ,m15_axi_rdata  
    ,m15_axi_rlast  

    ,m16_axi_awvalid
    ,m16_axi_awready  
    ,m16_axi_awaddr   
    ,m16_axi_awlen    
    ,m16_axi_wvalid   
    ,m16_axi_wready   
    ,m16_axi_wdata    
    ,m16_axi_wstrb    
    ,m16_axi_wlast    
    ,m16_axi_bvalid   
    ,m16_axi_bready   
    ,m16_axi_arvalid  
    ,m16_axi_arready  
    ,m16_axi_araddr   
    ,m16_axi_arlen    
    ,m16_axi_rvalid   
    ,m16_axi_rready   
    ,m16_axi_rdata    
    ,m16_axi_rlast    
                     
    ,m17_axi_awvalid  
    ,m17_axi_awready  
    ,m17_axi_awaddr   
    ,m17_axi_awlen    
    ,m17_axi_wvalid   
    ,m17_axi_wready   
    ,m17_axi_wdata    
    ,m17_axi_wstrb    
    ,m17_axi_wlast    
    ,m17_axi_bvalid   
    ,m17_axi_bready   
    ,m17_axi_arvalid  
    ,m17_axi_arready  
    ,m17_axi_araddr   
    ,m17_axi_arlen    
    ,m17_axi_rvalid   
    ,m17_axi_rready   
    ,m17_axi_rdata    
    ,m17_axi_rlast    
                     
    ,m18_axi_awvalid  
    ,m18_axi_awready  
    ,m18_axi_awaddr   
    ,m18_axi_awlen    
    ,m18_axi_wvalid   
    ,m18_axi_wready   
    ,m18_axi_wdata    
    ,m18_axi_wstrb    
    ,m18_axi_wlast    
    ,m18_axi_bvalid   
    ,m18_axi_bready   
    ,m18_axi_arvalid  
    ,m18_axi_arready  
    ,m18_axi_araddr   
    ,m18_axi_arlen    
    ,m18_axi_rvalid   
    ,m18_axi_rready   
    ,m18_axi_rdata    
    ,m18_axi_rlast    
                     
    ,m19_axi_awvalid  
    ,m19_axi_awready  
    ,m19_axi_awaddr   
    ,m19_axi_awlen    
    ,m19_axi_wvalid   
    ,m19_axi_wready   
    ,m19_axi_wdata    
    ,m19_axi_wstrb    
    ,m19_axi_wlast    
    ,m19_axi_bvalid   
    ,m19_axi_bready   
    ,m19_axi_arvalid  
    ,m19_axi_arready  
    ,m19_axi_araddr   
    ,m19_axi_arlen    
    ,m19_axi_rvalid   
    ,m19_axi_rready   
    ,m19_axi_rdata    
    ,m19_axi_rlast    
                    
    ,m20_axi_awvalid  
    ,m20_axi_awready  
    ,m20_axi_awaddr   
    ,m20_axi_awlen    
    ,m20_axi_wvalid   
    ,m20_axi_wready   
    ,m20_axi_wdata    
    ,m20_axi_wstrb    
    ,m20_axi_wlast    
    ,m20_axi_bvalid   
    ,m20_axi_bready   
    ,m20_axi_arvalid  
    ,m20_axi_arready  
    ,m20_axi_araddr   
    ,m20_axi_arlen    
    ,m20_axi_rvalid   
    ,m20_axi_rready   
    ,m20_axi_rdata    
    ,m20_axi_rlast    
                     
    ,m21_axi_awvalid  
    ,m21_axi_awready  
    ,m21_axi_awaddr   
    ,m21_axi_awlen    
    ,m21_axi_wvalid   
    ,m21_axi_wready   
    ,m21_axi_wdata    
    ,m21_axi_wstrb    
    ,m21_axi_wlast    
    ,m21_axi_bvalid   
    ,m21_axi_bready   
    ,m21_axi_arvalid  
    ,m21_axi_arready  
    ,m21_axi_araddr   
    ,m21_axi_arlen    
    ,m21_axi_rvalid   
    ,m21_axi_rready   
    ,m21_axi_rdata    
    ,m21_axi_rlast    
                     
    ,m22_axi_awvalid  
    ,m22_axi_awready  
    ,m22_axi_awaddr   
    ,m22_axi_awlen    
    ,m22_axi_wvalid   
    ,m22_axi_wready   
    ,m22_axi_wdata    
    ,m22_axi_wstrb    
    ,m22_axi_wlast    
    ,m22_axi_bvalid   
    ,m22_axi_bready   
    ,m22_axi_arvalid  
    ,m22_axi_arready  
    ,m22_axi_araddr   
    ,m22_axi_arlen    
    ,m22_axi_rvalid   
    ,m22_axi_rready   
    ,m22_axi_rdata    
    ,m22_axi_rlast    
                    
    ,m23_axi_awvalid  
    ,m23_axi_awready  
    ,m23_axi_awaddr   
    ,m23_axi_awlen    
    ,m23_axi_wvalid   
    ,m23_axi_wready   
    ,m23_axi_wdata    
    ,m23_axi_wstrb    
    ,m23_axi_wlast    
    ,m23_axi_bvalid   
    ,m23_axi_bready   
    ,m23_axi_arvalid  
    ,m23_axi_arready  
    ,m23_axi_araddr   
    ,m23_axi_arlen    
    ,m23_axi_rvalid   
    ,m23_axi_rready   
    ,m23_axi_rdata    
    ,m23_axi_rlast    
            
    ,m24_axi_awvalid  
    ,m24_axi_awready  
    ,m24_axi_awaddr   
    ,m24_axi_awlen    
    ,m24_axi_wvalid   
    ,m24_axi_wready   
    ,m24_axi_wdata    
    ,m24_axi_wstrb    
    ,m24_axi_wlast    
    ,m24_axi_bvalid   
    ,m24_axi_bready   
    ,m24_axi_arvalid  
    ,m24_axi_arready  
    ,m24_axi_araddr   
    ,m24_axi_arlen    
    ,m24_axi_rvalid   
    ,m24_axi_rready   
    ,m24_axi_rdata    
    ,m24_axi_rlast    
                    
    ,m25_axi_awvalid  
    ,m25_axi_awready  
    ,m25_axi_awaddr   
    ,m25_axi_awlen    
    ,m25_axi_wvalid   
    ,m25_axi_wready   
    ,m25_axi_wdata    
    ,m25_axi_wstrb    
    ,m25_axi_wlast    
    ,m25_axi_bvalid   
    ,m25_axi_bready   
    ,m25_axi_arvalid  
    ,m25_axi_arready  
    ,m25_axi_araddr   
    ,m25_axi_arlen    
    ,m25_axi_rvalid   
    ,m25_axi_rready   
    ,m25_axi_rdata    
    ,m25_axi_rlast    
                     
    ,m26_axi_awvalid  
    ,m26_axi_awready  
    ,m26_axi_awaddr   
    ,m26_axi_awlen    
    ,m26_axi_wvalid   
    ,m26_axi_wready   
    ,m26_axi_wdata    
    ,m26_axi_wstrb    
    ,m26_axi_wlast    
    ,m26_axi_bvalid   
    ,m26_axi_bready   
    ,m26_axi_arvalid  
    ,m26_axi_arready  
    ,m26_axi_araddr   
    ,m26_axi_arlen    
    ,m26_axi_rvalid   
    ,m26_axi_rready   
    ,m26_axi_rdata    
    ,m26_axi_rlast    
                 
    ,m27_axi_awvalid  
    ,m27_axi_awready  
    ,m27_axi_awaddr   
    ,m27_axi_awlen    
    ,m27_axi_wvalid   
    ,m27_axi_wready   
    ,m27_axi_wdata    
    ,m27_axi_wstrb    
    ,m27_axi_wlast    
    ,m27_axi_bvalid   
    ,m27_axi_bready   
    ,m27_axi_arvalid  
    ,m27_axi_arready  
    ,m27_axi_araddr   
    ,m27_axi_arlen    
    ,m27_axi_rvalid   
    ,m27_axi_rready   
    ,m27_axi_rdata    
    ,m27_axi_rlast    
                     
    ,m28_axi_awvalid  
    ,m28_axi_awready  
    ,m28_axi_awaddr   
    ,m28_axi_awlen    
    ,m28_axi_wvalid   
    ,m28_axi_wready   
    ,m28_axi_wdata    
    ,m28_axi_wstrb    
    ,m28_axi_wlast    
    ,m28_axi_bvalid   
    ,m28_axi_bready   
    ,m28_axi_arvalid  
    ,m28_axi_arready  
    ,m28_axi_araddr   
    ,m28_axi_arlen    
    ,m28_axi_rvalid   
    ,m28_axi_rready   
    ,m28_axi_rdata    
    ,m28_axi_rlast    
                     
    ,m29_axi_awvalid  
    ,m29_axi_awready  
    ,m29_axi_awaddr   
    ,m29_axi_awlen    
    ,m29_axi_wvalid   
    ,m29_axi_wready   
    ,m29_axi_wdata    
    ,m29_axi_wstrb    
    ,m29_axi_wlast    
    ,m29_axi_bvalid   
    ,m29_axi_bready   
    ,m29_axi_arvalid  
    ,m29_axi_arready  
    ,m29_axi_araddr   
    ,m29_axi_arlen    
    ,m29_axi_rvalid   
    ,m29_axi_rready   
    ,m29_axi_rdata    
    ,m29_axi_rlast    
                     
    ,m30_axi_awvalid  
    ,m30_axi_awready  
    ,m30_axi_awaddr   
    ,m30_axi_awlen    
    ,m30_axi_wvalid   
    ,m30_axi_wready   
    ,m30_axi_wdata    
    ,m30_axi_wstrb    
    ,m30_axi_wlast    
    ,m30_axi_bvalid   
    ,m30_axi_bready   
    ,m30_axi_arvalid  
    ,m30_axi_arready  
    ,m30_axi_araddr   
    ,m30_axi_arlen    
    ,m30_axi_rvalid   
    ,m30_axi_rready   
    ,m30_axi_rdata    
    ,m30_axi_rlast    
                     
    ,m31_axi_awvalid  
    ,m31_axi_awready  
    ,m31_axi_awaddr   
    ,m31_axi_awlen    
    ,m31_axi_wvalid   
    ,m31_axi_wready   
    ,m31_axi_wdata    
    ,m31_axi_wstrb    
    ,m31_axi_wlast    
    ,m31_axi_bvalid   
    ,m31_axi_bready   
    ,m31_axi_arvalid  
    ,m31_axi_arready  
    ,m31_axi_araddr   
    ,m31_axi_arlen    
    ,m31_axi_rvalid   
    ,m31_axi_rready   
    ,m31_axi_rdata    
    ,m31_axi_rlast    
    
    `endif
    
    ,done
    ,A
);

	input  wire                 beginClient           ;         
    input wire                  AXI_ACLK              ;
    (*KEEP = "true"*)input wire                  AXI_ARESET_N          ;
    output wire                 done                  ;	  
    
    input wire `Cmd cmd;
//    output wire  [A_W+D_W-1:0]  test                  ;
    

  output wire                                    m00_axi_awvalid   ;
  input  wire                                    m00_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m00_axi_awaddr    ;
  output wire [8-1:0]                            m00_axi_awlen     ;
  output wire                                    m00_axi_wvalid    ;
  input  wire                                    m00_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m00_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m00_axi_wstrb     ;
  output wire                                    m00_axi_wlast     ;
  input  wire                                    m00_axi_bvalid    ;
  output wire                                    m00_axi_bready    ;
  output wire                                    m00_axi_arvalid   ;
  input  wire                                    m00_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m00_axi_araddr    ;
  output wire [8-1:0]                            m00_axi_arlen     ;
  input  wire                                    m00_axi_rvalid    ;
  output wire                                    m00_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m00_axi_rdata     ;
  input  wire                                    m00_axi_rlast     ;
  
  `ifdef XBARorM1orM0
  output wire                                    m01_axi_awvalid   ;
  input  wire                                    m01_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m01_axi_awaddr    ;
  output wire [8-1:0]                            m01_axi_awlen     ;
  output wire                                    m01_axi_wvalid    ;
  input  wire                                    m01_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m01_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m01_axi_wstrb     ;
  output wire                                    m01_axi_wlast     ;
  input  wire                                    m01_axi_bvalid    ;
  output wire                                    m01_axi_bready    ;
  output wire                                    m01_axi_arvalid   ;
  input  wire                                    m01_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m01_axi_araddr    ;
  output wire [8-1:0]                            m01_axi_arlen     ;
  input  wire                                    m01_axi_rvalid    ;
  output wire                                    m01_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m01_axi_rdata     ;
  input  wire                                    m01_axi_rlast     ;
  
  output wire                                    m02_axi_awvalid   ;
  input  wire                                    m02_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m02_axi_awaddr    ;
  output wire [8-1:0]                            m02_axi_awlen     ;
  output wire                                    m02_axi_wvalid    ;
  input  wire                                    m02_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m02_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m02_axi_wstrb     ;
  output wire                                    m02_axi_wlast     ;
  input  wire                                    m02_axi_bvalid    ;
  output wire                                    m02_axi_bready    ;
  output wire                                    m02_axi_arvalid   ;
  input  wire                                    m02_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m02_axi_araddr    ;
  output wire [8-1:0]                            m02_axi_arlen     ;
  input  wire                                    m02_axi_rvalid    ;
  output wire                                    m02_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m02_axi_rdata     ;
  input  wire                                    m02_axi_rlast     ;
  
  output wire                                    m03_axi_awvalid   ;
  input  wire                                    m03_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m03_axi_awaddr    ;
  output wire [8-1:0]                            m03_axi_awlen     ;
  output wire                                    m03_axi_wvalid    ;
  input  wire                                    m03_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m03_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m03_axi_wstrb     ;
  output wire                                    m03_axi_wlast     ;
  input  wire                                    m03_axi_bvalid    ;
  output wire                                    m03_axi_bready    ;
  output wire                                    m03_axi_arvalid   ;
  input  wire                                    m03_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m03_axi_araddr    ;
  output wire [8-1:0]                            m03_axi_arlen     ;
  input  wire                                    m03_axi_rvalid    ;
  output wire                                    m03_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m03_axi_rdata     ;
  input  wire                                    m03_axi_rlast     ;
  
  `endif
`ifdef XBARorM1
  output wire                                    m04_axi_awvalid   ;
  input  wire                                    m04_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m04_axi_awaddr    ;
  output wire [8-1:0]                            m04_axi_awlen     ;
  output wire                                    m04_axi_wvalid    ;
  input  wire                                    m04_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m04_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m04_axi_wstrb     ;
  output wire                                    m04_axi_wlast     ;
  input  wire                                    m04_axi_bvalid    ;
  output wire                                    m04_axi_bready    ;
  output wire                                    m04_axi_arvalid   ;
  input  wire                                    m04_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m04_axi_araddr    ;
  output wire [8-1:0]                            m04_axi_arlen     ;
  input  wire                                    m04_axi_rvalid    ;
  output wire                                    m04_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m04_axi_rdata     ;
  input  wire                                    m04_axi_rlast     ;
  
  output wire                                    m05_axi_awvalid   ;
  input  wire                                    m05_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m05_axi_awaddr    ;
  output wire [8-1:0]                            m05_axi_awlen     ;
  output wire                                    m05_axi_wvalid    ;
  input  wire                                    m05_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m05_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m05_axi_wstrb     ;
  output wire                                    m05_axi_wlast     ;
  input  wire                                    m05_axi_bvalid    ;
  output wire                                    m05_axi_bready    ;
  output wire                                    m05_axi_arvalid   ;
  input  wire                                    m05_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m05_axi_araddr    ;
  output wire [8-1:0]                            m05_axi_arlen     ;
  input  wire                                    m05_axi_rvalid    ;
  output wire                                    m05_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m05_axi_rdata     ;
  input  wire                                    m05_axi_rlast     ;
  
  output wire                                    m06_axi_awvalid   ;
  input  wire                                    m06_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m06_axi_awaddr    ;
  output wire [8-1:0]                            m06_axi_awlen     ;
  output wire                                    m06_axi_wvalid    ;
  input  wire                                    m06_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m06_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m06_axi_wstrb     ;
  output wire                                    m06_axi_wlast     ;
  input  wire                                    m06_axi_bvalid    ;
  output wire                                    m06_axi_bready    ;
  output wire                                    m06_axi_arvalid   ;
  input  wire                                    m06_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m06_axi_araddr    ;
  output wire [8-1:0]                            m06_axi_arlen     ;
  input  wire                                    m06_axi_rvalid    ;
  output wire                                    m06_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m06_axi_rdata     ;
  input  wire                                    m06_axi_rlast     ;
  
  output wire                                    m07_axi_awvalid   ;
  input  wire                                    m07_axi_awready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m07_axi_awaddr    ;
  output wire [8-1:0]                            m07_axi_awlen     ;
  output wire                                    m07_axi_wvalid    ;
  input  wire                                    m07_axi_wready    ;
  output wire [C_M00_AXI_DATA_WIDTH-1:0]         m07_axi_wdata     ;
  output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m07_axi_wstrb     ;
  output wire                                    m07_axi_wlast     ;
  input  wire                                    m07_axi_bvalid    ;
  output wire                                    m07_axi_bready    ;
  output wire                                    m07_axi_arvalid   ;
  input  wire                                    m07_axi_arready   ;
  output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m07_axi_araddr    ;
  output wire [8-1:0]                            m07_axi_arlen     ;
  input  wire                                    m07_axi_rvalid    ;
  output wire                                    m07_axi_rready    ;
  input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m07_axi_rdata     ;
  input  wire                                    m07_axi_rlast     ;
   `endif
   `ifdef XBARorM0
  output wire                                    m08_axi_awvalid;
  input  wire                                    m08_axi_awready;
  output wire [C_M08_AXI_ADDR_WIDTH-1:0]         m08_axi_awaddr ;
  output wire [8-1:0]                            m08_axi_awlen  ;
  output wire                                    m08_axi_wvalid ;
  input  wire                                    m08_axi_wready ;
  output wire [C_M08_AXI_DATA_WIDTH-1:0]         m08_axi_wdata  ;
  output wire [C_M08_AXI_DATA_WIDTH/8-1:0]       m08_axi_wstrb  ;
  output wire                                    m08_axi_wlast  ;
  input  wire                                    m08_axi_bvalid ;
  output wire                                    m08_axi_bready ;
  output wire                                    m08_axi_arvalid;
  input  wire                                    m08_axi_arready;
  output wire [C_M08_AXI_ADDR_WIDTH-1:0]         m08_axi_araddr ;
  output wire [8-1:0]                            m08_axi_arlen  ;
  input  wire                                    m08_axi_rvalid ;
  output wire                                    m08_axi_rready ;
  input  wire [C_M08_AXI_DATA_WIDTH-1:0]         m08_axi_rdata  ;
  input  wire                                    m08_axi_rlast  ;
  // AXI4 master interface m09_axi                              ;
  output wire                                    m09_axi_awvalid;
  input  wire                                    m09_axi_awready;
  output wire [C_M09_AXI_ADDR_WIDTH-1:0]         m09_axi_awaddr ;
  output wire [8-1:0]                            m09_axi_awlen  ;
  output wire                                    m09_axi_wvalid ;
  input  wire                                    m09_axi_wready ;
  output wire [C_M09_AXI_DATA_WIDTH-1:0]         m09_axi_wdata  ;
  output wire [C_M09_AXI_DATA_WIDTH/8-1:0]       m09_axi_wstrb  ;
  output wire                                    m09_axi_wlast  ;
  input  wire                                    m09_axi_bvalid ;
  output wire                                    m09_axi_bready ;
  output wire                                    m09_axi_arvalid;
  input  wire                                    m09_axi_arready;
  output wire [C_M09_AXI_ADDR_WIDTH-1:0]         m09_axi_araddr ;
  output wire [8-1:0]                            m09_axi_arlen  ;
  input  wire                                    m09_axi_rvalid ;
  output wire                                    m09_axi_rready ;
  input  wire [C_M09_AXI_DATA_WIDTH-1:0]         m09_axi_rdata  ;
  input  wire                                    m09_axi_rlast  ;
  // AXI4 master interface m10_axi                              ;
  output wire                                    m10_axi_awvalid;
  input  wire                                    m10_axi_awready;
  output wire [C_M10_AXI_ADDR_WIDTH-1:0]         m10_axi_awaddr ;
  output wire [8-1:0]                            m10_axi_awlen  ;
  output wire                                    m10_axi_wvalid ;
  input  wire                                    m10_axi_wready ;
  output wire [C_M10_AXI_DATA_WIDTH-1:0]         m10_axi_wdata  ;
  output wire [C_M10_AXI_DATA_WIDTH/8-1:0]       m10_axi_wstrb  ;
  output wire                                    m10_axi_wlast  ;
  input  wire                                    m10_axi_bvalid ;
  output wire                                    m10_axi_bready ;
  output wire                                    m10_axi_arvalid;
  input  wire                                    m10_axi_arready;
  output wire [C_M10_AXI_ADDR_WIDTH-1:0]         m10_axi_araddr ;
  output wire [8-1:0]                            m10_axi_arlen  ;
  input  wire                                    m10_axi_rvalid ;
  output wire                                    m10_axi_rready ;
  input  wire [C_M10_AXI_DATA_WIDTH-1:0]         m10_axi_rdata  ;
  input  wire                                    m10_axi_rlast  ;
  // AXI4 master interface m11_axi                              ;
  output wire                                    m11_axi_awvalid;
  input  wire                                    m11_axi_awready;
  output wire [C_M11_AXI_ADDR_WIDTH-1:0]         m11_axi_awaddr ;
  output wire [8-1:0]                            m11_axi_awlen  ;
  output wire                                    m11_axi_wvalid ;
  input  wire                                    m11_axi_wready ;
  output wire [C_M11_AXI_DATA_WIDTH-1:0]         m11_axi_wdata  ;
  output wire [C_M11_AXI_DATA_WIDTH/8-1:0]       m11_axi_wstrb  ;
  output wire                                    m11_axi_wlast  ;
  input  wire                                    m11_axi_bvalid ;
  output wire                                    m11_axi_bready ;
  output wire                                    m11_axi_arvalid;
  input  wire                                    m11_axi_arready;
  output wire [C_M11_AXI_ADDR_WIDTH-1:0]         m11_axi_araddr ;
  output wire [8-1:0]                            m11_axi_arlen  ;
  input  wire                                    m11_axi_rvalid ;
  output wire                                    m11_axi_rready ;
  input  wire [C_M11_AXI_DATA_WIDTH-1:0]         m11_axi_rdata  ;
  input  wire                                    m11_axi_rlast  ;
     `endif
 `ifdef XBAR
  // AXI4 master interface m12_axi                              ;
  output wire                                    m12_axi_awvalid;
  input  wire                                    m12_axi_awready;
  output wire [C_M12_AXI_ADDR_WIDTH-1:0]         m12_axi_awaddr ;
  output wire [8-1:0]                            m12_axi_awlen  ;
  output wire                                    m12_axi_wvalid ;
  input  wire                                    m12_axi_wready ;
  output wire [C_M12_AXI_DATA_WIDTH-1:0]         m12_axi_wdata  ;
  output wire [C_M12_AXI_DATA_WIDTH/8-1:0]       m12_axi_wstrb  ;
  output wire                                    m12_axi_wlast  ;
  input  wire                                    m12_axi_bvalid ;
  output wire                                    m12_axi_bready ;
  output wire                                    m12_axi_arvalid;
  input  wire                                    m12_axi_arready;
  output wire [C_M12_AXI_ADDR_WIDTH-1:0]         m12_axi_araddr ;
  output wire [8-1:0]                            m12_axi_arlen  ;
  input  wire                                    m12_axi_rvalid ;
  output wire                                    m12_axi_rready ;
  input  wire [C_M12_AXI_DATA_WIDTH-1:0]         m12_axi_rdata  ;
  input  wire                                    m12_axi_rlast  ;
  // AXI4 master interface m13_axi                              ;
  output wire                                    m13_axi_awvalid;
  input  wire                                    m13_axi_awready;
  output wire [C_M13_AXI_ADDR_WIDTH-1:0]         m13_axi_awaddr ;
  output wire [8-1:0]                            m13_axi_awlen  ;
  output wire                                    m13_axi_wvalid ;
  input  wire                                    m13_axi_wready ;
  output wire [C_M13_AXI_DATA_WIDTH-1:0]         m13_axi_wdata  ;
  output wire [C_M13_AXI_DATA_WIDTH/8-1:0]       m13_axi_wstrb  ;
  output wire                                    m13_axi_wlast  ;
  input  wire                                    m13_axi_bvalid ;
  output wire                                    m13_axi_bready ;
  output wire                                    m13_axi_arvalid;
  input  wire                                    m13_axi_arready;
  output wire [C_M13_AXI_ADDR_WIDTH-1:0]         m13_axi_araddr ;
  output wire [8-1:0]                            m13_axi_arlen  ;
  input  wire                                    m13_axi_rvalid ;
  output wire                                    m13_axi_rready ;
  input  wire [C_M13_AXI_DATA_WIDTH-1:0]         m13_axi_rdata  ;
  input  wire                                    m13_axi_rlast  ;
  // AXI4 master interface m14_axi                              ;
  output wire                                    m14_axi_awvalid;
  input  wire                                    m14_axi_awready;
  output wire [C_M14_AXI_ADDR_WIDTH-1:0]         m14_axi_awaddr ;
  output wire [8-1:0]                            m14_axi_awlen  ;
  output wire                                    m14_axi_wvalid ;
  input  wire                                    m14_axi_wready ;
  output wire [C_M14_AXI_DATA_WIDTH-1:0]         m14_axi_wdata  ;
  output wire [C_M14_AXI_DATA_WIDTH/8-1:0]       m14_axi_wstrb  ;
  output wire                                    m14_axi_wlast  ;
  input  wire                                    m14_axi_bvalid ;
  output wire                                    m14_axi_bready ;
  output wire                                    m14_axi_arvalid;
  input  wire                                    m14_axi_arready;
  output wire [C_M14_AXI_ADDR_WIDTH-1:0]         m14_axi_araddr ;
  output wire [8-1:0]                            m14_axi_arlen  ;
  input  wire                                    m14_axi_rvalid ;
  output wire                                    m14_axi_rready ;
  input  wire [C_M14_AXI_DATA_WIDTH-1:0]         m14_axi_rdata  ;
  input  wire                                    m14_axi_rlast  ;
  // AXI4 master interface m15_axi                              ;
  output wire                                    m15_axi_awvalid;
  input  wire                                    m15_axi_awready;
  output wire [C_M15_AXI_ADDR_WIDTH-1:0]         m15_axi_awaddr ;
  output wire [8-1:0]                            m15_axi_awlen  ;
  output wire                                    m15_axi_wvalid ;
  input  wire                                    m15_axi_wready ;
  output wire [C_M15_AXI_DATA_WIDTH-1:0]         m15_axi_wdata  ;
  output wire [C_M15_AXI_DATA_WIDTH/8-1:0]       m15_axi_wstrb  ;
  output wire                                    m15_axi_wlast  ;
  input  wire                                    m15_axi_bvalid ;
  output wire                                    m15_axi_bready ;
  output wire                                    m15_axi_arvalid;
  input  wire                                    m15_axi_arready;
  output wire [C_M15_AXI_ADDR_WIDTH-1:0]         m15_axi_araddr ;
  output wire [8-1:0]                            m15_axi_arlen  ;
  input  wire                                    m15_axi_rvalid ;
  output wire                                    m15_axi_rready ;
  input  wire [C_M15_AXI_DATA_WIDTH-1:0]         m15_axi_rdata  ;
  input  wire                                    m15_axi_rlast  ;
//  `endif
// `ifdef XBARorM1  
  output wire                                    m16_axi_awvalid      ;
  input  wire                                    m16_axi_awready      ;
  output wire [C_M16_AXI_ADDR_WIDTH-1:0]         m16_axi_awaddr       ;
  output wire [8-1:0]                            m16_axi_awlen        ;
  output wire                                    m16_axi_wvalid       ;
  input  wire                                    m16_axi_wready       ;
  output wire [C_M16_AXI_DATA_WIDTH-1:0]         m16_axi_wdata        ;
  output wire [C_M16_AXI_DATA_WIDTH/8-1:0]       m16_axi_wstrb        ;
  output wire                                    m16_axi_wlast        ;
  input  wire                                    m16_axi_bvalid       ;
  output wire                                    m16_axi_bready       ;
  output wire                                    m16_axi_arvalid      ;
  input  wire                                    m16_axi_arready      ;
  output wire [C_M16_AXI_ADDR_WIDTH-1:0]         m16_axi_araddr       ;
  output wire [8-1:0]                            m16_axi_arlen        ;
  input  wire                                    m16_axi_rvalid       ;
  output wire                                    m16_axi_rready       ;
  input  wire [C_M16_AXI_DATA_WIDTH-1:0]         m16_axi_rdata        ;
  input  wire                                    m16_axi_rlast        ;
  // AXI4 master interface m17_axi                                    ;
  output wire                                    m17_axi_awvalid      ;
  input  wire                                    m17_axi_awready      ;
  output wire [C_M17_AXI_ADDR_WIDTH-1:0]         m17_axi_awaddr       ;
  output wire [8-1:0]                            m17_axi_awlen        ;
  output wire                                    m17_axi_wvalid       ;
  input  wire                                    m17_axi_wready       ;
  output wire [C_M17_AXI_DATA_WIDTH-1:0]         m17_axi_wdata        ;
  output wire [C_M17_AXI_DATA_WIDTH/8-1:0]       m17_axi_wstrb        ;
  output wire                                    m17_axi_wlast        ;
  input  wire                                    m17_axi_bvalid       ;
  output wire                                    m17_axi_bready       ;
  output wire                                    m17_axi_arvalid      ;
  input  wire                                    m17_axi_arready      ;
  output wire [C_M17_AXI_ADDR_WIDTH-1:0]         m17_axi_araddr       ;
  output wire [8-1:0]                            m17_axi_arlen        ;
  input  wire                                    m17_axi_rvalid       ;
  output wire                                    m17_axi_rready       ;
  input  wire [C_M17_AXI_DATA_WIDTH-1:0]         m17_axi_rdata        ;
  input  wire                                    m17_axi_rlast        ;
  // AXI4 master interface m18_axi                                    ;
  output wire                                    m18_axi_awvalid      ;
  input  wire                                    m18_axi_awready      ;
  output wire [C_M18_AXI_ADDR_WIDTH-1:0]         m18_axi_awaddr       ;
  output wire [8-1:0]                            m18_axi_awlen        ;
  output wire                                    m18_axi_wvalid       ;
  input  wire                                    m18_axi_wready       ;
  output wire [C_M18_AXI_DATA_WIDTH-1:0]         m18_axi_wdata        ;
  output wire [C_M18_AXI_DATA_WIDTH/8-1:0]       m18_axi_wstrb        ;
  output wire                                    m18_axi_wlast        ;
  input  wire                                    m18_axi_bvalid       ;
  output wire                                    m18_axi_bready       ;
  output wire                                    m18_axi_arvalid      ;
  input  wire                                    m18_axi_arready      ;
  output wire [C_M18_AXI_ADDR_WIDTH-1:0]         m18_axi_araddr       ;
  output wire [8-1:0]                            m18_axi_arlen        ;
  input  wire                                    m18_axi_rvalid       ;
  output wire                                    m18_axi_rready       ;
  input  wire [C_M18_AXI_DATA_WIDTH-1:0]         m18_axi_rdata        ;
  input  wire                                    m18_axi_rlast        ;
  // AXI4 master interface m19_axi                                    ;
  output wire                                    m19_axi_awvalid      ;
  input  wire                                    m19_axi_awready      ;
  output wire [C_M19_AXI_ADDR_WIDTH-1:0]         m19_axi_awaddr       ;
  output wire [8-1:0]                            m19_axi_awlen        ;
  output wire                                    m19_axi_wvalid       ;
  input  wire                                    m19_axi_wready       ;
  output wire [C_M19_AXI_DATA_WIDTH-1:0]         m19_axi_wdata        ;
  output wire [C_M19_AXI_DATA_WIDTH/8-1:0]       m19_axi_wstrb        ;
  output wire                                    m19_axi_wlast        ;
  input  wire                                    m19_axi_bvalid       ;
  output wire                                    m19_axi_bready       ;
  output wire                                    m19_axi_arvalid      ;
  input  wire                                    m19_axi_arready      ;
  output wire [C_M19_AXI_ADDR_WIDTH-1:0]         m19_axi_araddr       ;
  output wire [8-1:0]                            m19_axi_arlen        ;
  input  wire                                    m19_axi_rvalid       ;
  output wire                                    m19_axi_rready       ;
  input  wire [C_M19_AXI_DATA_WIDTH-1:0]         m19_axi_rdata        ;
  input  wire                                    m19_axi_rlast        ;
  // AXI4 master interface m20_axi                                    ;
  output wire                                    m20_axi_awvalid      ;
  input  wire                                    m20_axi_awready      ;
  output wire [C_M20_AXI_ADDR_WIDTH-1:0]         m20_axi_awaddr       ;
  output wire [8-1:0]                            m20_axi_awlen        ;
  output wire                                    m20_axi_wvalid       ;
  input  wire                                    m20_axi_wready       ;
  output wire [C_M20_AXI_DATA_WIDTH-1:0]         m20_axi_wdata        ;
  output wire [C_M20_AXI_DATA_WIDTH/8-1:0]       m20_axi_wstrb        ;
  output wire                                    m20_axi_wlast        ;
  input  wire                                    m20_axi_bvalid       ;
  output wire                                    m20_axi_bready       ;
  output wire                                    m20_axi_arvalid      ;
  input  wire                                    m20_axi_arready      ;
  output wire [C_M20_AXI_ADDR_WIDTH-1:0]         m20_axi_araddr       ;
  output wire [8-1:0]                            m20_axi_arlen        ;
  input  wire                                    m20_axi_rvalid       ;
  output wire                                    m20_axi_rready       ;
  input  wire [C_M20_AXI_DATA_WIDTH-1:0]         m20_axi_rdata        ;
  input  wire                                    m20_axi_rlast        ;
  // AXI4 master interface m21_axi                                    ;
  output wire                                    m21_axi_awvalid      ;
  input  wire                                    m21_axi_awready      ;
  output wire [C_M21_AXI_ADDR_WIDTH-1:0]         m21_axi_awaddr       ;
  output wire [8-1:0]                            m21_axi_awlen        ;
  output wire                                    m21_axi_wvalid       ;
  input  wire                                    m21_axi_wready       ;
  output wire [C_M21_AXI_DATA_WIDTH-1:0]         m21_axi_wdata        ;
  output wire [C_M21_AXI_DATA_WIDTH/8-1:0]       m21_axi_wstrb        ;
  output wire                                    m21_axi_wlast        ;
  input  wire                                    m21_axi_bvalid       ;
  output wire                                    m21_axi_bready       ;
  output wire                                    m21_axi_arvalid      ;
  input  wire                                    m21_axi_arready      ;
  output wire [C_M21_AXI_ADDR_WIDTH-1:0]         m21_axi_araddr       ;
  output wire [8-1:0]                            m21_axi_arlen        ;
  input  wire                                    m21_axi_rvalid       ;
  output wire                                    m21_axi_rready       ;
  input  wire [C_M21_AXI_DATA_WIDTH-1:0]         m21_axi_rdata        ;
  input  wire                                    m21_axi_rlast        ;
  // AXI4 master interface m22_axi                                    ;
  output wire                                    m22_axi_awvalid      ;
  input  wire                                    m22_axi_awready      ;
  output wire [C_M22_AXI_ADDR_WIDTH-1:0]         m22_axi_awaddr       ;
  output wire [8-1:0]                            m22_axi_awlen        ;
  output wire                                    m22_axi_wvalid       ;
  input  wire                                    m22_axi_wready       ;
  output wire [C_M22_AXI_DATA_WIDTH-1:0]         m22_axi_wdata        ;
  output wire [C_M22_AXI_DATA_WIDTH/8-1:0]       m22_axi_wstrb        ;
  output wire                                    m22_axi_wlast        ;
  input  wire                                    m22_axi_bvalid       ;
  output wire                                    m22_axi_bready       ;
  output wire                                    m22_axi_arvalid      ;
  input  wire                                    m22_axi_arready      ;
  output wire [C_M22_AXI_ADDR_WIDTH-1:0]         m22_axi_araddr       ;
  output wire [8-1:0]                            m22_axi_arlen        ;
  input  wire                                    m22_axi_rvalid       ;
  output wire                                    m22_axi_rready       ;
  input  wire [C_M22_AXI_DATA_WIDTH-1:0]         m22_axi_rdata        ;
  input  wire                                    m22_axi_rlast        ;
  // AXI4 master interface m23_axi                                    ;
  output wire                                    m23_axi_awvalid      ;
  input  wire                                    m23_axi_awready      ;
  output wire [C_M23_AXI_ADDR_WIDTH-1:0]         m23_axi_awaddr       ;
  output wire [8-1:0]                            m23_axi_awlen        ;
  output wire                                    m23_axi_wvalid       ;
  input  wire                                    m23_axi_wready       ;
  output wire [C_M23_AXI_DATA_WIDTH-1:0]         m23_axi_wdata        ;
  output wire [C_M23_AXI_DATA_WIDTH/8-1:0]       m23_axi_wstrb        ;
  output wire                                    m23_axi_wlast        ;
  input  wire                                    m23_axi_bvalid       ;
  output wire                                    m23_axi_bready       ;
  output wire                                    m23_axi_arvalid      ;
  input  wire                                    m23_axi_arready      ;
  output wire [C_M23_AXI_ADDR_WIDTH-1:0]         m23_axi_araddr       ;
  output wire [8-1:0]                            m23_axi_arlen        ;
  input  wire                                    m23_axi_rvalid       ;
  output wire                                    m23_axi_rready       ;
  input  wire [C_M23_AXI_DATA_WIDTH-1:0]         m23_axi_rdata        ;
  input  wire                                    m23_axi_rlast        ;
  // AXI4 master interface m24_axi                                    ;
//   `endif
   
//   `ifdef XBAR     
  output wire                                    m24_axi_awvalid      ;
  input  wire                                    m24_axi_awready      ;
  output wire [C_M24_AXI_ADDR_WIDTH-1:0]         m24_axi_awaddr       ;
  output wire [8-1:0]                            m24_axi_awlen        ;
  output wire                                    m24_axi_wvalid       ;
  input  wire                                    m24_axi_wready       ;
  output wire [C_M24_AXI_DATA_WIDTH-1:0]         m24_axi_wdata        ;
  output wire [C_M24_AXI_DATA_WIDTH/8-1:0]       m24_axi_wstrb        ;
  output wire                                    m24_axi_wlast        ;
  input  wire                                    m24_axi_bvalid       ;
  output wire                                    m24_axi_bready       ;
  output wire                                    m24_axi_arvalid      ;
  input  wire                                    m24_axi_arready      ;
  output wire [C_M24_AXI_ADDR_WIDTH-1:0]         m24_axi_araddr       ;
  output wire [8-1:0]                            m24_axi_arlen        ;
  input  wire                                    m24_axi_rvalid       ;
  output wire                                    m24_axi_rready       ;
  input  wire [C_M24_AXI_DATA_WIDTH-1:0]         m24_axi_rdata        ;
  input  wire                                    m24_axi_rlast        ;
  // AXI4 master interface m25_axi                                    ;
  output wire                                    m25_axi_awvalid      ;
  input  wire                                    m25_axi_awready      ;
  output wire [C_M25_AXI_ADDR_WIDTH-1:0]         m25_axi_awaddr       ;
  output wire [8-1:0]                            m25_axi_awlen        ;
  output wire                                    m25_axi_wvalid       ;
  input  wire                                    m25_axi_wready       ;
  output wire [C_M25_AXI_DATA_WIDTH-1:0]         m25_axi_wdata        ;
  output wire [C_M25_AXI_DATA_WIDTH/8-1:0]       m25_axi_wstrb        ;
  output wire                                    m25_axi_wlast        ;
  input  wire                                    m25_axi_bvalid       ;
  output wire                                    m25_axi_bready       ;
  output wire                                    m25_axi_arvalid      ;
  input  wire                                    m25_axi_arready      ;
  output wire [C_M25_AXI_ADDR_WIDTH-1:0]         m25_axi_araddr       ;
  output wire [8-1:0]                            m25_axi_arlen        ;
  input  wire                                    m25_axi_rvalid       ;
  output wire                                    m25_axi_rready       ;
  input  wire [C_M25_AXI_DATA_WIDTH-1:0]         m25_axi_rdata        ;
  input  wire                                    m25_axi_rlast        ;
  // AXI4 master interface m26_axi                                    ;
  output wire                                    m26_axi_awvalid      ;
  input  wire                                    m26_axi_awready      ;
  output wire [C_M26_AXI_ADDR_WIDTH-1:0]         m26_axi_awaddr       ;
  output wire [8-1:0]                            m26_axi_awlen        ;
  output wire                                    m26_axi_wvalid       ;
  input  wire                                    m26_axi_wready       ;
  output wire [C_M26_AXI_DATA_WIDTH-1:0]         m26_axi_wdata        ;
  output wire [C_M26_AXI_DATA_WIDTH/8-1:0]       m26_axi_wstrb        ;
  output wire                                    m26_axi_wlast        ;
  input  wire                                    m26_axi_bvalid       ;
  output wire                                    m26_axi_bready       ;
  output wire                                    m26_axi_arvalid      ;
  input  wire                                    m26_axi_arready      ;
  output wire [C_M26_AXI_ADDR_WIDTH-1:0]         m26_axi_araddr       ;
  output wire [8-1:0]                            m26_axi_arlen        ;
  input  wire                                    m26_axi_rvalid       ;
  output wire                                    m26_axi_rready       ;
  input  wire [C_M26_AXI_DATA_WIDTH-1:0]         m26_axi_rdata        ;
  input  wire                                    m26_axi_rlast        ;
  // AXI4 master interface m27_axi                                    ;
  output wire                                    m27_axi_awvalid      ;
  input  wire                                    m27_axi_awready      ;
  output wire [C_M27_AXI_ADDR_WIDTH-1:0]         m27_axi_awaddr       ;
  output wire [8-1:0]                            m27_axi_awlen        ;
  output wire                                    m27_axi_wvalid       ;
  input  wire                                    m27_axi_wready       ;
  output wire [C_M27_AXI_DATA_WIDTH-1:0]         m27_axi_wdata        ;
  output wire [C_M27_AXI_DATA_WIDTH/8-1:0]       m27_axi_wstrb        ;
  output wire                                    m27_axi_wlast        ;
  input  wire                                    m27_axi_bvalid       ;
  output wire                                    m27_axi_bready       ;
  output wire                                    m27_axi_arvalid      ;
  input  wire                                    m27_axi_arready      ;
  output wire [C_M27_AXI_ADDR_WIDTH-1:0]         m27_axi_araddr       ;
  output wire [8-1:0]                            m27_axi_arlen        ;
  input  wire                                    m27_axi_rvalid       ;
  output wire                                    m27_axi_rready       ;
  input  wire [C_M27_AXI_DATA_WIDTH-1:0]         m27_axi_rdata        ;
  input  wire                                    m27_axi_rlast        ;
  // AXI4 master interface m28_axi                                    ;
  output wire                                    m28_axi_awvalid      ;
  input  wire                                    m28_axi_awready      ;
  output wire [C_M28_AXI_ADDR_WIDTH-1:0]         m28_axi_awaddr       ;
  output wire [8-1:0]                            m28_axi_awlen        ;
  output wire                                    m28_axi_wvalid       ;
  input  wire                                    m28_axi_wready       ;
  output wire [C_M28_AXI_DATA_WIDTH-1:0]         m28_axi_wdata        ;
  output wire [C_M28_AXI_DATA_WIDTH/8-1:0]       m28_axi_wstrb        ;
  output wire                                    m28_axi_wlast        ;
  input  wire                                    m28_axi_bvalid       ;
  output wire                                    m28_axi_bready       ;
  output wire                                    m28_axi_arvalid      ;
  input  wire                                    m28_axi_arready      ;
  output wire [C_M28_AXI_ADDR_WIDTH-1:0]         m28_axi_araddr       ;
  output wire [8-1:0]                            m28_axi_arlen        ;
  input  wire                                    m28_axi_rvalid       ;
  output wire                                    m28_axi_rready       ;
  input  wire [C_M28_AXI_DATA_WIDTH-1:0]         m28_axi_rdata        ;
  input  wire                                    m28_axi_rlast        ;
  // AXI4 master interface m29_axi                                    ;
  output wire                                    m29_axi_awvalid      ;
  input  wire                                    m29_axi_awready      ;
  output wire [C_M29_AXI_ADDR_WIDTH-1:0]         m29_axi_awaddr       ;
  output wire [8-1:0]                            m29_axi_awlen        ;
  output wire                                    m29_axi_wvalid       ;
  input  wire                                    m29_axi_wready       ;
  output wire [C_M29_AXI_DATA_WIDTH-1:0]         m29_axi_wdata        ;
  output wire [C_M29_AXI_DATA_WIDTH/8-1:0]       m29_axi_wstrb        ;
  output wire                                    m29_axi_wlast        ;
  input  wire                                    m29_axi_bvalid       ;
  output wire                                    m29_axi_bready       ;
  output wire                                    m29_axi_arvalid      ;
  input  wire                                    m29_axi_arready      ;
  output wire [C_M29_AXI_ADDR_WIDTH-1:0]         m29_axi_araddr       ;
  output wire [8-1:0]                            m29_axi_arlen        ;
  input  wire                                    m29_axi_rvalid       ;
  output wire                                    m29_axi_rready       ;
  input  wire [C_M29_AXI_DATA_WIDTH-1:0]         m29_axi_rdata        ;
  input  wire                                    m29_axi_rlast        ;
  // AXI4 master interface m30_axi                                    ;
  output wire                                    m30_axi_awvalid      ;
  input  wire                                    m30_axi_awready      ;
  output wire [C_M30_AXI_ADDR_WIDTH-1:0]         m30_axi_awaddr       ;
  output wire [8-1:0]                            m30_axi_awlen        ;
  output wire                                    m30_axi_wvalid       ;
  input  wire                                    m30_axi_wready       ;
  output wire [C_M30_AXI_DATA_WIDTH-1:0]         m30_axi_wdata        ;
  output wire [C_M30_AXI_DATA_WIDTH/8-1:0]       m30_axi_wstrb        ;
  output wire                                    m30_axi_wlast        ;
  input  wire                                    m30_axi_bvalid       ;
  output wire                                    m30_axi_bready       ;
  output wire                                    m30_axi_arvalid      ;
  input  wire                                    m30_axi_arready      ;
  output wire [C_M30_AXI_ADDR_WIDTH-1:0]         m30_axi_araddr       ;
  output wire [8-1:0]                            m30_axi_arlen        ;
  input  wire                                    m30_axi_rvalid       ;
  output wire                                    m30_axi_rready       ;
  input  wire [C_M30_AXI_DATA_WIDTH-1:0]         m30_axi_rdata        ;
  input  wire                                    m30_axi_rlast        ;
  // AXI4 master interface m31_axi                                    ;
  output wire                                    m31_axi_awvalid      ;
  input  wire                                    m31_axi_awready      ;
  output wire [C_M31_AXI_ADDR_WIDTH-1:0]         m31_axi_awaddr       ;
  output wire [8-1:0]                            m31_axi_awlen        ;
  output wire                                    m31_axi_wvalid       ;
  input  wire                                    m31_axi_wready       ;
  output wire [C_M31_AXI_DATA_WIDTH-1:0]         m31_axi_wdata        ;
  output wire [C_M31_AXI_DATA_WIDTH/8-1:0]       m31_axi_wstrb        ;
  output wire                                    m31_axi_wlast        ;
  input  wire                                    m31_axi_bvalid       ;
  output wire                                    m31_axi_bready       ;
  output wire                                    m31_axi_arvalid      ;
  input  wire                                    m31_axi_arready      ;
  output wire [C_M31_AXI_ADDR_WIDTH-1:0]         m31_axi_araddr       ;
  output wire [8-1:0]                            m31_axi_arlen        ;
  input  wire                                    m31_axi_rvalid       ;
  output wire                                    m31_axi_rready       ;
  input  wire [C_M31_AXI_DATA_WIDTH-1:0]         m31_axi_rdata        ;
  input  wire                                    m31_axi_rlast        ;
  
  `endif
  input wire [C_M00_AXI_ADDR_WIDTH-1:0]         A    ;                                                           
    
	
	

   
    wire             clk                  ;        
    (*KEEP = "true"*) wire rst;
    assign clk =  AXI_ACLK;
    assign rst = AXI_ARESET_N;
    (*DONT_TOUCH = "true" *)reg ce = 1'b0;
    
	reg 	[D_W-1:0] 	in_r;
	reg 	[D_W-1:0] 	loopback_r 	[N-1:0];

    wire	[D_W-1:0]	grid_up		[LEVELS-1:0][2*N-1:0];	
	wire			grid_up_v	[LEVELS-1:0][2*N-1:0];	
	wire 			grid_up_r	[LEVELS-1:0][2*N-1:0];	
	wire 			grid_up_l	[LEVELS-1:0][2*N-1:0];	

	wire 	[D_W-1:0] 	grid_dn 	[LEVELS-1:0][2*N-1:0];	
	wire 		 	grid_dn_v 	[LEVELS-1:0][2*N-1:0];	
	wire 	 		grid_dn_r 	[LEVELS-1:0][2*N-1:0];	
	wire 	 		grid_dn_l 	[LEVELS-1:0][2*N-1:0];	

	wire 	[D_W-1:0] 	peo 		[N-1:0];	
	wire 		 	peo_v 		[N-1:0];	
	wire 	 		peo_r 		[N-1:0];	
	wire 	 		peo_l 		[N-1:0];	

	wire 	[D_W-1:0] 	pei 		[N-1:0];	
	wire 		 	pei_v 		[N-1:0];	
	wire 		 	pei_r 		[N-1:0];	
	wire 		 	pei_l 		[N-1:0];	
	
	// the bizarre configuration of array dimensions and sizes allows
	// verilog reduction operation to be used conveniently
	wire [N-1:0] done_pe;
	wire [N/2-1:0] done_sw [LEVELS-1:0];
	wire [LEVELS-1:0] done_sw_lvl; 
	
    reg [63:0] now_r;
	
	reg `Cmd cmd_r = {`linear,`WR,`p2p,6'b1};
	localparam integer TYPE_LEVELS=11;

    
	// tree 
`ifdef TREE
	localparam TYPE = {32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0,32'd0};
`endif
	// xbar 
`ifdef XBAR
	localparam TYPE = {32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1,32'd1};
`endif
	// mesh0 0.5 
`ifdef MESH0
	localparam TYPE = {32'd1,32'd0,32'd1,32'd0,32'd1,32'd0,32'd1,32'd0,32'd1,32'd0,32'd1};
`endif
	// mesh0 0.67 
`ifdef MESH1
	localparam TYPE = {32'd1,32'd1,32'd0,32'd0,32'd1,32'd1,32'd0,32'd0,32'd1,32'd1,32'd0};
`endif
wire in,out;
	genvar m, n, l, m1, q,hr;
	integer r;
	generate if (WRAP==1) begin: localwrap
		for (q = 0; q < N; q = q + 1) begin : as1
//			assign grid_dn[LEVELS-1][q] = in_r;
		end
		always @(posedge clk) begin
			in_r = in;
		end			
	end endgenerate

	reg [(A_W+D_W+2)*N-1:0] out_r;
	genvar x;
	generate for (x = 0; x < N; x = x + 1) begin: routeout
		always @(posedge clk) begin
			out_r[(x+1)*(A_W+D_W+2)-1:x*(A_W+D_W+2)] <= peo[x];
		end
	end endgenerate
	assign out = out_r;	
	generate for (m = 0; m < N/2; m = m + 1) begin : xs
		
	 assign pei[2*m] =   grid_dn[LEVELS-1][2*m];
     assign pei_v[2*m] = grid_dn_v[LEVELS-1][2*m];
     assign grid_dn_r[LEVELS-1][2*m] =  pei_r[2*m];
     assign pei_l[2*m] = grid_dn_l[LEVELS-1][2*m];
     				 
     assign grid_up [LEVELS-1][2*m] =   peo[2*m] ;
     assign grid_up_v [LEVELS-1][2*m] =   peo_v[2*m] ;
     assign peo_r[2*m] = grid_up_r[LEVELS-1][2*m];
     assign grid_up_l [LEVELS-1][2*m] = peo_l[2*m];
     				 
				 client_top #(.M_A_W(M_A_W),.P_W(P_W),.D_W(D_W), .A_W(A_W),.N(N), .posx(2*m), .LIMIT(LIMIT), .RATE(RATE), .SIGMA(SIGMA),.WRAP(WRAP), .PAT(PAT))
		cli0(.clk(clk), .rst(rst), .ce(ce),.cmd(cmd_r),
			.s_axis_c_wdata(pei[2*m]),
			.s_axis_c_wvalid(pei_v[2*m]),
			.s_axis_c_wready(pei_r[2*m]),
			.s_axis_c_wlast(pei_l[2*m]),
			.m_axis_c_wdata(peo[2*m]),
			.m_axis_c_wvalid(peo_v[2*m]),
			.m_axis_c_wready(peo_r[2*m]),
			.m_axis_c_wlast(peo_l[2*m]),
			.done(done_pe[2*m]));
			
	  assign pei[2*m+1] =   grid_dn[LEVELS-1][2*m+1];
      assign pei_v[2*m+1] = grid_dn_v[LEVELS-1][2*m+1];
      assign grid_dn_r[LEVELS-1][2*m+1] =  pei_r[2*m+1];
      assign pei_l[2*m+1] = grid_dn_l[LEVELS-1][2*m+1];
     
      assign grid_up [LEVELS-1][2*m+1] = peo[2*m+1] ;
	  assign grid_up_v [LEVELS-1][2*m+1] = peo_v[2*m+1] ;
      assign peo_r[2*m+1] = grid_up_r[LEVELS-1][2*m+1];	
      assign grid_up_l [LEVELS-1][2*m+1] = peo_l[2*m+1];
		
			 client_top #(.M_A_W(M_A_W),.P_W(P_W),.D_W(D_W), .A_W(A_W),.N(N), .posx(2*m+1), .LIMIT(LIMIT), .SIGMA(SIGMA), .RATE(RATE), .WRAP(WRAP), .PAT(PAT))
		cli1(.clk(clk), .rst(rst), .ce(ce), .cmd(cmd_r),
			.s_axis_c_wdata(pei[2*m+1]),
			.s_axis_c_wvalid(pei_v[2*m+1]),
			.s_axis_c_wready(pei_r[2*m+1]),
			.s_axis_c_wlast(pei_l[2*m+1]),
			.m_axis_c_wdata(peo[2*m+1]),
			.m_axis_c_wvalid(peo_v[2*m+1]),
			.m_axis_c_wready(peo_r[2*m+1]),
			.m_axis_c_wlast(peo_l[2*m+1]),
			.done(done_pe[2*m+1]));
	
	end endgenerate
	
	generate for (l = 0; l < LEVELS; l = l + 1) begin : reduce
		assign done_sw_lvl[l] = &done_sw[l];
	end endgenerate
	integer now=0;
//	assign done_all = &done_pe &done_sw_lvl & (now>16*LIMIT+N); // verilog supports reductions??!

    reg beginClient_r;

	always@(posedge clk) begin
		if(rst) begin
		  now_r<=0;
		  cmd_r <= cmd;
		  beginClient_r <= 0;
		  ce <= 0;
	    end
	    else begin
	       if(beginClient_r) begin
	         cmd_r <= cmd;
	         now_r<=now_r+1;
	         ce <= 1;
	       end
	   beginClient_r <= beginClient;   
	     end  
	end

   

// Nirdheeshwar


genvar ii;

  
    // AXI Write address channel
    wire [M_A_W-1:0]  AXI_00_AWADDR   [32-1:0];//        ;
    wire         AXI_00_AWVALID       [32-1:0];//        ;
                                       
    reg [1:0]    AXI_00_AWBURST       [32-1:0];// = 2'b01; // Fixed address size not supported.
    wire [3:0]    AXI_00_AWLEN         [32-1:0];// = 4'b0;   
    reg [2:0]    AXI_00_AWSIZE        [32-1:0];// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
    reg [5:0]    AXI_00_AWID          [32-1:0];// = 6'b0;
    wire         AXI_00_AWREADY       [32-1:0];//       ;
                                       
    // AXI Write channel               
    wire [C_M00_AXI_DATA_WIDTH-1:0] AXI_00_WDATA         [32-1:0];//       ;   
    wire          AXI_00_WLAST         [32-1:0];// = 1'b1;
    reg [31:0 ]  AXI_00_WSTRB         [32-1:0];// = 32'b1111;
    reg [31:0 ]  AXI_00_WDATA_PARITY  [32-1:0];// = 32'b0;
    wire         AXI_00_WVALID        [32-1:0];//        ;
    wire         AXI_00_WREADY        [32-1:0];//        ;
                                       
    // AXI Write Response channel      
    reg          AXI_00_BREADY        [32-1:0];// =1'b1; //input to HBM, not used
    wire [5:0]   AXI_00_BID           [32-1:0];//      ;
    wire [1:0]   AXI_00_BRESP         [32-1:0];//      ;
    wire         AXI_00_BVALID        [32-1:0];//      ;
                                       
    // AXI Read Address channel        
    wire [M_A_W-1:0]  AXI_00_ARADDR   [32-1:0];//      ;
    wire         AXI_00_ARVALID       [32-1:0];//      ;
    reg [1:0]    AXI_00_ARBURST       [32-1:0];// = 2'b01; // Fixed address size not supported.
    wire [3:0]    AXI_00_ARLEN         [32-1:0];// = 4'b0;   
    reg [2:0]    AXI_00_ARSIZE        [32-1:0];// = 3'b101; // Only 3'b101 supported. 2**5 bytes only
    reg [5:0]    AXI_00_ARID          [32-1:0];// = 6'b0;
    wire         AXI_00_ARREADY       [32-1:0];//      ;
                                       
    // AXI Read Response               
                                       
    wire         AXI_00_RVALID        [32-1:0];//      ;
    wire [5:0]   AXI_00_RID           [32-1:0];//      ;
    wire [C_M00_AXI_DATA_WIDTH-1:0] AXI_00_RDATA         [32-1:0];//      ;
    wire [31:0]  AXI_00_RDATA_PARITY  [32-1:0];//      ;
    wire [1:0]   AXI_00_RRESP         [32-1:0];//      ;
    wire         AXI_00_RLAST         [32-1:0];//      ;
    wire         AXI_00_RREADY        [32-1:0];//      ;

    
    

integer i;
always @(posedge AXI_ACLK) begin
    if(AXI_ARESET_N)begin
        for(i=0;i<N;i=i+1) begin
            AXI_00_AWBURST       [i] <= 2'b01; 
//            AXI_00_AWLEN         [i] <= 4'b0;  
            AXI_00_AWSIZE        [i] <= 3'b101;
            AXI_00_AWID          [i] <= 6'b0;  
                                 
//            AXI_00_WLAST         [i] <= 1'b1;     
            AXI_00_WSTRB         [i] <= 32'b1111; 
            AXI_00_WDATA_PARITY  [i] <= 32'b0;    
                                  
            AXI_00_BREADY        [i]<= 1'b1;
                                  
            AXI_00_ARBURST       [i]<= 2'b01; 
//            AXI_00_ARLEN         [i]<= 4'b0;  
            AXI_00_ARSIZE        [i]<= 3'b101;
            AXI_00_ARID          [i]<= 6'b0;  
        end
    end
    
end



 
//   	assign AXI_00_WDATA [0]    = grid_up[1][0][P_W-1:0];
//    assign AXI_00_AWADDR [0]   = grid_up[1][0][M_A_W+A_W+P_W-1:A_W+P_W];
//    assign AXI_00_WVALID  [0]  = grid_up_v[1][0];
//	assign AXI_00_AWVALID [0]  = grid_up_v[1][0];
//    assign grid_up_r[1][0]  = AXI_00_AWREADY[0] & AXI_00_WREADY[0];

assign AWREADY =  cmd;//AXI_00_AWREADY [0]  ;  
assign WREADY  =  AXI_00_WREADY  [0]  ;     
assign WDATA   =  AXI_00_WDATA   [0]  ;     
assign AWADDR  =  AXI_00_AWADDR  [0]  ;     
assign AWVALID =  AXI_00_AWVALID [0]  ;     
                                  
assign BRESP   =  AXI_00_BRESP   [0]  ;     
assign BVALID  =  AXI_00_BVALID  [0]  ;     
assign ARREADY =  AXI_00_ARREADY [0]  ;     
                                  
assign RVALID  =  AXI_00_RVALID  [1]   ; 
assign RDATA   =  AXI_00_RDATA   [1]   ; 
assign RRESP   =  AXI_00_RRESP   [1]   ; 
assign ARADDR  =  AXI_00_ARADDR  [1]   ; 
assign ARVALID =  AXI_00_ARVALID [1]   ; 


//assign test = grid_dn[LEVELS-1][2];


// FIFO part



wire [D_W-1:0] fifoData          [N-1:0] ;
wire           fifoValid         [N-1:0] ;

              
wire [D_W-1:0] readFifoData      [N-1:0] ;
wire           readFifoValid     [N-1:0] ;

             
wire [D_W-1:0] readRespFifoData  [N-1:0] ;
wire           readRespFifoValid [N-1:0] ;

wire [D_W-1:0] fifoWData          [N-1:0] ;
wire           fifoWValid         [N-1:0] ;
               
wire fifoR                       [N-1:0] ;
wire fifoDR                       [N-1:0] ;
wire readFifoR                   [N-1:0] ;
wire readRespFifoR               [N-1:0] ;


 reg [P_W-1:0]  readData  [N-1:0]   ;
 reg            readValid [N-1:0]   ;
 reg            readLast  [N-1:0]   ;
reg  [A_W-1:0] destAddr  [N-1:0]   ;
                                    


	writeToAxi inst2(

//     .BVALID          (BVALID        )                       
//    .HBM_REF_CLK_0  (HBM_REF_CLK_0     )                       
    .AXI_ACLK       (AXI_ACLK          )                       
   ,.AXI_ARESET_N   (AXI_ARESET_N      )                       
//   ,.APB_PCLK       (APB_PCLK          )                       
//   ,.APB_PRESET_N   (APB_PRESET_N      )                       
   
   ,.done(done)            
   ,.A(A)       
    /************ AXI 00 ***************/  
                    ,.AXI_00_AWVALID (AXI_00_AWVALID[0])
                    ,.AXI_00_AWADDR  (AXI_00_AWADDR [0])
                    ,.AXI_00_AWREADY (AXI_00_AWREADY[0])
                    ,.AXI_00_AWLEN   (AXI_00_AWLEN  [0])
                    ,.AXI_00_WDATA   (AXI_00_WDATA  [0])
                    ,.AXI_00_WVALID  (AXI_00_WVALID [0])
                    ,.AXI_00_WREADY  (AXI_00_WREADY [0])
                    ,.AXI_00_WLAST   (AXI_00_WLAST  [0])
                    ,.AXI_00_ARREADY (AXI_00_ARREADY[0])
                    ,.AXI_00_ARADDR  (AXI_00_ARADDR [0])
                    ,.AXI_00_ARVALID (AXI_00_ARVALID[0])
                    ,.AXI_00_ARLEN   (AXI_00_ARLEN  [0])
                    ,.AXI_00_RDATA   (AXI_00_RDATA  [0])
                    ,.AXI_00_RLAST   (AXI_00_RLAST  [0])
                    ,.AXI_00_RREADY  (AXI_00_RREADY [0])
                    ,.AXI_00_RVALID  (AXI_00_RVALID [0])
                    
                    `ifdef XBARorM1orM0
                    ,.AXI_01_AWVALID (AXI_00_AWVALID[1])
                    ,.AXI_01_AWADDR  (AXI_00_AWADDR [1])
                    ,.AXI_01_AWREADY (AXI_00_AWREADY[1])
                    ,.AXI_01_AWLEN   (AXI_00_AWLEN  [1])
                    ,.AXI_01_WDATA   (AXI_00_WDATA  [1])
                    ,.AXI_01_WVALID  (AXI_00_WVALID [1])
                    ,.AXI_01_WREADY  (AXI_00_WREADY [1])
                    ,.AXI_01_WLAST   (AXI_00_WLAST  [1])
                    ,.AXI_01_ARREADY (AXI_00_ARREADY[1])
                    ,.AXI_01_ARADDR  (AXI_00_ARADDR [1])
                    ,.AXI_01_ARVALID (AXI_00_ARVALID[1])
                    ,.AXI_01_ARLEN   (AXI_00_ARLEN  [1])
                    ,.AXI_01_RDATA   (AXI_00_RDATA  [1])
                    ,.AXI_01_RLAST   (AXI_00_RLAST  [1])
                    ,.AXI_01_RREADY  (AXI_00_RREADY [1])
                    ,.AXI_01_RVALID  (AXI_00_RVALID [1])
               
                    ,.AXI_02_AWVALID (AXI_00_AWVALID[2])
                    ,.AXI_02_AWADDR  (AXI_00_AWADDR [2])
                    ,.AXI_02_AWREADY (AXI_00_AWREADY[2])
                    ,.AXI_02_AWLEN   (AXI_00_AWLEN  [2])
                    ,.AXI_02_WDATA   (AXI_00_WDATA  [2])
                    ,.AXI_02_WVALID  (AXI_00_WVALID [2])
                    ,.AXI_02_WREADY  (AXI_00_WREADY [2])
                    ,.AXI_02_WLAST   (AXI_00_WLAST  [2])
                    ,.AXI_02_ARREADY (AXI_00_ARREADY[2])
                    ,.AXI_02_ARADDR  (AXI_00_ARADDR [2])
                    ,.AXI_02_ARVALID (AXI_00_ARVALID[2])
                    ,.AXI_02_ARLEN   (AXI_00_ARLEN  [2])
                    ,.AXI_02_RDATA   (AXI_00_RDATA  [2])
                    ,.AXI_02_RLAST   (AXI_00_RLAST  [2])
                    ,.AXI_02_RREADY  (AXI_00_RREADY [2])
                    ,.AXI_02_RVALID  (AXI_00_RVALID [2])
              
                    ,.AXI_03_AWVALID (AXI_00_AWVALID[3])
                    ,.AXI_03_AWADDR  (AXI_00_AWADDR [3])
                    ,.AXI_03_AWREADY (AXI_00_AWREADY[3])
                    ,.AXI_03_AWLEN   (AXI_00_AWLEN  [3])
                    ,.AXI_03_WDATA   (AXI_00_WDATA  [3])
                    ,.AXI_03_WVALID  (AXI_00_WVALID [3])
                    ,.AXI_03_WREADY  (AXI_00_WREADY [3])
                    ,.AXI_03_WLAST   (AXI_00_WLAST  [3])
                    ,.AXI_03_ARREADY (AXI_00_ARREADY[3])
                    ,.AXI_03_ARADDR  (AXI_00_ARADDR [3])
                    ,.AXI_03_ARVALID (AXI_00_ARVALID[3])
                    ,.AXI_03_ARLEN   (AXI_00_ARLEN  [3])
                    ,.AXI_03_RDATA   (AXI_00_RDATA  [3])
                    ,.AXI_03_RLAST   (AXI_00_RLAST  [3])
                    ,.AXI_03_RREADY  (AXI_00_RREADY [3])
                    ,.AXI_03_RVALID  (AXI_00_RVALID [3])
                    
                    `endif
                    `ifdef XBARorM1
                    ,.AXI_04_AWVALID (AXI_00_AWVALID[4])  
                    ,.AXI_04_AWADDR  (AXI_00_AWADDR [4])
                    ,.AXI_04_AWREADY (AXI_00_AWREADY[4])
                    ,.AXI_04_AWLEN   (AXI_00_AWLEN  [4])
                    ,.AXI_04_WDATA   (AXI_00_WDATA  [4])
                    ,.AXI_04_WVALID  (AXI_00_WVALID [4])
                    ,.AXI_04_WREADY  (AXI_00_WREADY [4])
                    ,.AXI_04_WLAST   (AXI_00_WLAST  [4])
                    ,.AXI_04_ARREADY (AXI_00_ARREADY[4])
                    ,.AXI_04_ARADDR  (AXI_00_ARADDR [4])
                    ,.AXI_04_ARVALID (AXI_00_ARVALID[4])
                    ,.AXI_04_ARLEN   (AXI_00_ARLEN  [4])
                    ,.AXI_04_RDATA   (AXI_00_RDATA  [4])
                    ,.AXI_04_RLAST   (AXI_00_RLAST  [4])
                    ,.AXI_04_RREADY  (AXI_00_RREADY [4])
                    ,.AXI_04_RVALID  (AXI_00_RVALID [4])
                  
                    ,.AXI_05_AWVALID (AXI_00_AWVALID[5])
                    ,.AXI_05_AWADDR  (AXI_00_AWADDR [5])
                    ,.AXI_05_AWREADY (AXI_00_AWREADY[5])
                    ,.AXI_05_AWLEN   (AXI_00_AWLEN  [5])
                    ,.AXI_05_WDATA   (AXI_00_WDATA  [5])
                    ,.AXI_05_WVALID  (AXI_00_WVALID [5])
                    ,.AXI_05_WREADY  (AXI_00_WREADY [5])
                    ,.AXI_05_WLAST   (AXI_00_WLAST  [5])
                    ,.AXI_05_ARREADY (AXI_00_ARREADY[5])
                    ,.AXI_05_ARADDR  (AXI_00_ARADDR [5])
                    ,.AXI_05_ARVALID (AXI_00_ARVALID[5])
                    ,.AXI_05_ARLEN   (AXI_00_ARLEN  [5])
                    ,.AXI_05_RDATA   (AXI_00_RDATA  [5])
                    ,.AXI_05_RLAST   (AXI_00_RLAST  [5])
                    ,.AXI_05_RREADY  (AXI_00_RREADY [5])
                    ,.AXI_05_RVALID  (AXI_00_RVALID [5])
                    
                    ,.AXI_06_AWVALID (AXI_00_AWVALID[6])
                    ,.AXI_06_AWADDR  (AXI_00_AWADDR [6])
                    ,.AXI_06_AWREADY (AXI_00_AWREADY[6])
                    ,.AXI_06_AWLEN   (AXI_00_AWLEN  [6])
                    ,.AXI_06_WDATA   (AXI_00_WDATA  [6])
                    ,.AXI_06_WVALID  (AXI_00_WVALID [6])
                    ,.AXI_06_WREADY  (AXI_00_WREADY [6])
                    ,.AXI_06_WLAST   (AXI_00_WLAST  [6])
                    ,.AXI_06_ARREADY (AXI_00_ARREADY[6])
                    ,.AXI_06_ARADDR  (AXI_00_ARADDR [6])
                    ,.AXI_06_ARVALID (AXI_00_ARVALID[6])
                    ,.AXI_06_ARLEN   (AXI_00_ARLEN  [6])
                    ,.AXI_06_RDATA   (AXI_00_RDATA  [6])
                    ,.AXI_06_RLAST   (AXI_00_RLAST  [6])
                    ,.AXI_06_RREADY  (AXI_00_RREADY [6])
                    ,.AXI_06_RVALID  (AXI_00_RVALID [6])
                   
                    ,.AXI_07_AWVALID (AXI_00_AWVALID[7])
                    ,.AXI_07_AWADDR  (AXI_00_AWADDR [7])
                    ,.AXI_07_AWREADY (AXI_00_AWREADY[7])
                    ,.AXI_07_AWLEN   (AXI_00_AWLEN  [7])
                    ,.AXI_07_WDATA   (AXI_00_WDATA  [7])
                    ,.AXI_07_WVALID  (AXI_00_WVALID [7])
                    ,.AXI_07_WREADY  (AXI_00_WREADY [7])
                    ,.AXI_07_WLAST   (AXI_00_WLAST  [7])
                    ,.AXI_07_ARREADY (AXI_00_ARREADY[7])
                    ,.AXI_07_ARADDR  (AXI_00_ARADDR [7])
                    ,.AXI_07_ARVALID (AXI_00_ARVALID[7])
                    ,.AXI_07_ARLEN   (AXI_00_ARLEN  [7])
                    ,.AXI_07_RDATA   (AXI_00_RDATA  [7])
                    ,.AXI_07_RLAST   (AXI_00_RLAST  [7])
                    ,.AXI_07_RREADY  (AXI_00_RREADY [7])
                    ,.AXI_07_RVALID  (AXI_00_RVALID [7])
                    
                    `endif
                    `ifdef XBARorM0
                    ,.AXI_08_AWVALID (AXI_00_AWVALID[8])
                    ,.AXI_08_AWADDR  (AXI_00_AWADDR [8])
                    ,.AXI_08_AWREADY (AXI_00_AWREADY[8])
                    ,.AXI_08_AWLEN   (AXI_00_AWLEN  [8])
                    ,.AXI_08_WDATA   (AXI_00_WDATA  [8])
                    ,.AXI_08_WVALID  (AXI_00_WVALID [8])
                    ,.AXI_08_WREADY  (AXI_00_WREADY [8])
                    ,.AXI_08_WLAST   (AXI_00_WLAST  [8])
                    ,.AXI_08_ARREADY (AXI_00_ARREADY[8])
                    ,.AXI_08_ARADDR  (AXI_00_ARADDR [8])
                    ,.AXI_08_ARVALID (AXI_00_ARVALID[8])
                    ,.AXI_08_ARLEN   (AXI_00_ARLEN  [8])
                    ,.AXI_08_RDATA   (AXI_00_RDATA  [8])
                    ,.AXI_08_RLAST   (AXI_00_RLAST  [8])
                    ,.AXI_08_RREADY  (AXI_00_RREADY [8])
                    ,.AXI_08_RVALID  (AXI_00_RVALID [8])
                    
                    ,.AXI_09_AWVALID (AXI_00_AWVALID[9])
                    ,.AXI_09_AWADDR  (AXI_00_AWADDR [9])
                    ,.AXI_09_AWREADY (AXI_00_AWREADY[9])
                    ,.AXI_09_AWLEN   (AXI_00_AWLEN  [9])
                    ,.AXI_09_WDATA   (AXI_00_WDATA  [9])
                    ,.AXI_09_WVALID  (AXI_00_WVALID [9])
                    ,.AXI_09_WREADY  (AXI_00_WREADY [9])
                    ,.AXI_09_WLAST   (AXI_00_WLAST  [9])
                    ,.AXI_09_ARREADY (AXI_00_ARREADY[9])
                    ,.AXI_09_ARADDR  (AXI_00_ARADDR [9])
                    ,.AXI_09_ARVALID (AXI_00_ARVALID[9])
                    ,.AXI_09_ARLEN   (AXI_00_ARLEN  [9])
                    ,.AXI_09_RDATA   (AXI_00_RDATA  [9])
                    ,.AXI_09_RLAST   (AXI_00_RLAST  [9])
                    ,.AXI_09_RREADY  (AXI_00_RREADY [9])
                    ,.AXI_09_RVALID  (AXI_00_RVALID [9])
                    
                    ,.AXI_10_AWVALID (AXI_00_AWVALID[10])
                    ,.AXI_10_AWADDR  (AXI_00_AWADDR [10])
                    ,.AXI_10_AWREADY (AXI_00_AWREADY[10])
                    ,.AXI_10_AWLEN   (AXI_00_AWLEN  [10])
                    ,.AXI_10_WDATA   (AXI_00_WDATA  [10])
                    ,.AXI_10_WVALID  (AXI_00_WVALID [10])
                    ,.AXI_10_WREADY  (AXI_00_WREADY [10])
                    ,.AXI_10_WLAST   (AXI_00_WLAST  [10])
                    ,.AXI_10_ARREADY (AXI_00_ARREADY[10])
                    ,.AXI_10_ARADDR  (AXI_00_ARADDR [10])
                    ,.AXI_10_ARVALID (AXI_00_ARVALID[10])
                    ,.AXI_10_ARLEN   (AXI_00_ARLEN  [10])
                    ,.AXI_10_RDATA   (AXI_00_RDATA  [10])
                    ,.AXI_10_RLAST   (AXI_00_RLAST  [10])
                    ,.AXI_10_RREADY  (AXI_00_RREADY [10])
                    ,.AXI_10_RVALID  (AXI_00_RVALID [10])
                    
                    ,.AXI_11_AWVALID (AXI_00_AWVALID[11])
                    ,.AXI_11_AWADDR  (AXI_00_AWADDR [11])
                    ,.AXI_11_AWREADY (AXI_00_AWREADY[11])
                    ,.AXI_11_AWLEN   (AXI_00_AWLEN  [11])
                    ,.AXI_11_WDATA   (AXI_00_WDATA  [11])
                    ,.AXI_11_WVALID  (AXI_00_WVALID [11])
                    ,.AXI_11_WREADY  (AXI_00_WREADY [11])
                    ,.AXI_11_WLAST   (AXI_00_WLAST  [11])
                    ,.AXI_11_ARREADY (AXI_00_ARREADY[11])
                    ,.AXI_11_ARADDR  (AXI_00_ARADDR [11])
                    ,.AXI_11_ARVALID (AXI_00_ARVALID[11])
                    ,.AXI_11_ARLEN   (AXI_00_ARLEN  [11])
                    ,.AXI_11_RDATA   (AXI_00_RDATA  [11])
                    ,.AXI_11_RLAST   (AXI_00_RLAST  [11])
                    ,.AXI_11_RREADY  (AXI_00_RREADY [11])
                    ,.AXI_11_RVALID  (AXI_00_RVALID [11])
                    `endif
                    `ifdef XBAR
                    ,.AXI_12_AWVALID (AXI_00_AWVALID[12]) 
                    ,.AXI_12_AWADDR  (AXI_00_AWADDR [12]) 
                    ,.AXI_12_AWREADY (AXI_00_AWREADY[12]) 
                    ,.AXI_12_AWLEN   (AXI_00_AWLEN  [12]) 
                    ,.AXI_12_WDATA   (AXI_00_WDATA  [12]) 
                    ,.AXI_12_WVALID  (AXI_00_WVALID [12]) 
                    ,.AXI_12_WREADY  (AXI_00_WREADY [12]) 
                    ,.AXI_12_WLAST   (AXI_00_WLAST  [12]) 
                    ,.AXI_12_ARREADY (AXI_00_ARREADY[12]) 
                    ,.AXI_12_ARADDR  (AXI_00_ARADDR [12]) 
                    ,.AXI_12_ARVALID (AXI_00_ARVALID[12]) 
                    ,.AXI_12_ARLEN   (AXI_00_ARLEN  [12]) 
                    ,.AXI_12_RDATA   (AXI_00_RDATA  [12]) 
                    ,.AXI_12_RLAST   (AXI_00_RLAST  [12]) 
                    ,.AXI_12_RREADY  (AXI_00_RREADY [12]) 
                    ,.AXI_12_RVALID  (AXI_00_RVALID [12]) 
                    
                    ,.AXI_13_AWVALID (AXI_00_AWVALID[13]) 
                    ,.AXI_13_AWADDR  (AXI_00_AWADDR [13]) 
                    ,.AXI_13_AWREADY (AXI_00_AWREADY[13]) 
                    ,.AXI_13_AWLEN   (AXI_00_AWLEN  [13]) 
                    ,.AXI_13_WDATA   (AXI_00_WDATA  [13]) 
                    ,.AXI_13_WVALID  (AXI_00_WVALID [13]) 
                    ,.AXI_13_WREADY  (AXI_00_WREADY [13]) 
                    ,.AXI_13_WLAST   (AXI_00_WLAST  [13]) 
                    ,.AXI_13_ARREADY (AXI_00_ARREADY[13]) 
                    ,.AXI_13_ARADDR  (AXI_00_ARADDR [13]) 
                    ,.AXI_13_ARVALID (AXI_00_ARVALID[13]) 
                    ,.AXI_13_ARLEN   (AXI_00_ARLEN  [13]) 
                    ,.AXI_13_RDATA   (AXI_00_RDATA  [13]) 
                    ,.AXI_13_RLAST   (AXI_00_RLAST  [13]) 
                    ,.AXI_13_RREADY  (AXI_00_RREADY [13]) 
                    ,.AXI_13_RVALID  (AXI_00_RVALID [13]) 
                   
                    ,.AXI_14_AWVALID (AXI_00_AWVALID[14])
                    ,.AXI_14_AWADDR  (AXI_00_AWADDR [14])
                    ,.AXI_14_AWREADY (AXI_00_AWREADY[14])
                    ,.AXI_14_AWLEN   (AXI_00_AWLEN  [14])
                    ,.AXI_14_WDATA   (AXI_00_WDATA  [14])
                    ,.AXI_14_WVALID  (AXI_00_WVALID [14])
                    ,.AXI_14_WREADY  (AXI_00_WREADY [14])
                    ,.AXI_14_WLAST   (AXI_00_WLAST  [14])
                    ,.AXI_14_ARREADY (AXI_00_ARREADY[14])
                    ,.AXI_14_ARADDR  (AXI_00_ARADDR [14])
                    ,.AXI_14_ARVALID (AXI_00_ARVALID[14])
                    ,.AXI_14_ARLEN   (AXI_00_ARLEN  [14])
                    ,.AXI_14_RDATA   (AXI_00_RDATA  [14])
                    ,.AXI_14_RLAST   (AXI_00_RLAST  [14])
                    ,.AXI_14_RREADY  (AXI_00_RREADY [14])
                    ,.AXI_14_RVALID  (AXI_00_RVALID [14])
                   
                    ,.AXI_15_AWVALID (AXI_00_AWVALID[15])
                    ,.AXI_15_AWADDR  (AXI_00_AWADDR [15])
                    ,.AXI_15_AWREADY (AXI_00_AWREADY[15])
                    ,.AXI_15_AWLEN   (AXI_00_AWLEN  [15])
                    ,.AXI_15_WDATA   (AXI_00_WDATA  [15])
                    ,.AXI_15_WVALID  (AXI_00_WVALID [15])
                    ,.AXI_15_WREADY  (AXI_00_WREADY [15])
                    ,.AXI_15_WLAST   (AXI_00_WLAST  [15])
                    ,.AXI_15_ARREADY (AXI_00_ARREADY[15])
                    ,.AXI_15_ARADDR  (AXI_00_ARADDR [15])
                    ,.AXI_15_ARVALID (AXI_00_ARVALID[15])
                    ,.AXI_15_ARLEN   (AXI_00_ARLEN  [15])
                    ,.AXI_15_RDATA   (AXI_00_RDATA  [15])
                    ,.AXI_15_RLAST   (AXI_00_RLAST  [15])
                    ,.AXI_15_RREADY  (AXI_00_RREADY [15])
                    ,.AXI_15_RVALID  (AXI_00_RVALID [15])
                   
                    ,.AXI_16_AWVALID (AXI_00_AWVALID[16]) 
                    ,.AXI_16_AWADDR  (AXI_00_AWADDR [16]) 
                    ,.AXI_16_AWREADY (AXI_00_AWREADY[16]) 
                    ,.AXI_16_AWLEN   (AXI_00_AWLEN  [16]) 
                    ,.AXI_16_WDATA   (AXI_00_WDATA  [16]) 
                    ,.AXI_16_WVALID  (AXI_00_WVALID [16]) 
                    ,.AXI_16_WREADY  (AXI_00_WREADY [16]) 
                    ,.AXI_16_WLAST   (AXI_00_WLAST  [16]) 
                    ,.AXI_16_ARREADY (AXI_00_ARREADY[16]) 
                    ,.AXI_16_ARADDR  (AXI_00_ARADDR [16]) 
                    ,.AXI_16_ARVALID (AXI_00_ARVALID[16]) 
                    ,.AXI_16_ARLEN   (AXI_00_ARLEN  [16]) 
                    ,.AXI_16_RDATA   (AXI_00_RDATA  [16]) 
                    ,.AXI_16_RLAST   (AXI_00_RLAST  [16]) 
                    ,.AXI_16_RREADY  (AXI_00_RREADY [16]) 
                    ,.AXI_16_RVALID  (AXI_00_RVALID [16]) 
                   
                    ,.AXI_17_AWVALID (AXI_00_AWVALID[17]) 
                    ,.AXI_17_AWADDR  (AXI_00_AWADDR [17]) 
                    ,.AXI_17_AWREADY (AXI_00_AWREADY[17]) 
                    ,.AXI_17_AWLEN   (AXI_00_AWLEN  [17]) 
                    ,.AXI_17_WDATA   (AXI_00_WDATA  [17]) 
                    ,.AXI_17_WVALID  (AXI_00_WVALID [17]) 
                    ,.AXI_17_WREADY  (AXI_00_WREADY [17]) 
                    ,.AXI_17_WLAST   (AXI_00_WLAST  [17]) 
                    ,.AXI_17_ARREADY (AXI_00_ARREADY[17]) 
                    ,.AXI_17_ARADDR  (AXI_00_ARADDR [17]) 
                    ,.AXI_17_ARVALID (AXI_00_ARVALID[17]) 
                    ,.AXI_17_ARLEN   (AXI_00_ARLEN  [17]) 
                    ,.AXI_17_RDATA   (AXI_00_RDATA  [17]) 
                    ,.AXI_17_RLAST   (AXI_00_RLAST  [17]) 
                    ,.AXI_17_RREADY  (AXI_00_RREADY [17]) 
                    ,.AXI_17_RVALID  (AXI_00_RVALID [17]) 
                    
                    ,.AXI_18_AWVALID (AXI_00_AWVALID[18])
                    ,.AXI_18_AWADDR  (AXI_00_AWADDR [18])
                    ,.AXI_18_AWREADY (AXI_00_AWREADY[18])
                    ,.AXI_18_AWLEN   (AXI_00_AWLEN  [18])
                    ,.AXI_18_WDATA   (AXI_00_WDATA  [18])
                    ,.AXI_18_WVALID  (AXI_00_WVALID [18])
                    ,.AXI_18_WREADY  (AXI_00_WREADY [18])
                    ,.AXI_18_WLAST   (AXI_00_WLAST  [18])
                    ,.AXI_18_ARREADY (AXI_00_ARREADY[18])
                    ,.AXI_18_ARADDR  (AXI_00_ARADDR [18])
                    ,.AXI_18_ARVALID (AXI_00_ARVALID[18])
                    ,.AXI_18_ARLEN   (AXI_00_ARLEN  [18])
                    ,.AXI_18_RDATA   (AXI_00_RDATA  [18])
                    ,.AXI_18_RLAST   (AXI_00_RLAST  [18])
                    ,.AXI_18_RREADY  (AXI_00_RREADY [18])
                    ,.AXI_18_RVALID  (AXI_00_RVALID [18])
                  
                    ,.AXI_19_AWVALID (AXI_00_AWVALID[19])
                    ,.AXI_19_AWADDR  (AXI_00_AWADDR [19])
                    ,.AXI_19_AWREADY (AXI_00_AWREADY[19])
                    ,.AXI_19_AWLEN   (AXI_00_AWLEN  [19])
                    ,.AXI_19_WDATA   (AXI_00_WDATA  [19])
                    ,.AXI_19_WVALID  (AXI_00_WVALID [19])
                    ,.AXI_19_WREADY  (AXI_00_WREADY [19])
                    ,.AXI_19_WLAST   (AXI_00_WLAST  [19])
                    ,.AXI_19_ARREADY (AXI_00_ARREADY[19])
                    ,.AXI_19_ARADDR  (AXI_00_ARADDR [19])
                    ,.AXI_19_ARVALID (AXI_00_ARVALID[19])
                    ,.AXI_19_ARLEN   (AXI_00_ARLEN  [19])
                    ,.AXI_19_RDATA   (AXI_00_RDATA  [19])
                    ,.AXI_19_RLAST   (AXI_00_RLAST  [19])
                    ,.AXI_19_RREADY  (AXI_00_RREADY [19])
                    ,.AXI_19_RVALID  (AXI_00_RVALID [19])
                   
                    ,.AXI_20_AWVALID (AXI_00_AWVALID[20])
                    ,.AXI_20_AWADDR  (AXI_00_AWADDR [20])
                    ,.AXI_20_AWREADY (AXI_00_AWREADY[20])
                    ,.AXI_20_AWLEN   (AXI_00_AWLEN  [20])
                    ,.AXI_20_WDATA   (AXI_00_WDATA  [20])
                    ,.AXI_20_WVALID  (AXI_00_WVALID [20])
                    ,.AXI_20_WREADY  (AXI_00_WREADY [20])
                    ,.AXI_20_WLAST   (AXI_00_WLAST  [20])
                    ,.AXI_20_ARREADY (AXI_00_ARREADY[20])
                    ,.AXI_20_ARADDR  (AXI_00_ARADDR [20])
                    ,.AXI_20_ARVALID (AXI_00_ARVALID[20])
                    ,.AXI_20_ARLEN   (AXI_00_ARLEN  [20])
                    ,.AXI_20_RDATA   (AXI_00_RDATA  [20])
                    ,.AXI_20_RLAST   (AXI_00_RLAST  [20])
                    ,.AXI_20_RREADY  (AXI_00_RREADY [20])
                    ,.AXI_20_RVALID  (AXI_00_RVALID [20])
                    
                    ,.AXI_21_AWVALID (AXI_00_AWVALID[21])
                    ,.AXI_21_AWADDR  (AXI_00_AWADDR [21])
                    ,.AXI_21_AWREADY (AXI_00_AWREADY[21])
                    ,.AXI_21_AWLEN   (AXI_00_AWLEN  [21])
                    ,.AXI_21_WDATA   (AXI_00_WDATA  [21])
                    ,.AXI_21_WVALID  (AXI_00_WVALID [21])
                    ,.AXI_21_WREADY  (AXI_00_WREADY [21])
                    ,.AXI_21_WLAST   (AXI_00_WLAST  [21])
                    ,.AXI_21_ARREADY (AXI_00_ARREADY[21])
                    ,.AXI_21_ARADDR  (AXI_00_ARADDR [21])
                    ,.AXI_21_ARVALID (AXI_00_ARVALID[21])
                    ,.AXI_21_ARLEN   (AXI_00_ARLEN  [21])
                    ,.AXI_21_RDATA   (AXI_00_RDATA  [21])
                    ,.AXI_21_RLAST   (AXI_00_RLAST  [21])
                    ,.AXI_21_RREADY  (AXI_00_RREADY [21])
                    ,.AXI_21_RVALID  (AXI_00_RVALID [21])
                   
                    ,.AXI_22_AWVALID (AXI_00_AWVALID[22])
                    ,.AXI_22_AWADDR  (AXI_00_AWADDR [22])
                    ,.AXI_22_AWREADY (AXI_00_AWREADY[22])
                    ,.AXI_22_AWLEN   (AXI_00_AWLEN  [22])
                    ,.AXI_22_WDATA   (AXI_00_WDATA  [22])
                    ,.AXI_22_WVALID  (AXI_00_WVALID [22])
                    ,.AXI_22_WREADY  (AXI_00_WREADY [22])
                    ,.AXI_22_WLAST   (AXI_00_WLAST  [22])
                    ,.AXI_22_ARREADY (AXI_00_ARREADY[22])
                    ,.AXI_22_ARADDR  (AXI_00_ARADDR [22])
                    ,.AXI_22_ARVALID (AXI_00_ARVALID[22])
                    ,.AXI_22_ARLEN   (AXI_00_ARLEN  [22])
                    ,.AXI_22_RDATA   (AXI_00_RDATA  [22])
                    ,.AXI_22_RLAST   (AXI_00_RLAST  [22])
                    ,.AXI_22_RREADY  (AXI_00_RREADY [22])
                    ,.AXI_22_RVALID  (AXI_00_RVALID [22])
                    
                    ,.AXI_23_AWVALID (AXI_00_AWVALID[23])
                    ,.AXI_23_AWADDR  (AXI_00_AWADDR [23])
                    ,.AXI_23_AWREADY (AXI_00_AWREADY[23])
                    ,.AXI_23_AWLEN   (AXI_00_AWLEN  [23])
                    ,.AXI_23_WDATA   (AXI_00_WDATA  [23])
                    ,.AXI_23_WVALID  (AXI_00_WVALID [23])
                    ,.AXI_23_WREADY  (AXI_00_WREADY [23])
                    ,.AXI_23_WLAST   (AXI_00_WLAST  [23])
                    ,.AXI_23_ARREADY (AXI_00_ARREADY[23])
                    ,.AXI_23_ARADDR  (AXI_00_ARADDR [23])
                    ,.AXI_23_ARVALID (AXI_00_ARVALID[23])
                    ,.AXI_23_ARLEN   (AXI_00_ARLEN  [23])
                    ,.AXI_23_RDATA   (AXI_00_RDATA  [23])
                    ,.AXI_23_RLAST   (AXI_00_RLAST  [23])
                    ,.AXI_23_RREADY  (AXI_00_RREADY [23])
                    ,.AXI_23_RVALID  (AXI_00_RVALID [23])
                   
                    ,.AXI_24_AWVALID (AXI_00_AWVALID[24])
                    ,.AXI_24_AWADDR  (AXI_00_AWADDR [24])
                    ,.AXI_24_AWREADY (AXI_00_AWREADY[24])
                    ,.AXI_24_AWLEN   (AXI_00_AWLEN  [24])
                    ,.AXI_24_WDATA   (AXI_00_WDATA  [24])
                    ,.AXI_24_WVALID  (AXI_00_WVALID [24])
                    ,.AXI_24_WREADY  (AXI_00_WREADY [24])
                    ,.AXI_24_WLAST   (AXI_00_WLAST  [24])
                    ,.AXI_24_ARREADY (AXI_00_ARREADY[24])
                    ,.AXI_24_ARADDR  (AXI_00_ARADDR [24])
                    ,.AXI_24_ARVALID (AXI_00_ARVALID[24])
                    ,.AXI_24_ARLEN   (AXI_00_ARLEN  [24])
                    ,.AXI_24_RDATA   (AXI_00_RDATA  [24])
                    ,.AXI_24_RLAST   (AXI_00_RLAST  [24])
                    ,.AXI_24_RREADY  (AXI_00_RREADY [24])
                    ,.AXI_24_RVALID  (AXI_00_RVALID [24])
                  
                    ,.AXI_25_AWVALID (AXI_00_AWVALID[25])
                    ,.AXI_25_AWADDR  (AXI_00_AWADDR [25])
                    ,.AXI_25_AWREADY (AXI_00_AWREADY[25])
                    ,.AXI_25_AWLEN   (AXI_00_AWLEN  [25])
                    ,.AXI_25_WDATA   (AXI_00_WDATA  [25])
                    ,.AXI_25_WVALID  (AXI_00_WVALID [25])
                    ,.AXI_25_WREADY  (AXI_00_WREADY [25])
                    ,.AXI_25_WLAST   (AXI_00_WLAST  [25])
                    ,.AXI_25_ARREADY (AXI_00_ARREADY[25])
                    ,.AXI_25_ARADDR  (AXI_00_ARADDR [25])
                    ,.AXI_25_ARVALID (AXI_00_ARVALID[25])
                    ,.AXI_25_ARLEN   (AXI_00_ARLEN  [25])
                    ,.AXI_25_RDATA   (AXI_00_RDATA  [25])
                    ,.AXI_25_RLAST   (AXI_00_RLAST  [25])
                    ,.AXI_25_RREADY  (AXI_00_RREADY [25])
                    ,.AXI_25_RVALID  (AXI_00_RVALID [25])
                   
                    ,.AXI_26_AWVALID (AXI_00_AWVALID[26])
                    ,.AXI_26_AWADDR  (AXI_00_AWADDR [26])
                    ,.AXI_26_AWREADY (AXI_00_AWREADY[26])
                    ,.AXI_26_AWLEN   (AXI_00_AWLEN  [26])
                    ,.AXI_26_WDATA   (AXI_00_WDATA  [26])
                    ,.AXI_26_WVALID  (AXI_00_WVALID [26])
                    ,.AXI_26_WREADY  (AXI_00_WREADY [26])
                    ,.AXI_26_WLAST   (AXI_00_WLAST  [26])
                    ,.AXI_26_ARREADY (AXI_00_ARREADY[26])
                    ,.AXI_26_ARADDR  (AXI_00_ARADDR [26])
                    ,.AXI_26_ARVALID (AXI_00_ARVALID[26])
                    ,.AXI_26_ARLEN   (AXI_00_ARLEN  [26])
                    ,.AXI_26_RDATA   (AXI_00_RDATA  [26])
                    ,.AXI_26_RLAST   (AXI_00_RLAST  [26])
                    ,.AXI_26_RREADY  (AXI_00_RREADY [26])
                    ,.AXI_26_RVALID  (AXI_00_RVALID [26])
                    
                    ,.AXI_27_AWVALID (AXI_00_AWVALID[27])
                    ,.AXI_27_AWADDR  (AXI_00_AWADDR [27])
                    ,.AXI_27_AWREADY (AXI_00_AWREADY[27])
                    ,.AXI_27_AWLEN   (AXI_00_AWLEN  [27])
                    ,.AXI_27_WDATA   (AXI_00_WDATA  [27])
                    ,.AXI_27_WVALID  (AXI_00_WVALID [27])
                    ,.AXI_27_WREADY  (AXI_00_WREADY [27])
                    ,.AXI_27_WLAST   (AXI_00_WLAST  [27])
                    ,.AXI_27_ARREADY (AXI_00_ARREADY[27])
                    ,.AXI_27_ARADDR  (AXI_00_ARADDR [27])
                    ,.AXI_27_ARVALID (AXI_00_ARVALID[27])
                    ,.AXI_27_ARLEN   (AXI_00_ARLEN  [27])
                    ,.AXI_27_RDATA   (AXI_00_RDATA  [27])
                    ,.AXI_27_RLAST   (AXI_00_RLAST  [27])
                    ,.AXI_27_RREADY  (AXI_00_RREADY [27])
                    ,.AXI_27_RVALID  (AXI_00_RVALID [27])
                    
                    ,.AXI_28_AWVALID (AXI_00_AWVALID[28])   
                    ,.AXI_28_AWADDR  (AXI_00_AWADDR [28])   
                    ,.AXI_28_AWREADY (AXI_00_AWREADY[28])   
                    ,.AXI_28_AWLEN   (AXI_00_AWLEN  [28])   
                    ,.AXI_28_WDATA   (AXI_00_WDATA  [28])   
                    ,.AXI_28_WVALID  (AXI_00_WVALID [28])   
                    ,.AXI_28_WREADY  (AXI_00_WREADY [28])   
                    ,.AXI_28_WLAST   (AXI_00_WLAST  [28])   
                    ,.AXI_28_ARREADY (AXI_00_ARREADY[28])   
                    ,.AXI_28_ARADDR  (AXI_00_ARADDR [28])   
                    ,.AXI_28_ARVALID (AXI_00_ARVALID[28])   
                    ,.AXI_28_ARLEN   (AXI_00_ARLEN  [28])   
                    ,.AXI_28_RDATA   (AXI_00_RDATA  [28])   
                    ,.AXI_28_RLAST   (AXI_00_RLAST  [28])   
                    ,.AXI_28_RREADY  (AXI_00_RREADY [28])   
                    ,.AXI_28_RVALID  (AXI_00_RVALID [28])   
                   
                    ,.AXI_29_AWVALID (AXI_00_AWVALID[29])   
                    ,.AXI_29_AWADDR  (AXI_00_AWADDR [29])   
                    ,.AXI_29_AWREADY (AXI_00_AWREADY[29])   
                    ,.AXI_29_AWLEN   (AXI_00_AWLEN  [29])   
                    ,.AXI_29_WDATA   (AXI_00_WDATA  [29])   
                    ,.AXI_29_WVALID  (AXI_00_WVALID [29])   
                    ,.AXI_29_WREADY  (AXI_00_WREADY [29])   
                    ,.AXI_29_WLAST   (AXI_00_WLAST  [29])   
                    ,.AXI_29_ARREADY (AXI_00_ARREADY[29])   
                    ,.AXI_29_ARADDR  (AXI_00_ARADDR [29])   
                    ,.AXI_29_ARVALID (AXI_00_ARVALID[29])   
                    ,.AXI_29_ARLEN   (AXI_00_ARLEN  [29])   
                    ,.AXI_29_RDATA   (AXI_00_RDATA  [29])   
                    ,.AXI_29_RLAST   (AXI_00_RLAST  [29])   
                    ,.AXI_29_RREADY  (AXI_00_RREADY [29])   
                    ,.AXI_29_RVALID  (AXI_00_RVALID [29])   
                   
                    ,.AXI_30_AWVALID (AXI_00_AWVALID[30])   
                    ,.AXI_30_AWADDR  (AXI_00_AWADDR [30])   
                    ,.AXI_30_AWREADY (AXI_00_AWREADY[30])   
                    ,.AXI_30_AWLEN   (AXI_00_AWLEN  [30])   
                    ,.AXI_30_WDATA   (AXI_00_WDATA  [30])   
                    ,.AXI_30_WVALID  (AXI_00_WVALID [30])   
                    ,.AXI_30_WREADY  (AXI_00_WREADY [30])   
                    ,.AXI_30_WLAST   (AXI_00_WLAST  [30])   
                    ,.AXI_30_ARREADY (AXI_00_ARREADY[30])   
                    ,.AXI_30_ARADDR  (AXI_00_ARADDR [30])   
                    ,.AXI_30_ARVALID (AXI_00_ARVALID[30])   
                    ,.AXI_30_ARLEN   (AXI_00_ARLEN  [30])   
                    ,.AXI_30_RDATA   (AXI_00_RDATA  [30])   
                    ,.AXI_30_RLAST   (AXI_00_RLAST  [30])   
                    ,.AXI_30_RREADY  (AXI_00_RREADY [30])   
                    ,.AXI_30_RVALID  (AXI_00_RVALID [30])   
                   
                    ,.AXI_31_AWVALID (AXI_00_AWVALID[31])   
                    ,.AXI_31_AWADDR  (AXI_00_AWADDR [31])   
                    ,.AXI_31_AWREADY (AXI_00_AWREADY[31])   
                    ,.AXI_31_AWLEN   (AXI_00_AWLEN  [31])   
                    ,.AXI_31_WDATA   (AXI_00_WDATA  [31])   
                    ,.AXI_31_WVALID  (AXI_00_WVALID [31])   
                    ,.AXI_31_WREADY  (AXI_00_WREADY [31])   
                    ,.AXI_31_WLAST   (AXI_00_WLAST  [31])   
                    ,.AXI_31_ARREADY (AXI_00_ARREADY[31])   
                    ,.AXI_31_ARADDR  (AXI_00_ARADDR [31])   
                    ,.AXI_31_ARVALID (AXI_00_ARVALID[31])   
                    ,.AXI_31_ARLEN   (AXI_00_ARLEN  [31])   
                    ,.AXI_31_RDATA   (AXI_00_RDATA  [31])   
                    ,.AXI_31_RLAST   (AXI_00_RLAST  [31])   
                    ,.AXI_31_RREADY  (AXI_00_RREADY [31])   
                    ,.AXI_31_RVALID  (AXI_00_RVALID [31])   
   `endif
   
    ,.m00_axi_awvalid      (   m00_axi_awvalid  )
    ,.m00_axi_awready      (   m00_axi_awready  )
    ,.m00_axi_awaddr       (   m00_axi_awaddr   )
    ,.m00_axi_awlen        (   m00_axi_awlen    )
    ,.m00_axi_wvalid       (   m00_axi_wvalid   )
    ,.m00_axi_wready       (   m00_axi_wready   )
    ,.m00_axi_wdata        (   m00_axi_wdata    )
    ,.m00_axi_wstrb        (   m00_axi_wstrb    )
    ,.m00_axi_wlast        (   m00_axi_wlast    )
    ,.m00_axi_bvalid       (   m00_axi_bvalid   )
    ,.m00_axi_bready       (   m00_axi_bready   )
    ,.m00_axi_arvalid      (   m00_axi_arvalid  )
    ,.m00_axi_arready      (   m00_axi_arready  )
    ,.m00_axi_araddr       (   m00_axi_araddr   )
    ,.m00_axi_arlen        (   m00_axi_arlen    )
    ,.m00_axi_rvalid       (   m00_axi_rvalid   )
    ,.m00_axi_rready       (   m00_axi_rready   )
    ,.m00_axi_rdata        (   m00_axi_rdata    )
    ,.m00_axi_rlast        (   m00_axi_rlast    )
    
    `ifdef XBARorM1orM0
    ,.m01_axi_awvalid      (   m01_axi_awvalid  )
    ,.m01_axi_awready      (   m01_axi_awready  )
    ,.m01_axi_awaddr       (   m01_axi_awaddr   )
    ,.m01_axi_awlen        (   m01_axi_awlen    )
    ,.m01_axi_wvalid       (   m01_axi_wvalid   )
    ,.m01_axi_wready       (   m01_axi_wready   )
    ,.m01_axi_wdata        (   m01_axi_wdata    )
    ,.m01_axi_wstrb        (   m01_axi_wstrb    )
    ,.m01_axi_wlast        (   m01_axi_wlast    )
    ,.m01_axi_bvalid       (   m01_axi_bvalid   )
    ,.m01_axi_bready       (   m01_axi_bready   )
    ,.m01_axi_arvalid      (   m01_axi_arvalid  )
    ,.m01_axi_arready      (   m01_axi_arready  )
    ,.m01_axi_araddr       (   m01_axi_araddr   )
    ,.m01_axi_arlen        (   m01_axi_arlen    )
    ,.m01_axi_rvalid       (   m01_axi_rvalid   )
    ,.m01_axi_rready       (   m01_axi_rready   )
    ,.m01_axi_rdata        (   m01_axi_rdata    )
    ,.m01_axi_rlast        (   m01_axi_rlast    )
    
    ,.m02_axi_awvalid      (   m02_axi_awvalid  )
    ,.m02_axi_awready      (   m02_axi_awready  )
    ,.m02_axi_awaddr       (   m02_axi_awaddr   )
    ,.m02_axi_awlen        (   m02_axi_awlen    )
    ,.m02_axi_wvalid       (   m02_axi_wvalid   )
    ,.m02_axi_wready       (   m02_axi_wready   )
    ,.m02_axi_wdata        (   m02_axi_wdata    )
    ,.m02_axi_wstrb        (   m02_axi_wstrb    )
    ,.m02_axi_wlast        (   m02_axi_wlast    )
    ,.m02_axi_bvalid       (   m02_axi_bvalid   )
    ,.m02_axi_bready       (   m02_axi_bready   )
    ,.m02_axi_arvalid      (   m02_axi_arvalid  )
    ,.m02_axi_arready      (   m02_axi_arready  )
    ,.m02_axi_araddr       (   m02_axi_araddr   )
    ,.m02_axi_arlen        (   m02_axi_arlen    )
    ,.m02_axi_rvalid       (   m02_axi_rvalid   )
    ,.m02_axi_rready       (   m02_axi_rready   )
    ,.m02_axi_rdata        (   m02_axi_rdata    )
    ,.m02_axi_rlast        (   m02_axi_rlast    )
    
    ,.m03_axi_awvalid      (   m03_axi_awvalid  )
    ,.m03_axi_awready      (   m03_axi_awready  )
    ,.m03_axi_awaddr       (   m03_axi_awaddr   )
    ,.m03_axi_awlen        (   m03_axi_awlen    )
    ,.m03_axi_wvalid       (   m03_axi_wvalid   )
    ,.m03_axi_wready       (   m03_axi_wready   )
    ,.m03_axi_wdata        (   m03_axi_wdata    )
    ,.m03_axi_wstrb        (   m03_axi_wstrb    )
    ,.m03_axi_wlast        (   m03_axi_wlast    )
    ,.m03_axi_bvalid       (   m03_axi_bvalid   )
    ,.m03_axi_bready       (   m03_axi_bready   )
    ,.m03_axi_arvalid      (   m03_axi_arvalid  )
    ,.m03_axi_arready      (   m03_axi_arready  )
    ,.m03_axi_araddr       (   m03_axi_araddr   )
    ,.m03_axi_arlen        (   m03_axi_arlen    )
    ,.m03_axi_rvalid       (   m03_axi_rvalid   )
    ,.m03_axi_rready       (   m03_axi_rready   )
    ,.m03_axi_rdata        (   m03_axi_rdata    )
    ,.m03_axi_rlast        (   m03_axi_rlast    )
    `endif
    `ifdef XBARorM1
    ,.m04_axi_awvalid      ( m04_axi_awvalid   )
    ,.m04_axi_awready      ( m04_axi_awready   )
    ,.m04_axi_awaddr       ( m04_axi_awaddr    )
    ,.m04_axi_awlen        ( m04_axi_awlen     )
    ,.m04_axi_wvalid       ( m04_axi_wvalid    )
    ,.m04_axi_wready       ( m04_axi_wready    )
    ,.m04_axi_wdata        ( m04_axi_wdata     )
    ,.m04_axi_wstrb        ( m04_axi_wstrb     )
    ,.m04_axi_wlast        ( m04_axi_wlast     )
    ,.m04_axi_bvalid       ( m04_axi_bvalid    )
    ,.m04_axi_bready       ( m04_axi_bready    )
    ,.m04_axi_arvalid      ( m04_axi_arvalid   )
    ,.m04_axi_arready      ( m04_axi_arready   )
    ,.m04_axi_araddr       ( m04_axi_araddr    )
    ,.m04_axi_arlen        ( m04_axi_arlen     )
    ,.m04_axi_rvalid       ( m04_axi_rvalid    )
    ,.m04_axi_rready       ( m04_axi_rready    )
    ,.m04_axi_rdata        ( m04_axi_rdata     )
    ,.m04_axi_rlast        ( m04_axi_rlast     )
    
    ,.m05_axi_awvalid      ( m05_axi_awvalid   ) 
    ,.m05_axi_awready      ( m05_axi_awready   ) 
    ,.m05_axi_awaddr       ( m05_axi_awaddr    ) 
    ,.m05_axi_awlen        ( m05_axi_awlen     ) 
    ,.m05_axi_wvalid       ( m05_axi_wvalid    ) 
    ,.m05_axi_wready       ( m05_axi_wready    ) 
    ,.m05_axi_wdata        ( m05_axi_wdata     ) 
    ,.m05_axi_wstrb        ( m05_axi_wstrb     ) 
    ,.m05_axi_wlast        ( m05_axi_wlast     ) 
    ,.m05_axi_bvalid       ( m05_axi_bvalid    ) 
    ,.m05_axi_bready       ( m05_axi_bready    ) 
    ,.m05_axi_arvalid      ( m05_axi_arvalid   ) 
    ,.m05_axi_arready      ( m05_axi_arready   ) 
    ,.m05_axi_araddr       ( m05_axi_araddr    ) 
    ,.m05_axi_arlen        ( m05_axi_arlen     ) 
    ,.m05_axi_rvalid       ( m05_axi_rvalid    ) 
    ,.m05_axi_rready       ( m05_axi_rready    ) 
    ,.m05_axi_rdata        ( m05_axi_rdata     ) 
    ,.m05_axi_rlast        ( m05_axi_rlast     ) 
        
    ,.m06_axi_awvalid      ( m06_axi_awvalid   ) 
    ,.m06_axi_awready      ( m06_axi_awready   ) 
    ,.m06_axi_awaddr       ( m06_axi_awaddr    ) 
    ,.m06_axi_awlen        ( m06_axi_awlen     ) 
    ,.m06_axi_wvalid       ( m06_axi_wvalid    ) 
    ,.m06_axi_wready       ( m06_axi_wready    ) 
    ,.m06_axi_wdata        ( m06_axi_wdata     ) 
    ,.m06_axi_wstrb        ( m06_axi_wstrb     ) 
    ,.m06_axi_wlast        ( m06_axi_wlast     ) 
    ,.m06_axi_bvalid       ( m06_axi_bvalid    ) 
    ,.m06_axi_bready       ( m06_axi_bready    ) 
    ,.m06_axi_arvalid      ( m06_axi_arvalid   ) 
    ,.m06_axi_arready      ( m06_axi_arready   ) 
    ,.m06_axi_araddr       ( m06_axi_araddr    ) 
    ,.m06_axi_arlen        ( m06_axi_arlen     ) 
    ,.m06_axi_rvalid       ( m06_axi_rvalid    ) 
    ,.m06_axi_rready       ( m06_axi_rready    ) 
    ,.m06_axi_rdata        ( m06_axi_rdata     ) 
    ,.m06_axi_rlast        ( m06_axi_rlast     ) 
        
    ,.m07_axi_awvalid      ( m07_axi_awvalid   ) 
    ,.m07_axi_awready      ( m07_axi_awready   ) 
    ,.m07_axi_awaddr       ( m07_axi_awaddr    ) 
    ,.m07_axi_awlen        ( m07_axi_awlen     ) 
    ,.m07_axi_wvalid       ( m07_axi_wvalid    ) 
    ,.m07_axi_wready       ( m07_axi_wready    ) 
    ,.m07_axi_wdata        ( m07_axi_wdata     ) 
    ,.m07_axi_wstrb        ( m07_axi_wstrb     ) 
    ,.m07_axi_wlast        ( m07_axi_wlast     ) 
    ,.m07_axi_bvalid       ( m07_axi_bvalid    ) 
    ,.m07_axi_bready       ( m07_axi_bready    ) 
    ,.m07_axi_arvalid      ( m07_axi_arvalid   ) 
    ,.m07_axi_arready      ( m07_axi_arready   ) 
    ,.m07_axi_araddr       ( m07_axi_araddr    ) 
    ,.m07_axi_arlen        ( m07_axi_arlen     ) 
    ,.m07_axi_rvalid       ( m07_axi_rvalid    ) 
    ,.m07_axi_rready       ( m07_axi_rready    ) 
    ,.m07_axi_rdata        ( m07_axi_rdata     ) 
    ,.m07_axi_rlast        ( m07_axi_rlast     ) 
       `endif
       `ifdef XBARorM0
    ,.m08_axi_awvalid      ( m08_axi_awvalid   )
    ,.m08_axi_awready      ( m08_axi_awready   )
    ,.m08_axi_awaddr       ( m08_axi_awaddr    )
    ,.m08_axi_awlen        ( m08_axi_awlen     )
    ,.m08_axi_wvalid       ( m08_axi_wvalid    )
    ,.m08_axi_wready       ( m08_axi_wready    )
    ,.m08_axi_wdata        ( m08_axi_wdata     )
    ,.m08_axi_wstrb        ( m08_axi_wstrb     )
    ,.m08_axi_wlast        ( m08_axi_wlast     )
    ,.m08_axi_bvalid       ( m08_axi_bvalid    )
    ,.m08_axi_bready       ( m08_axi_bready    )
    ,.m08_axi_arvalid      ( m08_axi_arvalid   )
    ,.m08_axi_arready      ( m08_axi_arready   )
    ,.m08_axi_araddr       ( m08_axi_araddr    )
    ,.m08_axi_arlen        ( m08_axi_arlen     )
    ,.m08_axi_rvalid       ( m08_axi_rvalid    )
    ,.m08_axi_rready       ( m08_axi_rready    )
    ,.m08_axi_rdata        ( m08_axi_rdata     )
    ,.m08_axi_rlast        ( m08_axi_rlast     )
    
    ,.m09_axi_awvalid      ( m09_axi_awvalid   ) 
    ,.m09_axi_awready      ( m09_axi_awready   ) 
    ,.m09_axi_awaddr       ( m09_axi_awaddr    ) 
    ,.m09_axi_awlen        ( m09_axi_awlen     ) 
    ,.m09_axi_wvalid       ( m09_axi_wvalid    ) 
    ,.m09_axi_wready       ( m09_axi_wready    ) 
    ,.m09_axi_wdata        ( m09_axi_wdata     ) 
    ,.m09_axi_wstrb        ( m09_axi_wstrb     ) 
    ,.m09_axi_wlast        ( m09_axi_wlast     ) 
    ,.m09_axi_bvalid       ( m09_axi_bvalid    ) 
    ,.m09_axi_bready       ( m09_axi_bready    ) 
    ,.m09_axi_arvalid      ( m09_axi_arvalid   ) 
    ,.m09_axi_arready      ( m09_axi_arready   ) 
    ,.m09_axi_araddr       ( m09_axi_araddr    ) 
    ,.m09_axi_arlen        ( m09_axi_arlen     ) 
    ,.m09_axi_rvalid       ( m09_axi_rvalid    ) 
    ,.m09_axi_rready       ( m09_axi_rready    ) 
    ,.m09_axi_rdata        ( m09_axi_rdata     ) 
    ,.m09_axi_rlast        ( m09_axi_rlast     ) 
        
    ,.m10_axi_awvalid      ( m10_axi_awvalid   ) 
    ,.m10_axi_awready      ( m10_axi_awready   ) 
    ,.m10_axi_awaddr       ( m10_axi_awaddr    ) 
    ,.m10_axi_awlen        ( m10_axi_awlen     ) 
    ,.m10_axi_wvalid       ( m10_axi_wvalid    ) 
    ,.m10_axi_wready       ( m10_axi_wready    ) 
    ,.m10_axi_wdata        ( m10_axi_wdata     ) 
    ,.m10_axi_wstrb        ( m10_axi_wstrb     ) 
    ,.m10_axi_wlast        ( m10_axi_wlast     ) 
    ,.m10_axi_bvalid       ( m10_axi_bvalid    ) 
    ,.m10_axi_bready       ( m10_axi_bready    ) 
    ,.m10_axi_arvalid      ( m10_axi_arvalid   ) 
    ,.m10_axi_arready      ( m10_axi_arready   ) 
    ,.m10_axi_araddr       ( m10_axi_araddr    ) 
    ,.m10_axi_arlen        ( m10_axi_arlen     ) 
    ,.m10_axi_rvalid       ( m10_axi_rvalid    ) 
    ,.m10_axi_rready       ( m10_axi_rready    ) 
    ,.m10_axi_rdata        ( m10_axi_rdata     ) 
    ,.m10_axi_rlast        ( m10_axi_rlast     ) 
        
    ,.m11_axi_awvalid      ( m11_axi_awvalid   ) 
    ,.m11_axi_awready      ( m11_axi_awready   ) 
    ,.m11_axi_awaddr       ( m11_axi_awaddr    ) 
    ,.m11_axi_awlen        ( m11_axi_awlen     ) 
    ,.m11_axi_wvalid       ( m11_axi_wvalid    ) 
    ,.m11_axi_wready       ( m11_axi_wready    ) 
    ,.m11_axi_wdata        ( m11_axi_wdata     ) 
    ,.m11_axi_wstrb        ( m11_axi_wstrb     ) 
    ,.m11_axi_wlast        ( m11_axi_wlast     ) 
    ,.m11_axi_bvalid       ( m11_axi_bvalid    ) 
    ,.m11_axi_bready       ( m11_axi_bready    ) 
    ,.m11_axi_arvalid      ( m11_axi_arvalid   ) 
    ,.m11_axi_arready      ( m11_axi_arready   ) 
    ,.m11_axi_araddr       ( m11_axi_araddr    ) 
    ,.m11_axi_arlen        ( m11_axi_arlen     ) 
    ,.m11_axi_rvalid       ( m11_axi_rvalid    ) 
    ,.m11_axi_rready       ( m11_axi_rready    ) 
    ,.m11_axi_rdata        ( m11_axi_rdata     ) 
    ,.m11_axi_rlast        ( m11_axi_rlast     ) 
       `endif
 `ifdef XBAR
    ,.m12_axi_awvalid      ( m12_axi_awvalid   )
    ,.m12_axi_awready      ( m12_axi_awready   )
    ,.m12_axi_awaddr       ( m12_axi_awaddr    )
    ,.m12_axi_awlen        ( m12_axi_awlen     )
    ,.m12_axi_wvalid       ( m12_axi_wvalid    )
    ,.m12_axi_wready       ( m12_axi_wready    )
    ,.m12_axi_wdata        ( m12_axi_wdata     )
    ,.m12_axi_wstrb        ( m12_axi_wstrb     )
    ,.m12_axi_wlast        ( m12_axi_wlast     )
    ,.m12_axi_bvalid       ( m12_axi_bvalid    )
    ,.m12_axi_bready       ( m12_axi_bready    )
    ,.m12_axi_arvalid      ( m12_axi_arvalid   )
    ,.m12_axi_arready      ( m12_axi_arready   )
    ,.m12_axi_araddr       ( m12_axi_araddr    )
    ,.m12_axi_arlen        ( m12_axi_arlen     )
    ,.m12_axi_rvalid       ( m12_axi_rvalid    )
    ,.m12_axi_rready       ( m12_axi_rready    )
    ,.m12_axi_rdata        ( m12_axi_rdata     )
    ,.m12_axi_rlast        ( m12_axi_rlast     )
    
    ,.m13_axi_awvalid      ( m13_axi_awvalid   ) 
    ,.m13_axi_awready      ( m13_axi_awready   ) 
    ,.m13_axi_awaddr       ( m13_axi_awaddr    ) 
    ,.m13_axi_awlen        ( m13_axi_awlen     ) 
    ,.m13_axi_wvalid       ( m13_axi_wvalid    ) 
    ,.m13_axi_wready       ( m13_axi_wready    ) 
    ,.m13_axi_wdata        ( m13_axi_wdata     ) 
    ,.m13_axi_wstrb        ( m13_axi_wstrb     ) 
    ,.m13_axi_wlast        ( m13_axi_wlast     ) 
    ,.m13_axi_bvalid       ( m13_axi_bvalid    ) 
    ,.m13_axi_bready       ( m13_axi_bready    ) 
    ,.m13_axi_arvalid      ( m13_axi_arvalid   ) 
    ,.m13_axi_arready      ( m13_axi_arready   ) 
    ,.m13_axi_araddr       ( m13_axi_araddr    ) 
    ,.m13_axi_arlen        ( m13_axi_arlen     ) 
    ,.m13_axi_rvalid       ( m13_axi_rvalid    ) 
    ,.m13_axi_rready       ( m13_axi_rready    ) 
    ,.m13_axi_rdata        ( m13_axi_rdata     ) 
    ,.m13_axi_rlast        ( m13_axi_rlast     ) 
        
    ,.m14_axi_awvalid      ( m14_axi_awvalid   ) 
    ,.m14_axi_awready      ( m14_axi_awready   ) 
    ,.m14_axi_awaddr       ( m14_axi_awaddr    ) 
    ,.m14_axi_awlen        ( m14_axi_awlen     ) 
    ,.m14_axi_wvalid       ( m14_axi_wvalid    ) 
    ,.m14_axi_wready       ( m14_axi_wready    ) 
    ,.m14_axi_wdata        ( m14_axi_wdata     ) 
    ,.m14_axi_wstrb        ( m14_axi_wstrb     ) 
    ,.m14_axi_wlast        ( m14_axi_wlast     ) 
    ,.m14_axi_bvalid       ( m14_axi_bvalid    ) 
    ,.m14_axi_bready       ( m14_axi_bready    ) 
    ,.m14_axi_arvalid      ( m14_axi_arvalid   ) 
    ,.m14_axi_arready      ( m14_axi_arready   ) 
    ,.m14_axi_araddr       ( m14_axi_araddr    ) 
    ,.m14_axi_arlen        ( m14_axi_arlen     ) 
    ,.m14_axi_rvalid       ( m14_axi_rvalid    ) 
    ,.m14_axi_rready       ( m14_axi_rready    ) 
    ,.m14_axi_rdata        ( m14_axi_rdata     ) 
    ,.m14_axi_rlast        ( m14_axi_rlast     ) 
        
    ,.m15_axi_awvalid      ( m15_axi_awvalid   ) 
    ,.m15_axi_awready      ( m15_axi_awready   ) 
    ,.m15_axi_awaddr       ( m15_axi_awaddr    ) 
    ,.m15_axi_awlen        ( m15_axi_awlen     ) 
    ,.m15_axi_wvalid       ( m15_axi_wvalid    ) 
    ,.m15_axi_wready       ( m15_axi_wready    ) 
    ,.m15_axi_wdata        ( m15_axi_wdata     ) 
    ,.m15_axi_wstrb        ( m15_axi_wstrb     ) 
    ,.m15_axi_wlast        ( m15_axi_wlast     ) 
    ,.m15_axi_bvalid       ( m15_axi_bvalid    ) 
    ,.m15_axi_bready       ( m15_axi_bready    ) 
    ,.m15_axi_arvalid      ( m15_axi_arvalid   ) 
    ,.m15_axi_arready      ( m15_axi_arready   ) 
    ,.m15_axi_araddr       ( m15_axi_araddr    ) 
    ,.m15_axi_arlen        ( m15_axi_arlen     ) 
    ,.m15_axi_rvalid       ( m15_axi_rvalid    ) 
    ,.m15_axi_rready       ( m15_axi_rready    ) 
    ,.m15_axi_rdata        ( m15_axi_rdata     ) 
    ,.m15_axi_rlast        ( m15_axi_rlast     ) 
    ,.m16_axi_awvalid ( m16_axi_awvalid )              
    ,.m16_axi_awready ( m16_axi_awready )              
    ,.m16_axi_awaddr  ( m16_axi_awaddr  )              
    ,.m16_axi_awlen   ( m16_axi_awlen   )              
    ,.m16_axi_wvalid  ( m16_axi_wvalid  )              
    ,.m16_axi_wready  ( m16_axi_wready  )              
    ,.m16_axi_wdata   ( m16_axi_wdata   )              
    ,.m16_axi_wstrb   ( m16_axi_wstrb   )              
    ,.m16_axi_wlast   ( m16_axi_wlast   )              
    ,.m16_axi_bvalid  ( m16_axi_bvalid  )              
    ,.m16_axi_bready  ( m16_axi_bready  )              
    ,.m16_axi_arvalid ( m16_axi_arvalid )              
    ,.m16_axi_arready ( m16_axi_arready )              
    ,.m16_axi_araddr  ( m16_axi_araddr  )              
    ,.m16_axi_arlen   ( m16_axi_arlen   )              
    ,.m16_axi_rvalid  ( m16_axi_rvalid  )              
    ,.m16_axi_rready  ( m16_axi_rready  )              
    ,.m16_axi_rdata   ( m16_axi_rdata   )              
    ,.m16_axi_rlast   ( m16_axi_rlast   )              
    ,.m17_axi_awvalid ( m17_axi_awvalid )              
    ,.m17_axi_awready ( m17_axi_awready )              
    ,.m17_axi_awaddr  ( m17_axi_awaddr  )              
    ,.m17_axi_awlen   ( m17_axi_awlen   )              
    ,.m17_axi_wvalid  ( m17_axi_wvalid  )              
    ,.m17_axi_wready  ( m17_axi_wready  )              
    ,.m17_axi_wdata   ( m17_axi_wdata   )              
    ,.m17_axi_wstrb   ( m17_axi_wstrb   )              
    ,.m17_axi_wlast   ( m17_axi_wlast   )              
    ,.m17_axi_bvalid  ( m17_axi_bvalid  )              
    ,.m17_axi_bready  ( m17_axi_bready  )              
    ,.m17_axi_arvalid ( m17_axi_arvalid )              
    ,.m17_axi_arready ( m17_axi_arready )              
    ,.m17_axi_araddr  ( m17_axi_araddr  )              
    ,.m17_axi_arlen   ( m17_axi_arlen   )              
    ,.m17_axi_rvalid  ( m17_axi_rvalid  )              
    ,.m17_axi_rready  ( m17_axi_rready  )              
    ,.m17_axi_rdata   ( m17_axi_rdata   )              
    ,.m17_axi_rlast   ( m17_axi_rlast   )              
    ,.m18_axi_awvalid ( m18_axi_awvalid )              
    ,.m18_axi_awready ( m18_axi_awready )              
    ,.m18_axi_awaddr  ( m18_axi_awaddr  )              
    ,.m18_axi_awlen   ( m18_axi_awlen   )              
    ,.m18_axi_wvalid  ( m18_axi_wvalid  )              
    ,.m18_axi_wready  ( m18_axi_wready  )              
    ,.m18_axi_wdata   ( m18_axi_wdata   )              
    ,.m18_axi_wstrb   ( m18_axi_wstrb   )              
    ,.m18_axi_wlast   ( m18_axi_wlast   )              
    ,.m18_axi_bvalid  ( m18_axi_bvalid  )              
    ,.m18_axi_bready  ( m18_axi_bready  )              
    ,.m18_axi_arvalid ( m18_axi_arvalid )              
    ,.m18_axi_arready ( m18_axi_arready )              
    ,.m18_axi_araddr  ( m18_axi_araddr  )              
    ,.m18_axi_arlen   ( m18_axi_arlen   )              
    ,.m18_axi_rvalid  ( m18_axi_rvalid  )              
    ,.m18_axi_rready  ( m18_axi_rready  )              
    ,.m18_axi_rdata   ( m18_axi_rdata   )              
    ,.m18_axi_rlast   ( m18_axi_rlast   )              
    ,.m19_axi_awvalid ( m19_axi_awvalid )              
    ,.m19_axi_awready ( m19_axi_awready )              
    ,.m19_axi_awaddr  ( m19_axi_awaddr  )              
    ,.m19_axi_awlen   ( m19_axi_awlen   )              
    ,.m19_axi_wvalid  ( m19_axi_wvalid  )              
    ,.m19_axi_wready  ( m19_axi_wready  )              
    ,.m19_axi_wdata   ( m19_axi_wdata   )              
    ,.m19_axi_wstrb   ( m19_axi_wstrb   )              
    ,.m19_axi_wlast   ( m19_axi_wlast   )              
    ,.m19_axi_bvalid  ( m19_axi_bvalid  )              
    ,.m19_axi_bready  ( m19_axi_bready  )              
    ,.m19_axi_arvalid ( m19_axi_arvalid )              
    ,.m19_axi_arready ( m19_axi_arready )              
    ,.m19_axi_araddr  ( m19_axi_araddr  )              
    ,.m19_axi_arlen   ( m19_axi_arlen   )              
    ,.m19_axi_rvalid  ( m19_axi_rvalid  )              
    ,.m19_axi_rready  ( m19_axi_rready  )              
    ,.m19_axi_rdata   ( m19_axi_rdata   )              
    ,.m19_axi_rlast   ( m19_axi_rlast   )              
    ,.m20_axi_awvalid ( m20_axi_awvalid )              
    ,.m20_axi_awready ( m20_axi_awready )              
    ,.m20_axi_awaddr  ( m20_axi_awaddr  )              
    ,.m20_axi_awlen   ( m20_axi_awlen   )              
    ,.m20_axi_wvalid  ( m20_axi_wvalid  )              
    ,.m20_axi_wready  ( m20_axi_wready  )              
    ,.m20_axi_wdata   ( m20_axi_wdata   )              
    ,.m20_axi_wstrb   ( m20_axi_wstrb   )              
    ,.m20_axi_wlast   ( m20_axi_wlast   )              
    ,.m20_axi_bvalid  ( m20_axi_bvalid  )              
    ,.m20_axi_bready  ( m20_axi_bready  )              
    ,.m20_axi_arvalid ( m20_axi_arvalid )              
    ,.m20_axi_arready ( m20_axi_arready )              
    ,.m20_axi_araddr  ( m20_axi_araddr  )              
    ,.m20_axi_arlen   ( m20_axi_arlen   )              
    ,.m20_axi_rvalid  ( m20_axi_rvalid  )              
    ,.m20_axi_rready  ( m20_axi_rready  )              
    ,.m20_axi_rdata   ( m20_axi_rdata   )              
    ,.m20_axi_rlast   ( m20_axi_rlast   )              
    ,.m21_axi_awvalid ( m21_axi_awvalid )              
    ,.m21_axi_awready ( m21_axi_awready )              
    ,.m21_axi_awaddr  ( m21_axi_awaddr  )              
    ,.m21_axi_awlen   ( m21_axi_awlen   )              
    ,.m21_axi_wvalid  ( m21_axi_wvalid  )              
    ,.m21_axi_wready  ( m21_axi_wready  )              
    ,.m21_axi_wdata   ( m21_axi_wdata   )              
    ,.m21_axi_wstrb   ( m21_axi_wstrb   )              
    ,.m21_axi_wlast   ( m21_axi_wlast   )              
    ,.m21_axi_bvalid  ( m21_axi_bvalid  )              
    ,.m21_axi_bready  ( m21_axi_bready  )              
    ,.m21_axi_arvalid ( m21_axi_arvalid )              
    ,.m21_axi_arready ( m21_axi_arready )              
    ,.m21_axi_araddr  ( m21_axi_araddr  )              
    ,.m21_axi_arlen   ( m21_axi_arlen   )              
    ,.m21_axi_rvalid  ( m21_axi_rvalid  )              
    ,.m21_axi_rready  ( m21_axi_rready  )              
    ,.m21_axi_rdata   ( m21_axi_rdata   )              
    ,.m21_axi_rlast   ( m21_axi_rlast   )              
    ,.m22_axi_awvalid ( m22_axi_awvalid )              
    ,.m22_axi_awready ( m22_axi_awready )              
    ,.m22_axi_awaddr  ( m22_axi_awaddr  )              
    ,.m22_axi_awlen   ( m22_axi_awlen   )              
    ,.m22_axi_wvalid  ( m22_axi_wvalid  )              
    ,.m22_axi_wready  ( m22_axi_wready  )              
    ,.m22_axi_wdata   ( m22_axi_wdata   )              
    ,.m22_axi_wstrb   ( m22_axi_wstrb   )              
    ,.m22_axi_wlast   ( m22_axi_wlast   )              
    ,.m22_axi_bvalid  ( m22_axi_bvalid  )              
    ,.m22_axi_bready  ( m22_axi_bready  )              
    ,.m22_axi_arvalid ( m22_axi_arvalid )              
    ,.m22_axi_arready ( m22_axi_arready )              
    ,.m22_axi_araddr  ( m22_axi_araddr  )              
    ,.m22_axi_arlen   ( m22_axi_arlen   )              
    ,.m22_axi_rvalid  ( m22_axi_rvalid  )              
    ,.m22_axi_rready  ( m22_axi_rready  )              
    ,.m22_axi_rdata   ( m22_axi_rdata   )              
    ,.m22_axi_rlast   ( m22_axi_rlast   )              
    ,.m23_axi_awvalid ( m23_axi_awvalid )              
    ,.m23_axi_awready ( m23_axi_awready )              
    ,.m23_axi_awaddr  ( m23_axi_awaddr  )              
    ,.m23_axi_awlen   ( m23_axi_awlen   )              
    ,.m23_axi_wvalid  ( m23_axi_wvalid  )              
    ,.m23_axi_wready  ( m23_axi_wready  )              
    ,.m23_axi_wdata   ( m23_axi_wdata   )              
    ,.m23_axi_wstrb   ( m23_axi_wstrb   )              
    ,.m23_axi_wlast   ( m23_axi_wlast   )              
    ,.m23_axi_bvalid  ( m23_axi_bvalid  )              
    ,.m23_axi_bready  ( m23_axi_bready  )              
    ,.m23_axi_arvalid ( m23_axi_arvalid )              
    ,.m23_axi_arready ( m23_axi_arready )              
    ,.m23_axi_araddr  ( m23_axi_araddr  )              
    ,.m23_axi_arlen   ( m23_axi_arlen   )              
    ,.m23_axi_rvalid  ( m23_axi_rvalid  )              
    ,.m23_axi_rready  ( m23_axi_rready  )              
    ,.m23_axi_rdata   ( m23_axi_rdata   )              
    ,.m23_axi_rlast   ( m23_axi_rlast   )              
    ,.m24_axi_awvalid ( m24_axi_awvalid )              
    ,.m24_axi_awready ( m24_axi_awready )              
    ,.m24_axi_awaddr  ( m24_axi_awaddr  )              
    ,.m24_axi_awlen   ( m24_axi_awlen   )              
    ,.m24_axi_wvalid  ( m24_axi_wvalid  )              
    ,.m24_axi_wready  ( m24_axi_wready  )              
    ,.m24_axi_wdata   ( m24_axi_wdata   )              
    ,.m24_axi_wstrb   ( m24_axi_wstrb   )              
    ,.m24_axi_wlast   ( m24_axi_wlast   )              
    ,.m24_axi_bvalid  ( m24_axi_bvalid  )              
    ,.m24_axi_bready  ( m24_axi_bready  )              
    ,.m24_axi_arvalid ( m24_axi_arvalid )              
    ,.m24_axi_arready ( m24_axi_arready )              
    ,.m24_axi_araddr  ( m24_axi_araddr  )              
    ,.m24_axi_arlen   ( m24_axi_arlen   )              
    ,.m24_axi_rvalid  ( m24_axi_rvalid  )              
    ,.m24_axi_rready  ( m24_axi_rready  )              
    ,.m24_axi_rdata   ( m24_axi_rdata   )              
    ,.m24_axi_rlast   ( m24_axi_rlast   )              
    ,.m25_axi_awvalid ( m25_axi_awvalid )              
    ,.m25_axi_awready ( m25_axi_awready )              
    ,.m25_axi_awaddr  ( m25_axi_awaddr  )              
    ,.m25_axi_awlen   ( m25_axi_awlen   )              
    ,.m25_axi_wvalid  ( m25_axi_wvalid  )              
    ,.m25_axi_wready  ( m25_axi_wready  )              
    ,.m25_axi_wdata   ( m25_axi_wdata   )              
    ,.m25_axi_wstrb   ( m25_axi_wstrb   )              
    ,.m25_axi_wlast   ( m25_axi_wlast   )              
    ,.m25_axi_bvalid  ( m25_axi_bvalid  )              
    ,.m25_axi_bready  ( m25_axi_bready  )              
    ,.m25_axi_arvalid ( m25_axi_arvalid )              
    ,.m25_axi_arready ( m25_axi_arready )              
    ,.m25_axi_araddr  ( m25_axi_araddr  )              
    ,.m25_axi_arlen   ( m25_axi_arlen   )              
    ,.m25_axi_rvalid  ( m25_axi_rvalid  )              
    ,.m25_axi_rready  ( m25_axi_rready  )              
    ,.m25_axi_rdata   ( m25_axi_rdata   )              
    ,.m25_axi_rlast   ( m25_axi_rlast   )              
    ,.m26_axi_awvalid ( m26_axi_awvalid )              
    ,.m26_axi_awready ( m26_axi_awready )              
    ,.m26_axi_awaddr  ( m26_axi_awaddr  )              
    ,.m26_axi_awlen   ( m26_axi_awlen   )              
    ,.m26_axi_wvalid  ( m26_axi_wvalid  )              
    ,.m26_axi_wready  ( m26_axi_wready  )              
    ,.m26_axi_wdata   ( m26_axi_wdata   )              
    ,.m26_axi_wstrb   ( m26_axi_wstrb   )              
    ,.m26_axi_wlast   ( m26_axi_wlast   )              
    ,.m26_axi_bvalid  ( m26_axi_bvalid  )              
    ,.m26_axi_bready  ( m26_axi_bready  )              
    ,.m26_axi_arvalid ( m26_axi_arvalid )              
    ,.m26_axi_arready ( m26_axi_arready )              
    ,.m26_axi_araddr  ( m26_axi_araddr  )              
    ,.m26_axi_arlen   ( m26_axi_arlen   )              
    ,.m26_axi_rvalid  ( m26_axi_rvalid  )              
    ,.m26_axi_rready  ( m26_axi_rready  )              
    ,.m26_axi_rdata   ( m26_axi_rdata   )              
    ,.m26_axi_rlast   ( m26_axi_rlast   )              
    ,.m27_axi_awvalid ( m27_axi_awvalid )              
    ,.m27_axi_awready ( m27_axi_awready )              
    ,.m27_axi_awaddr  ( m27_axi_awaddr  )              
    ,.m27_axi_awlen   ( m27_axi_awlen   )              
    ,.m27_axi_wvalid  ( m27_axi_wvalid  )              
    ,.m27_axi_wready  ( m27_axi_wready  )              
    ,.m27_axi_wdata   ( m27_axi_wdata   )              
    ,.m27_axi_wstrb   ( m27_axi_wstrb   )              
    ,.m27_axi_wlast   ( m27_axi_wlast   )              
    ,.m27_axi_bvalid  ( m27_axi_bvalid  )              
    ,.m27_axi_bready  ( m27_axi_bready  )              
    ,.m27_axi_arvalid ( m27_axi_arvalid )              
    ,.m27_axi_arready ( m27_axi_arready )              
    ,.m27_axi_araddr  ( m27_axi_araddr  )              
    ,.m27_axi_arlen   ( m27_axi_arlen   )              
    ,.m27_axi_rvalid  ( m27_axi_rvalid  )              
    ,.m27_axi_rready  ( m27_axi_rready  )              
    ,.m27_axi_rdata   ( m27_axi_rdata   )              
    ,.m27_axi_rlast   ( m27_axi_rlast   )              
    ,.m28_axi_awvalid ( m28_axi_awvalid )              
    ,.m28_axi_awready ( m28_axi_awready )              
    ,.m28_axi_awaddr  ( m28_axi_awaddr  )              
    ,.m28_axi_awlen   ( m28_axi_awlen   )              
    ,.m28_axi_wvalid  ( m28_axi_wvalid  )              
    ,.m28_axi_wready  ( m28_axi_wready  )              
    ,.m28_axi_wdata   ( m28_axi_wdata   )              
    ,.m28_axi_wstrb   ( m28_axi_wstrb   )              
    ,.m28_axi_wlast   ( m28_axi_wlast   )              
    ,.m28_axi_bvalid  ( m28_axi_bvalid  )              
    ,.m28_axi_bready  ( m28_axi_bready  )              
    ,.m28_axi_arvalid ( m28_axi_arvalid )              
    ,.m28_axi_arready ( m28_axi_arready )              
    ,.m28_axi_araddr  ( m28_axi_araddr  )              
    ,.m28_axi_arlen   ( m28_axi_arlen   )              
    ,.m28_axi_rvalid  ( m28_axi_rvalid  )              
    ,.m28_axi_rready  ( m28_axi_rready  )              
    ,.m28_axi_rdata   ( m28_axi_rdata   )              
    ,.m28_axi_rlast   ( m28_axi_rlast   )              
    ,.m29_axi_awvalid ( m29_axi_awvalid )              
    ,.m29_axi_awready ( m29_axi_awready )              
    ,.m29_axi_awaddr  ( m29_axi_awaddr  )              
    ,.m29_axi_awlen   ( m29_axi_awlen   )              
    ,.m29_axi_wvalid  ( m29_axi_wvalid  )              
    ,.m29_axi_wready  ( m29_axi_wready  )              
    ,.m29_axi_wdata   ( m29_axi_wdata   )              
    ,.m29_axi_wstrb   ( m29_axi_wstrb   )              
    ,.m29_axi_wlast   ( m29_axi_wlast   )              
    ,.m29_axi_bvalid  ( m29_axi_bvalid  )              
    ,.m29_axi_bready  ( m29_axi_bready  )              
    ,.m29_axi_arvalid ( m29_axi_arvalid )              
    ,.m29_axi_arready ( m29_axi_arready )              
    ,.m29_axi_araddr  ( m29_axi_araddr  )              
    ,.m29_axi_arlen   ( m29_axi_arlen   )              
    ,.m29_axi_rvalid  ( m29_axi_rvalid  )              
    ,.m29_axi_rready  ( m29_axi_rready  )              
    ,.m29_axi_rdata   ( m29_axi_rdata   )              
    ,.m29_axi_rlast   ( m29_axi_rlast   )              
    ,.m30_axi_awvalid ( m30_axi_awvalid )              
    ,.m30_axi_awready ( m30_axi_awready )              
    ,.m30_axi_awaddr  ( m30_axi_awaddr  )              
    ,.m30_axi_awlen   ( m30_axi_awlen   )              
    ,.m30_axi_wvalid  ( m30_axi_wvalid  )              
    ,.m30_axi_wready  ( m30_axi_wready  )              
    ,.m30_axi_wdata   ( m30_axi_wdata   )              
    ,.m30_axi_wstrb   ( m30_axi_wstrb   )              
    ,.m30_axi_wlast   ( m30_axi_wlast   )              
    ,.m30_axi_bvalid  ( m30_axi_bvalid  )              
    ,.m30_axi_bready  ( m30_axi_bready  )              
    ,.m30_axi_arvalid ( m30_axi_arvalid )              
    ,.m30_axi_arready ( m30_axi_arready )              
    ,.m30_axi_araddr  ( m30_axi_araddr  )              
    ,.m30_axi_arlen   ( m30_axi_arlen   )              
    ,.m30_axi_rvalid  ( m30_axi_rvalid  )              
    ,.m30_axi_rready  ( m30_axi_rready  )              
    ,.m30_axi_rdata   ( m30_axi_rdata   )              
    ,.m30_axi_rlast   ( m30_axi_rlast   )              
    ,.m31_axi_awvalid ( m31_axi_awvalid )              
    ,.m31_axi_awready ( m31_axi_awready )              
    ,.m31_axi_awaddr  ( m31_axi_awaddr  )              
    ,.m31_axi_awlen   ( m31_axi_awlen   )              
    ,.m31_axi_wvalid  ( m31_axi_wvalid  )              
    ,.m31_axi_wready  ( m31_axi_wready  )              
    ,.m31_axi_wdata   ( m31_axi_wdata   )              
    ,.m31_axi_wstrb   ( m31_axi_wstrb   )              
    ,.m31_axi_wlast   ( m31_axi_wlast   )              
    ,.m31_axi_bvalid  ( m31_axi_bvalid  )              
    ,.m31_axi_bready  ( m31_axi_bready  )              
    ,.m31_axi_arvalid ( m31_axi_arvalid )              
    ,.m31_axi_arready ( m31_axi_arready )              
    ,.m31_axi_araddr  ( m31_axi_araddr  )              
    ,.m31_axi_arlen   ( m31_axi_arlen   )              
    ,.m31_axi_rvalid  ( m31_axi_rvalid  )              
    ,.m31_axi_rready  ( m31_axi_rready  )              
    ,.m31_axi_rdata   ( m31_axi_rdata   )              
    ,.m31_axi_rlast   ( m31_axi_rlast   )    
    
    `endif          
    ,.cmd(cmd_r)
);

wire [2*N-1:0] writeValid;
reg  [2*N-1:0] writeValid_lock = 0;

wire writeFifoL[N-1:0];
wire writeDataFifoL[N-1:0];
wire readFifoL[N-1:0];
wire readRespFifoL[N-1:0];
wire readRespReady[N-1:0];
generate for(ii=0;

         `ifdef TREE
            ii<1;
         `elsif MESH1    
            ii<N/4;
          `else
            ii < N/2;
          `endif  
            ii=ii+1)  begin 
    assign AXI_00_AWVALID  [2*ii]         =   fifoValid[ii];
    assign AXI_00_AWADDR   [2*ii]         =   fifoData[ii][`mem_addr];
    assign AXI_00_AWLEN    [2*ii]         =   fifoData[ii][`len];
    
    assign AXI_00_WVALID   [2*ii]         =   fifoWValid[ii];
    assign AXI_00_WDATA    [2*ii]         =   fifoWData[ii];
    assign AXI_00_WLAST    [2*ii]         =   writeDataFifoL[ii];
    
    assign grid_up_r[LEVELS-1][ii ]       =   fifoR[ii] & readFifoR[ii] & readRespFifoR[ii] & fifoDR[ii];
    
    assign AXI_00_AWVALID  [2*ii+1]        =   fifoValid[ii+(1<<(LEVELS-1))];
    assign AXI_00_AWADDR   [2*ii+1]        =   fifoData[ii+(1<<(LEVELS-1))][`mem_addr];
    assign AXI_00_AWLEN    [2*ii+1]        =   fifoData[ii+(1<<(LEVELS-1))][`len];
    
    assign AXI_00_WVALID   [2*ii+1]        =   fifoWValid[ii+(1<<(LEVELS-1))];
    assign AXI_00_WDATA    [2*ii+1]        =   fifoWData[ii+(1<<(LEVELS-1))];
    assign AXI_00_WLAST    [2*ii+1]         =   writeDataFifoL[ii+(1<<(LEVELS-1))];
     
    assign grid_up_r[LEVELS-1][ii+(1<<(LEVELS-1))]     =   fifoR[ii+(1<<(LEVELS-1))] & readFifoR[ii+(1<<(LEVELS-1))] & readRespFifoR[ii+(1<<(LEVELS-1))] & fifoDR[ii+(1<<(LEVELS-1))];
    
    assign AXI_00_ARADDR   [2*ii]                    = readFifoData[ii][`mem_addr];
    assign AXI_00_ARVALID  [2*ii]                    = readFifoValid[ii];
    assign AXI_00_ARLEN    [2*ii]                    = readFifoData[ii][`len];
    
//    if(ii!=15) begin
//    assign AXI_00_RREADY   [2*ii]                    = 1;//grid_dn_r[LEVELS-1][ii];
//    assign grid_dn[LEVELS-1][ii]          = 0;// ((AXI_00_RDATA[2*ii][`APW] << 32)>>32) | ((readRespFifoData[ii][`APW] & 32'HFFFF_FFFF)<<224);
//    end
//    assign grid_dn[LEVELS-1][ii][D_W-1:P_W]        = 0;
//    assign grid_dn[LEVELS-1][ii][A_W+D_W-1:D_W]    = 0;//readRespFifoData[ii][A_W+33-1:33]; // 33 = APW True payload width
//    assign grid_dn_v[LEVELS-1][ii]                 = 0;//AXI_00_RVALID        [2*ii];
//    assign grid_dn_l[LEVELS-1][ii]                 = 0;
    
    assign AXI_00_ARADDR   [2*ii+1]                    = readFifoData[ii+(1<<(LEVELS-1))][`mem_addr];
    assign AXI_00_ARVALID  [2*ii+1]                    = readFifoValid[ii+(1<<(LEVELS-1))];
    assign AXI_00_ARLEN    [2*ii+1]                    = readFifoData[ii+(1<<(LEVELS-1))][`len];
//    assign AXI_00_RREADY   [2*ii+1]                    = 1;//grid_dn_r[LEVELS-1][ii+(1<<(LEVELS-1))];

//    assign grid_dn[LEVELS-1][ii+(1<<(LEVELS-1))]         = 0;// ((AXI_00_RDATA[2*ii+1][`APW] << 32)>>32) | ((readRespFifoData[ii+(1<<(LEVELS-1))][`APW] & 32'HFFFF_FFFF)<<224);

//    assign grid_dn[LEVELS-1][ii+(1<<(LEVELS-1))][D_W-1:P_W]        = 0;
//    assign grid_dn[LEVELS-1][ii+(1<<(LEVELS-1))][A_W+D_W-1:D_W]    = 0;//readRespFifoData[ii+(1<<(LEVELS-1))][A_W+33-1:33];
//    assign grid_dn_v[LEVELS-1][ii+(1<<(LEVELS-1))]                 = 0;//AXI_00_RVALID        [2*ii+1];
//    assign grid_dn_l[LEVELS-1][ii+(1<<(LEVELS-1))]                 = 0;
    
end
endgenerate


integer j;

generate for(ii=0;

         `ifdef TREE
            ii<1;
         `elsif MESH1    
            ii<N/4;
          `else
            ii < N/2;
          `endif  
            ii=ii+1)  begin 
            
            frame_response#(.D_W(260),.M_A_W(M_A_W),.A_W(A_W),.P_W(P_W)  ) response_in(
    .clk(clk)
    ,.rst(rst)
    ,.header_data({0,0,readRespFifoData[ii]})
    ,.header_valid(readRespFifoValid[ii])
    ,.header_ready(readRespReady[2*ii])
    
    ,.read_data({AXI_00_RLAST[2*ii],3'b0,AXI_00_RDATA[2*ii][255:0]})
    ,.read_valid(AXI_00_RVALID[2*ii])
    ,.read_ready(AXI_00_RREADY[2*ii])
    
    ,.output_data({grid_dn_l[LEVELS-1][ii],grid_dn[LEVELS-1][ii]})
    ,.output_valid(grid_dn_v[LEVELS-1][ii])
    ,.output_ready(grid_dn_r[LEVELS-1][ii])
    
);

frame_response#(.D_W(260),.M_A_W(M_A_W),.A_W(A_W),.P_W(P_W)  ) response_in2(
    .clk(clk)
    ,.rst(rst)
    ,.header_data({0,0,readRespFifoData[ii+(1<<(LEVELS-1))]})
    ,.header_valid(readRespFifoValid[ii+(1<<(LEVELS-1))])
    ,.header_ready(readRespReady[2*ii+1])
    
    ,.read_data({AXI_00_RLAST[2*ii+1],3'b0,AXI_00_RDATA[2*ii+1][255:0]})
    ,.read_valid(AXI_00_RVALID[2*ii+1])
    ,.read_ready(AXI_00_RREADY[2*ii+1])
    
    ,.output_data({grid_dn_l[LEVELS-1][ii+(1<<(LEVELS-1))],grid_dn[LEVELS-1][ii+(1<<(LEVELS-1))]})
    ,.output_valid(grid_dn_v[LEVELS-1][ii+(1<<(LEVELS-1))])
    ,.output_ready(grid_dn_r[LEVELS-1][ii+(1<<(LEVELS-1))])
    
);

//frame_response#(.D_W(D_W),.M_A_W(M_A_W),.A_W(A_W),.P_W(P_W)  ) response_in(
//    .clk(clk)
//    ,.rst(rst)
//    ,.frame({0,readRespFifoData[ii]})
//    ,.next_frame(readRespReady[2*ii])
//    ,.rdata({AXI_00_RLAST[2*ii],AXI_00_RVALID[2*ii],1'b0,AXI_00_RDATA[2*ii][255:0]})
//    ,.odata(grid_dn[LEVELS-1][ii])
//    ,.o_v(grid_dn_v[LEVELS-1][ii])
//    ,.o_l(grid_dn_l[LEVELS-1][ii])
//    ,.noc_r(grid_dn_r[LEVELS-1][ii])
//    ,.fifo_r(AXI_00_RREADY[2*ii])
    
//);

//frame_response#(.D_W(D_W),.M_A_W(M_A_W),.A_W(A_W),.P_W(P_W)  ) response_in2(
//    .clk(clk)
//    ,.rst(rst)
//    ,.frame({0,readRespFifoData[ii+(1<<(LEVELS-1))]})
//    ,.next_frame(readRespReady[2*ii+1])
//    ,.rdata({AXI_00_RLAST[2*ii+1],AXI_00_RVALID[2*ii+1],1'b0,AXI_00_RDATA[2*ii+1][255:0]})
//    ,.odata(grid_dn[LEVELS-1][ii+(1<<(LEVELS-1))])
//    ,.o_v(grid_dn_v[LEVELS-1][ii+(1<<(LEVELS-1))])
//    ,.o_l(grid_dn_l[LEVELS-1][ii+(1<<(LEVELS-1))])
//    ,.noc_r(grid_dn_r[LEVELS-1][ii+(1<<(LEVELS-1))])
//    ,.fifo_r(AXI_00_RREADY[2*ii+1])
    
//);

end
endgenerate

generate for(ii=0;ii<N/2;ii=ii+1) begin
        assign writeValid[ii] = grid_up_v[LEVELS-1][ii] & grid_up[LEVELS-1][ii][D_W-2] & grid_up_r[LEVELS-1][ii ]| writeValid_lock[ii];
        assign writeValid[ii+(1<<(LEVELS-1))] = grid_up_v[LEVELS-1][ii+(1<<(LEVELS-1))] & grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][D_W-2] &grid_up_r[LEVELS-1][ii+(1<<(LEVELS-1))] | writeValid_lock[ii+(1<<(LEVELS-1))];
    end
endgenerate
always @(posedge clk) begin
    
  for(j=0;j < N/2; j=j+1) begin   
    writeValid_lock[j] <= writeValid[j];// grid_up_v[LEVELS-1][j] & grid_up[LEVELS-1][j][D_W-1];
    if(grid_up_l[LEVELS-1][j]&&grid_up_r[LEVELS-1][j ])
        writeValid_lock[j] <= 1'b0;
        
    writeValid_lock[j+(1<<(LEVELS-1))] <= writeValid[j+(1<<(LEVELS-1))];//] grid_up_v[LEVELS-1][j+(1<<(LEVELS-1))] & grid_up[LEVELS-1][j+(1<<(LEVELS-1))][D_W-1];
    if(grid_up_l[LEVELS-1][j+(1<<(LEVELS-1))] && grid_up_r[LEVELS-1][j+(1<<(LEVELS-1)) ])
        writeValid_lock[j+(1<<(LEVELS-1))] <= 1'b0;
  end

end

  generate for(ii=0;

            `ifdef TREE
                  ii<1;
            `elsif MESH1   
                ii<N/4;
            `else
                ii < N/2;
            `endif
  
            ii=ii+1) begin   
    // Todo: decrease D_W-2 to D_W-3, and add grid_up[LEVELS-1][ii][D_W-2] to all valid signals.
    // BEFORE CHANGING TO AXI FIFO CHANGE THE RESET
   
axis_data_fifo_1 writeFifo(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid( grid_up_v[LEVELS-1][ii] & grid_up[LEVELS-1][ii][D_W-2] & grid_up_r[LEVELS-1][ii ]) // grid_up_v[LEVELS-1][ii] & grid_up[LEVELS-1][ii][D_W-1] &grid_up_r[LEVELS-1][ii ] 
      ,.s_axis_tready(fifoR[ii])
      ,.s_axis_tdata({grid_up[LEVELS-1][ii][`len],grid_up[LEVELS-1][ii][`mem_addr]})
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii])
      
      ,.m_axis_tvalid(fifoValid[ii])
      ,.m_axis_tready(AXI_00_AWREADY[2*ii]) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata({fifoData[ii][`len],fifoData[ii][`mem_addr]})
      ,.m_axis_tlast(writeFifoL[ii])
    
    );
    
    	 axis_data_fifo_0 writeDataFifo(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid(writeValid_lock[ii] & grid_up_v[LEVELS-1][ii] )
      ,.s_axis_tready(fifoDR[ii])
      ,.s_axis_tdata(grid_up[LEVELS-1][ii])
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii])
      
      ,.m_axis_tvalid(fifoWValid[ii])
      ,.m_axis_tready(AXI_00_WREADY[2*ii]) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata(fifoWData[ii])
      ,.m_axis_tlast(writeDataFifoL[ii])
    
    );
    
    
    
    	axis_data_fifo_1 readFifo(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid(grid_up_v[LEVELS-1][ii] & (~grid_up[LEVELS-1][ii][D_W-2]) & grid_up_r[LEVELS-1][ii ] & ~writeValid_lock[ii])
      ,.s_axis_tready(readFifoR[ii])
      ,.s_axis_tdata({grid_up[LEVELS-1][ii][`len],grid_up[LEVELS-1][ii][`mem_addr]})
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii])
      
      ,.m_axis_tvalid(readFifoValid[ii])
      ,.m_axis_tready(AXI_00_ARREADY[2*ii])
      ,.m_axis_tdata({readFifoData[ii][`len],readFifoData[ii][`mem_addr]})
      ,.m_axis_tlast(readFifoL[ii])
    
    );
    
    
    
   	axis_data_fifo_0 readRespFifo(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid(grid_up_v[LEVELS-1][ii] & (~grid_up[LEVELS-1][ii][D_W-2]) &grid_up_r[LEVELS-1][ii] & ~writeValid_lock[ii])
      ,.s_axis_tready(readRespFifoR[ii])
      ,.s_axis_tdata( {grid_up[LEVELS-1][ii][`mem_addr],grid_up[LEVELS-1][ii][`src_addr]}) 
//      ,.s_axis_tdata({grid_up[LEVELS-1][ii][`src_addr],grid_up[LEVELS-1][ii][`mem_addr]})                       
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii])
      
      ,.m_axis_tvalid(readRespFifoValid[ii])
      ,.m_axis_tready(readRespReady[2*ii])//AXI_00_RVALID[2*ii] & grid_dn_r[LEVELS-1][ii]
      ,.m_axis_tdata({readRespFifoData[ii][`mem_addr],readRespFifoData[ii][`src_addr]})
//      ,.m_axis_tdata({readRespFifoData[ii][`src_addr],readRespFifoData[ii][`mem_addr]})
      ,.m_axis_tlast(readRespFifoL[ii])
    
    );
    
//    // s
//`ifndef TREE
    	axis_data_fifo_1 writeFifo2(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid(grid_up_v[LEVELS-1][ii+(1<<(LEVELS-1))] & grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][D_W-2] &grid_up_r[LEVELS-1][ii+(1<<(LEVELS-1))] )
      ,.s_axis_tready(fifoR[ii+(1<<(LEVELS-1))])
      ,.s_axis_tdata({grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][`len],grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][`mem_addr]})
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii+(1<<(LEVELS-1))])
      
      ,.m_axis_tvalid(fifoValid[ii+(1<<(LEVELS-1))])
      ,.m_axis_tready(AXI_00_AWREADY[2*ii+1]) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata({fifoData[ii+(1<<(LEVELS-1))][`len],fifoData[ii+(1<<(LEVELS-1))][`mem_addr]})
      ,.m_axis_tlast(writeFifoL[ii+(1<<(LEVELS-1))])
    
    );
    
    	 axis_data_fifo_0 writeDataFifo2(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid(writeValid_lock[ii+(1<<(LEVELS-1))] & grid_up_v[LEVELS-1][ii+(1<<(LEVELS-1))] )
      ,.s_axis_tready(fifoDR[ii+(1<<(LEVELS-1))])
      ,.s_axis_tdata(grid_up[LEVELS-1][ii+(1<<(LEVELS-1))])
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii+(1<<(LEVELS-1))])
      
      ,.m_axis_tvalid(fifoWValid[ii+(1<<(LEVELS-1))])
      ,.m_axis_tready(AXI_00_WREADY[2*ii+1]) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata(fifoWData[ii+(1<<(LEVELS-1))])
      ,.m_axis_tlast(writeDataFifoL[ii+(1<<(LEVELS-1))])
    
    );
    
    
    
   axis_data_fifo_1 readFifo2(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid(grid_up_v[LEVELS-1][ii+(1<<(LEVELS-1))] & (~grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][D_W-2])&grid_up_r[LEVELS-1][ii+(1<<(LEVELS-1))] & ~writeValid_lock[ii+(1<<(LEVELS-1))] )
      ,.s_axis_tready(readFifoR[ii+(1<<(LEVELS-1))])
      ,.s_axis_tdata({grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][`len],grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][`mem_addr]})
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii+(1<<(LEVELS-1))])
      
      ,.m_axis_tvalid(readFifoValid[ii+(1<<(LEVELS-1))])
      ,.m_axis_tready(AXI_00_ARREADY[2*ii+1])
      ,.m_axis_tdata({readFifoData[ii+(1<<(LEVELS-1))][`len],readFifoData[ii+(1<<(LEVELS-1))][`mem_addr]})
      ,.m_axis_tlast(readFifoL[ii+(1<<(LEVELS-1))])
    
    );
    
    
    
   	axis_data_fifo_0 readRespFifo2(
       .s_axis_aresetn(~AXI_ARESET_N)
      ,.s_axis_aclk(AXI_ACLK)
      ,.s_axis_tvalid(grid_up_v[LEVELS-1][ii+(1<<(LEVELS-1))] & (~grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][D_W-2]) &grid_up_r[LEVELS-1][ii+(1<<(LEVELS-1))] & ~writeValid_lock[ii+(1<<(LEVELS-1))])
      ,.s_axis_tready(readRespFifoR[ii+(1<<(LEVELS-1))])
      ,.s_axis_tdata({grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][`mem_addr],grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][`src_addr]})
//      ,.s_axis_tdata(grid_up[LEVELS-1][ii+(1<<(LEVELS-1))][`src_addr])
      ,.s_axis_tlast(grid_up_l[LEVELS-1][ii+(1<<(LEVELS-1))])
      
      ,.m_axis_tvalid(readRespFifoValid[ii+(1<<(LEVELS-1))])
      ,.m_axis_tready(readRespReady[2*ii+1])//AXI_00_RVALID[2*ii+1]& grid_dn_r[LEVELS-1][ii+(1<<(LEVELS-1))]
      ,.m_axis_tdata({readRespFifoData[ii+(1<<(LEVELS-1))][`mem_addr],readRespFifoData[ii+(1<<(LEVELS-1))][`src_addr]})
//      ,.m_axis_tdata(readRespFifoData[ii+(1<<(LEVELS-1))][`src_addr])
      ,.m_axis_tlast(readRespFifoL[ii+(1<<(LEVELS-1))])
    
    );

end

endgenerate



endmodule