`timescale 1ns / 1ps
`include "commands.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/02/2020 10:46:55 PM
// Design Name: 
// Module Name: frame_response
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module frame_response #(
	parameter M_A_W = 16,
	parameter A_W	= 6,	// address width
	parameter P_W	= 32,		// data width
	parameter D_W   = 1+M_A_W+A_W+P_W
    )(
        input wire clk,
        input wire rst,
        
        input wire [D_W-1:0] frame,  
        output reg next_frame=0,  
        input wire [D_W+1:0] rdata,// last,valid,data
        
        output reg [D_W-1:0] odata=0,
        output reg o_v=0,
        output reg o_l=0,
        
        input wire noc_r,
        output wire fifo_r 
           );
    
    enum {IDLE,HEAD,DATA} state = IDLE;
    
    wire txfer = o_v & noc_r;
    
    reg [D_W+1:0] rdata_r=0;
    reg [D_W+1:0] rdata_bp=0;
    reg dr_reg = 0;
    assign fifo_r = dr_reg;
    
    wire [D_W+1:0] data_sr_out = rdata;   
    
    always @(posedge clk) begin
        case (state)
            IDLE: begin
                
                state <= IDLE;
                if(rdata[D_W]) // if read data is valid
                    state <= HEAD;
            end
            HEAD: begin
                state <= HEAD;
               if(o_v & noc_r) begin
                    state <= DATA;
                    next_frame <= 1'b1;
                end
            end
            DATA: begin
                next_frame <= 1'b0;
                if(o_l & o_v & noc_r) begin
                        state <= IDLE;
                end
            end
        endcase
    end
    
     always @(*) begin
     odata = 0;
     o_l = 0;
     o_v = 0;
     dr_reg = 0;
        case (state)
            IDLE: begin
                dr_reg = 1'b0;
                odata[D_W-1:0] = 0;
                odata[D_W-1] = 0;
                o_v = 1'b0;
                o_l = 1'b0;
             end
             HEAD: begin
                    dr_reg = 1'b0;
                    odata[`dest_addr] = frame[`src_addr];
                    odata[M_A_W-1:0] = frame[`mem_addr];
                    odata[D_W-1] =1'b1;
                    o_v=1'b1;
                    o_l=1'b0;
              end
              DATA: begin
                  dr_reg = noc_r;
                  odata[D_W-1:0] = data_sr_out[D_W-1:0];
                  o_l = data_sr_out[D_W+1];
                  o_v = data_sr_out[D_W];
            end
        endcase
    end

endmodule
