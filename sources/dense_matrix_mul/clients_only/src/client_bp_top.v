
`include	"commands.h"
`timescale 1ps / 1ps
module client_top 
#(
    parameter M_A_W = 16,
	parameter N 	= 2,		// total number of clients
	parameter P_W	= 32,       // data width 
	parameter A_W	= $clog2(N)+1,	// address width
	parameter D_W	= 1+M_A_W+A_W+P_W,		
	parameter WRAP  = 1,            // wrapping means throttling of reinjection
	parameter PAT   = `RANDOM,      // default RANDOM pattern
	parameter RATE  = 10,           // rate of injection (in percent) 
	parameter LIMIT = 16,           // when to stop injectin packets
	parameter SIGMA = 4,            // radius for LOCAL traffic
	parameter posx 	= 2		// position

)
(
	input				clk,
	input				rst,
	input				ce,
	input	wire	`Cmd		cmd,
	input		[D_W-1:0]	s_axis_c_wdata,
	input				s_axis_c_wvalid,
	output	wire			s_axis_c_wready,
	input				s_axis_c_wlast,

	output	wire	[D_W-1:0]	m_axis_c_wdata,
	output	wire			m_axis_c_wvalid,
	input				m_axis_c_wready,
	output	wire			m_axis_c_wlast,

	output	wire			done
);

wire	[D_W:0]	c_o_d_c;
wire			c_o_v_c;
wire			c_o_b_c;

reg	[D_W:0]	c_o_d_c_r=0;
reg			c_o_v_c_r=0;


wire read_ready;

wire [D_W-1:0] read_data;
wire read_last;
wire read_valid;

axis_data_fifo_0 read(
       .s_axis_aresetn(~rst)
      ,.s_axis_aclk(clk)
      ,.s_axis_tvalid(s_axis_c_wvalid) // grid_up_v[LEVELS-1][ii] & grid_up[LEVELS-1][ii][D_W-1] &grid_up_r[LEVELS-1][ii ] 
      ,.s_axis_tready(s_axis_c_wready)
      ,.s_axis_tdata(s_axis_c_wdata)
      ,.s_axis_tlast(s_axis_c_wlast)
      
      ,.m_axis_tvalid(read_valid)
      ,.m_axis_tready(read_ready) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata(read_data)
      ,.m_axis_tlast(read_last)
    
    );
    
    
    axis_data_fifo_1 write(
       .s_axis_aresetn(~rst)
      ,.s_axis_aclk(clk)
      ,.s_axis_tvalid(c_o_v_c_r) // grid_up_v[LEVELS-1][ii] & grid_up[LEVELS-1][ii][D_W-1] &grid_up_r[LEVELS-1][ii ] 
      ,.s_axis_tready(c_o_b_c)
      ,.s_axis_tdata(c_o_d_c_r[D_W-1:0])
      ,.s_axis_tlast(c_o_d_c_r[D_W])
      
      ,.m_axis_tvalid(m_axis_c_wvalid)
      ,.m_axis_tready(m_axis_c_wready) //AXI_00_AWREADY & AXI_00_WREADY &
      ,.m_axis_tdata(m_axis_c_wdata)
      ,.m_axis_tlast(m_axis_c_wlast)
    
    );
    

always@(posedge clk) begin
    if(rst) begin
        c_o_d_c_r <= 0;
        c_o_v_c_r <= 0;
    end
    else begin
        if( c_o_b_c ==1) begin
            c_o_d_c_r <= c_o_d_c;
            c_o_v_c_r <= c_o_v_c;
        end
    end
end

reg ce_r = 0;

wire ce_pulse = ce & ~ce_r;
always@(posedge clk) begin
    ce_r <= ce;
end


     wrapper_0 cli_mm(
        .ap_clk(clk),
        .ap_rst_n(~rst),
        .ap_start(ce_pulse),
        .ap_done(done),
        .ap_idle(),
        .ap_ready(),
        .baseA_V('h00000),
        .baseB_V('h20000),
        .baseC_V('h40000),
        .src_V(posx),
        .posx_V(posx),
        .write_port_V_V_TDATA(c_o_d_c),
        .write_port_V_V_TVALID(c_o_v_c	),
        .write_port_V_V_TREADY(c_o_b_c	),
        .read_port_V_V_TDATA({read_last,read_data}),
        .read_port_V_V_TVALID(read_valid),
        .read_port_V_V_TREADY(read_ready),
        .matrix_size_V(`size)
);



endmodule
