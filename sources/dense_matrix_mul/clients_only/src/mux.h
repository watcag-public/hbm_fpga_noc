// Mux direction controls
`define NONE 	3'b000
`define LEFT 	3'b100
`define RIGHT 	3'b101
`define U0 	3'b110
`define U1 	3'b111

`define offset_len 202
`define dest_addr A_W+`offset_len-1:`offset_len
`define src_addr  2*A_W+`offset_len-1:A_W+`offset_len
`define addr_info 1+1+1+8+M_A_W+2*A_W+`offset_len:`offset_len
`define addr_info_len 1+1+1+8+M_A_W+2*A_W
`define len 8+M_A_W+2*A_W+`offset_len-1:M_A_W+2*A_W+`offset_len
`define mem_addr M_A_W+2*A_W+`offset_len-1:2*A_W+`offset_len

`define size 1024
`define block_size 8
