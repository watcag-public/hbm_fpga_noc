############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
set_directive_unroll "vec_to_ss/vec_to_ss_label1"
set_directive_inline "vec_to_ss/vec_to_ss_label1"
set_directive_unroll "ss_to_vec/ss_to_vec_label0"
set_directive_inline "ss_to_vec/ss_to_vec_label0"
set_directive_interface -mode axis -register -register_mode both "wrapper" read_port
set_directive_interface -mode axis -register -register_mode both "wrapper" write_port
set_directive_pipeline "write_ab/write"
set_directive_pipeline "store_a/Read_A"
set_directive_pipeline "readB_mul/Read_B"
set_directive_array_reshape -type cyclic -factor 8 -dim 1 "wrapper" a
set_directive_array_partition -type complete -dim 0 "wrapper" ab
set_directive_resource -core RAM_S2P_URAM "wrapper" a
