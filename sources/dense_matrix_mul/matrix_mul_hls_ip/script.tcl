############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project -reset parallel_blocked_mat_mul
set_top wrapper
add_files src/wrapper.cpp
add_files src/wrapper.h
add_files -tb src/wrapper_tb.cpp -cflags "-Wno-unknown-pragmas"
open_solution "solution1"
set_part {xcu280-fsvh2892-2L-e}
create_clock -period 4 -name default
config_export -display_name parallel_blocked_mat_mul -format ip_catalog -rtl verilog -vivado_optimization_level 2 -vivado_phys_opt place -vivado_report_level 0
config_sdx -target none
set_clock_uncertainty 12.5%
source "directives.tcl"
# csim_design -clean
csynth_design
# cosim_design
export_design -rtl verilog -format ip_catalog -display_name "parallel_blocked_mat_mul"
exit
