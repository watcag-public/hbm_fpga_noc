#include "wrapper.h"

static data_type ab[block_size][block_size];

void ss_to_vec(stream_size input, vec &output) {
	ss_to_vec_label0: for (int i = 0; i < block_size; i++) {

		output.element[i] = input.range(32 * (i + 1) - 1, i * 32);
	}
}

void vec_to_ss(vec input, stream_size &output) {
	vec_to_ss_label1: for (int i = 0; i < block_size; i++) {
		output.range(32 * (i + 1) - 1, i * 32) = input.element[i];
	}

}
void clear_ab(data_type ab[block_size][block_size]) {
	clear_ab: for (int i = 0; i < block_size; i++)
		for (int j = 0; j < block_size; j++)
			ab[i][j] = 0;
}

void issue_b(unsigned int col, mem_data_type baseB, ap_uint<6> src,ap_uint<5> port_id,
		hls::stream<stream_size> &write_port) {
	issue_B: for (int b = 0; b < (trans_cnt); b++) {
		stream_size write_element;
		write_element = 0;
		write_element(memory_addr) = (mem_data_type) ((baseB)
				+ ((col * size) + b * block_size * burst_length) * 4);
		write_element(port) = port_id(4,0);
		write_element(last) = 1;
		write_element(head) = 1;
		write_element(rw) = 0;
		write_element(dest_addr) = 32;
		write_element(src_addr) = src; // change to src addr later.
		write_element(len) = burst_length - 1;
		write_element(offset) = 0;
		write_port.write(write_element);

	}

}

void issue_a(unsigned int row, mem_data_type baseA, ap_uint<6> src,ap_uint<5> port_id,
		hls::stream<stream_size> &write_port) {
	issue_A: for (int b = 0; b < (trans_cnt); b++) {
		stream_size write_element;
		write_element = 0;
		write_element(memory_addr) = (mem_data_type) ((baseA)
				+ ((row * size) + b * block_size * burst_length) * 4); // offset byte_locations
		write_element(port) = port_id(4,0);
		write_element(last) = 1;
		write_element(head) = 1;
		write_element(rw) = 0;
		write_element(dest_addr) = 32;
		write_element(src_addr) = src; // change to src addr later.
		write_element(len) = burst_length - 1;
		write_element(offset) = 0;
		write_port.write(write_element);

	}
}

void store_a(hls::stream<stream_size> &read_port,
		data_type a[block_size][size]) {
	int ii = 0;
	Read_A: for (int k = 0; k < size + trans_cnt; k++) {
		stream_size read_element;
		vec a_element;
		read_element = read_port.read();
		if ((k % (burst_length + 1)) != 0) {
			ss_to_vec(read_element, a_element);
			Store_A: for (int j = 0; j < block_size; j++) {
				a[j][ii] = a_element.element[j];
			}
			ii++;
		}
	}
}

void readB_mul(hls::stream<stream_size> &read_port, data_type a[block_size][size]) {

	int ii = 0;
	Read_B: for (int k = 0; k < size + trans_cnt; k++) {
		stream_size read_element;
		vec b_element;
		read_element = read_port.read();
		if ((k % (burst_length + 1)) != 0) {
			ss_to_vec(read_element, b_element);
			mat_mul: for (int i = 0; i < block_size; i++)
				mat_mul_inner: for (int j = 0; j < block_size; j++) {
					ab[i][j] = ab[i][j] + (a[i][ii] * b_element.element[j]);
				}
			ii++;
		}
	}

}




void write_ab(unsigned int row, unsigned int col,mem_data_type baseC,
			ap_uint<6> src, hls::stream<stream_size> &write_port){
	stream_size write_element;
	vec block_out_element;
	write_element = 0;
	write_element(memory_addr) =
			(mem_data_type) ((baseC) + (row * size + col * block_size) * 4);
	write_element(port) = src(4,0);
	write_element(last) = 0;
	write_element(head) = 1;
	write_element(rw) = 1;
	write_element(dest_addr) = 32;
	write_element(src_addr) = src; // change to src addr later.
	write_element(len) = block_size - 1;
	write_element(offset) = 0;
	write_port.write(write_element);
	write: for (int i = 0; i < block_size; i++) {
//				stream_size write_element;
		write_element=0;
		mem_data_type temp;
		write_element = 0;
		stuff_block_out: for (int j = 0; j < block_size; j++) {
			block_out_element.element[j] = ab[i][j];
					ab[i][j] = 0;
		}
		vec_to_ss(block_out_element, write_element);
		if (i == block_size - 1)
			write_element(last) = 1;
		write_port.write(write_element);

	}
}

void wrapper(mem_data_type baseA, mem_data_type baseB, mem_data_type baseC,
		ap_uint<6> src, ap_uint<6> posx, hls::stream<stream_size> &write_port,
		hls::stream<stream_size> &read_port,
		ap_uint<13> matrix_size) {
	int row = 0;

	int patches = size/(block_size*pe_cnt);
	int required_count_patch = (pe_cnt);
	data_type a[block_size][size];

	int it = 0;
	stream_size write_element;
	stream_size read_element;

	vec a_element, b_element, block_out_element;
	mem_data_type temp;

	int col;
	/* Act as an conductor to issue read and write request for blocked matrix multiplication*/
	/* 1) Issue read requests for One block_size of row from A and one block size of col from B*/
	int count = 0;
	load_A:for (int i = 0; i < size/(block_size*pe_cnt); i += 1) {
		mem_data_type A = baseA+i*(block_size*size*4);
		issue_a(0,  A, posx,src, write_port);
		store_a(read_port, a);
		load_B:for (int j = 0; j < size/(block_size*pe_cnt); j += 1) {
			load_B_n_w:for(int k=0;k<required_count_patch;k++){
				mem_data_type B = baseB+(j)*(block_size*size*4);
				mem_data_type C = baseC+count*(block_size*block_size*4);
				int temp = ((k)+src)%pe_cnt;
				issue_b(0, B, posx,temp, write_port);
				readB_mul(read_port, a);
				write_ab(0, 0, C, src, write_port);
				count++;
			}
		}
	}



}
