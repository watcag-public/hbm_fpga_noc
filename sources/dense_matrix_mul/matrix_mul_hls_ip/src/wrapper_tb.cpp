#include "wrapper.h"
#include <stdlib.h>
#include <iostream>
#include <assert.h>
using namespace std;
int temp;

void matmul_sw(data_type a[size][size], data_type b[size][size],
		data_type out[size][size]) {
	int sum = 0;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			sum = 0;
			for (int k = 0; k < size; k++) {
				sum += a[i][k] * b[k][j];
			}
			out[i][j] = sum;
		}
	}
}

int main() {
//	assert(block_size==8);
//	assert(size >= burst_length);
	hls::stream<stream_size> write_port("write_port"), read_port("read_port");
	mem_data_type baseA = 0x0;
	mem_data_type baseB = 0x10000;
	mem_data_type baseC = 0x20000;
	static full_mat out_hw;

	static data_type a[size][size];
	static data_type b[size][size];
	static data_type out[size][size], sw_out[size][size];
	vec a_element, b_element, block_out_element, addr;
	stream_size write_element;
	stream_size read_element;
	mem_data_type temp;
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++) {
			a[i][j] = (data_type) (rand() % 100);
			b[i][j] = (data_type) (rand() % 100);
			out[i][j] = (data_type) 0;
			sw_out[i][j] = (data_type) 0;
		}

	cout << "A matrix" << endl;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
//
	cout << "B matrix" << endl;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
//			cout << b[i][j] << " ";
		}
//		cout << endl;
	}
	int matrix_size = size;
	int patches = matrix_size / (block_size * pe_cnt);
	int required_count_patch = (pe_cnt);

	int row, col, it = 0;
	for (int elements = 0; elements < pe_cnt; elements++) {

		for (int i = 0; i < matrix_size / (block_size * pe_cnt); i += 1) {

			row = (elements) * block_size + i*(block_size * pe_cnt);
			for (int b = 0; b < trans_cnt; b++) {
				temp = (mem_data_type) (baseA
						+ ((row * size) + b * block_size * burst_length) * 4);
				read_element(memory_addr) = temp;
				read_port.write(read_element);

				for (int k = 0; k < burst_length; k++) {
					for (int ii = 0; ii < block_size; ii++) {
						a_element.element[ii] =
								a[row + ii][k + b * burst_length]; // load one block of A and B
					}
					vec_to_ss(a_element, read_element);
					read_port.write(read_element); // Send it over to mat_mul keep it in the stream

				}
			}

			for (int j = 0; j < matrix_size / (block_size * pe_cnt); j += 1)
				{
					for(int k=0;k<pe_cnt;k++){
					col = (((k) + (elements)) % pe_cnt) * block_size+j*(block_size * pe_cnt);
					for (int bi = 0; bi < trans_cnt; bi++) {
						temp =
								(mem_data_type) (baseB
										+ ((col * size)
												+ bi * block_size * burst_length)
												* 4);
						read_element(memory_addr) = temp;
						read_port.write(read_element);

						for (int k = 0; k < burst_length; k++) {
							for (int ii = 0; ii < block_size; ii++) {
								b_element.element[ii] =
										b[k + bi * burst_length][col + ii];
							}
							vec_to_ss(b_element, read_element);
							read_port.write(read_element);
						}
					}

					it = it + 1;
				}
			}

		}
		wrapper(baseA, baseB, baseC, elements, elements, write_port, read_port, matrix_size);

		for (int i = 0; i < matrix_size / (block_size * pe_cnt); i += 1) {
			cout << "Issue A" << endl;
			for (int b = 0; b < (trans_cnt); b++) {
				write_element = write_port.read();
				cout << hex << write_element(246, 214) << endl;
			}
			for (int j = 0; j < matrix_size / (block_size * pe_cnt); j += 1) {
				for (int k = 0; k < required_count_patch; k++) {
					cout << "Issue B" << endl;
					for (int b = 0; b < (trans_cnt); b++) {
						write_element = write_port.read();
						cout << hex << write_element(246, 214) << endl;
					}
					cout << "Issue AB" << endl;
					write_element = write_port.read();
					cout << hex << write_element(246, 214) << endl;

					int temp = (k + elements) % pe_cnt;
					for (int m = 0; m < block_size; m++) {
						write_element = write_port.read();
						ss_to_vec(write_element, block_out_element);
						for (int ii = 0; ii < block_size; ii++)
							out_hw.m[m + (i * pe_cnt * block_size)
									+ (elements * block_size)][ii
									+ (temp * block_size)
									+ (j * pe_cnt * block_size)] =
									block_out_element.element[ii];

					}
				}
				cout << "One patch done" << endl;
			}
		}
	}
	matmul_sw(a, b, sw_out);

	cout << dec << "Computed in Hardware" << endl;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			cout << out_hw.m[i][j] << " ";
		}
		cout << endl;
	}
//
	cout << "Computed in software" << endl;
	int fail = 0;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			cout << sw_out[i][j] << " ";
			if (out_hw.m[i][j] != sw_out[i][j]) {
				fail = 1;
//					cout<<out_hw.m[i][j]<<" "<<sw_out[i][j]<<endl;
//					return 1;

			}
		}
		cout << endl;
	}
	if (fail) {
		cout << "Failed";
		return 1;
	}
	cout << "Test Passed";
	return 0;

}
