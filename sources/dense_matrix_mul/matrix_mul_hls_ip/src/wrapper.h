#include "ap_int.h"
#include "hls_stream.h"
#include<iostream>
#include<iomanip>
#include<vector>
#include <stdlib.h>
#include "/tools/Xilinx/Vivado/2019.2/include/gmp.h"
#define __gmp_const const

#define size 1024
#define block_size 8
#define burst_length 16

#define pe_cnt 32

#define trans_cnt size/burst_length
//#define trans_cnt 1


typedef ap_uint<32> data_type;
typedef ap_uint<33> mem_data_type;

typedef ap_uint<259> stream_size;

typedef struct{
	data_type element[block_size];
} vec;

typedef struct{
	data_type m[block_size][block_size];
} mat;


using namespace std;

#define number_of_b_req (size/block_size)*((size/block_size))*trans_cnt
#define number_of_a_req (size/block_size)*trans_cnt
#define number_of_req number_of_b_req+number_of_a_req


#define offset 201,0
#define dest_addr 207,202
#define src_addr 213,208
#define memory_addr 241,214
#define port 246,242
#define len 254,247
#define data 255,0

#define rw   256,256
#define head 257,257
#define last 258,258




typedef struct{
	data_type m[size][size];
} full_mat;

void wrapper(mem_data_type baseA,mem_data_type baseB,mem_data_type baseC, ap_uint<6> src, ap_uint<6> posx, hls::stream<stream_size> &write_port,
		hls::stream<stream_size> &read_port,ap_uint<13> matrix_size);


void b_mul(mem_data_type baseC,hls::stream<vec> &A,hls::stream<vec> &B,hls::stream<vec> &block_out, data_type iteration);
void ss_to_vec(stream_size input, vec &output);
void vec_to_ss(vec input, stream_size &output);


