`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/13/2019 06:15:22 PM
// Design Name: 
// Module Name: lfsr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module lfsr
#(parameter x=1,
  parameter bit_width = 8)
   (
   
    input wire CLK,
    input wire RESET,
    input wire enable,
    output wire [bit_width-1:0] RAND
    );
    
    generate if (bit_width == 8) begin
        reg [15:0] lfsr=x;
        assign RAND = lfsr[bit_width-1:0];
        integer i;
        always @(posedge CLK)
        begin
            if(RESET) begin
               lfsr <= x;
                
            end
            else if(enable==0)begin
                lfsr[0] <= lfsr[15] ^ lfsr[13] ^ lfsr[12] ^ lfsr[10];
                for(i=0;i<15;i=i+1)
                    lfsr[i+1]<=lfsr[i];
                
            end
        end
    end
    else if (bit_width == 23) begin
    reg [31:0] lfsr=x;
        assign RAND = lfsr[bit_width-1:0];
        integer i;
        always @(posedge CLK)
        begin
            if(RESET) begin
               lfsr <= x;
                
            end
            else if(enable==0)begin
                lfsr[0] <= lfsr[31] ^ lfsr[21] ^ lfsr[1] ^ lfsr[0];
                for(i=0;i<31;i=i+1)
                    lfsr[i+1]<=lfsr[i];
                
            end
        end
    end
   endgenerate
   
   
endmodule

