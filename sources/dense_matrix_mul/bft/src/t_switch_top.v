`timescale 1ps / 1ps
module t_switch_top
#
(
	parameter	N	= 4,			//number of clients
	parameter	WRAP	= 1,			//crossbar?
    parameter	P_W	= 32,
	parameter A_W	= $clog2(N)+1,
	parameter M_A_W = 16,
	parameter D_W	= 1+M_A_W+A_W+P_W,
	parameter	posl	= 0,			//which level
	parameter	posx	= 0,		//which position
	parameter	DEBUG	= 1,
	parameter	FD	= 32,
	parameter	HR	= 4
)
(
	input  wire 			clk,		// clock
	input  wire 			rst,		// reset
	input  wire 			ce,		// clock enable
	
	input  	wire	[A_W+D_W-1:0] 	s_axis_l_wdata	,	
	output	wire			s_axis_l_wready	,
	input	wire			s_axis_l_wvalid	,
	input	wire			s_axis_l_wlast	,
	
	input  	wire	[A_W+D_W-1:0] 	s_axis_r_wdata	,
	output	wire			s_axis_r_wready	,
	input	wire			s_axis_r_wvalid	,
	input	wire			s_axis_r_wlast	,
	
	input  	wire	[A_W+D_W-1:0] 	s_axis_u0_wdata	,
	output	wire			s_axis_u0_wready, 
	input	wire			s_axis_u0_wvalid, 
	input	wire			s_axis_u0_wlast	,

	output 	wire	[A_W+D_W-1:0] 	m_axis_l_wdata	,
	input	wire			m_axis_l_wready	,
	output	wire			m_axis_l_wvalid	,
	output	wire			m_axis_l_wlast	,	

	output 	wire	[A_W+D_W-1:0] 	m_axis_r_wdata	,
	input	wire			m_axis_r_wready	,
	output	wire			m_axis_r_wvalid	,
	output	wire			m_axis_r_wlast	,	

	output 	wire	[A_W+D_W-1:0] 	m_axis_u0_wdata	,
	input	wire			m_axis_u0_wready,
	output	wire			m_axis_u0_wvalid,
	output	wire			m_axis_u0_wlast	,	

	output 	wire 			done		// done
);



reg     [A_W+D_W-1:0]   previous_l;
reg     [A_W+D_W-1:0]   previous_r;
reg     [A_W+D_W-1:0]   previous_u0;
integer now=0;



always@(posedge clk)
begin
	now <= now+1;
    if (previous_l!=m_axis_l_wdata )//&& m_axis_l_wready==1'b11 && m_axis_l_wvalid==1'1b1)
    begin
        $display("Time%0d: switching",now-1);
        previous_l <= m_axis_l_wdata;
    end
    if (previous_r!=m_axis_r_wdata )//&& m_axis_l_wready==1'b11 && m_axis_l_wvalid==1'1b1)
    begin
        $display("Time%0d: switching",now-1);
        previous_r <= m_axis_r_wdata;
    end
    if (previous_u0!=m_axis_u0_wdata )//&& m_axis_l_wready==1'b11 && m_axis_l_wvalid==1'1b1)
    begin
        $display("Time%0d: switching",now-1);
        previous_u0 <= m_axis_u0_wdata;
    end
end
wire	[A_W+D_W:0]	t_o_d_l;
wire			t_o_v_l;
wire			t_o_b_l;

wire	[A_W+D_W:0]	t_o_d_r;
wire			t_o_v_r;
wire			t_o_b_r;

wire	[A_W+D_W:0]	t_o_d_u0;
wire			t_o_v_u0;
wire			t_o_b_u0;

assign	m_axis_l_wdata		= t_o_d_l[A_W+D_W-1:0];
assign	m_axis_l_wvalid		= t_o_v_l;
assign	m_axis_l_wlast		= t_o_d_l[A_W+D_W];
assign	t_o_b_l			= ~m_axis_l_wready;

assign	m_axis_r_wdata		= t_o_d_r[A_W+D_W-1:0];
assign	m_axis_r_wvalid		= t_o_v_r;
assign	m_axis_r_wlast		= t_o_d_r[A_W+D_W];
assign	t_o_b_r			= ~m_axis_r_wready;

assign	m_axis_u0_wdata		= t_o_d_u0[A_W+D_W-1:0];
assign	m_axis_u0_wvalid	= t_o_v_u0;
assign	m_axis_u0_wlast		= t_o_d_u0[A_W+D_W];
assign	t_o_b_u0		= ~m_axis_u0_wready;

wire	[A_W+D_W:0]	t_i_d_l;
wire			t_i_v_l;
wire			t_i_b_l;

wire	[A_W+D_W:0]	t_i_d_r;
wire			t_i_v_r;
wire			t_i_b_r;

wire	[A_W+D_W:0]	t_i_d_u0;
wire			t_i_v_u0;
wire			t_i_b_u0;

t_switch
#(
    .M_A_W(M_A_W),.P_W(P_W),
	.N		(N	),	
	.WRAP		(WRAP	),
	.A_W		(A_W	),
	.D_W		(D_W	),
	.posl  		(posl	),
	.posx 		(posx	)
)
t_switch_inst
(
	.clk		(clk		),		// clock
	.rst		(rst		),		// reset
	.ce		(ce		),		// clock enable
	`ifdef SIM
	.done		(done		),	// done
	`endif
	.l_i		(t_i_d_l	),	// left  input payload
	.l_i_bp		(t_i_b_l	),	// left  input backpressured
	.l_i_v		(t_i_v_l	),	// left  input valid
	.r_i		(t_i_d_r	),	// right input payload
	.r_i_bp		(t_i_b_r	),	// right input backpressured
	.r_i_v		(t_i_v_r	),	// right input valid
	.u0_i		(t_i_d_u0	),	// u0    input payload
	.u0_i_bp	(t_i_b_u0	),	// u0    input backpressured
	.u0_i_v		(t_i_v_u0	),	// u0    input valid
	.l_o		(t_o_d_l	),	// left  input payload
	.l_o_bp		(t_o_b_l	),	// left  input backpressured
	.l_o_v		(t_o_v_l	),	// left  input valid
	.r_o		(t_o_d_r	),	// right input payload
	.r_o_bp		(t_o_b_r	),	// right input backpressured
	.r_o_v		(t_o_v_r	),	// right input valid
	.u0_o		(t_o_d_u0	),	// u0    input payload
	.u0_o_bp	(t_o_b_u0	),	// u0    input backpressured
	.u0_o_v		(t_o_v_u0	)	// u0    input valid
);

wire	[A_W+D_W:0]	bp_i_d_l;
wire			bp_i_v_l;
wire			bp_i_b_l;

wire	[A_W+D_W:0]	bp_o_d_l;
wire			bp_o_v_l;
wire			bp_o_b_l;


assign	bp_i_v_l	= s_axis_l_wvalid;
assign	bp_i_d_l	= {s_axis_l_wlast, s_axis_l_wdata};
assign	s_axis_l_wready	= ~bp_i_b_l;

assign	t_i_v_l		= bp_o_v_l;
assign	t_i_d_l		= bp_o_d_l;
assign	bp_o_b_l	= t_i_b_l;

shadow_reg_combi
#(
	.D_W		(D_W	),
	.A_W		(A_W	)
	,.P_W(P_W),.M_A_W(M_A_W)
)
bp_L
(
	.clk		(clk		), 
	.rst		(rst		), 
	.i_v		(bp_i_v_l	),
	.i_d		(bp_i_d_l	), 
 	.i_b		(bp_i_b_l	),
 	.o_v		(bp_o_v_l	),
	.o_d		(bp_o_d_l	),	 
	.o_b		(bp_o_b_l	) // unregistered from DOR logic
);

wire	[A_W+D_W:0]	bp_i_d_r;
wire			bp_i_v_r;
wire			bp_i_b_r;

wire	[A_W+D_W:0]	bp_o_d_r;
wire			bp_o_v_r;
wire			bp_o_b_r;


assign	bp_i_v_r	= s_axis_r_wvalid;
assign	bp_i_d_r	= {s_axis_r_wlast, s_axis_r_wdata};
assign	s_axis_r_wready	= ~bp_i_b_r;

assign	t_i_v_r		= bp_o_v_r;
assign	t_i_d_r		= bp_o_d_r;
assign	bp_o_b_r	= t_i_b_r;

shadow_reg_combi
#(
	.D_W		(D_W	),
	.A_W		(A_W	)
	,.P_W(P_W),.M_A_W(M_A_W)
)
bp_R
(
	.clk		(clk		), 
	.rst		(rst		), 
	.i_v		(bp_i_v_r	),
	.i_d		(bp_i_d_r	), 
 	.i_b		(bp_i_b_r	),
 	.o_v		(bp_o_v_r	),
	.o_d		(bp_o_d_r	),	 
	.o_b		(bp_o_b_r	) // unregistered from DOR logic
);

wire	[A_W+D_W:0]	bp_i_d_u0;
wire			bp_i_v_u0;
wire			bp_i_b_u0;

wire	[A_W+D_W:0]	bp_o_d_u0;
wire			bp_o_v_u0;
wire			bp_o_b_u0;


assign	bp_i_v_u0	= s_axis_u0_wvalid;
assign	bp_i_d_u0	= {s_axis_u0_wlast, s_axis_u0_wdata};
assign	s_axis_u0_wready= ~bp_i_b_u0;

assign	t_i_v_u0	= bp_o_v_u0;
assign	t_i_d_u0	= bp_o_d_u0;
assign	bp_o_b_u0	= t_i_b_u0;

shadow_reg_combi
#(
	.D_W		(D_W	),
	.A_W		(A_W	)
	,.P_W(P_W),.M_A_W(M_A_W)
)
bp_U0
(
	.clk		(clk		), 
	.rst		(rst		), 
	.i_v		(bp_i_v_u0	),
	.i_d		(bp_i_d_u0	), 
 	.i_b		(bp_i_b_u0	),
 	.o_v		(bp_o_v_u0	),
	.o_d		(bp_o_d_u0	),	 
	.o_b		(bp_o_b_u0	) // unregistered from DOR logic
);

endmodule
