// This is a generated file. Use and modify at your own risk.
////////////////////////////////////////////////////////////////////////////////
// default_nettype of none prevents implicit wire declaration.
`include "commands.h"
`define XBAR   


`ifdef XBAR
    `define XBARorM1orM0
    `define XBARorM1
    `define XBARorM0
 `endif
 
 `ifdef MESH1
    `define XBARorM1orM0
    `define XBARorM1
 `endif
 
 `ifdef MESH0
    `define XBARorM1orM0
    `define XBARorM0
 `endif
`default_nettype none
`timescale 1 ps / 1 ps
import axi_vip_pkg::*;
import slv_m00_axi_vip_pkg::*;
import slv_m01_axi_vip_pkg::*;
import slv_m02_axi_vip_pkg::*;
import slv_m03_axi_vip_pkg::*;
import slv_m04_axi_vip_pkg::*;
import slv_m05_axi_vip_pkg::*;
import slv_m06_axi_vip_pkg::*;
import slv_m07_axi_vip_pkg::*;
import slv_m08_axi_vip_pkg::*;
import slv_m09_axi_vip_pkg::*;
import slv_m10_axi_vip_pkg::*;
import slv_m11_axi_vip_pkg::*;
import slv_m12_axi_vip_pkg::*;
import slv_m13_axi_vip_pkg::*;
import slv_m14_axi_vip_pkg::*;
import slv_m15_axi_vip_pkg::*;
import slv_m16_axi_vip_pkg::*;
import slv_m17_axi_vip_pkg::*;
import slv_m18_axi_vip_pkg::*;
import slv_m19_axi_vip_pkg::*;
import slv_m20_axi_vip_pkg::*;
import slv_m21_axi_vip_pkg::*;
import slv_m22_axi_vip_pkg::*;
import slv_m23_axi_vip_pkg::*;
import slv_m24_axi_vip_pkg::*;
import slv_m25_axi_vip_pkg::*;
import slv_m26_axi_vip_pkg::*;
import slv_m27_axi_vip_pkg::*;
import slv_m28_axi_vip_pkg::*;
import slv_m29_axi_vip_pkg::*;
import slv_m30_axi_vip_pkg::*;
import slv_m31_axi_vip_pkg::*;
import control_bft_32_xbar_v2_vip_pkg::*;

module bft_32_xbar_v2_tb ();
parameter integer LP_MAX_LENGTH = 8192;
parameter integer LP_MAX_TRANSFER_LENGTH = 16384 / 4;
parameter integer C_S_AXI_CONTROL_ADDR_WIDTH = 12;
parameter integer C_S_AXI_CONTROL_DATA_WIDTH = 32;
parameter integer C_M00_AXI_ADDR_WIDTH = 64;
parameter integer C_M00_AXI_DATA_WIDTH = 256;
parameter integer C_M01_AXI_ADDR_WIDTH = 64;
parameter integer C_M01_AXI_DATA_WIDTH = 256;
parameter integer C_M02_AXI_ADDR_WIDTH = 64;
parameter integer C_M02_AXI_DATA_WIDTH = 256;
parameter integer C_M03_AXI_ADDR_WIDTH = 64;
parameter integer C_M03_AXI_DATA_WIDTH = 256;
parameter integer C_M04_AXI_ADDR_WIDTH = 64;
parameter integer C_M04_AXI_DATA_WIDTH = 256;
parameter integer C_M05_AXI_ADDR_WIDTH = 64;
parameter integer C_M05_AXI_DATA_WIDTH = 256;
parameter integer C_M06_AXI_ADDR_WIDTH = 64;
parameter integer C_M06_AXI_DATA_WIDTH = 256;
parameter integer C_M07_AXI_ADDR_WIDTH = 64;
parameter integer C_M07_AXI_DATA_WIDTH = 256;
parameter integer C_M08_AXI_ADDR_WIDTH = 64;
parameter integer C_M08_AXI_DATA_WIDTH = 256;
parameter integer C_M09_AXI_ADDR_WIDTH = 64;
parameter integer C_M09_AXI_DATA_WIDTH = 256;
parameter integer C_M10_AXI_ADDR_WIDTH = 64;
parameter integer C_M10_AXI_DATA_WIDTH = 256;
parameter integer C_M11_AXI_ADDR_WIDTH = 64;
parameter integer C_M11_AXI_DATA_WIDTH = 256;
parameter integer C_M12_AXI_ADDR_WIDTH = 64;
parameter integer C_M12_AXI_DATA_WIDTH = 256;
parameter integer C_M13_AXI_ADDR_WIDTH = 64;
parameter integer C_M13_AXI_DATA_WIDTH = 256;
parameter integer C_M14_AXI_ADDR_WIDTH = 64;
parameter integer C_M14_AXI_DATA_WIDTH = 256;
parameter integer C_M15_AXI_ADDR_WIDTH = 64;
parameter integer C_M15_AXI_DATA_WIDTH = 256;

parameter integer C_M16_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M16_AXI_DATA_WIDTH = 256;
parameter integer C_M17_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M17_AXI_DATA_WIDTH = 256;
parameter integer C_M18_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M18_AXI_DATA_WIDTH = 256;
parameter integer C_M19_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M19_AXI_DATA_WIDTH = 256;
parameter integer C_M20_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M20_AXI_DATA_WIDTH = 256;
parameter integer C_M21_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M21_AXI_DATA_WIDTH = 256;
parameter integer C_M22_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M22_AXI_DATA_WIDTH = 256;
parameter integer C_M23_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M23_AXI_DATA_WIDTH = 256;
parameter integer C_M24_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M24_AXI_DATA_WIDTH = 256;
parameter integer C_M25_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M25_AXI_DATA_WIDTH = 256;
parameter integer C_M26_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M26_AXI_DATA_WIDTH = 256;
parameter integer C_M27_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M27_AXI_DATA_WIDTH = 256;
parameter integer C_M28_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M28_AXI_DATA_WIDTH = 256;
parameter integer C_M29_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M29_AXI_DATA_WIDTH = 256;
parameter integer C_M30_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M30_AXI_DATA_WIDTH = 256;
parameter integer C_M31_AXI_ADDR_WIDTH = 64 ;
parameter integer C_M31_AXI_DATA_WIDTH = 256;

// Control Register
parameter KRNL_CTRL_REG_ADDR     = 32'h00000000;
parameter CTRL_START_MASK        = 32'h00000001;
parameter CTRL_DONE_MASK         = 32'h00000002;
parameter CTRL_IDLE_MASK         = 32'h00000004;
parameter CTRL_READY_MASK        = 32'h00000008;
parameter CTRL_CONTINUE_MASK     = 32'h00000010; // Only ap_ctrl_chain
parameter CTRL_AUTO_RESTART_MASK = 32'h00000080; // Not used


// Global Interrupt Enable Register
parameter KRNL_GIE_REG_ADDR      = 32'h00000004;
parameter GIE_GIE_MASK           = 32'h00000001;
// IP Interrupt Enable Register
parameter KRNL_IER_REG_ADDR      = 32'h00000008;
parameter IER_DONE_MASK          = 32'h00000001;
parameter IER_READY_MASK         = 32'h00000002;
// IP Interrupt Status Register
parameter KRNL_ISR_REG_ADDR      = 32'h0000000c;
parameter ISR_DONE_MASK          = 32'h00000001;
parameter ISR_READY_MASK         = 32'h00000002;

parameter integer LP_CLK_PERIOD_PS = 4000; // 250 MHz
    parameter size = `size;
     parameter block_size = 8;
        integer count = 0;
           integer baseA = 28'h0;
      integer baseB = 28'h20000;
       integer unsigned a[size][size];
     integer unsigned b[size][size];
     
//System Signals
logic ap_clk = 0;

initial begin: AP_CLK
  forever begin
    ap_clk = #(LP_CLK_PERIOD_PS/2) ~ap_clk;
  end
end
//AXI4 master interface m00_axi
wire [1-1:0] m00_axi_awvalid;
wire [1-1:0] m00_axi_awready;
wire [C_M00_AXI_ADDR_WIDTH-1:0] m00_axi_awaddr;
wire [8-1:0] m00_axi_awlen;
wire [1-1:0] m00_axi_wvalid;
wire [1-1:0] m00_axi_wready;
wire [C_M00_AXI_DATA_WIDTH-1:0] m00_axi_wdata;
wire [C_M00_AXI_DATA_WIDTH/8-1:0] m00_axi_wstrb;
wire [1-1:0] m00_axi_wlast;
wire [1-1:0] m00_axi_bvalid;
wire [1-1:0] m00_axi_bready;
wire [1-1:0] m00_axi_arvalid;
wire [1-1:0] m00_axi_arready;
wire [C_M00_AXI_ADDR_WIDTH-1:0] m00_axi_araddr;
wire [8-1:0] m00_axi_arlen;
wire [1-1:0] m00_axi_rvalid;
wire [1-1:0] m00_axi_rready;
wire [C_M00_AXI_DATA_WIDTH-1:0] m00_axi_rdata;
wire [1-1:0] m00_axi_rlast;
//AXI4 master interface m01_axi
`ifdef XBARorM1orM0


wire [1-1:0] m01_axi_awvalid;
wire [1-1:0] m01_axi_awready;
wire [C_M01_AXI_ADDR_WIDTH-1:0] m01_axi_awaddr;
wire [8-1:0] m01_axi_awlen;
wire [1-1:0] m01_axi_wvalid;
wire [1-1:0] m01_axi_wready;
wire [C_M01_AXI_DATA_WIDTH-1:0] m01_axi_wdata;
wire [C_M01_AXI_DATA_WIDTH/8-1:0] m01_axi_wstrb;
wire [1-1:0] m01_axi_wlast;
wire [1-1:0] m01_axi_bvalid;
wire [1-1:0] m01_axi_bready;
wire [1-1:0] m01_axi_arvalid;
wire [1-1:0] m01_axi_arready;
wire [C_M01_AXI_ADDR_WIDTH-1:0] m01_axi_araddr;
wire [8-1:0] m01_axi_arlen;
wire [1-1:0] m01_axi_rvalid;
wire [1-1:0] m01_axi_rready;
wire [C_M01_AXI_DATA_WIDTH-1:0] m01_axi_rdata;
wire [1-1:0] m01_axi_rlast;
//AXI4 master interface m02_axi
wire [1-1:0] m02_axi_awvalid;
wire [1-1:0] m02_axi_awready;
wire [C_M02_AXI_ADDR_WIDTH-1:0] m02_axi_awaddr;
wire [8-1:0] m02_axi_awlen;
wire [1-1:0] m02_axi_wvalid;
wire [1-1:0] m02_axi_wready;
wire [C_M02_AXI_DATA_WIDTH-1:0] m02_axi_wdata;
wire [C_M02_AXI_DATA_WIDTH/8-1:0] m02_axi_wstrb;
wire [1-1:0] m02_axi_wlast;
wire [1-1:0] m02_axi_bvalid;
wire [1-1:0] m02_axi_bready;
wire [1-1:0] m02_axi_arvalid;
wire [1-1:0] m02_axi_arready;
wire [C_M02_AXI_ADDR_WIDTH-1:0] m02_axi_araddr;
wire [8-1:0] m02_axi_arlen;
wire [1-1:0] m02_axi_rvalid;
wire [1-1:0] m02_axi_rready;
wire [C_M02_AXI_DATA_WIDTH-1:0] m02_axi_rdata;
wire [1-1:0] m02_axi_rlast;
//AXI4 master interface m03_axi
wire [1-1:0] m03_axi_awvalid;
wire [1-1:0] m03_axi_awready;
wire [C_M03_AXI_ADDR_WIDTH-1:0] m03_axi_awaddr;
wire [8-1:0] m03_axi_awlen;
wire [1-1:0] m03_axi_wvalid;
wire [1-1:0] m03_axi_wready;
wire [C_M03_AXI_DATA_WIDTH-1:0] m03_axi_wdata;
wire [C_M03_AXI_DATA_WIDTH/8-1:0] m03_axi_wstrb;
wire [1-1:0] m03_axi_wlast;
wire [1-1:0] m03_axi_bvalid;
wire [1-1:0] m03_axi_bready;
wire [1-1:0] m03_axi_arvalid;
wire [1-1:0] m03_axi_arready;
wire [C_M03_AXI_ADDR_WIDTH-1:0] m03_axi_araddr;
wire [8-1:0] m03_axi_arlen;
wire [1-1:0] m03_axi_rvalid;
wire [1-1:0] m03_axi_rready;
wire [C_M03_AXI_DATA_WIDTH-1:0] m03_axi_rdata;
wire [1-1:0] m03_axi_rlast;
//AXI4 master interface m04_axi
`endif
`ifdef XBARorM1
wire [1-1:0] m04_axi_awvalid;
wire [1-1:0] m04_axi_awready;
wire [C_M04_AXI_ADDR_WIDTH-1:0] m04_axi_awaddr;
wire [8-1:0] m04_axi_awlen;
wire [1-1:0] m04_axi_wvalid;
wire [1-1:0] m04_axi_wready;
wire [C_M04_AXI_DATA_WIDTH-1:0] m04_axi_wdata;
wire [C_M04_AXI_DATA_WIDTH/8-1:0] m04_axi_wstrb;
wire [1-1:0] m04_axi_wlast;
wire [1-1:0] m04_axi_bvalid;
wire [1-1:0] m04_axi_bready;
wire [1-1:0] m04_axi_arvalid;
wire [1-1:0] m04_axi_arready;
wire [C_M04_AXI_ADDR_WIDTH-1:0] m04_axi_araddr;
wire [8-1:0] m04_axi_arlen;
wire [1-1:0] m04_axi_rvalid;
wire [1-1:0] m04_axi_rready;
wire [C_M04_AXI_DATA_WIDTH-1:0] m04_axi_rdata;
wire [1-1:0] m04_axi_rlast;
//AXI4 master interface m05_axi
wire [1-1:0] m05_axi_awvalid;
wire [1-1:0] m05_axi_awready;
wire [C_M05_AXI_ADDR_WIDTH-1:0] m05_axi_awaddr;
wire [8-1:0] m05_axi_awlen;
wire [1-1:0] m05_axi_wvalid;
wire [1-1:0] m05_axi_wready;
wire [C_M05_AXI_DATA_WIDTH-1:0] m05_axi_wdata;
wire [C_M05_AXI_DATA_WIDTH/8-1:0] m05_axi_wstrb;
wire [1-1:0] m05_axi_wlast;
wire [1-1:0] m05_axi_bvalid;
wire [1-1:0] m05_axi_bready;
wire [1-1:0] m05_axi_arvalid;
wire [1-1:0] m05_axi_arready;
wire [C_M05_AXI_ADDR_WIDTH-1:0] m05_axi_araddr;
wire [8-1:0] m05_axi_arlen;
wire [1-1:0] m05_axi_rvalid;
wire [1-1:0] m05_axi_rready;
wire [C_M05_AXI_DATA_WIDTH-1:0] m05_axi_rdata;
wire [1-1:0] m05_axi_rlast;
//AXI4 master interface m06_axi
wire [1-1:0] m06_axi_awvalid;
wire [1-1:0] m06_axi_awready;
wire [C_M06_AXI_ADDR_WIDTH-1:0] m06_axi_awaddr;
wire [8-1:0] m06_axi_awlen;
wire [1-1:0] m06_axi_wvalid;
wire [1-1:0] m06_axi_wready;
wire [C_M06_AXI_DATA_WIDTH-1:0] m06_axi_wdata;
wire [C_M06_AXI_DATA_WIDTH/8-1:0] m06_axi_wstrb;
wire [1-1:0] m06_axi_wlast;
wire [1-1:0] m06_axi_bvalid;
wire [1-1:0] m06_axi_bready;
wire [1-1:0] m06_axi_arvalid;
wire [1-1:0] m06_axi_arready;
wire [C_M06_AXI_ADDR_WIDTH-1:0] m06_axi_araddr;
wire [8-1:0] m06_axi_arlen;
wire [1-1:0] m06_axi_rvalid;
wire [1-1:0] m06_axi_rready;
wire [C_M06_AXI_DATA_WIDTH-1:0] m06_axi_rdata;
wire [1-1:0] m06_axi_rlast;
//AXI4 master interface m07_axi
wire [1-1:0] m07_axi_awvalid;
wire [1-1:0] m07_axi_awready;
wire [C_M07_AXI_ADDR_WIDTH-1:0] m07_axi_awaddr;
wire [8-1:0] m07_axi_awlen;
wire [1-1:0] m07_axi_wvalid;
wire [1-1:0] m07_axi_wready;
wire [C_M07_AXI_DATA_WIDTH-1:0] m07_axi_wdata;
wire [C_M07_AXI_DATA_WIDTH/8-1:0] m07_axi_wstrb;
wire [1-1:0] m07_axi_wlast;
wire [1-1:0] m07_axi_bvalid;
wire [1-1:0] m07_axi_bready;
wire [1-1:0] m07_axi_arvalid;
wire [1-1:0] m07_axi_arready;
wire [C_M07_AXI_ADDR_WIDTH-1:0] m07_axi_araddr;
wire [8-1:0] m07_axi_arlen;
wire [1-1:0] m07_axi_rvalid;
wire [1-1:0] m07_axi_rready;
wire [C_M07_AXI_DATA_WIDTH-1:0] m07_axi_rdata;
wire [1-1:0] m07_axi_rlast;
//AXI4 master interface m08_axi
`endif
`ifdef XBARorM0 

wire [1-1:0] m08_axi_awvalid;
wire [1-1:0] m08_axi_awready;
wire [C_M08_AXI_ADDR_WIDTH-1:0] m08_axi_awaddr;
wire [8-1:0] m08_axi_awlen;
wire [1-1:0] m08_axi_wvalid;
wire [1-1:0] m08_axi_wready;
wire [C_M08_AXI_DATA_WIDTH-1:0] m08_axi_wdata;
wire [C_M08_AXI_DATA_WIDTH/8-1:0] m08_axi_wstrb;
wire [1-1:0] m08_axi_wlast;
wire [1-1:0] m08_axi_bvalid;
wire [1-1:0] m08_axi_bready;
wire [1-1:0] m08_axi_arvalid;
wire [1-1:0] m08_axi_arready;
wire [C_M08_AXI_ADDR_WIDTH-1:0] m08_axi_araddr;
wire [8-1:0] m08_axi_arlen;
wire [1-1:0] m08_axi_rvalid;
wire [1-1:0] m08_axi_rready;
wire [C_M08_AXI_DATA_WIDTH-1:0] m08_axi_rdata;
wire [1-1:0] m08_axi_rlast;
//AXI4 master interface m09_axi
wire [1-1:0] m09_axi_awvalid;
wire [1-1:0] m09_axi_awready;
wire [C_M09_AXI_ADDR_WIDTH-1:0] m09_axi_awaddr;
wire [8-1:0] m09_axi_awlen;
wire [1-1:0] m09_axi_wvalid;
wire [1-1:0] m09_axi_wready;
wire [C_M09_AXI_DATA_WIDTH-1:0] m09_axi_wdata;
wire [C_M09_AXI_DATA_WIDTH/8-1:0] m09_axi_wstrb;
wire [1-1:0] m09_axi_wlast;
wire [1-1:0] m09_axi_bvalid;
wire [1-1:0] m09_axi_bready;
wire [1-1:0] m09_axi_arvalid;
wire [1-1:0] m09_axi_arready;
wire [C_M09_AXI_ADDR_WIDTH-1:0] m09_axi_araddr;
wire [8-1:0] m09_axi_arlen;
wire [1-1:0] m09_axi_rvalid;
wire [1-1:0] m09_axi_rready;
wire [C_M09_AXI_DATA_WIDTH-1:0] m09_axi_rdata;
wire [1-1:0] m09_axi_rlast;
//AXI4 master interface m10_axi
wire [1-1:0] m10_axi_awvalid;
wire [1-1:0] m10_axi_awready;
wire [C_M10_AXI_ADDR_WIDTH-1:0] m10_axi_awaddr;
wire [8-1:0] m10_axi_awlen;
wire [1-1:0] m10_axi_wvalid;
wire [1-1:0] m10_axi_wready;
wire [C_M10_AXI_DATA_WIDTH-1:0] m10_axi_wdata;
wire [C_M10_AXI_DATA_WIDTH/8-1:0] m10_axi_wstrb;
wire [1-1:0] m10_axi_wlast;
wire [1-1:0] m10_axi_bvalid;
wire [1-1:0] m10_axi_bready;
wire [1-1:0] m10_axi_arvalid;
wire [1-1:0] m10_axi_arready;
wire [C_M10_AXI_ADDR_WIDTH-1:0] m10_axi_araddr;
wire [8-1:0] m10_axi_arlen;
wire [1-1:0] m10_axi_rvalid;
wire [1-1:0] m10_axi_rready;
wire [C_M10_AXI_DATA_WIDTH-1:0] m10_axi_rdata;
wire [1-1:0] m10_axi_rlast;
//AXI4 master interface m11_axi
wire [1-1:0] m11_axi_awvalid;
wire [1-1:0] m11_axi_awready;
wire [C_M11_AXI_ADDR_WIDTH-1:0] m11_axi_awaddr;
wire [8-1:0] m11_axi_awlen;
wire [1-1:0] m11_axi_wvalid;
wire [1-1:0] m11_axi_wready;
wire [C_M11_AXI_DATA_WIDTH-1:0] m11_axi_wdata;
wire [C_M11_AXI_DATA_WIDTH/8-1:0] m11_axi_wstrb;
wire [1-1:0] m11_axi_wlast;
wire [1-1:0] m11_axi_bvalid;
wire [1-1:0] m11_axi_bready;
wire [1-1:0] m11_axi_arvalid;
wire [1-1:0] m11_axi_arready;
wire [C_M11_AXI_ADDR_WIDTH-1:0] m11_axi_araddr;
wire [8-1:0] m11_axi_arlen;
wire [1-1:0] m11_axi_rvalid;
wire [1-1:0] m11_axi_rready;
wire [C_M11_AXI_DATA_WIDTH-1:0] m11_axi_rdata;
wire [1-1:0] m11_axi_rlast;
//AXI4 master interface m12_axi
wire [1-1:0] m12_axi_awvalid;
wire [1-1:0] m12_axi_awready;
wire [C_M12_AXI_ADDR_WIDTH-1:0] m12_axi_awaddr;
wire [8-1:0] m12_axi_awlen;
wire [1-1:0] m12_axi_wvalid;
wire [1-1:0] m12_axi_wready;
wire [C_M12_AXI_DATA_WIDTH-1:0] m12_axi_wdata;
wire [C_M12_AXI_DATA_WIDTH/8-1:0] m12_axi_wstrb;
wire [1-1:0] m12_axi_wlast;
wire [1-1:0] m12_axi_bvalid;
wire [1-1:0] m12_axi_bready;
wire [1-1:0] m12_axi_arvalid;
wire [1-1:0] m12_axi_arready;
wire [C_M12_AXI_ADDR_WIDTH-1:0] m12_axi_araddr;
wire [8-1:0] m12_axi_arlen;
wire [1-1:0] m12_axi_rvalid;
wire [1-1:0] m12_axi_rready;
wire [C_M12_AXI_DATA_WIDTH-1:0] m12_axi_rdata;
wire [1-1:0] m12_axi_rlast;
//AXI4 master interface m13_axi

`endif
`ifdef XBAR 

wire [1-1:0] m13_axi_awvalid;
wire [1-1:0] m13_axi_awready;
wire [C_M13_AXI_ADDR_WIDTH-1:0] m13_axi_awaddr;
wire [8-1:0] m13_axi_awlen;
wire [1-1:0] m13_axi_wvalid;
wire [1-1:0] m13_axi_wready;
wire [C_M13_AXI_DATA_WIDTH-1:0] m13_axi_wdata;
wire [C_M13_AXI_DATA_WIDTH/8-1:0] m13_axi_wstrb;
wire [1-1:0] m13_axi_wlast;
wire [1-1:0] m13_axi_bvalid;
wire [1-1:0] m13_axi_bready;
wire [1-1:0] m13_axi_arvalid;
wire [1-1:0] m13_axi_arready;
wire [C_M13_AXI_ADDR_WIDTH-1:0] m13_axi_araddr;
wire [8-1:0] m13_axi_arlen;
wire [1-1:0] m13_axi_rvalid;
wire [1-1:0] m13_axi_rready;
wire [C_M13_AXI_DATA_WIDTH-1:0] m13_axi_rdata;
wire [1-1:0] m13_axi_rlast;
//AXI4 master interface m14_axi
wire [1-1:0] m14_axi_awvalid;
wire [1-1:0] m14_axi_awready;
wire [C_M14_AXI_ADDR_WIDTH-1:0] m14_axi_awaddr;
wire [8-1:0] m14_axi_awlen;
wire [1-1:0] m14_axi_wvalid;
wire [1-1:0] m14_axi_wready;
wire [C_M14_AXI_DATA_WIDTH-1:0] m14_axi_wdata;
wire [C_M14_AXI_DATA_WIDTH/8-1:0] m14_axi_wstrb;
wire [1-1:0] m14_axi_wlast;
wire [1-1:0] m14_axi_bvalid;
wire [1-1:0] m14_axi_bready;
wire [1-1:0] m14_axi_arvalid;
wire [1-1:0] m14_axi_arready;
wire [C_M14_AXI_ADDR_WIDTH-1:0] m14_axi_araddr;
wire [8-1:0] m14_axi_arlen;
wire [1-1:0] m14_axi_rvalid;
wire [1-1:0] m14_axi_rready;
wire [C_M14_AXI_DATA_WIDTH-1:0] m14_axi_rdata;
wire [1-1:0] m14_axi_rlast;
//AXI4 master interface m15_axi
wire [1-1:0] m15_axi_awvalid;
wire [1-1:0] m15_axi_awready;
wire [C_M15_AXI_ADDR_WIDTH-1:0] m15_axi_awaddr;
wire [8-1:0] m15_axi_awlen;
wire [1-1:0] m15_axi_wvalid;
wire [1-1:0] m15_axi_wready;
wire [C_M15_AXI_DATA_WIDTH-1:0] m15_axi_wdata;
wire [C_M15_AXI_DATA_WIDTH/8-1:0] m15_axi_wstrb;
wire [1-1:0] m15_axi_wlast;
wire [1-1:0] m15_axi_bvalid;
wire [1-1:0] m15_axi_bready;
wire [1-1:0] m15_axi_arvalid;
wire [1-1:0] m15_axi_arready;
wire [C_M15_AXI_ADDR_WIDTH-1:0] m15_axi_araddr;
wire [8-1:0] m15_axi_arlen;
wire [1-1:0] m15_axi_rvalid;
wire [1-1:0] m15_axi_rready;
wire [C_M15_AXI_DATA_WIDTH-1:0] m15_axi_rdata;
wire [1-1:0] m15_axi_rlast;

wire [1-1:0] m16_axi_awvalid;
wire [1-1:0] m16_axi_awready;
wire [C_M16_AXI_ADDR_WIDTH-1:0] m16_axi_awaddr;
wire [8-1:0] m16_axi_awlen;
wire [1-1:0] m16_axi_wvalid;
wire [1-1:0] m16_axi_wready;
wire [C_M16_AXI_DATA_WIDTH-1:0] m16_axi_wdata;
wire [C_M16_AXI_DATA_WIDTH/8-1:0] m16_axi_wstrb;
wire [1-1:0] m16_axi_wlast;
wire [1-1:0] m16_axi_bvalid;
wire [1-1:0] m16_axi_bready;
wire [1-1:0] m16_axi_arvalid;
wire [1-1:0] m16_axi_arready;
wire [C_M16_AXI_ADDR_WIDTH-1:0] m16_axi_araddr;
wire [8-1:0] m16_axi_arlen;
wire [1-1:0] m16_axi_rvalid;
wire [1-1:0] m16_axi_rready;
wire [C_M16_AXI_DATA_WIDTH-1:0] m16_axi_rdata;
wire [1-1:0] m16_axi_rlast;
//AXI4 master interface m17_axi
wire [1-1:0] m17_axi_awvalid;
wire [1-1:0] m17_axi_awready;
wire [C_M17_AXI_ADDR_WIDTH-1:0] m17_axi_awaddr;
wire [8-1:0] m17_axi_awlen;
wire [1-1:0] m17_axi_wvalid;
wire [1-1:0] m17_axi_wready;
wire [C_M17_AXI_DATA_WIDTH-1:0] m17_axi_wdata;
wire [C_M17_AXI_DATA_WIDTH/8-1:0] m17_axi_wstrb;
wire [1-1:0] m17_axi_wlast;
wire [1-1:0] m17_axi_bvalid;
wire [1-1:0] m17_axi_bready;
wire [1-1:0] m17_axi_arvalid;
wire [1-1:0] m17_axi_arready;
wire [C_M17_AXI_ADDR_WIDTH-1:0] m17_axi_araddr;
wire [8-1:0] m17_axi_arlen;
wire [1-1:0] m17_axi_rvalid;
wire [1-1:0] m17_axi_rready;
wire [C_M17_AXI_DATA_WIDTH-1:0] m17_axi_rdata;
wire [1-1:0] m17_axi_rlast;
//AXI4 master interface m18_axi
wire [1-1:0] m18_axi_awvalid;
wire [1-1:0] m18_axi_awready;
wire [C_M18_AXI_ADDR_WIDTH-1:0] m18_axi_awaddr;
wire [8-1:0] m18_axi_awlen;
wire [1-1:0] m18_axi_wvalid;
wire [1-1:0] m18_axi_wready;
wire [C_M18_AXI_DATA_WIDTH-1:0] m18_axi_wdata;
wire [C_M18_AXI_DATA_WIDTH/8-1:0] m18_axi_wstrb;
wire [1-1:0] m18_axi_wlast;
wire [1-1:0] m18_axi_bvalid;
wire [1-1:0] m18_axi_bready;
wire [1-1:0] m18_axi_arvalid;
wire [1-1:0] m18_axi_arready;
wire [C_M18_AXI_ADDR_WIDTH-1:0] m18_axi_araddr;
wire [8-1:0] m18_axi_arlen;
wire [1-1:0] m18_axi_rvalid;
wire [1-1:0] m18_axi_rready;
wire [C_M18_AXI_DATA_WIDTH-1:0] m18_axi_rdata;
wire [1-1:0] m18_axi_rlast;
//AXI4 master interface m19_axi
wire [1-1:0] m19_axi_awvalid;
wire [1-1:0] m19_axi_awready;
wire [C_M19_AXI_ADDR_WIDTH-1:0] m19_axi_awaddr;
wire [8-1:0] m19_axi_awlen;
wire [1-1:0] m19_axi_wvalid;
wire [1-1:0] m19_axi_wready;
wire [C_M19_AXI_DATA_WIDTH-1:0] m19_axi_wdata;
wire [C_M19_AXI_DATA_WIDTH/8-1:0] m19_axi_wstrb;
wire [1-1:0] m19_axi_wlast;
wire [1-1:0] m19_axi_bvalid;
wire [1-1:0] m19_axi_bready;
wire [1-1:0] m19_axi_arvalid;
wire [1-1:0] m19_axi_arready;
wire [C_M19_AXI_ADDR_WIDTH-1:0] m19_axi_araddr;
wire [8-1:0] m19_axi_arlen;
wire [1-1:0] m19_axi_rvalid;
wire [1-1:0] m19_axi_rready;
wire [C_M19_AXI_DATA_WIDTH-1:0] m19_axi_rdata;
wire [1-1:0] m19_axi_rlast;
//AXI4 master interface m20_axi
wire [1-1:0] m20_axi_awvalid;
wire [1-1:0] m20_axi_awready;
wire [C_M20_AXI_ADDR_WIDTH-1:0] m20_axi_awaddr;
wire [8-1:0] m20_axi_awlen;
wire [1-1:0] m20_axi_wvalid;
wire [1-1:0] m20_axi_wready;
wire [C_M20_AXI_DATA_WIDTH-1:0] m20_axi_wdata;
wire [C_M20_AXI_DATA_WIDTH/8-1:0] m20_axi_wstrb;
wire [1-1:0] m20_axi_wlast;
wire [1-1:0] m20_axi_bvalid;
wire [1-1:0] m20_axi_bready;
wire [1-1:0] m20_axi_arvalid;
wire [1-1:0] m20_axi_arready;
wire [C_M20_AXI_ADDR_WIDTH-1:0] m20_axi_araddr;
wire [8-1:0] m20_axi_arlen;
wire [1-1:0] m20_axi_rvalid;
wire [1-1:0] m20_axi_rready;
wire [C_M20_AXI_DATA_WIDTH-1:0] m20_axi_rdata;
wire [1-1:0] m20_axi_rlast;
//AXI4 master interface m21_axi
wire [1-1:0] m21_axi_awvalid;
wire [1-1:0] m21_axi_awready;
wire [C_M21_AXI_ADDR_WIDTH-1:0] m21_axi_awaddr;
wire [8-1:0] m21_axi_awlen;
wire [1-1:0] m21_axi_wvalid;
wire [1-1:0] m21_axi_wready;
wire [C_M21_AXI_DATA_WIDTH-1:0] m21_axi_wdata;
wire [C_M21_AXI_DATA_WIDTH/8-1:0] m21_axi_wstrb;
wire [1-1:0] m21_axi_wlast;
wire [1-1:0] m21_axi_bvalid;
wire [1-1:0] m21_axi_bready;
wire [1-1:0] m21_axi_arvalid;
wire [1-1:0] m21_axi_arready;
wire [C_M21_AXI_ADDR_WIDTH-1:0] m21_axi_araddr;
wire [8-1:0] m21_axi_arlen;
wire [1-1:0] m21_axi_rvalid;
wire [1-1:0] m21_axi_rready;
wire [C_M21_AXI_DATA_WIDTH-1:0] m21_axi_rdata;
wire [1-1:0] m21_axi_rlast;
//AXI4 master interface m22_axi
wire [1-1:0] m22_axi_awvalid;
wire [1-1:0] m22_axi_awready;
wire [C_M22_AXI_ADDR_WIDTH-1:0] m22_axi_awaddr;
wire [8-1:0] m22_axi_awlen;
wire [1-1:0] m22_axi_wvalid;
wire [1-1:0] m22_axi_wready;
wire [C_M22_AXI_DATA_WIDTH-1:0] m22_axi_wdata;
wire [C_M22_AXI_DATA_WIDTH/8-1:0] m22_axi_wstrb;
wire [1-1:0] m22_axi_wlast;
wire [1-1:0] m22_axi_bvalid;
wire [1-1:0] m22_axi_bready;
wire [1-1:0] m22_axi_arvalid;
wire [1-1:0] m22_axi_arready;
wire [C_M22_AXI_ADDR_WIDTH-1:0] m22_axi_araddr;
wire [8-1:0] m22_axi_arlen;
wire [1-1:0] m22_axi_rvalid;
wire [1-1:0] m22_axi_rready;
wire [C_M22_AXI_DATA_WIDTH-1:0] m22_axi_rdata;
wire [1-1:0] m22_axi_rlast;
//AXI4 master interface m23_axi
wire [1-1:0] m23_axi_awvalid;
wire [1-1:0] m23_axi_awready;
wire [C_M23_AXI_ADDR_WIDTH-1:0] m23_axi_awaddr;
wire [8-1:0] m23_axi_awlen;
wire [1-1:0] m23_axi_wvalid;
wire [1-1:0] m23_axi_wready;
wire [C_M23_AXI_DATA_WIDTH-1:0] m23_axi_wdata;
wire [C_M23_AXI_DATA_WIDTH/8-1:0] m23_axi_wstrb;
wire [1-1:0] m23_axi_wlast;
wire [1-1:0] m23_axi_bvalid;
wire [1-1:0] m23_axi_bready;
wire [1-1:0] m23_axi_arvalid;
wire [1-1:0] m23_axi_arready;
wire [C_M23_AXI_ADDR_WIDTH-1:0] m23_axi_araddr;
wire [8-1:0] m23_axi_arlen;
wire [1-1:0] m23_axi_rvalid;
wire [1-1:0] m23_axi_rready;
wire [C_M23_AXI_DATA_WIDTH-1:0] m23_axi_rdata;
wire [1-1:0] m23_axi_rlast;
//AXI4 master interface m24_axi
wire [1-1:0] m24_axi_awvalid;
wire [1-1:0] m24_axi_awready;
wire [C_M24_AXI_ADDR_WIDTH-1:0] m24_axi_awaddr;
wire [8-1:0] m24_axi_awlen;
wire [1-1:0] m24_axi_wvalid;
wire [1-1:0] m24_axi_wready;
wire [C_M24_AXI_DATA_WIDTH-1:0] m24_axi_wdata;
wire [C_M24_AXI_DATA_WIDTH/8-1:0] m24_axi_wstrb;
wire [1-1:0] m24_axi_wlast;
wire [1-1:0] m24_axi_bvalid;
wire [1-1:0] m24_axi_bready;
wire [1-1:0] m24_axi_arvalid;
wire [1-1:0] m24_axi_arready;
wire [C_M24_AXI_ADDR_WIDTH-1:0] m24_axi_araddr;
wire [8-1:0] m24_axi_arlen;
wire [1-1:0] m24_axi_rvalid;
wire [1-1:0] m24_axi_rready;
wire [C_M24_AXI_DATA_WIDTH-1:0] m24_axi_rdata;
wire [1-1:0] m24_axi_rlast;
//AXI4 master interface m25_axi
wire [1-1:0] m25_axi_awvalid;
wire [1-1:0] m25_axi_awready;
wire [C_M25_AXI_ADDR_WIDTH-1:0] m25_axi_awaddr;
wire [8-1:0] m25_axi_awlen;
wire [1-1:0] m25_axi_wvalid;
wire [1-1:0] m25_axi_wready;
wire [C_M25_AXI_DATA_WIDTH-1:0] m25_axi_wdata;
wire [C_M25_AXI_DATA_WIDTH/8-1:0] m25_axi_wstrb;
wire [1-1:0] m25_axi_wlast;
wire [1-1:0] m25_axi_bvalid;
wire [1-1:0] m25_axi_bready;
wire [1-1:0] m25_axi_arvalid;
wire [1-1:0] m25_axi_arready;
wire [C_M25_AXI_ADDR_WIDTH-1:0] m25_axi_araddr;
wire [8-1:0] m25_axi_arlen;
wire [1-1:0] m25_axi_rvalid;
wire [1-1:0] m25_axi_rready;
wire [C_M25_AXI_DATA_WIDTH-1:0] m25_axi_rdata;
wire [1-1:0] m25_axi_rlast;
//AXI4 master interface m26_axi
wire [1-1:0] m26_axi_awvalid;
wire [1-1:0] m26_axi_awready;
wire [C_M26_AXI_ADDR_WIDTH-1:0] m26_axi_awaddr;
wire [8-1:0] m26_axi_awlen;
wire [1-1:0] m26_axi_wvalid;
wire [1-1:0] m26_axi_wready;
wire [C_M26_AXI_DATA_WIDTH-1:0] m26_axi_wdata;
wire [C_M26_AXI_DATA_WIDTH/8-1:0] m26_axi_wstrb;
wire [1-1:0] m26_axi_wlast;
wire [1-1:0] m26_axi_bvalid;
wire [1-1:0] m26_axi_bready;
wire [1-1:0] m26_axi_arvalid;
wire [1-1:0] m26_axi_arready;
wire [C_M26_AXI_ADDR_WIDTH-1:0] m26_axi_araddr;
wire [8-1:0] m26_axi_arlen;
wire [1-1:0] m26_axi_rvalid;
wire [1-1:0] m26_axi_rready;
wire [C_M26_AXI_DATA_WIDTH-1:0] m26_axi_rdata;
wire [1-1:0] m26_axi_rlast;
//AXI4 master interface m27_axi
wire [1-1:0] m27_axi_awvalid;
wire [1-1:0] m27_axi_awready;
wire [C_M27_AXI_ADDR_WIDTH-1:0] m27_axi_awaddr;
wire [8-1:0] m27_axi_awlen;
wire [1-1:0] m27_axi_wvalid;
wire [1-1:0] m27_axi_wready;
wire [C_M27_AXI_DATA_WIDTH-1:0] m27_axi_wdata;
wire [C_M27_AXI_DATA_WIDTH/8-1:0] m27_axi_wstrb;
wire [1-1:0] m27_axi_wlast;
wire [1-1:0] m27_axi_bvalid;
wire [1-1:0] m27_axi_bready;
wire [1-1:0] m27_axi_arvalid;
wire [1-1:0] m27_axi_arready;
wire [C_M27_AXI_ADDR_WIDTH-1:0] m27_axi_araddr;
wire [8-1:0] m27_axi_arlen;
wire [1-1:0] m27_axi_rvalid;
wire [1-1:0] m27_axi_rready;
wire [C_M27_AXI_DATA_WIDTH-1:0] m27_axi_rdata;
wire [1-1:0] m27_axi_rlast;
//AXI4 master interface m28_axi
wire [1-1:0] m28_axi_awvalid;
wire [1-1:0] m28_axi_awready;
wire [C_M28_AXI_ADDR_WIDTH-1:0] m28_axi_awaddr;
wire [8-1:0] m28_axi_awlen;
wire [1-1:0] m28_axi_wvalid;
wire [1-1:0] m28_axi_wready;
wire [C_M28_AXI_DATA_WIDTH-1:0] m28_axi_wdata;
wire [C_M28_AXI_DATA_WIDTH/8-1:0] m28_axi_wstrb;
wire [1-1:0] m28_axi_wlast;
wire [1-1:0] m28_axi_bvalid;
wire [1-1:0] m28_axi_bready;
wire [1-1:0] m28_axi_arvalid;
wire [1-1:0] m28_axi_arready;
wire [C_M28_AXI_ADDR_WIDTH-1:0] m28_axi_araddr;
wire [8-1:0] m28_axi_arlen;
wire [1-1:0] m28_axi_rvalid;
wire [1-1:0] m28_axi_rready;
wire [C_M28_AXI_DATA_WIDTH-1:0] m28_axi_rdata;
wire [1-1:0] m28_axi_rlast;
//AXI4 master interface m29_axi
wire [1-1:0] m29_axi_awvalid;
wire [1-1:0] m29_axi_awready;
wire [C_M29_AXI_ADDR_WIDTH-1:0] m29_axi_awaddr;
wire [8-1:0] m29_axi_awlen;
wire [1-1:0] m29_axi_wvalid;
wire [1-1:0] m29_axi_wready;
wire [C_M29_AXI_DATA_WIDTH-1:0] m29_axi_wdata;
wire [C_M29_AXI_DATA_WIDTH/8-1:0] m29_axi_wstrb;
wire [1-1:0] m29_axi_wlast;
wire [1-1:0] m29_axi_bvalid;
wire [1-1:0] m29_axi_bready;
wire [1-1:0] m29_axi_arvalid;
wire [1-1:0] m29_axi_arready;
wire [C_M29_AXI_ADDR_WIDTH-1:0] m29_axi_araddr;
wire [8-1:0] m29_axi_arlen;
wire [1-1:0] m29_axi_rvalid;
wire [1-1:0] m29_axi_rready;
wire [C_M29_AXI_DATA_WIDTH-1:0] m29_axi_rdata;
wire [1-1:0] m29_axi_rlast;
//AXI4 master interface m30_axi
wire [1-1:0] m30_axi_awvalid;
wire [1-1:0] m30_axi_awready;
wire [C_M30_AXI_ADDR_WIDTH-1:0] m30_axi_awaddr;
wire [8-1:0] m30_axi_awlen;
wire [1-1:0] m30_axi_wvalid;
wire [1-1:0] m30_axi_wready;
wire [C_M30_AXI_DATA_WIDTH-1:0] m30_axi_wdata;
wire [C_M30_AXI_DATA_WIDTH/8-1:0] m30_axi_wstrb;
wire [1-1:0] m30_axi_wlast;
wire [1-1:0] m30_axi_bvalid;
wire [1-1:0] m30_axi_bready;
wire [1-1:0] m30_axi_arvalid;
wire [1-1:0] m30_axi_arready;
wire [C_M30_AXI_ADDR_WIDTH-1:0] m30_axi_araddr;
wire [8-1:0] m30_axi_arlen;
wire [1-1:0] m30_axi_rvalid;
wire [1-1:0] m30_axi_rready;
wire [C_M30_AXI_DATA_WIDTH-1:0] m30_axi_rdata;
wire [1-1:0] m30_axi_rlast;
//AXI4 master interface m31_axi
wire [1-1:0] m31_axi_awvalid;
wire [1-1:0] m31_axi_awready;
wire [C_M31_AXI_ADDR_WIDTH-1:0] m31_axi_awaddr;
wire [8-1:0] m31_axi_awlen;
wire [1-1:0] m31_axi_wvalid;
wire [1-1:0] m31_axi_wready;
wire [C_M31_AXI_DATA_WIDTH-1:0] m31_axi_wdata;
wire [C_M31_AXI_DATA_WIDTH/8-1:0] m31_axi_wstrb;
wire [1-1:0] m31_axi_wlast;
wire [1-1:0] m31_axi_bvalid;
wire [1-1:0] m31_axi_bready;
wire [1-1:0] m31_axi_arvalid;
wire [1-1:0] m31_axi_arready;
wire [C_M31_AXI_ADDR_WIDTH-1:0] m31_axi_araddr;
wire [8-1:0] m31_axi_arlen;
wire [1-1:0] m31_axi_rvalid;
wire [1-1:0] m31_axi_rready;
wire [C_M31_AXI_DATA_WIDTH-1:0] m31_axi_rdata;
wire [1-1:0] m31_axi_rlast;
`endif
//AXI4LITE control signals
wire [1-1:0] s_axi_control_awvalid;
wire [1-1:0] s_axi_control_awready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0] s_axi_control_awaddr;
wire [1-1:0] s_axi_control_wvalid;
wire [1-1:0] s_axi_control_wready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0] s_axi_control_wdata;
wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control_wstrb;
wire [1-1:0] s_axi_control_arvalid;
wire [1-1:0] s_axi_control_arready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0] s_axi_control_araddr;
wire [1-1:0] s_axi_control_rvalid;
wire [1-1:0] s_axi_control_rready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0] s_axi_control_rdata;
wire [2-1:0] s_axi_control_rresp;
wire [1-1:0] s_axi_control_bvalid;
wire [1-1:0] s_axi_control_bready;
wire [2-1:0] s_axi_control_bresp;
wire interrupt;

// DUT instantiation
bft_32_xbar_v2 #(
  .C_S_AXI_CONTROL_ADDR_WIDTH ( C_S_AXI_CONTROL_ADDR_WIDTH ),
  .C_S_AXI_CONTROL_DATA_WIDTH ( C_S_AXI_CONTROL_DATA_WIDTH ),
  .C_M00_AXI_ADDR_WIDTH       ( C_M00_AXI_ADDR_WIDTH       ),
  .C_M00_AXI_DATA_WIDTH       ( C_M00_AXI_DATA_WIDTH       ),
  .C_M01_AXI_ADDR_WIDTH       ( C_M01_AXI_ADDR_WIDTH       ),
  .C_M01_AXI_DATA_WIDTH       ( C_M01_AXI_DATA_WIDTH       ),
  .C_M02_AXI_ADDR_WIDTH       ( C_M02_AXI_ADDR_WIDTH       ),
  .C_M02_AXI_DATA_WIDTH       ( C_M02_AXI_DATA_WIDTH       ),
  .C_M03_AXI_ADDR_WIDTH       ( C_M03_AXI_ADDR_WIDTH       ),
  .C_M03_AXI_DATA_WIDTH       ( C_M03_AXI_DATA_WIDTH       ),
  .C_M04_AXI_ADDR_WIDTH       ( C_M04_AXI_ADDR_WIDTH       ),
  .C_M04_AXI_DATA_WIDTH       ( C_M04_AXI_DATA_WIDTH       ),
  .C_M05_AXI_ADDR_WIDTH       ( C_M05_AXI_ADDR_WIDTH       ),
  .C_M05_AXI_DATA_WIDTH       ( C_M05_AXI_DATA_WIDTH       ),
  .C_M06_AXI_ADDR_WIDTH       ( C_M06_AXI_ADDR_WIDTH       ),
  .C_M06_AXI_DATA_WIDTH       ( C_M06_AXI_DATA_WIDTH       ),
  .C_M07_AXI_ADDR_WIDTH       ( C_M07_AXI_ADDR_WIDTH       ),
  .C_M07_AXI_DATA_WIDTH       ( C_M07_AXI_DATA_WIDTH       ),
  .C_M08_AXI_ADDR_WIDTH       ( C_M08_AXI_ADDR_WIDTH       ),
  .C_M08_AXI_DATA_WIDTH       ( C_M08_AXI_DATA_WIDTH       ),
  .C_M09_AXI_ADDR_WIDTH       ( C_M09_AXI_ADDR_WIDTH       ),
  .C_M09_AXI_DATA_WIDTH       ( C_M09_AXI_DATA_WIDTH       ),
  .C_M10_AXI_ADDR_WIDTH       ( C_M10_AXI_ADDR_WIDTH       ),
  .C_M10_AXI_DATA_WIDTH       ( C_M10_AXI_DATA_WIDTH       ),
  .C_M11_AXI_ADDR_WIDTH       ( C_M11_AXI_ADDR_WIDTH       ),
  .C_M11_AXI_DATA_WIDTH       ( C_M11_AXI_DATA_WIDTH       ),
  .C_M12_AXI_ADDR_WIDTH       ( C_M12_AXI_ADDR_WIDTH       ),
  .C_M12_AXI_DATA_WIDTH       ( C_M12_AXI_DATA_WIDTH       ),
  .C_M13_AXI_ADDR_WIDTH       ( C_M13_AXI_ADDR_WIDTH       ),
  .C_M13_AXI_DATA_WIDTH       ( C_M13_AXI_DATA_WIDTH       ),
  .C_M14_AXI_ADDR_WIDTH       ( C_M14_AXI_ADDR_WIDTH       ),
  .C_M14_AXI_DATA_WIDTH       ( C_M14_AXI_DATA_WIDTH       ),
  .C_M15_AXI_ADDR_WIDTH       ( C_M15_AXI_ADDR_WIDTH       ),
  .C_M15_AXI_DATA_WIDTH       ( C_M15_AXI_DATA_WIDTH       )
)
inst_dut (
  .ap_clk                ( ap_clk                ),
  .m00_axi_awvalid       ( m00_axi_awvalid       ),
  .m00_axi_awready       ( m00_axi_awready       ),
  .m00_axi_awaddr        ( m00_axi_awaddr        ),
  .m00_axi_awlen         ( m00_axi_awlen         ),
  .m00_axi_wvalid        ( m00_axi_wvalid        ),
  .m00_axi_wready        ( m00_axi_wready        ),
  .m00_axi_wdata         ( m00_axi_wdata         ),
  .m00_axi_wstrb         ( m00_axi_wstrb         ),
  .m00_axi_wlast         ( m00_axi_wlast         ),
  .m00_axi_bvalid        ( m00_axi_bvalid        ),
  .m00_axi_bready        ( m00_axi_bready        ),
  .m00_axi_arvalid       ( m00_axi_arvalid       ),
  .m00_axi_arready       ( m00_axi_arready       ),
  .m00_axi_araddr        ( m00_axi_araddr        ),
  .m00_axi_arlen         ( m00_axi_arlen         ),
  .m00_axi_rvalid        ( m00_axi_rvalid        ),
  .m00_axi_rready        ( m00_axi_rready        ),
  .m00_axi_rdata         ( m00_axi_rdata         ),
  .m00_axi_rlast         ( m00_axi_rlast         )
  `ifdef XBARorM1orM0
  ,.m01_axi_awvalid       ( m01_axi_awvalid       ),
  .m01_axi_awready       ( m01_axi_awready       ),
  .m01_axi_awaddr        ( m01_axi_awaddr        ),
  .m01_axi_awlen         ( m01_axi_awlen         ),
  .m01_axi_wvalid        ( m01_axi_wvalid        ),
  .m01_axi_wready        ( m01_axi_wready        ),
  .m01_axi_wdata         ( m01_axi_wdata         ),
  .m01_axi_wstrb         ( m01_axi_wstrb         ),
  .m01_axi_wlast         ( m01_axi_wlast         ),
  .m01_axi_bvalid        ( m01_axi_bvalid        ),
  .m01_axi_bready        ( m01_axi_bready        ),
  .m01_axi_arvalid       ( m01_axi_arvalid       ),
  .m01_axi_arready       ( m01_axi_arready       ),
  .m01_axi_araddr        ( m01_axi_araddr        ),
  .m01_axi_arlen         ( m01_axi_arlen         ),
  .m01_axi_rvalid        ( m01_axi_rvalid        ),
  .m01_axi_rready        ( m01_axi_rready        ),
  .m01_axi_rdata         ( m01_axi_rdata         ),
  .m01_axi_rlast         ( m01_axi_rlast         ),
  .m02_axi_awvalid       ( m02_axi_awvalid       ),
  .m02_axi_awready       ( m02_axi_awready       ),
  .m02_axi_awaddr        ( m02_axi_awaddr        ),
  .m02_axi_awlen         ( m02_axi_awlen         ),
  .m02_axi_wvalid        ( m02_axi_wvalid        ),
  .m02_axi_wready        ( m02_axi_wready        ),
  .m02_axi_wdata         ( m02_axi_wdata         ),
  .m02_axi_wstrb         ( m02_axi_wstrb         ),
  .m02_axi_wlast         ( m02_axi_wlast         ),
  .m02_axi_bvalid        ( m02_axi_bvalid        ),
  .m02_axi_bready        ( m02_axi_bready        ),
  .m02_axi_arvalid       ( m02_axi_arvalid       ),
  .m02_axi_arready       ( m02_axi_arready       ),
  .m02_axi_araddr        ( m02_axi_araddr        ),
  .m02_axi_arlen         ( m02_axi_arlen         ),
  .m02_axi_rvalid        ( m02_axi_rvalid        ),
  .m02_axi_rready        ( m02_axi_rready        ),
  .m02_axi_rdata         ( m02_axi_rdata         ),
  .m02_axi_rlast         ( m02_axi_rlast         ),
  .m03_axi_awvalid       ( m03_axi_awvalid       ),
  .m03_axi_awready       ( m03_axi_awready       ),
  .m03_axi_awaddr        ( m03_axi_awaddr        ),
  .m03_axi_awlen         ( m03_axi_awlen         ),
  .m03_axi_wvalid        ( m03_axi_wvalid        ),
  .m03_axi_wready        ( m03_axi_wready        ),
  .m03_axi_wdata         ( m03_axi_wdata         ),
  .m03_axi_wstrb         ( m03_axi_wstrb         ),
  .m03_axi_wlast         ( m03_axi_wlast         ),
  .m03_axi_bvalid        ( m03_axi_bvalid        ),
  .m03_axi_bready        ( m03_axi_bready        ),
  .m03_axi_arvalid       ( m03_axi_arvalid       ),
  .m03_axi_arready       ( m03_axi_arready       ),
  .m03_axi_araddr        ( m03_axi_araddr        ),
  .m03_axi_arlen         ( m03_axi_arlen         ),
  .m03_axi_rvalid        ( m03_axi_rvalid        ),
  .m03_axi_rready        ( m03_axi_rready        ),
  .m03_axi_rdata         ( m03_axi_rdata         ),
  .m03_axi_rlast         ( m03_axi_rlast         )
  
`endif
`ifdef XBARorM1
  ,.m04_axi_awvalid       ( m04_axi_awvalid       ),
  .m04_axi_awready       ( m04_axi_awready       ),
  .m04_axi_awaddr        ( m04_axi_awaddr        ),
  .m04_axi_awlen         ( m04_axi_awlen         ),
  .m04_axi_wvalid        ( m04_axi_wvalid        ),
  .m04_axi_wready        ( m04_axi_wready        ),
  .m04_axi_wdata         ( m04_axi_wdata         ),
  .m04_axi_wstrb         ( m04_axi_wstrb         ),
  .m04_axi_wlast         ( m04_axi_wlast         ),
  .m04_axi_bvalid        ( m04_axi_bvalid        ),
  .m04_axi_bready        ( m04_axi_bready        ),
  .m04_axi_arvalid       ( m04_axi_arvalid       ),
  .m04_axi_arready       ( m04_axi_arready       ),
  .m04_axi_araddr        ( m04_axi_araddr        ),
  .m04_axi_arlen         ( m04_axi_arlen         ),
  .m04_axi_rvalid        ( m04_axi_rvalid        ),
  .m04_axi_rready        ( m04_axi_rready        ),
  .m04_axi_rdata         ( m04_axi_rdata         ),
  .m04_axi_rlast         ( m04_axi_rlast         ),
  .m05_axi_awvalid       ( m05_axi_awvalid       ),
  .m05_axi_awready       ( m05_axi_awready       ),
  .m05_axi_awaddr        ( m05_axi_awaddr        ),
  .m05_axi_awlen         ( m05_axi_awlen         ),
  .m05_axi_wvalid        ( m05_axi_wvalid        ),
  .m05_axi_wready        ( m05_axi_wready        ),
  .m05_axi_wdata         ( m05_axi_wdata         ),
  .m05_axi_wstrb         ( m05_axi_wstrb         ),
  .m05_axi_wlast         ( m05_axi_wlast         ),
  .m05_axi_bvalid        ( m05_axi_bvalid        ),
  .m05_axi_bready        ( m05_axi_bready        ),
  .m05_axi_arvalid       ( m05_axi_arvalid       ),
  .m05_axi_arready       ( m05_axi_arready       ),
  .m05_axi_araddr        ( m05_axi_araddr        ),
  .m05_axi_arlen         ( m05_axi_arlen         ),
  .m05_axi_rvalid        ( m05_axi_rvalid        ),
  .m05_axi_rready        ( m05_axi_rready        ),
  .m05_axi_rdata         ( m05_axi_rdata         ),
  .m05_axi_rlast         ( m05_axi_rlast         ),
  .m06_axi_awvalid       ( m06_axi_awvalid       ),
  .m06_axi_awready       ( m06_axi_awready       ),
  .m06_axi_awaddr        ( m06_axi_awaddr        ),
  .m06_axi_awlen         ( m06_axi_awlen         ),
  .m06_axi_wvalid        ( m06_axi_wvalid        ),
  .m06_axi_wready        ( m06_axi_wready        ),
  .m06_axi_wdata         ( m06_axi_wdata         ),
  .m06_axi_wstrb         ( m06_axi_wstrb         ),
  .m06_axi_wlast         ( m06_axi_wlast         ),
  .m06_axi_bvalid        ( m06_axi_bvalid        ),
  .m06_axi_bready        ( m06_axi_bready        ),
  .m06_axi_arvalid       ( m06_axi_arvalid       ),
  .m06_axi_arready       ( m06_axi_arready       ),
  .m06_axi_araddr        ( m06_axi_araddr        ),
  .m06_axi_arlen         ( m06_axi_arlen         ),
  .m06_axi_rvalid        ( m06_axi_rvalid        ),
  .m06_axi_rready        ( m06_axi_rready        ),
  .m06_axi_rdata         ( m06_axi_rdata         ),
  .m06_axi_rlast         ( m06_axi_rlast         ),
  .m07_axi_awvalid       ( m07_axi_awvalid       ),
  .m07_axi_awready       ( m07_axi_awready       ),
  .m07_axi_awaddr        ( m07_axi_awaddr        ),
  .m07_axi_awlen         ( m07_axi_awlen         ),
  .m07_axi_wvalid        ( m07_axi_wvalid        ),
  .m07_axi_wready        ( m07_axi_wready        ),
  .m07_axi_wdata         ( m07_axi_wdata         ),
  .m07_axi_wstrb         ( m07_axi_wstrb         ),
  .m07_axi_wlast         ( m07_axi_wlast         ),
  .m07_axi_bvalid        ( m07_axi_bvalid        ),
  .m07_axi_bready        ( m07_axi_bready        ),
  .m07_axi_arvalid       ( m07_axi_arvalid       ),
  .m07_axi_arready       ( m07_axi_arready       ),
  .m07_axi_araddr        ( m07_axi_araddr        ),
  .m07_axi_arlen         ( m07_axi_arlen         ),
  .m07_axi_rvalid        ( m07_axi_rvalid        ),
  .m07_axi_rready        ( m07_axi_rready        ),
  .m07_axi_rdata         ( m07_axi_rdata         ),
  .m07_axi_rlast         ( m07_axi_rlast         )
  
`endif
`ifdef XBARorM0 
  ,.m08_axi_awvalid       ( m08_axi_awvalid       ),
  .m08_axi_awready       ( m08_axi_awready       ),
  .m08_axi_awaddr        ( m08_axi_awaddr        ),
  .m08_axi_awlen         ( m08_axi_awlen         ),
  .m08_axi_wvalid        ( m08_axi_wvalid        ),
  .m08_axi_wready        ( m08_axi_wready        ),
  .m08_axi_wdata         ( m08_axi_wdata         ),
  .m08_axi_wstrb         ( m08_axi_wstrb         ),
  .m08_axi_wlast         ( m08_axi_wlast         ),
  .m08_axi_bvalid        ( m08_axi_bvalid        ),
  .m08_axi_bready        ( m08_axi_bready        ),
  .m08_axi_arvalid       ( m08_axi_arvalid       ),
  .m08_axi_arready       ( m08_axi_arready       ),
  .m08_axi_araddr        ( m08_axi_araddr        ),
  .m08_axi_arlen         ( m08_axi_arlen         ),
  .m08_axi_rvalid        ( m08_axi_rvalid        ),
  .m08_axi_rready        ( m08_axi_rready        ),
  .m08_axi_rdata         ( m08_axi_rdata         ),
  .m08_axi_rlast         ( m08_axi_rlast         ),
  .m09_axi_awvalid       ( m09_axi_awvalid       ),
  .m09_axi_awready       ( m09_axi_awready       ),
  .m09_axi_awaddr        ( m09_axi_awaddr        ),
  .m09_axi_awlen         ( m09_axi_awlen         ),
  .m09_axi_wvalid        ( m09_axi_wvalid        ),
  .m09_axi_wready        ( m09_axi_wready        ),
  .m09_axi_wdata         ( m09_axi_wdata         ),
  .m09_axi_wstrb         ( m09_axi_wstrb         ),
  .m09_axi_wlast         ( m09_axi_wlast         ),
  .m09_axi_bvalid        ( m09_axi_bvalid        ),
  .m09_axi_bready        ( m09_axi_bready        ),
  .m09_axi_arvalid       ( m09_axi_arvalid       ),
  .m09_axi_arready       ( m09_axi_arready       ),
  .m09_axi_araddr        ( m09_axi_araddr        ),
  .m09_axi_arlen         ( m09_axi_arlen         ),
  .m09_axi_rvalid        ( m09_axi_rvalid        ),
  .m09_axi_rready        ( m09_axi_rready        ),
  .m09_axi_rdata         ( m09_axi_rdata         ),
  .m09_axi_rlast         ( m09_axi_rlast         ),
  .m10_axi_awvalid       ( m10_axi_awvalid       ),
  .m10_axi_awready       ( m10_axi_awready       ),
  .m10_axi_awaddr        ( m10_axi_awaddr        ),
  .m10_axi_awlen         ( m10_axi_awlen         ),
  .m10_axi_wvalid        ( m10_axi_wvalid        ),
  .m10_axi_wready        ( m10_axi_wready        ),
  .m10_axi_wdata         ( m10_axi_wdata         ),
  .m10_axi_wstrb         ( m10_axi_wstrb         ),
  .m10_axi_wlast         ( m10_axi_wlast         ),
  .m10_axi_bvalid        ( m10_axi_bvalid        ),
  .m10_axi_bready        ( m10_axi_bready        ),
  .m10_axi_arvalid       ( m10_axi_arvalid       ),
  .m10_axi_arready       ( m10_axi_arready       ),
  .m10_axi_araddr        ( m10_axi_araddr        ),
  .m10_axi_arlen         ( m10_axi_arlen         ),
  .m10_axi_rvalid        ( m10_axi_rvalid        ),
  .m10_axi_rready        ( m10_axi_rready        ),
  .m10_axi_rdata         ( m10_axi_rdata         ),
  .m10_axi_rlast         ( m10_axi_rlast         ),
  .m11_axi_awvalid       ( m11_axi_awvalid       ),
  .m11_axi_awready       ( m11_axi_awready       ),
  .m11_axi_awaddr        ( m11_axi_awaddr        ),
  .m11_axi_awlen         ( m11_axi_awlen         ),
  .m11_axi_wvalid        ( m11_axi_wvalid        ),
  .m11_axi_wready        ( m11_axi_wready        ),
  .m11_axi_wdata         ( m11_axi_wdata         ),
  .m11_axi_wstrb         ( m11_axi_wstrb         ),
  .m11_axi_wlast         ( m11_axi_wlast         ),
  .m11_axi_bvalid        ( m11_axi_bvalid        ),
  .m11_axi_bready        ( m11_axi_bready        ),
  .m11_axi_arvalid       ( m11_axi_arvalid       ),
  .m11_axi_arready       ( m11_axi_arready       ),
  .m11_axi_araddr        ( m11_axi_araddr        ),
  .m11_axi_arlen         ( m11_axi_arlen         ),
  .m11_axi_rvalid        ( m11_axi_rvalid        ),
  .m11_axi_rready        ( m11_axi_rready        ),
  .m11_axi_rdata         ( m11_axi_rdata         ),
  .m11_axi_rlast         ( m11_axi_rlast         )
  `endif
`ifdef XBAR
  ,.m12_axi_awvalid       ( m12_axi_awvalid       ),
  .m12_axi_awready       ( m12_axi_awready       ),
  .m12_axi_awaddr        ( m12_axi_awaddr        ),
  .m12_axi_awlen         ( m12_axi_awlen         ),
  .m12_axi_wvalid        ( m12_axi_wvalid        ),
  .m12_axi_wready        ( m12_axi_wready        ),
  .m12_axi_wdata         ( m12_axi_wdata         ),
  .m12_axi_wstrb         ( m12_axi_wstrb         ),
  .m12_axi_wlast         ( m12_axi_wlast         ),
  .m12_axi_bvalid        ( m12_axi_bvalid        ),
  .m12_axi_bready        ( m12_axi_bready        ),
  .m12_axi_arvalid       ( m12_axi_arvalid       ),
  .m12_axi_arready       ( m12_axi_arready       ),
  .m12_axi_araddr        ( m12_axi_araddr        ),
  .m12_axi_arlen         ( m12_axi_arlen         ),
  .m12_axi_rvalid        ( m12_axi_rvalid        ),
  .m12_axi_rready        ( m12_axi_rready        ),
  .m12_axi_rdata         ( m12_axi_rdata         ),
  .m12_axi_rlast         ( m12_axi_rlast         ),
  .m13_axi_awvalid       ( m13_axi_awvalid       ),
  .m13_axi_awready       ( m13_axi_awready       ),
  .m13_axi_awaddr        ( m13_axi_awaddr        ),
  .m13_axi_awlen         ( m13_axi_awlen         ),
  .m13_axi_wvalid        ( m13_axi_wvalid        ),
  .m13_axi_wready        ( m13_axi_wready        ),
  .m13_axi_wdata         ( m13_axi_wdata         ),
  .m13_axi_wstrb         ( m13_axi_wstrb         ),
  .m13_axi_wlast         ( m13_axi_wlast         ),
  .m13_axi_bvalid        ( m13_axi_bvalid        ),
  .m13_axi_bready        ( m13_axi_bready        ),
  .m13_axi_arvalid       ( m13_axi_arvalid       ),
  .m13_axi_arready       ( m13_axi_arready       ),
  .m13_axi_araddr        ( m13_axi_araddr        ),
  .m13_axi_arlen         ( m13_axi_arlen         ),
  .m13_axi_rvalid        ( m13_axi_rvalid        ),
  .m13_axi_rready        ( m13_axi_rready        ),
  .m13_axi_rdata         ( m13_axi_rdata         ),
  .m13_axi_rlast         ( m13_axi_rlast         ),
  .m14_axi_awvalid       ( m14_axi_awvalid       ),
  .m14_axi_awready       ( m14_axi_awready       ),
  .m14_axi_awaddr        ( m14_axi_awaddr        ),
  .m14_axi_awlen         ( m14_axi_awlen         ),
  .m14_axi_wvalid        ( m14_axi_wvalid        ),
  .m14_axi_wready        ( m14_axi_wready        ),
  .m14_axi_wdata         ( m14_axi_wdata         ),
  .m14_axi_wstrb         ( m14_axi_wstrb         ),
  .m14_axi_wlast         ( m14_axi_wlast         ),
  .m14_axi_bvalid        ( m14_axi_bvalid        ),
  .m14_axi_bready        ( m14_axi_bready        ),
  .m14_axi_arvalid       ( m14_axi_arvalid       ),
  .m14_axi_arready       ( m14_axi_arready       ),
  .m14_axi_araddr        ( m14_axi_araddr        ),
  .m14_axi_arlen         ( m14_axi_arlen         ),
  .m14_axi_rvalid        ( m14_axi_rvalid        ),
  .m14_axi_rready        ( m14_axi_rready        ),
  .m14_axi_rdata         ( m14_axi_rdata         ),
  .m14_axi_rlast         ( m14_axi_rlast         ),
  .m15_axi_awvalid       ( m15_axi_awvalid       ),
  .m15_axi_awready       ( m15_axi_awready       ),
  .m15_axi_awaddr        ( m15_axi_awaddr        ),
  .m15_axi_awlen         ( m15_axi_awlen         ),
  .m15_axi_wvalid        ( m15_axi_wvalid        ),
  .m15_axi_wready        ( m15_axi_wready        ),
  .m15_axi_wdata         ( m15_axi_wdata         ),
  .m15_axi_wstrb         ( m15_axi_wstrb         ),
  .m15_axi_wlast         ( m15_axi_wlast         ),
  .m15_axi_bvalid        ( m15_axi_bvalid        ),
  .m15_axi_bready        ( m15_axi_bready        ),
  .m15_axi_arvalid       ( m15_axi_arvalid       ),
  .m15_axi_arready       ( m15_axi_arready       ),
  .m15_axi_araddr        ( m15_axi_araddr        ),
  .m15_axi_arlen         ( m15_axi_arlen         ),
  .m15_axi_rvalid        ( m15_axi_rvalid        ),
  .m15_axi_rready        ( m15_axi_rready        ),
  .m15_axi_rdata         ( m15_axi_rdata         ),
  .m15_axi_rlast         ( m15_axi_rlast         ),
   .m16_axi_awvalid ( m16_axi_awvalid ),
  .m16_axi_awready ( m16_axi_awready ),
  .m16_axi_awaddr  ( m16_axi_awaddr  ),
  .m16_axi_awlen   ( m16_axi_awlen   ),
  .m16_axi_wvalid  ( m16_axi_wvalid  ),
  .m16_axi_wready  ( m16_axi_wready  ),
  .m16_axi_wdata   ( m16_axi_wdata   ),
  .m16_axi_wstrb   ( m16_axi_wstrb   ),
  .m16_axi_wlast   ( m16_axi_wlast   ),
  .m16_axi_bvalid  ( m16_axi_bvalid  ),
  .m16_axi_bready  ( m16_axi_bready  ),
  .m16_axi_arvalid ( m16_axi_arvalid ),
  .m16_axi_arready ( m16_axi_arready ),
  .m16_axi_araddr  ( m16_axi_araddr  ),
  .m16_axi_arlen   ( m16_axi_arlen   ),
  .m16_axi_rvalid  ( m16_axi_rvalid  ),
  .m16_axi_rready  ( m16_axi_rready  ),
  .m16_axi_rdata   ( m16_axi_rdata   ),
  .m16_axi_rlast   ( m16_axi_rlast   ),
  .m17_axi_awvalid ( m17_axi_awvalid ),
  .m17_axi_awready ( m17_axi_awready ),
  .m17_axi_awaddr  ( m17_axi_awaddr  ),
  .m17_axi_awlen   ( m17_axi_awlen   ),
  .m17_axi_wvalid  ( m17_axi_wvalid  ),
  .m17_axi_wready  ( m17_axi_wready  ),
  .m17_axi_wdata   ( m17_axi_wdata   ),
  .m17_axi_wstrb   ( m17_axi_wstrb   ),
  .m17_axi_wlast   ( m17_axi_wlast   ),
  .m17_axi_bvalid  ( m17_axi_bvalid  ),
  .m17_axi_bready  ( m17_axi_bready  ),
  .m17_axi_arvalid ( m17_axi_arvalid ),
  .m17_axi_arready ( m17_axi_arready ),
  .m17_axi_araddr  ( m17_axi_araddr  ),
  .m17_axi_arlen   ( m17_axi_arlen   ),
  .m17_axi_rvalid  ( m17_axi_rvalid  ),
  .m17_axi_rready  ( m17_axi_rready  ),
  .m17_axi_rdata   ( m17_axi_rdata   ),
  .m17_axi_rlast   ( m17_axi_rlast   ),
  .m18_axi_awvalid ( m18_axi_awvalid ),
  .m18_axi_awready ( m18_axi_awready ),
  .m18_axi_awaddr  ( m18_axi_awaddr  ),
  .m18_axi_awlen   ( m18_axi_awlen   ),
  .m18_axi_wvalid  ( m18_axi_wvalid  ),
  .m18_axi_wready  ( m18_axi_wready  ),
  .m18_axi_wdata   ( m18_axi_wdata   ),
  .m18_axi_wstrb   ( m18_axi_wstrb   ),
  .m18_axi_wlast   ( m18_axi_wlast   ),
  .m18_axi_bvalid  ( m18_axi_bvalid  ),
  .m18_axi_bready  ( m18_axi_bready  ),
  .m18_axi_arvalid ( m18_axi_arvalid ),
  .m18_axi_arready ( m18_axi_arready ),
  .m18_axi_araddr  ( m18_axi_araddr  ),
  .m18_axi_arlen   ( m18_axi_arlen   ),
  .m18_axi_rvalid  ( m18_axi_rvalid  ),
  .m18_axi_rready  ( m18_axi_rready  ),
  .m18_axi_rdata   ( m18_axi_rdata   ),
  .m18_axi_rlast   ( m18_axi_rlast   ),
  .m19_axi_awvalid ( m19_axi_awvalid ),
  .m19_axi_awready ( m19_axi_awready ),
  .m19_axi_awaddr  ( m19_axi_awaddr  ),
  .m19_axi_awlen   ( m19_axi_awlen   ),
  .m19_axi_wvalid  ( m19_axi_wvalid  ),
  .m19_axi_wready  ( m19_axi_wready  ),
  .m19_axi_wdata   ( m19_axi_wdata   ),
  .m19_axi_wstrb   ( m19_axi_wstrb   ),
  .m19_axi_wlast   ( m19_axi_wlast   ),
  .m19_axi_bvalid  ( m19_axi_bvalid  ),
  .m19_axi_bready  ( m19_axi_bready  ),
  .m19_axi_arvalid ( m19_axi_arvalid ),
  .m19_axi_arready ( m19_axi_arready ),
  .m19_axi_araddr  ( m19_axi_araddr  ),
  .m19_axi_arlen   ( m19_axi_arlen   ),
  .m19_axi_rvalid  ( m19_axi_rvalid  ),
  .m19_axi_rready  ( m19_axi_rready  ),
  .m19_axi_rdata   ( m19_axi_rdata   ),
  .m19_axi_rlast   ( m19_axi_rlast   ),
  .m20_axi_awvalid ( m20_axi_awvalid ),
  .m20_axi_awready ( m20_axi_awready ),
  .m20_axi_awaddr  ( m20_axi_awaddr  ),
  .m20_axi_awlen   ( m20_axi_awlen   ),
  .m20_axi_wvalid  ( m20_axi_wvalid  ),
  .m20_axi_wready  ( m20_axi_wready  ),
  .m20_axi_wdata   ( m20_axi_wdata   ),
  .m20_axi_wstrb   ( m20_axi_wstrb   ),
  .m20_axi_wlast   ( m20_axi_wlast   ),
  .m20_axi_bvalid  ( m20_axi_bvalid  ),
  .m20_axi_bready  ( m20_axi_bready  ),
  .m20_axi_arvalid ( m20_axi_arvalid ),
  .m20_axi_arready ( m20_axi_arready ),
  .m20_axi_araddr  ( m20_axi_araddr  ),
  .m20_axi_arlen   ( m20_axi_arlen   ),
  .m20_axi_rvalid  ( m20_axi_rvalid  ),
  .m20_axi_rready  ( m20_axi_rready  ),
  .m20_axi_rdata   ( m20_axi_rdata   ),
  .m20_axi_rlast   ( m20_axi_rlast   ),
  .m21_axi_awvalid ( m21_axi_awvalid ),
  .m21_axi_awready ( m21_axi_awready ),
  .m21_axi_awaddr  ( m21_axi_awaddr  ),
  .m21_axi_awlen   ( m21_axi_awlen   ),
  .m21_axi_wvalid  ( m21_axi_wvalid  ),
  .m21_axi_wready  ( m21_axi_wready  ),
  .m21_axi_wdata   ( m21_axi_wdata   ),
  .m21_axi_wstrb   ( m21_axi_wstrb   ),
  .m21_axi_wlast   ( m21_axi_wlast   ),
  .m21_axi_bvalid  ( m21_axi_bvalid  ),
  .m21_axi_bready  ( m21_axi_bready  ),
  .m21_axi_arvalid ( m21_axi_arvalid ),
  .m21_axi_arready ( m21_axi_arready ),
  .m21_axi_araddr  ( m21_axi_araddr  ),
  .m21_axi_arlen   ( m21_axi_arlen   ),
  .m21_axi_rvalid  ( m21_axi_rvalid  ),
  .m21_axi_rready  ( m21_axi_rready  ),
  .m21_axi_rdata   ( m21_axi_rdata   ),
  .m21_axi_rlast   ( m21_axi_rlast   ),
  .m22_axi_awvalid ( m22_axi_awvalid ),
  .m22_axi_awready ( m22_axi_awready ),
  .m22_axi_awaddr  ( m22_axi_awaddr  ),
  .m22_axi_awlen   ( m22_axi_awlen   ),
  .m22_axi_wvalid  ( m22_axi_wvalid  ),
  .m22_axi_wready  ( m22_axi_wready  ),
  .m22_axi_wdata   ( m22_axi_wdata   ),
  .m22_axi_wstrb   ( m22_axi_wstrb   ),
  .m22_axi_wlast   ( m22_axi_wlast   ),
  .m22_axi_bvalid  ( m22_axi_bvalid  ),
  .m22_axi_bready  ( m22_axi_bready  ),
  .m22_axi_arvalid ( m22_axi_arvalid ),
  .m22_axi_arready ( m22_axi_arready ),
  .m22_axi_araddr  ( m22_axi_araddr  ),
  .m22_axi_arlen   ( m22_axi_arlen   ),
  .m22_axi_rvalid  ( m22_axi_rvalid  ),
  .m22_axi_rready  ( m22_axi_rready  ),
  .m22_axi_rdata   ( m22_axi_rdata   ),
  .m22_axi_rlast   ( m22_axi_rlast   ),
  .m23_axi_awvalid ( m23_axi_awvalid ),
  .m23_axi_awready ( m23_axi_awready ),
  .m23_axi_awaddr  ( m23_axi_awaddr  ),
  .m23_axi_awlen   ( m23_axi_awlen   ),
  .m23_axi_wvalid  ( m23_axi_wvalid  ),
  .m23_axi_wready  ( m23_axi_wready  ),
  .m23_axi_wdata   ( m23_axi_wdata   ),
  .m23_axi_wstrb   ( m23_axi_wstrb   ),
  .m23_axi_wlast   ( m23_axi_wlast   ),
  .m23_axi_bvalid  ( m23_axi_bvalid  ),
  .m23_axi_bready  ( m23_axi_bready  ),
  .m23_axi_arvalid ( m23_axi_arvalid ),
  .m23_axi_arready ( m23_axi_arready ),
  .m23_axi_araddr  ( m23_axi_araddr  ),
  .m23_axi_arlen   ( m23_axi_arlen   ),
  .m23_axi_rvalid  ( m23_axi_rvalid  ),
  .m23_axi_rready  ( m23_axi_rready  ),
  .m23_axi_rdata   ( m23_axi_rdata   ),
  .m23_axi_rlast   ( m23_axi_rlast   ),
  .m24_axi_awvalid ( m24_axi_awvalid ),
  .m24_axi_awready ( m24_axi_awready ),
  .m24_axi_awaddr  ( m24_axi_awaddr  ),
  .m24_axi_awlen   ( m24_axi_awlen   ),
  .m24_axi_wvalid  ( m24_axi_wvalid  ),
  .m24_axi_wready  ( m24_axi_wready  ),
  .m24_axi_wdata   ( m24_axi_wdata   ),
  .m24_axi_wstrb   ( m24_axi_wstrb   ),
  .m24_axi_wlast   ( m24_axi_wlast   ),
  .m24_axi_bvalid  ( m24_axi_bvalid  ),
  .m24_axi_bready  ( m24_axi_bready  ),
  .m24_axi_arvalid ( m24_axi_arvalid ),
  .m24_axi_arready ( m24_axi_arready ),
  .m24_axi_araddr  ( m24_axi_araddr  ),
  .m24_axi_arlen   ( m24_axi_arlen   ),
  .m24_axi_rvalid  ( m24_axi_rvalid  ),
  .m24_axi_rready  ( m24_axi_rready  ),
  .m24_axi_rdata   ( m24_axi_rdata   ),
  .m24_axi_rlast   ( m24_axi_rlast   ),
  .m25_axi_awvalid ( m25_axi_awvalid ),
  .m25_axi_awready ( m25_axi_awready ),
  .m25_axi_awaddr  ( m25_axi_awaddr  ),
  .m25_axi_awlen   ( m25_axi_awlen   ),
  .m25_axi_wvalid  ( m25_axi_wvalid  ),
  .m25_axi_wready  ( m25_axi_wready  ),
  .m25_axi_wdata   ( m25_axi_wdata   ),
  .m25_axi_wstrb   ( m25_axi_wstrb   ),
  .m25_axi_wlast   ( m25_axi_wlast   ),
  .m25_axi_bvalid  ( m25_axi_bvalid  ),
  .m25_axi_bready  ( m25_axi_bready  ),
  .m25_axi_arvalid ( m25_axi_arvalid ),
  .m25_axi_arready ( m25_axi_arready ),
  .m25_axi_araddr  ( m25_axi_araddr  ),
  .m25_axi_arlen   ( m25_axi_arlen   ),
  .m25_axi_rvalid  ( m25_axi_rvalid  ),
  .m25_axi_rready  ( m25_axi_rready  ),
  .m25_axi_rdata   ( m25_axi_rdata   ),
  .m25_axi_rlast   ( m25_axi_rlast   ),
  .m26_axi_awvalid ( m26_axi_awvalid ),
  .m26_axi_awready ( m26_axi_awready ),
  .m26_axi_awaddr  ( m26_axi_awaddr  ),
  .m26_axi_awlen   ( m26_axi_awlen   ),
  .m26_axi_wvalid  ( m26_axi_wvalid  ),
  .m26_axi_wready  ( m26_axi_wready  ),
  .m26_axi_wdata   ( m26_axi_wdata   ),
  .m26_axi_wstrb   ( m26_axi_wstrb   ),
  .m26_axi_wlast   ( m26_axi_wlast   ),
  .m26_axi_bvalid  ( m26_axi_bvalid  ),
  .m26_axi_bready  ( m26_axi_bready  ),
  .m26_axi_arvalid ( m26_axi_arvalid ),
  .m26_axi_arready ( m26_axi_arready ),
  .m26_axi_araddr  ( m26_axi_araddr  ),
  .m26_axi_arlen   ( m26_axi_arlen   ),
  .m26_axi_rvalid  ( m26_axi_rvalid  ),
  .m26_axi_rready  ( m26_axi_rready  ),
  .m26_axi_rdata   ( m26_axi_rdata   ),
  .m26_axi_rlast   ( m26_axi_rlast   ),
  .m27_axi_awvalid ( m27_axi_awvalid ),
  .m27_axi_awready ( m27_axi_awready ),
  .m27_axi_awaddr  ( m27_axi_awaddr  ),
  .m27_axi_awlen   ( m27_axi_awlen   ),
  .m27_axi_wvalid  ( m27_axi_wvalid  ),
  .m27_axi_wready  ( m27_axi_wready  ),
  .m27_axi_wdata   ( m27_axi_wdata   ),
  .m27_axi_wstrb   ( m27_axi_wstrb   ),
  .m27_axi_wlast   ( m27_axi_wlast   ),
  .m27_axi_bvalid  ( m27_axi_bvalid  ),
  .m27_axi_bready  ( m27_axi_bready  ),
  .m27_axi_arvalid ( m27_axi_arvalid ),
  .m27_axi_arready ( m27_axi_arready ),
  .m27_axi_araddr  ( m27_axi_araddr  ),
  .m27_axi_arlen   ( m27_axi_arlen   ),
  .m27_axi_rvalid  ( m27_axi_rvalid  ),
  .m27_axi_rready  ( m27_axi_rready  ),
  .m27_axi_rdata   ( m27_axi_rdata   ),
  .m27_axi_rlast   ( m27_axi_rlast   ),
  .m28_axi_awvalid ( m28_axi_awvalid ),
  .m28_axi_awready ( m28_axi_awready ),
  .m28_axi_awaddr  ( m28_axi_awaddr  ),
  .m28_axi_awlen   ( m28_axi_awlen   ),
  .m28_axi_wvalid  ( m28_axi_wvalid  ),
  .m28_axi_wready  ( m28_axi_wready  ),
  .m28_axi_wdata   ( m28_axi_wdata   ),
  .m28_axi_wstrb   ( m28_axi_wstrb   ),
  .m28_axi_wlast   ( m28_axi_wlast   ),
  .m28_axi_bvalid  ( m28_axi_bvalid  ),
  .m28_axi_bready  ( m28_axi_bready  ),
  .m28_axi_arvalid ( m28_axi_arvalid ),
  .m28_axi_arready ( m28_axi_arready ),
  .m28_axi_araddr  ( m28_axi_araddr  ),
  .m28_axi_arlen   ( m28_axi_arlen   ),
  .m28_axi_rvalid  ( m28_axi_rvalid  ),
  .m28_axi_rready  ( m28_axi_rready  ),
  .m28_axi_rdata   ( m28_axi_rdata   ),
  .m28_axi_rlast   ( m28_axi_rlast   ),
  .m29_axi_awvalid ( m29_axi_awvalid ),
  .m29_axi_awready ( m29_axi_awready ),
  .m29_axi_awaddr  ( m29_axi_awaddr  ),
  .m29_axi_awlen   ( m29_axi_awlen   ),
  .m29_axi_wvalid  ( m29_axi_wvalid  ),
  .m29_axi_wready  ( m29_axi_wready  ),
  .m29_axi_wdata   ( m29_axi_wdata   ),
  .m29_axi_wstrb   ( m29_axi_wstrb   ),
  .m29_axi_wlast   ( m29_axi_wlast   ),
  .m29_axi_bvalid  ( m29_axi_bvalid  ),
  .m29_axi_bready  ( m29_axi_bready  ),
  .m29_axi_arvalid ( m29_axi_arvalid ),
  .m29_axi_arready ( m29_axi_arready ),
  .m29_axi_araddr  ( m29_axi_araddr  ),
  .m29_axi_arlen   ( m29_axi_arlen   ),
  .m29_axi_rvalid  ( m29_axi_rvalid  ),
  .m29_axi_rready  ( m29_axi_rready  ),
  .m29_axi_rdata   ( m29_axi_rdata   ),
  .m29_axi_rlast   ( m29_axi_rlast   ),
  .m30_axi_awvalid ( m30_axi_awvalid ),
  .m30_axi_awready ( m30_axi_awready ),
  .m30_axi_awaddr  ( m30_axi_awaddr  ),
  .m30_axi_awlen   ( m30_axi_awlen   ),
  .m30_axi_wvalid  ( m30_axi_wvalid  ),
  .m30_axi_wready  ( m30_axi_wready  ),
  .m30_axi_wdata   ( m30_axi_wdata   ),
  .m30_axi_wstrb   ( m30_axi_wstrb   ),
  .m30_axi_wlast   ( m30_axi_wlast   ),
  .m30_axi_bvalid  ( m30_axi_bvalid  ),
  .m30_axi_bready  ( m30_axi_bready  ),
  .m30_axi_arvalid ( m30_axi_arvalid ),
  .m30_axi_arready ( m30_axi_arready ),
  .m30_axi_araddr  ( m30_axi_araddr  ),
  .m30_axi_arlen   ( m30_axi_arlen   ),
  .m30_axi_rvalid  ( m30_axi_rvalid  ),
  .m30_axi_rready  ( m30_axi_rready  ),
  .m30_axi_rdata   ( m30_axi_rdata   ),
  .m30_axi_rlast   ( m30_axi_rlast   ),
  .m31_axi_awvalid ( m31_axi_awvalid ),
  .m31_axi_awready ( m31_axi_awready ),
  .m31_axi_awaddr  ( m31_axi_awaddr  ),
  .m31_axi_awlen   ( m31_axi_awlen   ),
  .m31_axi_wvalid  ( m31_axi_wvalid  ),
  .m31_axi_wready  ( m31_axi_wready  ),
  .m31_axi_wdata   ( m31_axi_wdata   ),
  .m31_axi_wstrb   ( m31_axi_wstrb   ),
  .m31_axi_wlast   ( m31_axi_wlast   ),
  .m31_axi_bvalid  ( m31_axi_bvalid  ),
  .m31_axi_bready  ( m31_axi_bready  ),
  .m31_axi_arvalid ( m31_axi_arvalid ),
  .m31_axi_arready ( m31_axi_arready ),
  .m31_axi_araddr  ( m31_axi_araddr  ),
  .m31_axi_arlen   ( m31_axi_arlen   ),
  .m31_axi_rvalid  ( m31_axi_rvalid  ),
  .m31_axi_rready  ( m31_axi_rready  ),
  .m31_axi_rdata   ( m31_axi_rdata   ),
  .m31_axi_rlast   ( m31_axi_rlast   )
  `endif
  ,.s_axi_control_awvalid ( s_axi_control_awvalid ),
  .s_axi_control_awready ( s_axi_control_awready ),
  .s_axi_control_awaddr  ( s_axi_control_awaddr  ),
  .s_axi_control_wvalid  ( s_axi_control_wvalid  ),
  .s_axi_control_wready  ( s_axi_control_wready  ),
  .s_axi_control_wdata   ( s_axi_control_wdata   ),
  .s_axi_control_wstrb   ( s_axi_control_wstrb   ),
  .s_axi_control_arvalid ( s_axi_control_arvalid ),
  .s_axi_control_arready ( s_axi_control_arready ),
  .s_axi_control_araddr  ( s_axi_control_araddr  ),
  .s_axi_control_rvalid  ( s_axi_control_rvalid  ),
  .s_axi_control_rready  ( s_axi_control_rready  ),
  .s_axi_control_rdata   ( s_axi_control_rdata   ),
  .s_axi_control_rresp   ( s_axi_control_rresp   ),
  .s_axi_control_bvalid  ( s_axi_control_bvalid  ),
  .s_axi_control_bready  ( s_axi_control_bready  ),
  .s_axi_control_bresp   ( s_axi_control_bresp   ),
  .interrupt             ( interrupt             )
);

// Master Control instantiation
control_bft_32_xbar_v2_vip inst_control_bft_32_xbar_v2_vip (
  .aclk          ( ap_clk                ),
  .m_axi_awvalid ( s_axi_control_awvalid ),
  .m_axi_awready ( s_axi_control_awready ),
  .m_axi_awaddr  ( s_axi_control_awaddr  ),
  .m_axi_wvalid  ( s_axi_control_wvalid  ),
  .m_axi_wready  ( s_axi_control_wready  ),
  .m_axi_wdata   ( s_axi_control_wdata   ),
  .m_axi_wstrb   ( s_axi_control_wstrb   ),
  .m_axi_arvalid ( s_axi_control_arvalid ),
  .m_axi_arready ( s_axi_control_arready ),
  .m_axi_araddr  ( s_axi_control_araddr  ),
  .m_axi_rvalid  ( s_axi_control_rvalid  ),
  .m_axi_rready  ( s_axi_control_rready  ),
  .m_axi_rdata   ( s_axi_control_rdata   ),
  .m_axi_rresp   ( s_axi_control_rresp   ),
  .m_axi_bvalid  ( s_axi_control_bvalid  ),
  .m_axi_bready  ( s_axi_control_bready  ),
  .m_axi_bresp   ( s_axi_control_bresp   )
);

control_bft_32_xbar_v2_vip_mst_t  ctrl;

// Slave MM VIP instantiation
slv_m00_axi_vip inst_slv_m00_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m00_axi_awvalid ),
  .s_axi_awready ( m00_axi_awready ),
  .s_axi_awaddr  ( m00_axi_awaddr  ),
  .s_axi_awlen   ( m00_axi_awlen   ),
  .s_axi_wvalid  ( m00_axi_wvalid  ),
  .s_axi_wready  ( m00_axi_wready  ),
  .s_axi_wdata   ( m00_axi_wdata   ),
  .s_axi_wstrb   ( m00_axi_wstrb   ),
  .s_axi_wlast   ( m00_axi_wlast   ),
  .s_axi_bvalid  ( m00_axi_bvalid  ),
  .s_axi_bready  ( m00_axi_bready  ),
  .s_axi_arvalid ( m00_axi_arvalid ),
  .s_axi_arready ( m00_axi_arready ),
  .s_axi_araddr  ( m00_axi_araddr  ),
  .s_axi_arlen   ( m00_axi_arlen   ),
  .s_axi_rvalid  ( m00_axi_rvalid  ),
  .s_axi_rready  ( m00_axi_rready  ),
  .s_axi_rdata   ( m00_axi_rdata   ),
  .s_axi_rlast   ( m00_axi_rlast   )
);


slv_m00_axi_vip_slv_mem_t   m00_axi;
slv_m00_axi_vip_slv_t   m00_axi_slv;
`ifdef XBARorM1orM0

// Slave MM VIP instantiation
slv_m01_axi_vip inst_slv_m01_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m01_axi_awvalid ),
  .s_axi_awready ( m01_axi_awready ),
  .s_axi_awaddr  ( m01_axi_awaddr  ),
  .s_axi_awlen   ( m01_axi_awlen   ),
  .s_axi_wvalid  ( m01_axi_wvalid  ),
  .s_axi_wready  ( m01_axi_wready  ),
  .s_axi_wdata   ( m01_axi_wdata   ),
  .s_axi_wstrb   ( m01_axi_wstrb   ),
  .s_axi_wlast   ( m01_axi_wlast   ),
  .s_axi_bvalid  ( m01_axi_bvalid  ),
  .s_axi_bready  ( m01_axi_bready  ),
  .s_axi_arvalid ( m01_axi_arvalid ),
  .s_axi_arready ( m01_axi_arready ),
  .s_axi_araddr  ( m01_axi_araddr  ),
  .s_axi_arlen   ( m01_axi_arlen   ),
  .s_axi_rvalid  ( m01_axi_rvalid  ),
  .s_axi_rready  ( m01_axi_rready  ),
  .s_axi_rdata   ( m01_axi_rdata   ),
  .s_axi_rlast   ( m01_axi_rlast   )
);


slv_m01_axi_vip_slv_mem_t   m01_axi;
slv_m01_axi_vip_slv_t   m01_axi_slv;

// Slave MM VIP instantiation
slv_m02_axi_vip inst_slv_m02_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m02_axi_awvalid ),
  .s_axi_awready ( m02_axi_awready ),
  .s_axi_awaddr  ( m02_axi_awaddr  ),
  .s_axi_awlen   ( m02_axi_awlen   ),
  .s_axi_wvalid  ( m02_axi_wvalid  ),
  .s_axi_wready  ( m02_axi_wready  ),
  .s_axi_wdata   ( m02_axi_wdata   ),
  .s_axi_wstrb   ( m02_axi_wstrb   ),
  .s_axi_wlast   ( m02_axi_wlast   ),
  .s_axi_bvalid  ( m02_axi_bvalid  ),
  .s_axi_bready  ( m02_axi_bready  ),
  .s_axi_arvalid ( m02_axi_arvalid ),
  .s_axi_arready ( m02_axi_arready ),
  .s_axi_araddr  ( m02_axi_araddr  ),
  .s_axi_arlen   ( m02_axi_arlen   ),
  .s_axi_rvalid  ( m02_axi_rvalid  ),
  .s_axi_rready  ( m02_axi_rready  ),
  .s_axi_rdata   ( m02_axi_rdata   ),
  .s_axi_rlast   ( m02_axi_rlast   )
);


slv_m02_axi_vip_slv_mem_t   m02_axi;
slv_m02_axi_vip_slv_t   m02_axi_slv;

// Slave MM VIP instantiation
slv_m03_axi_vip inst_slv_m03_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m03_axi_awvalid ),
  .s_axi_awready ( m03_axi_awready ),
  .s_axi_awaddr  ( m03_axi_awaddr  ),
  .s_axi_awlen   ( m03_axi_awlen   ),
  .s_axi_wvalid  ( m03_axi_wvalid  ),
  .s_axi_wready  ( m03_axi_wready  ),
  .s_axi_wdata   ( m03_axi_wdata   ),
  .s_axi_wstrb   ( m03_axi_wstrb   ),
  .s_axi_wlast   ( m03_axi_wlast   ),
  .s_axi_bvalid  ( m03_axi_bvalid  ),
  .s_axi_bready  ( m03_axi_bready  ),
  .s_axi_arvalid ( m03_axi_arvalid ),
  .s_axi_arready ( m03_axi_arready ),
  .s_axi_araddr  ( m03_axi_araddr  ),
  .s_axi_arlen   ( m03_axi_arlen   ),
  .s_axi_rvalid  ( m03_axi_rvalid  ),
  .s_axi_rready  ( m03_axi_rready  ),
  .s_axi_rdata   ( m03_axi_rdata   ),
  .s_axi_rlast   ( m03_axi_rlast   )
);


slv_m03_axi_vip_slv_mem_t   m03_axi;
slv_m03_axi_vip_slv_t   m03_axi_slv;

`endif
`ifdef XBARorM1

// Slave MM VIP instantiation
slv_m04_axi_vip inst_slv_m04_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m04_axi_awvalid ),
  .s_axi_awready ( m04_axi_awready ),
  .s_axi_awaddr  ( m04_axi_awaddr  ),
  .s_axi_awlen   ( m04_axi_awlen   ),
  .s_axi_wvalid  ( m04_axi_wvalid  ),
  .s_axi_wready  ( m04_axi_wready  ),
  .s_axi_wdata   ( m04_axi_wdata   ),
  .s_axi_wstrb   ( m04_axi_wstrb   ),
  .s_axi_wlast   ( m04_axi_wlast   ),
  .s_axi_bvalid  ( m04_axi_bvalid  ),
  .s_axi_bready  ( m04_axi_bready  ),
  .s_axi_arvalid ( m04_axi_arvalid ),
  .s_axi_arready ( m04_axi_arready ),
  .s_axi_araddr  ( m04_axi_araddr  ),
  .s_axi_arlen   ( m04_axi_arlen   ),
  .s_axi_rvalid  ( m04_axi_rvalid  ),
  .s_axi_rready  ( m04_axi_rready  ),
  .s_axi_rdata   ( m04_axi_rdata   ),
  .s_axi_rlast   ( m04_axi_rlast   )
);


slv_m04_axi_vip_slv_mem_t   m04_axi;
slv_m04_axi_vip_slv_t   m04_axi_slv;

// Slave MM VIP instantiation
slv_m05_axi_vip inst_slv_m05_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m05_axi_awvalid ),
  .s_axi_awready ( m05_axi_awready ),
  .s_axi_awaddr  ( m05_axi_awaddr  ),
  .s_axi_awlen   ( m05_axi_awlen   ),
  .s_axi_wvalid  ( m05_axi_wvalid  ),
  .s_axi_wready  ( m05_axi_wready  ),
  .s_axi_wdata   ( m05_axi_wdata   ),
  .s_axi_wstrb   ( m05_axi_wstrb   ),
  .s_axi_wlast   ( m05_axi_wlast   ),
  .s_axi_bvalid  ( m05_axi_bvalid  ),
  .s_axi_bready  ( m05_axi_bready  ),
  .s_axi_arvalid ( m05_axi_arvalid ),
  .s_axi_arready ( m05_axi_arready ),
  .s_axi_araddr  ( m05_axi_araddr  ),
  .s_axi_arlen   ( m05_axi_arlen   ),
  .s_axi_rvalid  ( m05_axi_rvalid  ),
  .s_axi_rready  ( m05_axi_rready  ),
  .s_axi_rdata   ( m05_axi_rdata   ),
  .s_axi_rlast   ( m05_axi_rlast   )
);


slv_m05_axi_vip_slv_mem_t   m05_axi;
slv_m05_axi_vip_slv_t   m05_axi_slv;

// Slave MM VIP instantiation
slv_m06_axi_vip inst_slv_m06_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m06_axi_awvalid ),
  .s_axi_awready ( m06_axi_awready ),
  .s_axi_awaddr  ( m06_axi_awaddr  ),
  .s_axi_awlen   ( m06_axi_awlen   ),
  .s_axi_wvalid  ( m06_axi_wvalid  ),
  .s_axi_wready  ( m06_axi_wready  ),
  .s_axi_wdata   ( m06_axi_wdata   ),
  .s_axi_wstrb   ( m06_axi_wstrb   ),
  .s_axi_wlast   ( m06_axi_wlast   ),
  .s_axi_bvalid  ( m06_axi_bvalid  ),
  .s_axi_bready  ( m06_axi_bready  ),
  .s_axi_arvalid ( m06_axi_arvalid ),
  .s_axi_arready ( m06_axi_arready ),
  .s_axi_araddr  ( m06_axi_araddr  ),
  .s_axi_arlen   ( m06_axi_arlen   ),
  .s_axi_rvalid  ( m06_axi_rvalid  ),
  .s_axi_rready  ( m06_axi_rready  ),
  .s_axi_rdata   ( m06_axi_rdata   ),
  .s_axi_rlast   ( m06_axi_rlast   )
);


slv_m06_axi_vip_slv_mem_t   m06_axi;
slv_m06_axi_vip_slv_t   m06_axi_slv;

// Slave MM VIP instantiation
slv_m07_axi_vip inst_slv_m07_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m07_axi_awvalid ),
  .s_axi_awready ( m07_axi_awready ),
  .s_axi_awaddr  ( m07_axi_awaddr  ),
  .s_axi_awlen   ( m07_axi_awlen   ),
  .s_axi_wvalid  ( m07_axi_wvalid  ),
  .s_axi_wready  ( m07_axi_wready  ),
  .s_axi_wdata   ( m07_axi_wdata   ),
  .s_axi_wstrb   ( m07_axi_wstrb   ),
  .s_axi_wlast   ( m07_axi_wlast   ),
  .s_axi_bvalid  ( m07_axi_bvalid  ),
  .s_axi_bready  ( m07_axi_bready  ),
  .s_axi_arvalid ( m07_axi_arvalid ),
  .s_axi_arready ( m07_axi_arready ),
  .s_axi_araddr  ( m07_axi_araddr  ),
  .s_axi_arlen   ( m07_axi_arlen   ),
  .s_axi_rvalid  ( m07_axi_rvalid  ),
  .s_axi_rready  ( m07_axi_rready  ),
  .s_axi_rdata   ( m07_axi_rdata   ),
  .s_axi_rlast   ( m07_axi_rlast   )
);


slv_m07_axi_vip_slv_mem_t   m07_axi;
slv_m07_axi_vip_slv_t   m07_axi_slv;
`endif
`ifdef XBARorM0 
// Slave MM VIP instantiation
slv_m08_axi_vip inst_slv_m08_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m08_axi_awvalid ),
  .s_axi_awready ( m08_axi_awready ),
  .s_axi_awaddr  ( m08_axi_awaddr  ),
  .s_axi_awlen   ( m08_axi_awlen   ),
  .s_axi_wvalid  ( m08_axi_wvalid  ),
  .s_axi_wready  ( m08_axi_wready  ),
  .s_axi_wdata   ( m08_axi_wdata   ),
  .s_axi_wstrb   ( m08_axi_wstrb   ),
  .s_axi_wlast   ( m08_axi_wlast   ),
  .s_axi_bvalid  ( m08_axi_bvalid  ),
  .s_axi_bready  ( m08_axi_bready  ),
  .s_axi_arvalid ( m08_axi_arvalid ),
  .s_axi_arready ( m08_axi_arready ),
  .s_axi_araddr  ( m08_axi_araddr  ),
  .s_axi_arlen   ( m08_axi_arlen   ),
  .s_axi_rvalid  ( m08_axi_rvalid  ),
  .s_axi_rready  ( m08_axi_rready  ),
  .s_axi_rdata   ( m08_axi_rdata   ),
  .s_axi_rlast   ( m08_axi_rlast   )
);



slv_m08_axi_vip_slv_mem_t   m08_axi;
slv_m08_axi_vip_slv_t   m08_axi_slv;

// Slave MM VIP instantiation
slv_m09_axi_vip inst_slv_m09_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m09_axi_awvalid ),
  .s_axi_awready ( m09_axi_awready ),
  .s_axi_awaddr  ( m09_axi_awaddr  ),
  .s_axi_awlen   ( m09_axi_awlen   ),
  .s_axi_wvalid  ( m09_axi_wvalid  ),
  .s_axi_wready  ( m09_axi_wready  ),
  .s_axi_wdata   ( m09_axi_wdata   ),
  .s_axi_wstrb   ( m09_axi_wstrb   ),
  .s_axi_wlast   ( m09_axi_wlast   ),
  .s_axi_bvalid  ( m09_axi_bvalid  ),
  .s_axi_bready  ( m09_axi_bready  ),
  .s_axi_arvalid ( m09_axi_arvalid ),
  .s_axi_arready ( m09_axi_arready ),
  .s_axi_araddr  ( m09_axi_araddr  ),
  .s_axi_arlen   ( m09_axi_arlen   ),
  .s_axi_rvalid  ( m09_axi_rvalid  ),
  .s_axi_rready  ( m09_axi_rready  ),
  .s_axi_rdata   ( m09_axi_rdata   ),
  .s_axi_rlast   ( m09_axi_rlast   )
);


slv_m09_axi_vip_slv_mem_t   m09_axi;
slv_m09_axi_vip_slv_t   m09_axi_slv;

// Slave MM VIP instantiation
slv_m10_axi_vip inst_slv_m10_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m10_axi_awvalid ),
  .s_axi_awready ( m10_axi_awready ),
  .s_axi_awaddr  ( m10_axi_awaddr  ),
  .s_axi_awlen   ( m10_axi_awlen   ),
  .s_axi_wvalid  ( m10_axi_wvalid  ),
  .s_axi_wready  ( m10_axi_wready  ),
  .s_axi_wdata   ( m10_axi_wdata   ),
  .s_axi_wstrb   ( m10_axi_wstrb   ),
  .s_axi_wlast   ( m10_axi_wlast   ),
  .s_axi_bvalid  ( m10_axi_bvalid  ),
  .s_axi_bready  ( m10_axi_bready  ),
  .s_axi_arvalid ( m10_axi_arvalid ),
  .s_axi_arready ( m10_axi_arready ),
  .s_axi_araddr  ( m10_axi_araddr  ),
  .s_axi_arlen   ( m10_axi_arlen   ),
  .s_axi_rvalid  ( m10_axi_rvalid  ),
  .s_axi_rready  ( m10_axi_rready  ),
  .s_axi_rdata   ( m10_axi_rdata   ),
  .s_axi_rlast   ( m10_axi_rlast   )
);


slv_m10_axi_vip_slv_mem_t   m10_axi;
slv_m10_axi_vip_slv_t   m10_axi_slv;

// Slave MM VIP instantiation
slv_m11_axi_vip inst_slv_m11_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m11_axi_awvalid ),
  .s_axi_awready ( m11_axi_awready ),
  .s_axi_awaddr  ( m11_axi_awaddr  ),
  .s_axi_awlen   ( m11_axi_awlen   ),
  .s_axi_wvalid  ( m11_axi_wvalid  ),
  .s_axi_wready  ( m11_axi_wready  ),
  .s_axi_wdata   ( m11_axi_wdata   ),
  .s_axi_wstrb   ( m11_axi_wstrb   ),
  .s_axi_wlast   ( m11_axi_wlast   ),
  .s_axi_bvalid  ( m11_axi_bvalid  ),
  .s_axi_bready  ( m11_axi_bready  ),
  .s_axi_arvalid ( m11_axi_arvalid ),
  .s_axi_arready ( m11_axi_arready ),
  .s_axi_araddr  ( m11_axi_araddr  ),
  .s_axi_arlen   ( m11_axi_arlen   ),
  .s_axi_rvalid  ( m11_axi_rvalid  ),
  .s_axi_rready  ( m11_axi_rready  ),
  .s_axi_rdata   ( m11_axi_rdata   ),
  .s_axi_rlast   ( m11_axi_rlast   )
);


slv_m11_axi_vip_slv_mem_t   m11_axi;
slv_m11_axi_vip_slv_t   m11_axi_slv;

`endif
`ifdef XBAR 
// Slave MM VIP instantiation
slv_m12_axi_vip inst_slv_m12_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m12_axi_awvalid ),
  .s_axi_awready ( m12_axi_awready ),
  .s_axi_awaddr  ( m12_axi_awaddr  ),
  .s_axi_awlen   ( m12_axi_awlen   ),
  .s_axi_wvalid  ( m12_axi_wvalid  ),
  .s_axi_wready  ( m12_axi_wready  ),
  .s_axi_wdata   ( m12_axi_wdata   ),
  .s_axi_wstrb   ( m12_axi_wstrb   ),
  .s_axi_wlast   ( m12_axi_wlast   ),
  .s_axi_bvalid  ( m12_axi_bvalid  ),
  .s_axi_bready  ( m12_axi_bready  ),
  .s_axi_arvalid ( m12_axi_arvalid ),
  .s_axi_arready ( m12_axi_arready ),
  .s_axi_araddr  ( m12_axi_araddr  ),
  .s_axi_arlen   ( m12_axi_arlen   ),
  .s_axi_rvalid  ( m12_axi_rvalid  ),
  .s_axi_rready  ( m12_axi_rready  ),
  .s_axi_rdata   ( m12_axi_rdata   ),
  .s_axi_rlast   ( m12_axi_rlast   )
);


slv_m12_axi_vip_slv_mem_t   m12_axi;
slv_m12_axi_vip_slv_t   m12_axi_slv;

// Slave MM VIP instantiation
slv_m13_axi_vip inst_slv_m13_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m13_axi_awvalid ),
  .s_axi_awready ( m13_axi_awready ),
  .s_axi_awaddr  ( m13_axi_awaddr  ),
  .s_axi_awlen   ( m13_axi_awlen   ),
  .s_axi_wvalid  ( m13_axi_wvalid  ),
  .s_axi_wready  ( m13_axi_wready  ),
  .s_axi_wdata   ( m13_axi_wdata   ),
  .s_axi_wstrb   ( m13_axi_wstrb   ),
  .s_axi_wlast   ( m13_axi_wlast   ),
  .s_axi_bvalid  ( m13_axi_bvalid  ),
  .s_axi_bready  ( m13_axi_bready  ),
  .s_axi_arvalid ( m13_axi_arvalid ),
  .s_axi_arready ( m13_axi_arready ),
  .s_axi_araddr  ( m13_axi_araddr  ),
  .s_axi_arlen   ( m13_axi_arlen   ),
  .s_axi_rvalid  ( m13_axi_rvalid  ),
  .s_axi_rready  ( m13_axi_rready  ),
  .s_axi_rdata   ( m13_axi_rdata   ),
  .s_axi_rlast   ( m13_axi_rlast   )
);


slv_m13_axi_vip_slv_mem_t   m13_axi;
slv_m13_axi_vip_slv_t   m13_axi_slv;

// Slave MM VIP instantiation
slv_m14_axi_vip inst_slv_m14_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m14_axi_awvalid ),
  .s_axi_awready ( m14_axi_awready ),
  .s_axi_awaddr  ( m14_axi_awaddr  ),
  .s_axi_awlen   ( m14_axi_awlen   ),
  .s_axi_wvalid  ( m14_axi_wvalid  ),
  .s_axi_wready  ( m14_axi_wready  ),
  .s_axi_wdata   ( m14_axi_wdata   ),
  .s_axi_wstrb   ( m14_axi_wstrb   ),
  .s_axi_wlast   ( m14_axi_wlast   ),
  .s_axi_bvalid  ( m14_axi_bvalid  ),
  .s_axi_bready  ( m14_axi_bready  ),
  .s_axi_arvalid ( m14_axi_arvalid ),
  .s_axi_arready ( m14_axi_arready ),
  .s_axi_araddr  ( m14_axi_araddr  ),
  .s_axi_arlen   ( m14_axi_arlen   ),
  .s_axi_rvalid  ( m14_axi_rvalid  ),
  .s_axi_rready  ( m14_axi_rready  ),
  .s_axi_rdata   ( m14_axi_rdata   ),
  .s_axi_rlast   ( m14_axi_rlast   )
);


slv_m14_axi_vip_slv_mem_t   m14_axi;
slv_m14_axi_vip_slv_t   m14_axi_slv;

// Slave MM VIP instantiation
slv_m15_axi_vip inst_slv_m15_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m15_axi_awvalid ),
  .s_axi_awready ( m15_axi_awready ),
  .s_axi_awaddr  ( m15_axi_awaddr  ),
  .s_axi_awlen   ( m15_axi_awlen   ),
  .s_axi_wvalid  ( m15_axi_wvalid  ),
  .s_axi_wready  ( m15_axi_wready  ),
  .s_axi_wdata   ( m15_axi_wdata   ),
  .s_axi_wstrb   ( m15_axi_wstrb   ),
  .s_axi_wlast   ( m15_axi_wlast   ),
  .s_axi_bvalid  ( m15_axi_bvalid  ),
  .s_axi_bready  ( m15_axi_bready  ),
  .s_axi_arvalid ( m15_axi_arvalid ),
  .s_axi_arready ( m15_axi_arready ),
  .s_axi_araddr  ( m15_axi_araddr  ),
  .s_axi_arlen   ( m15_axi_arlen   ),
  .s_axi_rvalid  ( m15_axi_rvalid  ),
  .s_axi_rready  ( m15_axi_rready  ),
  .s_axi_rdata   ( m15_axi_rdata   ),
  .s_axi_rlast   ( m15_axi_rlast   )
);


slv_m15_axi_vip_slv_mem_t   m15_axi;
slv_m15_axi_vip_slv_t   m15_axi_slv;

// Slave MM VIP instantiation
slv_m16_axi_vip inst_slv_m16_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m16_axi_awvalid ),
  .s_axi_awready ( m16_axi_awready ),
  .s_axi_awaddr  ( m16_axi_awaddr  ),
  .s_axi_awlen   ( m16_axi_awlen   ),
  .s_axi_wvalid  ( m16_axi_wvalid  ),
  .s_axi_wready  ( m16_axi_wready  ),
  .s_axi_wdata   ( m16_axi_wdata   ),
  .s_axi_wstrb   ( m16_axi_wstrb   ),
  .s_axi_wlast   ( m16_axi_wlast   ),
  .s_axi_bvalid  ( m16_axi_bvalid  ),
  .s_axi_bready  ( m16_axi_bready  ),
  .s_axi_arvalid ( m16_axi_arvalid ),
  .s_axi_arready ( m16_axi_arready ),
  .s_axi_araddr  ( m16_axi_araddr  ),
  .s_axi_arlen   ( m16_axi_arlen   ),
  .s_axi_rvalid  ( m16_axi_rvalid  ),
  .s_axi_rready  ( m16_axi_rready  ),
  .s_axi_rdata   ( m16_axi_rdata   ),
  .s_axi_rlast   ( m16_axi_rlast   )
);


slv_m16_axi_vip_slv_mem_t   m16_axi;
slv_m16_axi_vip_slv_t   m16_axi_slv;

// Slave MM VIP instantiation
slv_m17_axi_vip inst_slv_m17_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m17_axi_awvalid ),
  .s_axi_awready ( m17_axi_awready ),
  .s_axi_awaddr  ( m17_axi_awaddr  ),
  .s_axi_awlen   ( m17_axi_awlen   ),
  .s_axi_wvalid  ( m17_axi_wvalid  ),
  .s_axi_wready  ( m17_axi_wready  ),
  .s_axi_wdata   ( m17_axi_wdata   ),
  .s_axi_wstrb   ( m17_axi_wstrb   ),
  .s_axi_wlast   ( m17_axi_wlast   ),
  .s_axi_bvalid  ( m17_axi_bvalid  ),
  .s_axi_bready  ( m17_axi_bready  ),
  .s_axi_arvalid ( m17_axi_arvalid ),
  .s_axi_arready ( m17_axi_arready ),
  .s_axi_araddr  ( m17_axi_araddr  ),
  .s_axi_arlen   ( m17_axi_arlen   ),
  .s_axi_rvalid  ( m17_axi_rvalid  ),
  .s_axi_rready  ( m17_axi_rready  ),
  .s_axi_rdata   ( m17_axi_rdata   ),
  .s_axi_rlast   ( m17_axi_rlast   )
);


slv_m17_axi_vip_slv_mem_t   m17_axi;
slv_m17_axi_vip_slv_t   m17_axi_slv;

// Slave MM VIP instantiation
slv_m18_axi_vip inst_slv_m18_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m18_axi_awvalid ),
  .s_axi_awready ( m18_axi_awready ),
  .s_axi_awaddr  ( m18_axi_awaddr  ),
  .s_axi_awlen   ( m18_axi_awlen   ),
  .s_axi_wvalid  ( m18_axi_wvalid  ),
  .s_axi_wready  ( m18_axi_wready  ),
  .s_axi_wdata   ( m18_axi_wdata   ),
  .s_axi_wstrb   ( m18_axi_wstrb   ),
  .s_axi_wlast   ( m18_axi_wlast   ),
  .s_axi_bvalid  ( m18_axi_bvalid  ),
  .s_axi_bready  ( m18_axi_bready  ),
  .s_axi_arvalid ( m18_axi_arvalid ),
  .s_axi_arready ( m18_axi_arready ),
  .s_axi_araddr  ( m18_axi_araddr  ),
  .s_axi_arlen   ( m18_axi_arlen   ),
  .s_axi_rvalid  ( m18_axi_rvalid  ),
  .s_axi_rready  ( m18_axi_rready  ),
  .s_axi_rdata   ( m18_axi_rdata   ),
  .s_axi_rlast   ( m18_axi_rlast   )
);


slv_m18_axi_vip_slv_mem_t   m18_axi;
slv_m18_axi_vip_slv_t   m18_axi_slv;

// Slave MM VIP instantiation
slv_m19_axi_vip inst_slv_m19_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m19_axi_awvalid ),
  .s_axi_awready ( m19_axi_awready ),
  .s_axi_awaddr  ( m19_axi_awaddr  ),
  .s_axi_awlen   ( m19_axi_awlen   ),
  .s_axi_wvalid  ( m19_axi_wvalid  ),
  .s_axi_wready  ( m19_axi_wready  ),
  .s_axi_wdata   ( m19_axi_wdata   ),
  .s_axi_wstrb   ( m19_axi_wstrb   ),
  .s_axi_wlast   ( m19_axi_wlast   ),
  .s_axi_bvalid  ( m19_axi_bvalid  ),
  .s_axi_bready  ( m19_axi_bready  ),
  .s_axi_arvalid ( m19_axi_arvalid ),
  .s_axi_arready ( m19_axi_arready ),
  .s_axi_araddr  ( m19_axi_araddr  ),
  .s_axi_arlen   ( m19_axi_arlen   ),
  .s_axi_rvalid  ( m19_axi_rvalid  ),
  .s_axi_rready  ( m19_axi_rready  ),
  .s_axi_rdata   ( m19_axi_rdata   ),
  .s_axi_rlast   ( m19_axi_rlast   )
);


slv_m19_axi_vip_slv_mem_t   m19_axi;
slv_m19_axi_vip_slv_t   m19_axi_slv;

// Slave MM VIP instantiation
slv_m20_axi_vip inst_slv_m20_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m20_axi_awvalid ),
  .s_axi_awready ( m20_axi_awready ),
  .s_axi_awaddr  ( m20_axi_awaddr  ),
  .s_axi_awlen   ( m20_axi_awlen   ),
  .s_axi_wvalid  ( m20_axi_wvalid  ),
  .s_axi_wready  ( m20_axi_wready  ),
  .s_axi_wdata   ( m20_axi_wdata   ),
  .s_axi_wstrb   ( m20_axi_wstrb   ),
  .s_axi_wlast   ( m20_axi_wlast   ),
  .s_axi_bvalid  ( m20_axi_bvalid  ),
  .s_axi_bready  ( m20_axi_bready  ),
  .s_axi_arvalid ( m20_axi_arvalid ),
  .s_axi_arready ( m20_axi_arready ),
  .s_axi_araddr  ( m20_axi_araddr  ),
  .s_axi_arlen   ( m20_axi_arlen   ),
  .s_axi_rvalid  ( m20_axi_rvalid  ),
  .s_axi_rready  ( m20_axi_rready  ),
  .s_axi_rdata   ( m20_axi_rdata   ),
  .s_axi_rlast   ( m20_axi_rlast   )
);


slv_m20_axi_vip_slv_mem_t   m20_axi;
slv_m20_axi_vip_slv_t   m20_axi_slv;

// Slave MM VIP instantiation
slv_m21_axi_vip inst_slv_m21_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m21_axi_awvalid ),
  .s_axi_awready ( m21_axi_awready ),
  .s_axi_awaddr  ( m21_axi_awaddr  ),
  .s_axi_awlen   ( m21_axi_awlen   ),
  .s_axi_wvalid  ( m21_axi_wvalid  ),
  .s_axi_wready  ( m21_axi_wready  ),
  .s_axi_wdata   ( m21_axi_wdata   ),
  .s_axi_wstrb   ( m21_axi_wstrb   ),
  .s_axi_wlast   ( m21_axi_wlast   ),
  .s_axi_bvalid  ( m21_axi_bvalid  ),
  .s_axi_bready  ( m21_axi_bready  ),
  .s_axi_arvalid ( m21_axi_arvalid ),
  .s_axi_arready ( m21_axi_arready ),
  .s_axi_araddr  ( m21_axi_araddr  ),
  .s_axi_arlen   ( m21_axi_arlen   ),
  .s_axi_rvalid  ( m21_axi_rvalid  ),
  .s_axi_rready  ( m21_axi_rready  ),
  .s_axi_rdata   ( m21_axi_rdata   ),
  .s_axi_rlast   ( m21_axi_rlast   )
);


slv_m21_axi_vip_slv_mem_t   m21_axi;
slv_m21_axi_vip_slv_t   m21_axi_slv;

// Slave MM VIP instantiation
slv_m22_axi_vip inst_slv_m22_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m22_axi_awvalid ),
  .s_axi_awready ( m22_axi_awready ),
  .s_axi_awaddr  ( m22_axi_awaddr  ),
  .s_axi_awlen   ( m22_axi_awlen   ),
  .s_axi_wvalid  ( m22_axi_wvalid  ),
  .s_axi_wready  ( m22_axi_wready  ),
  .s_axi_wdata   ( m22_axi_wdata   ),
  .s_axi_wstrb   ( m22_axi_wstrb   ),
  .s_axi_wlast   ( m22_axi_wlast   ),
  .s_axi_bvalid  ( m22_axi_bvalid  ),
  .s_axi_bready  ( m22_axi_bready  ),
  .s_axi_arvalid ( m22_axi_arvalid ),
  .s_axi_arready ( m22_axi_arready ),
  .s_axi_araddr  ( m22_axi_araddr  ),
  .s_axi_arlen   ( m22_axi_arlen   ),
  .s_axi_rvalid  ( m22_axi_rvalid  ),
  .s_axi_rready  ( m22_axi_rready  ),
  .s_axi_rdata   ( m22_axi_rdata   ),
  .s_axi_rlast   ( m22_axi_rlast   )
);


slv_m22_axi_vip_slv_mem_t   m22_axi;
slv_m22_axi_vip_slv_t   m22_axi_slv;

// Slave MM VIP instantiation
slv_m23_axi_vip inst_slv_m23_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m23_axi_awvalid ),
  .s_axi_awready ( m23_axi_awready ),
  .s_axi_awaddr  ( m23_axi_awaddr  ),
  .s_axi_awlen   ( m23_axi_awlen   ),
  .s_axi_wvalid  ( m23_axi_wvalid  ),
  .s_axi_wready  ( m23_axi_wready  ),
  .s_axi_wdata   ( m23_axi_wdata   ),
  .s_axi_wstrb   ( m23_axi_wstrb   ),
  .s_axi_wlast   ( m23_axi_wlast   ),
  .s_axi_bvalid  ( m23_axi_bvalid  ),
  .s_axi_bready  ( m23_axi_bready  ),
  .s_axi_arvalid ( m23_axi_arvalid ),
  .s_axi_arready ( m23_axi_arready ),
  .s_axi_araddr  ( m23_axi_araddr  ),
  .s_axi_arlen   ( m23_axi_arlen   ),
  .s_axi_rvalid  ( m23_axi_rvalid  ),
  .s_axi_rready  ( m23_axi_rready  ),
  .s_axi_rdata   ( m23_axi_rdata   ),
  .s_axi_rlast   ( m23_axi_rlast   )
);


slv_m23_axi_vip_slv_mem_t   m23_axi;
slv_m23_axi_vip_slv_t   m23_axi_slv;

// Slave MM VIP instantiation
slv_m24_axi_vip inst_slv_m24_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m24_axi_awvalid ),
  .s_axi_awready ( m24_axi_awready ),
  .s_axi_awaddr  ( m24_axi_awaddr  ),
  .s_axi_awlen   ( m24_axi_awlen   ),
  .s_axi_wvalid  ( m24_axi_wvalid  ),
  .s_axi_wready  ( m24_axi_wready  ),
  .s_axi_wdata   ( m24_axi_wdata   ),
  .s_axi_wstrb   ( m24_axi_wstrb   ),
  .s_axi_wlast   ( m24_axi_wlast   ),
  .s_axi_bvalid  ( m24_axi_bvalid  ),
  .s_axi_bready  ( m24_axi_bready  ),
  .s_axi_arvalid ( m24_axi_arvalid ),
  .s_axi_arready ( m24_axi_arready ),
  .s_axi_araddr  ( m24_axi_araddr  ),
  .s_axi_arlen   ( m24_axi_arlen   ),
  .s_axi_rvalid  ( m24_axi_rvalid  ),
  .s_axi_rready  ( m24_axi_rready  ),
  .s_axi_rdata   ( m24_axi_rdata   ),
  .s_axi_rlast   ( m24_axi_rlast   )
);


slv_m24_axi_vip_slv_mem_t   m24_axi;
slv_m24_axi_vip_slv_t   m24_axi_slv;

// Slave MM VIP instantiation
slv_m25_axi_vip inst_slv_m25_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m25_axi_awvalid ),
  .s_axi_awready ( m25_axi_awready ),
  .s_axi_awaddr  ( m25_axi_awaddr  ),
  .s_axi_awlen   ( m25_axi_awlen   ),
  .s_axi_wvalid  ( m25_axi_wvalid  ),
  .s_axi_wready  ( m25_axi_wready  ),
  .s_axi_wdata   ( m25_axi_wdata   ),
  .s_axi_wstrb   ( m25_axi_wstrb   ),
  .s_axi_wlast   ( m25_axi_wlast   ),
  .s_axi_bvalid  ( m25_axi_bvalid  ),
  .s_axi_bready  ( m25_axi_bready  ),
  .s_axi_arvalid ( m25_axi_arvalid ),
  .s_axi_arready ( m25_axi_arready ),
  .s_axi_araddr  ( m25_axi_araddr  ),
  .s_axi_arlen   ( m25_axi_arlen   ),
  .s_axi_rvalid  ( m25_axi_rvalid  ),
  .s_axi_rready  ( m25_axi_rready  ),
  .s_axi_rdata   ( m25_axi_rdata   ),
  .s_axi_rlast   ( m25_axi_rlast   )
);


slv_m25_axi_vip_slv_mem_t   m25_axi;
slv_m25_axi_vip_slv_t   m25_axi_slv;

// Slave MM VIP instantiation
slv_m26_axi_vip inst_slv_m26_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m26_axi_awvalid ),
  .s_axi_awready ( m26_axi_awready ),
  .s_axi_awaddr  ( m26_axi_awaddr  ),
  .s_axi_awlen   ( m26_axi_awlen   ),
  .s_axi_wvalid  ( m26_axi_wvalid  ),
  .s_axi_wready  ( m26_axi_wready  ),
  .s_axi_wdata   ( m26_axi_wdata   ),
  .s_axi_wstrb   ( m26_axi_wstrb   ),
  .s_axi_wlast   ( m26_axi_wlast   ),
  .s_axi_bvalid  ( m26_axi_bvalid  ),
  .s_axi_bready  ( m26_axi_bready  ),
  .s_axi_arvalid ( m26_axi_arvalid ),
  .s_axi_arready ( m26_axi_arready ),
  .s_axi_araddr  ( m26_axi_araddr  ),
  .s_axi_arlen   ( m26_axi_arlen   ),
  .s_axi_rvalid  ( m26_axi_rvalid  ),
  .s_axi_rready  ( m26_axi_rready  ),
  .s_axi_rdata   ( m26_axi_rdata   ),
  .s_axi_rlast   ( m26_axi_rlast   )
);


slv_m26_axi_vip_slv_mem_t   m26_axi;
slv_m26_axi_vip_slv_t   m26_axi_slv;

// Slave MM VIP instantiation
slv_m27_axi_vip inst_slv_m27_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m27_axi_awvalid ),
  .s_axi_awready ( m27_axi_awready ),
  .s_axi_awaddr  ( m27_axi_awaddr  ),
  .s_axi_awlen   ( m27_axi_awlen   ),
  .s_axi_wvalid  ( m27_axi_wvalid  ),
  .s_axi_wready  ( m27_axi_wready  ),
  .s_axi_wdata   ( m27_axi_wdata   ),
  .s_axi_wstrb   ( m27_axi_wstrb   ),
  .s_axi_wlast   ( m27_axi_wlast   ),
  .s_axi_bvalid  ( m27_axi_bvalid  ),
  .s_axi_bready  ( m27_axi_bready  ),
  .s_axi_arvalid ( m27_axi_arvalid ),
  .s_axi_arready ( m27_axi_arready ),
  .s_axi_araddr  ( m27_axi_araddr  ),
  .s_axi_arlen   ( m27_axi_arlen   ),
  .s_axi_rvalid  ( m27_axi_rvalid  ),
  .s_axi_rready  ( m27_axi_rready  ),
  .s_axi_rdata   ( m27_axi_rdata   ),
  .s_axi_rlast   ( m27_axi_rlast   )
);


slv_m27_axi_vip_slv_mem_t   m27_axi;
slv_m27_axi_vip_slv_t   m27_axi_slv;

// Slave MM VIP instantiation
slv_m28_axi_vip inst_slv_m28_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m28_axi_awvalid ),
  .s_axi_awready ( m28_axi_awready ),
  .s_axi_awaddr  ( m28_axi_awaddr  ),
  .s_axi_awlen   ( m28_axi_awlen   ),
  .s_axi_wvalid  ( m28_axi_wvalid  ),
  .s_axi_wready  ( m28_axi_wready  ),
  .s_axi_wdata   ( m28_axi_wdata   ),
  .s_axi_wstrb   ( m28_axi_wstrb   ),
  .s_axi_wlast   ( m28_axi_wlast   ),
  .s_axi_bvalid  ( m28_axi_bvalid  ),
  .s_axi_bready  ( m28_axi_bready  ),
  .s_axi_arvalid ( m28_axi_arvalid ),
  .s_axi_arready ( m28_axi_arready ),
  .s_axi_araddr  ( m28_axi_araddr  ),
  .s_axi_arlen   ( m28_axi_arlen   ),
  .s_axi_rvalid  ( m28_axi_rvalid  ),
  .s_axi_rready  ( m28_axi_rready  ),
  .s_axi_rdata   ( m28_axi_rdata   ),
  .s_axi_rlast   ( m28_axi_rlast   )
);


slv_m28_axi_vip_slv_mem_t   m28_axi;
slv_m28_axi_vip_slv_t   m28_axi_slv;

// Slave MM VIP instantiation
slv_m29_axi_vip inst_slv_m29_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m29_axi_awvalid ),
  .s_axi_awready ( m29_axi_awready ),
  .s_axi_awaddr  ( m29_axi_awaddr  ),
  .s_axi_awlen   ( m29_axi_awlen   ),
  .s_axi_wvalid  ( m29_axi_wvalid  ),
  .s_axi_wready  ( m29_axi_wready  ),
  .s_axi_wdata   ( m29_axi_wdata   ),
  .s_axi_wstrb   ( m29_axi_wstrb   ),
  .s_axi_wlast   ( m29_axi_wlast   ),
  .s_axi_bvalid  ( m29_axi_bvalid  ),
  .s_axi_bready  ( m29_axi_bready  ),
  .s_axi_arvalid ( m29_axi_arvalid ),
  .s_axi_arready ( m29_axi_arready ),
  .s_axi_araddr  ( m29_axi_araddr  ),
  .s_axi_arlen   ( m29_axi_arlen   ),
  .s_axi_rvalid  ( m29_axi_rvalid  ),
  .s_axi_rready  ( m29_axi_rready  ),
  .s_axi_rdata   ( m29_axi_rdata   ),
  .s_axi_rlast   ( m29_axi_rlast   )
);


slv_m29_axi_vip_slv_mem_t   m29_axi;
slv_m29_axi_vip_slv_t   m29_axi_slv;

// Slave MM VIP instantiation
slv_m30_axi_vip inst_slv_m30_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m30_axi_awvalid ),
  .s_axi_awready ( m30_axi_awready ),
  .s_axi_awaddr  ( m30_axi_awaddr  ),
  .s_axi_awlen   ( m30_axi_awlen   ),
  .s_axi_wvalid  ( m30_axi_wvalid  ),
  .s_axi_wready  ( m30_axi_wready  ),
  .s_axi_wdata   ( m30_axi_wdata   ),
  .s_axi_wstrb   ( m30_axi_wstrb   ),
  .s_axi_wlast   ( m30_axi_wlast   ),
  .s_axi_bvalid  ( m30_axi_bvalid  ),
  .s_axi_bready  ( m30_axi_bready  ),
  .s_axi_arvalid ( m30_axi_arvalid ),
  .s_axi_arready ( m30_axi_arready ),
  .s_axi_araddr  ( m30_axi_araddr  ),
  .s_axi_arlen   ( m30_axi_arlen   ),
  .s_axi_rvalid  ( m30_axi_rvalid  ),
  .s_axi_rready  ( m30_axi_rready  ),
  .s_axi_rdata   ( m30_axi_rdata   ),
  .s_axi_rlast   ( m30_axi_rlast   )
);


slv_m30_axi_vip_slv_mem_t   m30_axi;
slv_m30_axi_vip_slv_t   m30_axi_slv;

// Slave MM VIP instantiation
slv_m31_axi_vip inst_slv_m31_axi_vip (
  .aclk          ( ap_clk          ),
  .s_axi_awvalid ( m31_axi_awvalid ),
  .s_axi_awready ( m31_axi_awready ),
  .s_axi_awaddr  ( m31_axi_awaddr  ),
  .s_axi_awlen   ( m31_axi_awlen   ),
  .s_axi_wvalid  ( m31_axi_wvalid  ),
  .s_axi_wready  ( m31_axi_wready  ),
  .s_axi_wdata   ( m31_axi_wdata   ),
  .s_axi_wstrb   ( m31_axi_wstrb   ),
  .s_axi_wlast   ( m31_axi_wlast   ),
  .s_axi_bvalid  ( m31_axi_bvalid  ),
  .s_axi_bready  ( m31_axi_bready  ),
  .s_axi_arvalid ( m31_axi_arvalid ),
  .s_axi_arready ( m31_axi_arready ),
  .s_axi_araddr  ( m31_axi_araddr  ),
  .s_axi_arlen   ( m31_axi_arlen   ),
  .s_axi_rvalid  ( m31_axi_rvalid  ),
  .s_axi_rready  ( m31_axi_rready  ),
  .s_axi_rdata   ( m31_axi_rdata   ),
  .s_axi_rlast   ( m31_axi_rlast   )
);


slv_m31_axi_vip_slv_mem_t   m31_axi;
slv_m31_axi_vip_slv_t   m31_axi_slv;

`endif

parameter NUM_AXIS_MST = 0;
parameter NUM_AXIS_SLV = 0;

bit               error_found = 0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m00_axi
bit [63:0] A_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m01_axi
bit [63:0] B_ptr = 64'h10000000;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m02_axi
bit [63:0] C_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m03_axi
bit [63:0] D_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m04_axi
bit [63:0] E_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m05_axi
bit [63:0] F_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m06_axi
bit [63:0] G_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m07_axi
bit [63:0] H_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m08_axi
bit [63:0] I_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m09_axi
bit [63:0] J_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m10_axi
bit [63:0] K_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m11_axi
bit [63:0] L_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m12_axi
bit [63:0] M_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m13_axi
bit [63:0] N_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m14_axi
bit [63:0] O_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m15_axi
bit [63:0] P_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m00_axi
bit [63:0] Q_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m01_axi
bit [63:0] R_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m02_axi
bit [63:0] S_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m03_axi
bit [63:0] T_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m04_axi
bit [63:0] U_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m05_axi
bit [63:0] V_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m06_axi
bit [63:0] W_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m07_axi
bit [63:0] X_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m08_axi
bit [63:0] Y_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m09_axi
bit [63:0] Z_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m10_axi
bit [63:0] AA_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m11_axi
bit [63:0] AB_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m12_axi
bit [63:0] AC_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m13_axi
bit [63:0] AD_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m14_axi
bit [63:0] AE_ptr = 64'h0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m15_axi
bit [63:0] AF_ptr = 64'h0;

integer unsigned c[64][64];

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m00_axi memory.
function void m00_axi_fill_memory(
  input bit [63:0] ptr,
  input integer        length
);


     parameter size = 64;
     parameter block_size = 8;

     
     integer i,j,k;

     for( i =0;i<size;i++) begin
      for( j =0;j<size;j++)
      begin
        a[i][j] = $urandom_range(512,0);
        b[i][j] = $urandom_range(512,0);
        c[i][j] = 0;
//        $write("%d ",a[i][j]);
      end
//      $display("");
    end
    
         for( i =0;i<size;i++) begin
      for( j =0;j<size;j++)begin
        for(k=0;k<size;k++) begin
            c[i][j] =  c[i][j]+(a[i][k]*b[k][j]);
//        $write("%d ",a[i][j]);
        end
      end
//      $display("")
    end
    count =0;
     for(int k=0;k<size;k=k+block_size)begin
      for(int i =0;i<size;i++)begin
       for(int j=0; j<block_size;j++)begin
//        $write("%0d ",a[k+j][i]);
        m00_axi.mem_model.backdoor_memory_write_4byte(baseA + (count * 4), a[k+j][i]); 
        m00_axi.mem_model.backdoor_memory_write_4byte(baseB + (count * 4), b[i][k+j]); 
        count++;
//        end

        end
      end
    end
endfunction
`ifdef XBARorM1orM0

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m01_axi memory.
function void m01_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
for(int k=0;k<size;k=k+block_size)begin
  for(int i =0;i<size;i++)begin
   for(int j=0; j<block_size;j++)begin
//        $write("%0d ",a[k+j][i]);
    m01_axi.mem_model.backdoor_memory_write_4byte(ptr+baseA + (count * 4), a[k+j][i]); 
    m01_axi.mem_model.backdoor_memory_write_4byte(ptr+baseB + (count * 4), b[i][k+j]); 
    count++;
//        end

    end
  end
end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m02_axi memory.
function void m02_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m02_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m03_axi memory.
function void m03_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m03_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction
`endif
`ifdef XBARorM1

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m04_axi memory.
function void m04_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m04_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m05_axi memory.
function void m05_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m05_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m06_axi memory.
function void m06_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m06_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m07_axi memory.
function void m07_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m07_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction
`endif
`ifdef XBARorM0 

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m08_axi memory.
function void m08_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m08_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m09_axi memory.
function void m09_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m09_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m10_axi memory.
function void m10_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m10_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m11_axi memory.
function void m11_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m11_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction
`endif
`ifdef XBAR 

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m12_axi memory.
function void m12_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m12_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m13_axi memory.
function void m13_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m13_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m14_axi memory.
function void m14_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m14_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m15_axi memory.
function void m15_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m15_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction


/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m16_axi memory.
function void m16_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m16_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m17_axi memory.
function void m17_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m17_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m18_axi memory.
function void m18_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m18_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m19_axi memory.
function void m19_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m19_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m20_axi memory.
function void m20_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m20_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m21_axi memory.
function void m21_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m21_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m22_axi memory.
function void m22_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m22_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m23_axi memory.
function void m23_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m23_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m24_axi memory.
function void m24_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m24_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m25_axi memory.
function void m25_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m25_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m26_axi memory.
function void m26_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m26_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m27_axi memory.
function void m27_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m27_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m28_axi memory.
function void m28_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m28_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m29_axi memory.
function void m29_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m29_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m30_axi memory.
function void m30_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m30_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m31_axi memory.
function void m31_axi_fill_memory(
  input bit [63:0] ptr,
  input integer    length
);
  for (longint unsigned slot = 0; slot < length; slot++) begin
    m31_axi.mem_model.backdoor_memory_write_4byte(ptr + (slot * 4), slot);
  end
endfunction
`endif

/////////////////////////////////////////////////////////////////////////////////////////////////
// Generate a random 32bit number
function bit [31:0] get_random_4bytes();
  bit [31:0] rptr;
  ptr_random_failed: assert(std::randomize(rptr));
  return(rptr);
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Generate a random 64bit 4k aligned address pointer.
function bit [63:0] get_random_ptr();
  bit [63:0] rptr;
  ptr_random_failed: assert(std::randomize(rptr));
  rptr[31:0] &= ~(32'h00000fff);
  return(rptr);
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface non-blocking write
// The task will return when the transaction has been accepted by the driver. It will be some
// amount of time before it will appear on the interface.
task automatic write_register (input bit [31:0] addr_in, input bit [31:0] data);
  axi_transaction   wr_xfer;
  wr_xfer = ctrl.wr_driver.create_transaction("wr_xfer");
  assert(wr_xfer.randomize() with {addr == addr_in;});
  wr_xfer.set_data_beat(0, data);
  ctrl.wr_driver.send(wr_xfer);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface blocking write
// The task will return when the BRESP has been returned from the kernel.
task automatic blocking_write_register (input bit [31:0] addr_in, input bit [31:0] data);
  axi_transaction   wr_xfer;
  axi_transaction   wr_rsp;
  wr_xfer = ctrl.wr_driver.create_transaction("wr_xfer");
  wr_xfer.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
  assert(wr_xfer.randomize() with {addr == addr_in;});
  wr_xfer.set_data_beat(0, data);
  ctrl.wr_driver.send(wr_xfer);
  ctrl.wr_driver.wait_rsp(wr_rsp);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface blocking read
// The task will return when the BRESP has been returned from the kernel.
task automatic read_register (input bit [31:0] addr, output bit [31:0] rddata);
  axi_transaction   rd_xfer;
  axi_transaction   rd_rsp;
  bit [31:0] rd_value;
  rd_xfer = ctrl.rd_driver.create_transaction("rd_xfer");
  rd_xfer.set_addr(addr);
  rd_xfer.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
  ctrl.rd_driver.send(rd_xfer);
  ctrl.rd_driver.wait_rsp(rd_rsp);
  rd_value = rd_rsp.get_data_beat(0);
  rddata = rd_value;
endtask



/////////////////////////////////////////////////////////////////////////////////////////////////
// Poll the Control interface status register.
// This will poll until the DONE flag in the status register is asserted.
task automatic poll_done_register ();
  bit [31:0] rd_value;
  do begin
    read_register(KRNL_CTRL_REG_ADDR, rd_value);
  end while ((rd_value & CTRL_DONE_MASK) == 0);
endtask

// This will poll until the IDLE flag in the status register is asserted.
task automatic poll_idle_register ();
  bit [31:0] rd_value;
  do begin
    read_register(KRNL_CTRL_REG_ADDR, rd_value);
  end while ((rd_value & CTRL_IDLE_MASK) == 0);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Write to the control registers to enable the triggering of interrupts for the kernel
task automatic enable_interrupts();
  $display("Starting: Enabling Interrupts....");
  write_register(KRNL_GIE_REG_ADDR, GIE_GIE_MASK);
  write_register(KRNL_IER_REG_ADDR, IER_DONE_MASK);
  $display("Finished: Interrupts enabled.");
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Disabled the interrupts.
task automatic disable_interrupts();
  $display("Starting: Disable Interrupts....");
  write_register(KRNL_GIE_REG_ADDR, 32'h0);
  write_register(KRNL_IER_REG_ADDR, 32'h0);
  $display("Finished: Interrupts disabled.");
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
//When the interrupt is asserted, read the correct registers and clear the asserted interrupt.
task automatic service_interrupts();
  bit [31:0] rd_value;
  $display("Starting Servicing interrupts....");
  read_register(KRNL_CTRL_REG_ADDR, rd_value);
  $display("Control Register: 0x%0x", rd_value);

  blocking_write_register(KRNL_CTRL_REG_ADDR, rd_value);

  if ((rd_value & CTRL_DONE_MASK) == 0) begin
    $error("%t : DONE bit not asserted. Register value: (0x%0x)", $time, rd_value);
  end
  read_register(KRNL_ISR_REG_ADDR, rd_value);
  $display("Interrupt Status Register: 0x%0x", rd_value);
  blocking_write_register(KRNL_ISR_REG_ADDR, rd_value);
  $display("Finished Servicing interrupts");
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Start the control VIP, SLAVE memory models and AXI4-Stream.
task automatic start_vips();
  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Control Master: ctrl");
  ctrl = new("ctrl", bft_32_xbar_v2_tb.inst_control_bft_32_xbar_v2_vip.inst.IF);
  ctrl.start_master();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m00_axi");
  m00_axi = new("m00_axi", bft_32_xbar_v2_tb.inst_slv_m00_axi_vip.inst.IF);
  m00_axi.start_slave();
`ifdef XBARorM1orM0
  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m01_axi");
  m01_axi = new("m01_axi", bft_32_xbar_v2_tb.inst_slv_m01_axi_vip.inst.IF);
  m01_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m02_axi");
  m02_axi = new("m02_axi", bft_32_xbar_v2_tb.inst_slv_m02_axi_vip.inst.IF);
  m02_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m03_axi");
  m03_axi = new("m03_axi", bft_32_xbar_v2_tb.inst_slv_m03_axi_vip.inst.IF);
  m03_axi.start_slave();
`endif
`ifdef XBARorM1

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m04_axi");
  m04_axi = new("m04_axi", bft_32_xbar_v2_tb.inst_slv_m04_axi_vip.inst.IF);
  m04_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m05_axi");
  m05_axi = new("m05_axi", bft_32_xbar_v2_tb.inst_slv_m05_axi_vip.inst.IF);
  m05_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m06_axi");
  m06_axi = new("m06_axi", bft_32_xbar_v2_tb.inst_slv_m06_axi_vip.inst.IF);
  m06_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m07_axi");
  m07_axi = new("m07_axi", bft_32_xbar_v2_tb.inst_slv_m07_axi_vip.inst.IF);
  m07_axi.start_slave();
`endif
`ifdef XBARorM0 

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m08_axi");
  m08_axi = new("m08_axi", bft_32_xbar_v2_tb.inst_slv_m08_axi_vip.inst.IF);
  m08_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m09_axi");
  m09_axi = new("m09_axi", bft_32_xbar_v2_tb.inst_slv_m09_axi_vip.inst.IF);
  m09_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m10_axi");
  m10_axi = new("m10_axi", bft_32_xbar_v2_tb.inst_slv_m10_axi_vip.inst.IF);
  m10_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m11_axi");
  m11_axi = new("m11_axi", bft_32_xbar_v2_tb.inst_slv_m11_axi_vip.inst.IF);
  m11_axi.start_slave();
`endif
`ifdef XBAR 

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m12_axi");
  m12_axi = new("m12_axi", bft_32_xbar_v2_tb.inst_slv_m12_axi_vip.inst.IF);
  m12_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m13_axi");
  m13_axi = new("m13_axi", bft_32_xbar_v2_tb.inst_slv_m13_axi_vip.inst.IF);
  m13_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m14_axi");
  m14_axi = new("m14_axi", bft_32_xbar_v2_tb.inst_slv_m14_axi_vip.inst.IF);
  m14_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m15_axi");
  m15_axi = new("m15_axi", bft_32_xbar_v2_tb.inst_slv_m15_axi_vip.inst.IF);
  m15_axi.start_slave();

  
  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m16_axi");
  m16_axi = new("m16_axi", bft_32_xbar_v2_tb.inst_slv_m16_axi_vip.inst.IF);
  m16_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m17_axi");
  m17_axi = new("m17_axi", bft_32_xbar_v2_tb.inst_slv_m17_axi_vip.inst.IF);
  m17_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m18_axi");
  m18_axi = new("m18_axi", bft_32_xbar_v2_tb.inst_slv_m18_axi_vip.inst.IF);
  m18_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m19_axi");
  m19_axi = new("m19_axi", bft_32_xbar_v2_tb.inst_slv_m19_axi_vip.inst.IF);
  m19_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m20_axi");
  m20_axi = new("m20_axi", bft_32_xbar_v2_tb.inst_slv_m20_axi_vip.inst.IF);
  m20_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m21_axi");
  m21_axi = new("m21_axi", bft_32_xbar_v2_tb.inst_slv_m21_axi_vip.inst.IF);
  m21_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m22_axi");
  m22_axi = new("m22_axi", bft_32_xbar_v2_tb.inst_slv_m22_axi_vip.inst.IF);
  m22_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m23_axi");
  m23_axi = new("m23_axi", bft_32_xbar_v2_tb.inst_slv_m23_axi_vip.inst.IF);
  m23_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m24_axi");
  m24_axi = new("m24_axi", bft_32_xbar_v2_tb.inst_slv_m24_axi_vip.inst.IF);
  m24_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m25_axi");
  m25_axi = new("m25_axi", bft_32_xbar_v2_tb.inst_slv_m25_axi_vip.inst.IF);
  m25_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m26_axi");
  m26_axi = new("m26_axi", bft_32_xbar_v2_tb.inst_slv_m26_axi_vip.inst.IF);
  m26_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m27_axi");
  m27_axi = new("m27_axi", bft_32_xbar_v2_tb.inst_slv_m27_axi_vip.inst.IF);
  m27_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m28_axi");
  m28_axi = new("m28_axi", bft_32_xbar_v2_tb.inst_slv_m28_axi_vip.inst.IF);
  m28_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m29_axi");
  m29_axi = new("m29_axi", bft_32_xbar_v2_tb.inst_slv_m29_axi_vip.inst.IF);
  m29_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m30_axi");
  m30_axi = new("m30_axi", bft_32_xbar_v2_tb.inst_slv_m30_axi_vip.inst.IF);
  m30_axi.start_slave();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m31_axi");
  m31_axi = new("m31_axi", bft_32_xbar_v2_tb.inst_slv_m31_axi_vip.inst.IF);
  m31_axi.start_slave();
`endif
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, set the Slave to not de-assert WREADY at any time.
// This will show the fastest outbound bandwidth from the WRITE channel.
task automatic slv_no_backpressure_wready();
  axi_ready_gen     rgen;
  $display("%t - Applying slv_no_backpressure_wready", $time);

  rgen = new("m00_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m00_axi.wr_driver.set_wready_gen(rgen);

`ifdef XBARorM1orM0

  rgen = new("m01_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m01_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m02_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m02_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m03_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m03_axi.wr_driver.set_wready_gen(rgen);


`endif
`ifdef XBARorM1

  rgen = new("m04_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m04_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m05_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m05_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m06_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m06_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m07_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m07_axi.wr_driver.set_wready_gen(rgen);
`endif
`ifdef XBARorM0 

  rgen = new("m08_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m08_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m09_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m09_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m10_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m10_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m11_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m11_axi.wr_driver.set_wready_gen(rgen);

`endif
`ifdef XBAR 

  rgen = new("m12_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m12_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m13_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m13_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m14_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m14_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m15_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m15_axi.wr_driver.set_wready_gen(rgen);
  
    rgen = new("m16_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m16_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m17_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m17_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m18_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m18_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m19_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m19_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m20_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m20_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m21_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m21_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m22_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m22_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m23_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m23_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m24_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m24_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m25_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m25_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m26_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m26_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m27_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m27_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m28_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m28_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m29_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m29_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m30_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m30_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m31_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m31_axi.wr_driver.set_wready_gen(rgen);
`endif
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, apply a WREADY policy to introduce backpressure.
// Based on the simulation seed the order/shape of the WREADY per-channel will be different.
task automatic slv_random_backpressure_wready();
  axi_ready_gen     rgen;
  $display("%t - Applying slv_random_backpressure_wready", $time);

  rgen = new("m00_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m00_axi.wr_driver.set_wready_gen(rgen);
`ifdef XBARorM1orM0

  rgen = new("m01_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m01_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m02_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m02_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m03_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m03_axi.wr_driver.set_wready_gen(rgen);
`endif
`ifdef XBARorM1
  rgen = new("m04_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m04_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m05_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m05_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m06_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m06_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m07_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m07_axi.wr_driver.set_wready_gen(rgen);
`endif
`ifdef XBARorM0 

  rgen = new("m08_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m08_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m09_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m09_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m10_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m10_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m11_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m11_axi.wr_driver.set_wready_gen(rgen);
`endif
`ifdef XBAR 

  rgen = new("m12_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m12_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m13_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m13_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m14_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m14_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m15_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m15_axi.wr_driver.set_wready_gen(rgen);
  
  rgen = new("m16_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m16_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m17_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m17_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m18_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m18_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m19_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m19_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m20_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m20_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m21_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m21_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m22_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m22_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m23_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m23_axi.wr_driver.set_wready_gen(rgen);
  
  rgen = new("m24_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m24_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m25_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m25_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m26_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m26_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m27_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m27_axi.wr_driver.set_wready_gen(rgen);
  
  rgen = new("m28_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m28_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m29_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m29_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m30_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m30_axi.wr_driver.set_wready_gen(rgen);

  rgen = new("m31_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,16);
  rgen.set_high_time_range(1,16);
  rgen.set_event_count_range(3,5);
  m31_axi.wr_driver.set_wready_gen(rgen);


`endif
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, force the memory model to not insert any inter-beat
// gaps on the READ channel.
task automatic slv_no_delay_rvalid();
  $display("%t - Applying slv_no_delay_rvalid", $time);

  m00_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m00_axi.mem_model.set_inter_beat_gap(0);

`ifdef XBARorM1orM0

  m01_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m01_axi.mem_model.set_inter_beat_gap(0);

  m02_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m02_axi.mem_model.set_inter_beat_gap(0);

  m03_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m03_axi.mem_model.set_inter_beat_gap(0);


`endif
`ifdef XBARorM1

  m04_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m04_axi.mem_model.set_inter_beat_gap(0);

  m05_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m05_axi.mem_model.set_inter_beat_gap(0);

  m06_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m06_axi.mem_model.set_inter_beat_gap(0);

  m07_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m07_axi.mem_model.set_inter_beat_gap(0);
`endif
`ifdef XBARorM0 
  m08_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m08_axi.mem_model.set_inter_beat_gap(0);

  m09_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m09_axi.mem_model.set_inter_beat_gap(0);

  m10_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m10_axi.mem_model.set_inter_beat_gap(0);

  m11_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m11_axi.mem_model.set_inter_beat_gap(0);
`endif
`ifdef XBAR 
  m12_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m12_axi.mem_model.set_inter_beat_gap(0);

  m13_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m13_axi.mem_model.set_inter_beat_gap(0);

  m14_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m14_axi.mem_model.set_inter_beat_gap(0);

  m15_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m15_axi.mem_model.set_inter_beat_gap(0);
  
  
  m16_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m16_axi.mem_model.set_inter_beat_gap(0);

  m17_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m17_axi.mem_model.set_inter_beat_gap(0);

  m18_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m18_axi.mem_model.set_inter_beat_gap(0);

  m19_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m19_axi.mem_model.set_inter_beat_gap(0);

  m20_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m20_axi.mem_model.set_inter_beat_gap(0);

  m21_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m21_axi.mem_model.set_inter_beat_gap(0);

  m22_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m22_axi.mem_model.set_inter_beat_gap(0);

  m23_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m23_axi.mem_model.set_inter_beat_gap(0);

  m24_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m24_axi.mem_model.set_inter_beat_gap(0);

  m25_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m25_axi.mem_model.set_inter_beat_gap(0);

  m26_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m26_axi.mem_model.set_inter_beat_gap(0);

  m27_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m27_axi.mem_model.set_inter_beat_gap(0);

  m28_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m28_axi.mem_model.set_inter_beat_gap(0);

  m29_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m29_axi.mem_model.set_inter_beat_gap(0);

  m30_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m30_axi.mem_model.set_inter_beat_gap(0);

  m31_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m31_axi.mem_model.set_inter_beat_gap(0);

`endif

endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, Allow the memory model to insert any inter-beat
// gaps on the READ channel.
task automatic slv_random_delay_rvalid();
  $display("%t - Applying slv_random_delay_rvalid", $time);

  m00_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m00_axi.mem_model.set_inter_beat_gap_range(0,10);
`ifdef XBARorM1orM0

  m01_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m01_axi.mem_model.set_inter_beat_gap_range(0,10);

  m02_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m02_axi.mem_model.set_inter_beat_gap_range(0,10);

  m03_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m03_axi.mem_model.set_inter_beat_gap_range(0,10);
`endif
`ifdef XBARorM1

  m04_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m04_axi.mem_model.set_inter_beat_gap_range(0,10);

  m05_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m05_axi.mem_model.set_inter_beat_gap_range(0,10);

  m06_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m06_axi.mem_model.set_inter_beat_gap_range(0,10);

  m07_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m07_axi.mem_model.set_inter_beat_gap_range(0,10);

`endif
`ifdef XBARorM0 
  m08_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m08_axi.mem_model.set_inter_beat_gap_range(0,10);

  m09_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m09_axi.mem_model.set_inter_beat_gap_range(0,10);

  m10_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m10_axi.mem_model.set_inter_beat_gap_range(0,10);

  m11_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m11_axi.mem_model.set_inter_beat_gap_range(0,10);

`endif
`ifdef XBAR 
  m12_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m12_axi.mem_model.set_inter_beat_gap_range(0,10);

  m13_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m13_axi.mem_model.set_inter_beat_gap_range(0,10);

  m14_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m14_axi.mem_model.set_inter_beat_gap_range(0,10);

  m15_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m15_axi.mem_model.set_inter_beat_gap_range(0,10);

  m16_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m16_axi.mem_model.set_inter_beat_gap_range(0,26);

  m17_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m17_axi.mem_model.set_inter_beat_gap_range(0,26);

  m18_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m18_axi.mem_model.set_inter_beat_gap_range(0,26);

  m19_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m19_axi.mem_model.set_inter_beat_gap_range(0,26);

  m20_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m20_axi.mem_model.set_inter_beat_gap_range(0,26);

  m21_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m21_axi.mem_model.set_inter_beat_gap_range(0,26);

  m22_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m22_axi.mem_model.set_inter_beat_gap_range(0,26);

  m23_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m23_axi.mem_model.set_inter_beat_gap_range(0,26);

  m24_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m24_axi.mem_model.set_inter_beat_gap_range(0,26);

  m25_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m25_axi.mem_model.set_inter_beat_gap_range(0,26);

  m26_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m26_axi.mem_model.set_inter_beat_gap_range(0,26);

  m27_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m27_axi.mem_model.set_inter_beat_gap_range(0,26);

  m28_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m28_axi.mem_model.set_inter_beat_gap_range(0,26);

  m29_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m29_axi.mem_model.set_inter_beat_gap_range(0,26);

  m30_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m30_axi.mem_model.set_inter_beat_gap_range(0,26);

  m31_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m31_axi.mem_model.set_inter_beat_gap_range(0,26);
`endif
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Check to ensure, following reset the value of the register is 0.
// Check that only the width of the register bits can be written.
task automatic check_register_value(input bit [31:0] addr_in, input integer unsigned register_width, output bit error_found);
  bit [31:0] rddata;
  bit [31:0] mask_data;
  error_found = 0;
  if (register_width < 32) begin
    mask_data = (1 << register_width) - 1;
  end else begin
    mask_data = 32'hffffffff;
  end
  read_register(addr_in, rddata);
  if (rddata != 32'h0) begin
    $error("Initial value mismatch: A:0x%0x : Expected 0x%x -> Got 0x%x", addr_in, 0, rddata);
    error_found = 1;
  end
  blocking_write_register(addr_in, 32'hffffffff);
  read_register(addr_in, rddata);
  if (rddata != mask_data) begin
    $error("Initial value mismatch: A:0x%0x : Expected 0x%x -> Got 0x%x", addr_in, mask_data, rddata);
    error_found = 1;
  end
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the scalar registers, check:
// * reset value
// * correct number bits set on a write
task automatic check_scalar_registers(output bit error_found);
  bit tmp_error_found = 0;
  error_found = 0;
  $display("%t : Checking post reset values of scalar registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Check ID 0: scalar00 (0x010)
  check_register_value(32'h010, 32, tmp_error_found);
  error_found |= tmp_error_found;

endtask

task automatic set_scalar_registers();
  $display("%t : Setting Scalar Registers registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 0: scalar00 (0x010) -> 32'hffffffff (scalar)
  write_register(32'h010, 32'h0xfff020);

endtask

task automatic check_pointer_registers(output bit error_found);
  bit tmp_error_found = 0;
  ///////////////////////////////////////////////////////////////////////////
  //Check the reset states of the pointer registers.
  $display("%t : Checking post reset values of pointer registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x018)
  check_register_value(32'h018, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x01c)
  check_register_value(32'h01c, 32, tmp_error_found);
  error_found |= tmp_error_found;

`ifdef XBARorM1orM0

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x024)
  check_register_value(32'h024, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x028)
  check_register_value(32'h028, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x030)
  check_register_value(32'h030, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x034)
  check_register_value(32'h034, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x03c)
  check_register_value(32'h03c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x040)
  check_register_value(32'h040, 32, tmp_error_found);
  error_found |= tmp_error_found;

`endif
`ifdef XBARorM1
  ///////////////////////////////////////////////////////////////////////////
  //Write ID 5: E (0x048)
  check_register_value(32'h048, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 5: E (0x04c)
  check_register_value(32'h04c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x054)
  check_register_value(32'h054, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x058)
  check_register_value(32'h058, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x060)
  check_register_value(32'h060, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x064)
  check_register_value(32'h064, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x06c)
  check_register_value(32'h06c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x070)
  check_register_value(32'h070, 32, tmp_error_found);
  error_found |= tmp_error_found;

`endif
`ifdef XBARorM0 


  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x078)
  check_register_value(32'h078, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x07c)
  check_register_value(32'h07c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x084)
  check_register_value(32'h084, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x088)
  check_register_value(32'h088, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x090)
  check_register_value(32'h090, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x094)
  check_register_value(32'h094, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x09c)
  check_register_value(32'h09c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x0a0)
  check_register_value(32'h0a0, 32, tmp_error_found);
  error_found |= tmp_error_found;

`endif
`ifdef XBAR 
  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0a8)
  check_register_value(32'h0a8, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0ac)
  check_register_value(32'h0ac, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b4)
  check_register_value(32'h0b4, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b8)
  check_register_value(32'h0b8, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c0)
  check_register_value(32'h0c0, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c4)
  check_register_value(32'h0c4, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0cc)
  check_register_value(32'h0cc, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0d0)
  check_register_value(32'h0d0, 32, tmp_error_found);
  error_found |= tmp_error_found;

   ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x018)
  check_register_value(32'h0d8, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x01c)
  check_register_value(32'h0dc, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x024)
  check_register_value(32'h0e4, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x028)
  check_register_value(32'h0e8, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x030)
  check_register_value(32'h0f0, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x034)
  check_register_value(32'h0f4, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x03c)
  check_register_value(32'h0fc, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x040)
  check_register_value(32'h0100, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 5: E (0x048)
  check_register_value(32'h0108, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 5: E (0x04c)
  check_register_value(32'h010c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x054)
  check_register_value(32'h0114, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x058)
  check_register_value(32'h0118, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x060)
  check_register_value(32'h0120, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x064)
  check_register_value(32'h0124, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x06c)
  check_register_value(32'h012c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x070)
  check_register_value(32'h0130, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x078)
  check_register_value(32'h0138, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x07c)
  check_register_value(32'h013c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x084)
  check_register_value(32'h0144, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x088)
  check_register_value(32'h0148, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x090)
  check_register_value(32'h0150, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x094)
  check_register_value(32'h0154, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x09c)
  check_register_value(32'h015c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x0a0)
  check_register_value(32'h0160, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0a8)
  check_register_value(32'h0168, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0ac)
  check_register_value(32'h016c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b4)
  check_register_value(32'h0174, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b8)
  check_register_value(32'h0178, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c0)
  check_register_value(32'h0180, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c4)
  check_register_value(32'h0184, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0cc)
  check_register_value(32'h018c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0d0)
  check_register_value(32'h0190, 32, tmp_error_found);
  error_found |= tmp_error_found;   
`endif
endtask

task automatic set_memory_pointers();
  ///////////////////////////////////////////////////////////////////////////
  //Randomly generate memory pointers.
  A_ptr = 64'h00000000;
  `ifdef XBARorM1orM0

  B_ptr = 64'h10000000;
  C_ptr = get_random_ptr();
  D_ptr = get_random_ptr();
`endif
`ifdef XBARorM1

  E_ptr = get_random_ptr();
  F_ptr = get_random_ptr();
  G_ptr = get_random_ptr();
  H_ptr = get_random_ptr();
  
`endif
`ifdef XBARorM0 

  I_ptr = get_random_ptr();
  J_ptr = get_random_ptr();
  K_ptr = get_random_ptr();
  L_ptr = get_random_ptr();
  
`endif
`ifdef XBAR 
  M_ptr = get_random_ptr();
  N_ptr = get_random_ptr();
  O_ptr = get_random_ptr();
  P_ptr = get_random_ptr();

  Q_ptr = get_random_ptr();
  R_ptr = get_random_ptr();
  S_ptr = get_random_ptr();
  T_ptr = get_random_ptr();
  U_ptr = get_random_ptr();
  V_ptr = get_random_ptr();
  W_ptr = get_random_ptr();
  X_ptr = get_random_ptr();
  Y_ptr = get_random_ptr();
  Z_ptr = get_random_ptr();
  AA_ptr = get_random_ptr();
  AB_ptr = get_random_ptr();
  AC_ptr = get_random_ptr();
  AD_ptr = get_random_ptr();
  AE_ptr = get_random_ptr();
  AF_ptr = get_random_ptr();
  
  `endif
  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x018) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h018, A_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x01c) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h01c, A_ptr[63:32]);
  
`ifdef XBARorM1orM0

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x024) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h024, B_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x028) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h028, B_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x030) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h030, C_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x034) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h034, C_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x03c) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h03c, D_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x040) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h040, D_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  
  `endif
`ifdef XBARorM1

  //Write ID 5: E (0x048) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h048, E_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 5: E (0x04c) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h04c, E_ptr[63:32]);


  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x054) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h054, F_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x058) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h058, F_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x060) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h060, G_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x064) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h064, G_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x06c) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h06c, H_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x070) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h070, H_ptr[63:32]);

`endif
`ifdef XBARorM0 
  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x078) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h078, I_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x07c) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h07c, I_ptr[63:32]);



  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x084) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h084, J_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x088) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h088, J_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x090) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h090, K_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x094) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h094, K_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x09c) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h09c, L_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x0a0) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0a0, L_ptr[63:32]);


`endif
`ifdef XBAR 

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0a8) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0a8, M_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0ac) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0ac, M_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b4) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0b4, N_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b8) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0b8, N_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c0) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0c0, O_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c4) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0c4, O_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0cc) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0cc, P_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0d0) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0d0, P_ptr[63:32]);
    
     ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x018) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0d8, Q_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: A (0x01c) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0dc, Q_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x024) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0e4, R_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: B (0x028) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0e8, R_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x030) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0f0, S_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 3: C (0x034) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0f4, S_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x03c) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0fc, T_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 4: D (0x040) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0100, T_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 5: E (0x048) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0108, U_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 5: E (0x04c) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h010c, U_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x054) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0114, V_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 6: F (0x058) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0118, V_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x060) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0120, W_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 7: G (0x064) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0124, W_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x06c) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h012c, X_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 8: H (0x070) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0130, X_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x078) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0138, Y_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 9: I (0x07c) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h013c, Y_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x084) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0144, Z_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 10: J (0x088) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0148, Z_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x090) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0150, AA_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 11: K (0x094) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0154, AA_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x09c) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h015c, AB_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 12: L (0x0a0) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0160, AB_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0a8) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0168, AC_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 13: M (0x0ac) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h016c, AC_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b4) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0174, AD_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 14: N (0x0b8) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0188, AD_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c0) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h0190, AE_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 15: O (0x0c4) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0194, AE_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0cc) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h019c, AF_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 16: P (0x0d0) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h0200, AF_ptr[63:32]);
 `endif
endtask

task automatic backdoor_fill_memories();

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m00_axi_fill_memory(A_ptr, LP_MAX_LENGTH);

`ifdef XBARorM1orM0

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m01_axi_fill_memory(B_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m02_axi_fill_memory(C_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m03_axi_fill_memory(D_ptr, LP_MAX_LENGTH);


`endif
`ifdef XBARorM1

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m04_axi_fill_memory(E_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m05_axi_fill_memory(F_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m06_axi_fill_memory(G_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m07_axi_fill_memory(H_ptr, LP_MAX_LENGTH);

`endif
`ifdef XBARorM0 

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m08_axi_fill_memory(I_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m09_axi_fill_memory(J_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m10_axi_fill_memory(K_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m11_axi_fill_memory(L_ptr, LP_MAX_LENGTH);


`endif
`ifdef XBAR 
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m12_axi_fill_memory(M_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m13_axi_fill_memory(N_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m14_axi_fill_memory(O_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m15_axi_fill_memory(P_ptr, LP_MAX_LENGTH);

    /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m16_axi_fill_memory(Q_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m17_axi_fill_memory(R_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m18_axi_fill_memory(S_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m19_axi_fill_memory(T_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m20_axi_fill_memory(U_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m21_axi_fill_memory(V_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m22_axi_fill_memory(W_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m23_axi_fill_memory(X_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m24_axi_fill_memory(Y_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m25_axi_fill_memory(Z_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m26_axi_fill_memory(AA_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m27_axi_fill_memory(AB_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m28_axi_fill_memory(AC_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m29_axi_fill_memory(AD_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m30_axi_fill_memory(AE_ptr, LP_MAX_LENGTH);

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m31_axi_fill_memory(AF_ptr, LP_MAX_LENGTH);

`endif
endtask

function automatic bit check_kernel_result();
 
  bit [31:0]        ret_rd_value = 32'h0;
  bit error_found = 0;
  integer baseC = 'h40000;
  integer outC[size][size];

  integer error_counter=0;
  integer count=0;

 
//  = 'h10000;
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m00_axi
  bit fail =0;
for(int row=0;row<size;row=row+block_size)
   for(int col=0;col<size;col=col+block_size)
  for(int i=0;i<block_size;i++)
    for(int j=0;j<block_size;j++)begin
        outC[row+i][col+j] = m00_axi.mem_model.backdoor_memory_read_4byte(baseC + (count * 4));
        count = count+1;
    end
    
  for(int i=0;i<size;i++)
    for(int j=0;j<size;j++)begin
        if(c[i][j] != outC[i][j])
            fail = 1;
    end
    if(fail)
        $display("MATRIX INCORRECT");
    else
        $display("MATRIX CORRECT");
//  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
//    ret_rd_value = m00_axi.mem_model.backdoor_memory_read_4byte(A_ptr + (slot * 4));
//    if (slot < LP_MAX_TRANSFER_LENGTH) begin
//      if (ret_rd_value != (slot + 1)) begin
//        $error("Memory Mismatch: m00_axi : @0x%x : Expected 0x%x -> Got 0x%x ", A_ptr + (slot * 4), slot + 1, ret_rd_value);
//        error_found |= 1;
//        error_counter++;
//      end
//    end else begin
//      if (ret_rd_value != slot) begin
//        $error("Memory Mismatch: m00_axi : @0x%x : Expected 0x%x -> Got 0x%x ", A_ptr + (slot * 4), slot, ret_rd_value);
//        error_found |= 1;
//        error_counter++;
//      end
//    end
//    if (error_counter > 5) begin
//      $display("Too many errors found. Exiting check of m00_axi.");
//      slot = LP_MAX_LENGTH;
//    end
//  end
//  error_counter = 0;
`ifdef XBARorM1orM0

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m01_axi
         fail =0;
        for(int row=0;row<size;row=row+block_size)
           for(int col=0;col<size;col=col+block_size)
          for(int i=0;i<block_size;i++)
            for(int j=0;j<block_size;j++)begin
                outC[row+i][col+j] = m01_axi.mem_model.backdoor_memory_read_4byte(baseC + (count * 4));
                count = count+1;
            end
            
          for(int i=0;i<size;i++)
            for(int j=0;j<size;j++)begin
                if(c[i][j] != outC[i][j])
                    fail = 1;
            end
            if(fail)
                $display("MATRIX INCORRECT");
            else
                $display("MATRIX CORRECT");
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m02_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m02_axi.mem_model.backdoor_memory_read_4byte(C_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m02_axi : @0x%x : Expected 0x%x -> Got 0x%x ", C_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m02_axi : @0x%x : Expected 0x%x -> Got 0x%x ", C_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m02_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m03_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m03_axi.mem_model.backdoor_memory_read_4byte(D_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m03_axi : @0x%x : Expected 0x%x -> Got 0x%x ", D_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m03_axi : @0x%x : Expected 0x%x -> Got 0x%x ", D_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m03_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;
`endif
`ifdef XBARorM1
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m04_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m04_axi.mem_model.backdoor_memory_read_4byte(E_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m04_axi : @0x%x : Expected 0x%x -> Got 0x%x ", E_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m04_axi : @0x%x : Expected 0x%x -> Got 0x%x ", E_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m04_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m05_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m05_axi.mem_model.backdoor_memory_read_4byte(F_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m05_axi : @0x%x : Expected 0x%x -> Got 0x%x ", F_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m05_axi : @0x%x : Expected 0x%x -> Got 0x%x ", F_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m05_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m06_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m06_axi.mem_model.backdoor_memory_read_4byte(G_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m06_axi : @0x%x : Expected 0x%x -> Got 0x%x ", G_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m06_axi : @0x%x : Expected 0x%x -> Got 0x%x ", G_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m06_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m07_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m07_axi.mem_model.backdoor_memory_read_4byte(H_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m07_axi : @0x%x : Expected 0x%x -> Got 0x%x ", H_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m07_axi : @0x%x : Expected 0x%x -> Got 0x%x ", H_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m07_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

`endif
`ifdef XBARorM0 
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m08_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m08_axi.mem_model.backdoor_memory_read_4byte(I_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m08_axi : @0x%x : Expected 0x%x -> Got 0x%x ", I_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m08_axi : @0x%x : Expected 0x%x -> Got 0x%x ", I_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m08_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m09_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m09_axi.mem_model.backdoor_memory_read_4byte(J_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m09_axi : @0x%x : Expected 0x%x -> Got 0x%x ", J_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m09_axi : @0x%x : Expected 0x%x -> Got 0x%x ", J_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m09_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m10_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m10_axi.mem_model.backdoor_memory_read_4byte(K_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m10_axi : @0x%x : Expected 0x%x -> Got 0x%x ", K_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m10_axi : @0x%x : Expected 0x%x -> Got 0x%x ", K_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m10_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m11_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m11_axi.mem_model.backdoor_memory_read_4byte(L_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m11_axi : @0x%x : Expected 0x%x -> Got 0x%x ", L_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m11_axi : @0x%x : Expected 0x%x -> Got 0x%x ", L_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m11_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;
`endif
`ifdef XBAR 

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m12_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m12_axi.mem_model.backdoor_memory_read_4byte(M_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m12_axi : @0x%x : Expected 0x%x -> Got 0x%x ", M_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m12_axi : @0x%x : Expected 0x%x -> Got 0x%x ", M_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m12_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m13_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m13_axi.mem_model.backdoor_memory_read_4byte(N_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m13_axi : @0x%x : Expected 0x%x -> Got 0x%x ", N_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m13_axi : @0x%x : Expected 0x%x -> Got 0x%x ", N_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m13_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m14_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m14_axi.mem_model.backdoor_memory_read_4byte(O_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m14_axi : @0x%x : Expected 0x%x -> Got 0x%x ", O_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m14_axi : @0x%x : Expected 0x%x -> Got 0x%x ", O_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m14_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m15_axi
  for (longint unsigned slot = 0; slot < LP_MAX_LENGTH; slot++) begin
    ret_rd_value = m15_axi.mem_model.backdoor_memory_read_4byte(P_ptr + (slot * 4));
    if (slot < LP_MAX_TRANSFER_LENGTH) begin
      if (ret_rd_value != (slot + 1)) begin
        $error("Memory Mismatch: m15_axi : @0x%x : Expected 0x%x -> Got 0x%x ", P_ptr + (slot * 4), slot + 1, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end else begin
      if (ret_rd_value != slot) begin
        $error("Memory Mismatch: m15_axi : @0x%x : Expected 0x%x -> Got 0x%x ", P_ptr + (slot * 4), slot, ret_rd_value);
        error_found |= 1;
        error_counter++;
      end
    end
    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m15_axi.");
      slot = LP_MAX_LENGTH;
    end
  end
  error_counter = 0;
`endif
  return(error_found);
endfunction

bit choose_pressure_type = 0;
bit axis_choose_pressure_type = 0;
bit [0-1:0] axis_tlast_received;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Set up the kernel for operation and set the kernel START bit.
// The task will poll the DONE bit and check the results when complete.
task automatic multiple_iteration(input integer unsigned num_iterations, output bit error_found);
  error_found = 0;

  $display("Starting: multiple_iteration");
  for (integer unsigned iter = 0; iter < num_iterations; iter++) begin

    
    $display("Starting iteration: %d / %d", iter+1, num_iterations);
    RAND_WREADY_PRESSURE_FAILED: assert(std::randomize(choose_pressure_type));
    case(choose_pressure_type)
      0: slv_no_backpressure_wready();
      1: slv_random_backpressure_wready();
    endcase
    RAND_RVALID_PRESSURE_FAILED: assert(std::randomize(choose_pressure_type));
    case(choose_pressure_type)
      0: slv_no_delay_rvalid();
      1: slv_random_delay_rvalid();
    endcase

    set_scalar_registers();
    set_memory_pointers();
    backdoor_fill_memories();
    // Check that Kernel is IDLE before starting.
    poll_idle_register();
    ///////////////////////////////////////////////////////////////////////////
    //Start transfers
    blocking_write_register(KRNL_CTRL_REG_ADDR, CTRL_START_MASK);

    ctrl.wait_drivers_idle();
    ///////////////////////////////////////////////////////////////////////////
    //Wait for interrupt being asserted or poll done register
    @(posedge interrupt);

    ///////////////////////////////////////////////////////////////////////////
    // Service the interrupt
    service_interrupts();
    wait(interrupt == 0);

    ///////////////////////////////////////////////////////////////////////////
    error_found |= check_kernel_result()   ;

    $display("Finished iteration: %d / %d", iter+1, num_iterations);
  end
 endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
//Instantiate AXI4 LITE VIP
initial begin : STIMULUS
  #200000;
  start_vips();
  check_scalar_registers(error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  check_pointer_registers(error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  enable_interrupts();

  multiple_iteration(1, error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end else begin
    $display( "Test completed successfully");
  end
  
    #200000;
 
  $finish;
end

endmodule
`default_nettype wire

