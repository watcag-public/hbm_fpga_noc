#!/bin/bash
wd=`pwd`
$VITIS/bin/v++ --target hw --link --remote_ip_cache ~/workspace/ip_cache --kernel_frequency 225 --profile_kernel data:all:all:all:counters --config common-config.ini --vivado.prop run.impl_1.STEPS.ROUTE_DESIGN.TCL.POST=$wd/post_routing.tcl --config binary_container_1-link.ini -o binary_container_1.xclbin ../src/bft_32_xbar_v2.xo
