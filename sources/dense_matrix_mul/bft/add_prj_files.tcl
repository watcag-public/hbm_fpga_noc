add_files -norecurse -scan_for_includes {./src/}
import_files -norecurse {./src/}

move_files -fileset sim_1 [get_files ./$prj_name/$prj_name.srcs/sources_1/imports/src/bft_tb.sv]

set_property  ip_repo_paths  ../matrix_mul_hls_ip/parallel_blocked_mat_mul/solution1/impl/ip [current_project]
update_ip_catalog
create_ip -name wrapper -vendor xilinx.com -library hls -version 1.0 -module_name wrapper_0
generate_target {instantiation_template} [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/wrapper_0/wrapper_0.xci]
generate_target all [get_files  ./$prj_name/$prj_name.srcs/sources_1/ip/wrapper_0/wrapper_0.xci]
catch { config_ip_cache -export [get_ips -all wrapper_0] }
export_ip_user_files -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/wrapper_0/wrapper_0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ./$prj_name/$prj_name.srcs/sources_1/ip/wrapper_0/wrapper_0.xci]
launch_runs -jobs 2 wrapper_0_synth_1
export_simulation -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/wrapper_0/wrapper_0.xci] -directory ./$prj_name/$prj_name.ip_user_files/sim_scripts -ip_user_files_dir ./$prj_name/$prj_name.ip_user_files -ipstatic_source_dir ./$prj_name/$prj_name.ip_user_files/ipstatic -lib_map_path [list {modelsim=./$prj_name/$prj_name.cache/compile_simlib/modelsim} {questa=./$prj_name/$prj_name.cache/compile_simlib/questa} {ies=./$prj_name/$prj_name.cache/compile_simlib/ies} {xcelium=./$prj_name/$prj_name.cache/compile_simlib/xcelium} {vcs=./$prj_name/$prj_name.cache/compile_simlib/vcs} {riviera=./$prj_name/$prj_name.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet


create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name axis_data_fifo_0
set_property -dict [list CONFIG.TDATA_NUM_BYTES {32} CONFIG.FIFO_DEPTH {32} CONFIG.HAS_TLAST {1}] [get_ips axis_data_fifo_0]
generate_target {instantiation_template} [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]
generate_target all [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]
catch { config_ip_cache -export [get_ips -all axis_data_fifo_0] }
export_ip_user_files -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]
launch_runs -jobs 2 axis_data_fifo_0_synth_1
export_simulation -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci] -directory ./$prj_name/$prj_name.ip_user_files/sim_scripts -ip_user_files_dir ./$prj_name/$prj_name.ip_user_files -ipstatic_source_dir ./$prj_name/$prj_name.ip_user_files/ipstatic -lib_map_path [list {modelsim= ./$prj_name/$prj_name.cache/compile_simlib/modelsim} {questa= ./$prj_name/$prj_name.cache/compile_simlib/questa} {ies= ./$prj_name/$prj_name.cache/compile_simlib/ies} {xcelium= ./$prj_name/$prj_name.cache/compile_simlib/xcelium} {vcs= ./$prj_name/$prj_name.cache/compile_simlib/vcs} {riviera= ./$prj_name/$prj_name.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name axis_data_fifo_1
set_property -dict [list CONFIG.TDATA_NUM_BYTES {33} CONFIG.FIFO_DEPTH {64} CONFIG.HAS_TLAST {1}] [get_ips axis_data_fifo_1]
generate_target {instantiation_template} [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci]
generate_target all [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci]
catch { config_ip_cache -export [get_ips -all axis_data_fifo_1] }
export_ip_user_files -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci]
launch_runs -jobs 2 axis_data_fifo_1_synth_1
export_simulation -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_1/axis_data_fifo_1.xci] -directory ./$prj_name/$prj_name.ip_user_files/sim_scripts -ip_user_files_dir ./$prj_name/$prj_name.ip_user_files -ipstatic_source_dir ./$prj_name/$prj_name.ip_user_files/ipstatic -lib_map_path [list {modelsim= ./$prj_name/$prj_name.cache/compile_simlib/modelsim} {questa= ./$prj_name/$prj_name.cache/compile_simlib/questa} {ies= ./$prj_name/$prj_name.cache/compile_simlib/ies} {xcelium= ./$prj_name/$prj_name.cache/compile_simlib/xcelium} {vcs= ./$prj_name/$prj_name.cache/compile_simlib/vcs} {riviera= ./$prj_name/$prj_name.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet