`include	"commands.h"
 `define SYNTHETIC
 `timescale 1ps / 1ps
module client 
#(

	parameter N 	= 2,		// total number of clients
	parameter M_A_W = 16,
	parameter A_W	= $clog2(N)+1,	// address width
	parameter P_W	= 32,		// data width
	parameter D_W   = 1+M_A_W+A_W+P_W,
	parameter WRAP  = 1,            // wrapping means throttling of reinjection
	parameter PAT   = `RANDOM,      // default RANDOM pattern
	parameter RATE  = 100,           // rate of injection (in percent) 
	parameter LIMIT = 16,           // when to stop injectin packets
	parameter SIGMA = 4,            // radius for LOCAL traffic
	parameter posx 	= 2,		// position
	parameter filename = "posx"
	
)


(
	input	wire			clk,
	input	wire		rst,
	input	wire	`Cmd		cmd_w,
	//last,Destination PE addr,RW,Memory addr, Source addr,   DataWidth
	//1,M_A_W,A_W,1,A_W,D_W
	(*dont_touch ="true"*) input	wire	[ A_W+D_W:0]	c_i,
	(*dont_touch ="true"*) input	wire			c_i_v,
	(*dont_touch ="true"*) output	wire			c_i_bp,

	(*dont_touch ="true"*) output	reg	[ A_W+D_W:0]	c_o=0,
	(*dont_touch ="true"*) output	reg			c_o_v=0,
	(*dont_touch ="true"*) input	wire			c_o_bp,

	output	wire			done
);


reg rst_r = 0;
always@(posedge clk) begin
    rst_r <= rst;
end

generate if(`FP==1) begin
always @(posedge clk)begin
    if(c_i_v & ~c_o_bp) begin
        c_o <= c_i;
        c_o_v <= c_i_v;
    end
end
end
else begin
localparam LEVELS	= $clog2(N);
localparam excess   = LEVELS - 5;
localparam reg[31:0] packet_count = `packet_count;
localparam MAW = 33;

wire [22:0] r_wire;
reg [31:0] r_wire_lfsr;
wire [7:0] r_dest;
reg  [15:0] r_dest_lfsr;
wire [7:0] r;

(* max_fanout = 25 *) reg `Cmd cmd;
assign r_dest = r_dest_lfsr[7:0];
assign r_wire = r_wire_lfsr[22:0];
assign	c_i_bp	= 1'b0;


reg wait_for_write=1'b0;
reg wait_to_send = 1'b0;
reg[31:0]	 attempts,sent,read_request;
reg 			done_sig;
reg	[A_W-1:0]	next = 0; //Destination PE
reg [M_A_W-1:0] mem_addr;//Destination Mem
reg [A_W-1:0]   source_addr=0;
(* max_fanout = 25 *) reg             RW=0;
reg             MemPacket =1;
reg			    next_v = 0;
reg [P_W-1:0] 	tmp=0;
reg [MAW-LEVELS-1:0] offset = 0;
reg error = 0;
reg [31:0] read_response = 0;
reg restart = 0;


function [4:0] bitrev(input [4:0] i);
		bitrev = {i[0],i[1],i[2],i[3],i[4]};
	endfunction

 localparam base = (posx<N/2)?(2*(posx)):(2*(posx)-N+1);

 reg [LEVELS-1:0] loc  =  base;
 reg [LEVELS-1:0] dest =   base;
 reg [LEVELS-1:0] src  =  base;

reg [LEVELS-1:0] next_dest = 0;
	reg [63:0] now=0; 
	
//		lfsr #(.x(posx+1),.bit_width(23)) rand(.CLK(clk),.RESET(rst|restart),.RAND(r_wire),.enable(wait_for_write|c_o_bp|~(r <= cmd`rate)));
//		lfsr #(.x(posx+2)) rand2(.CLK(clk),.RESET(rst|restart),.RAND(r_dest),.enable(wait_for_write|c_o_bp|~(r <= cmd`rate)));
		
		lfsr #(.x(posx+3)) rate_rand(.CLK(clk),.RESET(rst_r),.RAND(r),.enable(0));
integer iter;		

always@(posedge clk)
begin
    cmd <= cmd_w;
	if (rst_r==1'b1)
	begin
	
	    r_dest_lfsr <= posx+2;
	    r_wire_lfsr <= posx+1;
		c_o		<= 'b0;
		c_o_v		<= 1'b0;

		attempts	 <= 0;
		sent		 <= 0;	
		read_request <= 0;
        if(cmd`access_type == 1)
            offset <= (posx+1)<<5;
        else
            offset <= 0;

		case(cmd`channel)
		 `cb: begin
		 

              if((((base[LEVELS-1:excess]) >> 2)&1'b1) == 0) begin
                loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 4);
                dest[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 4);
                src[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 4);
              end
		      else begin
                loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess] - 4);
                dest[LEVELS-1:excess] <= (base[LEVELS-1:excess] - 4);
                src[LEVELS-1:excess]  <= (base[LEVELS-1:excess] - 4);
              end
		  end
		  
		  `cs: begin
		  
                loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 16) & (32 -1 );
                dest[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16) & (32 -1 );
                src[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 16) & (32 -1 );
		  end
		  `br:  begin
		  
                  loc[LEVELS-1:excess]  <=  bitrev(base[LEVELS-1:excess]);
                  dest[LEVELS-1:excess] <=  bitrev(base[LEVELS-1:excess]);
                  src[LEVELS-1:excess]  <=  bitrev(base[LEVELS-1:excess]);
		  end
		  `nn: begin
		  
		      loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess]+1'b1)&(32 -1 );
              dest[LEVELS-1:excess] <= (base[LEVELS-1:excess]+1'b1)&(32 -1 );
              src[LEVELS-1:excess]  <= (base[LEVELS-1:excess]+1'b1)&(32 -1 );
		  end
          `to: begin
               
               loc[LEVELS-1:excess]  <= ((base[LEVELS-1:excess]) + (32/2) - 1) & (32 - 1); 
               dest[LEVELS-1:excess] <= ((base[LEVELS-1:excess]) + (32/2) - 1) & (32 - 1);
               src[LEVELS-1:excess]  <= ((base[LEVELS-1:excess]) + (32/2) - 1) & (32 - 1);
		  end
		  
          `cc: begin
          
               loc[LEVELS-1:excess]  <= 32 - 1 - (base[LEVELS-1:excess]); 
               dest[LEVELS-1:excess] <= 32 - 1 - (base[LEVELS-1:excess]);
               src[LEVELS-1:excess]  <= 32 - 1 - (base[LEVELS-1:excess]);
		  end
		  
		  default : begin	    
		      loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess]); 
              dest[LEVELS-1:excess] <= (base[LEVELS-1:excess]);
              src[LEVELS-1:excess]  <= (base[LEVELS-1:excess]);
		  end
		  
		endcase
	end
	else
	begin
	
		if (c_o_bp==1'b0)
		begin
		
		 if(c_o_v == 1) begin
		 
            $display("Time%0d: Sent packet from PE(%0d) to PE(%0d) with packetid=%0d , data=%X, source_addr =%0d, mem_addr =%X, RW=%d",now,posx,next,((posx)*LIMIT+sent),c_o[P_W-1:0],source_addr,c_o[M_A_W+A_W+P_W-1:A_W+P_W],c_o[D_W-1]);
            $display("Read_request %d Write request %d",read_request,sent);

   
        end
        
        
			if((r <= cmd`rate)) 
			begin
			    
		  		
				c_o_v <= 1'b0;                
				if(next_v==1) 
				begin
				    c_o_v			<= 1'b1;
				    c_o[A_W+D_W-1:D_W]	<= next;
                 
                    c_o[A_W+P_W-1:P_W] <= source_addr;
                    c_o[M_A_W+A_W+P_W-1:A_W+P_W]<= mem_addr;
                    c_o[D_W-1] <= RW;
                    c_o[D_W-2] <= MemPacket;
                    /*PAYLOAD*/
                      case(cmd`operation)
                       `WR:begin
                           if(read_response == packet_count )
                               read_response <= 0;
                               c_o[P_W-1:0]	<= mem_addr;//
                       end
                     endcase
                     
                     case(cmd`operation)
                       `WR:begin
                          c_o[P_W-1:0]	<= mem_addr;//  
                          if(read_response == packet_count )
	                            c_o[P_W-1:0]	<= error<<63 | now;//
                       end
                     endcase
	                   
                    if(RW) begin
                       sent <= sent + 1;
                       case(cmd`access_type)
                           `linear:begin
                               offset <= offset + 32;
                           end
                           `random:begin
                                 offset <= r_wire<<5;
                           end
                       endcase

                   
                       case(cmd`channel)
                           `cs:begin
                              dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                              if(loc[LEVELS-1:excess] < 16) begin
                                if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                    dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                    else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(16)-1)
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                              end
                              else begin
                                     if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < 16 )
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                    else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(32)-1)
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                                end
                           end
                           `cb:begin
                                 
                               dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                               
                               if(loc[LEVELS-1:excess] < 4) begin
                                  if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                        dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                    else if((loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(4)-1)
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                                end
                                
                                else begin
                                     if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < (loc[LEVELS-1:excess] - (loc[LEVELS-1:excess] % 4)))
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                    else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(loc[LEVELS-1:excess]+ (3-(loc[LEVELS-1:excess] % 4))))
                                        dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                               
                               end
                           end
                           
                           default: begin
                                dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                    dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 32;
                                else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>32-1)
                                    dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 32;
                           end

//                           default: begin
//                                dest[LEVELS-1:excess] <= next_dest[LEVELS-1:excess] - (cmd`sigma>>1);
//                                if(next_dest[LEVELS-1:excess] < (cmd`sigma>>1))
//                                    dest[LEVELS-1:excess]<= next_dest[LEVELS-1:excess] - (cmd`sigma>>1) + 32;
//                                else if((next_dest[LEVELS-1:excess]-(cmd`sigma>>1))>32-1)
//                                    dest[LEVELS-1:excess]<= next_dest[LEVELS-1:excess] - (cmd`sigma>>1) - 32;
//                           end
                       endcase               
					   end
					   
					 else begin
			     read_request<=read_request+1'b1;
			        case(cmd`operation)
                            `WR:begin
                                case(cmd`access_type) 
                                    `linear : begin
                                        offset <= offset + 32;
                                    end
                                    `random : begin
                                        offset <= r_wire<<5;	
                                    end
                    endcase
                     case(cmd`channel)
                                   `cs:begin
                                          dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                           if(loc[LEVELS-1:excess] < 16) begin
                                             if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                                 dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                                 else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(16)-1)
                                                     dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                                           end
                                           else begin
                                                  if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < 16 )
                                                     dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                                 else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(32)-1)
                                                     dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                                             end                                   
                                         end
                                   `cb:begin
                                      dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                                          
                                          if(loc[LEVELS-1:excess] < 4) begin
                                             if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                                   dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                               else if((loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(4)-1)
                                                   dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                                           end
                                           
                                           else begin
                                                if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < (loc[LEVELS-1:excess] - (loc[LEVELS-1:excess] % 4)))
                                                   dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                               else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(loc[LEVELS-1:excess]+ (3-(loc[LEVELS-1:excess] % 4))))
                                                   dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                                          
                                           end
                                     end
                               
                                   default: begin
                                     dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                   if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                       dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 32;
                                   else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>32-1)
                                       dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 32;
                                    end
//                           default: begin
//                                dest[LEVELS-1:excess] <= next_dest[LEVELS-1:excess] - (cmd`sigma>>1);
//                                if(next_dest[LEVELS-1:excess] < (cmd`sigma>>1))
//                                    dest[LEVELS-1:excess]<= next_dest[LEVELS-1:excess] - (cmd`sigma>>1) + 32;
//                                else if((next_dest[LEVELS-1:excess]-(cmd`sigma>>1))>32-1)
//                                    dest[LEVELS-1:excess]<= next_dest[LEVELS-1:excess] - (cmd`sigma>>1) - 32;
//                                end
                               endcase
                                               
                               
                           end  
                       endcase
 
					   end
					   next_dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1));
					     if(restart == 1 ) begin
					       r_dest_lfsr <= posx+2;
					       r_wire_lfsr <= posx+1;
					    end
					    else begin   
					    
                            r_dest_lfsr[0] <= r_dest_lfsr[15] ^ r_dest_lfsr[13] ^ r_dest_lfsr[12] ^ r_dest_lfsr[10];
                            for(iter=0;iter<15;iter=iter+1)
                                  r_dest_lfsr[iter+1]<=r_dest_lfsr[iter];
                                  
                            r_wire_lfsr[0] <= r_wire_lfsr[31] ^ r_wire_lfsr[21] ^ r_wire_lfsr[1] ^ r_wire_lfsr[0];
                            for(iter=0;iter<31;iter=iter+1)
                                r_wire_lfsr[iter+1]<=r_wire_lfsr[iter];
                        end        
                          

                       case(cmd`operation)
                           `WR:begin
                                if(sent == packet_count-2) 
                                     restart <= 1;
                                else
					                 restart <= 0;
					       
                                 if(sent == packet_count-1) begin
					                 offset <= 0;
					                 dest <= loc;
					                 if (cmd`access_type) begin
					                    offset <= (posx+1)<<5;     
					                   end
                                end
                           end
                       endcase

				end
			end
			else
			begin
				c_o_v	<= 1'b0;
			end
		end
		else
		begin
//			c_o_v	<= 1'b0;
		end
	
		if ((attempts<LIMIT & {r} < RATE) )
		begin
			attempts	<= attempts + 1;
		end
		
		if(c_i_v==1'b1) 
		begin
			$display("Time%0d: Received packet at PE(%0d) with address=%0x packetid=%0x ",now-1,posx,c_i[255:224],c_i[31:0]);
             case(cmd`operation)
                   `WR:begin
                       read_response <= read_response + 1'b1;
                        $display("%x",read_response);
                        if(c_i[31:0] != c_i[255:224]) 
                            error <= 1'b1;
                         end
               endcase			
			
		end
	end
end

always @(*) 
begin
next_v <= 0;
next <= 0;    
RW <= 0;
mem_addr <= {dest,offset};
source_addr <= posx;
    if(now >= 0 && sent <packet_count)
        begin
                next     <= {1'b1,{A_W-1{1'b0}}};
                mem_addr <= {dest,offset};
                source_addr <= posx;
                RW <= 1;
                if(sent <packet_count) begin         
                    next_v <=1;
                end
                else begin
                next_v <=0;
                end                   
            end      
    else if(now >= 0 && sent==packet_count && read_request <packet_count  )
        begin
                next     <= {1'b1,{A_W-1{1'b0}}};
                mem_addr <= {dest,offset};
                source_addr <= posx;
                RW <= 0;
            
                if(read_request <packet_count) begin  
                    next_v <=1;
                end
                
                else begin
                next_v <=0;
                end                   
            end
     else if( now >= 0 && read_response == packet_count) begin
                    
                     next     <= {1'b1,{A_W-1{1'b0}}};
                 mem_addr <= {loc,{MAW-LEVELS{1'b0}}};
                 source_addr <= posx;            
                 RW <= 1;
                 next_v <=1;
       end         
            
     else begin
            source_addr <= posx;
            mem_addr<= 0;
            RW <= 0;
     end
 end
 



always @(posedge clk) begin
if(rst_r) begin
   now<=0;
end
else begin
      now     <= now + 1;
end
end
end 
endgenerate
endmodule

