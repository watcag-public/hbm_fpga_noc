`timescale 1ns / 1ps

//credits: http://billauer.co.il/reg_fifo.html

module fwft_fifo
#(parameter DW = 10,
  parameter WATERMARK = 16
)
(
    input wire clk,
    input wire srst,
    input wire [DW:0] din,
    input wire wr_en,
    output wire full,
    output wire  [DW:0] dout,
    input wire rd_en,
    output wire empty

    );
   reg                   dout_valid=0;

   
   wire                  fifo_rd_en, fifo_empty;
   
   reg [3:0] prog_full_thresh = WATERMARK;


    fifo_generator_1 fifo
(
	.clk		(clk				),
	.srst		(srst				),
	.din		(din),//
	.wr_en		(wr_en),//
	.prog_full		(full			),//
	.dout		(dout			),//
	.rd_en		(fifo_rd_en),//
	.empty		(fifo_empty			),//
	.prog_full_thresh(prog_full_thresh-1)
);

   assign fifo_rd_en = !fifo_empty && (!dout_valid || rd_en);
   assign empty = !dout_valid;
   
   
   always @(posedge clk) begin

    if (fifo_rd_en) begin
       dout_valid <= 1;
     end
    else if (rd_en)
       dout_valid <= 0;
  
    end

endmodule

