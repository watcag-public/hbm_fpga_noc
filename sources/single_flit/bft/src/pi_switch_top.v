`define XLNXSRLFIFO
`define HYPERFLEX
module pi_switch_top
#
(
	parameter	N	= 2,			//number of clients
	parameter	WRAP	= 1,			//crossbar?
		parameter	P_W	= 32,
	parameter A_W	= $clog2(N)+1,
	 parameter M_A_W = 16,
	parameter D_W	= 1+M_A_W+A_W+P_W,
	parameter	posl	= 0,			//which level
	parameter	posx	= 0,		//which position
	parameter	DEBUG	= 1,
	parameter	FD	= 10,
	parameter	HR	= 4
)
(
	input  wire 			clk,		// clock
	input  wire 			rst,		// reset
//	input  wire 			ce,		// clock enable
	
	input  	wire	[A_W+D_W-1:0] 	s_axis_l_wdata	,	
	output	wire			s_axis_l_wready	,
	input	wire			s_axis_l_wvalid	,
	input	wire			s_axis_l_wlast	,
	
	input  	wire	[A_W+D_W-1:0] 	s_axis_r_wdata	,
	output	wire			s_axis_r_wready	,
	input	wire			s_axis_r_wvalid	,
	input	wire			s_axis_r_wlast	,
	
	input  	wire	[A_W+D_W-1:0] 	s_axis_u0_wdata	,
	output	wire			s_axis_u0_wready, 
	input	wire			s_axis_u0_wvalid, 
	input	wire			s_axis_u0_wlast	,

	input  	wire	[A_W+D_W-1:0] 	s_axis_u1_wdata	,
	output	wire			s_axis_u1_wready, 
	input	wire			s_axis_u1_wvalid, 
	input	wire			s_axis_u1_wlast	,

	output 	wire	[A_W+D_W-1:0] 	m_axis_l_wdata	,
	input	wire			m_axis_l_wready	,
	output	wire			m_axis_l_wvalid	,
	output	wire			m_axis_l_wlast	,	

	output 	wire	[A_W+D_W-1:0] 	m_axis_r_wdata	,
	input	wire			m_axis_r_wready	,
	output	wire			m_axis_r_wvalid	,
	output	wire			m_axis_r_wlast	,	

	output 	wire	[A_W+D_W-1:0] 	m_axis_u0_wdata	,
	input	wire			m_axis_u0_wready,
	output	wire			m_axis_u0_wvalid,
	output	wire			m_axis_u0_wlast	,	

	output 	wire	[A_W+D_W-1:0] 	m_axis_u1_wdata	,
	input	wire			m_axis_u1_wready,
	output	wire			m_axis_u1_wvalid,
	output	wire			m_axis_u1_wlast		
//	output 	wire 			done		// done
);

generate begin

reg     [A_W+D_W-1:0]   previous_l;
reg     [A_W+D_W-1:0]   previous_r;
reg     [A_W+D_W-1:0]   previous_u0;
reg     [A_W+D_W-1:0]   previous_u1;

wire	[A_W+D_W:0]	pi_o_d_l;
wire			pi_o_v_l;
wire			pi_o_b_l;

wire	[A_W+D_W:0]	pi_o_d_r;
wire			pi_o_v_r;
wire			pi_o_b_r;

wire	[A_W+D_W:0]	pi_o_d_u0;
wire			pi_o_v_u0;
wire			pi_o_b_u0;

wire	[A_W+D_W:0]	pi_o_d_u1;
wire			pi_o_v_u1;
wire			pi_o_b_u1;

if(0) begin 
 
wire	[A_W+D_W:0]	pi_o_d_l_bp;
wire			    pi_o_v_l_bp;
wire			    pi_o_b_l_bp;

 (*keep_hierarchy = "true"*) pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(0)    // Pipeline length
)
bp_maxis_L
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(pi_o_v_l	),
	.i_d		(pi_o_d_l	), 
 	.i_b		(pi_o_b_l	),
 	.o_v		(pi_o_v_l_bp	),
	.o_d		(pi_o_d_l_bp	),	 
	.o_b		(pi_o_b_l_bp) // unregistered from DOR logic
);

assign	m_axis_l_wdata	= pi_o_d_l_bp [A_W+D_W-1:0];
assign	m_axis_l_wvalid = pi_o_v_l_bp;
assign	pi_o_b_l_bp	   = ~m_axis_l_wready;
assign	m_axis_l_wlast	= pi_o_d_l_bp[A_W+D_W];

 
wire	[A_W+D_W:0]	pi_o_d_r_bp;
wire			    pi_o_v_r_bp;
wire			    pi_o_b_r_bp;

 (*keep_hierarchy = "true"*) pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(0)    // Pipeline length
)
bp_maxis_R
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(pi_o_v_r	),
	.i_d		(pi_o_d_r	), 
 	.i_b		(pi_o_b_r	),
 	.o_v		(pi_o_v_r_bp	),
	.o_d		(pi_o_d_r_bp	),	 
	.o_b		(pi_o_b_r_bp) // unregistered from DOR logic
);

assign	m_axis_r_wdata	= pi_o_d_r_bp [A_W+D_W-1:0];
assign	m_axis_r_wvalid = pi_o_v_r_bp;
assign	pi_o_b_r_bp	   = ~m_axis_r_wready;
assign	m_axis_r_wlast	= pi_o_d_r_bp[A_W+D_W];

end 
else begin

    assign	m_axis_l_wdata	= pi_o_d_l[A_W+D_W-1:0];
    assign	m_axis_l_wvalid	= pi_o_v_l;
    assign	pi_o_b_l	= ~m_axis_l_wready;
    assign	m_axis_l_wlast	= pi_o_d_l[A_W+D_W];
    
    assign	m_axis_r_wdata	= pi_o_d_r[A_W+D_W-1:0];
    assign	m_axis_r_wvalid	= pi_o_v_r;
    assign	pi_o_b_r	= ~m_axis_r_wready;
    assign	m_axis_r_wlast	= pi_o_d_r[A_W+D_W];
    

end


if(posl >=5)  begin // to do. Dont need pipe for l <= 1. do what you have done for maxis l and r ((posl == 2) && (posx>=12 && posx <=15))||(posl==3 && (posx>=7&&posx<=15))

wire	[A_W+D_W:0]	pi_o_d_u0_bp;
wire			pi_o_v_u0_bp;
wire			pi_o_b_u0_bp;

 (*keep_hierarchy = "true"*) pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(0)    // Pipeline length
)
bp_maxis_U0
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(pi_o_v_u0	),
	.i_d		(pi_o_d_u0	), 
 	.i_b		(pi_o_b_u0	),
 	.o_v		(pi_o_v_u0_bp	),
	.o_d		(pi_o_d_u0_bp	),	 
	.o_b		(	pi_o_b_u0_bp) // unregistered from DOR logic
);

assign	m_axis_u0_wdata	= pi_o_d_u0_bp [A_W+D_W-1:0];
assign	m_axis_u0_wvalid= pi_o_v_u0_bp;
assign	pi_o_b_u0_bp	= ~m_axis_u0_wready;
assign	m_axis_u0_wlast	= pi_o_d_u0[A_W+D_W];


wire	[A_W+D_W:0]	pi_o_d_u1_bp;
wire			pi_o_v_u1_bp;
wire			pi_o_b_u1_bp;

  (*keep_hierarchy = "true"*)pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(0)    // Pipeline length
)
bp_maxis_u1
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(pi_o_v_u1	),
	.i_d		(pi_o_d_u1	), 
 	.i_b		(pi_o_b_u1	),
 	.o_v		(pi_o_v_u1_bp	),
	.o_d		(pi_o_d_u1_bp	),	 
	.o_b		(	pi_o_b_u1_bp) // unregistered from DOR logic
);

assign	m_axis_u1_wdata	= pi_o_d_u1_bp [A_W+D_W-1:0];
assign	m_axis_u1_wvalid= pi_o_v_u1_bp;
assign	pi_o_b_u1_bp	= ~m_axis_u1_wready;
assign	m_axis_u1_wlast	= pi_o_d_u1[A_W+D_W];


end else begin
assign	m_axis_u0_wdata	= pi_o_d_u0[A_W+D_W-1:0];
assign	m_axis_u0_wvalid= pi_o_v_u0;
assign	pi_o_b_u0	= ~m_axis_u0_wready;
assign	m_axis_u0_wlast	= pi_o_d_u0[A_W+D_W];

assign	m_axis_u1_wdata	= pi_o_d_u1[A_W+D_W-1:0];
assign	m_axis_u1_wvalid= pi_o_v_u1;
assign	pi_o_b_u1	= ~m_axis_u1_wready;
assign	m_axis_u1_wlast	= pi_o_d_u1[A_W+D_W];


end


wire	[A_W+D_W:0]	pi_i_d_l;
wire			pi_i_v_l;
wire			pi_i_b_l;

wire	[A_W+D_W:0]	pi_i_d_r;
wire			pi_i_v_r;
wire			pi_i_b_r;

wire	[A_W+D_W:0]	pi_i_d_u0;
wire			pi_i_v_u0;
wire			pi_i_b_u0;

wire	[A_W+D_W:0]	pi_i_d_u1;
wire			pi_i_v_u1;
wire			pi_i_b_u1;

  (*keep_hierarchy = "true"*)pi_switch
#(
	.N		(N	),	
	.WRAP		(WRAP	),
	.A_W		(A_W	),
	.D_W		(D_W	),
	.posl  		(posl	),
	.posx 		(posx	)
	,.P_W(P_W),.M_A_W(M_A_W)
)
pi_switch_inst
(
	.clk		(clk		),		// clock
	.rst		(rst		),		// reset
//	.ce		(ce		),		// clock enable
	`ifdef SIM
	.done		(done		),	// done
	`endif
	.l_i		(pi_i_d_l	),	// left  input payload
	.l_i_bp		(pi_i_b_l	),	// left  input backpressured
	.l_i_v		(pi_i_v_l	),	// left  input valid
	.r_i		(pi_i_d_r	),	// right input payload
	.r_i_bp		(pi_i_b_r	),	// right input backpressured
	.r_i_v		(pi_i_v_r	),	// right input valid
	.u0_i		(pi_i_d_u0	),	// u0    input payload
	.u0_i_bp	(pi_i_b_u0	),	// u0    input backpressured
	.u0_i_v		(pi_i_v_u0	),	// u0    input valid
	.u1_i		(pi_i_d_u1	),	// u1    input payload
	.u1_i_bp	(pi_i_b_u1	),	// u1    input backpressured
	.u1_i_v		(pi_i_v_u1	),	// u1    input valid
	.l_o		(pi_o_d_l	),	// left  input payload
	.l_o_bp		(pi_o_b_l	),	// left  input backpressured
	.l_o_v		(pi_o_v_l	),	// left  input valid
	.r_o		(pi_o_d_r	),	// right input payload
	.r_o_bp		(pi_o_b_r	),	// right input backpressured
	.r_o_v		(pi_o_v_r	),	// right input valid
	.u0_o		(pi_o_d_u0	),	// u0    input payload
	.u0_o_bp	(pi_o_b_u0	),	// u0    input backpressured
	.u0_o_v		(pi_o_v_u0	),	// u0    input valid
	.u1_o		(pi_o_d_u1	),	// u1    input payload
	.u1_o_bp	(pi_o_b_u1	),	// u1    input backpressured
	.u1_o_v		(pi_o_v_u1	)	// u1    input valid
);

wire	[A_W+D_W:0]	bp_i_d_l;
wire			bp_i_v_l;
wire			bp_i_b_l;

wire	[A_W+D_W:0]	bp_o_d_l;
wire			bp_o_v_l;
wire			bp_o_b_l;


assign	bp_i_v_l	= s_axis_l_wvalid;
assign	bp_i_d_l	= {s_axis_l_wlast, s_axis_l_wdata};
assign	s_axis_l_wready	= ~bp_i_b_l;

assign	pi_i_v_l	= bp_o_v_l;
assign	pi_i_d_l	= bp_o_d_l;
assign	bp_o_b_l	= pi_i_b_l;
if(posl == 7) begin // <=2 

 (*keep_hierarchy = "true"*)pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(2)    // Pipeline length
)
bp_L
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(bp_i_v_l	),
	.i_d		(bp_i_d_l	), 
 	.i_b		(bp_i_b_l	),
 	.o_v		(bp_o_v_l	),
	.o_d		(bp_o_d_l	),	 
	.o_b		(bp_o_b_l	) // unregistered from DOR logic
);

end else begin


 (*keep_hierarchy = "true"*)shadow_reg_combi
#(
	.D_W		(D_W	),
	.A_W		(A_W	)
	,.P_W(P_W),.M_A_W(M_A_W)
)
bp_L
(
	.clk		(clk		), 
//	.rst		(rst		), 
//	.i_v		(bp_i_v_l_r	),
//	.i_d		(bp_i_d_l_r	), 	
	.i_v		(bp_i_v_l	),
	.i_d		(bp_i_d_l	), 
 	.i_b		(bp_i_b_l	),
 	.o_v		(bp_o_v_l	),
	.o_d		(bp_o_d_l	),	 
	.o_b		(bp_o_b_l	) // unregistered from DOR logic
);

end
wire	[A_W+D_W:0]	bp_i_d_r;
wire			bp_i_v_r;
wire			bp_i_b_r;

wire	[A_W+D_W:0]	bp_o_d_r;
wire			bp_o_v_r;
wire			bp_o_b_r;


assign	bp_i_v_r	= s_axis_r_wvalid;
assign	bp_i_d_r	= {s_axis_r_wlast, s_axis_r_wdata};
assign	s_axis_r_wready	= ~bp_i_b_r;

assign	pi_i_v_r	= bp_o_v_r;
assign	pi_i_d_r	= bp_o_d_r;
assign	bp_o_b_r	= pi_i_b_r;
if((posl==3 && (posx>=7&&posx<=15))||(posl==4)) begin

 (*keep_hierarchy = "true"*)pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(2)    // Pipeline length
)
bp_R
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(bp_i_v_r	),
	.i_d		(bp_i_d_r	), 
 	.i_b		(bp_i_b_r	),
 	.o_v		(bp_o_v_r	),
	.o_d		(bp_o_d_r	),	 
	.o_b		(bp_o_b_r	) // unregistered from DOR logic
);
end else begin


 (*keep_hierarchy = "true"*)shadow_reg_combi
#(
	.D_W		(D_W	),
	.A_W		(A_W	)
	,.P_W(P_W),.M_A_W(M_A_W)
)
bp_R
(
	.clk		(clk		), 
//	.rst		(rst		), 
//	.i_v		(bp_i_v_r_r	),
//	.i_d		(bp_i_d_r_r	), 	
	.i_v		(bp_i_v_r	),
	.i_d		(bp_i_d_r	), 
 	.i_b		(bp_i_b_r	),
 	.o_v		(bp_o_v_r	),
	.o_d		(bp_o_d_r	),	 
	.o_b		(bp_o_b_r	) // unregistered from DOR logic
);


end
wire	[A_W+D_W:0]	bp_i_d_u0;
wire			bp_i_v_u0;
wire			bp_i_b_u0;

wire	[A_W+D_W:0]	bp_o_d_u0;
wire			bp_o_v_u0;
wire			bp_o_b_u0;


assign	bp_i_v_u0	= s_axis_u0_wvalid;
assign	bp_i_d_u0	= {s_axis_u0_wlast, s_axis_u0_wdata};
assign	s_axis_u0_wready= ~bp_i_b_u0;

assign	pi_i_v_u0	= bp_o_v_u0;
assign	pi_i_d_u0	= bp_o_d_u0;
assign	bp_o_b_u0	= pi_i_b_u0;

if(((posl == 2) && (posx>=12 && posx <=15)) || ((posl == 3) && (posx>=7 && posx <=15))) begin
  (*keep_hierarchy = "true"*) pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(2)    // Pipeline length
)
bp_U0
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(bp_i_v_u0	),
	.i_d		(bp_i_d_u0	), 
 	.i_b		(bp_i_b_u0	),
 	.o_v		(bp_o_v_u0	),
	.o_d		(bp_o_d_u0	),	 
	.o_b		(bp_o_b_u0	) // unregistered from DOR logic
);

end else begin

  (*keep_hierarchy = "true"*) shadow_reg_combi
#(
	.D_W		(D_W	),
	.A_W		(A_W	)
	,.P_W(P_W),.M_A_W(M_A_W)
)
bp_U0
(
	.clk		(clk		), 
//	.rst		(rst		), 
//	.i_v		(bp_i_v_u0_r	),
//	.i_d		(bp_i_d_u0_r	), 
	.i_v		(bp_i_v_u0	),
	.i_d		(bp_i_d_u0	), 
 	.i_b		(bp_i_b_u0	),
 	.o_v		(bp_o_v_u0	),
	.o_d		(bp_o_d_u0	),	 
	.o_b		(bp_o_b_u0	) // unregistered from DOR logic
);

end

wire	[A_W+D_W:0]	bp_i_d_u1;
wire			bp_i_v_u1;
wire			bp_i_b_u1;

wire	[A_W+D_W:0]	bp_o_d_u1;
wire			bp_o_v_u1;
wire			bp_o_b_u1;


assign	bp_i_v_u1	= s_axis_u1_wvalid;
assign	bp_i_d_u1	= {s_axis_u1_wlast, s_axis_u1_wdata};
assign	s_axis_u1_wready= ~bp_i_b_u1;

assign	pi_i_v_u1	= bp_o_v_u1;
assign	pi_i_d_u1	= bp_o_d_u1;
assign	bp_o_b_u1	= pi_i_b_u1;
if(((posl == 2) && (posx>=12 && posx <=15)) || ((posl == 3) && (posx>=7 && posx <=15)) ) begin // <= 1

 (*keep_hierarchy = "true"*)pipe_bp
#(
	.D_W		(A_W+D_W+1	),
  .FD(16),  // FIFO depth
  .HR(2)    // Pipeline length
)
bp_U1
(
	.clk		(clk		), 
//	.rst		(rst		), 
	.i_v		(bp_i_v_u1	),
	.i_d		(bp_i_d_u1	), 
 	.i_b		(bp_i_b_u1	),
 	.o_v		(bp_o_v_u1	),
	.o_d		(bp_o_d_u1	),	 
	.o_b		(bp_o_b_u1	) // unregistered from DOR logic
);
end else begin

 (*keep_hierarchy = "true"*)shadow_reg_combi
#(
	.D_W		(D_W	),
	.A_W		(A_W	)
	,.P_W(P_W),.M_A_W(M_A_W)

)
bp_U1
(
	.clk		(clk		), 
//	.rst		(rst		), 
//	.i_v		(bp_i_v_u1_r	),
//	.i_d		(bp_i_d_u1_r	), 
	.i_v		(bp_i_v_u1	),
	.i_d		(bp_i_d_u1	), 
 	.i_b		(bp_i_b_u1	),
 	.o_v		(bp_o_v_u1	),
	.o_d		(bp_o_d_u1	),	 
	.o_b		(bp_o_b_u1	) // unregistered from DOR logic
);
end

end
endgenerate
endmodule
