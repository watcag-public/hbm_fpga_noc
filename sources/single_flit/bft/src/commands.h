`include "system.h"

//`define UNIT_TEST_REACHABILITY2
//`define UNIT_TEST_REACHABILITY4
//`define UNIT_TEST_TRIANGLE_T
//`define UNIT_TEST_TRIANGLE_PI
//`define MEM_WRITE

`define LOCAL   1
`define ROOT    0

// `define MEM_TEST
//`define DEBUG
//`define DUMP 

// Testbench commands
`define Cmd [19:0]

`define rate [19:12]

`define access_type [11]

`define linear 1'b0
`define random 1'b1

`define operation [10:9]

`define WR 2'b00
`define RO 2'b01
`define WO 2'b10
`define RW 2'b11

`define channel [8:6]

`define p2p 3'b000
`define cb  3'b001
`define cs  3'b010

`define br  3'b011 
`define to  3'b100
`define nn  3'b101
`define cc  3'b110

`define sigma [5:0]


`define RANDOM 0

`define MAW 33+A_W+P_W-1:A_W+P_W
`define AW  A_W+P_W-1:P_W
`define APW 33-1:0

`define packet_count 2**23

`define FP 0
