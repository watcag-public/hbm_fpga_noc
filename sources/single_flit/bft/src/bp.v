`timescale 1ps / 1ps
module shadow_reg_combi 
#(
   
	parameter	P_W	= 32,
	parameter	A_W	= 32,
	 parameter M_A_W = 16,
	parameter D_W	= 1+M_A_W+A_W+P_W,
	parameter	posl	= 0,
	parameter	posx	= 0
)
(
	input	wire			clk, 

	input 	wire 			i_v,
	input 	wire	[A_W+D_W:0]	i_d, 
	output 	wire 			i_b,
	output 	wire 			o_v,
	output 	wire 	[A_W+D_W:0] 	o_d, 
	input 	wire 			o_b // unregistered from DOR logic
);

   // shadow register
   (* max_fanout = 1 *)reg	s_v_r=0;
    (* max_fanout = 25 *)reg o_b_r=0;
   reg [A_W+D_W-1:0] s_d_r;
   always @(posedge clk) begin

		   o_b_r <= o_b;
		   if(!o_b_r) begin
			s_v_r <= i_v;
			s_d_r <= i_d;
		   end
   end
   assign o_v = (o_b_r)?s_v_r:i_v;
   assign o_d = (o_b_r)?s_d_r:i_d;
   assign i_b = o_b_r; // input backpressure is registered
endmodule 

