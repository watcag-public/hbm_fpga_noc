
add_files -norecurse -scan_for_includes {./src/}
import_files -norecurse {./src/}
# add_files -norecurse -scan_for_includes {./src/bp.v ./src/writeToAxi.v ./src/client_bp_top.v ./src/bft_32_xbar_v2_control_s_axi.v ./src/bft.v ./src/bft_32_xbar.v ./src/bft_32_xbar_v2_example.sv ./src/client_bp.v ./src/fifo.v ./src/bft_tb.sv ./src/lfsr.v}
# import_files -norecurse {./src/bp.v ./src/writeToAxi.v ./src/client_bp_top.v ./src/bft_32_xbar_v2_control_s_axi.v ./src/bft.v ./src/bft_32_xbar.v ./src/bft_32_xbar_v2_example.sv ./src/client_bp.v ./src/fifo.v ./src/bft_tb.sv ./src/lfsr.v}
move_files -fileset sim_1 [get_files  ./$prj_name/$prj_name.srcs/sources_1/imports/src/bft_tb.sv]

create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name axis_data_fifo_0
set_property -dict [list CONFIG.TDATA_NUM_BYTES {25} CONFIG.FIFO_DEPTH {256} CONFIG.HAS_TLAST {1}] [get_ips axis_data_fifo_0]
generate_target {instantiation_template} [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]
generate_target all [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]

catch { config_ip_cache -export [get_ips -all axis_data_fifo_0] }
export_ip_user_files -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci]
launch_runs -jobs 2 axis_data_fifo_0_synth_1

export_simulation -of_objects [get_files ./$prj_name/$prj_name.srcs/sources_1/ip/axis_data_fifo_0/axis_data_fifo_0.xci] -directory ./$prj_name/$prj_name.ip_user_files/sim_scripts -ip_user_files_dir ./$prj_name/$prj_name.ip_user_files -ipstatic_source_dir ./$prj_name/$prj_name.ip_user_files/ipstatic -lib_map_path [list {modelsim= ./$prj_name/$prj_name.cache/compile_simlib/modelsim} {questa= ./$prj_name/$prj_name.cache/compile_simlib/questa} {ies= ./$prj_name/$prj_name.cache/compile_simlib/ies} {xcelium= ./$prj_name/$prj_name.cache/compile_simlib/xcelium} {vcs= ./$prj_name/$prj_name.cache/compile_simlib/vcs} {riviera= ./$prj_name/$prj_name.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet
