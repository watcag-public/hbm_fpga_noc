`include "phalanx.h"
`include "phalanx_tb.h"
`include "parameters.vh"
 `define SYNTHETIC

 `timescale 1ps / 1ps
module Client3 #(
	parameter PAT		= `RANDOM,
	parameter RATE		= 5,
	parameter INTERVAL	= 16,
	parameter PACKLIMIT     = 16,
	parameter SIGMA		= 3,
    parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter X_W		= 2,
	parameter Y_W		= 2,
	parameter D_W		= 1+X_W+Y_W+M_A_W + P_W,	// data width
	parameter CORES		= 4,
	parameter X_MAX		= 1<<X_W,
	parameter Y_MAX		= 1<<Y_W,
	parameter X			= 0,	// X address of this node
	parameter Y			= 0		// Y address of this node
) (
	input  wire clk,		// clock
	input  wire rst,		// reset
	(* dont_touch = "true" *)input  wire ce,			// clock enable
	(* dont_touch = "true" *)input  wire `Cmd cmd_w,	// client test command
	(* dont_touch = "true" *)input  wire i_ack,		// router input accepted this cycle
	(* dont_touch = "true" *)input  wire o_v,		// router output message valid
	(* dont_touch = "true" *)input  wire `Msg o,		// router output message from router
	(* dont_touch = "true" *)output reg  `Msg i,		// router input message, registered, validated by i`v
	output wire done,		// done
	output [63:0] received
);

 localparam posx = ((Y) + (X)*(Y_MAX-1));
(* max_fanout = 25 *) reg `Cmd cmd;
 reg rst_r = 0;
reg [15:0] cnt = 0;
 wire [7:0] r;

 always@(posedge clk) begin
     rst_r <= rst;
     cmd <= cmd_w;
 end
 lfsr #(.x(posx+3)) rate_rand(.CLK(clk),.RESET(rst),.RAND(r),.enable(i_ack));
// `ifdef FP
generate if(`FP==1) begin 
 lfsr #(.x(posx+2)) rand2(.CLK(clk),.RESET(rst|restart),.RAND(r_dest),.enable(~firstPacket&(~next_v|~(r <= cmd`rate))));
  always @(posedge clk) begin
     if(o_v ) begin
       cnt <= o;
     end
     
     i <= cnt+r;
     
 end
end
else begin


 localparam N = 32; // change it to parameter
 localparam LEVELS	= $clog2(N);
 localparam excess   = LEVELS - 5;
 localparam reg[31:0] packet_count = 2**23;
 localparam MAW = 33;
 localparam A_W = X_W+Y_W;

 localparam MAX_PENDING = 8;
 //reg [19:0] cmd = 'h0xff820;

// wire [22:0] r_wire;
// reg [31:0] r_wire_lfsr;
 wire [7:0] r_dest;
 reg  [15:0] r_dest_lfsr;

 assign r_dest = r_dest_lfsr[7:0];
// assign r_wire = r_wire_lfsr[22:0];

 assign	c_i_bp	= 1'b0;


 reg wait_for_write=1'b0;
 reg wait_to_send = 1'b0;
 reg[31:0]	 attempts,sent,read_request;
 reg 			done_sig;
 reg	[A_W-1:0]	next = 0; //Destination PE
 reg [M_A_W-1:0] mem_addr;//Destination Mem
 reg [A_W-1:0]   source_addr=0;
 reg             RW=0;
 reg             MemPacket =1;
 reg			    next_v = 0;
 reg [P_W-1:0] 	tmp=0;
 reg [MAW-LEVELS-1:0] offset = 0;
 reg error = 0;
 reg [31:0] read_response = 0;
 reg restart = 0;

 wire [7:0] pending;
 assign pending = read_request - read_response;
 function [4:0] bitrev(input [4:0] i);
 		bitrev = {i[0],i[1],i[2],i[3],i[4]};
 	endfunction

  localparam base = posx;//(posx<N/2)?(2*(posx)):(2*(posx)-N+1);

  reg [LEVELS-1:0] loc  =   posx;//(posx<N/2)?(2*(posx)):(2*(posx)-N+1);
  reg [LEVELS-1:0] dest =   posx;//(posx<N/2)?(2*(posx)):(2*(posx)-N+1);
  reg [LEVELS-1:0] src  =   posx;//(posx<N/2)?(2*(posx)):(2*(posx)-N+1);

 	reg [63:0] now=0;
 	reg newPacket=0;
 	reg firstPacket=1;
//		lfsr #(.x(posx+1),.bit_width(23)) rand(.CLK(clk),.RESET(rst|restart),.RAND(r_wire),.enable(~firstPacket&(~next_v|~(r <= cmd`rate))));
//		lfsr #(.x(posx+2)) rand2(.CLK(clk),.RESET(rst|restart),.RAND(r_dest),.enable(~firstPacket&(~next_v|~(r <= cmd`rate))));
		
 		
 integer iter;





always@(posedge clk)
begin
	if (rst_r==1'b1)
	begin
	    r_dest_lfsr <= posx+2;
//	    r_wire_lfsr <= posx+1;
		i		<= 'b0;
		i`v		<= 1'b0;
		attempts	 <= 0;
		sent		 <= 0;	
		read_request <= 0;
        if(cmd`access_type == 1)
            offset <= (posx+1)<<5;
        else
            offset <= 0;
		wait_for_write <= 1'b0;
		wait_to_send <= 1'b0;
		firstPacket <= 1'b1;
        newPacket <= 1'b0;
		case(cmd`channel)
		 `cb: begin
		 
              if((loc[LEVELS-1:excess]) < 16) begin
                loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 4) & (16 -1 );
                dest[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 4) & (16 -1);
                src[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 4) & (16 -1 );
              end
		      else begin
		        loc[LEVELS-1:excess]  <=  16 + ((base[LEVELS-1:excess] +4) & (16 -1 )) ;        
                dest[LEVELS-1:excess] <=  16 + ((base[LEVELS-1:excess] +4) & (16 -1 ))  ;          
                src[LEVELS-1:excess]  <=  16 + ((base[LEVELS-1:excess] +4) & (16 -1 )) ;        
		      end
		      
		  end
		  
		  `cs: begin
		  
    
                loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 16) & (32 -1 );
                dest[LEVELS-1:excess] <= (base[LEVELS-1:excess] + 16) & (32 -1 );
                src[LEVELS-1:excess]  <= (base[LEVELS-1:excess] + 16) & (32 -1 );
        
//		      loc  <= (posx<N/2)?((2*posx+(N/2))%N):(posx+(posx-N/2+1))%N;
//		      dest <= (posx<N/2)?((2*posx+(N/2))%N):(posx+(posx-N/2+1))%N;
//		      src  <= (posx<N/2)?((2*posx+(N/2))%N):(posx+(posx-N/2+1))%N;
		  end
		  `br:  begin
		  
                  loc[LEVELS-1:excess]  <=  bitrev(base[LEVELS-1:excess]);
                  dest[LEVELS-1:excess] <=  bitrev(base[LEVELS-1:excess]);
                  src[LEVELS-1:excess]  <=  bitrev(base[LEVELS-1:excess]);
              //loc  <= bitrev((posx<N/2)?(2*(posx)):(2*(posx)-N+1));
//              dest <= bitrev((posx<N/2)?(2*(posx)):(2*(posx)-N+1));
//              src  <= bitrev((posx<N/2)?(2*(posx)):(2*(posx)-N+1));
		      
		  end
		  `nn: begin
		  
		      loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess]+1'b1)&(32 -1 );
              dest[LEVELS-1:excess] <= (base[LEVELS-1:excess]+1'b1)&(32 -1 );
              src[LEVELS-1:excess]  <= (base[LEVELS-1:excess]+1'b1)&(32 -1 );
		  
//		      loc  <= ((posx<N/2)?(2*(posx)):(2*(posx)-N+1)) + 1'b1;
//              dest <= ((posx<N/2)?(2*(posx)):(2*(posx)-N+1)) + 1'b1;
//              src  <= ((posx<N/2)?(2*(posx)):(2*(posx)-N+1)) + 1'b1;
		  end
          `to: begin
               
               loc[LEVELS-1:excess]  <= ((base[LEVELS-1:excess]) + (32/2) - 1) & (32 - 1); 
               dest[LEVELS-1:excess] <= ((base[LEVELS-1:excess]) + (32/2) - 1) & (32 - 1);
               src[LEVELS-1:excess]  <= ((base[LEVELS-1:excess]) + (32/2) - 1) & (32 - 1);
//		      loc  <= (((posx<N/2)?(2*(posx)):(2*(posx)-N+1)) + N/2 - 1) & (N-1);
//              dest <= (((posx<N/2)?(2*(posx)):(2*(posx)-N+1)) + N/2 - 1) & (N-1);
//              src  <= (((posx<N/2)?(2*(posx)):(2*(posx)-N+1)) + N/2 - 1) & (N-1);
		  end
		  
          `cc: begin
          
               loc[LEVELS-1:excess]  <= 32 - 1 - (base[LEVELS-1:excess]); 
               dest[LEVELS-1:excess] <= 32 - 1 - (base[LEVELS-1:excess]);
               src[LEVELS-1:excess]  <= 32 - 1 - (base[LEVELS-1:excess]);
//		      loc  <= N-1 - ((posx<N/2)?(2*(posx)):(2*(posx)-N+1));
//              dest <= N-1 - ((posx<N/2)?(2*(posx)):(2*(posx)-N+1));
//              src  <= N-1 - ((posx<N/2)?(2*(posx)):(2*(posx)-N+1));
		  end
		  
		  default : begin	    
		      loc[LEVELS-1:excess]  <= (base[LEVELS-1:excess]); 
              dest[LEVELS-1:excess] <= (base[LEVELS-1:excess]);
              src[LEVELS-1:excess]  <= (base[LEVELS-1:excess]);
//		      loc  <= (posx<N/2)?(2*(posx)):(2*(posx)-N+1);
//              dest <= (posx<N/2)?(2*(posx)):(2*(posx)-N+1);
//              src  <= (posx<N/2)?(2*(posx)):(2*(posx)-N+1);
		  end
		  
		endcase
	end
	else
	begin
	
		if (i_ack || !i`v)
		begin
		
		 if(i`v == 1 && 1) begin
		     
            $display("Time%0d: Sent packet from PE(%0d) to PE(%0d) chan)(%0d) with packetid=%0d , data=%X, source_addr =%0d, mem_addr =%X, RW=%d",
                            now,                   posx,     i`x,  i[M_A_W + P_W-1:M_A_W+P_W-5] & ('d3),            ((posx)*PACKLIMIT+sent),i[P_W-1:0],source_addr,i`MA,i[D_W-1]);
            $display("Read_request %d Write request %d",read_request,sent);

   
        end
        
            newPacket <= 1'b0;
			if((r <= cmd`rate)) 
			begin
			    
		  		
				i`v <= 1'b0;                
				if(next_v==1) 
				begin
				    i`v			<= 1'b1;
                    newPacket <= 1'b1;
                    i`x         <= mem_addr[32:28] >> 2;
                    i`y         <= Y_MAX-1;
                    i[X_W+Y_W+M_A_W + P_W-1:Y_W+M_A_W + P_W] <= X; // +Y_W+M_A_W + P_W // return x
                    i[Y_W+M_A_W + P_W-1:M_A_W + P_W]         <= Y;// return y
                    i[M_A_W+P_W-1:P_W]<= mem_addr;                // memory address
                    i[D_W-1] <= RW;                               // read write bit
//                    c_o[D_W-2] <= MemPacket;
                    firstPacket <= 0;
                    /*PAYLOAD*/

                       if(read_response == packet_count )
                           read_response <= 0;
                           i[P_W-1:0]	<= mem_addr;//

                     
                      i[P_W-1:0]	<= mem_addr;   
                      if(read_response == packet_count )
                            i[P_W-1:0]	<= error<<63 | now;//

                    if(RW) begin
                       sent <= sent + 1;
					      

					/*Offset generation */
					

                               offset <= offset + 32;

                   
                       case(cmd`channel)
                           `cs:begin
                              dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                              if(loc[LEVELS-1:excess] < 16) begin
                                if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                    dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                    else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(16)-1)
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                              end
                              else begin
                                     if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < 16 )
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                    else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(32)-1)
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                                end
                           end
                           `cb:begin
                               dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                               
                               if(loc[LEVELS-1:excess] < 4) begin
                                  if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                        dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                    else if((loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(4)-1)
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                                end
                                
                                else begin
                                     if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < (loc[LEVELS-1:excess] - (loc[LEVELS-1:excess] % 4)))
                                        dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                    else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(loc[LEVELS-1:excess]+ (3-(loc[LEVELS-1:excess] % 4))))
                                        dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                               
                               end
                           end
                           
                           default: begin
                                dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                    dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 32;
                                else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>32-1)
                                    dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 32;
                           end
                       endcase
                                             
					   end
					   
					 else begin
			            read_request<=read_request+1'b1;

                                        offset <= offset + 32;

                               case(cmd`channel)
                                   `cs:begin
                                          dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                           if(loc[LEVELS-1:excess] < 16) begin
                                             if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                                 dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                                 else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(16)-1)
                                                     dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                                           end
                                           else begin
                                                  if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < 16 )
                                                     dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 16;
                                                 else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(32)-1)
                                                     dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 16;
                                             end                                   
                                         end
                                   `cb:begin
                                      dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                                          
                                          if(loc[LEVELS-1:excess] < 4) begin
                                             if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                                   dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                               else if((loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(4)-1)
                                                   dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                                           end
                                           
                                           else begin
                                                if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < (loc[LEVELS-1:excess] - (loc[LEVELS-1:excess] % 4)))
                                                   dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 4;
                                               else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(loc[LEVELS-1:excess]+ (3-(loc[LEVELS-1:excess] % 4))))
                                                   dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 4;
                                          
                                           end
                                     end
                               
                                   default: begin
                                     dest[LEVELS-1:excess] <= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                   if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                       dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + 32;
                                   else if((loc[LEVELS-1:excess]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>32-1)
                                       dest[LEVELS-1:excess]<= loc[LEVELS-1:excess] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - 32;
                                    end
                               endcase
                                               
					   end
					    if(restart == 1 ) begin
					       r_dest_lfsr <= posx+2;
					    end
					    else begin   
					    
                            r_dest_lfsr[0] <= r_dest_lfsr[15] ^ r_dest_lfsr[13] ^ r_dest_lfsr[12] ^ r_dest_lfsr[10];
                            for(iter=0;iter<15;iter=iter+1)
                                  r_dest_lfsr[iter+1]<=r_dest_lfsr[iter];
                        end        
                          

                         restart <= 0;
                        if(sent == packet_count-2 ) 
                             restart <= 1;
                        else
                             restart <= 0;
                   
                        if(sent == packet_count-1) begin
                             offset <= 0;
                             dest <= loc;
//					                  restart <= 1;
                             if (cmd`access_type) begin
                                offset <= (posx+1)<<5;     
                               end
                        end
                            


				end
			end
			else
			begin
				i`v	<= 1'b0;
			end
		end
		else
		begin
//			c_o_v	<= 1'b0;
		end
	
		if ((attempts<PACKLIMIT & {r} < RATE) )
		begin
			attempts	<= attempts + 1;
		end
		
		if(o_v==1'b1) 
		begin
		      if(1)
			$display("Time%0d: Received packet at PE(%0d) with address=%0x packetid=%0x ",now-1,posx,o[64:33],o[31:0]);

                       read_response <= read_response + 1'b1;
                        $display("%x",read_response);
                        if(o[31:0] != o[64:33] && 1) 
                            error <= 1'b1;
		
			
		end
	end
end

always @(*) // gets executed whenever sent,offset,dest 
begin
next_v <= 0;
next <= 0;    
source_addr <= posx;
mem_addr<= 0;
 RW <= 1;

                if(now >= 0 && (1) && sent <packet_count)
                    begin

                            mem_addr <= {dest,offset};
                            source_addr <= posx;
                            RW <= 1;
                            if(sent <packet_count) begin         
                                next_v <=1;
                            end
                            else begin
                            next_v <=0;
                            end                   
                        end      
                else if(now >= 0  && (1) && sent==packet_count && read_request <packet_count && pending < MAX_PENDING )
                    begin
                            mem_addr <= {dest,offset};
                            source_addr <= posx;
                            RW <= 0;
                        
                            if(read_request <packet_count) begin  
                                next_v <=1;
                            end
                            
                            else begin
                            next_v <=0;
                            end                   
                        end
                 else if( now >= 0 && read_response == packet_count ) begin
                                
                                 next     <= {1'b1,{A_W-1{1'b0}}};
                                 mem_addr <= {loc,{MAW-LEVELS{1'b0}}};
                             source_addr <= posx;            
                             RW <= 1;
                             next_v <=1;
                   end         
                        
                      
	

end


	always @(posedge clk) begin
	    if(rst) begin
	       now<=0;
	    end
		else begin
              now     <= now + 1;
		end
	end
end
endgenerate
endmodule
