`include "phalanx.h"

module Fanout #(
	parameter NUM_CHANNELS = 1, // choose how many Hoplites to try
	parameter X_W	= 1,	// X address width
	parameter Y_W	= 1,	// Y address width
	parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width
	parameter X_MAX	= 1<<X_W,
	parameter Y_MAX	= 1<<Y_W,
	parameter X	= 3,	// X address of this node
	parameter Y	= 2		// Y address of this node
) (
	input wire clk,
	input wire rst,
	input  wire `Msg i,	// input message from client
	output wire i_ack,	// acknowledgement to client
	output wire `MsgChan ii,	// input message to chosen hoplite
	input wire `Chan ii_ack, // input accepted by chosen hoplite
	input wire done_all
);

	reg [$clog2(NUM_CHANNELS):0] choice=0;
	reg [5:0] dest;
	always @(*) begin
//		if(rst) begin
//			choice <= 0;
//		end else begin
//			if(|ii_ack) begin
//				// switch priority to next port after sending
//				// packet
//				choice <= (choice+1)%NUM_CHANNELS;
//			end
            choice = 0;
            if(i`v)
                choice = i[M_A_W + P_W-1:M_A_W+P_W-5] & ('d3);
//		end
	end

	genvar j;
	generate for (j=0; j< NUM_CHANNELS; j=j+1) begin: js
		assign ii[`Msg_W*(j+1)-1:`Msg_W*j] = (choice==j)? i: 0;
	end endgenerate

	assign i_ack = ii_ack[choice]; 



endmodule

