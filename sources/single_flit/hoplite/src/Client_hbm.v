///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// client.v -- Phalanx testbench client
//
// hard tabs=4

`include "phalanx.h"
`include "phalanx_tb.h"

///////////////////////////////////////////////////////////////////////////////
// Testbench client -- inject various test traffic patterns into the NOC
//
module Client_hbm #(
	parameter PAT		= `RANDOM,
	parameter RATE		= 5,
	parameter INTERVAL	= 16,
	parameter PACKLIMIT     = 16,
	parameter SIGMA		= 3,
	parameter P_W       = 256,
	parameter M_A_W     = 33,
    parameter X_W		= 2,
	parameter Y_W		= 2,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width

	parameter CORES		= 4,
	parameter X_MAX		= 1<<X_W,
	parameter Y_MAX		= 1<<Y_W,
	parameter X			= 0,	// X address of this node
	parameter Y			= 0,
	parameter CHAN      = 0,		// Y address of this node
    parameter ADDR_WIDTH = 32,
	parameter DATA_WIDTH = 256
) (
	input  wire clk,		// clock
//	input  wire rst,		// reset
	input  wire ce,			// clock enable
	input  wire `Cmd cmd,	// client test command
	input  wire i_ack,		// router input accepted this cycle
	input  wire o_v,		// router output message valid
	input  wire `Msg o,		// router output message from router
	output reg  `Msg i,		// router input message, registered, validated by i`v
	output wire done,		// done
	output [63:0] received,
	output wire i_busy,
	output wire                                    awvalid      ,   
    input  wire                                    awready      ,
    output wire [ADDR_WIDTH-1:0]                   awaddr       ,
    output wire                                    wvalid       ,
    input  wire                                    wready       ,
    output wire [DATA_WIDTH-1:0]                   wdata        ,
    input  wire                                    bvalid       ,
    output wire                                    bready       ,
    output wire                                    arvalid      ,
    input  wire                                    arready      ,
    output wire [ADDR_WIDTH-1:0]                   araddr       ,
    input  wire                                    rvalid       ,
    output wire                                    rready       ,
    input  wire [DATA_WIDTH-1:0]                   rdata        ,
    input  wire                                    rlast         
);

reg [15:0] reset = {15{1'b1}};
wire rst = reset[0];
 always @(posedge clk) begin
   reset <= reset >> 1;

 end

//    assign i_busy = 0;
	`com `Msg next;

	integer r;

	function [7:0] bitrev(input [7:0] i);
		bitrev = {i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7]};
	endfunction

	function integer local_rnd(input integer i);
		local_rnd = i + {$random} % SIGMA - SIGMA/2;
	endfunction

// avoiding SystemVerilog for iverilog compatibility
	function integer local_window(input integer r, input integer max);
		local_window = (r < 0) ? 0 : (r >= max) ? (max-1) : r;
	endfunction

	function integer tornado(input integer i, input integer max);
		tornado = (i + max/2-1) % max;
	endfunction
	
	reg done_sig;
	reg [63:0] now=0;
        reg [15:0] x_loc;
        reg [15:0] y_loc;

	reg [63:0] attempts=0;
	reg [63:0] received_r = 0 ;
	assign received = received_r;
	integer waiting=0;
	reg [63:0] sent=0;
	integer wait_r=0;
	
	
	wire busy;
	

	wire [D_W-1:0] writeAddress_o;
    wire           writeAddress_ready;
    
    wire [D_W-1:0] writeData_o;
    wire           writeData_ready;
    
    wire [D_W-1:0] readAddress_o;
    wire           readAddress_ready;
    
    wire [D_W-1:0] readResp_o;
    wire           readResp_ready;
    wire [47:0] readResp_temp;
    
    
	assign awaddr = writeAddress_o[ADDR_WIDTH-1:0];
	assign wdata  = writeData_o[63:0];
    assign araddr = readAddress_o[ADDR_WIDTH-1:0];
    
    assign bready = 1'b1;
    assign rready = i_ack;//wait_r[0];
    assign busy   = writeAddress_ready & writeData_ready & readAddress_ready & readResp_ready; //& readAddress_ready & readResp_ready
    assign i_busy = ~busy;
    wire dummy;
    // Three fifos need 33 bits. So 5 bytes of data. One fifo needs 33+8, so 6 bytes of data
    axis_data_fifo_0 writeAddress (
                               .s_axis_aresetn    (~rst)
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & o[D_W-1] & busy)
                              ,.s_axis_tready   (writeAddress_ready)
                              ,.s_axis_tdata    ({o[119:64],o`MA})
                              
                              ,.m_axis_tvalid   (awvalid)
                              ,.m_axis_tready   (awready)
                              ,.m_axis_tdata    (writeAddress_o)
                            );
                            
   axis_data_fifo_0 writeData (
                               .s_axis_aresetn  (~rst)
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & o[D_W-1] & busy)
                              ,.s_axis_tready   (writeData_ready)
                              ,.s_axis_tdata    ({o[119:64],o[63:0]} )
                          
                              ,.m_axis_tvalid   (wvalid)
                              ,.m_axis_tready   (wready)
                              ,.m_axis_tdata    (writeData_o)
                        );

    
    axis_data_fifo_0 readAddress (
                               .s_axis_aresetn  (~rst)
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & ~o[D_W-1] & busy )
                              ,.s_axis_tready   (readAddress_ready)
                              ,.s_axis_tdata    ({o[119:64],o`MA})
                              
                              ,.m_axis_tvalid   (arvalid)
                              ,.m_axis_tready   (arready)
                              ,.m_axis_tdata    (readAddress_o)
                            );
                                                  
   axis_data_fifo_0 readResponse (
                               .s_axis_aresetn  (~rst)
                              ,.s_axis_aclk     (clk)
                              ,.s_axis_tvalid   (o_v & ~o[D_W-1] & busy)
                              ,.s_axis_tready   (readResp_ready)
                              ,.s_axis_tdata    ({o[119:64],o`XY_A,o`MA})
                          
                              ,.m_axis_tvalid   (dummy)
                              ,.m_axis_tready   (i_ack & rvalid)
                              ,.m_axis_tdata    (readResp_temp)
                        );                        


assign readResp_o = {readResp_temp[X_W+Y_W+ADDR_WIDTH-1:ADDR_WIDTH],readResp_temp[ADDR_WIDTH-1:0],120'b0};
always @(*) begin
    i<=0;
    if(rvalid) begin
        i`v <= rvalid;
        i`x <= readResp_o`X_A;
        i`y <= readResp_o`Y_A;
        i[P_W-1:0] <= 256'b0 | (rdata[P_W-1:0]& 32'HFFFF_FFFF) |( (readResp_o`MA & 32'HFFFF_FFFF)<<33);
    end
end

	always @(posedge clk) begin
//		if (rst) begin
////			i <= 0;
//			now <= 0;
//			done_sig<=0;
//		end else begin
//			now <= now + 1;
			if (o_v  &busy) begin
			    received_r <= received_r + 1'b1;
//				$display("M,%0d,%0x,%0d,%0d,%d",received_r,o`d,X,Y,CHAN);
			end
            

//            wait_r <= wait_r + 1'b1;
//	   end
	end
endmodule
