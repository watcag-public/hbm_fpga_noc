///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// client.v -- Phalanx testbench client
//
// hard tabs=4

`include "phalanx.h"
`include "phalanx_tb.h"

///////////////////////////////////////////////////////////////////////////////
// Testbench client -- inject various test traffic patterns into the NOC
//
module client_hbm_top #(
	parameter PAT		  = `RANDOM,
	parameter RATE		  = 5,
	parameter INTERVAL	  = 16,
	parameter PACKLIMIT   = 16,
	parameter SIGMA		  = 3,
	parameter P_W         = 256,
	parameter M_A_W       = 33,
	parameter D_W		  = 1+P_W+M_A_W+X_W+Y_W,	// data width
	parameter X_W		  = 2,
	parameter Y_W		  = 2,
	parameter CORES		  = 4,
	parameter X_MAX		  = 1<<X_W,
	parameter Y_MAX		  = 1<<Y_W,
	parameter X			  = 0,	// X address of this node
	parameter Y			  = 0,
	parameter CHAN        = 0,		// Y address of this node
	parameter ADDR_WIDTH = 32,
	parameter DATA_WIDTH = 256
) (
	input  wire clk,		// clock
	input  wire rst,		// reset
	input  wire ce,			// clock enable
	input  wire `Cmd cmd,	// client test command
	input  wire i_ack,		// router input accepted this cycle
	input  wire o_v,		// router output message valid
	input  wire `Msg o,		// router output message from router
	output wire  `Msg i,		// router input message, registered, validated by i`v
	output wire done,		// done
	output [63:0] received,
	output wire i_busy,
	
	output wire                                    awvalid      , 
    input  wire                                    awready      , 
    output wire [ADDR_WIDTH-1:0]                   awaddr       , 
    output wire                                    wvalid       , 
    input  wire                                    wready       , 
    output wire [DATA_WIDTH-1:0]                   wdata        , 
    input  wire                                    bvalid       , 
    output wire                                    bready       , 
    output wire                                    arvalid      , 
    input  wire                                    arready      , 
    output wire [ADDR_WIDTH-1:0]                   araddr       , 
    input  wire                                    rvalid       , 
    output wire                                    rready       , 
    input  wire [DATA_WIDTH-1:0]                   rdata        , 
    input  wire                                    rlast          
);                                                                

wire dummy;

wire `Msg o_s;
wire      o_v_s;

 shadow_reg_combi #(
                         .D_W( 3 + X_W + Y_W + D_W     )
                        ,.A_W( 0                       )
                    )
                           bp(
                                 .clk( clk                           ) 
                                ,.rst( rst                           )
                                ,.i_v( o_v                           ) 
                                ,.i_d( o                             ) 
                                ,.i_b(dummy                          )
                                ,.o_v( o_v_s                         )
                                ,.o_d( o_s                           ) 
                                ,.o_b( i_busy                        )
                             );
 Client_hbm #(
                    .PAT       ( PAT        )
                   ,.RATE      ( RATE       )
                   ,.PACKLIMIT ( PACKLIMIT  )
                   ,.P_W       ( P_W        )
                   ,.M_A_W     ( M_A_W      )
                   ,.D_W       ( D_W        )
                   ,.X_W       ( X_W        )
                   ,.Y_W       ( Y_W        )
                   ,.X_MAX     ( X_MAX      )
                   ,.Y_MAX     ( Y_MAX      )
                   ,.X         ( X          )
                   ,.Y         ( Y          )
                   ,.CHAN      ( CHAN       )
                   ,.ADDR_WIDTH( ADDR_WIDTH ) 
                   ,.DATA_WIDTH( DATA_WIDTH )
                  )
                cli(
                     .clk      ( clk       )
//                    ,.rst      ( rst       )
                    ,.ce       ( ce        )
                    ,.cmd      ( cmd       )
                    ,.i_ack    ( i_ack     )
                    ,.o_v      ( o_v_s     )
                    ,.o        ( o_s       ) // message from the router
                    ,.i        ( i         )      // message to the router
                    ,.done     ( done      )
                    ,.received ( received  )
                    ,.i_busy   ( i_busy    )
                    
                    ,.awvalid  ( awvalid   )
                    ,.awready  ( awready   )
                    ,.awaddr   ( awaddr    )
                    ,.wvalid   ( wvalid    )
                    ,.wready   ( wready    )
                    ,.wdata    ( wdata     )
                    ,.bvalid   ( bvalid    )
                    ,.bready   ( bready    )
                    ,.arvalid  ( arvalid   )
                    ,.arready  ( arready   )
                    ,.araddr   ( araddr    )
                    ,.rvalid   ( rvalid    )
                    ,.rready   ( rready    )
                    ,.rdata    ( rdata     )
                    ,.rlast    ( rlast     )
                    
                   );
                 
endmodule
