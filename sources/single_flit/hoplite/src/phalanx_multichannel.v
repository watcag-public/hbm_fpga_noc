///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// phalanx.v -- Phalanx NOC testbench
//
// hard tabs=4

`include "phalanx.h"
`include "phalanx_tb.h"

///////////////////////////////////////////////////////////////////////////////
// Generic Phalanx NOC
//
module Phalanx_multichannel #(
	parameter PAT = `RANDOM,
	parameter NUM_CHANNELS = 4,
	parameter ALLOW_SKIP = 1,
	parameter TIMING_ANALYSIS_MODE = 1,
	parameter RATE = 100,
	parameter RATE_SCALE = 1,
	parameter PACKLIMIT = 16,
	parameter ROUTE = "DOR",
	parameter LOWPOWER	= 0,
	parameter XBAR		= 1,
	parameter DISABLE_FAST	= 0,
	parameter GAP		= 2,
	parameter GAP_SKIP_X	= GAP,
	parameter GAP_SKIP_Y	= GAP,
	parameter S2F		= 1,
	parameter ALLOW_W2SF = 1,  // allow w2sf turn
	parameter ALLOW_W2EF = 1,  // allow w2ef turn
	parameter ALLOW_N2SF = 1,  // allow n2sf turn
	parameter X_W		= 3,
	parameter Y_W		= 3,
	parameter P_W       = 120,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W, // read_write, data, target address, X_W and Y_W information about the sender
	parameter X_MAX		= 1<<X_W,
	parameter Y_MAX		= (1<<Y_W)-3,
	parameter integer C_M00_AXI_ADDR_WIDTH       = 64 ,
    parameter integer C_M00_AXI_DATA_WIDTH       = 256,
    parameter integer C_M01_AXI_ADDR_WIDTH       = 64 ,
    parameter integer C_M01_AXI_DATA_WIDTH       = 256,
    parameter integer C_M02_AXI_ADDR_WIDTH       = 64 ,
    parameter integer C_M02_AXI_DATA_WIDTH       = 256,
    parameter integer C_M03_AXI_ADDR_WIDTH       = 64,
    parameter integer C_M03_AXI_DATA_WIDTH       = 256,
    parameter integer C_M04_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M04_AXI_DATA_WIDTH = 256,
    parameter integer C_M05_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M05_AXI_DATA_WIDTH = 256,
    parameter integer C_M06_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M06_AXI_DATA_WIDTH = 256,
    parameter integer C_M07_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M07_AXI_DATA_WIDTH = 256,
    parameter integer C_M08_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M08_AXI_DATA_WIDTH = 256,
    parameter integer C_M09_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M09_AXI_DATA_WIDTH = 256,
    parameter integer C_M10_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M10_AXI_DATA_WIDTH = 256,
    parameter integer C_M11_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M11_AXI_DATA_WIDTH = 256,
    parameter integer C_M12_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M12_AXI_DATA_WIDTH = 256,
    parameter integer C_M13_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M13_AXI_DATA_WIDTH = 256,
    parameter integer C_M14_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M14_AXI_DATA_WIDTH = 256,
    parameter integer C_M15_AXI_ADDR_WIDTH = 64 ,
    parameter integer C_M15_AXI_DATA_WIDTH = 256,
  parameter integer C_M16_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M16_AXI_DATA_WIDTH       = 256,
  parameter integer C_M17_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M17_AXI_DATA_WIDTH       = 256,
  parameter integer C_M18_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M18_AXI_DATA_WIDTH       = 256,
  parameter integer C_M19_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M19_AXI_DATA_WIDTH       = 256,
  parameter integer C_M20_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M20_AXI_DATA_WIDTH       = 256,
  parameter integer C_M21_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M21_AXI_DATA_WIDTH       = 256,
  parameter integer C_M22_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M22_AXI_DATA_WIDTH       = 256,
  parameter integer C_M23_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M23_AXI_DATA_WIDTH       = 256,
  parameter integer C_M24_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M24_AXI_DATA_WIDTH       = 256,
  parameter integer C_M25_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M25_AXI_DATA_WIDTH       = 256,
  parameter integer C_M26_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M26_AXI_DATA_WIDTH       = 256,
  parameter integer C_M27_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M27_AXI_DATA_WIDTH       = 256,
  parameter integer C_M28_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M28_AXI_DATA_WIDTH       = 256,
  parameter integer C_M29_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M29_AXI_DATA_WIDTH       = 256,
  parameter integer C_M30_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M30_AXI_DATA_WIDTH       = 256,
  parameter integer C_M31_AXI_ADDR_WIDTH       = 64 ,
  parameter integer C_M31_AXI_DATA_WIDTH       = 256,
    parameter N = 32
    
) (
	input  wire clk,
	input  wire rst,
	input  wire ce,
	input  wire `Cmd cmd,
	output wire out_v,
	output wire `Msg out,
	output reg done_all,
	output wire ap_done,
    output wire                                    m00_axi_awvalid      ,
    input  wire                                    m00_axi_awready      ,
    output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m00_axi_awaddr       ,
    output wire [8-1:0]                            m00_axi_awlen        ,
    output wire                                    m00_axi_wvalid       ,
    input  wire                                    m00_axi_wready       ,
    output wire [C_M00_AXI_DATA_WIDTH-1:0]         m00_axi_wdata        ,
    output wire [C_M00_AXI_DATA_WIDTH/8-1:0]       m00_axi_wstrb        ,
    output wire                                    m00_axi_wlast        ,
    input  wire                                    m00_axi_bvalid       ,
    output wire                                    m00_axi_bready       ,
    output wire                                    m00_axi_arvalid      ,
    input  wire                                    m00_axi_arready      ,
    output wire [C_M00_AXI_ADDR_WIDTH-1:0]         m00_axi_araddr       ,
    output wire [8-1:0]                            m00_axi_arlen        ,
    input  wire                                    m00_axi_rvalid       ,
    output wire                                    m00_axi_rready       ,
    input  wire [C_M00_AXI_DATA_WIDTH-1:0]         m00_axi_rdata        ,
    input  wire                                    m00_axi_rlast        ,
    // AXI4 master interface m01_axi

    output wire                                    m01_axi_awvalid      ,
    input  wire                                    m01_axi_awready      ,
    output wire [C_M01_AXI_ADDR_WIDTH-1:0]         m01_axi_awaddr       ,
    output wire [8-1:0]                            m01_axi_awlen        ,
    output wire                                    m01_axi_wvalid       ,
    input  wire                                    m01_axi_wready       ,
    output wire [C_M01_AXI_DATA_WIDTH-1:0]         m01_axi_wdata        ,
    output wire [C_M01_AXI_DATA_WIDTH/8-1:0]       m01_axi_wstrb        ,
    output wire                                    m01_axi_wlast        ,
    input  wire                                    m01_axi_bvalid       ,
    output wire                                    m01_axi_bready       ,
    output wire                                    m01_axi_arvalid      ,
    input  wire                                    m01_axi_arready      ,
    output wire [C_M01_AXI_ADDR_WIDTH-1:0]         m01_axi_araddr       ,
    output wire [8-1:0]                            m01_axi_arlen        ,
    input  wire                                    m01_axi_rvalid       ,
    output wire                                    m01_axi_rready       ,
    input  wire [C_M01_AXI_DATA_WIDTH-1:0]         m01_axi_rdata        ,
    input  wire                                    m01_axi_rlast        ,
    // AXI4 master interface m02_axi
    output wire                                    m02_axi_awvalid      ,
    input  wire                                    m02_axi_awready      ,
    output wire [C_M02_AXI_ADDR_WIDTH-1:0]         m02_axi_awaddr       ,
    output wire [8-1:0]                            m02_axi_awlen        ,
    output wire                                    m02_axi_wvalid       ,
    input  wire                                    m02_axi_wready       ,
    output wire [C_M02_AXI_DATA_WIDTH-1:0]         m02_axi_wdata        ,
    output wire [C_M02_AXI_DATA_WIDTH/8-1:0]       m02_axi_wstrb        ,
    output wire                                    m02_axi_wlast        ,
    input  wire                                    m02_axi_bvalid       ,
    output wire                                    m02_axi_bready       ,
    output wire                                    m02_axi_arvalid      ,
    input  wire                                    m02_axi_arready      ,
    output wire [C_M02_AXI_ADDR_WIDTH-1:0]         m02_axi_araddr       ,
    output wire [8-1:0]                            m02_axi_arlen        ,
    input  wire                                    m02_axi_rvalid       ,
    output wire                                    m02_axi_rready       ,
    input  wire [C_M02_AXI_DATA_WIDTH-1:0]         m02_axi_rdata        ,
    input  wire                                    m02_axi_rlast        ,
    // AXI4 master interface m03_axi
    output wire                                    m03_axi_awvalid      ,
    input  wire                                    m03_axi_awready      ,
    output wire [C_M03_AXI_ADDR_WIDTH-1:0]         m03_axi_awaddr       ,
    output wire [8-1:0]                            m03_axi_awlen        ,
    output wire                                    m03_axi_wvalid       ,
    input  wire                                    m03_axi_wready       ,
    output wire [C_M03_AXI_DATA_WIDTH-1:0]         m03_axi_wdata        ,
    output wire [C_M03_AXI_DATA_WIDTH/8-1:0]       m03_axi_wstrb        ,
    output wire                                    m03_axi_wlast        ,
    input  wire                                    m03_axi_bvalid       ,
    output wire                                    m03_axi_bready       ,
    output wire                                    m03_axi_arvalid      ,
    input  wire                                    m03_axi_arready      ,
    output wire [C_M03_AXI_ADDR_WIDTH-1:0]         m03_axi_araddr       ,
    output wire [8-1:0]                            m03_axi_arlen        ,
    input  wire                                    m03_axi_rvalid       ,
    output wire                                    m03_axi_rready       ,
    input  wire [C_M03_AXI_DATA_WIDTH-1:0]         m03_axi_rdata        ,
    input  wire                                    m03_axi_rlast        ,
    output wire                              m04_axi_awvalid,
  input  wire                              m04_axi_awready,
  output wire [C_M04_AXI_ADDR_WIDTH-1:0]   m04_axi_awaddr ,
  output wire [8-1:0]                      m04_axi_awlen  ,
  output wire                              m04_axi_wvalid ,
  input  wire                              m04_axi_wready ,
  output wire [C_M04_AXI_DATA_WIDTH-1:0]   m04_axi_wdata  ,
  output wire [C_M04_AXI_DATA_WIDTH/8-1:0] m04_axi_wstrb  ,
  output wire                              m04_axi_wlast  ,
  input  wire                              m04_axi_bvalid ,
  output wire                              m04_axi_bready ,
  output wire                              m04_axi_arvalid,
  input  wire                              m04_axi_arready,
  output wire [C_M04_AXI_ADDR_WIDTH-1:0]   m04_axi_araddr ,
  output wire [8-1:0]                      m04_axi_arlen  ,
  input  wire                              m04_axi_rvalid ,
  output wire                              m04_axi_rready ,
  input  wire [C_M04_AXI_DATA_WIDTH-1:0]   m04_axi_rdata  ,
  input  wire                              m04_axi_rlast  ,
  // AXI4 master interface m05_axi
  output wire                              m05_axi_awvalid,
  input  wire                              m05_axi_awready,
  output wire [C_M05_AXI_ADDR_WIDTH-1:0]   m05_axi_awaddr ,
  output wire [8-1:0]                      m05_axi_awlen  ,
  output wire                              m05_axi_wvalid ,
  input  wire                              m05_axi_wready ,
  output wire [C_M05_AXI_DATA_WIDTH-1:0]   m05_axi_wdata  ,
  output wire [C_M05_AXI_DATA_WIDTH/8-1:0] m05_axi_wstrb  ,
  output wire                              m05_axi_wlast  ,
  input  wire                              m05_axi_bvalid ,
  output wire                              m05_axi_bready ,
  output wire                              m05_axi_arvalid,
  input  wire                              m05_axi_arready,
  output wire [C_M05_AXI_ADDR_WIDTH-1:0]   m05_axi_araddr ,
  output wire [8-1:0]                      m05_axi_arlen  ,
  input  wire                              m05_axi_rvalid ,
  output wire                              m05_axi_rready ,
  input  wire [C_M05_AXI_DATA_WIDTH-1:0]   m05_axi_rdata  ,
  input  wire                              m05_axi_rlast  ,
  // AXI4 master interface m06_axi
  output wire                              m06_axi_awvalid,
  input  wire                              m06_axi_awready,
  output wire [C_M06_AXI_ADDR_WIDTH-1:0]   m06_axi_awaddr ,
  output wire [8-1:0]                      m06_axi_awlen  ,
  output wire                              m06_axi_wvalid ,
  input  wire                              m06_axi_wready ,
  output wire [C_M06_AXI_DATA_WIDTH-1:0]   m06_axi_wdata  ,
  output wire [C_M06_AXI_DATA_WIDTH/8-1:0] m06_axi_wstrb  ,
  output wire                              m06_axi_wlast  ,
  input  wire                              m06_axi_bvalid ,
  output wire                              m06_axi_bready ,
  output wire                              m06_axi_arvalid,
  input  wire                              m06_axi_arready,
  output wire [C_M06_AXI_ADDR_WIDTH-1:0]   m06_axi_araddr ,
  output wire [8-1:0]                      m06_axi_arlen  ,
  input  wire                              m06_axi_rvalid ,
  output wire                              m06_axi_rready ,
  input  wire [C_M06_AXI_DATA_WIDTH-1:0]   m06_axi_rdata  ,
  input  wire                              m06_axi_rlast  ,
  // AXI4 master interface m07_axi
  output wire                              m07_axi_awvalid,
  input  wire                              m07_axi_awready,
  output wire [C_M07_AXI_ADDR_WIDTH-1:0]   m07_axi_awaddr ,
  output wire [8-1:0]                      m07_axi_awlen  ,
  output wire                              m07_axi_wvalid ,
  input  wire                              m07_axi_wready ,
  output wire [C_M07_AXI_DATA_WIDTH-1:0]   m07_axi_wdata  ,
  output wire [C_M07_AXI_DATA_WIDTH/8-1:0] m07_axi_wstrb  ,
  output wire                              m07_axi_wlast  ,
  input  wire                              m07_axi_bvalid ,
  output wire                              m07_axi_bready ,
  output wire                              m07_axi_arvalid,
  input  wire                              m07_axi_arready,
  output wire [C_M07_AXI_ADDR_WIDTH-1:0]   m07_axi_araddr ,
  output wire [8-1:0]                      m07_axi_arlen  ,
  input  wire                              m07_axi_rvalid ,
  output wire                              m07_axi_rready ,
  input  wire [C_M07_AXI_DATA_WIDTH-1:0]   m07_axi_rdata  ,
  input  wire                              m07_axi_rlast  ,
  // AXI4 master interface m08_axi
  output wire                              m08_axi_awvalid,
  input  wire                              m08_axi_awready,
  output wire [C_M08_AXI_ADDR_WIDTH-1:0]   m08_axi_awaddr ,
  output wire [8-1:0]                      m08_axi_awlen  ,
  output wire                              m08_axi_wvalid ,
  input  wire                              m08_axi_wready ,
  output wire [C_M08_AXI_DATA_WIDTH-1:0]   m08_axi_wdata  ,
  output wire [C_M08_AXI_DATA_WIDTH/8-1:0] m08_axi_wstrb  ,
  output wire                              m08_axi_wlast  ,
  input  wire                              m08_axi_bvalid ,
  output wire                              m08_axi_bready ,
  output wire                              m08_axi_arvalid,
  input  wire                              m08_axi_arready,
  output wire [C_M08_AXI_ADDR_WIDTH-1:0]   m08_axi_araddr ,
  output wire [8-1:0]                      m08_axi_arlen  ,
  input  wire                              m08_axi_rvalid ,
  output wire                              m08_axi_rready ,
  input  wire [C_M08_AXI_DATA_WIDTH-1:0]   m08_axi_rdata  ,
  input  wire                              m08_axi_rlast  ,
  // AXI4 master interface m09_axi
  output wire                              m09_axi_awvalid,
  input  wire                              m09_axi_awready,
  output wire [C_M09_AXI_ADDR_WIDTH-1:0]   m09_axi_awaddr ,
  output wire [8-1:0]                      m09_axi_awlen  ,
  output wire                              m09_axi_wvalid ,
  input  wire                              m09_axi_wready ,
  output wire [C_M09_AXI_DATA_WIDTH-1:0]   m09_axi_wdata  ,
  output wire [C_M09_AXI_DATA_WIDTH/8-1:0] m09_axi_wstrb  ,
  output wire                              m09_axi_wlast  ,
  input  wire                              m09_axi_bvalid ,
  output wire                              m09_axi_bready ,
  output wire                              m09_axi_arvalid,
  input  wire                              m09_axi_arready,
  output wire [C_M09_AXI_ADDR_WIDTH-1:0]   m09_axi_araddr ,
  output wire [8-1:0]                      m09_axi_arlen  ,
  input  wire                              m09_axi_rvalid ,
  output wire                              m09_axi_rready ,
  input  wire [C_M09_AXI_DATA_WIDTH-1:0]   m09_axi_rdata  ,
  input  wire                              m09_axi_rlast  ,
  // AXI4 master interface m10_axi
  output wire                              m10_axi_awvalid,
  input  wire                              m10_axi_awready,
  output wire [C_M10_AXI_ADDR_WIDTH-1:0]   m10_axi_awaddr ,
  output wire [8-1:0]                      m10_axi_awlen  ,
  output wire                              m10_axi_wvalid ,
  input  wire                              m10_axi_wready ,
  output wire [C_M10_AXI_DATA_WIDTH-1:0]   m10_axi_wdata  ,
  output wire [C_M10_AXI_DATA_WIDTH/8-1:0] m10_axi_wstrb  ,
  output wire                              m10_axi_wlast  ,
  input  wire                              m10_axi_bvalid ,
  output wire                              m10_axi_bready ,
  output wire                              m10_axi_arvalid,
  input  wire                              m10_axi_arready,
  output wire [C_M10_AXI_ADDR_WIDTH-1:0]   m10_axi_araddr ,
  output wire [8-1:0]                      m10_axi_arlen  ,
  input  wire                              m10_axi_rvalid ,
  output wire                              m10_axi_rready ,
  input  wire [C_M10_AXI_DATA_WIDTH-1:0]   m10_axi_rdata  ,
  input  wire                              m10_axi_rlast  ,
  // AXI4 master interface m11_axi
  output wire                              m11_axi_awvalid,
  input  wire                              m11_axi_awready,
  output wire [C_M11_AXI_ADDR_WIDTH-1:0]   m11_axi_awaddr ,
  output wire [8-1:0]                      m11_axi_awlen  ,
  output wire                              m11_axi_wvalid ,
  input  wire                              m11_axi_wready ,
  output wire [C_M11_AXI_DATA_WIDTH-1:0]   m11_axi_wdata  ,
  output wire [C_M11_AXI_DATA_WIDTH/8-1:0] m11_axi_wstrb  ,
  output wire                              m11_axi_wlast  ,
  input  wire                              m11_axi_bvalid ,
  output wire                              m11_axi_bready ,
  output wire                              m11_axi_arvalid,
  input  wire                              m11_axi_arready,
  output wire [C_M11_AXI_ADDR_WIDTH-1:0]   m11_axi_araddr ,
  output wire [8-1:0]                      m11_axi_arlen  ,
  input  wire                              m11_axi_rvalid ,
  output wire                              m11_axi_rready ,
  input  wire [C_M11_AXI_DATA_WIDTH-1:0]   m11_axi_rdata  ,
  input  wire                              m11_axi_rlast  ,
  // AXI4 master interface m12_axi
  output wire                              m12_axi_awvalid,
  input  wire                              m12_axi_awready,
  output wire [C_M12_AXI_ADDR_WIDTH-1:0]   m12_axi_awaddr ,
  output wire [8-1:0]                      m12_axi_awlen  ,
  output wire                              m12_axi_wvalid ,
  input  wire                              m12_axi_wready ,
  output wire [C_M12_AXI_DATA_WIDTH-1:0]   m12_axi_wdata  ,
  output wire [C_M12_AXI_DATA_WIDTH/8-1:0] m12_axi_wstrb  ,
  output wire                              m12_axi_wlast  ,
  input  wire                              m12_axi_bvalid ,
  output wire                              m12_axi_bready ,
  output wire                              m12_axi_arvalid,
  input  wire                              m12_axi_arready,
  output wire [C_M12_AXI_ADDR_WIDTH-1:0]   m12_axi_araddr ,
  output wire [8-1:0]                      m12_axi_arlen  ,
  input  wire                              m12_axi_rvalid ,
  output wire                              m12_axi_rready ,
  input  wire [C_M12_AXI_DATA_WIDTH-1:0]   m12_axi_rdata  ,
  input  wire                              m12_axi_rlast  ,
  // AXI4 master interface m13_axi
  output wire                              m13_axi_awvalid,
  input  wire                              m13_axi_awready,
  output wire [C_M13_AXI_ADDR_WIDTH-1:0]   m13_axi_awaddr ,
  output wire [8-1:0]                      m13_axi_awlen  ,
  output wire                              m13_axi_wvalid ,
  input  wire                              m13_axi_wready ,
  output wire [C_M13_AXI_DATA_WIDTH-1:0]   m13_axi_wdata  ,
  output wire [C_M13_AXI_DATA_WIDTH/8-1:0] m13_axi_wstrb  ,
  output wire                              m13_axi_wlast  ,
  input  wire                              m13_axi_bvalid ,
  output wire                              m13_axi_bready ,
  output wire                              m13_axi_arvalid,
  input  wire                              m13_axi_arready,
  output wire [C_M13_AXI_ADDR_WIDTH-1:0]   m13_axi_araddr ,
  output wire [8-1:0]                      m13_axi_arlen  ,
  input  wire                              m13_axi_rvalid ,
  output wire                              m13_axi_rready ,
  input  wire [C_M13_AXI_DATA_WIDTH-1:0]   m13_axi_rdata  ,
  input  wire                              m13_axi_rlast  ,
  // AXI4 master interface m14_axi
  output wire                              m14_axi_awvalid,
  input  wire                              m14_axi_awready,
  output wire [C_M14_AXI_ADDR_WIDTH-1:0]   m14_axi_awaddr ,
  output wire [8-1:0]                      m14_axi_awlen  ,
  output wire                              m14_axi_wvalid ,
  input  wire                              m14_axi_wready ,
  output wire [C_M14_AXI_DATA_WIDTH-1:0]   m14_axi_wdata  ,
  output wire [C_M14_AXI_DATA_WIDTH/8-1:0] m14_axi_wstrb  ,
  output wire                              m14_axi_wlast  ,
  input  wire                              m14_axi_bvalid ,
  output wire                              m14_axi_bready ,
  output wire                              m14_axi_arvalid,
  input  wire                              m14_axi_arready,
  output wire [C_M14_AXI_ADDR_WIDTH-1:0]   m14_axi_araddr ,
  output wire [8-1:0]                      m14_axi_arlen  ,
  input  wire                              m14_axi_rvalid ,
  output wire                              m14_axi_rready ,
  input  wire [C_M14_AXI_DATA_WIDTH-1:0]   m14_axi_rdata  ,
  input  wire                              m14_axi_rlast  ,
  // AXI4 master interface m15_axi
  output wire                              m15_axi_awvalid,
  input  wire                              m15_axi_awready,
  output wire [C_M15_AXI_ADDR_WIDTH-1:0]   m15_axi_awaddr ,
  output wire [8-1:0]                      m15_axi_awlen  ,
  output wire                              m15_axi_wvalid ,
  input  wire                              m15_axi_wready ,
  output wire [C_M15_AXI_DATA_WIDTH-1:0]   m15_axi_wdata  ,
  output wire [C_M15_AXI_DATA_WIDTH/8-1:0] m15_axi_wstrb  ,
  output wire                              m15_axi_wlast  ,
  input  wire                              m15_axi_bvalid ,
  output wire                              m15_axi_bready ,
  output wire                              m15_axi_arvalid,
  input  wire                              m15_axi_arready,
  output wire [C_M15_AXI_ADDR_WIDTH-1:0]   m15_axi_araddr ,
  output wire [8-1:0]                      m15_axi_arlen  ,
  input  wire                              m15_axi_rvalid ,
  output wire                              m15_axi_rready ,
  input  wire [C_M15_AXI_DATA_WIDTH-1:0]   m15_axi_rdata  ,
  input  wire                              m15_axi_rlast  ,
   output wire                                    m16_axi_awvalid      ,
  input  wire                                    m16_axi_awready      ,
  output wire [C_M16_AXI_ADDR_WIDTH-1:0]         m16_axi_awaddr       ,
  output wire [8-1:0]                            m16_axi_awlen        ,
  output wire                                    m16_axi_wvalid       ,
  input  wire                                    m16_axi_wready       ,
  output wire [C_M16_AXI_DATA_WIDTH-1:0]         m16_axi_wdata        ,
  output wire [C_M16_AXI_DATA_WIDTH/8-1:0]       m16_axi_wstrb        ,
  output wire                                    m16_axi_wlast        ,
  input  wire                                    m16_axi_bvalid       ,
  output wire                                    m16_axi_bready       ,
  output wire                                    m16_axi_arvalid      ,
  input  wire                                    m16_axi_arready      ,
  output wire [C_M16_AXI_ADDR_WIDTH-1:0]         m16_axi_araddr       ,
  output wire [8-1:0]                            m16_axi_arlen        ,
  input  wire                                    m16_axi_rvalid       ,
  output wire                                    m16_axi_rready       ,
  input  wire [C_M16_AXI_DATA_WIDTH-1:0]         m16_axi_rdata        ,
  input  wire                                    m16_axi_rlast        ,
  // AXI4 master interface m17_axi
  output wire                                    m17_axi_awvalid      ,
  input  wire                                    m17_axi_awready      ,
  output wire [C_M17_AXI_ADDR_WIDTH-1:0]         m17_axi_awaddr       ,
  output wire [8-1:0]                            m17_axi_awlen        ,
  output wire                                    m17_axi_wvalid       ,
  input  wire                                    m17_axi_wready       ,
  output wire [C_M17_AXI_DATA_WIDTH-1:0]         m17_axi_wdata        ,
  output wire [C_M17_AXI_DATA_WIDTH/8-1:0]       m17_axi_wstrb        ,
  output wire                                    m17_axi_wlast        ,
  input  wire                                    m17_axi_bvalid       ,
  output wire                                    m17_axi_bready       ,
  output wire                                    m17_axi_arvalid      ,
  input  wire                                    m17_axi_arready      ,
  output wire [C_M17_AXI_ADDR_WIDTH-1:0]         m17_axi_araddr       ,
  output wire [8-1:0]                            m17_axi_arlen        ,
  input  wire                                    m17_axi_rvalid       ,
  output wire                                    m17_axi_rready       ,
  input  wire [C_M17_AXI_DATA_WIDTH-1:0]         m17_axi_rdata        ,
  input  wire                                    m17_axi_rlast        ,
  // AXI4 master interface m18_axi
  output wire                                    m18_axi_awvalid      ,
  input  wire                                    m18_axi_awready      ,
  output wire [C_M18_AXI_ADDR_WIDTH-1:0]         m18_axi_awaddr       ,
  output wire [8-1:0]                            m18_axi_awlen        ,
  output wire                                    m18_axi_wvalid       ,
  input  wire                                    m18_axi_wready       ,
  output wire [C_M18_AXI_DATA_WIDTH-1:0]         m18_axi_wdata        ,
  output wire [C_M18_AXI_DATA_WIDTH/8-1:0]       m18_axi_wstrb        ,
  output wire                                    m18_axi_wlast        ,
  input  wire                                    m18_axi_bvalid       ,
  output wire                                    m18_axi_bready       ,
  output wire                                    m18_axi_arvalid      ,
  input  wire                                    m18_axi_arready      ,
  output wire [C_M18_AXI_ADDR_WIDTH-1:0]         m18_axi_araddr       ,
  output wire [8-1:0]                            m18_axi_arlen        ,
  input  wire                                    m18_axi_rvalid       ,
  output wire                                    m18_axi_rready       ,
  input  wire [C_M18_AXI_DATA_WIDTH-1:0]         m18_axi_rdata        ,
  input  wire                                    m18_axi_rlast        ,
  // AXI4 master interface m19_axi
  output wire                                    m19_axi_awvalid      ,
  input  wire                                    m19_axi_awready      ,
  output wire [C_M19_AXI_ADDR_WIDTH-1:0]         m19_axi_awaddr       ,
  output wire [8-1:0]                            m19_axi_awlen        ,
  output wire                                    m19_axi_wvalid       ,
  input  wire                                    m19_axi_wready       ,
  output wire [C_M19_AXI_DATA_WIDTH-1:0]         m19_axi_wdata        ,
  output wire [C_M19_AXI_DATA_WIDTH/8-1:0]       m19_axi_wstrb        ,
  output wire                                    m19_axi_wlast        ,
  input  wire                                    m19_axi_bvalid       ,
  output wire                                    m19_axi_bready       ,
  output wire                                    m19_axi_arvalid      ,
  input  wire                                    m19_axi_arready      ,
  output wire [C_M19_AXI_ADDR_WIDTH-1:0]         m19_axi_araddr       ,
  output wire [8-1:0]                            m19_axi_arlen        ,
  input  wire                                    m19_axi_rvalid       ,
  output wire                                    m19_axi_rready       ,
  input  wire [C_M19_AXI_DATA_WIDTH-1:0]         m19_axi_rdata        ,
  input  wire                                    m19_axi_rlast        ,
  // AXI4 master interface m20_axi
  output wire                                    m20_axi_awvalid      ,
  input  wire                                    m20_axi_awready      ,
  output wire [C_M20_AXI_ADDR_WIDTH-1:0]         m20_axi_awaddr       ,
  output wire [8-1:0]                            m20_axi_awlen        ,
  output wire                                    m20_axi_wvalid       ,
  input  wire                                    m20_axi_wready       ,
  output wire [C_M20_AXI_DATA_WIDTH-1:0]         m20_axi_wdata        ,
  output wire [C_M20_AXI_DATA_WIDTH/8-1:0]       m20_axi_wstrb        ,
  output wire                                    m20_axi_wlast        ,
  input  wire                                    m20_axi_bvalid       ,
  output wire                                    m20_axi_bready       ,
  output wire                                    m20_axi_arvalid      ,
  input  wire                                    m20_axi_arready      ,
  output wire [C_M20_AXI_ADDR_WIDTH-1:0]         m20_axi_araddr       ,
  output wire [8-1:0]                            m20_axi_arlen        ,
  input  wire                                    m20_axi_rvalid       ,
  output wire                                    m20_axi_rready       ,
  input  wire [C_M20_AXI_DATA_WIDTH-1:0]         m20_axi_rdata        ,
  input  wire                                    m20_axi_rlast        ,
  // AXI4 master interface m21_axi
  output wire                                    m21_axi_awvalid      ,
  input  wire                                    m21_axi_awready      ,
  output wire [C_M21_AXI_ADDR_WIDTH-1:0]         m21_axi_awaddr       ,
  output wire [8-1:0]                            m21_axi_awlen        ,
  output wire                                    m21_axi_wvalid       ,
  input  wire                                    m21_axi_wready       ,
  output wire [C_M21_AXI_DATA_WIDTH-1:0]         m21_axi_wdata        ,
  output wire [C_M21_AXI_DATA_WIDTH/8-1:0]       m21_axi_wstrb        ,
  output wire                                    m21_axi_wlast        ,
  input  wire                                    m21_axi_bvalid       ,
  output wire                                    m21_axi_bready       ,
  output wire                                    m21_axi_arvalid      ,
  input  wire                                    m21_axi_arready      ,
  output wire [C_M21_AXI_ADDR_WIDTH-1:0]         m21_axi_araddr       ,
  output wire [8-1:0]                            m21_axi_arlen        ,
  input  wire                                    m21_axi_rvalid       ,
  output wire                                    m21_axi_rready       ,
  input  wire [C_M21_AXI_DATA_WIDTH-1:0]         m21_axi_rdata        ,
  input  wire                                    m21_axi_rlast        ,
  // AXI4 master interface m22_axi
  output wire                                    m22_axi_awvalid      ,
  input  wire                                    m22_axi_awready      ,
  output wire [C_M22_AXI_ADDR_WIDTH-1:0]         m22_axi_awaddr       ,
  output wire [8-1:0]                            m22_axi_awlen        ,
  output wire                                    m22_axi_wvalid       ,
  input  wire                                    m22_axi_wready       ,
  output wire [C_M22_AXI_DATA_WIDTH-1:0]         m22_axi_wdata        ,
  output wire [C_M22_AXI_DATA_WIDTH/8-1:0]       m22_axi_wstrb        ,
  output wire                                    m22_axi_wlast        ,
  input  wire                                    m22_axi_bvalid       ,
  output wire                                    m22_axi_bready       ,
  output wire                                    m22_axi_arvalid      ,
  input  wire                                    m22_axi_arready      ,
  output wire [C_M22_AXI_ADDR_WIDTH-1:0]         m22_axi_araddr       ,
  output wire [8-1:0]                            m22_axi_arlen        ,
  input  wire                                    m22_axi_rvalid       ,
  output wire                                    m22_axi_rready       ,
  input  wire [C_M22_AXI_DATA_WIDTH-1:0]         m22_axi_rdata        ,
  input  wire                                    m22_axi_rlast        ,
  // AXI4 master interface m23_axi
  output wire                                    m23_axi_awvalid      ,
  input  wire                                    m23_axi_awready      ,
  output wire [C_M23_AXI_ADDR_WIDTH-1:0]         m23_axi_awaddr       ,
  output wire [8-1:0]                            m23_axi_awlen        ,
  output wire                                    m23_axi_wvalid       ,
  input  wire                                    m23_axi_wready       ,
  output wire [C_M23_AXI_DATA_WIDTH-1:0]         m23_axi_wdata        ,
  output wire [C_M23_AXI_DATA_WIDTH/8-1:0]       m23_axi_wstrb        ,
  output wire                                    m23_axi_wlast        ,
  input  wire                                    m23_axi_bvalid       ,
  output wire                                    m23_axi_bready       ,
  output wire                                    m23_axi_arvalid      ,
  input  wire                                    m23_axi_arready      ,
  output wire [C_M23_AXI_ADDR_WIDTH-1:0]         m23_axi_araddr       ,
  output wire [8-1:0]                            m23_axi_arlen        ,
  input  wire                                    m23_axi_rvalid       ,
  output wire                                    m23_axi_rready       ,
  input  wire [C_M23_AXI_DATA_WIDTH-1:0]         m23_axi_rdata        ,
  input  wire                                    m23_axi_rlast        ,
  // AXI4 master interface m24_axi
  output wire                                    m24_axi_awvalid      ,
  input  wire                                    m24_axi_awready      ,
  output wire [C_M24_AXI_ADDR_WIDTH-1:0]         m24_axi_awaddr       ,
  output wire [8-1:0]                            m24_axi_awlen        ,
  output wire                                    m24_axi_wvalid       ,
  input  wire                                    m24_axi_wready       ,
  output wire [C_M24_AXI_DATA_WIDTH-1:0]         m24_axi_wdata        ,
  output wire [C_M24_AXI_DATA_WIDTH/8-1:0]       m24_axi_wstrb        ,
  output wire                                    m24_axi_wlast        ,
  input  wire                                    m24_axi_bvalid       ,
  output wire                                    m24_axi_bready       ,
  output wire                                    m24_axi_arvalid      ,
  input  wire                                    m24_axi_arready      ,
  output wire [C_M24_AXI_ADDR_WIDTH-1:0]         m24_axi_araddr       ,
  output wire [8-1:0]                            m24_axi_arlen        ,
  input  wire                                    m24_axi_rvalid       ,
  output wire                                    m24_axi_rready       ,
  input  wire [C_M24_AXI_DATA_WIDTH-1:0]         m24_axi_rdata        ,
  input  wire                                    m24_axi_rlast        ,
  // AXI4 master interface m25_axi
  output wire                                    m25_axi_awvalid      ,
  input  wire                                    m25_axi_awready      ,
  output wire [C_M25_AXI_ADDR_WIDTH-1:0]         m25_axi_awaddr       ,
  output wire [8-1:0]                            m25_axi_awlen        ,
  output wire                                    m25_axi_wvalid       ,
  input  wire                                    m25_axi_wready       ,
  output wire [C_M25_AXI_DATA_WIDTH-1:0]         m25_axi_wdata        ,
  output wire [C_M25_AXI_DATA_WIDTH/8-1:0]       m25_axi_wstrb        ,
  output wire                                    m25_axi_wlast        ,
  input  wire                                    m25_axi_bvalid       ,
  output wire                                    m25_axi_bready       ,
  output wire                                    m25_axi_arvalid      ,
  input  wire                                    m25_axi_arready      ,
  output wire [C_M25_AXI_ADDR_WIDTH-1:0]         m25_axi_araddr       ,
  output wire [8-1:0]                            m25_axi_arlen        ,
  input  wire                                    m25_axi_rvalid       ,
  output wire                                    m25_axi_rready       ,
  input  wire [C_M25_AXI_DATA_WIDTH-1:0]         m25_axi_rdata        ,
  input  wire                                    m25_axi_rlast        ,
  // AXI4 master interface m26_axi
  output wire                                    m26_axi_awvalid      ,
  input  wire                                    m26_axi_awready      ,
  output wire [C_M26_AXI_ADDR_WIDTH-1:0]         m26_axi_awaddr       ,
  output wire [8-1:0]                            m26_axi_awlen        ,
  output wire                                    m26_axi_wvalid       ,
  input  wire                                    m26_axi_wready       ,
  output wire [C_M26_AXI_DATA_WIDTH-1:0]         m26_axi_wdata        ,
  output wire [C_M26_AXI_DATA_WIDTH/8-1:0]       m26_axi_wstrb        ,
  output wire                                    m26_axi_wlast        ,
  input  wire                                    m26_axi_bvalid       ,
  output wire                                    m26_axi_bready       ,
  output wire                                    m26_axi_arvalid      ,
  input  wire                                    m26_axi_arready      ,
  output wire [C_M26_AXI_ADDR_WIDTH-1:0]         m26_axi_araddr       ,
  output wire [8-1:0]                            m26_axi_arlen        ,
  input  wire                                    m26_axi_rvalid       ,
  output wire                                    m26_axi_rready       ,
  input  wire [C_M26_AXI_DATA_WIDTH-1:0]         m26_axi_rdata        ,
  input  wire                                    m26_axi_rlast        ,
  // AXI4 master interface m27_axi
  output wire                                    m27_axi_awvalid      ,
  input  wire                                    m27_axi_awready      ,
  output wire [C_M27_AXI_ADDR_WIDTH-1:0]         m27_axi_awaddr       ,
  output wire [8-1:0]                            m27_axi_awlen        ,
  output wire                                    m27_axi_wvalid       ,
  input  wire                                    m27_axi_wready       ,
  output wire [C_M27_AXI_DATA_WIDTH-1:0]         m27_axi_wdata        ,
  output wire [C_M27_AXI_DATA_WIDTH/8-1:0]       m27_axi_wstrb        ,
  output wire                                    m27_axi_wlast        ,
  input  wire                                    m27_axi_bvalid       ,
  output wire                                    m27_axi_bready       ,
  output wire                                    m27_axi_arvalid      ,
  input  wire                                    m27_axi_arready      ,
  output wire [C_M27_AXI_ADDR_WIDTH-1:0]         m27_axi_araddr       ,
  output wire [8-1:0]                            m27_axi_arlen        ,
  input  wire                                    m27_axi_rvalid       ,
  output wire                                    m27_axi_rready       ,
  input  wire [C_M27_AXI_DATA_WIDTH-1:0]         m27_axi_rdata        ,
  input  wire                                    m27_axi_rlast        ,
  // AXI4 master interface m28_axi
  output wire                                    m28_axi_awvalid      ,
  input  wire                                    m28_axi_awready      ,
  output wire [C_M28_AXI_ADDR_WIDTH-1:0]         m28_axi_awaddr       ,
  output wire [8-1:0]                            m28_axi_awlen        ,
  output wire                                    m28_axi_wvalid       ,
  input  wire                                    m28_axi_wready       ,
  output wire [C_M28_AXI_DATA_WIDTH-1:0]         m28_axi_wdata        ,
  output wire [C_M28_AXI_DATA_WIDTH/8-1:0]       m28_axi_wstrb        ,
  output wire                                    m28_axi_wlast        ,
  input  wire                                    m28_axi_bvalid       ,
  output wire                                    m28_axi_bready       ,
  output wire                                    m28_axi_arvalid      ,
  input  wire                                    m28_axi_arready      ,
  output wire [C_M28_AXI_ADDR_WIDTH-1:0]         m28_axi_araddr       ,
  output wire [8-1:0]                            m28_axi_arlen        ,
  input  wire                                    m28_axi_rvalid       ,
  output wire                                    m28_axi_rready       ,
  input  wire [C_M28_AXI_DATA_WIDTH-1:0]         m28_axi_rdata        ,
  input  wire                                    m28_axi_rlast        ,
  // AXI4 master interface m29_axi
  output wire                                    m29_axi_awvalid      ,
  input  wire                                    m29_axi_awready      ,
  output wire [C_M29_AXI_ADDR_WIDTH-1:0]         m29_axi_awaddr       ,
  output wire [8-1:0]                            m29_axi_awlen        ,
  output wire                                    m29_axi_wvalid       ,
  input  wire                                    m29_axi_wready       ,
  output wire [C_M29_AXI_DATA_WIDTH-1:0]         m29_axi_wdata        ,
  output wire [C_M29_AXI_DATA_WIDTH/8-1:0]       m29_axi_wstrb        ,
  output wire                                    m29_axi_wlast        ,
  input  wire                                    m29_axi_bvalid       ,
  output wire                                    m29_axi_bready       ,
  output wire                                    m29_axi_arvalid      ,
  input  wire                                    m29_axi_arready      ,
  output wire [C_M29_AXI_ADDR_WIDTH-1:0]         m29_axi_araddr       ,
  output wire [8-1:0]                            m29_axi_arlen        ,
  input  wire                                    m29_axi_rvalid       ,
  output wire                                    m29_axi_rready       ,
  input  wire [C_M29_AXI_DATA_WIDTH-1:0]         m29_axi_rdata        ,
  input  wire                                    m29_axi_rlast        ,
  // AXI4 master interface m30_axi
  output wire                                    m30_axi_awvalid      ,
  input  wire                                    m30_axi_awready      ,
  output wire [C_M30_AXI_ADDR_WIDTH-1:0]         m30_axi_awaddr       ,
  output wire [8-1:0]                            m30_axi_awlen        ,
  output wire                                    m30_axi_wvalid       ,
  input  wire                                    m30_axi_wready       ,
  output wire [C_M30_AXI_DATA_WIDTH-1:0]         m30_axi_wdata        ,
  output wire [C_M30_AXI_DATA_WIDTH/8-1:0]       m30_axi_wstrb        ,
  output wire                                    m30_axi_wlast        ,
  input  wire                                    m30_axi_bvalid       ,
  output wire                                    m30_axi_bready       ,
  output wire                                    m30_axi_arvalid      ,
  input  wire                                    m30_axi_arready      ,
  output wire [C_M30_AXI_ADDR_WIDTH-1:0]         m30_axi_araddr       ,
  output wire [8-1:0]                            m30_axi_arlen        ,
  input  wire                                    m30_axi_rvalid       ,
  output wire                                    m30_axi_rready       ,
  input  wire [C_M30_AXI_DATA_WIDTH-1:0]         m30_axi_rdata        ,
  input  wire                                    m30_axi_rlast        ,
  // AXI4 master interface m31_axi
  output wire                                    m31_axi_awvalid      ,
  input  wire                                    m31_axi_awready      ,
  output wire [C_M31_AXI_ADDR_WIDTH-1:0]         m31_axi_awaddr       ,
  output wire [8-1:0]                            m31_axi_awlen        ,
  output wire                                    m31_axi_wvalid       ,
  input  wire                                    m31_axi_wready       ,
  output wire [C_M31_AXI_DATA_WIDTH-1:0]         m31_axi_wdata        ,
  output wire [C_M31_AXI_DATA_WIDTH/8-1:0]       m31_axi_wstrb        ,
  output wire                                    m31_axi_wlast        ,
  input  wire                                    m31_axi_bvalid       ,
  output wire                                    m31_axi_bready       ,
  output wire                                    m31_axi_arvalid      ,
  input  wire                                    m31_axi_arready      ,
  output wire [C_M31_AXI_ADDR_WIDTH-1:0]         m31_axi_araddr       ,
  output wire [8-1:0]                            m31_axi_arlen        ,
  input  wire                                    m31_axi_rvalid       ,
  output wire                                    m31_axi_rready       ,
  input  wire [C_M31_AXI_DATA_WIDTH-1:0]         m31_axi_rdata        ,
  input  wire                                    m31_axi_rlast        
   );

	wire `Msg i `XY;			// eastbound channels
	wire `MsgChan ii `XY;	// eastbound channels
	wire `MsgChan e `XY;			// eastbound channels
	wire `Msg o `XY;			// southbound/output channels
	wire `MsgChan oo `XY;	// southbound/output channels
	wire `MsgChan e_f `XY;			// eastbound fsat channels
	wire `MsgChan o_f `XY;			// southbound/output fast channels
	wire `XY done;				// done array
	wire `XYC done_h;			// done array inside hoplite
	wire `XY i_ack;				// input messages accepted this cycle
	wire `Chan ii_ack `XY;	// input messages accepted this cycle
	wire `Chan ii_busy `XY;	// input messages accepted this cycle
	wire `XY o_v;				// output messages valid
	wire `Chan oo_v `XY;	// output messages valid
	wire done_all_pe, done_all_h;   	// done signals never defined?
    wire [63:0] recv `XYC;
	genvar x, y,c;
	localparam c1 = 0;
	
	localparam DATA_WIDTH  = C_M00_AXI_DATA_WIDTH;
	localparam ADDR_WIDTH  = C_M00_AXI_ADDR_WIDTH;
	localparam packetCount = 2**23;

   wire                  awvalid [N-1:0]  ;   
   wire                  awready [N-1:0]  ;   
   wire [33-1:0] awaddr  [N-1:0]  ;   
   wire                  wvalid  [N-1:0]  ;   
   wire                  wready  [N-1:0]  ;   
   wire [DATA_WIDTH-1:0] wdata   [N-1:0]  ;   
   wire                  bvalid  [N-1:0]  ;   
   wire                  bready  [N-1:0]  ;   
   wire                  arvalid [N-1:0]  ;   
   wire                  arready [N-1:0]  ;   
   wire [33-1:0] araddr  [N-1:0]  ;   
   wire                  rvalid  [N-1:0]  ;   
   wire                  rready  [N-1:0]  ;   
   wire [DATA_WIDTH-1:0] rdata   [N-1:0]  ;   
   wire                  rlast   [N-1:0]  ;   
   reg ap_done_r = 0;
   
    assign ap_done = ap_done_r;
	generate for (y = 0; y < Y_MAX-1; y = y + 1) begin : ys
		for (x = 0; x < X_MAX; x = x + 1) begin : xs
			(* keep_hierarchy = "yes" *)Client3 #(.PAT(PAT), .RATE(RATE), .PACKLIMIT(PACKLIMIT) ,.P_W(P_W) ,.M_A_W(M_A_W) ,
				.D_W(D_W), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX), .X(x), .Y(y))
			cli(.clk(clk), .rst(ce_r[(y) + (x)*(Y_MAX-1)][4]), .ce(1'b1), 
				.i_ack(i_ack[`xy]),
				.o_v(o_v[`xy]),
				.o(o[`xy]),
				.i(i[`xy]),
				.done(done[`xy])
				,.received(recv[`xyc1])
				,.cmd_w(cmd_r[(y) + (x)*(Y_MAX-1)][3]));

			(* keep_hierarchy = "yes" *)Fanout #(.NUM_CHANNELS(NUM_CHANNELS),
				.P_W(P_W) ,.M_A_W(M_A_W)  ,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX), .X(x), .Y(y))
			fanout_inst (.clk(clk), .rst(1'b0), 
					.i(i[`xy]), 
					.i_ack(i_ack[`xy]),
					.ii(ii[`xy]),
					.ii_ack(ii_ack[`xy]),
					.done_all(done_all_r1));

			(* keep_hierarchy = "yes" *)fanin_top #(.NUM_CHANNELS(NUM_CHANNELS),
				.P_W(P_W) ,.M_A_W(M_A_W)  ,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX), .X(x), .Y(y))
			fanin_inst (.clk(clk), .rst(1'b0), 
					.o(o[`xy]), 
					.o_v(o_v[`xy]),
					.oo(oo[`xy]),
					.ii_busy(ii_busy[`xy]),
					.oo_v(oo_v[`xy]),
					.done_all(done_all_r1));
				
				// sweep over multiple NoC channels
			
			for (c = 0; c < NUM_CHANNELS; c = c + 1) begin : cs
			(* keep_hierarchy = "yes" *)Hoplite #(.ROUTE(ROUTE), .LOWPOWER(LOWPOWER),.XBAR(XBAR) ,.P_W(P_W) ,.M_A_W(M_A_W) 
			,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), 
			.X_MAX(X_MAX), .Y_MAX(Y_MAX),.X(x), .Y(y), .CHAN(c))
			hop(.clk(clk), .rst(1'b0), .ce(1'b1),
				.n_in(oo[`xy1]`Slice),
				.w_in(e[`xy2]`Slice),
				.i(ii[`xy]`Slice),
				.i_ack(ii_ack[`xy][c]),
				.i_busy(ii_busy[`xy][c]),
				.o_v(oo_v[`xy][c]),
				.s_out(oo[`xy]`Slice),
				.e_out(e[`xy]`Slice),
				.done(done_h[`xyc]),
				.done_all(done_all_r1));
			end
		end
	end endgenerate
	
    generate 
        for (y = Y_MAX-1; y <= Y_MAX-1; y = y + 1) begin : ys_hbm
            for (x = 0; x < X_MAX; x = x + 1) begin : xs
                genvar c;
                for (c = 0; c < NUM_CHANNELS; c = c + 1) begin : cs
                 (* keep_hierarchy = "yes" *) client_hbm_top #(.PAT(PAT), .RATE(RATE), .PACKLIMIT(PACKLIMIT ),.P_W(P_W) ,.M_A_W(M_A_W) 
                    ,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX), .X(x), .Y(y),.CHAN(c) , .ADDR_WIDTH(33) , .DATA_WIDTH(DATA_WIDTH))
                     cli(.clk(clk), .rst(1'b0), .ce(1'b1), .cmd(0),
                    .i_ack(ii_ack[`xy][c]),
                    .o_v(oo_v[`xy][c]),
                    .o(oo[`xy]`Slice), // message from the router
                    .i(ii[`xy]`Slice),       // message to the router
                    .done(done[`xy])
                    ,.received(recv[`xyc])
                    ,.i_busy(ii_busy[`xy][c])
                    ,.awvalid  (     awvalid   [c + x*NUM_CHANNELS]  )
                    ,.awready  (     awready   [c + x*NUM_CHANNELS]  )
                    ,.awaddr   (     awaddr    [c + x*NUM_CHANNELS]  )
                    ,.wvalid   (     wvalid    [c + x*NUM_CHANNELS]  )
                    ,.wready   (     wready    [c + x*NUM_CHANNELS]  )
                    ,.wdata    (     wdata     [c + x*NUM_CHANNELS]  )
                    ,.bvalid   (     bvalid    [c + x*NUM_CHANNELS]  )
                    ,.bready   (     bready    [c + x*NUM_CHANNELS]  )
                    ,.arvalid  (     arvalid   [c + x*NUM_CHANNELS]  )
                    ,.arready  (     arready   [c + x*NUM_CHANNELS]  )
                    ,.araddr   (     araddr    [c + x*NUM_CHANNELS]  )
                    ,.rvalid   (     rvalid    [c + x*NUM_CHANNELS]  )
                    ,.rready   (     rready    [c + x*NUM_CHANNELS]  )
                    ,.rdata    (     rdata     [c + x*NUM_CHANNELS]  )
                    ,.rlast    (     rlast     [c + x*NUM_CHANNELS]  )
                    );
                   
                    (* keep_hierarchy = "yes" *)Hoplite #(.ROUTE(ROUTE), .LOWPOWER(LOWPOWER),.XBAR(XBAR),.P_W(P_W) ,.M_A_W(M_A_W) 
                    ,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), 
                    .X_MAX(X_MAX), .Y_MAX(Y_MAX),.X(x), .Y(y), .CHAN(c))
                    hop(.clk(clk), .rst(1'b0), .ce(1'b1),
                        .n_in(oo[`xy1]`Slice),
                        .w_in(e[`xy2]`Slice),
                        
                        .i(ii[`xy]`Slice),
                        .i_ack(ii_ack[`xy][c]),
                        .i_busy(ii_busy[`xy][c]),
                        .o_v(oo_v[`xy][c]),
                        .s_out(oo[`xy]`Slice),
                        .e_out(e[`xy]`Slice),
                        .done(done_h[`xyc]),
                        .done_all(done_all_r1));
                end
            end
        end
    endgenerate
	assign out_v = o[0]`v;
	assign out = o[0];
	assign done_all_pe = &done; // verilog supports reductions??!
	assign done_all_h = &done_h; // verilog supports reductions??!
	wire done_all_c;
	assign done_all_c = done_all_pe & done_all_h;


	integer cycles;
	reg done_all_r, done_all_r1;
	reg [63:0] sum;
	reg[63:0] writeCount_add_0[15:0];
    reg[63:0] writeCount_add_1[7:0];
    reg[63:0] writeCount_add_2[3:0];
    reg[63:0] writeCount_add_3[1:0];
	integer writeCount [31:0];
    reg `Cmd cmd_r [31:0][4:0];
    reg ce_r [31:0][4:0];
    
    integer j,k;
    reg beginClient_r=0;
    
generate if (`FP ==0) begin
initial 
    begin

		 for(j=0;j<32;j=j+1) begin
          for(k=0;k<5;k=k+1) begin
	        cmd_r[j][k] = {`linear,`WR,`p2p,6'b1};
            ce_r[j][k] = 1'b1;
          end		 
    end 
end

//always@(posedge clk) begin

//	for(j=0;j<32;j=j+1) begin
//            cmd_r[j][0] <= cmd;	
//         end    
//      if(beginClient_r) begin
//      for(j=0;j<32;j=j+1) begin
//	         ce_r[j][0] <= 0;
//	      end
//	       end
//    for(j=0;j<32;j=j+1) begin
//          for(k=0;k<4;k=k+1) begin
//	        cmd_r[j][k+1] <= cmd_r[j][k];
//            ce_r[j][k+1] <= ce_r[j][k];
//          end
//       end
//	       beginClient_r <= ce;
 
//	end
	
		always@(posedge clk) begin
	for(j=0;j<32;j=j+1) begin
            cmd_r[j][0] <= cmd;	
         end    
         
      for(j=0;j<32;j=j+1) begin
	         ce_r[j][0] <= ~beginClient_r;
      end

    for(j=0;j<32;j=j+1) begin
          for(k=0;k<11;k=k+1) begin
	        cmd_r[j][k+1] <= cmd_r[j][k];
            ce_r[j][k+1] <= ce_r[j][k];
          end
       end
	       beginClient_r <= ce;
 
	end
	
	integer u,v,w;	integer readCount [31:0];
initial begin
			for(u=0;u<N;u= u+1) begin
			     writeCount[u] = 0;
                 readCount[u]  = 0;

		    end
			     ap_done_r = 0;
end


	reg [63:0] sum_read;
	reg[63:0] readCount_add_0[15:0];
    reg[63:0] readCount_add_1[7:0];
    reg[63:0] readCount_add_2[3:0];
    reg[63:0] readCount_add_3[1:0];

	
	

	always @(posedge clk)
	begin
		if(rst) begin
		      for(u=0;u<N;u= u+1) begin
			     writeCount[u] <= 0;
			   end
		end 
//			cycles <= 0;
//			done_all_r <= 0;
//			done_all_r1 <= 0;
//			done_all <= 0;

		
			     
//			ap_done_r <= 0;
		 else begin
		
		    if(m00_axi_bvalid) 
		      writeCount[0] <= writeCount[0]+1'b1;
		      
		    if(m01_axi_bvalid) 
		      writeCount[1] <= writeCount[1]+1'b1;
		      
		    if(m02_axi_bvalid) 
		      writeCount[2] <= writeCount[2]+1'b1;
		      
		    if(m03_axi_bvalid) 
		      writeCount[3] <= writeCount[3]+1'b1;
		      
		    if(m04_axi_bvalid) 
		      writeCount[4] <= writeCount[4]+1'b1;
		      
		    if(m05_axi_bvalid) 
		      writeCount[5] <= writeCount[5]+1'b1;
		      
		    if(m06_axi_bvalid) 
		      writeCount[6] <= writeCount[6]+1'b1;
		      
		    if(m07_axi_bvalid) 
		      writeCount[7] <= writeCount[7]+1'b1;
		      
	        if(m08_axi_bvalid) 
		      writeCount[8] <= writeCount[8]+1'b1;
		      
		    if(m09_axi_bvalid) 
		      writeCount[9] <= writeCount[9]+1'b1;
		      
		    if(m10_axi_bvalid) 
		      writeCount[10] <= writeCount[10]+1'b1;
		      
		    if(m11_axi_bvalid) 
		      writeCount[11] <= writeCount[11]+1'b1;
		      
		    if(m12_axi_bvalid) 
		      writeCount[12] <= writeCount[12]+1'b1;
		      
		    if(m13_axi_bvalid) 
		      writeCount[13] <= writeCount[13]+1'b1;
		      
		    if(m14_axi_bvalid) 
		      writeCount[14] <= writeCount[14]+1'b1;
		      
		    if(m15_axi_bvalid) 
		      writeCount[15] <= writeCount[15]+1'b1;
		      
		    if(m16_axi_bvalid) 
		      writeCount[16] <= writeCount[16]+1'b1;
		      
		    if(m17_axi_bvalid) 
		      writeCount[17] <= writeCount[17]+1'b1;
		      
		    if(m18_axi_bvalid) 
		      writeCount[18] <= writeCount[18]+1'b1;
		      
		    if(m19_axi_bvalid) 
		      writeCount[19] <= writeCount[19]+1'b1;
		      
		    if(m20_axi_bvalid) 
		      writeCount[20] <= writeCount[20]+1'b1;
		      
		    if(m21_axi_bvalid) 
		      writeCount[21] <= writeCount[21]+1'b1;
		      
		    if(m22_axi_bvalid) 
		      writeCount[22] <= writeCount[22]+1'b1;
		      
		    if(m23_axi_bvalid) 
		      writeCount[23] <= writeCount[23]+1'b1;
		      
	        if(m24_axi_bvalid) 
		      writeCount[24] <= writeCount[24]+1'b1;
		      
		    if(m25_axi_bvalid) 
		      writeCount[25] <= writeCount[25]+1'b1;
		      
		    if(m26_axi_bvalid) 
		      writeCount[26] <= writeCount[26]+1'b1;
		      
		    if(m27_axi_bvalid) 
		      writeCount[27] <= writeCount[27]+1'b1;
		      
		    if(m28_axi_bvalid) 
		      writeCount[28] <= writeCount[28]+1'b1;
		      
		    if(m29_axi_bvalid) 
		      writeCount[29] <= writeCount[29]+1'b1;
		      
		    if(m30_axi_bvalid) 
		      writeCount[30] <= writeCount[30]+1'b1;
		      
		    if(m31_axi_bvalid) 
		      writeCount[31] <= writeCount[31]+1'b1;
		      
            writeCount_add_0[0] <= writeCount[0]+writeCount[1];
            writeCount_add_0[1] <= writeCount[2]+writeCount[3];
            writeCount_add_0[2] <= writeCount[4]+writeCount[5];
            writeCount_add_0[3] <= writeCount[6]+writeCount[7];
                       
            writeCount_add_0[4] <= writeCount[8]+writeCount[9];
            writeCount_add_0[5] <= writeCount[10]+writeCount[11];
            writeCount_add_0[6] <= writeCount[12]+writeCount[13];
            writeCount_add_0[7] <= writeCount[14]+writeCount[15];
            
            writeCount_add_0[8] <= writeCount[16]+writeCount[17];
            writeCount_add_0[9] <= writeCount[18]+writeCount[19];
            writeCount_add_0[10] <= writeCount[20]+writeCount[21];
            writeCount_add_0[11] <= writeCount[22]+writeCount[23];
               
            writeCount_add_0[12] <= writeCount[24]+writeCount[25];
            writeCount_add_0[13] <= writeCount[26]+writeCount[27];
            writeCount_add_0[14] <= writeCount[28]+writeCount[29];
            writeCount_add_0[15] <= writeCount[30]+writeCount[31];
            
            writeCount_add_1[0] <= writeCount_add_0[0]+writeCount_add_0[1];
            writeCount_add_1[1] <= writeCount_add_0[2]+writeCount_add_0[3];
            writeCount_add_1[2] <= writeCount_add_0[4]+writeCount_add_0[5];
            writeCount_add_1[3] <= writeCount_add_0[6]+writeCount_add_0[7];
            writeCount_add_1[4] <= writeCount_add_0[8]+writeCount_add_0[9];
            writeCount_add_1[5] <= writeCount_add_0[10]+writeCount_add_0[11];
            writeCount_add_1[6] <= writeCount_add_0[12]+writeCount_add_0[13];
            writeCount_add_1[7] <= writeCount_add_0[14]+writeCount_add_0[15];
            
            writeCount_add_2[0] <= writeCount_add_1[0]+writeCount_add_1[1];
            writeCount_add_2[1] <= writeCount_add_1[2]+writeCount_add_1[3];
            writeCount_add_2[2] <= writeCount_add_1[4]+writeCount_add_1[5];
            writeCount_add_2[3] <= writeCount_add_1[6]+writeCount_add_1[7];
                      
            writeCount_add_3[0] <= writeCount_add_2[0]+writeCount_add_2[1];
            writeCount_add_3[1] <= writeCount_add_2[2]+writeCount_add_2[3];
            
            sum <= writeCount_add_3[0]+writeCount_add_3[1];
            
            		    if(m00_axi_rvalid && m00_axi_rready) 
		      readCount[0] <= readCount[0]+1'b1;
		      
		    if(m01_axi_rvalid && m01_axi_rready) 
		      readCount[1] <= readCount[1]+1'b1;
		      
		    if(m02_axi_rvalid && m02_axi_rready) 
		      readCount[2] <= readCount[2]+1'b1;
		      
		    if(m03_axi_rvalid && m03_axi_rready) 
		      readCount[3] <= readCount[3]+1'b1;
		      
		    if(m04_axi_rvalid && m04_axi_rready) 
		      readCount[4] <= readCount[4]+1'b1;
		      
		    if(m05_axi_rvalid && m05_axi_rready) 
		      readCount[5] <= readCount[5]+1'b1;
		      
		    if(m06_axi_rvalid && m06_axi_rready) 
		      readCount[6] <= readCount[6]+1'b1;
		      
		    if(m07_axi_rvalid && m07_axi_rready) 
		      readCount[7] <= readCount[7]+1'b1;
		      
	        if(m08_axi_rvalid && m08_axi_rready) 
		      readCount[8] <= readCount[8]+1'b1;
		      
		    if(m09_axi_rvalid && m09_axi_rready) 
		      readCount[9] <= readCount[9]+1'b1;
		      
		    if(m10_axi_rvalid && m10_axi_rready) 
		      readCount[10] <= readCount[10]+1'b1;
		      
		    if(m11_axi_rvalid && m11_axi_rready) 
		      readCount[11] <= readCount[11]+1'b1;
		      
		    if(m12_axi_rvalid && m12_axi_rready) 
		      readCount[12] <= readCount[12]+1'b1;
		      
		    if(m13_axi_rvalid && m13_axi_rready) 
		      readCount[13] <= readCount[13]+1'b1;
		      
		    if(m14_axi_rvalid && m14_axi_rready) 
		      readCount[14] <= readCount[14]+1'b1;
		      
		    if(m15_axi_rvalid && m15_axi_rready) 
		      readCount[15] <= readCount[15]+1'b1;
		      
		    if(m16_axi_rvalid && m16_axi_rready) 
		      readCount[16] <= readCount[16]+1'b1;
		      
		    if(m17_axi_rvalid && m17_axi_rready)  
		      readCount[17] <= readCount[17]+1'b1;
		      
		    if(m18_axi_rvalid && m18_axi_rready) 
		      readCount[18] <= readCount[18]+1'b1;
		      
		    if(m19_axi_rvalid && m19_axi_rready) 
		      readCount[19] <= readCount[19]+1'b1;
		      
		    if(m20_axi_rvalid && m20_axi_rready) 
		      readCount[20] <= readCount[20]+1'b1;
		      
		    if(m21_axi_rvalid && m21_axi_rready) 
		      readCount[21] <= readCount[21]+1'b1;
		      
		    if(m22_axi_rvalid && m22_axi_rready) 
		      readCount[22] <= readCount[22]+1'b1;
		      
		    if(m23_axi_rvalid && m23_axi_rready) 
		      readCount[23] <= readCount[23]+1'b1;
		      
	        if(m24_axi_rvalid && m24_axi_rready) 
		      readCount[24] <= readCount[24]+1'b1;
		      
		    if(m25_axi_rvalid && m25_axi_rready) 
		      readCount[25] <= readCount[25]+1'b1;
		      
		    if(m26_axi_rvalid && m26_axi_rready) 
		      readCount[26] <= readCount[26]+1'b1;
		      
		    if(m27_axi_rvalid && m27_axi_rready) 
		      readCount[27] <= readCount[27]+1'b1;
		      
		    if(m28_axi_rvalid && m28_axi_rready) 
		      readCount[28] <= readCount[28]+1'b1;
		      
		    if(m29_axi_rvalid && m29_axi_rready) 
		      readCount[29] <= readCount[29]+1'b1;
		      
		    if(m30_axi_rvalid && m30_axi_rready) 
		      readCount[30] <= readCount[30]+1'b1;
		      
		    if(m31_axi_rvalid && m31_axi_rready) 
		      readCount[31] <= readCount[31]+1'b1;
		      
            readCount_add_0[0] <= readCount[0]+readCount[1];
            readCount_add_0[1] <= readCount[2]+readCount[3];
            readCount_add_0[2] <= readCount[4]+readCount[5];
            readCount_add_0[3] <= readCount[6]+readCount[7];
                       
            readCount_add_0[4] <= readCount[8]+readCount[9];
            readCount_add_0[5] <= readCount[10]+readCount[11];
            readCount_add_0[6] <= readCount[12]+readCount[13];
            readCount_add_0[7] <= readCount[14]+readCount[15];
            
            readCount_add_0[8] <= readCount[16]+readCount[17];
            readCount_add_0[9] <= readCount[18]+readCount[19];
            readCount_add_0[10] <= readCount[20]+readCount[21];
            readCount_add_0[11] <= readCount[22]+readCount[23];
               
            readCount_add_0[12] <= readCount[24]+readCount[25];
            readCount_add_0[13] <= readCount[26]+readCount[27];
            readCount_add_0[14] <= readCount[28]+readCount[29];
            readCount_add_0[15] <= readCount[30]+readCount[31];
            
            readCount_add_1[0] <= readCount_add_0[0]+readCount_add_0[1];
            readCount_add_1[1] <= readCount_add_0[2]+readCount_add_0[3];
            readCount_add_1[2] <= readCount_add_0[4]+readCount_add_0[5];
            readCount_add_1[3] <= readCount_add_0[6]+readCount_add_0[7];
            readCount_add_1[4] <= readCount_add_0[8]+readCount_add_0[9];
            readCount_add_1[5] <= readCount_add_0[10]+readCount_add_0[11];
            readCount_add_1[6] <= readCount_add_0[12]+readCount_add_0[13];
            readCount_add_1[7] <= readCount_add_0[14]+readCount_add_0[15];
            
            readCount_add_2[0] <= readCount_add_1[0]+readCount_add_1[1];
            readCount_add_2[1] <= readCount_add_1[2]+readCount_add_1[3];
            readCount_add_2[2] <= readCount_add_1[4]+readCount_add_1[5];
            readCount_add_2[3] <= readCount_add_1[6]+readCount_add_1[7];
                      
            readCount_add_3[0] <= readCount_add_2[0]+readCount_add_2[1];
            readCount_add_3[1] <= readCount_add_2[2]+readCount_add_2[3];
            
            sum_read <= readCount_add_3[0]+readCount_add_3[1];
            
            
			cycles <= cycles + 1;
			done_all_r <= done_all_c;
			done_all_r1 <= done_all_r;
			done_all <= done_all_r1;
			
//			sum <= (writeCount[0]+writeCount[1]+writeCount[2]+writeCount[3]+writeCount[4]+writeCount[5]+writeCount[6]+writeCount[7]
//			        +writeCount[8]+writeCount[9]+writeCount[10]+writeCount[11]+writeCount[12]+writeCount[13]+writeCount[14]+writeCount[15]);
			ap_done_r <= 0;   
			if( sum == 32*(packetCount+1)) begin
//			 $finish();
                ap_done_r<=1'b1;
                sum <= 0;
                writeCount_add_3[0] <= 0;
                writeCount_add_2[0] <= 0;
                writeCount_add_1[0] <= 0;
                writeCount_add_0[0]<=0;
                writeCount[0] <= writeCount[0] + 1;
                writeCount[1] <= writeCount[1] + 1;
                writeCount[2] <= writeCount[2] + 1;
                writeCount[3] <= writeCount[3] + 1;
                
             
                end
                
		end
	end
	
end
endgenerate
	
	// fix ready signal from the hbm portss
assign m00_axi_awvalid =  awvalid          [0];
assign  awready[0]     =  m00_axi_awready;
assign m00_axi_awaddr  =  {31'b0,awaddr    [0]};
assign m00_axi_wvalid  =  wvalid           [0];
assign wready[0]       =   m00_axi_wready    ;
assign m00_axi_wdata   =  wdata            [0];
//assign m00_axi_bvalid  =  bvalid           [0];
assign m00_axi_bready  =  bready           [0];
assign m00_axi_arvalid =  arvalid          [0];
assign arready[0]      =  m00_axi_arready    ;
assign m00_axi_araddr  =  {31'b0,araddr    [0]}; 
assign rvalid           [0] = m00_axi_rvalid  ;
assign m00_axi_rready  =  rready           [0];
assign rdata       [0] = m00_axi_rdata   ;
//assign  rlast      [0] = m00_axi_rlast   ;
assign m00_axi_awlen   =  8'b0;
assign m00_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m00_axi_wlast   =  1'b1;
assign m00_axi_arlen   =  8'b0;
     
assign m01_axi_awvalid =  awvalid          [1];       
assign awready      [1]=  m01_axi_awready     ;       
assign m01_axi_awaddr  =  {31'b0,awaddr    [1]};      
assign m01_axi_wvalid  =  wvalid           [1];       
assign wready       [1]=  m01_axi_wready;       
assign m01_axi_wdata   =  wdata            [1];       
//assign m01_axi_bvalid  =  bvalid           [1];       
assign m01_axi_bready  =  bready           [1];       
assign m01_axi_arvalid =  arvalid          [1];       
assign arready      [1]=  m01_axi_arready     ;       
assign m01_axi_araddr  =  {31'b0,araddr    [1]};      
assign rvalid           [1] = m01_axi_rvalid  ;       
assign m01_axi_rready  =  rready           [1];       
assign rdata            [1] = m01_axi_rdata   ;       
//assign m01_axi_rlast   =  rlast            [1];       
assign m01_axi_awlen   =  8'b0;                       
assign m01_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m01_axi_wlast   =  1'b1;                       
assign m01_axi_arlen   =  8'b0;                         
  
assign m02_axi_awvalid =  awvalid          [2];             
assign awready      [2]=  m02_axi_awready     ;             
assign m02_axi_awaddr  =  {31'b0,awaddr    [2]};            
assign m02_axi_wvalid  =  wvalid           [2];             
assign  wready     [2] =  m02_axi_wready;             
assign m02_axi_wdata   =  wdata            [2];             
//assign m02_axi_bvalid  =  bvalid           [2];             
assign m02_axi_bready  =  bready           [2];             
assign m02_axi_arvalid =  arvalid          [2];             
assign arready     [2] =  m02_axi_arready;             
assign m02_axi_araddr  =  {31'b0,araddr    [2]};            
assign rvalid           [2]= m02_axi_rvalid  ;             
assign m02_axi_rready  =  rready           [2];             
assign rdata            [2] = m02_axi_rdata   ;             
//assign m02_axi_rlast   =  rlast            [2];             
assign m02_axi_awlen   =  8'b0;                             
assign m02_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m02_axi_wlast   =  1'b1;                             
assign m02_axi_arlen   =  8'b0;                             
                
assign m03_axi_awvalid =  awvalid          [3];       
assign awready      [3]=    m03_axi_awready  ;       
assign m03_axi_awaddr  =  {31'b0,awaddr    [3]};      
assign m03_axi_wvalid  =  wvalid           [3];       
assign wready       [3]= m03_axi_wready    ;       
assign m03_axi_wdata   =  wdata            [3];       
//assign m03_axi_bvalid  =  bvalid           [3];       
assign m03_axi_bready  =  bready           [3];       
assign m03_axi_arvalid =  arvalid          [3];       
assign arready      [3]= m03_axi_arready     ;       
assign m03_axi_araddr  =  {31'b0,araddr    [3]};      
assign rvalid           [3]= m03_axi_rvalid  ;       
assign m03_axi_rready  =  rready           [3];       
assign rdata            [3] = m03_axi_rdata   ;      
//assign m03_axi_rlast   =  rlast            [3];       
assign m03_axi_awlen   =  8'b0;                                      
assign m03_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m03_axi_wlast   =  1'b1;                       
assign m03_axi_arlen   =  8'b0;                       

assign m04_axi_awvalid =  awvalid          [4];
assign  awready[4]     =  m04_axi_awready;
assign m04_axi_awaddr  =  {31'b0,awaddr    [4]};
assign m04_axi_wvalid  =  wvalid           [4];
assign wready[4]       =   m04_axi_wready    ;
assign m04_axi_wdata   =  wdata            [4];
//assign m04_axi_bvalid  =  bvalid           [4];
assign m04_axi_bready  =  bready           [4];
assign m04_axi_arvalid =  arvalid          [4];
assign arready[4]      =  m04_axi_arready    ;
assign m04_axi_araddr  =  {31'b0,araddr    [4]}; 
assign rvalid           [4] = m04_axi_rvalid  ;
assign m04_axi_rready  =  rready           [4];
assign rdata       [4] = m04_axi_rdata   ;
//assign  rlast      [4] = m04_axi_rlast   ;
assign m04_axi_awlen   =  8'b0;
assign m04_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m04_axi_wlast   =  1'b1;
assign m04_axi_arlen   =  8'b0;
     
assign m05_axi_awvalid =  awvalid          [5];       
assign awready      [5]=  m05_axi_awready     ;       
assign m05_axi_awaddr  =  {31'b0,awaddr    [5]};      
assign m05_axi_wvalid  =  wvalid           [5];       
assign wready       [5]=  m05_axi_wready;       
assign m05_axi_wdata   =  wdata            [5];       
//assign m05_axi_bvalid  =  bvalid           [5];       
assign m05_axi_bready  =  bready           [5];       
assign m05_axi_arvalid =  arvalid          [5];       
assign arready      [5]=  m05_axi_arready     ;       
assign m05_axi_araddr  =  {31'b0,araddr    [5]};      
assign rvalid           [5] = m05_axi_rvalid  ;       
assign m05_axi_rready  =  rready           [5];       
assign rdata            [5] = m05_axi_rdata   ;       
//assign m05_axi_rlast   =  rlast            [5];       
assign m05_axi_awlen   =  8'b0;                       
assign m05_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m05_axi_wlast   =  1'b1;                       
assign m05_axi_arlen   =  8'b0;                         
  
assign m06_axi_awvalid =  awvalid          [6];             
assign awready      [6]=  m06_axi_awready     ;             
assign m06_axi_awaddr  =  {31'b0,awaddr    [6]};            
assign m06_axi_wvalid  =  wvalid           [6];             
assign  wready     [6] =  m06_axi_wready;             
assign m06_axi_wdata   =  wdata            [6];             
//assign m06_axi_bvalid  =  bvalid           [6];             
assign m06_axi_bready  =  bready           [6];             
assign m06_axi_arvalid =  arvalid          [6];             
assign arready     [6] =  m06_axi_arready;             
assign m06_axi_araddr  =  {31'b0,araddr    [6]};            
assign rvalid           [6]= m06_axi_rvalid  ;             
assign m06_axi_rready  =  rready           [6];             
assign rdata            [6] = m06_axi_rdata   ;             
//assign m06_axi_rlast   =  rlast            [6];             
assign m06_axi_awlen   =  8'b0;                             
assign m06_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m06_axi_wlast   =  1'b1;                             
assign m06_axi_arlen   =  8'b0;                             
                
assign m07_axi_awvalid =  awvalid          [7];       
assign awready      [7]=    m07_axi_awready  ;       
assign m07_axi_awaddr  =  {31'b0,awaddr    [7]};      
assign m07_axi_wvalid  =  wvalid           [7];       
assign wready       [7]= m07_axi_wready    ;       
assign m07_axi_wdata   =  wdata            [7];       
//assign m07_axi_bvalid  =  bvalid           [7];       
assign m07_axi_bready  =  bready           [7];       
assign m07_axi_arvalid =  arvalid          [7];       
assign arready      [7]= m07_axi_arready     ;       
assign m07_axi_araddr  =  {31'b0,araddr    [7]};      
assign rvalid           [7]= m07_axi_rvalid  ;       
assign m07_axi_rready  =  rready           [7];       
assign rdata            [7] = m07_axi_rdata   ;      
//assign m07_axi_rlast   =  rlast            [7];       
assign m07_axi_awlen   =  8'b0;                                      
assign m07_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m07_axi_wlast   =  1'b1;                       
assign m07_axi_arlen   =  8'b0;            

assign m08_axi_awvalid =  awvalid          [8];
assign  awready[8]     =  m08_axi_awready;
assign m08_axi_awaddr  =  {31'b0,awaddr    [8]};
assign m08_axi_wvalid  =  wvalid           [8];
assign wready[8]       =   m08_axi_wready    ;
assign m08_axi_wdata   =  wdata            [8];
//assign m08_axi_bvalid  =  bvalid           [8];
assign m08_axi_bready  =  bready           [8];
assign m08_axi_arvalid =  arvalid          [8];
assign arready[8]      =  m08_axi_arready    ;
assign m08_axi_araddr  =  {31'b0,araddr    [8]}; 
assign rvalid           [8] = m08_axi_rvalid  ;
assign m08_axi_rready  =  rready           [8];
assign rdata       [8] = m08_axi_rdata   ;
//assign  rlast      [8] = m08_axi_rlast   ;
assign m08_axi_awlen   =  8'b0;
assign m08_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m08_axi_wlast   =  1'b1;
assign m08_axi_arlen   =  8'b0;
     
assign m09_axi_awvalid =  awvalid          [9];       
assign awready      [9]=  m09_axi_awready     ;       
assign m09_axi_awaddr  =  {31'b0,awaddr    [9]};      
assign m09_axi_wvalid  =  wvalid           [9];       
assign wready       [9]=  m09_axi_wready;       
assign m09_axi_wdata   =  wdata            [9];       
//assign m09_axi_bvalid  =  bvalid           [9];       
assign m09_axi_bready  =  bready           [9];       
assign m09_axi_arvalid =  arvalid          [9];       
assign arready      [9]=  m09_axi_arready     ;       
assign m09_axi_araddr  =  {31'b0,araddr    [9]};      
assign rvalid           [9] = m09_axi_rvalid  ;       
assign m09_axi_rready  =  rready           [9];       
assign rdata            [9] = m09_axi_rdata   ;       
//assign m09_axi_rlast   =  rlast            [9];       
assign m09_axi_awlen   =  8'b0;                       
assign m09_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m09_axi_wlast   =  1'b1;                       
assign m09_axi_arlen   =  8'b0;                         
  
assign m10_axi_awvalid =  awvalid          [10];             
assign awready      [10]=  m10_axi_awready     ;             
assign m10_axi_awaddr  =  {31'b0,awaddr    [10]};            
assign m10_axi_wvalid  =  wvalid           [10];             
assign  wready     [10] =  m10_axi_wready;             
assign m10_axi_wdata   =  wdata            [10];             
//assign m10_axi_bvalid  =  bvalid           [10];             
assign m10_axi_bready  =  bready           [10];             
assign m10_axi_arvalid =  arvalid          [10];             
assign arready     [10] =  m10_axi_arready;             
assign m10_axi_araddr  =  {31'b0,araddr    [10]};            
assign rvalid           [10]= m10_axi_rvalid  ;             
assign m10_axi_rready  =  rready           [10];             
assign rdata            [10] = m10_axi_rdata   ;             
//assign m10_axi_rlast   =  rlast            [10];             
assign m10_axi_awlen   =  8'b0;                             
assign m10_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m10_axi_wlast   =  1'b1;                             
assign m10_axi_arlen   =  8'b0;                             
                
assign m11_axi_awvalid =  awvalid          [11];       
assign awready      [11]=    m11_axi_awready  ;       
assign m11_axi_awaddr  =  {31'b0,awaddr    [11]};      
assign m11_axi_wvalid  =  wvalid           [11];       
assign wready       [11]= m11_axi_wready    ;       
assign m11_axi_wdata   =  wdata            [11];       
//assign m11_axi_bvalid  =  bvalid           [11];       
assign m11_axi_bready  =  bready           [11];       
assign m11_axi_arvalid =  arvalid          [11];       
assign arready      [11]= m11_axi_arready     ;       
assign m11_axi_araddr  =  {31'b0,araddr    [11]};      
assign rvalid           [11]= m11_axi_rvalid  ;       
assign m11_axi_rready  =  rready           [11];       
assign rdata            [11] = m11_axi_rdata   ;      
//assign m11_axi_rlast   =  rlast            [11];       
assign m11_axi_awlen   =  8'b0;                                      
assign m11_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m11_axi_wlast   =  1'b1;                       
assign m11_axi_arlen   =  8'b0;                       

assign m12_axi_awvalid =  awvalid          [12];
assign  awready[12]     =  m12_axi_awready;
assign m12_axi_awaddr  =  {31'b0,awaddr    [12]};
assign m12_axi_wvalid  =  wvalid           [12];
assign wready[12]       =   m12_axi_wready    ;
assign m12_axi_wdata   =  wdata            [12];
//assign m12_axi_bvalid  =  bvalid           [12];
assign m12_axi_bready  =  bready           [12];
assign m12_axi_arvalid =  arvalid          [12];
assign arready[12]      =  m12_axi_arready    ;
assign m12_axi_araddr  =  {31'b0,araddr    [12]}; 
assign rvalid           [12] = m12_axi_rvalid  ;
assign m12_axi_rready  =  rready           [12];
assign rdata       [12] = m12_axi_rdata   ;
//assign  rlast      [12] = m12_axi_rlast   ;
assign m12_axi_awlen   =  8'b0;
assign m12_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m12_axi_wlast   =  1'b1;
assign m12_axi_arlen   =  8'b0;
     
assign m13_axi_awvalid =  awvalid          [13];       
assign awready      [13]=  m13_axi_awready     ;       
assign m13_axi_awaddr  =  {31'b0,awaddr    [13]};      
assign m13_axi_wvalid  =  wvalid           [13];       
assign wready       [13]=  m13_axi_wready;       
assign m13_axi_wdata   =  wdata            [13];       
//assign m13_axi_bvalid  =  bvalid           [13];       
assign m13_axi_bready  =  bready           [13];       
assign m13_axi_arvalid =  arvalid          [13];       
assign arready      [13]=  m13_axi_arready     ;       
assign m13_axi_araddr  =  {31'b0,araddr    [13]};      
assign rvalid           [13] = m13_axi_rvalid  ;       
assign m13_axi_rready  =  rready           [13];       
assign rdata            [13] = m13_axi_rdata   ;       
//assign m13_axi_rlast   =  rlast            [13];       
assign m13_axi_awlen   =  8'b0;                       
assign m13_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m13_axi_wlast   =  1'b1;                       
assign m13_axi_arlen   =  8'b0;                         
  
assign m14_axi_awvalid =  awvalid          [14];             
assign awready      [14]=  m14_axi_awready     ;             
assign m14_axi_awaddr  =  {31'b0,awaddr    [14]};            
assign m14_axi_wvalid  =  wvalid           [14];             
assign  wready     [14] =  m14_axi_wready;             
assign m14_axi_wdata   =  wdata            [14];             
//assign m14_axi_bvalid  =  bvalid           [14];             
assign m14_axi_bready  =  bready           [14];             
assign m14_axi_arvalid =  arvalid          [14];             
assign arready     [14] =  m14_axi_arready;             
assign m14_axi_araddr  =  {31'b0,araddr    [14]};            
assign rvalid           [14]= m14_axi_rvalid  ;             
assign m14_axi_rready  =  rready           [14];             
assign rdata            [14] = m14_axi_rdata   ;             
//assign m14_axi_rlast   =  rlast            [14];             
assign m14_axi_awlen   =  8'b0;                             
assign m14_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m14_axi_wlast   =  1'b1;                             
assign m14_axi_arlen   =  8'b0;                             
                
assign m15_axi_awvalid =  awvalid          [15];       
assign awready      [15]=    m15_axi_awready  ;       
assign m15_axi_awaddr  =  {31'b0,awaddr    [15]};      
assign m15_axi_wvalid  =  wvalid           [15];       
assign wready       [15]= m15_axi_wready    ;       
assign m15_axi_wdata   =  wdata            [15];       
//assign m15_axi_bvalid  =  bvalid           [15];       
assign m15_axi_bready  =  bready           [15];       
assign m15_axi_arvalid =  arvalid          [15];       
assign arready      [15]= m15_axi_arready     ;       
assign m15_axi_araddr  =  {31'b0,araddr    [15]};      
assign rvalid           [15]= m15_axi_rvalid  ;       
assign m15_axi_rready  =  rready           [15];       
assign rdata            [15] = m15_axi_rdata   ;      
//assign m15_axi_rlast   =  rlast            [15];       
assign m15_axi_awlen   =  8'b0;                                      
assign m15_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m15_axi_wlast   =  1'b1;                       
assign m15_axi_arlen   =  8'b0;            

	// fix ready signal from the hbm portss
assign m16_axi_awvalid =  awvalid          [16];
assign  awready[16]     =  m16_axi_awready;
assign m16_axi_awaddr  =  {31'b0,awaddr    [16]};
assign m16_axi_wvalid  =  wvalid           [16];
assign wready[16]       =   m16_axi_wready    ;
assign m16_axi_wdata   =  wdata            [16];
//assign m16_axi_bvalid  =  bvalid           [16];
assign m16_axi_bready  =  bready           [16];
assign m16_axi_arvalid =  arvalid          [16];
assign arready[16]      =  m16_axi_arready    ;
assign m16_axi_araddr  =  {31'b0,araddr    [16]}; 
assign rvalid           [16] = m16_axi_rvalid  ;
assign m16_axi_rready  =  rready           [16];
assign rdata       [16] = m16_axi_rdata   ;
//assign  rlast      [16] = m16_axi_rlast   ;
assign m16_axi_awlen   =  8'b0;
assign m16_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m16_axi_wlast   =  1'b1;
assign m16_axi_arlen   =  8'b0;
     
assign m17_axi_awvalid =  awvalid          [17];       
assign awready      [17]=  m17_axi_awready     ;       
assign m17_axi_awaddr  =  {31'b0,awaddr    [17]};      
assign m17_axi_wvalid  =  wvalid           [17];       
assign wready       [17]=  m17_axi_wready;       
assign m17_axi_wdata   =  wdata            [17];       
//assign m17_axi_bvalid  =  bvalid           [17];       
assign m17_axi_bready  =  bready           [17];       
assign m17_axi_arvalid =  arvalid          [17];       
assign arready      [17]=  m17_axi_arready     ;       
assign m17_axi_araddr  =  {31'b0,araddr    [17]};      
assign rvalid           [17] = m17_axi_rvalid  ;       
assign m17_axi_rready  =  rready           [17];       
assign rdata            [17] = m17_axi_rdata   ;       
//assign m17_axi_rlast   =  rlast            [17];       
assign m17_axi_awlen   =  8'b0;                       
assign m17_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m17_axi_wlast   =  1'b1;                       
assign m17_axi_arlen   =  8'b0;                         
  
assign m18_axi_awvalid =  awvalid          [18];             
assign awready      [18]=  m18_axi_awready     ;             
assign m18_axi_awaddr  =  {31'b0,awaddr    [18]};            
assign m18_axi_wvalid  =  wvalid           [18];             
assign  wready     [18] =  m18_axi_wready;             
assign m18_axi_wdata   =  wdata            [18];             
//assign m18_axi_bvalid  =  bvalid           [18];             
assign m18_axi_bready  =  bready           [18];             
assign m18_axi_arvalid =  arvalid          [18];             
assign arready     [18] =  m18_axi_arready;             
assign m18_axi_araddr  =  {31'b0,araddr    [18]};            
assign rvalid           [18]= m18_axi_rvalid  ;             
assign m18_axi_rready  =  rready           [18];             
assign rdata            [18] = m18_axi_rdata   ;             
//assign m18_axi_rlast   =  rlast            [18];             
assign m18_axi_awlen   =  8'b0;                             
assign m18_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m18_axi_wlast   =  1'b1;                             
assign m18_axi_arlen   =  8'b0;                             
                
assign m19_axi_awvalid =  awvalid          [19];       
assign awready      [19]=    m19_axi_awready  ;       
assign m19_axi_awaddr  =  {31'b0,awaddr    [19]};      
assign m19_axi_wvalid  =  wvalid           [19];       
assign wready       [19]= m19_axi_wready    ;       
assign m19_axi_wdata   =  wdata            [19];       
//assign m19_axi_bvalid  =  bvalid           [19];       
assign m19_axi_bready  =  bready           [19];       
assign m19_axi_arvalid =  arvalid          [19];       
assign arready      [19]= m19_axi_arready     ;       
assign m19_axi_araddr  =  {31'b0,araddr    [19]};      
assign rvalid           [19]= m19_axi_rvalid  ;       
assign m19_axi_rready  =  rready           [19];       
assign rdata            [19] = m19_axi_rdata   ;      
//assign m19_axi_rlast   =  rlast            [19];       
assign m19_axi_awlen   =  8'b0;                                      
assign m19_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m19_axi_wlast   =  1'b1;                       
assign m19_axi_arlen   =  8'b0;                       

assign m20_axi_awvalid =  awvalid          [20];
assign  awready[20]     =  m20_axi_awready;
assign m20_axi_awaddr  =  {31'b0,awaddr    [20]};
assign m20_axi_wvalid  =  wvalid           [20];
assign wready[20]       =   m20_axi_wready    ;
assign m20_axi_wdata   =  wdata            [20];
//assign m20_axi_bvalid  =  bvalid           [20];
assign m20_axi_bready  =  bready           [20];
assign m20_axi_arvalid =  arvalid          [20];
assign arready[20]      =  m20_axi_arready    ;
assign m20_axi_araddr  =  {31'b0,araddr    [20]}; 
assign rvalid           [20] = m20_axi_rvalid  ;
assign m20_axi_rready  =  rready           [20];
assign rdata       [20] = m20_axi_rdata   ;
//assign  rlast      [20] = m20_axi_rlast   ;
assign m20_axi_awlen   =  8'b0;
assign m20_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m20_axi_wlast   =  1'b1;
assign m20_axi_arlen   =  8'b0;
     
assign m21_axi_awvalid =  awvalid          [21];       
assign awready      [21]=  m21_axi_awready     ;       
assign m21_axi_awaddr  =  {31'b0,awaddr    [21]};      
assign m21_axi_wvalid  =  wvalid           [21];       
assign wready       [21]=  m21_axi_wready;       
assign m21_axi_wdata   =  wdata            [21];       
//assign m21_axi_bvalid  =  bvalid           [21];       
assign m21_axi_bready  =  bready           [21];       
assign m21_axi_arvalid =  arvalid          [21];       
assign arready      [21]=  m21_axi_arready     ;       
assign m21_axi_araddr  =  {31'b0,araddr    [21]};      
assign rvalid           [21] = m21_axi_rvalid  ;       
assign m21_axi_rready  =  rready           [21];       
assign rdata            [21] = m21_axi_rdata   ;       
//assign m21_axi_rlast   =  rlast            [21];       
assign m21_axi_awlen   =  8'b0;                       
assign m21_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m21_axi_wlast   =  1'b1;                       
assign m21_axi_arlen   =  8'b0;                         
  
assign m22_axi_awvalid =  awvalid          [22];             
assign awready      [22]=  m22_axi_awready     ;             
assign m22_axi_awaddr  =  {31'b0,awaddr    [22]};            
assign m22_axi_wvalid  =  wvalid           [22];             
assign  wready     [22] =  m22_axi_wready;             
assign m22_axi_wdata   =  wdata            [22];             
//assign m22_axi_bvalid  =  bvalid           [22];             
assign m22_axi_bready  =  bready           [22];             
assign m22_axi_arvalid =  arvalid          [22];             
assign arready     [22] =  m22_axi_arready;             
assign m22_axi_araddr  =  {31'b0,araddr    [22]};            
assign rvalid           [22]= m22_axi_rvalid  ;             
assign m22_axi_rready  =  rready           [22];             
assign rdata            [22] = m22_axi_rdata   ;             
//assign m22_axi_rlast   =  rlast            [22];             
assign m22_axi_awlen   =  8'b0;                             
assign m22_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m22_axi_wlast   =  1'b1;                             
assign m22_axi_arlen   =  8'b0;                             
                
assign m23_axi_awvalid =  awvalid          [23];       
assign awready      [23]=    m23_axi_awready  ;       
assign m23_axi_awaddr  =  {31'b0,awaddr    [23]};      
assign m23_axi_wvalid  =  wvalid           [23];       
assign wready       [23]= m23_axi_wready    ;       
assign m23_axi_wdata   =  wdata            [23];       
//assign m23_axi_bvalid  =  bvalid           [23];       
assign m23_axi_bready  =  bready           [23];       
assign m23_axi_arvalid =  arvalid          [23];       
assign arready      [23]= m23_axi_arready     ;       
assign m23_axi_araddr  =  {31'b0,araddr    [23]};      
assign rvalid           [23]= m23_axi_rvalid  ;       
assign m23_axi_rready  =  rready           [23];       
assign rdata            [23] = m23_axi_rdata   ;      
//assign m23_axi_rlast   =  rlast            [23];       
assign m23_axi_awlen   =  8'b0;                                      
assign m23_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m23_axi_wlast   =  1'b1;                       
assign m23_axi_arlen   =  8'b0;            

assign m24_axi_awvalid =  awvalid          [24];
assign  awready[24]     =  m24_axi_awready;
assign m24_axi_awaddr  =  {31'b0,awaddr    [24]};
assign m24_axi_wvalid  =  wvalid           [24];
assign wready[24]       =   m24_axi_wready    ;
assign m24_axi_wdata   =  wdata            [24];
//assign m24_axi_bvalid  =  bvalid           [24];
assign m24_axi_bready  =  bready           [24];
assign m24_axi_arvalid =  arvalid          [24];
assign arready[24]      =  m24_axi_arready    ;
assign m24_axi_araddr  =  {31'b0,araddr    [24]}; 
assign rvalid           [24] = m24_axi_rvalid  ;
assign m24_axi_rready  =  rready           [24];
assign rdata       [24] = m24_axi_rdata   ;
//assign  rlast      [24] = m24_axi_rlast   ;
assign m24_axi_awlen   =  8'b0;
assign m24_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m24_axi_wlast   =  1'b1;
assign m24_axi_arlen   =  8'b0;
     
assign m25_axi_awvalid =  awvalid          [25];       
assign awready      [25]=  m25_axi_awready     ;       
assign m25_axi_awaddr  =  {31'b0,awaddr    [25]};      
assign m25_axi_wvalid  =  wvalid           [25];       
assign wready       [25]=  m25_axi_wready;       
assign m25_axi_wdata   =  wdata            [25];       
//assign m25_axi_bvalid  =  bvalid           [25];       
assign m25_axi_bready  =  bready           [25];       
assign m25_axi_arvalid =  arvalid          [25];       
assign arready      [25]=  m25_axi_arready     ;       
assign m25_axi_araddr  =  {31'b0,araddr    [25]};      
assign rvalid           [25] = m25_axi_rvalid  ;       
assign m25_axi_rready  =  rready           [25];       
assign rdata            [25] = m25_axi_rdata   ;       
//assign m25_axi_rlast   =  rlast            [25];       
assign m25_axi_awlen   =  8'b0;                       
assign m25_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m25_axi_wlast   =  1'b1;                       
assign m25_axi_arlen   =  8'b0;                         
  
assign m26_axi_awvalid =  awvalid          [26];             
assign awready      [26]=  m26_axi_awready     ;             
assign m26_axi_awaddr  =  {31'b0,awaddr    [26]};            
assign m26_axi_wvalid  =  wvalid           [26];             
assign  wready     [26] =  m26_axi_wready;             
assign m26_axi_wdata   =  wdata            [26];             
//assign m26_axi_bvalid  =  bvalid           [26];             
assign m26_axi_bready  =  bready           [26];             
assign m26_axi_arvalid =  arvalid          [26];             
assign arready     [26] =  m26_axi_arready;             
assign m26_axi_araddr  =  {31'b0,araddr    [26]};            
assign rvalid           [26]= m26_axi_rvalid  ;             
assign m26_axi_rready  =  rready           [26];             
assign rdata            [26] = m26_axi_rdata   ;             
//assign m26_axi_rlast   =  rlast            [26];             
assign m26_axi_awlen   =  8'b0;                             
assign m26_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m26_axi_wlast   =  1'b1;                             
assign m26_axi_arlen   =  8'b0;                             
                
assign m27_axi_awvalid =  awvalid          [27];       
assign awready      [27]=    m27_axi_awready  ;       
assign m27_axi_awaddr  =  {31'b0,awaddr    [27]};      
assign m27_axi_wvalid  =  wvalid           [27];       
assign wready       [27]= m27_axi_wready    ;       
assign m27_axi_wdata   =  wdata            [27];       
//assign m27_axi_bvalid  =  bvalid           [27];       
assign m27_axi_bready  =  bready           [27];       
assign m27_axi_arvalid =  arvalid          [27];       
assign arready      [27]= m27_axi_arready     ;       
assign m27_axi_araddr  =  {31'b0,araddr    [27]};      
assign rvalid           [27]= m27_axi_rvalid  ;       
assign m27_axi_rready  =  rready           [27];       
assign rdata            [27] = m27_axi_rdata   ;      
//assign m27_axi_rlast   =  rlast            [27];       
assign m27_axi_awlen   =  8'b0;                                      
assign m27_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m27_axi_wlast   =  1'b1;                       
assign m27_axi_arlen   =  8'b0;                       

assign m28_axi_awvalid =  awvalid          [28];
assign  awready[28]     =  m28_axi_awready;
assign m28_axi_awaddr  =  {31'b0,awaddr    [28]};
assign m28_axi_wvalid  =  wvalid           [28];
assign wready[28]       =   m28_axi_wready    ;
assign m28_axi_wdata   =  wdata            [28];
//assign m28_axi_bvalid  =  bvalid           [28];
assign m28_axi_bready  =  bready           [28];
assign m28_axi_arvalid =  arvalid          [28];
assign arready[28]      =  m28_axi_arready    ;
assign m28_axi_araddr  =  {31'b0,araddr    [28]}; 
assign rvalid           [28] = m28_axi_rvalid  ;
assign m28_axi_rready  =  rready           [28];
assign rdata       [28] = m28_axi_rdata   ;
//assign  rlast      [28] = m28_axi_rlast   ;
assign m28_axi_awlen   =  8'b0;
assign m28_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;           
assign m28_axi_wlast   =  1'b1;
assign m28_axi_arlen   =  8'b0;
     
assign m29_axi_awvalid =  awvalid          [29];       
assign awready      [29]=  m29_axi_awready     ;       
assign m29_axi_awaddr  =  {31'b0,awaddr    [29]};      
assign m29_axi_wvalid  =  wvalid           [29];       
assign wready       [29]=  m29_axi_wready;       
assign m29_axi_wdata   =  wdata            [29];       
//assign m29_axi_bvalid  =  bvalid           [29];       
assign m29_axi_bready  =  bready           [29];       
assign m29_axi_arvalid =  arvalid          [29];       
assign arready      [29]=  m29_axi_arready     ;       
assign m29_axi_araddr  =  {31'b0,araddr    [29]};      
assign rvalid           [29] = m29_axi_rvalid  ;       
assign m29_axi_rready  =  rready           [29];       
assign rdata            [29] = m29_axi_rdata   ;       
//assign m29_axi_rlast   =  rlast            [29];       
assign m29_axi_awlen   =  8'b0;                       
assign m29_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m29_axi_wlast   =  1'b1;                       
assign m29_axi_arlen   =  8'b0;                         
  
assign m30_axi_awvalid =  awvalid          [30];             
assign awready      [30]=  m30_axi_awready     ;             
assign m30_axi_awaddr  =  {31'b0,awaddr    [30]};            
assign m30_axi_wvalid  =  wvalid           [30];             
assign  wready     [30] =  m30_axi_wready;             
assign m30_axi_wdata   =  wdata            [30];             
//assign m30_axi_bvalid  =  bvalid           [30];             
assign m30_axi_bready  =  bready           [30];             
assign m30_axi_arvalid =  arvalid          [30];             
assign arready     [30] =  m30_axi_arready;             
assign m30_axi_araddr  =  {31'b0,araddr    [30]};            
assign rvalid           [30]= m30_axi_rvalid  ;             
assign m30_axi_rready  =  rready           [30];             
assign rdata            [30] = m30_axi_rdata   ;             
//assign m30_axi_rlast   =  rlast            [30];             
assign m30_axi_awlen   =  8'b0;                             
assign m30_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;          
assign m30_axi_wlast   =  1'b1;                             
assign m30_axi_arlen   =  8'b0;                             
                
assign m31_axi_awvalid =  awvalid          [31];       
assign awready      [31]=    m31_axi_awready  ;       
assign m31_axi_awaddr  =  {31'b0,awaddr    [31]};      
assign m31_axi_wvalid  =  wvalid           [31];       
assign wready       [31]= m31_axi_wready    ;       
assign m31_axi_wdata   =  wdata            [31];       
//assign m31_axi_bvalid  =  bvalid           [31];       
assign m31_axi_bready  =  bready           [31];       
assign m31_axi_arvalid =  arvalid          [31];       
assign arready      [31]= m31_axi_arready     ;       
assign m31_axi_araddr  =  {31'b0,araddr    [31]};      
assign rvalid           [31]= m31_axi_rvalid  ;       
assign m31_axi_rready  =  rready           [31];       
assign rdata            [31] = m31_axi_rdata   ;      
//assign m31_axi_rlast   =  rlast            [31];       
assign m31_axi_awlen   =  8'b0;                                      
assign m31_axi_wstrb   =  64'hFFFF_FFFF_FFFF_FFFF;    
assign m31_axi_wlast   =  1'b1;                       
assign m31_axi_arlen   =  8'b0;            




endmodule
