///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// phalanx_tb.h -- Phalanx testbench definitions
//
// hard tabs=4

// Testbench commands
//`define Cmd [5:0]
//`define Cmd_IDLE	6'd0
//`define Cmd_0x0		6'd1
//`define Cmd_00y		6'd2
//`define Cmd_0xy		6'd3
//`define Cmd_E		6'd4
//`define Cmd_W		6'd5
//`define Cmd_S		6'd6
//`define Cmd_N		6'd7
//`define Cmd_SE		6'd8
//`define Cmd_SW		6'd9
//`define Cmd_NE		6'd10
//`define Cmd_NW		6'd11
//`define Cmd_MX		6'd12
//`define Cmd_MY		6'd13
//`define Cmd_MXY		6'd14
//`define Cmd_Axy		6'd15
//`define Cmd_Rot		6'd16
//`define Cmd_Id		6'd17
//`define Cmd_MCY_00	6'd18
//`define Cmd_MCY_10	6'd19
//`define Cmd_MCY_22	6'd20
//`define Cmd_MCY_32	6'd21
//`define Cmd_MCY		6'd22
//`define Cmd_MCY_1	6'd23
//`define Cmd_MCX_00	6'd24
//`define Cmd_MCX_01	6'd25
//`define Cmd_MCX_22	6'd26
//`define Cmd_MCX_23	6'd27
//`define Cmd_MCX		6'd28
//`define Cmd_MCX_1	6'd29
//`define Cmd_BC_00	6'd30
//`define Cmd_BC_22	6'd31
//`define Cmd_BC		6'd32

`define XY [X_MAX*Y_MAX-1:0] // stupid Verilog
`define XYMsg [X_MAX*Y_MAX*(3+D_W+X_W+Y_W)-1:0] // stupid Verilog
`define xy ((x) + (y)*X_MAX)
`define xy1 ((x) +((y+Y_MAX-1)%Y_MAX)*X_MAX)
`define xy1Msg ((x) +((y+Y_MAX-1)%Y_MAX)*X_MAX)*(3+D_W+X_W+Y_W)
`define xy2 (((x+X_MAX-1)%X_MAX)+(y)*X_MAX)

`define RANDOM		3'd0
//`define LOCAL		3'd1
`define BITREV		3'd2
`define TORNADO		3'd3
`define TRANSPOSE	3'd4
`define MEM         3'd5
// message fields
`define dl		[41:0]

`define LENGTH		32768
`define Slice [`Msg_W*(c+1)-1:`Msg_W*c]
`define XYC [X_MAX*Y_MAX*NUM_CHANNELS-1:0] // stupid Verilog
`define xyc ((x) + (y)*X_MAX)+(c*X_MAX*Y_MAX)
`define xyc1 ((x) + (y)*X_MAX)+(c1*X_MAX*Y_MAX)
