`define Y_INIT 32'hF0F0CCAA
`define X_INIT 32'hCCAACCAA

module XSwitch #(
	parameter	W		= 1
) (
	input wire	[1:0]	sel,
	input wire	[W-1:0] yi,
	input wire	[W-1:0] xi,
	input wire	[W-1:0] i,
	output wire [W-1:0] y,
	output wire [W-1:0] x);

	genvar j;
	generate
		// Vivado RLOCs support is quite problematic and best avoided.
		for (j = 0; j < W; j = j + 1) begin : e
			LUT6_2 #(.INIT({`Y_INIT,`X_INIT})) mux(.I0(i[j]), .I1(xi[j]), .I2(yi[j]),
					 .I3(sel[0]), .I4(sel[1]), .I5(1'b1), .O5(x[j]), .O6(y[j]));
		end
	endgenerate
endmodule

