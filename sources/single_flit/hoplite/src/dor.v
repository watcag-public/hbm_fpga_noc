///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// dor.v -- Dimension order routing (with multicast) function
//
// hard tabs=4

`include "phalanx.h"

/*
Multicast supports regular message sends to a specific dest (x,y),
and also supports multicast message sends from any agent (x0,y0)
to multiple wildcard agents.

Besides regular point-to-point message sends, there are three forms
of multicast, for different header encodings:
* `M(v:1, mx:0, my:0, x,  y,  ...) -- regular message to (x,y)
* `M(v:1, mx:0, my:1, x,  y0, ...) -- "column multicast" to all (x,*)
* `M(v:1, mx:1, my:0, x0, y,  ...) -- "row multicast" to all (*,y)
* `M(v:1, mx:1, my:1, x0, y0, ...) -- "broadast" to all (*,*)

Any agent (x0,y0) can initiate a multicast to all agents
or to particular rows or columns of agents.

Note that for x (and/or y) dimension multicast, the corresponding
message x (resp. y) fields are the source x0 (y0) not dest x (y).
For multicast these fields are used by the routing function to
determine when to terminate propagation of the multicast message.

----------
Column Multicast

The simplest multicast is a column multicast to all agents (x,*).

The base DOR routing policy (X, then Y), ensures that a southbound
(Y ring) message has highest priority, and is necessarily delivered
to any (*,y) in at most Y_MAX cycles.

Similarly a column multicast message on a Y ring wins all south
output port contentions and can traverse every router in a column
in at most Y_MAX cycles.

When a column multicast message reaches its dest X router and
successfully arbitrates for the south output port, it is output
from router at (x,y0) as well as propagated to the next southerly
router at (x,(y0+1)%Y_MAX). From that point on the southbound column
multicast message wins every southbound arbitration, so this
output-and-retransmit-column multicast repeats until the next router
would be (x,y0) again, at which point the column multicast is
complete.

----------
Row Multicast

Row multicast would be quite simple but for two issues.

1. Output port contention: There is no guarantee that a message
from the west can be output upon reaching its destination (x,y)
because the Hoplite datapath shares the south port and the output
port to save mux area. Any southbound or output message from the
north has priority over a message from the west that has reached
its destination -- the west message deflects east to try again
later.

In an unloaded network a row multicast delivery takes as few as
X_MAX cycles, but in a loaded network, deflections can require
multiple cycles around the X ring.

2. Strict dimension order routing. The design tenet that any agent
(x0,y0) can row multicast on any row means a naive implementation,
e.g. send row multicast message from (x0,y0) to (x0,y) then (x0+1,y),
etc., cannot work because the message from (x0,y0) to (x0,y) would
traverse the Y ring, arriving from the north, and by definition,
it cannot make the turn to the eastbound X ring.  Even if the router
crossbar was enhanced to enable N->E routes, there may be at the
same router an incoming message from the west port. Indeed, the
particular X ring may be saturated with messages all waiting to
enter the Y ring, so the row multicast message on the Y ring can't
enter the X ring. Deadlock.

Therefore, row multicast from (x0,y0) to an arbitrary row (*,y)
must traverse the X ring first, and only then, the Y ring to route
from (*,y0) to (*,y).

So in a 4x4 torus a row multicast from (1,1) to (*,0) traverses
* (1,1) injecting a S message to (1,0) via (1,2) (1,3) (1,0)
* (2,1) injecting a S message to (2,0) via (2,2) (2,3) (2,0)
* (3,1) injecting a S message to (3,0) via (3,2) (3,3) (3,0)
* (0,1) injecting a S message to (0,0) via (0,2) (0,3) (0,0)

A row multicast *to the same row* is much simpler with a latency
of X_MAX cycles on an unloaded NOC.

To handle south channel busy deflection yet ensure eventual delivery
along every x's Y ring, additional state ("nx" -- next x) must be
added to each W input, used as an induction variable to track the
next pending row multicast x in (*,y).  When a row multicast message
to (x,y0) is successfully output to Y ring x, the next router on
the X ring x'= (x+1)%X_MAX. In general, upon delivery to (x,y), it
is retransmitted eastbound as a row multicast to (x+1,y), until
the next router would be (x0,y).

On the other hand, if a row multicast message at (x,y0) cannot yet
head south to (x,y), it deflects east, which traverses the entire
X ring to retry at (x0,y) after X_MAX more cycles.

----------
Broadcast -- all columns, all rows multicast

Broadcast to all agents is a special kind of row multicast that
multicasts column multicasts into each Y ring. As with row multicast,
under NOC load, deflections may incur multiple trips around an
X ring to complete delivery of all column (Y ring) multicasts.

*/

///////////////////////////////////////////////////////////////////////////////
// DOR -- dimension ordered routing (with multicast) function
//
module DOR #(
	parameter MCAST = 1,	// multicast?
	parameter XBAR	= 1,	// crossbar?
    parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data payload width
	parameter X_W	= 2,	// X address width
	parameter Y_W	= 2,	// Y address width
	parameter X_MAX	= 1<<X_W,
	parameter Y_MAX	= 1<<Y_W,
	parameter X		= 0,	// X address of this node
	parameter Y		= 0		// Y address of this node
) (
	input  wire `Msg n,		// input message from north, registered
	input  wire `Msg w,		// input message from west,  registered
	input  wire `Msg i,		// input message, registered
	input  wire `X nx,		// input X multicast next x
	output `com `Rt r,		// output route
	output `com `X nx_out,	// output X multicast next x
	input wire i_busy
);
	`com s_busy;
	`com e_busy;
	`com xEq;
    `com yEqN;
	// route N->S(O)
	task NtoS; begin s_busy = 1; r`s_sel = `N; r`s_v = 1; end endtask
	task NtoO; begin s_busy = 1; r`o_sel = `NO; r`s_sel= `N; r`o_v = 1; end endtask

	// route W->E (sometimes for deflection)
	task WtoE; begin e_busy = 1; r`e_sel = `W; r`e_v = 1; end endtask

	// route W->S(O)
	task WtoS; begin
		s_busy = 1; r`s_sel = `W; r`s_v = 1;
		if (!XBAR) begin e_busy = 1; r`e_sel = `W; end
	end endtask
	task WtoO; begin s_busy = 1; r`o_sel = `WO; r`s_sel=`W; r`o_v = 1; end endtask


	// route I->E
	task ItoE; begin e_busy = 1; r`e_sel = `I; r`e_v = 1; r`i_ack = 1; end endtask

	// route I->S(O)
	task ItoS; begin
		s_busy = 1; r`s_sel = `I; r`s_v = 1; r`i_ack = 1;
		if (!XBAR) begin e_busy = 1; r`e_sel = `I; end
	end endtask

	// A message was routed to south/output. Set its output and south
	// valid flags, depending upon its Y multicast mode.
	task SvOv(input `Msg m); begin
		// Propagate msg S if more multicast, or not yet at dest.
		r`s_v = `_my(m) ? (`NEXTY != m`y) : (m`y != Y);
		// Output msg if multicast or at dest.
		r`o_v = `_my(m) ? 1'b1            : (m`y == Y);
	end endtask

	// Determine where to send each message N/W/I,
	// whether there is a valid output message,
	// and whether to accept the input message.
	always @* begin
		r = 0;
		s_busy = 0;
		e_busy = 0;
		nx_out = 0;

		// route N input -- by DOR it is already on the right column X
		if (n`v) begin
		  if((n`y == Y) && !i_busy) begin
		   NtoO();
		  end
		  else begin
			NtoS();		
		  end						// continue N->S(O)
		end

		// route W input
		xEq = `_mx(w) ? (nx == X) : (w`x == X);
		if (w`v) begin
			nx_out = nx;						// default: propagate current multicast X next x
//			if (xEq && !s_busy) begin			// at dest X, turn W->S to output if S avail
//				WtoS();
//				SvOv(w);
//				if (`_mx(w) && `NEXTX != w`x) begin // propagate until return to src x
//					WtoE();
//					nx_out = `NEXTX;			// advance to next pending x
//				end
//			end
//			else begin
//				WtoE();							// continue/deflect W->E
//			end
            if( (w`x == X) && (w`y == Y) && !i_busy && !s_busy) begin
                WtoO();
            end
            else if( (w`x == X) && (w`y == Y) && ((!i_busy && s_busy) || i_busy)) begin
                WtoE();
            end
            else if( (w`x == X) && !s_busy ) begin
                WtoS();
            end
            else begin
                WtoE();
            end
		end

		// route local input
		r`i_ack = 0;							// by default, input stalls
		if (i`v) begin
			// If multicast on X (and/or Y), i`x (and/or i`y) must be src X,Y:
			// assert((!`_mx(i) || i`x == X) && (!`_my(i) || i`y == Y));
			if (`_mx(i)) begin					 // X multicast: inject I->S(O),E
				if (!e_busy && !s_busy) begin
					ItoS();
					SvOv(i);
					ItoE();
					r`e_v = `NEXTX != X;
					nx_out = `NEXTX;
				end
			end
			else if (i`x == X) begin			// already at dest X, inject I->S(O)
				if (!s_busy) begin
					ItoS();
				end
				else if(!e_busy) begin
				    ItoE();
				end
			end
			else if (!e_busy) begin
				ItoE();						 	// not yet at dest X, inject I->E
			end
		end
	end
endmodule