
`define LOCAL   1
`define ROOT    0

// `define MEM_TEST
//`define DEBUG
//`define DUMP 

// Testbench commands
`define Cmd [19:0]

`define rate [19:12]

`define access_type [11]

`define linear 1'b0
`define random 1'b1

`define operation [10:9]

`define WR 2'b00
`define RO 2'b01
`define W_O 2'b10
`define R_W 2'b11

`define channel [8:6]

`define p2p 3'b000
`define cb  3'b001
`define cs  3'b010
`define br  3'b011 
`define to  3'b100
`define nn  3'b101
`define cc  3'b110

`define sigma [5:0]