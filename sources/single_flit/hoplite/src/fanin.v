`include "phalanx.h"

module Fanin #(
	parameter NUM_CHANNELS = 1, // choose how many Hoplites to try
	parameter X_W	= 4,	// X address width
	parameter Y_W	= 4,	// Y address width
	parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width
	parameter X_MAX	= 1<<X_W,
	parameter Y_MAX	= 1<<Y_W,
	parameter X	= 3,	// X address of this node
	parameter Y	= 2		// Y address of this node
) (
	input wire clk,
	input wire rst,
	output wire `Msg o,	// output message to client
	output wire o_v,	// valid to client
	input wire `MsgChan oo,	// input message to client
	input wire [NUM_CHANNELS-1:0] oo_v,	// output valid from hoplite
	output wire [NUM_CHANNELS-1:0] ii_busy, // client refusing delivery
	input wire done_all
);

/*	reg [$clog2(NUM_CHANNELS):0] choice;
	always @(posedge clk) begin
		if(rst) begin
			choice <= 0;
		end else begin
			choice <= (choice+1)%NUM_CHANNELS;
		end
	end
*/

	reg [$clog2(NUM_CHANNELS):0] choice=0;
  	integer k;
  	always @(*) begin
	  choice = 0; 
	  for (k=NUM_CHANNELS-1; k>=0; k=k-1) begin
		  if (oo_v[k]) begin
			  choice = k;
		  end
	  end
  	end
	wire `Msg bundle [NUM_CHANNELS-1:0];
//	assign ii_busy[0] = 0; // first channel always has higher priority
	assign bundle[0] = oo[`Msg_W-1:0]; // first channel weirdness
    wire oops;
    assign  oops = |oo_v;
	genvar i;
	generate 
	   for(i=0;i<NUM_CHANNELS;i = i+1) begin 
	      assign ii_busy[i] = (oops && ((choice) != i))? oo_v[i]&1:0;
	   end
	endgenerate
	generate 
		for (i = 0; i < NUM_CHANNELS-1; i = i + 1) begin : is			
//			assign ii_busy[i+1] = oo_v[i] | ii_busy[i];
			assign bundle[i+1] = oo[`Msg_W*(i+2)-1:`Msg_W*(i+1)];
		end
	endgenerate
	 
	assign o = bundle[choice];
	assign o_v = oo_v[choice];

	`ifndef SYNTHESIS
	integer packets_recv, packets_sent;
	reg displayed;
	always @(posedge clk) begin
		if(rst) begin
			packets_recv <= 0;
			packets_sent <= 0;
			displayed <=0;
		end else begin
			if(oo_v!=0) begin
				packets_recv <= packets_recv + 1;
			end
			if(o_v) begin
				packets_sent <= packets_sent + 1;
			end
			if(done_all & !displayed) begin
				displayed <= 1;
				//`ifdef DEBUG
				$display("Sanity, Fanin(%0d,%0d), recv=%0d, sent=%0d",X,Y,packets_recv,packets_sent);
				//`endif
			end
		end
	end
	`endif

endmodule

