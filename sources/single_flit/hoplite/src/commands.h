`include "system.h"

////`define UNIT_TEST_REACHABILITY2
////`define UNIT_TEST_REACHABILITY4
////`define UNIT_TEST_TRIANGLE_T
////`define UNIT_TEST_TRIANGLE_PI
////`define MEM_WRITE

//`define LOCAL   1
//`define ROOT    0

//// `define MEM_TEST
////`define DEBUG
////`define DUMP 

//// Testbench commands
//`define Cmd [19:0]

//`define rate [19:12]

//`define access_type [11]

//`define linear 1'b0
//`define random 1'b1

//`define operation [10:9]

//`define WR 2'b00
//`define RO 2'b01
//`define WO 2'b10
//`define RW 2'b11

//`define channel [8:6]

//`define p2p 3'b000
//`define cb  3'b001
//`define cs  3'b010
//`define br  3'b011 
//`define to  3'b100
//`define nn  3'b101
//`define cc  3'b110

//`define sigma [5:0]

////`define Cmd_IDLE 6'd0
////`define Cmd_01   6'd1
////`define Cmd_02   6'd2
////`define Cmd_03   6'd3
////`define Cmd_10   6'd4
////`define Cmd_12   6'd5
////`define Cmd_13   6'd6
////`define Cmd_20   6'd7
////`define Cmd_21   6'd8
////`define Cmd_23   6'd9
////`define Cmd_30   6'd10
////`define Cmd_31   6'd11
////`define Cmd_32   6'd12
////// deflection tests start here
////`define Cmd_02_12     6'd13
////`define Cmd_01_12     6'd14
////`define Cmd_40_70     6'd15
////`define Cmd_02_25     6'd16
////`define Cmd_swap      6'd17
////// randomized stress test
////`define Cmd_RND       6'd18
////`define Cmd_MEM_W     6'd19
////`define Cmd_MEM_R     6'd20

//`define RANDOM 0
////`define LOCAL 1
////`define BITREV 2
////`define TORNADO 3

////`define sixteen_cross_sixteen
////`define addr_rand
