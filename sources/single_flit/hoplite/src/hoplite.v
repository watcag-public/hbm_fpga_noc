///////////////////////////////////////////////////////////////////////////////
// Phalanx/Hoplite FPGA-optimized 2D torus NOC/router
// Copyright (C) 2015, Gray Research LLC. All rights reserved.
//
// hoplite.v -- Hoplite austere unidirectional 2-D torus deflection router
//
// hard tabs=4

`include "phalanx.h"

///////////////////////////////////////////////////////////////////////////////
// Hoplite -- austere unidirectional 2-D torus deflection router
//
module Hoplite #(
	parameter ROUTE = "DOR",// routing policy; default: dimension ordered
	parameter LOWPOWER = 0,	// optimize for low power
	parameter MCAST	= 0,	// multicast?
	parameter XBAR	= 0,	// crossbar?
	parameter X_W	= 4,	// X address width
	parameter Y_W	= 4,	// Y address width
	parameter P_W       = 256,
	parameter M_A_W     = 33,
	parameter D_W		= 1+P_W+M_A_W+X_W+Y_W,	// data width
	parameter X_MAX	= 1<<X_W,
	parameter Y_MAX	= 1<<Y_W,
	parameter X	= 3,	// X address of this node
	parameter Y	= 2,	// Y address of this node
	parameter CHAN = 1
) (
	input  wire clk,		// clock
	input  wire rst,		// reset
	input  wire ce,			// clock enable
	input  wire `Msg n_in,	// input message from north
	input  wire `Msg w_in,	// input message from west
	input  wire `Msg i,		// input message, registered, validated by i`v
	input  wire `X nx_in,	// input multicast X next x
	output wire i_ack,		// input accepted this cycle
	output wire o_v,		// s_out is valid output message for this node
	output wire `Msg s_out,	// southbound output message
	output wire `Msg e_out,	// eastbound output message
	output  wire `X nx_out,	// input multicast X next x
	output wire done,	// no message being processed
	input wire done_all,	// no message being processed
	input wire i_busy
);

	(* shreg_extract = "false" *) reg  `Msg n=0;			// north input message register
	(* shreg_extract = "false" *)reg  `Msg n_p1=0,n_p0=0;
	reg  `Msg w=0;			// west input message register
	reg  `X nx=0;				// X multicast next x register
	wire `Rt r;				// router decision

	reg  `Msg e_out_r=0, e_out_r1=0;
	reg  `Msg s_out_r=0, s_out_r1=0;
	wire `Msg e_out_c, s_out_c ; 
	wire o_v_c;
	reg o_v_r=0, o_v_r1=0;
	// Register north and west input messages: v (valid bit) and xv (the rest).
	always @(posedge clk) begin
		// delay outputs by DELAY cycles
//		if(rst) begin
//			e_out_r <= 0;
//			s_out_r <= 0;
//			e_out_r1 <= 0;
//			s_out_r1 <= 0;
//			o_v_r <= 0;
//			o_v_r1 <= 0;
//		end else begin
			e_out_r <= e_out_c;
			s_out_r <= s_out_c;
			
			e_out_r1 <= e_out_r;
			s_out_r1 <= s_out_r;
			
			o_v_r <= o_v_c;
			o_v_r1 <= o_v_r;
//		end
	
//		if (rst) begin
//			n`v <= 0;
//			w`v <= 0;
//		end
//		else 
//		if (ce) begin
            n_p0 <= n_in;
            n_p1 <= n_p0;
			n`v <= n_p1`v;
			w`v <= w_in`v;
//		end

		// Register rest of input messages.
		// LOWPOWER: don't register an input message unless it is valid.
		// This is a parameter because this power optimization sometimes
		// becomes a critical path / impacts clock period.
		//
//		if (ce && (!LOWPOWER || n_in`v)) begin
			n`xv <= n_p1`xv;
//		end
//		if (ce && (!LOWPOWER || w_in`v)) begin
			w`xv <= w_in`xv;
//		end
		// Register X multicast next x state
//		if (rst) begin
//			nx <= 0;
//		end
//		else
//		 if (ce) begin
			nx <= nx_in;
//		end
	end

	generate if (ROUTE == "DOR") begin : D
		DOR #( .XBAR(XBAR), .P_W(P_W) ,.M_A_W(M_A_W)  ,.D_W(D_W), .X_W(X_W), .Y_W(Y_W), .X_MAX(X_MAX), .Y_MAX(Y_MAX), .X(X), .Y(Y))
		    dor(.n(n), .w(w),.nx(nx), .nx_out(nx_out), .i(i), .r(r),.i_busy(i_busy));
	end endgenerate

	assign i_ack   = r`i_ack;
	assign o_v_c     = r`o_v;
	assign s_out_c`v = r`s_v;
	assign e_out_c`v = r`e_v;

	assign s_out = s_out_r;
	assign e_out = e_out_r;
	assign o_v = o_v_r;

	// Altera's fracturable ALM 6-LUT can implement two 3-1 muxes/ALM, e.g. a 3-2 crossbar
	generate if (XBAR) begin : Alt
		Mux2 #(.W(`Msg_W-1)) e_mux(.s(r`e_sel_w), .i0(i`xv), .i1(w`xv), .o(e_out_c`xv));
		Mux3 #(.W(`Msg_W-1)) s_mux(.s(r`s_sel), .i0(i`xv), .i1(w`xv), .i2(n`xv), .o(s_out_c`xv));
	end endgenerate

	// Xilinx's fracturable 6-LUT can implement two 2-1 muxes/6-LUT
	generate if (!XBAR) begin : Xil
		XSwitch #(.W(`Msg_W-1)) muxes(
			.sel({r`e_sel_w,r`s_sel_n}),
			.yi(n`xv),.xi(w`xv),.i(i`xv),
			.y(s_out_c`xv),.x(e_out_c`xv));
//		Mux2 #(.W(`Msg_W-1)) e_mux(.s(r`e_sel_w), .i0(i`xv),     .i1(w`xv), .o(e_out_c`xv));
//		Mux2 #(.W(`Msg_W-1)) s_mux(.s(r`s_sel_n), .i0(e_out`xv), .i1(n`xv), .o(s_out_c`xv));
	end endgenerate

	// generate done signal for hoplite
	assign done = !(n_in`v || w_in`v || i`v || o_v);

	// help compute statistics..
//	integer packets_recv, packets_sent;
//	reg displayed;

//	always@(posedge clk) begin

//		if(rst) begin
//			packets_recv <= 0;
//			packets_sent <= 0;
//			displayed <= 0;
//		end else begin
//			if(n_in`v | w_in`v | (i`v & !i_ack))
//			begin
//				packets_recv <= packets_recv + (n_in`v+w_in`v+(i`v & !i_ack));
//			end
//			if(o_v | e_out`v | s_out`v) begin
//				packets_sent <= packets_sent + (o_v + e_out`v + s_out`v);
//			end
//			if(done_all & !displayed) begin
//			//if(done & !displayed) begin
//				displayed <= 1;
//				$display("Sanity, Hoplite(%0d,%0d), recv=%0d, sent=%0d",X,Y,packets_recv,packets_sent);
//			end
//		end
//	end

endmodule



module Mux2 #(
	parameter W = 1
) (
	(* keep *) input wire s,
	input wire [W-1:0] i0,
	input wire [W-1:0] i1,
	output wire [W-1:0] o
);
	assign o = s ? i1 : i0;
endmodule


module Mux3 #(
	parameter W = 1
) (
	input wire [1:0] s,
	input wire [W-1:0] i0,
	input wire [W-1:0] i1,
	input wire [W-1:0] i2,
	output wire [W-1:0] o
);
	assign o = s[1] ? i2 : s[0] ? i1 : i0;
endmodule
