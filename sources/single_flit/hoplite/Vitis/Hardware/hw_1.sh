#!/bin/bash
wd=`pwd`
echo "read_xdc $wd/floorplan.xdc" > pre_placement.tcl
$VITIS/2019.2/bin/v++ --target hw --link  --kernel_frequency 500 --remote_ip_cache ~/workspace/ip_cache --vivado.prop run.impl_1.STEPS.PLACE_DESIGN.TCL.PRE=$wd/pre_placement.tcl  --vivado.prop run.impl_1.STEPS.PLACE_DESIGN.TCL.POST=$wd/post_placement.tcl --vivado.prop run.impl_1.STEPS.ROUTE_DESIGN.TCL.POST=$wd/post_routing.tcl --config common-config.ini  --save-temps -R2 --vivado.param project.writeIntermediateCheckpoints=1 --config binary_container_1-link.ini -o binary_container_1.xclbin ../src/hoplite_hbm.xo
rm pre_placement.tcl