#set_param general.maxThreads

phys_opt_design -directive AggressiveExplore
phys_opt_design -slr_crossing_opt -tns_cleanup

phys_opt_design -retime
set_param route.enableGlobalHoldIter 1 
