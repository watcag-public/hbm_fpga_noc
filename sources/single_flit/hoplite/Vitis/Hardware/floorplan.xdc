#fifth row
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0] [get_cells -hier -filter {NAME =~ *inst_control_s_axi*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[0]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[7]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[1]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[6]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[2]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[5]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[3]*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[4]*}]

#first_row
create_pblock x0y0
resize_pblock x0y0 -add {CLOCKREGION_X0Y4:CLOCKREGION_X0Y5}
add_cells_to_pblock [get_pblocks x0y0]  [get_cells -hier -filter {NAME =~ *ys[0].xs[0]*}]

create_pblock x0y7
resize_pblock x0y7 -add {CLOCKREGION_X1Y4:CLOCKREGION_X1Y5}
add_cells_to_pblock [get_pblocks x0y7]  [get_cells -hier -filter {NAME =~ *ys[0].xs[7]*}]

create_pblock x0y1_x0y6
resize_pblock x0y1_x0y6 -add {CLOCKREGION_X2Y4:CLOCKREGION_X2Y5}
add_cells_to_pblock [get_pblocks x0y1_x0y6]  [get_cells -hier -filter {NAME =~ *ys[0].xs[1]*}]
add_cells_to_pblock [get_pblocks x0y1_x0y6]  [get_cells -hier -filter {NAME =~ *ys[0].xs[6]*}]


create_pblock x0y2
resize_pblock x0y2 -add {CLOCKREGION_X3Y4:CLOCKREGION_X3Y5}
add_cells_to_pblock [get_pblocks x0y2]  [get_cells -hier -filter {NAME =~ *ys[0].xs[2]*}]

create_pblock x0y5
resize_pblock x0y5 -add {CLOCKREGION_X4Y4:CLOCKREGION_X4Y5}
add_cells_to_pblock [get_pblocks x0y5]  [get_cells -hier -filter {NAME =~ *ys[0].xs[5]*}]

create_pblock x0y3
resize_pblock x0y3 -add {CLOCKREGION_X5Y4:CLOCKREGION_X5Y5}
add_cells_to_pblock [get_pblocks x0y3]  [get_cells -hier -filter {NAME =~ *ys[0].xs[3]*}]

create_pblock x0y4
resize_pblock x0y4 -add {CLOCKREGION_X6Y4:CLOCKREGION_X6Y5}
add_cells_to_pblock [get_pblocks x0y4]  [get_cells -hier -filter {NAME =~ *ys[0].xs[4]*}]

#fourth_row
create_pblock x3y0
resize_pblock x3y0 -add {CLOCKREGION_X0Y6:CLOCKREGION_X0Y7}
add_cells_to_pblock [get_pblocks x3y0]  [get_cells -hier -filter {NAME =~ *ys[3].xs[0]*}]

create_pblock x3y7
resize_pblock x3y7 -add {CLOCKREGION_X1Y6:CLOCKREGION_X1Y7}
add_cells_to_pblock [get_pblocks x3y7]  [get_cells -hier -filter {NAME =~ *ys[3].xs[7]*}]

create_pblock x3y1_x3y6
resize_pblock x3y1_x3y6 -add {CLOCKREGION_X2Y6:CLOCKREGION_X2Y7}
add_cells_to_pblock [get_pblocks x3y1_x3y6]  [get_cells -hier -filter {NAME =~ *ys[3].xs[1]*}]
add_cells_to_pblock [get_pblocks x3y1_x3y6]  [get_cells -hier -filter {NAME =~ *ys[3].xs[6]*}]

create_pblock x3y2
resize_pblock x3y2 -add {CLOCKREGION_X3Y6:CLOCKREGION_X3Y7}
add_cells_to_pblock [get_pblocks x3y2]  [get_cells -hier -filter {NAME =~ *ys[3].xs[2]*}]

create_pblock x3y5
resize_pblock x3y5 -add {CLOCKREGION_X4Y6:CLOCKREGION_X4Y7}
add_cells_to_pblock [get_pblocks x3y5]  [get_cells -hier -filter {NAME =~ *ys[3].xs[5]*}]

create_pblock x3y3
resize_pblock x3y3 -add {CLOCKREGION_X5Y6:CLOCKREGION_X5Y7}
add_cells_to_pblock [get_pblocks x3y3]  [get_cells -hier -filter {NAME =~ *ys[3].xs[3]*}]

create_pblock x3y4
resize_pblock x3y4 -add {CLOCKREGION_X6Y6:CLOCKREGION_X6Y7}
add_cells_to_pblock [get_pblocks x3y4]  [get_cells -hier -filter {NAME =~ *ys[3].xs[4]*}]


#second_row
create_pblock x1y0
resize_pblock x1y0 -add {CLOCKREGION_X0Y8:CLOCKREGION_X0Y9}
add_cells_to_pblock [get_pblocks x1y0]  [get_cells -hier -filter {NAME =~ *ys[1].xs[0]*}]

create_pblock x1y7
resize_pblock x1y7 -add {CLOCKREGION_X1Y8:CLOCKREGION_X1Y9}
add_cells_to_pblock [get_pblocks x1y7]  [get_cells -hier -filter {NAME =~ *ys[1].xs[7]*}]

create_pblock x1y1_x1y6
resize_pblock x1y1_x1y6 -add {CLOCKREGION_X2Y8:CLOCKREGION_X2Y9}
add_cells_to_pblock [get_pblocks x1y1_x1y6]  [get_cells -hier -filter {NAME =~ *ys[1].xs[1]*}]
add_cells_to_pblock [get_pblocks x1y1_x1y6]  [get_cells -hier -filter {NAME =~ *ys[1].xs[6]*}]

create_pblock x1y2
resize_pblock x1y2 -add {CLOCKREGION_X3Y8:CLOCKREGION_X3Y9}
add_cells_to_pblock [get_pblocks x1y2]  [get_cells -hier -filter {NAME =~ *ys[1].xs[2]*}]

create_pblock x1y5
resize_pblock x1y5 -add {CLOCKREGION_X4Y8:CLOCKREGION_X4Y9}
add_cells_to_pblock [get_pblocks x1y5]  [get_cells -hier -filter {NAME =~ *ys[1].xs[5]*}]

create_pblock x1y3
resize_pblock x1y3 -add {CLOCKREGION_X5Y8:CLOCKREGION_X5Y9}
add_cells_to_pblock [get_pblocks x1y3]  [get_cells -hier -filter {NAME =~ *ys[1].xs[3]*}]

create_pblock x1y4
resize_pblock x1y4 -add {CLOCKREGION_X6Y8:CLOCKREGION_X6Y9}
add_cells_to_pblock [get_pblocks x1y4]  [get_cells -hier -filter {NAME =~ *ys[1].xs[4]*}]

#third_row
create_pblock x2y0
resize_pblock x2y0 -add {CLOCKREGION_X0Y10:CLOCKREGION_X0Y11}
add_cells_to_pblock [get_pblocks x2y0]  [get_cells -hier -filter {NAME =~ *ys[2].xs[0]*}]

create_pblock x2y7
resize_pblock x2y7 -add {CLOCKREGION_X1Y10:CLOCKREGION_X1Y11}
add_cells_to_pblock [get_pblocks x2y7]  [get_cells -hier -filter {NAME =~ *ys[2].xs[7]*}]

create_pblock x2y1_x2y6
resize_pblock x2y1_x2y6 -add {CLOCKREGION_X2Y10:CLOCKREGION_X2Y11}
add_cells_to_pblock [get_pblocks x2y1_x2y6]  [get_cells -hier -filter {NAME =~ *ys[2].xs[1]*}]
add_cells_to_pblock [get_pblocks x2y1_x2y6]  [get_cells -hier -filter {NAME =~ *ys[2].xs[6]*}]

create_pblock x2y2
resize_pblock x2y2 -add {CLOCKREGION_X3Y10:CLOCKREGION_X3Y11}
add_cells_to_pblock [get_pblocks x2y2]  [get_cells -hier -filter {NAME =~ *ys[2].xs[2]*}]

create_pblock x2y5
resize_pblock x2y5 -add {CLOCKREGION_X4Y10:CLOCKREGION_X4Y11}
add_cells_to_pblock [get_pblocks x2y5]  [get_cells -hier -filter {NAME =~ *ys[2].xs[5]*}]

create_pblock x2y3
resize_pblock x2y3 -add {CLOCKREGION_X5Y10:CLOCKREGION_X5Y11}
add_cells_to_pblock [get_pblocks x2y3]  [get_cells -hier -filter {NAME =~ *ys[2].xs[3]*}]

create_pblock x2y4
resize_pblock x2y4 -add {CLOCKREGION_X6Y10:CLOCKREGION_X6Y11}
add_cells_to_pblock [get_pblocks x2y4]  [get_cells -hier -filter {NAME =~ *ys[2].xs[4]*}]


add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[*].cs[*].hop/n_p0*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys_hbm[4].xs[*].cs[*].hop/n_p1*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR0]  [get_cells -hier -filter {NAME =~ *ys[0].xs[*].cs[*].hop/n_p0*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -filter {NAME =~ *ys[0].xs[*].cs[*].hop/n_p1*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -filter {NAME =~ *ys[1].xs[*].cs[*].hop/n_p0*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -filter {NAME =~ *ys[1].xs[*].cs[*].hop/n_p1*}]

add_cells_to_pblock [get_pblocks pblock_dynamic_SLR2]  [get_cells -hier -filter {NAME =~ *ys[3].xs[*].cs[*].hop/n_p0*}]
add_cells_to_pblock [get_pblocks pblock_dynamic_SLR1]  [get_cells -hier -filter {NAME =~ *ys[3].xs[*].cs[*].hop/n_p1*}]


# set_property USER_SLL_REG FALSE [get_cells -hier -filter {NAME =~ *ys[0].xs[*].cs[*].hop/n_reg*}]
# set_property USER_SLL_REG FALSE [get_cells -hier -filter {NAME =~ *ys[0].xs[*].cs[*].hop/s_out_r_reg*}]

# set_property USER_SLL_REG FALSE [get_cells -hier -filter {NAME =~ *ys[2].xs[*].cs[*].hop/n_reg*}]
# set_property USER_SLL_REG FALSE [get_cells -hier -filter {NAME =~ *ys[2].xs[*].cs[*].hop/s_out_r_reg*}]

# set_property USER_SLL_REG FALSE [get_cells -hier -filter {NAME =~ *ys[3].xs[*].cs[*].hop/n_reg*}]
# set_property USER_SLL_REG FALSE [get_cells -hier -filter {NAME =~ *ys[3].xs[*].cs[*].hop/s_out_r_reg*}]




