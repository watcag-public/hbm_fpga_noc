// This is a generated file. Use and modify at your own risk.
////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
Vendor: Xilinx
Associated Filename: main.c
#Purpose: This example shows a basic vector add +1 (constant) by manipulating
#         memory inplace.
*******************************************************************************/

#include <fcntl.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <CL/opencl.h>
#include <CL/cl_ext.h>
#include "xclhal2.h"

////////////////////////////////////////////////////////////////////////////////

#define NUM_WORKGROUPS (1)
#define WORKGROUP_SIZE (256)
#define MAX_LENGTH 8192
#define MEM_ALIGNMENT 4096
#if defined(VITIS_PLATFORM) && !defined(TARGET_DEVICE)
#define STR_VALUE(arg)      #arg
#define GET_STRING(name) STR_VALUE(name)
#define TARGET_DEVICE GET_STRING(VITIS_PLATFORM)
#endif

////////////////////////////////////////////////////////////////////////////////

cl_uint load_file_to_memory(const char *filename, char **result)
{
    cl_uint size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL) {
        *result = NULL;
        return -1; // -1 means file opening fail
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *)malloc(size+1);
    if (size != fread(*result, sizeof(char), size, f)) {
        free(*result);
        return -2; // -2 means file reading fail
    }
    fclose(f);
    (*result)[size] = 0;
    return size;
}

int main(int argc, char** argv)
{

    cl_int err;                            // error code returned from api calls
    cl_uint check_status = 0;
    const cl_uint number_of_words = 4096; // 16KB of data


    cl_platform_id platform_id;         // platform id
    cl_device_id device_id;             // compute device id
    cl_context context;                 // compute context
    cl_command_queue commands;          // compute command queue
    cl_program program;                 // compute programs
    cl_kernel kernel;                   // compute kernel

    cl_uint* h_data;                                // host memory for input vector
    char cl_platform_vendor[1001];
    char target_device_name[1001] = TARGET_DEVICE;

    cl_uint* h_A_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_A;                         // device memory used for a vector

    cl_uint* h_B_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_B;                         // device memory used for a vector

    cl_uint* h_C_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_C;                         // device memory used for a vector

    cl_uint* h_D_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_D;                         // device memory used for a vector

    cl_uint* h_E_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_E;                         // device memory used for a vector

    cl_uint* h_F_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_F;                         // device memory used for a vector

    cl_uint* h_G_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_G;                         // device memory used for a vector

    cl_uint* h_H_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_H;                         // device memory used for a vector

    cl_uint* h_I_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_I;                         // device memory used for a vector

    cl_uint* h_J_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_J;                         // device memory used for a vector

    cl_uint* h_K_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_K;                         // device memory used for a vector

    cl_uint* h_L_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_L;                         // device memory used for a vector

    cl_uint* h_M_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_M;                         // device memory used for a vector

    cl_uint* h_N_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_N;                         // device memory used for a vector

    cl_uint* h_O_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_O;                         // device memory used for a vector

    cl_uint* h_P_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_P;                         // device memory used for a vector

    cl_uint* h_Q_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_Q;                         // device memory used for a vector

    cl_uint* h_R_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_R;                         // device memory used for a vector

    cl_uint* h_S_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_S;                         // device memory used for a vector

    cl_uint* h_T_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_T;                         // device memory used for a vector

    cl_uint* h_U_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_U;                         // device memory used for a vector

    cl_uint* h_V_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_V;                         // device memory used for a vector

    cl_uint* h_W_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_W;                         // device memory used for a vector

    cl_uint* h_X_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_X;                         // device memory used for a vector

    cl_uint* h_Y_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_Y;                         // device memory used for a vector

    cl_uint* h_Z_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_Z;                         // device memory used for a vector

    cl_uint* h_AA_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_AA;                         // device memory used for a vector

    cl_uint* h_AB_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_AB;                         // device memory used for a vector

    cl_uint* h_AC_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_AC;                         // device memory used for a vector

    cl_uint* h_AD_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_AD;                         // device memory used for a vector

    cl_uint* h_AE_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_AE;                         // device memory used for a vector

    cl_uint* h_AF_output = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*)); // host memory for output vector
    cl_mem d_AF;    

    if (argc != 3) {
        printf("Usage: %s xclbin cmd\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Fill our data sets with pattern
    h_data = (cl_uint*)aligned_alloc(MEM_ALIGNMENT,MAX_LENGTH * sizeof(cl_uint*));
    for(cl_uint i = 0; i < MAX_LENGTH; i++) {
        h_data[i]  = 0;
        h_A_output[i] = 0; 
        h_B_output[i] = 0; 
        h_C_output[i] = 0; 
        h_D_output[i] = 0; 
        h_E_output[i] = 0; 
        h_F_output[i] = 0; 
        h_G_output[i] = 0; 
        h_H_output[i] = 0; 
        h_I_output[i] = 0; 
        h_J_output[i] = 0; 
        h_K_output[i] = 0; 
        h_L_output[i] = 0; 
        h_M_output[i] = 0; 
        h_N_output[i] = 0; 
        h_O_output[i] = 0; 
        h_P_output[i] = 0; 
        h_Q_output[i] = 0; 
        h_R_output[i] = 0; 
        h_S_output[i] = 0; 
        h_T_output[i] = 0; 
        h_U_output[i] = 0; 
        h_V_output[i] = 0; 
        h_W_output[i] = 0; 
        h_X_output[i] = 0; 
        h_Y_output[i] = 0; 
        h_Z_output[i] = 0; 
        h_AA_output[i] = 0; 
        h_AB_output[i] = 0; 
        h_AC_output[i] = 0; 
        h_AD_output[i] = 0; 
        h_AE_output[i] = 0; 
        h_AF_output[i] = 0; 

    }

    // Get all platforms and then select Xilinx platform
    cl_platform_id platforms[16];       // platform id
    cl_uint platform_count;
    cl_uint platform_found = 0;
    err = clGetPlatformIDs(16, platforms, &platform_count);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to find an OpenCL platform!\n");
        printf("Test failed\n");
        return EXIT_FAILURE;
    }
    printf("INFO: Found %d platforms\n", platform_count);

    // Find Xilinx Plaftorm
    for (cl_uint iplat=0; iplat<platform_count; iplat++) {
        err = clGetPlatformInfo(platforms[iplat], CL_PLATFORM_VENDOR, 1000, (void *)cl_platform_vendor,NULL);
        if (err != CL_SUCCESS) {
            printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
            printf("Test failed\n");
            return EXIT_FAILURE;
        }
        if (strcmp(cl_platform_vendor, "Xilinx") == 0) {
            printf("INFO: Selected platform %d from %s\n", iplat, cl_platform_vendor);
            platform_id = platforms[iplat];
            platform_found = 1;
        }
    }
    if (!platform_found) {
        printf("ERROR: Platform Xilinx not found. Exit.\n");
        return EXIT_FAILURE;
    }

    // Get Accelerator compute device
    cl_uint num_devices;
    cl_uint device_found = 0;
    cl_device_id devices[16];  // compute device id
    char cl_device_name[1001];
    err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &num_devices);
    printf("INFO: Found %d devices\n", num_devices);
    if (err != CL_SUCCESS) {
        printf("ERROR: Failed to create a device group!\n");
        printf("ERROR: Test failed\n");
        return -1;
    }

    //iterate all devices to select the target device.
    for (cl_uint i=0; i<num_devices; i++) {
        err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name, 0);
        if (err != CL_SUCCESS) {
            printf("Error: Failed to get device name for device %d!\n", i);
            printf("Test failed\n");
            return EXIT_FAILURE;
        }
        printf("CL_DEVICE_NAME %s\n", cl_device_name);
        if(strcmp(cl_device_name, target_device_name) == 0) {
            device_id = devices[i];
            device_found = 1;
            printf("Selected %s as the target device\n", cl_device_name);
        }
    }

    if (!device_found) {
        printf("Target device %s not found. Exit.\n", target_device_name);
        return EXIT_FAILURE;
    }

    // Create a compute context
    //
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    if (!context) {
        printf("Error: Failed to create a compute context!\n");
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

    // Create a command commands
    commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
    if (!commands) {
        printf("Error: Failed to create a command commands!\n");
        printf("Error: code %i\n",err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

    cl_int status;

    // Create Program Objects
    // Load binary from disk
    unsigned char *kernelbinary;
    char *xclbin = argv[1];

    //------------------------------------------------------------------------------
    // xclbin
    //------------------------------------------------------------------------------
    printf("INFO: loading xclbin %s\n", xclbin);
    cl_uint n_i0 = load_file_to_memory(xclbin, (char **) &kernelbinary);
    if (n_i0 < 0) {
        printf("failed to load kernel from xclbin: %s\n", xclbin);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

    size_t n0 = n_i0;

    // Create the compute program from offline
    program = clCreateProgramWithBinary(context, 1, &device_id, &n0,
                                        (const unsigned char **) &kernelbinary, &status, &err);
    free(kernelbinary);

    if ((!program) || (err!=CL_SUCCESS)) {
        printf("Error: Failed to create compute program from binary %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    // Build the program executable
    //
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

    // Create the compute kernel in the program we wish to run
    //
    kernel = clCreateKernel(program, "clients_only", &err);
    if (!kernel || err != CL_SUCCESS) {
        printf("Error: Failed to create compute kernel!\n");
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

    // Create structs to define memory bank mapping
    cl_mem_ext_ptr_t mem_ext;
    mem_ext.obj = NULL;
    mem_ext.param = 0;


    mem_ext.flags = 0|XCL_MEM_TOPOLOGY;
    d_A = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 1|XCL_MEM_TOPOLOGY;
    d_B = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 2|XCL_MEM_TOPOLOGY;
    d_C = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 3|XCL_MEM_TOPOLOGY;
    d_D = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 4|XCL_MEM_TOPOLOGY;
    d_E = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 5|XCL_MEM_TOPOLOGY;
    d_F = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 6|XCL_MEM_TOPOLOGY;
    d_G = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 7|XCL_MEM_TOPOLOGY;
    d_H = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 8|XCL_MEM_TOPOLOGY;
    d_I = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 9|XCL_MEM_TOPOLOGY;
    d_J = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 10|XCL_MEM_TOPOLOGY;
    d_K = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 11|XCL_MEM_TOPOLOGY;
    d_L = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 12|XCL_MEM_TOPOLOGY;
    d_M = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 13|XCL_MEM_TOPOLOGY;
    d_N = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 14|XCL_MEM_TOPOLOGY;
    d_O = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 15|XCL_MEM_TOPOLOGY;
    d_P = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }

     mem_ext.flags = 16|XCL_MEM_TOPOLOGY;
    d_Q = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 17|XCL_MEM_TOPOLOGY;
    d_R = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 18|XCL_MEM_TOPOLOGY;
    d_S = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 19|XCL_MEM_TOPOLOGY;
    d_T = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 20|XCL_MEM_TOPOLOGY;
    d_U = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 21|XCL_MEM_TOPOLOGY;
    d_V = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 22|XCL_MEM_TOPOLOGY;
    d_W = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 23|XCL_MEM_TOPOLOGY;
    d_X = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 24|XCL_MEM_TOPOLOGY;
    d_Y = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 25|XCL_MEM_TOPOLOGY;
    d_Z = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 26|XCL_MEM_TOPOLOGY;
    d_AA = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 27|XCL_MEM_TOPOLOGY;
    d_AB = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 28|XCL_MEM_TOPOLOGY;
    d_AC = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 29|XCL_MEM_TOPOLOGY;
    d_AD = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 30|XCL_MEM_TOPOLOGY;
    d_AE = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }


    mem_ext.flags = 31|XCL_MEM_TOPOLOGY;
    d_AF = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX,  sizeof(cl_uint) * number_of_words, &mem_ext, &err);
    if (err != CL_SUCCESS) {
      std::cout << "Return code for clCreateBuffer flags=" << mem_ext.flags << ": " << err << std::endl;
    }



    if (!(d_A&&d_B&&d_C&&d_D&&d_E&&d_F&&d_G&&d_H&&d_I&&d_J&&d_K&&d_L&&d_M&&d_N&&d_O&&d_P && d_Q&&d_R&&d_S&&d_T&&d_U&&d_V&&d_W&&d_X&&d_Y&&d_Z&&d_AA&&d_AB&&d_AC&&d_AD&&d_AE&&d_AF)) {
        printf("Error: Failed to allocate device memory!\n");
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_A, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_A: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_B, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_B: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_C, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_C: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_D, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_D: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_E, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_E: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_F, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_F: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_G, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_G: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_H, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_H: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_I, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_I: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_J, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_J: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_K, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_K: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_L, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_L: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_M, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_M: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_N, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_N: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_O, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_O: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_P, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_P: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

 err = clEnqueueWriteBuffer(commands, d_Q, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_A: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_R, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_B: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_S, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_C: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_T, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_D: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_U, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_E: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_V, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_F: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_W, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_G: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_X, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_H: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_Y, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_I: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_Z, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_J: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_AA, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_K: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_AB, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_L: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_AC, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_M: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_AD, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_N: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_AE, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_O: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    err = clEnqueueWriteBuffer(commands, d_AF, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_data, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to write to source array h_data: d_P: %d!\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }


    // Set the arguments to our compute kernel
    // cl_uint vector_length = MAX_LENGTH;
    err = 0;
    cl_uint d_scalar00 = std::stoul(argv[2],nullptr,0);
    err |= clSetKernelArg(kernel, 0, sizeof(cl_uint), &d_scalar00); // Not used in example RTL logic.
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_A); 
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_B); 
    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_C); 
    err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_D); 
    err |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &d_E); 
    err |= clSetKernelArg(kernel, 6, sizeof(cl_mem), &d_F); 
    err |= clSetKernelArg(kernel, 7, sizeof(cl_mem), &d_G); 
    err |= clSetKernelArg(kernel, 8, sizeof(cl_mem), &d_H); 
    err |= clSetKernelArg(kernel, 9, sizeof(cl_mem), &d_I); 
    err |= clSetKernelArg(kernel, 10, sizeof(cl_mem), &d_J); 
    err |= clSetKernelArg(kernel, 11, sizeof(cl_mem), &d_K); 
    err |= clSetKernelArg(kernel, 12, sizeof(cl_mem), &d_L); 
    err |= clSetKernelArg(kernel, 13, sizeof(cl_mem), &d_M); 
    err |= clSetKernelArg(kernel, 14, sizeof(cl_mem), &d_N); 
    err |= clSetKernelArg(kernel, 15, sizeof(cl_mem), &d_O); 
    err |= clSetKernelArg(kernel, 16, sizeof(cl_mem), &d_P); 
    err |= clSetKernelArg(kernel, 17, sizeof(cl_mem), &d_Q); 
    err |= clSetKernelArg(kernel, 18, sizeof(cl_mem), &d_R); 
    err |= clSetKernelArg(kernel, 19, sizeof(cl_mem), &d_S); 
    err |= clSetKernelArg(kernel, 20, sizeof(cl_mem), &d_T); 
    err |= clSetKernelArg(kernel, 21, sizeof(cl_mem), &d_U); 
    err |= clSetKernelArg(kernel, 22, sizeof(cl_mem), &d_V); 
    err |= clSetKernelArg(kernel, 23, sizeof(cl_mem), &d_W); 
    err |= clSetKernelArg(kernel, 24, sizeof(cl_mem), &d_X); 
    err |= clSetKernelArg(kernel, 25, sizeof(cl_mem), &d_Y); 
    err |= clSetKernelArg(kernel, 26, sizeof(cl_mem), &d_Z); 
    err |= clSetKernelArg(kernel, 27, sizeof(cl_mem), &d_AA); 
    err |= clSetKernelArg(kernel, 28, sizeof(cl_mem), &d_AB); 
    err |= clSetKernelArg(kernel, 29, sizeof(cl_mem), &d_AC); 
    err |= clSetKernelArg(kernel, 30, sizeof(cl_mem), &d_AD); 
    err |= clSetKernelArg(kernel, 31, sizeof(cl_mem), &d_AE); 
    err |= clSetKernelArg(kernel, 32, sizeof(cl_mem), &d_AF); 

    if (err != CL_SUCCESS) {
        printf("Error: Failed to set kernel arguments! %d\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

    size_t global[1];
    size_t local[1];
    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    printf("Starting the program \n", err);
    global[0] = 1;
    local[0] = 1;
    err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, (size_t*)&global, (size_t*)&local, 0, NULL, NULL);
    if (err) {
        printf("Error: Failed to execute kernel! %d\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }

    clFinish(commands);

  printf("Kernel done \n", err);
    // Read back the results from the device to verify the output
    //
    cl_event readevent;

    err = 0;
    err |= clEnqueueReadBuffer( commands, d_A, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_A_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_B, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_B_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_C, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_C_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_D, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_D_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_E, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_E_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_F, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_F_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_G, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_G_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_H, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_H_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_I, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_I_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_J, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_J_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_K, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_K_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_L, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_L_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_M, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_M_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_N, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_N_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_O, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_O_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_P, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_P_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_Q, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_Q_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_R, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_R_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_S, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_S_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_T, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_T_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_U, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_U_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_V, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_V_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_W, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_W_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_X, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_X_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_Y, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_Y_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_Z, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_Z_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_AA, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_AA_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_AB, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_AB_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_AC, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_AC_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_AD, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_AD_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_AE, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_AE_output, 0, NULL, &readevent );

    err |= clEnqueueReadBuffer( commands, d_AF, CL_TRUE, 0, sizeof(cl_uint) * number_of_words, h_AF_output, 0, NULL, &readevent );

    if (err != CL_SUCCESS) {
        printf("Error: Failed to read output array! %d\n", err);
        printf("Test failed\n");
        return EXIT_FAILURE;
    }
    clWaitForEvents(1, &readevent);
    // Check Results
  printf("Done reading \n", err);

  if(h_A_output[7] != 0)
  {
    printf("ERROR in clients_only::m00_axi \n");
    check_status = 1;
  }

  printf("Cycle count m00 axi : %d\n",h_A_output[0]);

  if(h_B_output[7] != 0)
  {
    printf("ERROR in clients_only::m01_axi \n");
    check_status = 1;
  }

  printf("Cycle count m01 axi : %d\n",h_B_output[0]);

    if(h_C_output[7] != 0)
  {
    printf("%x \n",h_C_output[7]);
    printf("ERROR in clients_only::m02_axi \n");
    check_status = 1;
  }

  printf("Cycle count m02 axi : %d\n",h_C_output[0]);

  if(h_D_output[7] != 0)
  {
    printf("ERROR in clients_only::m03_axi \n");
    check_status = 1;
  }

  printf("Cycle count m03 axi : %d\n",h_D_output[0]);

  if(h_E_output[7] != 0)
  {
    printf("ERROR in clients_only::m04_axi \n");
    check_status = 1;
  }

  printf("Cycle count m04 axi : %d\n",h_E_output[0]);

  if(h_F_output[7] != 0)
  {
    printf("ERROR in clients_only::m05_axi \n");
    check_status = 1;
  }

  printf("Cycle count m05 axi : %d\n",h_F_output[0]);

    if(h_G_output[7] != 0)
  {
    printf("ERROR in clients_only::m06_axi \n");
    check_status = 1;
  }

  printf("Cycle count m06 axi : %d\n",h_G_output[0]);

  if(h_H_output[7] != 0)
  {
    printf("ERROR in clients_only::m07_axi \n");
    check_status = 1;
  }

  printf("Cycle count m07 axi : %d\n",h_H_output[0]);

  

  if(h_I_output[7] != 0)
  {
    printf("ERROR in clients_only::m08_axi \n");
    check_status = 1;
  }

  printf("Cycle count m08 axi : %d\n",h_I_output[0]);

  if(h_J_output[7] != 0)
  {
    printf("ERROR in clients_only::m09_axi \n");
    check_status = 1;
  }

  printf("Cycle count m09 axi : %d\n",h_J_output[0]);

    if(h_K_output[7] != 0)
  {
    printf("ERROR in clients_only::m10_axi \n");
    check_status = 1;
  }

  printf("Cycle count m10 axi : %d\n",h_K_output[0]);

  if(h_L_output[7] != 0)
  {
    printf("ERROR in clients_only::m11_axi \n");
    check_status = 1;
  }

  printf("Cycle count m11 axi : %d\n",h_L_output[0]);

  if(h_M_output[7] != 0)
  {
    printf("ERROR in clients_only::m12_axi \n");
    check_status = 1;
  }

  printf("Cycle count m12 axi : %d\n",h_M_output[0]);

  if(h_N_output[7] != 0)
  {
    printf("ERROR in clients_only::m13_axi \n");
    check_status = 1;
  }

  printf("Cycle count m13 axi : %d\n",h_N_output[0]);

    if(h_O_output[7] != 0)
  {
    printf("ERROR in clients_only::m14_axi \n");
    check_status = 1;
  }

  printf("Cycle count m14 axi : %d\n",h_O_output[0]);

  if(h_P_output[7] != 0)
  {
    printf("ERROR in clients_only::m15_axi \n");
    check_status = 1;
  }

  printf("Cycle count m15 axi : %d\n",h_P_output[0]);

  
  if(h_Q_output[7] != 0)
  {
    printf("ERROR in clients_only::m16_axi \n");
    check_status = 1;
  }

  printf("Cycle count m16 axi : %d\n",h_Q_output[0]);

  if(h_R_output[7] != 0)
  {
    printf("ERROR in clients_only::m17_axi \n");
    check_status = 1;
  }

  printf("Cycle count m17 axi : %d\n",h_R_output[0]);

    if(h_S_output[7] != 0)
  {
    printf("ERROR in clients_only::m18_axi \n");
    check_status = 1;
  }

  printf("Cycle count m18 axi : %d\n",h_S_output[0]);

  if(h_T_output[7] != 0)
  {
    printf("ERROR in clients_only::m19_axi \n");
    check_status = 1;
  }

  printf("Cycle count m19 axi : %d\n",h_T_output[0]);

  if(h_U_output[7] != 0)
  {
    printf("ERROR in clients_only::m20_axi \n");
    check_status = 1;
  }

  printf("Cycle count m20 axi : %d\n",h_U_output[0]);

  if(h_V_output[7] != 0)
  {
    printf("ERROR in clients_only::m21_axi \n");
    check_status = 1;
  }

  printf("Cycle count m21 axi : %d\n",h_V_output[0]);

    if(h_W_output[7] != 0)
  {
    printf("ERROR in clients_only::m22_axi \n");
    check_status = 1;
  }

  printf("Cycle count m22 axi : %d\n",h_W_output[0]);

  if(h_X_output[7] != 0)
  {
    printf("ERROR in clients_only::m23_axi \n");
    check_status = 1;
  }

  printf("Cycle count m23 axi : %d\n",h_X_output[0]);

  

  if(h_Y_output[7] != 0)
  {
    printf("ERROR in clients_only::m24_axi \n");
    check_status = 1;
  }

  printf("Cycle count m24 axi : %d\n",h_Y_output[0]);

  if(h_Z_output[7] != 0)
  {
    printf("ERROR in clients_only::m25_axi \n");
    check_status = 1;
  }

  printf("Cycle count m25 axi : %d\n",h_Z_output[0]);

    if(h_AA_output[7] != 0)
  {
    printf("ERROR in clients_only::m26_axi \n");
    check_status = 1;
  }

  printf("Cycle count m26 axi : %d\n",h_AA_output[0]);

  if(h_AB_output[7] != 0)
  {
    printf("ERROR in clients_only::m27_axi \n");
    check_status = 1;
  }

  printf("Cycle count m27 axi : %d\n",h_AB_output[0]);

  if(h_AC_output[7] != 0)
  {
    printf("ERROR in clients_only::m28_axi \n");
    check_status = 1;
  }

  printf("Cycle count m28 axi : %d\n",h_AC_output[0]);

  if(h_AD_output[7] != 0)
  {
    printf("ERROR in clients_only::m29_axi \n");
    check_status = 1;
  }

  printf("Cycle count m29 axi : %d\n",h_AD_output[0]);

    if(h_AE_output[7] != 0)
  {
    printf("ERROR in clients_only::m30_axi \n");
    check_status = 1;
  }

  printf("Cycle count m30 axi : %d\n",h_AE_output[0]);

  if(h_AF_output[7] != 0)
  {
    printf("ERROR in clients_only::m31_axi \n");
    check_status = 1;
  }

  printf("Cycle count m31 axi : %d\n",h_AF_output[0]);

  

  



    // for (cl_uint i = 0; i < 8; i+=1) {
    //     if ((h_data[i] + 1) != h_A_output[i]) {
    //         printf("ERROR in clients_only::m00_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x) (0x%x)\n", i, i*4, h_data[i], h_data[i], h_A_output[i], h_A_output[i], h_A_output[i]);
    //         check_status = 1;
    //     }
    //    //printf("i=%d, input=%d, output=%d\n", i,  h_data[i]+1, h_A_output[i]);
    // }


//     for (cl_uint i = 0;i < 8; i+=1) {
//         if ((h_data[i] + 1) != h_B_output[i]) {
//             printf("ERROR in clients_only::m01_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x) (0x%x) \n", i, i*4, h_data[i], h_data[i], h_B_output[i], h_B_output[i],h_B_output[i]);
//             check_status = 1;
//         }
//        //printf("i=%d, input=%d, output=%d\n", i,  h_data[i]+1, h_B_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_C_output[i]) {
//             printf("ERROR in clients_only::m02_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_C_output[i], h_C_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_C_input[i], h_C_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_D_output[i]) {
//             printf("ERROR in clients_only::m03_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_D_output[i], h_D_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_D_input[i], h_D_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_E_output[i]) {
//             printf("ERROR in clients_only::m04_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_E_output[i], h_E_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_E_input[i], h_E_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_F_output[i]) {
//             printf("ERROR in clients_only::m05_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_F_output[i], h_F_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_F_input[i], h_F_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_G_output[i]) {
//             printf("ERROR in clients_only::m06_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_G_output[i], h_G_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_G_input[i], h_G_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_H_output[i]) {
//             printf("ERROR in clients_only::m07_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_H_output[i], h_H_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_H_input[i], h_H_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_I_output[i]) {
//             printf("ERROR in clients_only::m08_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_I_output[i], h_I_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_I_input[i], h_I_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_J_output[i]) {
//             printf("ERROR in clients_only::m09_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_J_output[i], h_J_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_J_input[i], h_J_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_K_output[i]) {
//             printf("ERROR in clients_only::m10_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_K_output[i], h_K_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_K_input[i], h_K_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_L_output[i]) {
//             printf("ERROR in clients_only::m11_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_L_output[i], h_L_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_L_input[i], h_L_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_M_output[i]) {
//             printf("ERROR in clients_only::m12_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_M_output[i], h_M_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_M_input[i], h_M_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_N_output[i]) {
//             printf("ERROR in clients_only::m13_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_N_output[i], h_N_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_N_input[i], h_N_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_O_output[i]) {
//             printf("ERROR in clients_only::m14_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_O_output[i], h_O_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_O_input[i], h_O_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_P_output[i]) {
//             printf("ERROR in clients_only::m15_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_P_output[i], h_P_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_P_input[i], h_P_output[i]);
//     }

//  for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_Q_output[i]) {
//             printf("ERROR in clients_only::m16_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_Q_output[i], h_Q_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_A_input[i], h_A_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_R_output[i]) {
//             printf("ERROR in clients_only::m17_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_R_output[i], h_R_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_R_input[i], h_R_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_S_output[i]) {
//             printf("ERROR in clients_only::m18_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_S_output[i], h_S_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_S_input[i], h_S_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_T_output[i]) {
//             printf("ERROR in clients_only::m19_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_T_output[i], h_T_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_T_input[i], h_T_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_U_output[i]) {
//             printf("ERROR in clients_only::m20_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_U_output[i], h_U_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_U_input[i], h_U_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_V_output[i]) {
//             printf("ERROR in clients_only::m21_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_V_output[i], h_V_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_V_input[i], h_V_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_W_output[i]) {
//             printf("ERROR in clients_only::m22_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_W_output[i], h_W_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_W_input[i], h_W_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_X_output[i]) {
//             printf("ERROR in clients_only::m23_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_X_output[i], h_X_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_X_input[i], h_X_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_Y_output[i]) {
//             printf("ERROR in clients_only::m24_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_Y_output[i], h_Y_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_Y_input[i], h_Y_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_Z_output[i]) {
//             printf("ERROR in clients_only::m25_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_Z_output[i], h_Z_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_Z_input[i], h_Z_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AA_output[i]) {
//             printf("ERROR in clients_only::m26_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AA_output[i], h_AA_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AA_input[i], h_AA_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AB_output[i]) {
//             printf("ERROR in clients_only::m27_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AB_output[i], h_AB_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AB_input[i], h_AB_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AC_output[i]) {
//             printf("ERROR in clients_only::m28_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AC_output[i], h_AC_output[i]);
//             check_status = 1;
//         }
//         if ((h_data[i] + 1) != h_AD_output[i]) {
//             printf("ERROR in clients_only::m29_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AD_output[i], h_AD_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AD_input[i], h_AD_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AE_output[i]) {
//             printf("ERROR in clients_only::m30_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AE_output[i], h_AE_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AE_input[i], h_AE_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AF_output[i]) {
//             printf("ERROR in clients_only::m31_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AF_output[i], h_AF_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AF_input[i], h_AF_output[i]);
//     }

//   // printf("Comp. done \n", err);


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AD_output[i]) {
//             printf("ERROR in clients_only::m29_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AD_output[i], h_AD_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AD_input[i], h_AD_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AE_output[i]) {
//             printf("ERROR in clients_only::m30_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AE_output[i], h_AE_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AE_input[i], h_AE_output[i]);
//     }


//     for (cl_uint i = 15;i < 16; i+=8) {
//         if ((h_data[i] + 1) != h_AF_output[i]) {
//             printf("ERROR in clients_only::m31_axi - array index %d (host addr 0x%03x) - input=%d (0x%x), output=%d (0x%x)\n", i, i*4, h_data[i], h_data[i], h_AF_output[i], h_AF_output[i]);
//             check_status = 1;
//         }
//       //  printf("i=%d, input=%d, output=%d\n", i,  h_AF_input[i], h_AF_output[i]);
//     }

  printf("Comp. done \n", err);

    //--------------------------------------------------------------------------
    // Shutdown and cleanup
    //-------------------------------------------------------------------------- 
    clReleaseMemObject(d_A);
    free(h_A_output);

    clReleaseMemObject(d_B);
    free(h_B_output);

    clReleaseMemObject(d_C);
    free(h_C_output);

    clReleaseMemObject(d_D);
    free(h_D_output);

    clReleaseMemObject(d_E);
    free(h_E_output);

    clReleaseMemObject(d_F);
    free(h_F_output);

    clReleaseMemObject(d_G);
    free(h_G_output);

    clReleaseMemObject(d_H);
    free(h_H_output);

    clReleaseMemObject(d_I);
    free(h_I_output);

    clReleaseMemObject(d_J);
    free(h_J_output);

    clReleaseMemObject(d_K);
    free(h_K_output);

    clReleaseMemObject(d_L);
    free(h_L_output);

    clReleaseMemObject(d_M);
    free(h_M_output);

    clReleaseMemObject(d_N);
    free(h_N_output);

    clReleaseMemObject(d_O);
    free(h_O_output);

    clReleaseMemObject(d_P);
    free(h_P_output);

    clReleaseMemObject(d_Q);
    free(h_Q_output);

    clReleaseMemObject(d_R);
    free(h_R_output);

    clReleaseMemObject(d_S);
    free(h_S_output);

    clReleaseMemObject(d_T);
    free(h_T_output);

    clReleaseMemObject(d_U);
    free(h_U_output);

    clReleaseMemObject(d_V);
    free(h_V_output);

    clReleaseMemObject(d_W);
    free(h_W_output);

    clReleaseMemObject(d_X);
    free(h_X_output);

    clReleaseMemObject(d_Y);
    free(h_Y_output);

    clReleaseMemObject(d_Z);
    free(h_Z_output);

    clReleaseMemObject(d_AA);
    free(h_AA_output);

    clReleaseMemObject(d_AB);
    free(h_AB_output);

    clReleaseMemObject(d_AC);
    free(h_AC_output);

    clReleaseMemObject(d_AD);
    free(h_AD_output);

    clReleaseMemObject(d_AE);
    free(h_AE_output);

    clReleaseMemObject(d_AF);
    free(h_AF_output);




    free(h_data);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(commands);
    clReleaseContext(context);

    if (check_status) {
        printf("INFO: Test failed\n");
        return EXIT_FAILURE;
    } else {
        printf("INFO: Test completed successfully.\n");
        return EXIT_SUCCESS;
    }


} // end of main
