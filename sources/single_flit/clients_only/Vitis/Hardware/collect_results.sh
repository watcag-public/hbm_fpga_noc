
#!/bin/bash
set -x

declare -a policy=("P2P-32" "P2P-8" "P2P-4" "P2P-2" "P2P-1" "CB-1" "CB-4" "CS-1" "CS-16" "BR" "TOR" "NN" "CC")
cmd=("0xff020"  "0xff008"  "0xff004"  "0xff002"  "0xff001"  "0xff041"  "0xff044"  "0xff081"  "0xff090" "0xff0c1" "0xff101" "0xff141" "0xff181")  
i=0
rm average_vitis.csv
echo "policy,burst_len,bandwidth">>average_vitis.csv
for cmd in ${cmd[@]}
do
    ./clients_only binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./clients_only binary_container_1.xclbin $cmd 
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    ./clients_only binary_container_1.xclbin $cmd
    cat profile_summary.csv | grep -A 2 "Top Data Transfer: Kernels to Global Memory" | tail -n1|cut -d"," -f9 >> results_vitis.txt
    
    bw=`awk '{ total += $1 } END { print total/NR }' results_vitis.txt`
    
    echo "${policy[i]},1,$bw" >> average_vitis.csv
    rm results_vitis.txt
    ((i=i+1))
done

    