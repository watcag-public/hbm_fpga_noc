set prj_name $argv
create_project $prj_name ./$prj_name -part xcu280-fsvh2892-2L-e -f
set_property board_part xilinx.com:au280:part0:1.1 [current_project]

source add_prj_files.tcl
source vip.tcl
source generate_kernel.tcl
source package_xo.tcl

exit