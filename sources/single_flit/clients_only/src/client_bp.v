`include	"commands.h"
 `define SYNTHETIC

 `timescale 1ps / 1ps
module client 
#(

	parameter N 	= 2,		// total number of clients
	parameter M_A_W = 16,
	parameter A_W	= $clog2(N)+1,	// address width
	parameter P_W	= 32,		// data width
	parameter D_W   = 1+M_A_W+A_W+P_W,
	parameter WRAP  = 1,            // wrapping means throttling of reinjection
	parameter PAT   = `RANDOM,      // default RANDOM pattern
	parameter RATE  = 100,           // rate of injection (in percent) 
	parameter LIMIT = 16,           // when to stop injectin packets
	parameter SIGMA = 4,            // radius for LOCAL traffic
	parameter posx 	= 2,		// position
	parameter filename = "posx"
	
)


(
	input				clk,
	input				rst,
	input				ce,
	input	wire	`Cmd		cmd,
	//last,Destination PE addr,RW,Memory addr, Source addr,   DataWidth
	//1,M_A_W,A_W,1,A_W,D_W
	input		[ A_W+D_W:0]	c_i,
	input				c_i_v,
	output	wire			c_i_bp,

	output	reg	[ A_W+D_W:0]	c_o,
	output	reg			c_o_v,
	input				c_o_bp,

	output	wire			done
);

localparam reg[31:0] packet_count = 2**23;
wire [22:0] r_wire;
wire [7:0] r_dest;
wire [7:0] r;
//lfsr rand (.CLK(clk),.RESET(rst),.RAND(r_wire));
assign	c_i_bp	= 1'b0;

//integer cmd=`Cmd_/IDLE;
reg wait_for_write=1'b0;
reg wait_to_send = 1'b0;
reg[31:0]	 attempts,sent,read_request;
reg 			done_sig;
reg	[A_W-1:0]	next = 0; //Destination PE
reg [M_A_W-1:0] mem_addr;//Destination Mem
reg [A_W-1:0]   source_addr=0;
reg             RW=0;
reg             MemPacket =1;
reg			    next_v = 0;
reg [P_W-1:0] 	tmp=0;
reg [27:0] offset = 0;
reg error = 0;
reg [31:0] read_response = 0;
reg restart = 0;

//`ifndef locality
//    `ifdef eight_cross_eight
//    localparam loc = (posx[4:0]<N/2)?(((posx[4:0]+2)*2)%N):(((((posx[4:0]%(N/2))+2)*2)+1)%N);
//     reg [4:0] dest= loc;//=(posx[4:0]<28)?(posx[4:0]+4):(posx[4:0]%4);//==31?0:posx[4:0]+1;//posx[4:0];
//     reg [4:0] src = loc;//=(posx[4:0]<28)?(posx[4:0]+4):(posx[4:0]%4);//==31?0:posx[4:0]+1;//posx[4:0];
//    `elsif sixteen_cross_sixteen
//    localparam loc = (posx[4:0]<N/2)?((2*posx[4:0]+(N/2))%N):(posx[4:0]+(posx[4:0]-N/2+1))%N;
//     reg [4:0] dest = loc;//=(posx[4:0]<28)?(posx[4:0]+4):(posx[4:0]%4);//==31?0:posx[4:0]+1;//posx[4:0];
//     reg [4:0] src  = loc;
// `endif
//`elsif locality
//   `ifdef eight_cross_eight
//    localparam loc = (posx[4:0]<N/2)?(((posx[4:0]+2)*2)%N):(((((posx[4:0]%(N/2))+2)*2)+1)%N);
//     reg [4:0] dest= loc;//=(posx[4:0]<28)?(posx[4:0]+4):(posx[4:0]%4);//==31?0:posx[4:0]+1;//posx[4:0];
//     reg [4:0] src = loc;//=(posx[4:0]<28)?(posx[4:0]+4):(posx[4:0]%4);//==31?0:posx[4:0]+1;//posx[4:0];
//    `elsif sixteen_cross_sixteen
//    localparam loc = (posx[4:0]<N/2)?((2*posx[4:0]+(N/2))%N):(posx[4:0]+(posx[4:0]-N/2+1))%N;
//     reg [4:0] dest = loc;//=(posx[4:0]<28)?(posx[4:0]+4):(posx[4:0]%4);//==31?0:posx[4:0]+1;//posx[4:0];
//     reg [4:0] src  = loc;
//   `else
//     localparam loc = (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);
//     reg [4:0] dest= loc;//=(posx[4:0]<28)?(posx[4:0]+4):(posx[4:0]%4);//==31?0:posx[4:0]+1;//posx[4:0];
//     reg [4:0] src = loc;
//    `endif
//`endif
function [9:0] bitrev(input [4:0] i);
		bitrev = {i[0],i[1],i[2],i[3],i[4]};
	endfunction

 localparam base = (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);

 reg [4:0] loc  =   (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);
 reg [4:0] dest =   (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);
 reg [4:0] src  =   (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);

	reg [63:0] now=0;
	
		lfsr #(.x(posx[4:0]+1),.bit_width(23)) rand(.CLK(clk),.RESET(rst|restart),.RAND(r_wire),.enable(wait_for_write|c_o_bp|~(r <= cmd`rate)));
		lfsr #(.x(posx[4:0]+2)) rand2(.CLK(clk),.RESET(rst|restart),.RAND(r_dest),.enable(wait_for_write|c_o_bp|~(r <= cmd`rate)));
		
		lfsr #(.x(posx[4:0]+3)) rate_rand(.CLK(clk),.RESET(rst),.RAND(r),.enable(0));
		
always@(posedge clk)
begin
	if (rst==1'b1)
	begin
		c_o		<= 'b0;
		c_o_v		<= 1'b0;
//		r		<= 'b0;
		attempts	 <= 0;
		sent		 <= 0;	
		read_request <= 0;
//       `ifdef addr_rand
        if(cmd`access_type == 1)
            offset <= (posx[4:0]+1)<<5;
//       `else
        else
            offset <= 0;
//       `endif
		wait_for_write <= 1'b0;
		wait_to_send <= 1'b0;
		

		case(cmd`channel)
		 `cb: begin
		 
              if(loc[4:0] < N/2) begin
                loc[4:0]  <= (base[4:0] +4) & (N/2 -1 );
                dest[4:0] <= (base[4:0]+4) & (N/2 -1);
                src[4:0]  <= (base[4:0] +4) & (N/2 -1 );
              end
		      else begin
		        loc[4:0] <=  N/2 + ((base[4:0] +4) & (N/2 -1 )) ;        
                dest[4:0] <= N/2 + ((base[4:0]+4) & (N/2 -1)  ) ;          
                src[4:0] <=  N/2 + ((base[4:0] +4) & (N/2 -1 )) ;        
		      end
		  end
		  `cs: begin
		      loc  <= (posx[4:0]<N/2)?((2*posx[4:0]+(N/2))%N):(posx[4:0]+(posx[4:0]-N/2+1))%N;
		      dest <= (posx[4:0]<N/2)?((2*posx[4:0]+(N/2))%N):(posx[4:0]+(posx[4:0]-N/2+1))%N;
		      src  <= (posx[4:0]<N/2)?((2*posx[4:0]+(N/2))%N):(posx[4:0]+(posx[4:0]-N/2+1))%N;
		  end
		  `br:  begin
		  
              loc  <= bitrev((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1));
              dest <= bitrev((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1));
              src  <= bitrev((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1));
		      
		  end
		  `nn: begin
		      loc  <= ((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1)) + 1'b1;
              dest <= ((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1)) + 1'b1;
              src  <= ((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1)) + 1'b1;
		  end
          `to: begin
		      loc  <= (((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1)) + N/2 - 1) & (N-1);
              dest <= (((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1)) + N/2 - 1) & (N-1);
              src  <= (((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1)) + N/2 - 1) & (N-1);
		  end
		  
          `cc: begin
		      loc  <= N-1 - ((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1));
              dest <= N-1 - ((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1));
              src  <= N-1 - ((posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1));
		  end
		  
		  default : begin	    
		      loc  <= (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);
              dest <= (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);
              src  <= (posx[4:0]<N/2)?(2*(posx[4:0])):(2*(posx[4:0])-N+1);
		  end
		  
		endcase
	end
	else
	begin
	
		if (c_o_bp==1'b0)
		begin
		
		 if(c_o_v == 1) begin
		 
            $display("Time%0d: Sent packet from PE(%0d) to PE(%0d) with packetid=%0d , data=%X, source_addr =%0d, mem_addr =%X, RW=%d",now,posx,next,((posx)*LIMIT+sent),c_o[P_W-1:0],source_addr,c_o[M_A_W+A_W+P_W-1:A_W+P_W],c_o[D_W-1]);
            $display("Read_request %d Write request %d",read_request,sent);

   
        end
        
        
			if((r <= cmd`rate)) 
			begin
			    
		  		
				c_o_v <= 1'b0;                
				if(next_v==1) 
				begin
				    c_o_v			<= 1'b1;
				    c_o[A_W+D_W-1:D_W]	<= next;
                 
                    c_o[A_W+P_W-1:P_W] <= source_addr;
                    c_o[M_A_W+A_W+P_W-1:A_W+P_W]<= mem_addr;
                    c_o[D_W-1] <= RW;
                    c_o[D_W-2] <= MemPacket;
                    /*PAYLOAD*/
                      case(cmd`operation)
                       `RO:begin
                            if(read_response == packet_count )
                               read_response <= 0;
                               c_o[P_W-1:0]	<= now;//
                       end
                       `WR:begin
                           if(read_response == packet_count )
                               read_response <= 0;
                               c_o[P_W-1:0]	<= mem_addr;//
                       end
                     endcase
                     
                     case(cmd`operation)
                       `RO:begin
                          c_o[P_W-1:0]	<= 0;//
                          if(read_response == packet_count )
                            c_o[P_W-1:0]	<= error<<255 | now;//
                       end
                       `WR:begin
                          c_o[P_W-1:0]	<= mem_addr;//  
                          if(read_response == packet_count )
	                            c_o[P_W-1:0]	<= error<<255 | now;//
                       end
                       `WO:begin
                           c_o[P_W-1:0]	<= sent+1;//
                       end
                       `RW:begin
                           c_o[P_W-1:0]	<= tmp;// {16{32'h00000001}} ;;
                       end
                       
                     endcase
	                   
                    if(RW) begin
                       sent <= sent + 1;
					      
//					    if(sent == packet_count) 
//					        c_o[P_W-1:0] <= now;
					/*Offset generation */
					
                       case(cmd`access_type)
                           `linear:begin
                               offset <= offset + 32;
                           end
                           `random:begin
                                 offset <= r_wire<<5;
                           end
                       endcase

                   
                       case(cmd`channel)
                           `cs:begin
                              dest[4:0] <= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                              if(loc[4:0] < N/2) begin
                                if((loc[4:0]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                    dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/2;
                                    else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(N/2)-1)
                                        dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/2;
                              end
                              else begin
                                     if((loc[4:0]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < N/2 )
                                        dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/2;
                                    else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(N)-1)
                                        dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/2;
                                end
                           end
                           `cb:begin
                               dest[4:0] <= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                               if(loc[4:0] < N/8) begin
                                  if((loc[4:0]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                        dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/8;
                                    else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(N/8)-1)
                                        dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/8;
                                end
                                else begin
                                     if((loc[4:0]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < (loc[4:0] - (loc[4:0] % 4)))
                                        dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/8;
                                    else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(loc[4:0]+ (3-(loc[4:0] % 4))))
                                        dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/8;
                               
                               end
                           end
                           
                           default: begin
                                dest[4:0] <= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                if((loc[4:0]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                    dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N;
                                else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>N-1)
                                    dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N;
                           end
                       endcase
                                           
                  
                     case(cmd`operation)
                           `RW:begin
                                wait_for_write <= 1'b0;	
                                if(read_request == packet_count && sent == packet_count - 1) begin
                                    offset <= 0;
                                    dest[4:0] <= loc[4:0];
                                end
                           end
                           `WO:begin
                              if(sent == packet_count - 1) begin
                                offset <= 0;
                                dest[4:0] <= loc[4:0];
                            end
                           end
                       endcase
                   
                 
					   end
					   
					 else begin
			     read_request<=read_request+1'b1;
			        case(cmd`operation)
                           `RW:begin
                              wait_to_send <=1'b1;
                              wait_for_write <= 1'b1;
                           end
                           `WR:begin
                                case(cmd`access_type) 
                                    `linear : begin
                                        offset <= offset + 32;
                                    end
                                    `random : begin
                                        offset <= r_wire<<5;	
                                    end
                               endcase
                               case(cmd`channel)
                                   `cs:begin
                                      dest[4:0] <= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                      if(loc[4:0] < N/2) begin
                                        if((loc[4:0]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                            dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/2;
                                            else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(N/2)-1)
                                                dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/2;
                                      end
                                      else begin
                                             if((loc[4:0]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < N/2 )
                                                dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/2;
                                            else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(N)-1)
                                                dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/2;
                                        end
                                   end
                                   `cb:begin
                                       dest[4:0] <= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                       if(loc[4:0] < N/8) begin
                                          if((loc[4:0]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                                dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/8;
                                            else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(N/8)-1)
                                                dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/8;
                                        end
                                        else begin
                                             if((loc[4:0]+ (r_dest&(cmd`sigma-1))) -(cmd`sigma>>1) < (loc[4:0] - (loc[4:0] % 4)))
                                                dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N/8;
                                            else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>(loc[4:0]+ (3-(loc[4:0] % 4))))
                                                dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N/8;
                                       
                                       end
                                   end
                                   
                                   default: begin
                                        dest[4:0] <= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1);
                                        if((loc[4:0]+ (r_dest&(cmd`sigma-1))) < (cmd`sigma>>1))
                                            dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) + N;
                                        else if((loc[4:0]+ (r_dest&(cmd`sigma-1))-(cmd`sigma>>1))>N-1)
                                            dest[4:0]<= loc[4:0] + (r_dest&(cmd`sigma-1)) - (cmd`sigma>>1) - N;
                                    end
                               endcase
                                               
                               
                           end  
                       endcase
 
					   end
					   
					   

                  
                       case(cmd`operation)
                           `RW:begin
                             if(read_request == packet_count && sent == packet_count - 1) begin
                                 wait_for_write <= 1'b1;
                             end
                           end
                           `WR:begin
                                if(sent == packet_count-2) 
                                     restart <= 1;
                                else
					                 restart <= 0;
					       
                                 if(sent == packet_count-1) begin
					                 offset <= 0;
					                 dest <= loc;
					                 if (cmd`access_type) begin
					                    offset <= (posx[4:0]+1)<<5;     
					                   end
                                end
                           end
                       endcase

				end
			end
			else
			begin
				c_o_v	<= 1'b0;
			end
		end
		else
		begin
//			c_o_v	<= 1'b0;
		end
	
		if ((attempts<LIMIT & {r} < RATE) )
		begin
			attempts	<= attempts + 1;
		end
		
		if(c_i_v==1'b1) 
		begin
			$display("Time%0d: Received packet at PE(%0d) with address=%0x packetid=%0x ",now-1,posx,c_i[255:224],c_i[31:0]);
             case(cmd`operation)
                   `RW:begin
                         wait_to_send<=1'b0;
                        tmp <= (c_i[P_W-1:0] & 256'hFFFFFFFF)+1'b1;		
                        c_o[P_W-1:0]<= (c_i[P_W-1:0] & 256'hFFFFFFFF)+1'b1;	
                   end
                   `RO:begin
                       read_response <= read_response + 1'b1;
                       if(c_i[27:0] != c_i[255:224]) 
                            error <= 1'b1;
                       end
                   `WR:begin
                       read_response <= read_response + 1'b1;
                        $display("%x",read_response);
                        if(c_i[31:0] != c_i[255:224]) 
                            error <= 1'b1;
                         end
               endcase			
			
//			`ifdef RW
//			wait_to_send<=1'b0;
//            tmp <= (c_i[P_W-1:0] & 256'hFFFFFFFF)+1'b1;		
//            c_o[P_W-1:0]<= (c_i[P_W-1:0] & 256'hFFFFFFFF)+1'b1;	
//            `endif
//            `ifdef RO
//                read_response <= read_response + 1'b1;
//                if(c_i[27:0] != c_i[255:224]) 
//                    error <= 1'b1;
//            `endif
//            `ifdef WR
//                read_response <= read_response + 1'b1;
//                $display("%x",read_response);
//                if(c_i[31:0] != c_i[255:224]) 
//                    error <= 1'b1;
//            `endif
		end
	end
end

//always @(c_o_bp,c_o_v) begin

//	if (c_o_bp==1'b0 && c_o_v == 1) begin
		 
//            if(c_o[D_W-1] == 1) begin
//                sent <= sent+1'b1;
//            end
//            else begin
//                read_request<=read_request+1'b1;
                
//            end
//    end
//end
always @(*) 
begin
next_v <= 0;
next <= 0;    
case(cmd`operation)
           `RW:begin
                 if(now >= 0 && (1) && sent <=packet_count && wait_to_send == 1'b0) begin
                     next     <= {1'b1,{A_W-1{1'b0}}};
                     mem_addr <= {dest,offset};
                     source_addr <= posx;
                     if(wait_for_write == 1'b1) 
                        RW <= 1;
                     else
                        RW<=0;
                     if((sent <=packet_count || read_request < packet_count )) begin         
                         next_v <=1;
                     end
                     else begin
                        next_v <=0;
                     end                   
                end      
                else begin
                    source_addr <= posx;
                    mem_addr<= 0;
                    RW <= 0;
                end

           end
           `RO:begin
              if(now >= 0 && (1) && read_request <packet_count ) 
                begin
                     next     <= {1'b1,{A_W-1{1'b0}}};
                     mem_addr <= {dest,5'b0,offset};
                     source_addr <= posx;
                        RW<=0;
                     if(read_request < packet_count ) begin         
                         next_v <=1;
                     end
                     else begin
                        next_v <=0;
                     end                   
                 end      
              else if( now >= 0 && read_response == packet_count) begin
                                
                     next     <= {1'b1,{A_W-1{1'b0}}};
                     mem_addr <= {loc[4:0],5'b0,23'b0};
                     source_addr <= posx;            
                     RW <= 1;
                     next_v <=1;

              end
              else begin
                        source_addr <= posx;
                        mem_addr<= 0;
                        RW <= 0;
              end
      
           end
           
           `WR:begin
                if(now >= 0 && (1) && sent <packet_count)
                    begin
                            next     <= {1'b1,{A_W-1{1'b0}}};
                            mem_addr <= {dest,offset};
                            source_addr <= posx;
                            RW <= 1;
                            if(sent <packet_count) begin         
                                next_v <=1;
                            end
                            else begin
                            next_v <=0;
                            end                   
                        end      
                else if(now >= 0  && (1) && sent==packet_count && read_request <packet_count  )
                    begin
                            next     <= {1'b1,{A_W-1{1'b0}}};
                            mem_addr <= {dest,offset};
                            source_addr <= posx;
                            RW <= 0;
                        
                            if(read_request <packet_count) begin  
                                next_v <=1;
                            end
                            
                            else begin
                            next_v <=0;
                            end                   
                        end
                 else if( now >= 0 && read_response == packet_count) begin
                                
                                 next     <= {1'b1,{A_W-1{1'b0}}};
                                 mem_addr <= {loc[4:0],5'b0,23'b0};
                             source_addr <= posx;            
                             RW <= 1;
                             next_v <=1;
                   end         
                        
                 else begin
                        source_addr <= posx;
                        mem_addr<= 0;
                        RW <= 0;
                 end
             end
             
             `WO: begin
              if(now >= 0 && (1) && sent <=packet_count)   begin
                     next     <= {1'b1,{A_W-1{1'b0}}};
                     mem_addr <= {dest,5'b0,offset};
                     source_addr <= posx;            
                     RW <= 1;
                     if((sent <=packet_count)) begin         
                         next_v <=1;
                     end
                     else begin
                        next_v <=0;
                     end                   
               end      
              else begin
                    source_addr <= posx;
                    mem_addr<= 0;
                    RW <= 1;
              end
             end
   endcase			
               
               
//`ifdef RW
//  if(now >= 0 && (1) && sent <=packet_count && wait_to_send == 1'b0) 
//    begin
//                     next     <= {1'b1,{A_W-1{1'b0}}};
//                     mem_addr <= {dest,offset};
//                     source_addr <= posx;
//                     if(wait_for_write == 1'b1) 
//                        RW <= 1;
//                     else
//                        RW<=0;
//                     if((sent <=packet_count || read_request < packet_count )) begin         
//                         next_v <=1;
//                     end
//                     else begin
//                        next_v <=0;
//                     end                   
//       end      
//  else begin
//            source_addr <= posx;
//            mem_addr<= 0;
//            RW <= 0;
//  end
  
// `endif
 
// `ifdef WO
//  if(now >= 0 && (1) && sent <=packet_count) 
//    begin
//                     next     <= {1'b1,{A_W-1{1'b0}}};
//                     mem_addr <= {dest,5'b0,offset};
//                     source_addr <= posx;            
//                     RW <= 1;
//                     if((sent <=packet_count)) begin         
//                         next_v <=1;
//                     end
//                     else begin
//                        next_v <=0;
//                     end                   
//       end      
//  else begin
//            source_addr <= posx;
//            mem_addr<= 0;
//            RW <= 1;
//  end
  
// `endif
 
// `ifdef RO
//  if(now >= 0 && (1) && read_request <packet_count ) 
//    begin
//                     next     <= {1'b1,{A_W-1{1'b0}}};
//                     mem_addr <= {dest,5'b0,offset};
//                     source_addr <= posx;
//                        RW<=0;
//                     if(read_request < packet_count ) begin         
//                         next_v <=1;
//                     end
//                     else begin
//                        next_v <=0;
//                     end                   
//     end      
//  else if( now >= 0 && read_response == packet_count) begin
                    
//                     next     <= {1'b1,{A_W-1{1'b0}}};
//                     mem_addr <= {loc[4:0],5'b0,23'b0};
//                     source_addr <= posx;            
//                     RW <= 1;
//                     next_v <=1;

//  end
//  else begin
//            source_addr <= posx;
//            mem_addr<= 0;
//            RW <= 0;
//  end
  
// `endif
  
//  `ifdef WR
//    if(now >= 0 && (1) && sent <packet_count)
//        begin
//                next     <= {1'b1,{A_W-1{1'b0}}};
//                mem_addr <= {dest,offset};
//                source_addr <= posx;
//                RW <= 1;
//                if(sent <packet_count) begin         
//                    next_v <=1;
//                end
//                else begin
//                next_v <=0;
//                end                   
//            end      
//    else if(now >= 0  && (1) && sent==packet_count && read_request <packet_count  )
//        begin
//                next     <= {1'b1,{A_W-1{1'b0}}};
//                mem_addr <= {dest,offset};
//                source_addr <= posx;
//                RW <= 0;
            
//                if(read_request <packet_count) begin  
//                    next_v <=1;
//                end
                
//                else begin
//                next_v <=0;
//                end                   
//            end
//     else if( now >= 0 && read_response == packet_count) begin
                    
//                     next     <= {1'b1,{A_W-1{1'b0}}};
//                     mem_addr <= {loc[4:0],5'b0,23'b0};
//                     source_addr <= posx;            
//                     RW <= 1;
//                     next_v <=1;
//       end         
            
// else begin
//            source_addr <= posx;
//            mem_addr<= 0;
//            RW <= 0;
//  end
//  `endif

end


	always @(posedge clk) begin
	    if(rst) begin
	       now<=0;
	    end
		else begin
            if(ce) begin
              now     <= now + 1;
            end
		end
	end
	
endmodule

