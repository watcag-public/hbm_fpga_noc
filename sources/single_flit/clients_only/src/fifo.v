module fifo
#(WIDTH = 592)
(
   s_axis_aresetn ,
   s_axis_aclk    ,
   s_axis_tvalid  ,
   s_axis_tready  ,
   s_axis_tdata   ,
                  
   m_axis_tvalid  ,
   m_axis_tready  ,
   m_axis_tdata   ,

);

   input wire s_axis_aresetn ;
   input wire s_axis_aclk    ;
   input wire s_axis_tvalid  ;
   output wire s_axis_tready  ;
   input  wire [WIDTH-1:0]s_axis_tdata   ;

                              
   output wire m_axis_tvalid  ;
   input wire  m_axis_tready  ;
   output  wire [WIDTH-1:0] m_axis_tdata   ;
    

  reg [WIDTH-1:0] data;
  reg valid = 1'b0;
  reg ready = 1'b0;
  
  assign m_axis_tdata  = data;
  assign m_axis_tvalid = valid;
  assign s_axis_tready = ready;
  
  always@(posedge s_axis_aclk) begin
    if(s_axis_aresetn) begin
        data <= 0;
        valid <= 1'b0;
        ready <= 1'b1;
    end
    
    if(s_axis_tready && s_axis_tvalid) begin
        data  <= s_axis_tdata;
        ready <= 1'b0;
        valid <= 1'b1;
    end
    
    if(m_axis_tready && m_axis_tvalid) begin
       
        ready <= 1'b1;
        valid <= 1'b0;
    end
  end
endmodule