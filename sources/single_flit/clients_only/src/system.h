`define XBAR

`ifdef XBAR
    `define XBARorM1orM0
    `define XBARorM1
    `define XBARorM0
 `endif
 
 `ifdef MESH1
    `define XBARorM1orM0
    `define XBARorM1
 `endif
 
 `ifdef MESH0
    `define XBARorM1orM0
    `define XBARorM0
 `endif
 
 `ifdef RO
    `define ROorWR
  `endif
  
  `ifdef WR
    `define ROorWR
  `endif